/* Generated by the protocol buffer compiler.  DO NOT EDIT! */

#ifndef PROTOBUF_C_CShuttleEntry_2eproto_INCLUDED
#define PROTOBUF_C_CShuttleEntry_2eproto_INCLUDED

#include <google/protobuf-c/protobuf-c.h>

PROTOBUF_C_BEGIN_DECLS

#include "BError.pb-c.h"
#include "BMath.pb-c.h"
#include "BNetwork.pb-c.h"

typedef struct _CShuttleEntry_KnockRQ CShuttleEntry_KnockRQ;
typedef struct _CShuttleEntry_KnockRS CShuttleEntry_KnockRS;


/* --- enums --- */

typedef enum _CShuttleEntry_KnockRQ_Msg {
  CShuttleEntry_KnockRQ_Msg_ID = 33558528
} CShuttleEntry_KnockRQ_Msg;
typedef enum _CShuttleEntry_KnockRS_Msg {
  CShuttleEntry_KnockRS_Msg_ID = 33558529
} CShuttleEntry_KnockRS_Msg;
typedef enum _CShuttleEntry_Msg {
  CShuttleEntry_Msg_MinId = 33558528,
  CShuttleEntry_Msg_MaxId = 33558783
} CShuttleEntry_Msg;

/* --- messages --- */

struct  _CShuttleEntry_KnockRQ
{
  ProtobufCMessage base;
  char *native_client_version;
  char *native_source_version;
  BMath_Coord *coord_info;
};
extern char CShuttleEntry_KnockRQ_native_client_version_default_value[];
extern char CShuttleEntry_KnockRQ_native_source_version_default_value[];
#define CShuttleEntry_KnockRQ_Init \
 { PROTOBUF_C_MESSAGE_INIT (&CShuttleEntry_KnockRQ_descriptor) \
    , CShuttleEntry_KnockRQ_native_client_version_default_value, CShuttleEntry_KnockRQ_native_source_version_default_value, NULL }


struct  _CShuttleEntry_KnockRS
{
  ProtobufCMessage base;
  BError_Info *error;
  BNetwork_Address *addr;
  ProtobufCBinaryData public_key;
  char *remote_client_version;
  char *remote_source_version;
  char *remote_server_version;
};
extern char CShuttleEntry_KnockRS_remote_client_version_default_value[];
extern char CShuttleEntry_KnockRS_remote_Source_version_default_value[];
extern char CShuttleEntry_KnockRS_remote_Server_version_default_value[];
#define CShuttleEntry_KnockRS_Init \
 { PROTOBUF_C_MESSAGE_INIT (&CShuttleEntry_KnockRS_descriptor) \
    , NULL, NULL, {0,NULL}, CShuttleEntry_KnockRS_remote_client_version_default_value, CShuttleEntry_KnockRS_remote_Source_version_default_value, CShuttleEntry_KnockRS_remote_Server_version_default_value }


/* CShuttleEntry_KnockRQ methods */
void   CShuttleEntry_KnockRQ_init
                     (CShuttleEntry_KnockRQ         *message);
size_t CShuttleEntry_KnockRQ_get_packed_size
                     (const CShuttleEntry_KnockRQ   *message);
size_t CShuttleEntry_KnockRQ_pack
                     (const CShuttleEntry_KnockRQ   *message,
                      unsigned char             *out);
size_t CShuttleEntry_KnockRQ_pack_to_buffer
                     (const CShuttleEntry_KnockRQ   *message,
                      ProtobufCBuffer     *buffer);
CShuttleEntry_KnockRQ *
       CShuttleEntry_KnockRQ_unpack
                     (ProtobufCAllocator  *allocator,
                      size_t               len,
                      const unsigned char       *data);
void   CShuttleEntry_KnockRQ_free_unpacked
                     (CShuttleEntry_KnockRQ *message,
                      ProtobufCAllocator *allocator);
/* CShuttleEntry_KnockRS methods */
void   CShuttleEntry_KnockRS_init
                     (CShuttleEntry_KnockRS         *message);
size_t CShuttleEntry_KnockRS_get_packed_size
                     (const CShuttleEntry_KnockRS   *message);
size_t CShuttleEntry_KnockRS_pack
                     (const CShuttleEntry_KnockRS   *message,
                      unsigned char             *out);
size_t CShuttleEntry_KnockRS_pack_to_buffer
                     (const CShuttleEntry_KnockRS   *message,
                      ProtobufCBuffer     *buffer);
CShuttleEntry_KnockRS *
       CShuttleEntry_KnockRS_unpack
                     (ProtobufCAllocator  *allocator,
                      size_t               len,
                      const unsigned char       *data);
void   CShuttleEntry_KnockRS_free_unpacked
                     (CShuttleEntry_KnockRS *message,
                      ProtobufCAllocator *allocator);
/* --- per-message closures --- */

typedef void (*CShuttleEntry_KnockRQ_Closure)
                 (const CShuttleEntry_KnockRQ *message,
                  void *closure_data);
typedef void (*CShuttleEntry_KnockRS_Closure)
                 (const CShuttleEntry_KnockRS *message,
                  void *closure_data);

/* --- services --- */


/* --- descriptors --- */

extern const ProtobufCEnumDescriptor    CShuttleEntry_Msg_descriptor;
extern const ProtobufCMessageDescriptor CShuttleEntry_KnockRQ_descriptor;
extern const ProtobufCEnumDescriptor    CShuttleEntry_KnockRQ_Msg_descriptor;
extern const ProtobufCMessageDescriptor CShuttleEntry_KnockRS_descriptor;
extern const ProtobufCEnumDescriptor    CShuttleEntry_KnockRS_Msg_descriptor;

PROTOBUF_C_END_DECLS


#endif  /* PROTOBUF_CShuttleEntry_2eproto_INCLUDED */
