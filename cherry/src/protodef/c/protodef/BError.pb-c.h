/* Generated by the protocol buffer compiler.  DO NOT EDIT! */

#ifndef PROTOBUF_C_BError_2eproto_INCLUDED
#define PROTOBUF_C_BError_2eproto_INCLUDED

#include <google/protobuf-c/protobuf-c.h>

PROTOBUF_C_BEGIN_DECLS


typedef struct _BError_Info BError_Info;


/* --- enums --- */


/* --- messages --- */

struct  _BError_Info
{
  ProtobufCMessage base;
  unsigned int code;
  char *desc;
};
extern char BError_Info_desc_default_value[];
#define BError_Info_Init \
 { PROTOBUF_C_MESSAGE_INIT (&BError_Info_descriptor) \
    , 0, BError_Info_desc_default_value }


/* BError_Info methods */
void   BError_Info_init
                     (BError_Info         *message);
size_t BError_Info_get_packed_size
                     (const BError_Info   *message);
size_t BError_Info_pack
                     (const BError_Info   *message,
                      unsigned char             *out);
size_t BError_Info_pack_to_buffer
                     (const BError_Info   *message,
                      ProtobufCBuffer     *buffer);
BError_Info *
       BError_Info_unpack
                     (ProtobufCAllocator  *allocator,
                      size_t               len,
                      const unsigned char       *data);
void   BError_Info_free_unpacked
                     (BError_Info *message,
                      ProtobufCAllocator *allocator);
/* --- per-message closures --- */

typedef void (*BError_Info_Closure)
                 (const BError_Info *message,
                  void *closure_data);

/* --- services --- */


/* --- descriptors --- */

extern const ProtobufCMessageDescriptor BError_Info_descriptor;

PROTOBUF_C_END_DECLS


#endif  /* PROTOBUF_BError_2eproto_INCLUDED */
