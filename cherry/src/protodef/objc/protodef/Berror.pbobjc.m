// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: BError.proto

// This CPP symbol can be defined to use imports that match up to the framework
// imports needed when using CocoaPods.
#if !defined(GPB_USE_PROTOBUF_FRAMEWORK_IMPORTS)
 #define GPB_USE_PROTOBUF_FRAMEWORK_IMPORTS 0
#endif

#if GPB_USE_PROTOBUF_FRAMEWORK_IMPORTS
 #import <Protobuf/GPBProtocolBuffers_RuntimeSupport.h>
#else
 #import "GPBProtocolBuffers_RuntimeSupport.h"
#endif

 #import "Berror.pbobjc.h"
// @@protoc_insertion_point(imports)

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"

#pragma mark - BError_BerrorRoot

@implementation BError_BerrorRoot

// No extensions in the file and no imports, so no need to generate
// +extensionRegistry.

@end

#pragma mark - BError_BerrorRoot_FileDescriptor

static GPBFileDescriptor *BError_BerrorRoot_FileDescriptor(void) {
  // This is called by +initialize so there is no need to worry
  // about thread safety of the singleton.
  static GPBFileDescriptor *descriptor = NULL;
  if (!descriptor) {
    GPB_DEBUG_CHECK_RUNTIME_VERSIONS();
    descriptor = [[GPBFileDescriptor alloc] initWithPackage:@"BError"
                                                 objcPrefix:@"BError_"
                                                     syntax:GPBFileSyntaxProto2];
  }
  return descriptor;
}

#pragma mark - BError_Info

@implementation BError_Info

@dynamic hasCode, code;
@dynamic hasDesc, desc;

typedef struct BError_Info__storage_ {
  uint32_t _has_storage_[1];
  uint32_t code;
  NSString *desc;
} BError_Info__storage_;

// This method is threadsafe because it is initially called
// in +initialize for each subclass.
+ (GPBDescriptor *)descriptor {
  static GPBDescriptor *descriptor = nil;
  if (!descriptor) {
    static GPBMessageFieldDescription fields[] = {
      {
        .name = "code",
        .dataTypeSpecific.className = NULL,
        .number = BError_Info_FieldNumber_Code,
        .hasIndex = 0,
        .offset = (uint32_t)offsetof(BError_Info__storage_, code),
        .flags = (GPBFieldFlags)(GPBFieldRequired | GPBFieldHasDefaultValue),
        .dataType = GPBDataTypeUInt32,
      },
      {
        .name = "desc",
        .dataTypeSpecific.className = NULL,
        .number = BError_Info_FieldNumber_Desc,
        .hasIndex = 1,
        .offset = (uint32_t)offsetof(BError_Info__storage_, desc),
        .flags = (GPBFieldFlags)(GPBFieldRequired | GPBFieldHasDefaultValue),
        .dataType = GPBDataTypeString,
      },
    };
    GPBDescriptor *localDescriptor =
        [GPBDescriptor allocDescriptorForClass:[BError_Info class]
                                     rootClass:[BError_BerrorRoot class]
                                          file:BError_BerrorRoot_FileDescriptor()
                                        fields:fields
                                    fieldCount:(uint32_t)(sizeof(fields) / sizeof(GPBMessageFieldDescription))
                                   storageSize:sizeof(BError_Info__storage_)
                                         flags:GPBDescriptorInitializationFlag_None];
    NSAssert(descriptor == nil, @"Startup recursed!");
    descriptor = localDescriptor;
  }
  return descriptor;
}

@end


#pragma clang diagnostic pop

// @@protoc_insertion_point(global_scope)
