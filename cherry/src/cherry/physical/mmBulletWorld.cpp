/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmBulletWorld.h"

#include "BulletCollision/Gimpact/btGImpactCollisionAlgorithm.h"

#include "core/mmTime.h"
#include "core/mmThread.h"

static void* __static_mmBulletWorld_HandleThread(void* pArg);

void mmBulletWorld_Init(struct mmBulletWorld* p)
{
    p->d_collision_configuration = new btDefaultCollisionConfiguration();;
    ///use the default collision dispatcher. For parallel processing you can use a diffent dispatcher (see Extras/BulletMultiThreaded)
    p->d_collision_dispatcher = new btCollisionDispatcher(p->d_collision_configuration);
    ///btDbvtBroadphase is a good general purpose broadphase. You can also try out btAxis3Sweep.
    p->d_broadphase = new btDbvtBroadphase();
    ///the default constraint solver. For parallel processing you can use a different solver (see Extras/BulletMultiThreaded)
    p->d_constraint_solver = new btSequentialImpulseConstraintSolver;
    p->d_dynamics_world = new btDiscreteDynamicsWorld(p->d_collision_dispatcher,p->d_broadphase,p->d_constraint_solver,p->d_collision_configuration);
    p->d_debug_draw = new mmBulletDebugDrawer;
    p->d_dynamics_world->setDebugDrawer(p->d_debug_draw);
    //
    mmSpinlock_Init(&p->d_locker,0);
    p->d_time_step = 1/60.0f;
    p->d_active = 1;
    p->d_state = MM_TS_CLOSED;
    //
    btGImpactCollisionAlgorithm::registerAlgorithm(p->d_collision_dispatcher);
}
void mmBulletWorld_Destroy(struct mmBulletWorld* p)
{
    delete p->d_collision_configuration;
    p->d_collision_configuration = NULL;
    delete p->d_collision_dispatcher;
    p->d_collision_dispatcher = NULL;
    delete p->d_broadphase;
    p->d_broadphase = NULL;
    delete p->d_constraint_solver;
    p->d_constraint_solver = NULL;
    delete p->d_dynamics_world;
    p->d_dynamics_world = NULL;
    delete p->d_debug_draw;
    p->d_debug_draw = NULL;
    //
    mmSpinlock_Destroy(&p->d_locker);
    p->d_time_step = 1/60.0f;
    p->d_active = 0;
    p->d_state = MM_TS_CLOSED;
}
int mmBulletWorld_StepSimulation(struct mmBulletWorld* p, btScalar timeStep,int maxSubSteps/*=1*/, btScalar fixedTimeStep/*=btScalar(1.)/btScalar(60.)*/)
{
    return p->d_dynamics_world->stepSimulation(timeStep,maxSubSteps,fixedTimeStep);
}
void mmBulletWorld_Loop(struct mmBulletWorld* p)
{
    struct timeval time_point;
    struct timeval time_front;
    struct timeval time_after;
    btScalar _time_offset = 0;
    btScalar _distance = 0;
    int _max_sub_steps = 1;
    mmGettimeofday(&time_point, NULL);
    while(MM_TS_MOTION == p->d_state )
    {
        mmGettimeofday(&time_front, NULL);
        _time_offset = time_front.tv_sec - time_point.tv_sec + (btScalar)(time_front.tv_usec - time_point.tv_usec) / MM_USEC_PER_SEC;
        _max_sub_steps = (int)(_time_offset / p->d_time_step);
        _max_sub_steps = _max_sub_steps < 1 ? 1 : _max_sub_steps;
        if ( 1 == p->d_active )
        {
            mmBulletWorld_Lock(p);
            mmBulletWorld_StepSimulation(p,_time_offset,_max_sub_steps,p->d_time_step);
            mmBulletWorld_Unlock(p);
        }
        mmGettimeofday(&time_after, NULL);
        _time_offset = time_after.tv_sec - time_front.tv_sec + (btScalar)(time_after.tv_usec - time_front.tv_usec) / MM_USEC_PER_SEC;
        _distance = p->d_time_step - _time_offset;
        time_point = time_front;
        mmMSleep( (int)( (_distance < 0 ? 0 : _distance) * MM_MSEC_PER_SEC ) );
    }
}
void mmBulletWorld_Lock(struct mmBulletWorld* p)
{
    mmSpinlock_Lock(&p->d_locker);
}
void mmBulletWorld_Unlock(struct mmBulletWorld* p)
{
    mmSpinlock_Unlock(&p->d_locker);
}
void mmBulletWorld_Draw(struct mmBulletWorld* p)
{
    p->d_debug_draw->clearGeometry();
    p->d_dynamics_world->debugDrawWorld();
    p->d_debug_draw->drawGeometry();
}
// start thread.
void mmBulletWorld_Start(struct mmBulletWorld* p)
{
    p->d_state = MM_TS_FINISH == p->d_state ? MM_TS_CLOSED : MM_TS_MOTION;
    pthread_create(&p->d_thread, NULL, &__static_mmBulletWorld_HandleThread, p);
}
// interrupt thread.
void mmBulletWorld_Interrupt(struct mmBulletWorld* p)
{
    p->d_state = MM_TS_CLOSED;
}
void mmBulletWorld_Shutdown(struct mmBulletWorld* p)
{
    p->d_state = MM_TS_FINISH;
}
// join thread.
void mmBulletWorld_Join(struct mmBulletWorld* p)
{
    pthread_join(p->d_thread, NULL);
}

static void* __static_mmBulletWorld_HandleThread(void* pArg)
{
    struct mmBulletWorld* p = (struct mmBulletWorld*)(pArg);
    mmBulletWorld_Loop(p);
    return NULL;
}