/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmBulletDebugDrawer.h"
#include "OgreCamera.h"
#include "OgreHardwareVertexBuffer.h"
#include "OgreHardwareBufferManager.h"
#include "OgreMaterialManager.h"
#include "mmBulletOgreConver.h"
#include "core/mmLoggerManager.h"

btVector3 mmBulletDebugDrawer::d_axis_x_color = btVector3(0.7f,0.0f,0.0f);
btVector3 mmBulletDebugDrawer::d_axis_y_color = btVector3(0.0f,0.7f,0.0f);
btVector3 mmBulletDebugDrawer::d_axis_z_color = btVector3(0.0f,0.0f,0.7f);

mmBulletDebugDrawer::mmBulletDebugDrawer()
    : d_node_debug_draw(NULL)
    , d_debug_mode(0)
{
    Ogre::MaterialManager* _material_manager = Ogre::MaterialManager::getSingletonPtr();
    Ogre::MaterialPtr _material_axis_x = _material_manager->getByName("color/axis_x");
    Ogre::MaterialPtr _material_axis_y = _material_manager->getByName("color/axis_y");
    Ogre::MaterialPtr _material_axis_z = _material_manager->getByName("color/axis_z");
    Ogre::MaterialPtr _material_comm_draw = _material_manager->getByName("color/comm_draw");
    this->d_axis_x.setMaterial(_material_axis_x);
    this->d_axis_y.setMaterial(_material_axis_y);
    this->d_axis_z.setMaterial(_material_axis_z);
    this->d_comm_draw.setMaterial(_material_comm_draw);
    mmSpinlock_Init(&this->d_locker,0);
}
mmBulletDebugDrawer::~mmBulletDebugDrawer()
{
    mmSpinlock_Destroy(&this->d_locker);
}
void mmBulletDebugDrawer::drawLine(const btVector3& from,const btVector3& to,const btVector3& color)
{
    if (this->d_debug_mode > 0)
    {
        mmSpinlock_Lock(&this->d_locker);
        if ( this->d_axis_x_color == color )
        {
            this->d_axis_x.addLine(
                mmBulletOgreConver_BulletToOgreVector3(from), 
                mmBulletOgreConver_BulletToOgreVector3(to));
        }
        else if ( this->d_axis_y_color == color )
        {
            this->d_axis_y.addLine(
                mmBulletOgreConver_BulletToOgreVector3(from), 
                mmBulletOgreConver_BulletToOgreVector3(to));
        } 
        else if ( this->d_axis_z_color == color )
        {
            this->d_axis_z.addLine(
                mmBulletOgreConver_BulletToOgreVector3(from), 
                mmBulletOgreConver_BulletToOgreVector3(to));
        } 
        else
        {
            this->d_comm_draw.addLine(
                mmBulletOgreConver_BulletToOgreVector3(from), 
                mmBulletOgreConver_BulletToOgreVector3(to));
        }
        mmSpinlock_Unlock(&this->d_locker);
    }
}
void mmBulletDebugDrawer::drawLine(const btVector3& from,const btVector3& to, const btVector3& fromColor, const btVector3& toColor)
{
    (void) toColor;
    this->drawLine (from, to, fromColor);
}
void mmBulletDebugDrawer::drawContactPoint(const btVector3& PointOnB,const btVector3& normalOnB,btScalar distance,int lifeTime,const btVector3& color)
{

}
void mmBulletDebugDrawer::reportErrorWarning(const char* warningString)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    mmLoggerManager_LoggerSection(gLogger, "bullet", MM_LOG_WARNING, warningString);
}
void mmBulletDebugDrawer::draw3dText(const btVector3& location,const char* textString)
{

}
void mmBulletDebugDrawer::setDebugMode(int debugMode)
{
    this->d_debug_mode = debugMode;
}
int mmBulletDebugDrawer::getDebugMode() const
{
    return this->d_debug_mode;
}
void mmBulletDebugDrawer::setSceneNode(Ogre::SceneNode* _node)
{
    if (this->d_node_debug_draw)
    {
        this->d_node_debug_draw->detachObject(&this->d_axis_x);
        this->d_node_debug_draw->detachObject(&this->d_axis_y);
        this->d_node_debug_draw->detachObject(&this->d_axis_z);
        this->d_node_debug_draw->detachObject(&this->d_comm_draw);
    }
    this->d_node_debug_draw = _node;
    if (this->d_node_debug_draw)
    {
        this->d_node_debug_draw->attachObject(&this->d_axis_x);
        this->d_node_debug_draw->attachObject(&this->d_axis_y);
        this->d_node_debug_draw->attachObject(&this->d_axis_z);
        this->d_node_debug_draw->attachObject(&this->d_comm_draw);
    }
}
Ogre::SceneNode* mmBulletDebugDrawer::getSceneNode() const
{
    return this->d_node_debug_draw;
}
void mmBulletDebugDrawer::drawGeometry()
{
    mmSpinlock_Lock(&this->d_locker);
    this->d_axis_x.drawGeometry();
    this->d_axis_y.drawGeometry();
    this->d_axis_z.drawGeometry();
    this->d_comm_draw.drawGeometry();
    mmSpinlock_Unlock(&this->d_locker);
}
void mmBulletDebugDrawer::clearGeometry()
{
    mmSpinlock_Lock(&this->d_locker);
    this->d_axis_x.clear();
    this->d_axis_y.clear();
    this->d_axis_z.clear();
    this->d_comm_draw.clear();
    mmSpinlock_Unlock(&this->d_locker);
}