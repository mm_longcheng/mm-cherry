/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmBulletShapeFileManager.h"

#include "core/mmLogger.h"

#include "BulletCollision/CollisionDispatch/btCollisionObject.h"

namespace mm
{
    mmBulletShapeFileData::mmBulletShapeFileData()
        : d_manager(NULL)
        , d_asset("")
        , d_collision_shape(NULL)
        , d_reference(0)
    {

    }
    mmBulletShapeFileData::~mmBulletShapeFileData()
    {
        this->d_importer.deleteAllData();
    }

    mmBulletShapeFileManager::mmBulletShapeFileManager()
        : d_file_context(NULL)
    {

    }
    mmBulletShapeFileManager::~mmBulletShapeFileManager()
    {
        this->Clear();
    }
    void mmBulletShapeFileManager::SetFileContext(struct mmFileContext* file_context)
    {
        this->d_file_context = file_context;
    }
    struct mmFileContext* mmBulletShapeFileManager::GetFileContext()
    {
        return this->d_file_context;
    }
    mmBulletShapeFileData* mmBulletShapeFileManager::Produce(const std::string& fullname)
    {
        mmBulletShapeFileData* data = this->GetInstance(fullname);
        data->d_reference++;
        return data;
    }
    void mmBulletShapeFileManager::Recycle(const std::string& fullname)
    {
        mmBulletShapeFileData* data = this->Get(fullname);
        if (NULL != data)
        {
            data->d_reference--;
            if (0 == data->d_reference)
            {
                this->Rmv(fullname);
            }
        }
    }

    mmBulletShapeFileData* mmBulletShapeFileManager::Add(const std::string& fullname)
    {
        mmBulletShapeFileData* data = this->Get(fullname);
        if (NULL == data)
        {
            data = new mmBulletShapeFileData;
            data->d_manager = this;
            data->d_asset = fullname;

            struct mmByteBuffer byte_buffer;
            mmByteBuffer_Init(&byte_buffer);

            mmFileContext_AcquireFileByteBuffer(this->d_file_context, fullname.c_str(), &byte_buffer);

            if (NULL != byte_buffer.buffer && 0 != byte_buffer.length)
            {
                int index_cs_0 = data->d_importer.getNumCollisionShapes();
                int index_rb_0 = data->d_importer.getNumRigidBodies();
                data->d_importer.loadFileFromMemory((char*)(byte_buffer.buffer + byte_buffer.offset), (int)byte_buffer.length);
                int index_cs_1 = data->d_importer.getNumCollisionShapes();
                int index_rb_1 = data->d_importer.getNumRigidBodies();

                if (index_cs_1 == index_cs_0 + 1)
                {
                    data->d_collision_shape = data->d_importer.getCollisionShapeByIndex(index_cs_0);
                }
                if (index_rb_1 == index_rb_0 + 1)
                {
                    btCollisionObject* r = data->d_importer.getRigidBodyByIndex(index_rb_0);
                    data->d_collision_shape = r->getCollisionShape();
                }
                this->d_collision_shape_map.insert(collision_shape_map_type::value_type(fullname, data));
            }
            else
            {
                struct mmLogger* gLogger = mmLogger_Instance();
                mmLogger_LogE(gLogger, "mm_bullet_shape_factory::%s %d the shape parameter fullname:%s is invalid.", __FUNCTION__, __LINE__, fullname.c_str());
            }

            mmFileContext_ReleaseFileByteBuffer(this->d_file_context, &byte_buffer);

            mmByteBuffer_Destroy(&byte_buffer);
        }
        return data;
    }
    mmBulletShapeFileData* mmBulletShapeFileManager::Get(const std::string& fullname)
    {
        mmBulletShapeFileData* data = NULL;
        collision_shape_map_type::iterator it = this->d_collision_shape_map.find(fullname);
        if (it != this->d_collision_shape_map.end())
        {
            data = it->second;
        }
        return data;
    }
    mmBulletShapeFileData* mmBulletShapeFileManager::GetInstance(const std::string& fullname)
    {
        mmBulletShapeFileData* data = this->Get(fullname);
        if (NULL == data)
        {
            data = this->Add(fullname);
        }
        return data;
    }
    void mmBulletShapeFileManager::Rmv(const std::string& fullname)
    {
        mmBulletShapeFileData* data = NULL;
        collision_shape_map_type::iterator it = this->d_collision_shape_map.find(fullname);
        if (it != this->d_collision_shape_map.end())
        {
            data = it->second;
            this->d_collision_shape_map.erase(it);
            delete data;
        }
    }
    void mmBulletShapeFileManager::Clear()
    {
        mmBulletShapeFileData* data = NULL;
        collision_shape_map_type::iterator it = this->d_collision_shape_map.begin();
        while (it != this->d_collision_shape_map.end())
        {
            data = it->second;
            this->d_collision_shape_map.erase(it++);
            delete data;
        }
    }
}

