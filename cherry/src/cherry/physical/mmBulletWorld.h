/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmBulletWorld_h__
#define __mmBulletWorld_h__

#include "LinearMath/btDefaultMotionState.h"
#include "LinearMath/btIDebugDraw.h"
#include "BulletCollision/CollisionDispatch/btDefaultCollisionConfiguration.h"
#include "BulletCollision/BroadphaseCollision/btDbvtBroadphase.h"
#include "BulletCollision/CollisionShapes/btCollisionShape.h"
#include "BulletCollision/CollisionShapes/btBoxShape.h"
#include "BulletCollision/CollisionShapes/btSphereShape.h"
#include "BulletDynamics/Dynamics/btDiscreteDynamicsWorld.h"
#include "BulletDynamics/ConstraintSolver/btSequentialImpulseConstraintSolver.h"
#include "BulletDynamics/Dynamics/btRigidBody.h"

#include "pthread.h"

#include "core/mmSpinlock.h"

#include "mmBulletDebugDrawer.h"

struct mmBulletWorld
{
    btDefaultCollisionConfiguration* d_collision_configuration;
    btCollisionDispatcher* d_collision_dispatcher;
    btBroadphaseInterface* d_broadphase;
    btConstraintSolver* d_constraint_solver;
    btDynamicsWorld* d_dynamics_world;
    mmBulletDebugDrawer* d_debug_draw;
    
    // check thread.
    pthread_t d_thread;
    mmAtomic_t d_locker;
    // default is 1/60.0f second.
    btScalar d_time_step;
    // whether active step at loop, 1 is run 0 is stop. default is 0.
    mmSInt8_t d_active;
    // mmThreadState_t, default is MM_TS_CLOSED(0)
    mmSInt8_t d_state;
};

extern void mmBulletWorld_Init(struct mmBulletWorld* p);
extern void mmBulletWorld_Destroy(struct mmBulletWorld* p);

// lock yourself.
extern int mmBulletWorld_StepSimulation(struct mmBulletWorld* p, btScalar timeStep,int maxSubSteps=1, btScalar fixedTimeStep=btScalar(1.)/btScalar(60.));

extern void mmBulletWorld_Loop(struct mmBulletWorld* p);

extern void mmBulletWorld_Lock(struct mmBulletWorld* p);
extern void mmBulletWorld_Unlock(struct mmBulletWorld* p);

extern void mmBulletWorld_Draw(struct mmBulletWorld* p);

// start thread.
extern void mmBulletWorld_Start(struct mmBulletWorld* p);
// interrupt thread.
extern void mmBulletWorld_Interrupt(struct mmBulletWorld* p);
// shutdown thread.
extern void mmBulletWorld_Shutdown(struct mmBulletWorld* p);
// join thread.
extern void mmBulletWorld_Join(struct mmBulletWorld* p);

#endif//__mmBulletWorld_h__