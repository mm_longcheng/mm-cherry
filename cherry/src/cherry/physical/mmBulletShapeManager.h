/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmBulletShapeManager_h__
#define __mmBulletShapeManager_h__

#include "dish/mmFileContext.h"

#include "mmBulletShapeFileManager.h"

namespace mm
{
    class mmBulletShapeManager;

    class mmBulletShapeData
    {
    public:
        mmBulletShapeManager* d_manager;
        std::string d_name;
        std::string d_asset;
        btCollisionShape* d_collision_shape;
        int d_create_type;
        mmUInt32_t d_reference;
    public:
        mmBulletShapeData();
        ~mmBulletShapeData();
    };

    class mmBulletShapeManager
    {
    public:
        enum mmCreateType
        {
            None,
            BulletFile,
            Box,
            CapsuleX,
            CapsuleY,
            CapsuleZ,
            ConeX,
            ConeY,
            ConeZ,
            CylinderX,
            CylinderY,
            CylinderZ,
            Sphere,
            StaticPlane,
        };
    public:
        typedef btCollisionShape* (mmBulletShapeManager::*ShapeCreateFunction)(mmBulletShapeData* shape_data, const std::string& parameter);
        typedef std::map<std::string, ShapeCreateFunction> shape_create_function_map_type;

        typedef std::map<std::string, mmBulletShapeData*> collision_shape_data_map_type;
    public:
        mmBulletShapeFileManager* d_shape_file_manager;
        shape_create_function_map_type d_create;
        collision_shape_data_map_type d_collision_shape_data_map;
    public:
        mmBulletShapeManager();
        virtual ~mmBulletShapeManager();
    public:
        void SetShapeFileManager(mmBulletShapeFileManager* manager);
        mmBulletShapeFileManager* GetShapeFileManager(void);
    public:
        mmBulletShapeData* Add(const std::string& fullname, const std::string& type, const std::string& parameter);
        mmBulletShapeData* Get(const std::string& fullname);
        mmBulletShapeData* GetInstance(const std::string& fullname, const std::string& type, const std::string& parameter);
        void Rmv(const std::string& fullname);
        void Clear();
    public:
        mmBulletShapeData* Produce(const std::string& type, const std::string& parameter);
        void Recycle(mmBulletShapeData* shape_data);
    public:
        // (std::string(filename), std::string(pathname))
        // "{%s,%s,}"
        // {file.bullet, physical/shape}
        btCollisionShape* ShapeCreate_BulletFile(mmBulletShapeData* shape_data, const std::string& parameter);
    public:
        // (btVector3(x, y, z))
        // {%f,%f,%f,}
        // {1.0, 1.0, 1.0,}
        btCollisionShape* ShapeCreate_Box(mmBulletShapeData* shape_data, const std::string& parameter);
        // (btScalar radius, btScalar height)
        // {%f,%f,}
        // {1.0, 1.0,}
        btCollisionShape* ShapeCreate_CapsuleX(mmBulletShapeData* shape_data, const std::string& parameter);
        // (btScalar radius, btScalar height)
        // {%f,%f,}
        // {1.0, 1.0,}
        btCollisionShape* ShapeCreate_CapsuleY(mmBulletShapeData* shape_data, const std::string& parameter);
        // (btScalar radius, btScalar height)
        // {%f,%f,}
        // {1.0, 1.0,}
        btCollisionShape* ShapeCreate_CapsuleZ(mmBulletShapeData* shape_data, const std::string& parameter);
        // (btScalar radius, btScalar height)
        // {%f,%f,}
        // {1.0, 1.0,}
        btCollisionShape* ShapeCreate_ConeX(mmBulletShapeData* shape_data, const std::string& parameter);
        // (btScalar radius, btScalar height)
        // {%f,%f,}
        // {1.0, 1.0,}
        btCollisionShape* ShapeCreate_ConeY(mmBulletShapeData* shape_data, const std::string& parameter);
        // (btScalar radius, btScalar height)
        // {%f,%f,}
        // {1.0, 1.0,}
        btCollisionShape* ShapeCreate_ConeZ(mmBulletShapeData* shape_data, const std::string& parameter);
        // (btVector3(x, y, z))
        // {%f,%f,%f,}
        // {1.0, 1.0, 1.0,}
        btCollisionShape* ShapeCreate_CylinderX(mmBulletShapeData* shape_data, const std::string& parameter);
        // (btVector3(x, y, z))
        // {%f,%f,%f,}
        // {1.0, 1.0, 1.0,}
        btCollisionShape* ShapeCreate_CylinderY(mmBulletShapeData* shape_data, const std::string& parameter);
        // (btVector3(x, y, z))
        // {%f,%f,%f,}
        // {1.0, 1.0, 1.0,}
        btCollisionShape* ShapeCreate_CylinderZ(mmBulletShapeData* shape_data, const std::string& parameter);
        // (btScalar radius)
        // {%f,}
        // {1.0,}
        btCollisionShape* ShapeCreate_Sphere(mmBulletShapeData* shape_data, const std::string& parameter);
        // (const btVector3& planeNormal, btScalar planeConstant)
        // {%f,%f,%f,%f,}
        // {1.0, 1.0, 1.0, 1.0,}
        btCollisionShape* ShapeCreate_StaticPlane(mmBulletShapeData* shape_data, const std::string& parameter);
    };
}
#endif//__mmBulletShapeManager_h__