/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmBulletDebugDrawer_h__
#define __mmBulletDebugDrawer_h__

#include "core/mmSpinlock.h"

#include "LinearMath/btIDebugDraw.h"

#include "OgreSimpleRenderable.h"
#include "OgreSceneNode.h"

#include "mmBulletDebugLines.h"

class mmBulletDebugDrawer : public btIDebugDraw
{
public:
    static btVector3 d_axis_x_color;//(0.7f,0.0f,0.0f)
    static btVector3 d_axis_y_color;//(0.0f,0.7f,0.0f)
    static btVector3 d_axis_z_color;//(0.0f,0.0f,0.7f)
public:
    Ogre::SceneNode* d_node_debug_draw;// weak ref.
    //
    mmBulletDebugLines d_axis_x;
    mmBulletDebugLines d_axis_y;
    mmBulletDebugLines d_axis_z;
    mmBulletDebugLines d_comm_draw;
    //
    int d_debug_mode;
    mmAtomic_t d_locker;
public:
    mmBulletDebugDrawer(void);
    virtual ~mmBulletDebugDrawer(void);
public:
    virtual void drawLine(const btVector3& from,const btVector3& to,const btVector3& color);
    virtual void drawLine(const btVector3& from,const btVector3& to, const btVector3& fromColor, const btVector3& toColor);
    virtual void drawContactPoint(const btVector3& PointOnB,const btVector3& normalOnB,btScalar distance,int lifeTime,const btVector3& color);
    virtual void reportErrorWarning(const char* warningString);
    virtual void draw3dText(const btVector3& location,const char* textString);
public:
    virtual void setDebugMode(int debugMode);
    virtual int getDebugMode() const;
public:
    void setSceneNode(Ogre::SceneNode* _node);
    Ogre::SceneNode* getSceneNode() const;
public:
    // make sure at gl thread.
    virtual void drawGeometry();
    // make sure at gl thread.
    virtual void clearGeometry();
};

#endif//__mmBulletDebugDrawer_h__