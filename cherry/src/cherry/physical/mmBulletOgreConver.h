/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmBulletOgreConver_h__
#define __mmBulletOgreConver_h__

#include "LinearMath/btVector3.h"
#include "LinearMath/btQuaternion.h"
#include "OgreVector3.h"
#include "OgreQuaternion.h"

extern btVector3 mmBulletOgreConver_OgreToBulletVector3(const Ogre::Vector3& V);
extern Ogre::Vector3 mmBulletOgreConver_BulletToOgreVector3(const btVector3 &V);

extern btQuaternion mmBulletOgreConver_OgreToBulletQuaternion(const Ogre::Quaternion& Q);
extern Ogre::Quaternion mmBulletOgreConver_BulletToOgreQuaternion(const btQuaternion& Q);

#endif//__mmBulletOgreConver_h__
