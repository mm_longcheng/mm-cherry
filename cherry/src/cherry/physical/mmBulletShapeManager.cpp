/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmBulletShapeManager.h"

#include "core/mmLogger.h"
#include "core/mmAlloc.h"

#include "dish/mmFilePath.h"

#include "BulletCollision/CollisionShapes/btBoxShape.h"
#include "BulletCollision/CollisionShapes/btCapsuleShape.h"
#include "BulletCollision/CollisionShapes/btConeShape.h"
#include "BulletCollision/CollisionShapes/btCylinderShape.h"
#include "BulletCollision/CollisionShapes/btSphereShape.h"
#include "BulletCollision/CollisionShapes/btStaticPlaneShape.h"

#include "Extras/Serialize/BulletWorldImporter/btBulletWorldImporter.h"

#include "mmBulletOgreConver.h"

namespace mm
{
    static void __static_BulletShapeFactory_ParameterNoneSymbol(const std::string& parameter, std::string& parameter_none_symbol)
    {
        parameter_none_symbol = parameter;
        size_t len = parameter_none_symbol.size();
        size_t i = 0;
        char* buffer = (char*)parameter_none_symbol.data();

        for (i = 0; i < len; ++i)
        {
            if ('(' == buffer[i] || ')' == buffer[i] || '{' == buffer[i] || '}' == buffer[i] || ',' == buffer[i])
            {
                buffer[i] = ' ';
            }
        }
    }
    static void __static_BulletShapeFactory_NameNoneEmpty(const std::string& name, std::string& name_none_empty)
    {
        name_none_empty = "";
        size_t len = name.size();
        size_t i = 0;
        const char* buffer = (const char*)name.data();

        for (i = 0; i < len; ++i)
        {
            if (' ' != buffer[i])
            {
                name_none_empty.push_back(buffer[i]);
            }
        }
    }

    mmBulletShapeData::mmBulletShapeData()
        : d_manager(NULL)
        , d_name("")
        , d_asset("")
        , d_collision_shape(NULL)
        , d_create_type(mmBulletShapeManager::None)
        , d_reference(0)
    {

    }
    mmBulletShapeData::~mmBulletShapeData()
    {

    }

    mmBulletShapeManager::mmBulletShapeManager()
        : d_shape_file_manager(NULL)
    {
        this->d_create["BulletFile"] = &mmBulletShapeManager::ShapeCreate_BulletFile;
        this->d_create["Box"] = &mmBulletShapeManager::ShapeCreate_Box;
        this->d_create["CapsuleX"] = &mmBulletShapeManager::ShapeCreate_CapsuleX;
        this->d_create["CapsuleY"] = &mmBulletShapeManager::ShapeCreate_CapsuleY;
        this->d_create["CapsuleZ"] = &mmBulletShapeManager::ShapeCreate_CapsuleZ;
        this->d_create["ConeX"] = &mmBulletShapeManager::ShapeCreate_ConeX;
        this->d_create["ConeY"] = &mmBulletShapeManager::ShapeCreate_ConeY;
        this->d_create["ConeZ"] = &mmBulletShapeManager::ShapeCreate_ConeZ;
        this->d_create["CylinderX"] = &mmBulletShapeManager::ShapeCreate_CylinderX;
        this->d_create["CylinderY"] = &mmBulletShapeManager::ShapeCreate_CylinderY;
        this->d_create["CylinderZ"] = &mmBulletShapeManager::ShapeCreate_CylinderZ;
        this->d_create["Sphere"] = &mmBulletShapeManager::ShapeCreate_Sphere;
        this->d_create["StaticPlane"] = &mmBulletShapeManager::ShapeCreate_StaticPlane;
    }
    mmBulletShapeManager::~mmBulletShapeManager()
    {
        this->Clear();
    }
    void mmBulletShapeManager::SetShapeFileManager(mmBulletShapeFileManager* manager)
    {
        this->d_shape_file_manager = manager;
    }
    mmBulletShapeFileManager* mmBulletShapeManager::GetShapeFileManager()
    {
        return this->d_shape_file_manager;
    }
    mmBulletShapeData* mmBulletShapeManager::Add(const std::string& fullname, const std::string& type, const std::string& parameter)
    {
        mmBulletShapeData* shape_data = this->Get(fullname);
        if (NULL == shape_data)
        {
            shape_data = new mmBulletShapeData;
            shape_data->d_manager = this;
            shape_data->d_name = fullname;

            shape_create_function_map_type::iterator it = this->d_create.find(type);
            if (it != this->d_create.end())
            {
                ShapeCreateFunction create_function = it->second;
                shape_data->d_collision_shape = (this->*create_function)(shape_data, parameter);
                this->d_collision_shape_data_map.insert(collision_shape_data_map_type::value_type(fullname, shape_data));
            }
            else
            {
                struct mmLogger* gLogger = mmLogger_Instance();
                mmLogger_LogE(gLogger, "mmBulletShapeManager::%s %d the shape type:%s create not find.", __FUNCTION__, __LINE__, type.c_str());
                this->d_collision_shape_data_map.insert(collision_shape_data_map_type::value_type(fullname, shape_data));
            }
        }
        return shape_data;
    }
    mmBulletShapeData* mmBulletShapeManager::Get(const std::string& fullname)
    {
        mmBulletShapeData* shape_data = NULL;
        collision_shape_data_map_type::iterator it = this->d_collision_shape_data_map.find(fullname);
        if (it != this->d_collision_shape_data_map.end())
        {
            shape_data = it->second;
        }
        return shape_data;
    }
    mmBulletShapeData* mmBulletShapeManager::GetInstance(const std::string& fullname, const std::string& type, const std::string& parameter)
    {
        mmBulletShapeData* shape_data = this->Get(fullname);
        if (NULL == shape_data)
        {
            shape_data = this->Add(fullname, type, parameter);
        }
        return shape_data;
    }
    void mmBulletShapeManager::Rmv(const std::string& fullname)
    {
        mmBulletShapeData* shape_data = NULL;
        collision_shape_data_map_type::iterator it = this->d_collision_shape_data_map.find(fullname);
        if (it != this->d_collision_shape_data_map.end())
        {
            shape_data = it->second;
            this->d_collision_shape_data_map.erase(it);
            delete shape_data;
        }
    }
    void mmBulletShapeManager::Clear()
    {
        mmBulletShapeData* shape_data = NULL;
        collision_shape_data_map_type::iterator it = this->d_collision_shape_data_map.begin();
        while (it != this->d_collision_shape_data_map.end())
        {
            shape_data = it->second;
            this->d_collision_shape_data_map.erase(it++);
            delete shape_data;
        }
    }

    mmBulletShapeData* mmBulletShapeManager::Produce(const std::string& type, const std::string& parameter)
    {
        mmBulletShapeData* shape_data = NULL;
        std::string shapename = type + ":" + parameter;
        std::string shapename_none_empty;

        __static_BulletShapeFactory_NameNoneEmpty(shapename, shapename_none_empty);

        shape_data = this->GetInstance(shapename_none_empty, type, parameter);
        shape_data->d_reference++;
        return shape_data;
    }
    void mmBulletShapeManager::Recycle(mmBulletShapeData* shape_data)
    {
        shape_data->d_reference--;
        if (BulletFile == shape_data->d_create_type)
        {
            if (0 == shape_data->d_reference)
            {
                this->d_shape_file_manager->Recycle(shape_data->d_asset);
                this->Rmv(shape_data->d_name);
            }
        }
        else
        {
            if (0 == shape_data->d_reference)
            {
                delete shape_data->d_collision_shape;
                shape_data->d_collision_shape = NULL;
                this->Rmv(shape_data->d_name);
            }
        }
    }
    // (std::string(filename), std::string(pathname))
    btCollisionShape* mmBulletShapeManager::ShapeCreate_BulletFile(mmBulletShapeData* shape_data, const std::string& parameter)
    {
        btCollisionShape* _collision_shape = NULL;
        if (false == parameter.empty())
        {
            std::string parameter_none_symbol;
            __static_BulletShapeFactory_ParameterNoneSymbol(parameter, parameter_none_symbol);

            struct mmString pathname;
            struct mmString filename;
            struct mmString fullname;

            mmString_Init(&pathname);
            mmString_Init(&filename);
            mmString_Init(&fullname);

            mmString_Resize(&pathname, parameter.size());
            mmString_Resize(&filename, parameter.size());

            mmSscanf(parameter_none_symbol.data(), "%s %s", (char*)mmString_CStr(&pathname), (char*)mmString_CStr(&filename));

            mmDirectoryHaveSuffix(&pathname, mmString_CStr(&pathname));

            mmString_Assign(&fullname, &pathname);
            mmString_Append(&fullname, &filename);

            mmBulletShapeFileData* data = this->d_shape_file_manager->Produce(mmString_CStr(&fullname));
            _collision_shape = data->d_collision_shape;
            shape_data->d_asset = mmString_CStr(&fullname);
            shape_data->d_collision_shape = _collision_shape;
            shape_data->d_create_type = mmBulletShapeManager::BulletFile;

            mmString_Destroy(&pathname);
            mmString_Destroy(&filename);
            mmString_Destroy(&fullname);
        }
        else
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            mmLogger_LogE(gLogger, "mmBulletShapeManager::%s %d the shape parameter is invalid.", __FUNCTION__, __LINE__);
        }
        return _collision_shape;
    }
    // (btVector3(x, y, z))
    btCollisionShape* mmBulletShapeManager::ShapeCreate_Box(mmBulletShapeData* shape_data, const std::string& parameter)
    {
        btCollisionShape* _collision_shape = NULL;
        if (false == parameter.empty())
        {
            btScalar data[3] = {0};
            mmSscanf(parameter.data(), "{%f,%f,%f,}", &data[0], &data[1], &data[2]);
            _collision_shape = new btBoxShape(btVector3(data[0], data[1], data[2]));
            shape_data->d_collision_shape = _collision_shape;
            shape_data->d_create_type = mmBulletShapeManager::Box;
        }
        else
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            mmLogger_LogE(gLogger, "mmBulletShapeManager::%s %d the shape parameter is invalid.", __FUNCTION__, __LINE__);
        }
        return _collision_shape;
    }
    // (btScalar radius,btScalar height)
    btCollisionShape* mmBulletShapeManager::ShapeCreate_CapsuleX(mmBulletShapeData* shape_data, const std::string& parameter)
    {
        btCollisionShape* _collision_shape = NULL;
        if (false == parameter.empty())
        {
            btScalar data[2] = { 0 };
            mmSscanf(parameter.data(), "{%f,%f,}", &data[0], &data[1]);
            _collision_shape = new btCapsuleShapeX(data[0], data[1]);
            shape_data->d_collision_shape = _collision_shape;
            shape_data->d_create_type = mmBulletShapeManager::CapsuleX;
        }
        else
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            mmLogger_LogE(gLogger, "mmBulletShapeManager::%s %d the shape parameter is invalid.", __FUNCTION__, __LINE__);
        }
        return _collision_shape;
    }
    // (btScalar radius,btScalar height)
    btCollisionShape* mmBulletShapeManager::ShapeCreate_CapsuleY(mmBulletShapeData* shape_data, const std::string& parameter)
    {
        btCollisionShape* _collision_shape = NULL;
        if (false == parameter.empty())
        {
            btScalar data[2] = { 0 };
            mmSscanf(parameter.data(), "{%f,%f,}", &data[0], &data[1]);
            _collision_shape = new btCapsuleShape(data[0], data[1]);
            shape_data->d_collision_shape = _collision_shape;
            shape_data->d_create_type = mmBulletShapeManager::CapsuleY;
        }
        else
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            mmLogger_LogE(gLogger, "mmBulletShapeManager::%s %d the shape parameter is invalid.", __FUNCTION__, __LINE__);
        }
        return _collision_shape;
    }
    // (btScalar radius,btScalar height)
    btCollisionShape* mmBulletShapeManager::ShapeCreate_CapsuleZ(mmBulletShapeData* shape_data, const std::string& parameter)
    {
        btCollisionShape* _collision_shape = NULL;
        if (false == parameter.empty())
        {
            btScalar data[2] = { 0 };
            mmSscanf(parameter.data(), "{%f,%f,}", &data[0], &data[1]);
            _collision_shape = new btCapsuleShapeZ(data[0], data[1]);
            shape_data->d_collision_shape = _collision_shape;
            shape_data->d_create_type = mmBulletShapeManager::CapsuleZ;
        }
        else
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            mmLogger_LogE(gLogger, "mmBulletShapeManager::%s %d the shape parameter is invalid.", __FUNCTION__, __LINE__);
        }
        return _collision_shape;
    }
    // (btScalar radius,btScalar height)
    btCollisionShape* mmBulletShapeManager::ShapeCreate_ConeX(mmBulletShapeData* shape_data, const std::string& parameter)
    {
        btCollisionShape* _collision_shape = NULL;
        if (false == parameter.empty())
        {
            btScalar data[2] = { 0 };
            mmSscanf(parameter.data(), "{%f,%f,}", &data[0], &data[1]);
            _collision_shape = new btConeShapeX(data[0], data[1]);
            shape_data->d_collision_shape = _collision_shape;
            shape_data->d_create_type = mmBulletShapeManager::ConeX;
        }
        else
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            mmLogger_LogE(gLogger, "mmBulletShapeManager::%s %d the shape parameter is invalid.", __FUNCTION__, __LINE__);
        }
        return _collision_shape;
    }
    // (btScalar radius,btScalar height)
    btCollisionShape* mmBulletShapeManager::ShapeCreate_ConeY(mmBulletShapeData* shape_data, const std::string& parameter)
    {
        btCollisionShape* _collision_shape = NULL;
        if (false == parameter.empty())
        {
            btScalar data[2] = { 0 };
            mmSscanf(parameter.data(), "{%f,%f,}", &data[0], &data[1]);
            _collision_shape = new btConeShape(data[0], data[1]);
            shape_data->d_collision_shape = _collision_shape;
            shape_data->d_create_type = mmBulletShapeManager::ConeY;
        }
        else
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            mmLogger_LogE(gLogger, "mmBulletShapeManager::%s %d the shape parameter is invalid.", __FUNCTION__, __LINE__);
        }
        return _collision_shape;
    }
    // (btScalar radius,btScalar height)
    btCollisionShape* mmBulletShapeManager::ShapeCreate_ConeZ(mmBulletShapeData* shape_data, const std::string& parameter)
    {
        btCollisionShape* _collision_shape = NULL;
        if (false == parameter.empty())
        {
            btScalar data[2] = { 0 };
            mmSscanf(parameter.data(), "{%f,%f,}", &data[0], &data[1]);
            _collision_shape = new btConeShapeZ(data[0], data[1]);
            shape_data->d_collision_shape = _collision_shape;
            shape_data->d_create_type = mmBulletShapeManager::ConeZ;
        }
        else
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            mmLogger_LogE(gLogger, "mmBulletShapeManager::%s %d the shape parameter is invalid.", __FUNCTION__, __LINE__);
        }
        return _collision_shape;
    }
    // (btVector3(x, y, z))
    btCollisionShape* mmBulletShapeManager::ShapeCreate_CylinderX(mmBulletShapeData* shape_data, const std::string& parameter)
    {
        btCollisionShape* _collision_shape = NULL;
        if (false == parameter.empty())
        {
            btScalar data[3] = { 0 };
            mmSscanf(parameter.data(), "{%f,%f,%f,}", &data[0], &data[1], &data[2]);
            _collision_shape = new btCylinderShapeX(btVector3(data[0], data[1], data[2]));
            shape_data->d_collision_shape = _collision_shape;
            shape_data->d_create_type = mmBulletShapeManager::CylinderX;
        }
        else
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            mmLogger_LogE(gLogger, "mmBulletShapeManager::%s %d the shape parameter is invalid.", __FUNCTION__, __LINE__);
        }
        return _collision_shape;
    }
    // (btVector3(x, y, z))
    btCollisionShape* mmBulletShapeManager::ShapeCreate_CylinderY(mmBulletShapeData* shape_data, const std::string& parameter)
    {
        btCollisionShape* _collision_shape = NULL;
        if (false == parameter.empty())
        {
            btScalar data[3] = { 0 };
            mmSscanf(parameter.data(), "{%f,%f,%f,}", &data[0], &data[1], &data[2]);
            _collision_shape = new btCylinderShape(btVector3(data[0], data[1], data[2]));
            shape_data->d_collision_shape = _collision_shape;
            shape_data->d_create_type = mmBulletShapeManager::CylinderY;
        }
        else
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            mmLogger_LogE(gLogger, "mmBulletShapeManager::%s %d the shape parameter is invalid.", __FUNCTION__, __LINE__);
        }
        return _collision_shape;
    }
    // (btVector3(x, y, z))
    btCollisionShape* mmBulletShapeManager::ShapeCreate_CylinderZ(mmBulletShapeData* shape_data, const std::string& parameter)
    {
        btCollisionShape* _collision_shape = NULL;
        if (false == parameter.empty())
        {
            btScalar data[3] = { 0 };
            mmSscanf(parameter.data(), "{%f,%f,%f,}", &data[0], &data[1], &data[2]);
            _collision_shape = new btCylinderShapeZ(btVector3(data[0], data[1], data[2]));
            shape_data->d_collision_shape = _collision_shape;
            shape_data->d_create_type = mmBulletShapeManager::CylinderZ;
        }
        else
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            mmLogger_LogE(gLogger, "mmBulletShapeManager::%s %d the shape parameter is invalid.", __FUNCTION__, __LINE__);
        }
        return _collision_shape;
    }
    // (btScalar radius)
    btCollisionShape* mmBulletShapeManager::ShapeCreate_Sphere(mmBulletShapeData* shape_data, const std::string& parameter)
    {
        btCollisionShape* _collision_shape = NULL;
        if (false == parameter.empty())
        {
            btScalar data[1] = { 0 };
            mmSscanf(parameter.data(), "{%f,}", &data[0]);
            _collision_shape = new btSphereShape(data[0]);
            shape_data->d_collision_shape = _collision_shape;
            shape_data->d_create_type = mmBulletShapeManager::Sphere;
        }
        else
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            mmLogger_LogE(gLogger, "mmBulletShapeManager::%s %d the shape parameter is invalid.", __FUNCTION__, __LINE__);
        }
        return _collision_shape;
    }
    // (const btVector3& planeNormal,btScalar planeConstant)
    btCollisionShape* mmBulletShapeManager::ShapeCreate_StaticPlane(mmBulletShapeData* shape_data, const std::string& parameter)
    {
        btCollisionShape* _collision_shape = NULL;
        if (false == parameter.empty())
        {
            btScalar data[4] = { 0 };
            mmSscanf(parameter.data(), "{%f,%f,%f,%f,}", &data[0], &data[1], &data[2], &data[3]);
            _collision_shape = new btStaticPlaneShape(btVector3(data[0], data[1], data[2]), data[3]);
            shape_data->d_collision_shape = _collision_shape;
            shape_data->d_create_type = mmBulletShapeManager::StaticPlane;
        }
        else
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            mmLogger_LogE(gLogger, "mmBulletShapeManager::%s %d the shape parameter is invalid.", __FUNCTION__, __LINE__);
        }
        return _collision_shape;
    }   
}

