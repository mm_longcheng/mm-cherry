/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmBulletDebugLines_h__
#define __mmBulletDebugLines_h__

#include "LinearMath/btIDebugDraw.h"

#include "OgreSimpleRenderable.h"

class mmBulletDebugLines : public Ogre::SimpleRenderable
{
public:
    typedef std::vector<Ogre::Vector3> Vector3Array;
public:
    Ogre::HardwareVertexBufferSharedPtr d_vertex_buffer;
    Vector3Array d_points;
public:
    mmBulletDebugLines();
    virtual ~mmBulletDebugLines();
public:
    Ogre::Real getSquaredViewDepth(const Ogre::Camera *cam) const;
    Ogre::Real getBoundingRadius(void) const;
public:
    // make sure at gl thread.
    virtual void drawGeometry();
public:
    void clear();
public:
    void addLine(const Ogre::Vector3 &start,const Ogre::Vector3 &end);
    void addLine(Ogre::Real start_x, Ogre::Real start_y, Ogre::Real start_z, 
        Ogre::Real end_x, Ogre::Real end_y, Ogre::Real end_z);
    void addPoint(const Ogre::Vector3 &pt);
    void addPoint(Ogre::Real x, Ogre::Real y, Ogre::Real z);
};

#endif//__mmBulletDebugLines_h__