/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmBulletShapeFileManager_h__
#define __mmBulletShapeFileManager_h__

#include "dish/mmFileContext.h"

#include "BulletCollision/CollisionShapes/btCollisionShape.h"
#include "Extras/Serialize/BulletWorldImporter/btBulletWorldImporter.h"

#include <string>
#include <map>

namespace mm
{
    class mmBulletShapeFileManager;

    class mmBulletShapeFileData
    {
    public:
        mmBulletShapeFileManager* d_manager;
        std::string d_asset;
        btBulletWorldImporter d_importer;
        btCollisionShape* d_collision_shape;
        mmUInt32_t d_reference;
    public:
        mmBulletShapeFileData();
        ~mmBulletShapeFileData();
    };

    // bullet file must have only one btCollisionShape.
    class mmBulletShapeFileManager
    {
    public:
        typedef std::map<std::string, mmBulletShapeFileData*> collision_shape_map_type;
    public:
        struct mmFileContext* d_file_context;// weak ref.
        collision_shape_map_type d_collision_shape_map;
    public:
        mmBulletShapeFileManager();
        virtual ~mmBulletShapeFileManager();
    public:
        void SetFileContext(struct mmFileContext* file_context);
        struct mmFileContext* GetFileContext();
    public:
        mmBulletShapeFileData* Produce(const std::string& fullname);
        void Recycle(const std::string& fullname);
    public:
        mmBulletShapeFileData* Add(const std::string& fullname);
        mmBulletShapeFileData* Get(const std::string& fullname);
        mmBulletShapeFileData* GetInstance(const std::string& fullname);
        void Rmv(const std::string& fullname);
        void Clear();
    };
};
#endif//__mmBulletShapeFileManager_h__