/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmExplorerImage_h__
#define __mmExplorerImage_h__

#include "core/mmCore.h"

#include "container/mmRbtreeString.h"
#include "container/mmRbtreeU32.h"
#include "container/mmRbtsetVpt.h"

#include "explorer/mmExplorerExport.h"

#include "core/mmPrefix.h"

struct mmExplorerItem;

typedef mmUInt32_t(*mmExplorerImageFunc)(struct mmExplorerItem* item, struct mmString* real_type);

struct mmExplorerImage
{
    struct mmRbtreeStringString hSSImageMapper;
    struct mmRbtreeU32String hISImageMapper;
    struct mmRbtreeStringVpt hSSFuncMapper;

    struct mmString hDefaultSSImage;
    struct mmString hDefaultISImage;
};
MM_EXPORT_EXPLORER void mmExplorerImage_Init(struct mmExplorerImage* p);
MM_EXPORT_EXPLORER void mmExplorerImage_Destroy(struct mmExplorerImage* p);

MM_EXPORT_EXPLORER void mmExplorerImage_SetDefaultSSImage(struct mmExplorerImage* p, const char* image);
MM_EXPORT_EXPLORER void mmExplorerImage_SetDefaultISImage(struct mmExplorerImage* p, const char* image);

// ss
MM_EXPORT_EXPLORER void mmExplorerImage_AddSSImage(struct mmExplorerImage* p, const char* type, const char* image);
MM_EXPORT_EXPLORER void mmExplorerImage_RmvSSImage(struct mmExplorerImage* p, const char* type);
MM_EXPORT_EXPLORER void mmExplorerImage_GetSSImage(struct mmExplorerImage* p, const char* type, struct mmString* image);
MM_EXPORT_EXPLORER void mmExplorerImage_ClearSSImage(struct mmExplorerImage* p);

// is
MM_EXPORT_EXPLORER void mmExplorerImage_AddISImage(struct mmExplorerImage* p, mmUInt32_t type, const char* image);
MM_EXPORT_EXPLORER void mmExplorerImage_RmvISImage(struct mmExplorerImage* p, mmUInt32_t type);
MM_EXPORT_EXPLORER void mmExplorerImage_GetISImage(struct mmExplorerImage* p, mmUInt32_t type, struct mmString* image);
MM_EXPORT_EXPLORER void mmExplorerImage_ClearISImage(struct mmExplorerImage* p);

MM_EXPORT_EXPLORER void mmExplorerImage_AddSSFunc(struct mmExplorerImage* p, const char* type, mmExplorerImageFunc func);
MM_EXPORT_EXPLORER void mmExplorerImage_RmvSSFunc(struct mmExplorerImage* p, const char* type, mmExplorerImageFunc func);
MM_EXPORT_EXPLORER void mmExplorerImage_ClearSSFuncByType(struct mmExplorerImage* p, const char* type);
MM_EXPORT_EXPLORER void mmExplorerImage_ClearSSFunc(struct mmExplorerImage* p);

MM_EXPORT_EXPLORER void mmExplorerImage_ItemType(struct mmExplorerImage* p, struct mmExplorerItem* item);

MM_EXPORT_EXPLORER void mmExplorerImage_RegisterDefaultImageMapper(struct mmExplorerImage* p);

#include "core/mmSuffix.h"

#endif//__mmExplorerImage_h__