/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmExplorerItemEmu.h"
#include "parse/mmParseINI.h"
#include "dish/mmFileContext.h"
#include "dish/mmFilePath.h"

MM_EXPORT_EXPLORER const char* MM_EXPLORERITEMEMUINFORMATION_FILENAME = "information.txt";

MM_EXPORT_EXPLORER void mmExplorerItemEmuInformation_Init(struct mmExplorerItemEmuInformation* p)
{
    mmString_Init(&p->encoding);
    mmString_Init(&p->assets_name);
    mmString_Init(&p->source_name);
    mmString_Init(&p->author);
    mmString_Init(&p->source_version);
    mmString_Init(&p->client_version);
    mmString_Init(&p->introduction);
    mmString_Init(&p->player_number);
    mmString_Init(&p->introduction_string);

    mmString_Assigns(&p->encoding, "utf-8");
    mmString_Assigns(&p->assets_name, "");
    mmString_Assigns(&p->source_name, "");
    mmString_Assigns(&p->author, "unknown");
    mmString_Assigns(&p->source_version, "v1.0.0");
    mmString_Assigns(&p->client_version, "v1.0.0");
    mmString_Assigns(&p->introduction, "");
    mmString_Assigns(&p->player_number, "2");
    mmString_Assigns(&p->introduction_string, "Not introduction.");
}
MM_EXPORT_EXPLORER void mmExplorerItemEmuInformation_Destroy(struct mmExplorerItemEmuInformation* p)
{
    mmString_Destroy(&p->encoding);
    mmString_Destroy(&p->assets_name);
    mmString_Destroy(&p->source_name);
    mmString_Destroy(&p->author);
    mmString_Destroy(&p->source_version);
    mmString_Destroy(&p->client_version);
    mmString_Destroy(&p->introduction);
    mmString_Destroy(&p->player_number);
    mmString_Destroy(&p->introduction_string);
}
MM_EXPORT_EXPLORER void mmExplorerItemEmuInformation_Reset(struct mmExplorerItemEmuInformation* p)
{
    mmString_Assigns(&p->encoding, "utf-8");
    mmString_Assigns(&p->assets_name, "");
    mmString_Assigns(&p->source_name, "");
    mmString_Assigns(&p->author, "unknown");
    mmString_Assigns(&p->source_version, "v1.0.0");
    mmString_Assigns(&p->client_version, "v1.0.0");
    mmString_Assigns(&p->introduction, "");
    mmString_Assigns(&p->player_number, "2");
    mmString_Assigns(&p->introduction_string, "Not introduction.");
}

static void __static_mmVectorValue_FileAssetsInfoInitialization(struct mmExplorerItemEmu* p, struct mmVectorValue* _vector)
{
    struct mmVectorValueEventAllocator hEventAllocator;

    hEventAllocator.Produce = &mmVectorValue_FileAssetsInfoEventProduce;
    hEventAllocator.Recycle = &mmVectorValue_FileAssetsInfoEventRecycle;
    hEventAllocator.obj = p;
    mmVectorValue_SetElemSize(_vector, sizeof(struct mmFileAssetsInfo));
    mmVectorValue_SetEventAllocator(_vector, &hEventAllocator);
}

MM_EXPORT_EXPLORER void mmExplorerItemEmu_Init(struct mmExplorerItemEmu* p)
{
    p->file_context = NULL;
    mmExplorerItemEmuInformation_Init(&p->information);
    p->file_assets_source = NULL;
    mmString_Init(&p->fullname);
    mmString_Init(&p->pathname);
    mmString_Init(&p->basename);
    mmVectorValue_Init(&p->vector_cheatcode);
    mmVectorValue_Init(&p->vector_genie);
    mmVectorValue_Init(&p->vector_ips);
    mmVectorValue_Init(&p->vector_snapshot);
    mmVectorValue_Init(&p->vector_tone);

    mmString_Assigns(&p->fullname, "");
    mmString_Assigns(&p->pathname, "");
    mmString_Assigns(&p->basename, "");

    __static_mmVectorValue_FileAssetsInfoInitialization(p, &p->vector_cheatcode);
    __static_mmVectorValue_FileAssetsInfoInitialization(p, &p->vector_genie);
    __static_mmVectorValue_FileAssetsInfoInitialization(p, &p->vector_ips);
    __static_mmVectorValue_FileAssetsInfoInitialization(p, &p->vector_snapshot);
    __static_mmVectorValue_FileAssetsInfoInitialization(p, &p->vector_tone);
}
MM_EXPORT_EXPLORER void mmExplorerItemEmu_Destroy(struct mmExplorerItemEmu* p)
{
    p->file_context = NULL;
    mmExplorerItemEmuInformation_Destroy(&p->information);
    p->file_assets_source = NULL;
    mmString_Destroy(&p->fullname);
    mmString_Destroy(&p->pathname);
    mmString_Destroy(&p->basename);
    mmVectorValue_Destroy(&p->vector_cheatcode);
    mmVectorValue_Destroy(&p->vector_genie);
    mmVectorValue_Destroy(&p->vector_ips);
    mmVectorValue_Destroy(&p->vector_snapshot);
    mmVectorValue_Destroy(&p->vector_tone);
}

MM_EXPORT_EXPLORER void mmExplorerItemEmu_SetFileContext(struct mmExplorerItemEmu* p, struct mmFileContext* _file_context)
{
    p->file_context = _file_context;
}
MM_EXPORT_EXPLORER void mmExplorerItemEmu_SetPath(struct mmExplorerItemEmu* p, const char* path, const char* base)
{
    mmString_Assigns(&p->pathname, path);
    mmString_Assigns(&p->basename, base);
    mmString_Assigns(&p->fullname, path);
    mmString_Appends(&p->fullname, "/");
    mmString_Appends(&p->fullname, base);
}

MM_EXPORT_EXPLORER void mmExplorerItemEmu_UpdateValue(struct mmExplorerItemEmu* p)
{
    mmExplorerItemEmu_OnBeforeTerminate(p);
    mmExplorerItemEmu_OnFinishLaunching(p);
}

MM_EXPORT_EXPLORER mmBool_t mmExplorerItemEmu_IsValid(struct mmExplorerItemEmu* p)
{
    return 
        0 != mmString_CompareCStr(&p->information.assets_name, "") && 
        0 != mmString_CompareCStr(&p->information.source_name, "");
}

MM_EXPORT_EXPLORER void mmExplorerItemEmu_OnFinishLaunching(struct mmExplorerItemEmu* p)
{
    struct mmByteBuffer byte_buffer;

    const char* pFullName = mmString_CStr(&p->fullname);
    p->file_assets_source = mmFileContext_AddAssetsSource(p->file_context, pFullName, pFullName, ".");

    mmExplorerItemEmuInformation_Reset(&p->information);

    mmString_Assigns(&p->information.assets_name, "");
    mmString_Assigns(&p->information.source_name, "");

    if (0 != mmFileAssetsSource_IsDataExists(p->file_assets_source, MM_EXPLORERITEMEMUINFORMATION_FILENAME))
    {
        const char* pValue = "";
        mmUInt8_t* buffer = NULL;
        size_t length = 0;
        struct mmParseINI hINI;

        mmFileAssetsSource_AcquireFileByteBuffer(p->file_assets_source, MM_EXPLORERITEMEMUINFORMATION_FILENAME, &byte_buffer);

        length = byte_buffer.length;
        buffer = (byte_buffer.buffer + byte_buffer.offset);

        mmParseINI_Init(&hINI);

        mmParseINI_AnalysisBuffer(&hINI, buffer, length);

        pValue = mmParseINI_GetValue(&hINI, "encoding");
        mmString_Assigns(&p->information.encoding, pValue);

        pValue = mmParseINI_GetValue(&hINI, "assets_name");
        mmString_Assigns(&p->information.assets_name, pValue);

        pValue = mmParseINI_GetValue(&hINI, "source_name");
        mmString_Assigns(&p->information.source_name, pValue);

        pValue = mmParseINI_GetValue(&hINI, "author");
        mmString_Assigns(&p->information.author, pValue);

        pValue = mmParseINI_GetValue(&hINI, "source_version");
        mmString_Assigns(&p->information.source_version, pValue);

        pValue = mmParseINI_GetValue(&hINI, "client_version");
        mmString_Assigns(&p->information.client_version, pValue);

        pValue = mmParseINI_GetValue(&hINI, "introduction");
        mmString_Assigns(&p->information.introduction, pValue);

        pValue = mmParseINI_GetValue(&hINI, "player_number");
        mmString_Assigns(&p->information.player_number, pValue);

        mmParseINI_Destroy(&hINI);

        if (mmString_Empty(&p->information.source_name))
        {
            struct mmString qualified_name;
            struct mmString basename;
            struct mmString suffixname;

            mmString_Init(&qualified_name);
            mmString_Init(&basename);
            mmString_Init(&suffixname);

            mmString_Assign(&qualified_name, &p->information.assets_name);

            mmPathSplitSuffixName(&qualified_name, &basename, &suffixname);

            mmString_Assign(&p->information.source_name, &basename);

            mmString_Destroy(&qualified_name);
            mmString_Destroy(&basename);
            mmString_Destroy(&suffixname);
        }

        if (0 != mmFileAssetsSource_IsDataExists(p->file_assets_source, mmString_CStr(&p->information.introduction)))
        {
            const char* buff = NULL;
            size_t size = 0;
            struct mmByteBuffer hByteBufferIntroduction;

            mmFileAssetsSource_AcquireFileByteBuffer(p->file_assets_source, mmString_CStr(&p->information.introduction), &hByteBufferIntroduction);

            buff = (const char*)(hByteBufferIntroduction.buffer + hByteBufferIntroduction.offset);
            size = hByteBufferIntroduction.length;
            mmString_Assignsn(&p->information.introduction_string, buff, size);

            mmFileAssetsSource_ReleaseFileByteBuffer(p->file_assets_source, &hByteBufferIntroduction);
        }

        mmFileAssetsSource_ReleaseFileByteBuffer(p->file_assets_source, &byte_buffer);
    }
    else
    {
        // the first ".nes" file
        struct mmRbtreeStringVpt rbtree;

        mmRbtreeStringVpt_Init(&rbtree);

        mmFileAssetsSource_AcquireFindFiles(p->file_assets_source, ".", "*.nes", 1, 0, 1, &rbtree);
        mmFileAssetsSource_AcquireFindFiles(p->file_assets_source, ".", "*.NES", 1, 0, 1, &rbtree);
        mmFileAssetsSource_AcquireFindFiles(p->file_assets_source, ".", "*.Nes", 1, 0, 1, &rbtree);

        mmFileAssetsSource_AcquireFindFiles(p->file_assets_source, ".", "*.fds", 1, 0, 1, &rbtree);
        mmFileAssetsSource_AcquireFindFiles(p->file_assets_source, ".", "*.FDS", 1, 0, 1, &rbtree);
        mmFileAssetsSource_AcquireFindFiles(p->file_assets_source, ".", "*.Fds", 1, 0, 1, &rbtree);

        mmFileAssetsSource_AcquireFindFiles(p->file_assets_source, ".", "*.nsf", 1, 0, 1, &rbtree);
        mmFileAssetsSource_AcquireFindFiles(p->file_assets_source, ".", "*.NSF", 1, 0, 1, &rbtree);
        mmFileAssetsSource_AcquireFindFiles(p->file_assets_source, ".", "*.Nsf", 1, 0, 1, &rbtree);

        {
            struct mmRbNode* n = NULL;
            struct mmRbtreeStringVptIterator* it = NULL;
            struct mmFileAssetsInfo* e = NULL;
            n = mmRb_First(&rbtree.rbt);
            if (NULL != n)
            {
                struct mmString qualified_name;
                struct mmString basename;
                struct mmString suffixname;

                it = (struct mmRbtreeStringVptIterator*)mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
                e = (struct mmFileAssetsInfo*)(it->v);

                mmString_Init(&qualified_name);
                mmString_Init(&basename);
                mmString_Init(&suffixname);

                mmString_Assign(&qualified_name, &e->filename);

                mmPathSplitSuffixName(&qualified_name, &basename, &suffixname);

                mmString_Assign(&p->information.assets_name, &e->filename);
                mmString_Assign(&p->information.source_name, &basename);

                mmString_Destroy(&qualified_name);
                mmString_Destroy(&basename);
                mmString_Destroy(&suffixname);
            }
        }

        mmFileAssetsSource_ReleaseFindFiles(p->file_assets_source, &rbtree);

        mmRbtreeStringVpt_Destroy(&rbtree);
    }

    if (mmExplorerItemEmu_IsValid(p))
    {
        mmExplorerItemEmu_SearchFile(p, ".", "cheatcode/*.vct", &p->vector_cheatcode);
        mmExplorerItemEmu_SearchFile(p, ".", "genie/*.gen", &p->vector_genie);
        mmExplorerItemEmu_SearchFile(p, ".", "ips/*.ips", &p->vector_ips);
        mmExplorerItemEmu_SearchFile(p, ".", "snapshot/*.png", &p->vector_snapshot);
        mmExplorerItemEmu_SearchFile(p, ".", "tone/*.vtd", &p->vector_tone);
    }
}
MM_EXPORT_EXPLORER void mmExplorerItemEmu_OnBeforeTerminate(struct mmExplorerItemEmu* p)
{
    mmFileContext_RmvAssetsSource(p->file_context, mmString_CStr(&p->fullname));
    p->file_assets_source = NULL;
}

MM_EXPORT_EXPLORER void mmExplorerItemEmu_SearchFile(struct mmExplorerItemEmu* p, const char* directory, const char* pattern, struct mmVectorValue* _vector)
{
    // the first "directory/pattern" file
    struct mmRbtreeStringVpt rbtree;

    size_t index = 0;
    struct mmFileAssetsInfo* info = NULL;

    // clear first.
    mmVectorValue_Clear(_vector);

    mmRbtreeStringVpt_Init(&rbtree);

    mmFileAssetsSource_AcquireFindFiles(p->file_assets_source, directory, pattern, 0, 0, 1, &rbtree);

    mmVectorValue_AlignedMemory(_vector, rbtree.size);

    {
        struct mmRbNode* n = NULL;
        struct mmRbtreeStringVptIterator* it = NULL;
        struct mmFileAssetsInfo* e = NULL;
        n = mmRb_First(&rbtree.rbt);
        while (NULL != n)
        {
            it = (struct mmRbtreeStringVptIterator*)mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
            n = mmRb_Next(n);
            e = (struct mmFileAssetsInfo*)(it->v);
            //
            info = (struct mmFileAssetsInfo*)mmVectorValue_GetIndexReference(_vector, index++);

            // copy.
            info->file_assets = e->file_assets;
            mmString_Assign(&info->filename, &e->filename);
            mmString_Assign(&info->basename, &e->basename);
            mmString_Assign(&info->pathname, &e->pathname);
            info->csize = e->csize;
            info->dsize = e->dsize;
        }
    }

    mmFileAssetsSource_ReleaseFindFiles(p->file_assets_source, &rbtree);

    mmRbtreeStringVpt_Destroy(&rbtree);
}
