/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmExplorerItemZip_h__
#define __mmExplorerItemZip_h__

#include "core/mmCore.h"
#include "core/mmString.h"

#include "container/mmVectorValue.h"

#include "dish/mmFileContext.h"

#include "explorer/mmExplorerExport.h"

#include "core/mmPrefix.h"


struct mmExplorerItemZip
{
    struct mmFileAssetsSource file_assets_source;

    struct mmString fullname;
    struct mmString pathname;
    struct mmString basename;

    struct mmVectorValue vector_item;

    mmStat_t s;
    mmUInt32_t mode;
};
MM_EXPORT_EXPLORER void mmExplorerItemZip_Init(struct mmExplorerItemZip* p);
MM_EXPORT_EXPLORER void mmExplorerItemZip_Destroy(struct mmExplorerItemZip* p);

MM_EXPORT_EXPLORER void mmExplorerItemZip_SetPath(struct mmExplorerItemZip* p, const char* path, const char* base);

MM_EXPORT_EXPLORER void mmExplorerItemZip_Refresh(struct mmExplorerItemZip* p);

MM_EXPORT_EXPLORER void mmExplorerItemZip_OnFinishLaunching(struct mmExplorerItemZip* p);
MM_EXPORT_EXPLORER void mmExplorerItemZip_OnBeforeTerminate(struct mmExplorerItemZip* p);

#include "core/mmSuffix.h"

#endif//__mmExplorerItemZip_h__