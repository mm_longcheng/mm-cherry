﻿#ifndef __mmExplorerExport_h__
#define __mmExplorerExport_h__

#include "core/mmPlatform.h"

// windwos
#if MM_PLATFORM == MM_PLATFORM_WIN32

#  ifdef MM_STATIC_EXPLORER
#    define MM_EXPORT_EXPLORER
#    define MM_IMPORT_EXPLORER
#  else
#    ifndef MM_EXPORT_EXPLORER
#      ifdef MM_SHARED_EXPLORER
/* We are building this library */
#        define MM_EXPORT_EXPLORER __declspec(dllexport)
#      else
/* We are using this library */
#        define MM_EXPORT_EXPLORER __declspec(dllimport)
#      endif
#    endif

#    ifndef MM_PRIVATE_EXPLORER
#      define MM_PRIVATE_EXPLORER 
#    endif
#  endif

#  ifndef MM_DEPRECATED_EXPLORER
#    define MM_DEPRECATED_EXPLORER __declspec(deprecated)
#  endif

#  ifndef MM_DEPRECATED_EXPORT_EXPLORER
#    define MM_DEPRECATED_EXPORT_EXPLORER MM_EXPORT_EXPLORER MM_DEPRECATED_EXPLORER
#  endif

#  ifndef MM_DEPRECATED_PRIVATE_EXPLORER
#    define MM_DEPRECATED_PRIVATE_EXPLORER MM_PRIVATE_EXPLORER MM_DEPRECATED_EXPLORER
#  endif

#else// unix

// Add -fvisibility=hidden to compiler options. With -fvisibility=hidden, you are telling
// GCC that every declaration not explicitly marked with a visibility attribute (MM_EXPORT)
// has a hidden visibility (like in windows).
#  ifdef MM_STATIC_EXPLORER
#    define MM_EXPORT_EXPLORER
#    define MM_IMPORT_EXPLORER
#  else
#    ifndef MM_EXPORT_EXPLORER
#      ifdef MM_SHARED_EXPLORER
/* We are building this library */
#        define MM_EXPORT_EXPLORER __attribute__ ((visibility("default")))
#      else
/* We are using this library */
#        define MM_EXPORT_EXPLORER 
#      endif
#    endif

#    ifndef MM_PRIVATE_EXPLORER
#      define MM_PRIVATE_EXPLORER 
#    endif
#  endif

#  ifndef MM_DEPRECATED_EXPLORER
#    define MM_DEPRECATED_EXPLORER __attribute__ ((deprecated))
#  endif

#  ifndef MM_DEPRECATED_EXPORT_EXPLORER
#    define MM_DEPRECATED_EXPORT_EXPLORER MM_EXPORT_EXPLORER MM_DEPRECATED_EXPLORER
#  endif

#  ifndef MM_DEPRECATED_PRIVATE_EXPLORER
#    define MM_DEPRECATED_PRIVATE_EXPLORER MM_PRIVATE_EXPLORER MM_DEPRECATED_EXPLORER
#  endif

#endif

#endif//__mmExplorerExport_h__