/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmExplorerFinder.h"
#include "mmExplorerItem.h"

#include "nwsi/mmOgreFileSystemArchive.h"

#include "OgreResourceGroupManager.h"

MM_EXPORT_EXPLORER void mmExplorerFinder_Init(struct mmExplorerFinder* p)
{
    struct mmVectorValueEventAllocator hEventAllocator;

    mmString_Init(&p->directory);
    mmFileContext_Init(&p->file_context);
    mmVectorValue_Init(&p->vector_item);

    mmString_Assigns(&p->directory, ".");

    mmFileContext_SetAssetsRootFolder(&p->file_context, ".", ".");

    hEventAllocator.Produce = &mmVectorValue_ExplorerItemEventProduce;
    hEventAllocator.Recycle = &mmVectorValue_ExplorerItemEventRecycle;
    hEventAllocator.obj = p;
    mmVectorValue_SetElemSize(&p->vector_item, sizeof(struct mmExplorerItem));
    mmVectorValue_SetEventAllocator(&p->vector_item, &hEventAllocator);
}
MM_EXPORT_EXPLORER void mmExplorerFinder_Destroy(struct mmExplorerFinder* p)
{
    mmString_Destroy(&p->directory);
    mmFileContext_Destroy(&p->file_context);
    mmVectorValue_Destroy(&p->vector_item);
}

MM_EXPORT_EXPLORER void mmExplorerFinder_SetDirectory(struct mmExplorerFinder* p, const char* directory)
{
    mmString_Assigns(&p->directory, directory);
}

MM_EXPORT_EXPLORER void mmExplorerFinder_Refresh(struct mmExplorerFinder* p)
{
    mmExplorerFinder_OnBeforeTerminate(p);
    mmExplorerFinder_OnFinishLaunching(p);
}

MM_EXPORT_EXPLORER void mmExplorerFinder_OnFinishLaunching(struct mmExplorerFinder* p)
{
    Ogre::FileInfoListPtr _FileInfoListDir;
    Ogre::FileInfoListPtr _FileInfoListReg;

    size_t index = 0;
    struct mmExplorerItem* item = NULL;
    Ogre::FileInfoList::iterator it;

    Ogre::ResourceGroupManager* pResourceGroupManager = Ogre::ResourceGroupManager::getSingletonPtr();

    Ogre::String _directory(mmString_CStr(&p->directory));

    pResourceGroupManager->createResourceGroup(_directory);
    pResourceGroupManager->addResourceLocation(_directory, "mmFileSystem", _directory);
    pResourceGroupManager->initialiseResourceGroup(_directory);

    typedef Ogre::ResourceGroupManager::LocationList LocationList;
    const LocationList& _LocationList = pResourceGroupManager->getResourceLocationList(_directory);

    // replace with this file_context.
    for (LocationList::const_iterator it = _LocationList.begin();
        it != _LocationList.end(); it++)
    {
        Ogre::ResourceGroupManager::ResourceLocation* e = (*it);
        mm::mmOgreFileSystemArchive* archive = (mm::mmOgreFileSystemArchive*)e->archive;
        archive->mFileContext = &p->file_context;
    }

    // clear first.
    mmVectorValue_Clear(&p->vector_item);

    // search init info.
    _FileInfoListDir = pResourceGroupManager->findResourceFileInfo(_directory, "*", true);
    _FileInfoListReg = pResourceGroupManager->findResourceFileInfo(_directory, "*", false);

    assert(0 == p->vector_item.size && "0 == p->vector_item.size is invalid.");

    // resize the max size.
    mmVectorValue_AlignedMemory(&p->vector_item, _FileInfoListDir->size() + _FileInfoListReg->size());

    // sub directory.
    it = _FileInfoListDir->begin();
    while (it != _FileInfoListDir->end())
    {
        Ogre::FileInfo* e = &(*it);

        item = (struct mmExplorerItem*)mmVectorValue_GetIndexReference(&p->vector_item, index);

        mmExplorerItem_SetPath(item, mmString_CStr(&p->directory), e->basename.c_str());
        mmExplorerItem_UpdateMode(item);

        it++;
        index++;
    }

    // sub regular file.
    it = _FileInfoListReg->begin();
    while (it != _FileInfoListReg->end())
    {
        Ogre::FileInfo* e = &(*it);

        item = (struct mmExplorerItem*)mmVectorValue_GetIndexReference(&p->vector_item, index);

        mmExplorerItem_SetPath(item, mmString_CStr(&p->directory), e->basename.c_str());
        mmExplorerItem_UpdateMode(item);

        it++;
        index++;
    }
}
MM_EXPORT_EXPLORER void mmExplorerFinder_OnBeforeTerminate(struct mmExplorerFinder* p)
{
    mmVectorValue_Clear(&p->vector_item);

    Ogre::ResourceGroupManager* pResourceGroupManager = Ogre::ResourceGroupManager::getSingletonPtr();

    Ogre::String _directory(mmString_CStr(&p->directory));

    if (pResourceGroupManager->resourceGroupExists(_directory))
    {
        pResourceGroupManager->removeResourceLocation(_directory, _directory);
        pResourceGroupManager->destroyResourceGroup(_directory);
    }
}

