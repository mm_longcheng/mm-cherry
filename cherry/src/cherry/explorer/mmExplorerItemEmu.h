/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmExplorerItemEmu_h__
#define __mmExplorerItemEmu_h__

#include "core/mmCore.h"
#include "core/mmString.h"

#include "container/mmVectorValue.h"

#include "explorer/mmExplorerExport.h"

#include "core/mmPrefix.h"

//////////////////////////////////////////////////////////////////////////
// package.zip
//
//   cheatcode/          optional directory cheatcode
//   genie/              optional directory genie
//   ips/                optional directory ips
//   snapshot/           optional directory snapshot
//   tone/               optional directory tone
//   rom.nes             required file      rom source
//   introduction.txt    optional file      introduction txt
//   information.txt     optional file      information for this package, if it does not exist, 
//                                          the first ".nes" file will be use for assets_name 
//                                          and source_name.
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_EXPLORER extern const char* MM_EXPLORERITEMEMUINFORMATION_FILENAME;

struct mmExplorerItemEmuInformation
{
    struct mmString encoding;
    struct mmString assets_name;
    struct mmString source_name;
    struct mmString author;
    struct mmString source_version;
    struct mmString client_version;
    struct mmString introduction;
    struct mmString player_number;
    struct mmString introduction_string;
};
MM_EXPORT_EXPLORER void mmExplorerItemEmuInformation_Init(struct mmExplorerItemEmuInformation* p);
MM_EXPORT_EXPLORER void mmExplorerItemEmuInformation_Destroy(struct mmExplorerItemEmuInformation* p);
MM_EXPORT_EXPLORER void mmExplorerItemEmuInformation_Reset(struct mmExplorerItemEmuInformation* p);

struct mmExplorerItemEmu
{
    struct mmFileContext* file_context;// weak ref.

    struct mmExplorerItemEmuInformation information;
    struct mmFileAssetsSource* file_assets_source;

    struct mmString fullname;
    struct mmString pathname;
    struct mmString basename;

    struct mmVectorValue vector_cheatcode;
    struct mmVectorValue vector_genie;
    struct mmVectorValue vector_ips;
    struct mmVectorValue vector_snapshot;
    struct mmVectorValue vector_tone;
};
MM_EXPORT_EXPLORER void mmExplorerItemEmu_Init(struct mmExplorerItemEmu* p);
MM_EXPORT_EXPLORER void mmExplorerItemEmu_Destroy(struct mmExplorerItemEmu* p);

MM_EXPORT_EXPLORER void mmExplorerItemEmu_SetFileContext(struct mmExplorerItemEmu* p, struct mmFileContext* _file_context);
MM_EXPORT_EXPLORER void mmExplorerItemEmu_SetPath(struct mmExplorerItemEmu* p, const char* path, const char* base);

MM_EXPORT_EXPLORER void mmExplorerItemEmu_UpdateValue(struct mmExplorerItemEmu* p);

MM_EXPORT_EXPLORER mmBool_t mmExplorerItemEmu_IsValid(struct mmExplorerItemEmu* p);

MM_EXPORT_EXPLORER void mmExplorerItemEmu_OnFinishLaunching(struct mmExplorerItemEmu* p);
MM_EXPORT_EXPLORER void mmExplorerItemEmu_OnBeforeTerminate(struct mmExplorerItemEmu* p);

MM_EXPORT_EXPLORER void mmExplorerItemEmu_SearchFile(struct mmExplorerItemEmu* p, const char* directory, const char* pattern, struct mmVectorValue* _vector);

#include "core/mmSuffix.h"

#endif//__mmExplorerItemEmu_h__
