/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmExplorerItem.h"

#include "core/mmAlloc.h"

#include "dish/mmFilePath.h"


MM_EXPORT_EXPLORER void mmExplorerItem_Init(struct mmExplorerItem* p)
{
    mmString_Init(&p->fullname);
    mmString_Init(&p->pathname);
    mmString_Init(&p->basename);
    mmString_Init(&p->prefixname);
    mmString_Init(&p->suffixname);

    mmString_Init(&p->type_real);
    mmString_Init(&p->type_image);

    mmString_Assigns(&p->type_real, "");
    mmString_Assigns(&p->type_image, "");

    mmMemset(&p->s, 0, sizeof(mmStat_t));
    p->mode = MM_S_IFREG | MM_S_IRWXU | MM_S_IRWXG | MM_S_IRWXO;
}
MM_EXPORT_EXPLORER void mmExplorerItem_Destroy(struct mmExplorerItem* p)
{
    mmString_Destroy(&p->fullname);
    mmString_Destroy(&p->pathname);
    mmString_Destroy(&p->basename);
    mmString_Destroy(&p->prefixname);
    mmString_Destroy(&p->suffixname);

    mmString_Destroy(&p->type_real);
    mmString_Destroy(&p->type_image);

    mmMemset(&p->s, 0, sizeof(mmStat_t));
    p->mode = MM_S_IFREG | MM_S_IRWXU | MM_S_IRWXG | MM_S_IRWXO;
}

MM_EXPORT_EXPLORER void mmExplorerItem_SetPath(struct mmExplorerItem* p, const char* path, const char* base)
{
    mmString_Assigns(&p->pathname, path);
    mmString_Assigns(&p->basename, base);
    mmString_Assigns(&p->fullname, path);
    mmString_Appends(&p->fullname, "/");
    mmString_Appends(&p->fullname, base);

    mmPathSplitSuffixName(&p->basename, &p->prefixname, &p->suffixname);
}
MM_EXPORT_EXPLORER void mmExplorerItem_SetType(struct mmExplorerItem* p, const char* type_real, const char* type_image)
{
    mmString_Assigns(&p->type_real, type_real);
    mmString_Assigns(&p->type_image, type_image);
}

MM_EXPORT_EXPLORER void mmExplorerItem_UpdateMode(struct mmExplorerItem* p)
{
    mmStat(mmString_CStr(&p->fullname), &p->s);

    p->mode = mmStat_Mode(&p->s);
}

MM_EXPORT_EXPLORER void mmExplorerItem_ModeString(struct mmExplorerItem* p, char buffer[16])
{
    mmMemset(buffer, 0, 16);

    if (MM_S_ISLNK (p->mode)) { buffer[0] = 'l'; }
    if (MM_S_ISREG (p->mode)) { buffer[0] = '-'; }
    if (MM_S_ISDIR (p->mode)) { buffer[0] = 'd'; }
    if (MM_S_ISCHR (p->mode)) { buffer[0] = 'c'; }
    if (MM_S_ISBLK (p->mode)) { buffer[0] = 'b'; }
    if (MM_S_ISFIFO(p->mode)) { buffer[0] = 'f'; }
    if (MM_S_ISSOCK(p->mode)) { buffer[0] = 's'; }

    buffer[1] = (MM_S_IRUSR & p->mode) ? 'r' : '-';
    buffer[2] = (MM_S_IWUSR & p->mode) ? 'w' : '-';
    buffer[3] = (MM_S_IXUSR & p->mode) ? 'x' : '-';

    buffer[4] = (MM_S_IRGRP & p->mode) ? 'r' : '-';
    buffer[5] = (MM_S_IWGRP & p->mode) ? 'w' : '-';
    buffer[6] = (MM_S_IXGRP & p->mode) ? 'x' : '-';

    buffer[7] = (MM_S_IROTH & p->mode) ? 'r' : '-';
    buffer[8] = (MM_S_IWOTH & p->mode) ? 'w' : '-';
    buffer[9] = (MM_S_IXOTH & p->mode) ? 'x' : '-';
}
MM_EXPORT_EXPLORER void mmExplorerItem_SizeString(struct mmExplorerItem* p, char buffer[32])
{
    static const char* __size_unit[7] =
    {
        "B",
        "K",
        "M",
        "G",
        "T",
        "P",
        "E",
    };
    static mmUInt64_t __size_norm[7] =
    {
        1ULL,
        1024ULL,
        1048576ULL,
        1073741824ULL,
        1099511627776ULL,
        1125899906842624ULL,
        1152921504606846976ULL,
    };
    static const size_t __size_norm_size = MM_ARRAY_SIZE(__size_norm);

    size_t i = 0;
    size_t sz_a = 0;
    size_t sz_b = 0;
    mmUInt64_t st_size = p->s.st_size;
    mmUInt64_t norm = 0;
    float decimal = 0.0f;
    float decimal_value_l = 0.0f;
    float decimal_value_r = 0.0f;
    
    i = __size_norm_size - 1;
    while (0 != i)
    {
        if (__size_norm[i] < st_size)
        {
            break;
        }
        i--;
    }

    norm = __size_norm[i];
    sz_a = st_size / norm;
    sz_b = st_size % norm;
    decimal_value_l = (float)sz_a;
    decimal_value_r = (float)sz_b / (float)norm;
    decimal = decimal_value_l + decimal_value_r;

    mmSprintf(buffer, "%.2f%s", decimal, __size_unit[i]);
}

MM_EXPORT_EXPLORER void mmVectorValue_ExplorerItemEventProduce(void* obj, void* u)
{
    struct mmExplorerItem* e = (struct mmExplorerItem*)(u);
    mmExplorerItem_Init(e);
}
MM_EXPORT_EXPLORER void mmVectorValue_ExplorerItemEventRecycle(void* obj, void* u)
{
    struct mmExplorerItem* e = (struct mmExplorerItem*)(u);
    mmExplorerItem_Destroy(e);
}

