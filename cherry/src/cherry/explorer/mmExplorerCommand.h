/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmExplorerCommand_h__
#define __mmExplorerCommand_h__

#include "core/mmCore.h"
#include "core/mmString.h"
#include "core/mmFileSystem.h"

#include "container/mmRbtsetVpt.h"

#include "explorer/mmExplorerItem.h"
#include "explorer/mmExplorerExport.h"

#include "core/mmPrefix.h"


MM_EXPORT_EXPLORER void mmExplorerTouch(const char* f);
MM_EXPORT_EXPLORER void mmExplorerRemove(const char* f);


struct mmExplorerItem;

typedef void(*mmExplorerCommandCallbackUrl)(void* obj, struct mmExplorerItem* _item);
typedef void(*mmExplorerCommandCallbackStr)(void* obj, const char* directory);
struct mmExplorerCommandCallback
{
    mmExplorerCommandCallbackUrl OpenDir;// open directory
    mmExplorerCommandCallbackUrl OpenReg;// open regular
    mmExplorerCommandCallbackUrl Preview;
    mmExplorerCommandCallbackStr Refresh;
    void* obj;
};
MM_EXPORT_EXPLORER void mmExplorerCommandCallback_Init(struct mmExplorerCommandCallback* p);
MM_EXPORT_EXPLORER void mmExplorerCommandCallback_Destroy(struct mmExplorerCommandCallback* p);
MM_EXPORT_EXPLORER void mmExplorerCommandCallback_Reset(struct mmExplorerCommandCallback* p);

#define MM_EXPLORERCOMMAND_MODE_COPY 0x00000001
#define MM_EXPLORERCOMMAND_MODE_CUT  0x00000002

struct mmExplorerCommand
{
    struct mmExplorerCommandCallback callback;
    struct mmVectorValue vector_item;
    mmUInt32_t url_mode;
    mmUInt32_t index;
};

MM_EXPORT_EXPLORER void mmExplorerCommand_Init(struct mmExplorerCommand* p);
MM_EXPORT_EXPLORER void mmExplorerCommand_Destroy(struct mmExplorerCommand* p);

MM_EXPORT_EXPLORER void mmExplorerCommand_SetCallback(struct mmExplorerCommand* p, struct mmExplorerCommandCallback* callback);

MM_EXPORT_EXPLORER void mmExplorerCommand_RealBasename(struct mmExplorerCommand* p, struct mmString* _real_basename, const char* f);

MM_EXPORT_EXPLORER void mmExplorerCommand_Copy(struct mmExplorerCommand* p, struct mmRbtsetVpt* item_select);
MM_EXPORT_EXPLORER void mmExplorerCommand_Cut(struct mmExplorerCommand* p, struct mmRbtsetVpt* item_select);
MM_EXPORT_EXPLORER void mmExplorerCommand_Paste(struct mmExplorerCommand* p, const char* f);

MM_EXPORT_EXPLORER void mmExplorerCommand_Rename(struct mmExplorerCommand* p, const char* f, const char* t);
MM_EXPORT_EXPLORER void mmExplorerCommand_RenameItem(struct mmExplorerCommand* p, struct mmExplorerItem* _item, const char* t);
MM_EXPORT_EXPLORER void mmExplorerCommand_Remove(struct mmExplorerCommand* p, const char* f);
MM_EXPORT_EXPLORER void mmExplorerCommand_RemoveVector(struct mmExplorerCommand* p, struct mmRbtsetVpt* item_select);

MM_EXPORT_EXPLORER void mmExplorerCommand_Mkdir(struct mmExplorerCommand* p, const char* f, int mode);
MM_EXPORT_EXPLORER void mmExplorerCommand_Rmdir(struct mmExplorerCommand* p, const char* f);
MM_EXPORT_EXPLORER void mmExplorerCommand_Touch(struct mmExplorerCommand* p, const char* f);

MM_EXPORT_EXPLORER void mmExplorerCommand_OpenDir(struct mmExplorerCommand* p, struct mmExplorerItem* _item);
MM_EXPORT_EXPLORER void mmExplorerCommand_OpenReg(struct mmExplorerCommand* p, struct mmExplorerItem* _item);
MM_EXPORT_EXPLORER void mmExplorerCommand_Preview(struct mmExplorerCommand* p, struct mmExplorerItem* _item);
MM_EXPORT_EXPLORER void mmExplorerCommand_Refresh(struct mmExplorerCommand* p, const char* directory);

#include "core/mmSuffix.h"

#endif//__mmExplorerCommand_h__