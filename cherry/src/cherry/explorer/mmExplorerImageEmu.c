/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmExplorerImageEmu.h"

#include "core/mmLogger.h"

#include "dish/mmFilePath.h"

#include "explorer/mmExplorerItem.h"

#include "minizip/unzip.h"

MM_EXPORT_EXPLORER mmUInt32_t mmExplorerImage_FuncEmulator(struct mmExplorerItem* item, struct mmString* real_type)
{
    unzFile unzipFile = NULL;
    unz_global_info unzipGlobalInfo;
    unz_file_info unzipFileInfo;
    char fname_buf[256];
    mmUInt32_t code = MM_UNKNOWN;
    uLong i = 0;

    struct mmString qualified_name;
    struct mmString basename;
    struct mmString suffixname;

    size_t dir_pos;

    struct mmLogger* gLogger = mmLogger_Instance();

    mmString_Init(&qualified_name);
    mmString_Init(&basename);
    mmString_Init(&suffixname);

    do
    {
        // 16M = 1024 * 1024 * 16 = 16777216
        if (16777216 < item->s.st_size)
        {
            // Not so big nes file.
            code = MM_FAILURE;
            break;
        }
        if (!(unzipFile = unzOpen((const char*)mmString_CStr(&item->fullname))))
        {
            mmLogger_LogW(gLogger, "%s %d file: %s is not exist.", __FUNCTION__, __LINE__, mmString_CStr(&item->fullname));
            code = MM_FAILURE;
            break;
        }

        if (UNZ_OK != unzGetGlobalInfo(unzipFile, &unzipGlobalInfo))
        {
            unzClose(unzipFile);
            unzipFile = NULL;
            code = MM_FAILURE;
            break;
        }

        if (128 < unzipGlobalInfo.number_entry)
        {
            unzClose(unzipFile);
            unzipFile = NULL;
            // Not so complex emu zip file.
            code = MM_FAILURE;
            break;
        }

        for (i = 0; i < unzipGlobalInfo.number_entry; i++)
        {
            if (UNZ_OK != unzGetCurrentFileInfo(unzipFile, &unzipFileInfo, fname_buf, sizeof(fname_buf), NULL, 0, NULL, 0))
            {
                break;
            }

            mmString_Assigns(&qualified_name, fname_buf);

            dir_pos = mmString_FindLastOf(&qualified_name, '/');

            if (dir_pos == (size_t)mmStringNpos)
            {
                mmPathSplitSuffixName(&qualified_name, &basename, &suffixname);

                if (0 == mmString_CompareCStr(&suffixname, "nes") ||
                    0 == mmString_CompareCStr(&suffixname, "NES") ||
                    0 == mmString_CompareCStr(&suffixname, "Nes") ||
                    0 == mmString_CompareCStr(&suffixname, "fds") ||
                    0 == mmString_CompareCStr(&suffixname, "FDS") ||
                    0 == mmString_CompareCStr(&suffixname, "Fds") ||
                    0 == mmString_CompareCStr(&suffixname, "nsf") ||
                    0 == mmString_CompareCStr(&suffixname, "NSF") ||
                    0 == mmString_CompareCStr(&suffixname, "Nsf"))
                {
                    mmString_Assigns(real_type, "-.emu");
                    code = MM_SUCCESS;
                    break;
                }
            }

            // Next file
            if ((i + 1) < unzipGlobalInfo.number_entry)
            {
                if (UNZ_OK != unzGoToNextFile(unzipFile))
                {
                    break;
                }
            }
        }

    } while (0);

    if (unzipFile) 
    {
        unzCloseCurrentFile(unzipFile);
        unzClose(unzipFile);
    }

    mmString_Destroy(&qualified_name);
    mmString_Destroy(&basename);
    mmString_Destroy(&suffixname);

    return code;
}
