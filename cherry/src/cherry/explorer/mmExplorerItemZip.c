/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmExplorerItemZip.h"
#include "mmExplorerItem.h"

#include "dish/mmFilePath.h"

MM_EXPORT_EXPLORER void mmExplorerItemZip_Init(struct mmExplorerItemZip* p)
{
    struct mmVectorValueEventAllocator hEventAllocator;

    mmFileAssetsSource_Init(&p->file_assets_source);
    mmString_Init(&p->fullname);
    mmString_Init(&p->pathname);
    mmString_Init(&p->basename);
    mmVectorValue_Init(&p->vector_item);

    mmString_Assigns(&p->fullname, "");
    mmString_Assigns(&p->pathname, "");
    mmString_Assigns(&p->basename, "");

    hEventAllocator.Produce = &mmVectorValue_ExplorerItemEventProduce;
    hEventAllocator.Recycle = &mmVectorValue_ExplorerItemEventRecycle;
    hEventAllocator.obj = p;
    mmVectorValue_SetElemSize(&p->vector_item, sizeof(struct mmExplorerItem));
    mmVectorValue_SetEventAllocator(&p->vector_item, &hEventAllocator);
}
MM_EXPORT_EXPLORER void mmExplorerItemZip_Destroy(struct mmExplorerItemZip* p)
{
    mmFileAssetsSource_Destroy(&p->file_assets_source);
    mmString_Destroy(&p->fullname);
    mmString_Destroy(&p->pathname);
    mmString_Destroy(&p->basename);
    mmVectorValue_Destroy(&p->vector_item);
}

MM_EXPORT_EXPLORER void mmExplorerItemZip_SetPath(struct mmExplorerItemZip* p, const char* path, const char* base)
{
    mmString_Assigns(&p->pathname, path);
    mmString_Assigns(&p->basename, base);
    mmString_Assigns(&p->fullname, path);
    mmString_Appends(&p->fullname, "/");
    mmString_Appends(&p->fullname, base);

    mmFileAssetsSource_SetAssets(&p->file_assets_source, mmString_CStr(&p->fullname), mmString_CStr(&p->fullname), ".");

    mmStat(mmString_CStr(&p->fullname), &p->s);

    p->mode = mmStat_Mode(&p->s);
}

MM_EXPORT_EXPLORER void mmExplorerItemZip_Refresh(struct mmExplorerItemZip* p)
{
    mmExplorerItemZip_OnBeforeTerminate(p);
    mmExplorerItemZip_OnFinishLaunching(p);
}

MM_EXPORT_EXPLORER void mmExplorerItemZip_OnFinishLaunching(struct mmExplorerItemZip* p)
{
    size_t index = 0;
    struct mmExplorerItem* item = NULL;

    struct mmRbtreeStringVpt rbtree_dir;
    struct mmRbtreeStringVpt rbtree_reg;

    mmRbtreeStringVpt_Init(&rbtree_dir);
    mmRbtreeStringVpt_Init(&rbtree_reg);

    mmFileAssetsSource_AcquireEntry(&p->file_assets_source);

    mmFileAssetsSource_AcquireFindFiles(&p->file_assets_source, ".", "*", 0, 1, 1, &rbtree_dir);
    mmFileAssetsSource_AcquireFindFiles(&p->file_assets_source, ".", "*", 0, 0, 1, &rbtree_reg);

    assert(0 == p->vector_item.size && "0 == p->vector_item.size is invalid.");

    // resize the max size.
    mmVectorValue_AlignedMemory(&p->vector_item, rbtree_dir.size + rbtree_reg.size);

    // dir
    {
        struct mmFileAssetsInfo* e = NULL;
        struct mmRbNode* n = NULL;
        struct mmRbtreeStringVptIterator* it = NULL;
        n = mmRb_First(&rbtree_dir.rbt);
        while (NULL != n)
        {
            it = (struct mmRbtreeStringVptIterator*)mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
            n = mmRb_Next(n);
            e = (struct mmFileAssetsInfo*)(it->v);

            item = (struct mmExplorerItem*)mmVectorValue_GetIndexReference(&p->vector_item, index);

            mmExplorerItem_SetPath(item, ".", mmString_CStr(&e->basename));
            item->s = p->s;
            item->s.st_size = e->dsize;
            item->mode = p->mode;
            item->mode &= ~MM_S_IFREG;
            item->mode |= MM_S_IFDIR;

            index++;
        }
    }
    // reg
    {
        struct mmFileAssetsInfo* e = NULL;
        struct mmRbNode* n = NULL;
        struct mmRbtreeStringVptIterator* it = NULL;
        n = mmRb_First(&rbtree_reg.rbt);
        while (NULL != n)
        {
            it = (struct mmRbtreeStringVptIterator*)mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
            n = mmRb_Next(n);
            e = (struct mmFileAssetsInfo*)(it->v);

            item = (struct mmExplorerItem*)mmVectorValue_GetIndexReference(&p->vector_item, index);

            mmExplorerItem_SetPath(item, ".", mmString_CStr(&e->basename));
            item->s = p->s;
            item->s.st_size = e->dsize;
            item->mode = p->mode;
            item->mode &= ~MM_S_IFDIR;
            item->mode |= MM_S_IFREG;

            index++;
        }
    }

    mmFileAssetsSource_ReleaseFindFiles(&p->file_assets_source, &rbtree_dir);
    mmFileAssetsSource_ReleaseFindFiles(&p->file_assets_source, &rbtree_reg);

    mmFileAssetsSource_ReleaseEntry(&p->file_assets_source);

    mmRbtreeStringVpt_Destroy(&rbtree_dir);
    mmRbtreeStringVpt_Destroy(&rbtree_reg);
}
MM_EXPORT_EXPLORER void mmExplorerItemZip_OnBeforeTerminate(struct mmExplorerItemZip* p)
{
    mmVectorValue_Clear(&p->vector_item);
}
