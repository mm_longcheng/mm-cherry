/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmExplorerItem_h__
#define __mmExplorerItem_h__

#include "core/mmCore.h"
#include "core/mmString.h"
#include "core/mmFileSystem.h"

#include "container/mmVectorValue.h"

#include "explorer/mmExplorerExport.h"

#include "core/mmPrefix.h"


struct mmExplorerItem
{
    struct mmString fullname;
    struct mmString pathname;
    struct mmString basename;
    struct mmString prefixname;
    struct mmString suffixname;

    struct mmString type_real;
    struct mmString type_image;

    mmStat_t s;
    mmUInt32_t mode;
};
MM_EXPORT_EXPLORER void mmExplorerItem_Init(struct mmExplorerItem* p);
MM_EXPORT_EXPLORER void mmExplorerItem_Destroy(struct mmExplorerItem* p);

MM_EXPORT_EXPLORER void mmExplorerItem_SetPath(struct mmExplorerItem* p, const char* path, const char* base);
MM_EXPORT_EXPLORER void mmExplorerItem_SetType(struct mmExplorerItem* p, const char* type_real, const char* type_image);

MM_EXPORT_EXPLORER void mmExplorerItem_UpdateMode(struct mmExplorerItem* p);

MM_EXPORT_EXPLORER void mmExplorerItem_ModeString(struct mmExplorerItem* p, char buffer[16]);
MM_EXPORT_EXPLORER void mmExplorerItem_SizeString(struct mmExplorerItem* p, char buffer[32]);

MM_EXPORT_EXPLORER void mmVectorValue_ExplorerItemEventProduce(void* obj, void* u);
MM_EXPORT_EXPLORER void mmVectorValue_ExplorerItemEventRecycle(void* obj, void* u);

#include "core/mmSuffix.h"

#endif//__mmExplorerItem_h__