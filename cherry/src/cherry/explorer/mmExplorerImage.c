/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmExplorerImage.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"

#include "explorer/mmExplorerItem.h"

static void* __static_mmRbtreeStringVpt_RbtsetVptProduce(struct mmRbtreeStringVpt* p, const struct mmString* k)
{
    struct mmRbtsetVpt* e = (struct mmRbtsetVpt*)mmMalloc(sizeof(struct mmRbtsetVpt));
    mmRbtsetVpt_Init(e);
    return e;
}
static void* __static_mmRbtreeStringVpt_RbtsetVptRecycle(struct mmRbtreeStringVpt* p, const struct mmString* k, void* v)
{
    struct mmRbtsetVpt* e = (struct mmRbtsetVpt*)v;
    mmRbtsetVpt_Destroy(e);
    mmFree(e);
    return e;
}

MM_EXPORT_EXPLORER void mmExplorerImage_Init(struct mmExplorerImage* p)
{
    struct mmRbtreeStringVptAllocator hVptAllocator;

    mmRbtreeStringString_Init(&p->hSSImageMapper);
    mmRbtreeU32String_Init(&p->hISImageMapper);
    mmRbtreeStringVpt_Init(&p->hSSFuncMapper);

    mmString_Init(&p->hDefaultSSImage);
    mmString_Init(&p->hDefaultISImage);

    hVptAllocator.Produce = &__static_mmRbtreeStringVpt_RbtsetVptProduce;
    hVptAllocator.Recycle = &__static_mmRbtreeStringVpt_RbtsetVptRecycle;
    hVptAllocator.obj = p;
    mmRbtreeStringVpt_SetAllocator(&p->hSSFuncMapper, &hVptAllocator);
}
MM_EXPORT_EXPLORER void mmExplorerImage_Destroy(struct mmExplorerImage* p)
{
    mmRbtreeStringString_Destroy(&p->hSSImageMapper);
    mmRbtreeU32String_Destroy(&p->hISImageMapper);
    mmRbtreeStringVpt_Destroy(&p->hSSFuncMapper);

    mmString_Destroy(&p->hDefaultSSImage);
    mmString_Destroy(&p->hDefaultISImage);
}

MM_EXPORT_EXPLORER void mmExplorerImage_SetDefaultSSImage(struct mmExplorerImage* p, const char* image)
{
    mmString_Assigns(&p->hDefaultSSImage, image);
}
MM_EXPORT_EXPLORER void mmExplorerImage_SetDefaultISImage(struct mmExplorerImage* p, const char* image)
{
    mmString_Assigns(&p->hDefaultISImage, image);
}

// ss
MM_EXPORT_EXPLORER void mmExplorerImage_AddSSImage(struct mmExplorerImage* p, const char* type, const char* image)
{
    struct mmString hTypeWeak;
    struct mmString hImageWeak;

    mmString_MakeWeak(&hTypeWeak, type);
    mmString_MakeWeak(&hImageWeak, image);

    mmRbtreeStringString_Set(&p->hSSImageMapper, &hTypeWeak, &hImageWeak);
}
MM_EXPORT_EXPLORER void mmExplorerImage_RmvSSImage(struct mmExplorerImage* p, const char* type)
{
    struct mmString hTypeWeak;

    mmString_MakeWeak(&hTypeWeak, type);

    mmRbtreeStringString_Rmv(&p->hSSImageMapper, &hTypeWeak);
}
MM_EXPORT_EXPLORER void mmExplorerImage_GetSSImage(struct mmExplorerImage* p, const char* type, struct mmString* image)
{
    struct mmString hTypeWeak;
    struct mmRbtreeStringStringIterator* it = NULL;

    mmString_MakeWeak(&hTypeWeak, type);

    it = mmRbtreeStringString_GetIterator(&p->hSSImageMapper, &hTypeWeak);
    if (NULL == it)
    {
        mmString_Assign(image, &p->hDefaultSSImage);
    }
    else
    {
        mmString_Assign(image, &it->v);
    }
}
MM_EXPORT_EXPLORER void mmExplorerImage_ClearSSImage(struct mmExplorerImage* p)
{
    mmRbtreeStringString_Clear(&p->hSSImageMapper);
}

// is
MM_EXPORT_EXPLORER void mmExplorerImage_AddISImage(struct mmExplorerImage* p, mmUInt32_t type, const char* image)
{
    struct mmString hImageWeak;

    mmString_MakeWeak(&hImageWeak, image);

    mmRbtreeU32String_Set(&p->hISImageMapper, type, &hImageWeak);
}
MM_EXPORT_EXPLORER void mmExplorerImage_RmvISImage(struct mmExplorerImage* p, mmUInt32_t type)
{
    mmRbtreeU32String_Rmv(&p->hISImageMapper, type);
}
MM_EXPORT_EXPLORER void mmExplorerImage_GetISImage(struct mmExplorerImage* p, mmUInt32_t type, struct mmString* image)
{
    struct mmRbtreeU32StringIterator* it = NULL;

    it = mmRbtreeU32String_GetIterator(&p->hISImageMapper, type);
    if (NULL == it)
    {
        mmString_Assign(image, &p->hDefaultISImage);
    }
    else
    {
        mmString_Assign(image, &it->v);
    }
}
MM_EXPORT_EXPLORER void mmExplorerImage_ClearISImage(struct mmExplorerImage* p)
{
    mmRbtreeU32String_Clear(&p->hISImageMapper);
}


MM_EXPORT_EXPLORER void mmExplorerImage_AddSSFunc(struct mmExplorerImage* p, const char* type, mmExplorerImageFunc func)
{
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmString hTypeWeak;

    mmString_MakeWeak(&hTypeWeak, type);
    it = mmRbtreeStringVpt_GetIterator(&p->hSSFuncMapper, &hTypeWeak);
    if (NULL == it)
    {
        // Not exist, we create new one for it.
        struct mmRbtsetVpt* pRbtsetVpt = (struct mmRbtsetVpt*)mmRbtreeStringVpt_Add(&p->hSSFuncMapper, &hTypeWeak);
        mmRbtsetVpt_Add(pRbtsetVpt, func);
    }
    else
    {
        // Already exists just use it.
        struct mmRbtsetVpt* pRbtsetVpt = (struct mmRbtsetVpt*)it->v;
        mmRbtsetVpt_Add(pRbtsetVpt, func);
    }
}
MM_EXPORT_EXPLORER void mmExplorerImage_RmvSSFunc(struct mmExplorerImage* p, const char* type, mmExplorerImageFunc func)
{
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmString hTypeWeak;

    mmString_MakeWeak(&hTypeWeak, type);
    it = mmRbtreeStringVpt_GetIterator(&p->hSSFuncMapper, &hTypeWeak);
    if (NULL == it)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogW(gLogger, "%s %d ss type: %s not exist do nothing here.", __FUNCTION__, __LINE__, type);
    }
    else
    {
        struct mmRbtsetVpt* pRbtsetVpt = (struct mmRbtsetVpt*)it->v;
        mmRbtsetVpt_Rmv(pRbtsetVpt, func);
    }
}
MM_EXPORT_EXPLORER void mmExplorerImage_ClearSSFuncByType(struct mmExplorerImage* p, const char* type)
{
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmString hTypeWeak;

    mmString_MakeWeak(&hTypeWeak, type);
    it = mmRbtreeStringVpt_GetIterator(&p->hSSFuncMapper, &hTypeWeak);
    if (NULL == it)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogW(gLogger, "%s %d ss type: %s not exist do nothing here.", __FUNCTION__, __LINE__, type);
    }
    else
    {
        struct mmRbtsetVpt* pRbtsetVpt = (struct mmRbtsetVpt*)it->v;
        mmRbtsetVpt_Clear(pRbtsetVpt);
    }
}
MM_EXPORT_EXPLORER void mmExplorerImage_ClearSSFunc(struct mmExplorerImage* p)
{
    mmRbtreeStringVpt_Clear(&p->hSSFuncMapper);
}

MM_EXPORT_EXPLORER void mmExplorerImage_ItemType(struct mmExplorerImage* p, struct mmExplorerItem* item)
{
    struct mmString hTypeReal;
    struct mmString hTypeImage;
    struct mmString hCategory;

    mmString_Init(&hTypeReal);
    mmString_Init(&hTypeImage);
    mmString_Init(&hCategory);

    if (MM_S_ISREG(item->mode))
    {
        const char* pType = mmString_CStr(&item->suffixname);

        mmString_Assigns(&hCategory, "-");

        mmString_Assign(&hTypeReal, &hCategory);
        mmString_Appends(&hTypeReal, ".");
        mmString_Appends(&hTypeReal, pType);

        mmExplorerImage_GetSSImage(p, mmString_CStr(&hTypeReal), &hTypeImage);

        if (0 == mmString_Compare(&hTypeImage, &p->hDefaultSSImage))
        {
            mmString_Assigns(&hCategory, "u");

            mmString_Assign(&hTypeReal, &hCategory);
            mmString_Appends(&hTypeReal, ".");
            mmString_Appends(&hTypeReal, "---");
        }
        else
        {
            struct mmRbtreeStringVptIterator* it = NULL;
            it = mmRbtreeStringVpt_GetIterator(&p->hSSFuncMapper, &hTypeReal);
            if (NULL != it)
            {
                struct mmRbtsetVptIterator* iter = NULL;
                struct mmRbNode* n = NULL;
                struct mmRbtsetVpt* pRbtsetVpt = (struct mmRbtsetVpt*)it->v;
                mmExplorerImageFunc pFunc = NULL;
                mmUInt32_t hCode = MM_UNKNOWN;

                n = mmRb_First(&pRbtsetVpt->rbt);
                while (NULL != n)
                {
                    iter = (struct mmRbtsetVptIterator*)mmRb_Entry(n, struct mmRbtsetVptIterator, n);
                    n = mmRb_Next(n);
                    pFunc = (mmExplorerImageFunc)(iter->k);

                    hCode = (*pFunc)(item, &hTypeReal);
                }

                mmExplorerImage_GetSSImage(p, mmString_CStr(&hTypeReal), &hTypeImage);
            }
        }
    }
    else
    {
        mmString_Assigns(&hCategory, "d");

        mmString_Assign(&hTypeReal, &hCategory);
        mmString_Appends(&hTypeReal, ".");
        mmString_Appends(&hTypeReal, "---");

        mmExplorerImage_GetISImage(p, (item->mode & MM_S_IFMT), &hTypeImage);
    }

    mmExplorerItem_SetType(item, mmString_CStr(&hTypeReal), mmString_CStr(&hTypeImage));

    mmString_Destroy(&hTypeReal);
    mmString_Destroy(&hTypeImage);
    mmString_Destroy(&hCategory);
}

MM_EXPORT_EXPLORER void mmExplorerImage_RegisterDefaultImageMapper(struct mmExplorerImage* p)
{
    mmExplorerImage_SetDefaultSSImage(p, "ImagesetExplorer/ft_unknown");
    mmExplorerImage_SetDefaultISImage(p, "ImagesetExplorer/ft_directory");

    mmExplorerImage_AddSSImage(p, "-.txt", "ImagesetExplorer/ft_txt");
    mmExplorerImage_AddSSImage(p, "-.zip", "ImagesetExplorer/ft_pkg");
    mmExplorerImage_AddSSImage(p, "-.png", "ImagesetExplorer/ft_image");

    mmExplorerImage_AddSSImage(p, "-.nes", "ImagesetExplorer/ft_emulator");
    mmExplorerImage_AddSSImage(p, "-.NES", "ImagesetExplorer/ft_emulator");
    mmExplorerImage_AddSSImage(p, "-.Nes", "ImagesetExplorer/ft_emulator");

    mmExplorerImage_AddSSImage(p, "-.nsf", "ImagesetExplorer/ft_emulator");
    mmExplorerImage_AddSSImage(p, "-.NSF", "ImagesetExplorer/ft_emulator");
    mmExplorerImage_AddSSImage(p, "-.Nsf", "ImagesetExplorer/ft_emulator");

    mmExplorerImage_AddSSImage(p, "-.sdf", "ImagesetExplorer/ft_emulator");
    mmExplorerImage_AddSSImage(p, "-.SDF", "ImagesetExplorer/ft_emulator");
    mmExplorerImage_AddSSImage(p, "-.Sdf", "ImagesetExplorer/ft_emulator");

    mmExplorerImage_AddSSImage(p, "-.emu", "ImagesetExplorer/ft_pkg_emulator");

    mmExplorerImage_AddISImage(p, MM_S_IFSOCK, "ImagesetExplorer/ft_directory");
    mmExplorerImage_AddISImage(p, MM_S_IFLNK , "ImagesetExplorer/ft_directory");
    mmExplorerImage_AddISImage(p, MM_S_IFREG , "ImagesetExplorer/ft_directory");
    mmExplorerImage_AddISImage(p, MM_S_IFBLK , "ImagesetExplorer/ft_directory");
    mmExplorerImage_AddISImage(p, MM_S_IFDIR , "ImagesetExplorer/ft_directory");
    mmExplorerImage_AddISImage(p, MM_S_IFCHR , "ImagesetExplorer/ft_directory");
    mmExplorerImage_AddISImage(p, MM_S_IFIFO , "ImagesetExplorer/ft_directory");
}
