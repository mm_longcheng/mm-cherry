/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmExplorerCommand.h"

#include "core/mmLogger.h"

#include "dish/mmFilePath.h"

static void __static_mmExplorerCommand_CopyRbtset(struct mmExplorerCommand* p, struct mmRbtsetVpt* item_select);
static void __static_mmExplorerCommand_ClearRbtset(struct mmExplorerCommand* p);

MM_EXPORT_EXPLORER void mmExplorerTouch(const char* f)
{
    FILE* fd = fopen(f, "wb");
    if (NULL != fd)
    {
        fclose(fd);
    }
}
MM_EXPORT_EXPLORER void mmExplorerRemove(const char* f)
{
    if (0 == mmAccess(f, MM_ACCESS_F_OK))
    {
        mmRecursiveRemove(f);
    }
}

static void __static_mmExplorerCommandCallbackUrl_Open(void* obj, struct mmExplorerItem* _item)
{

}
static void __static_mmExplorerCommandCallbackUrl_Preview(void* obj, struct mmExplorerItem* _item)
{

}
static void __static_mmExplorerCommandCallbackStr_Refresh(void* obj, const char* directory)
{

}
MM_EXPORT_EXPLORER void mmExplorerCommandCallback_Init(struct mmExplorerCommandCallback* p)
{
    p->OpenDir = &__static_mmExplorerCommandCallbackUrl_Open;
    p->OpenReg = &__static_mmExplorerCommandCallbackUrl_Open;
    p->Preview = &__static_mmExplorerCommandCallbackUrl_Preview;
    p->Refresh = &__static_mmExplorerCommandCallbackStr_Refresh;
    p->obj = NULL;
}
MM_EXPORT_EXPLORER void mmExplorerCommandCallback_Destroy(struct mmExplorerCommandCallback* p)
{
    p->OpenDir = &__static_mmExplorerCommandCallbackUrl_Open;
    p->OpenReg = &__static_mmExplorerCommandCallbackUrl_Open;
    p->Preview = &__static_mmExplorerCommandCallbackUrl_Preview;
    p->Refresh = &__static_mmExplorerCommandCallbackStr_Refresh;
    p->obj = NULL;
}
MM_EXPORT_EXPLORER void mmExplorerCommandCallback_Reset(struct mmExplorerCommandCallback* p)
{
    p->OpenDir = &__static_mmExplorerCommandCallbackUrl_Open;
    p->OpenReg = &__static_mmExplorerCommandCallbackUrl_Open;
    p->Preview = &__static_mmExplorerCommandCallbackUrl_Preview;
    p->Refresh = &__static_mmExplorerCommandCallbackStr_Refresh;
    p->obj = NULL;
}

MM_EXPORT_EXPLORER void mmExplorerCommand_Init(struct mmExplorerCommand* p)
{
    struct mmVectorValueEventAllocator hEventAllocator;

    mmExplorerCommandCallback_Init(&p->callback);
    mmVectorValue_Init(&p->vector_item);
    p->index = 0;
    p->url_mode = 0;

    hEventAllocator.Produce = &mmVectorValue_ExplorerItemEventProduce;
    hEventAllocator.Recycle = &mmVectorValue_ExplorerItemEventRecycle;
    hEventAllocator.obj = p;
    mmVectorValue_SetElemSize(&p->vector_item, sizeof(struct mmExplorerItem));
    mmVectorValue_SetEventAllocator(&p->vector_item, &hEventAllocator);
}
MM_EXPORT_EXPLORER void mmExplorerCommand_Destroy(struct mmExplorerCommand* p)
{
    mmExplorerCommandCallback_Destroy(&p->callback);
    mmVectorValue_Destroy(&p->vector_item);
    p->index = 0;
    p->url_mode = 0;
}

MM_EXPORT_EXPLORER void mmExplorerCommand_SetCallback(struct mmExplorerCommand* p, struct mmExplorerCommandCallback* callback)
{
    assert(NULL != callback && "callback is a null.");
    p->callback = *callback;
}
MM_EXPORT_EXPLORER void mmExplorerCommand_RealBasename(struct mmExplorerCommand* p, struct mmString* _real_basename, const char* f)
{
    if (0 == mmAccess(f, MM_ACCESS_F_OK))
    {
        struct mmString qualified_name;
        struct mmString basename;
        struct mmString suffixname;
        int ac = 0;

        mmString_Init(&qualified_name);
        mmString_Init(&basename);
        mmString_Init(&suffixname);

        mmString_Assigns(&qualified_name, f);

        mmPathSplitSuffixName(&qualified_name, &basename, &suffixname);

        do
        {
            mmString_Clear(&qualified_name);

            p->index++;

            if (mmString_Empty(&suffixname))
            {
                mmString_Sprintf(&qualified_name, "%s(%u)", mmString_CStr(&basename), p->index);
            }
            else
            {
                mmString_Sprintf(&qualified_name, "%s(%u).%s", mmString_CStr(&basename), p->index, mmString_CStr(&suffixname));
            }

            ac = mmAccess(mmString_CStr(&qualified_name), MM_ACCESS_F_OK);
        } while (0 == ac);

        mmString_Assign(_real_basename, &qualified_name);

        mmString_Destroy(&qualified_name);
        mmString_Destroy(&basename);
        mmString_Destroy(&suffixname);
    }
    else
    {
        mmString_Assigns(_real_basename, f);
    }
}
MM_EXPORT_EXPLORER void mmExplorerCommand_Copy(struct mmExplorerCommand* p, struct mmRbtsetVpt* item_select)
{
    __static_mmExplorerCommand_ClearRbtset(p);
    __static_mmExplorerCommand_CopyRbtset(p, item_select);

    p->url_mode = MM_EXPLORERCOMMAND_MODE_COPY;
    p->index = 0;
}
MM_EXPORT_EXPLORER void mmExplorerCommand_Cut(struct mmExplorerCommand* p, struct mmRbtsetVpt* item_select)
{
    __static_mmExplorerCommand_ClearRbtset(p);
    __static_mmExplorerCommand_CopyRbtset(p, item_select);

    p->url_mode = MM_EXPLORERCOMMAND_MODE_CUT;
    p->index = 0;
}
MM_EXPORT_EXPLORER void mmExplorerCommand_Paste(struct mmExplorerCommand* p, const char* f)
{
    if (0 != p->vector_item.size)
    {
        struct mmString _real_fullname;
        struct mmString _real_basename;

        mmString_Init(&_real_fullname);
        mmString_Init(&_real_basename);

        if (MM_EXPLORERCOMMAND_MODE_COPY == p->url_mode)
        {
            size_t index = 0;
            struct mmExplorerItem* item = NULL;

            while (index < p->vector_item.size)
            {
                item = (struct mmExplorerItem*)mmVectorValue_GetIndexReference(&p->vector_item, index);

                mmString_Assigns(&_real_fullname, f);
                mmString_Appends(&_real_fullname, "/");
                mmString_Append(&_real_fullname, &item->basename);

                mmExplorerCommand_RealBasename(p, &_real_basename, mmString_CStr(&_real_fullname));

                mmRecursiveCopy(mmString_CStr(&item->fullname), mmString_CStr(&_real_basename));

                index++;
            }
        }
        else
        {
            size_t index = 0;
            struct mmExplorerItem* item = NULL;

            while (index < p->vector_item.size)
            {
                item = (struct mmExplorerItem*)mmVectorValue_GetIndexReference(&p->vector_item, index);

                mmString_Assigns(&_real_fullname, f);
                mmString_Appends(&_real_fullname, "/");
                mmString_Append(&_real_fullname, &item->basename);

                mmExplorerCommand_RealBasename(p, &_real_basename, mmString_CStr(&_real_fullname));

                mmRename(mmString_CStr(&item->fullname), mmString_CStr(&_real_basename));

                index++;
            }

            __static_mmExplorerCommand_ClearRbtset(p);
            p->url_mode = 0;
        }

        mmString_Destroy(&_real_fullname);
        mmString_Destroy(&_real_basename);
    }
}
MM_EXPORT_EXPLORER void mmExplorerCommand_Rename(struct mmExplorerCommand* p, const char* f, const char* t)
{
    if (0 != mmStrcmp(f, t))
    {
        mmRename(f, t);
    }
}
MM_EXPORT_EXPLORER void mmExplorerCommand_RenameItem(struct mmExplorerCommand* p, struct mmExplorerItem* _item, const char* t)
{
    struct mmString t_filename;

    mmString_Init(&t_filename);

    mmString_Assign(&t_filename, &_item->pathname);
    mmString_Appends(&t_filename, mmString_Empty(&_item->pathname) ? "" : "/");
    mmString_Appends(&t_filename, t);

    mmExplorerCommand_Rename(p, mmString_CStr(&_item->fullname), mmString_CStr(&t_filename));

    mmString_Destroy(&t_filename);
}
MM_EXPORT_EXPLORER void mmExplorerCommand_Remove(struct mmExplorerCommand* p, const char* f)
{
    mmExplorerRemove(f);
}
MM_EXPORT_EXPLORER void mmExplorerCommand_RemoveVector(struct mmExplorerCommand* p, struct mmRbtsetVpt* item_select)
{
    struct mmExplorerItem* e = NULL;
    struct mmRbNode* n = NULL;
    struct mmRbtsetVptIterator* it = NULL;
    //
    // max
    n = mmRb_First(&item_select->rbt);
    while (NULL != n)
    {
        it = (struct mmRbtsetVptIterator*)mmRb_Entry(n, struct mmRbtsetVptIterator, n);
        // next
        n = mmRb_Next(n);

        e = (struct mmExplorerItem*)it->k;

        mmExplorerRemove(mmString_CStr(&e->fullname));
    }
}
MM_EXPORT_EXPLORER void mmExplorerCommand_Mkdir(struct mmExplorerCommand* p, const char* f, int mode)
{
    if (0 != mmAccess(f, MM_ACCESS_F_OK))
    {
        mmMkdir(f, mode);
    }
}
MM_EXPORT_EXPLORER void mmExplorerCommand_Rmdir(struct mmExplorerCommand* p, const char* f)
{
    if (0 == mmAccess(f, MM_ACCESS_F_OK))
    {
        mmRmdir(f);
    }
}
MM_EXPORT_EXPLORER void mmExplorerCommand_Touch(struct mmExplorerCommand* p, const char* f)
{
    if (0 != mmAccess(f, MM_ACCESS_F_OK))
    {
        mmExplorerTouch(f);
    }
}

MM_EXPORT_EXPLORER void mmExplorerCommand_OpenDir(struct mmExplorerCommand* p, struct mmExplorerItem* _item)
{
    (*(p->callback.OpenDir))(p, _item);
}
MM_EXPORT_EXPLORER void mmExplorerCommand_OpenReg(struct mmExplorerCommand* p, struct mmExplorerItem* _item)
{
    (*(p->callback.OpenReg))(p, _item);
}
MM_EXPORT_EXPLORER void mmExplorerCommand_Preview(struct mmExplorerCommand* p, struct mmExplorerItem* _item)
{
    (*(p->callback.Preview))(p, _item);
}
MM_EXPORT_EXPLORER void mmExplorerCommand_Refresh(struct mmExplorerCommand* p, const char* directory)
{
    (*(p->callback.Refresh))(p, directory);
}
//////////////////////////////////////////////////////////////////////////
static void __static_mmExplorerCommand_CopyRbtset(struct mmExplorerCommand* p, struct mmRbtsetVpt* item_select)
{
    struct mmExplorerItem* e = NULL;
    size_t index = 0;
    struct mmExplorerItem* item = NULL;
    struct mmRbNode* n = NULL;
    struct mmRbtsetVptIterator* it = NULL;
    //
    // resize the max size.
    mmVectorValue_AlignedMemory(&p->vector_item, item_select->size);
    // max
    n = mmRb_First(&item_select->rbt);
    while (NULL != n)
    {
        it = (struct mmRbtsetVptIterator*)mmRb_Entry(n, struct mmRbtsetVptIterator, n);
        // next
        n = mmRb_Next(n);

        e = (struct mmExplorerItem*)it->k;

        item = (struct mmExplorerItem*)mmVectorValue_GetIndexReference(&p->vector_item, index);

        mmExplorerItem_SetPath(item, mmString_CStr(&e->pathname), mmString_CStr(&e->basename));
        mmExplorerItem_SetType(item, mmString_CStr(&e->type_real), mmString_CStr(&e->type_image));

        index++;
    }
}
static void __static_mmExplorerCommand_ClearRbtset(struct mmExplorerCommand* p)
{
    mmVectorValue_Clear(&p->vector_item);
}

