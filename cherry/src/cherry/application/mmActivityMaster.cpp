/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmActivityMaster.h"

#include "core/mmAlloc.h"

#include "dish/mmValue.h"

#include "nwsi/mmEventSurface.h"

#include "module/mmModuleRegister.h"

#include "layer/director/mmLayerConfiguration.h"
#include "layer/director/mmLayerFactoryRegister.h"

void mmActivityMaster_Init(struct mmActivityMaster* p)
{
    mmActivityInterface_Init(&p->hSuper);
    p->pContextMaster = NULL;
    p->pSurfaceMaster = NULL;

    mmLayerDirector_Init(&p->hLayerDirector);
        
    p->hSuper.SetContext = &mmActivityMaster_SetContext;
    p->hSuper.SetSurface = &mmActivityMaster_SetSurface;
    p->hSuper.OnStart = &mmActivityMaster_OnStart;
    p->hSuper.OnInterrupt = &mmActivityMaster_OnInterrupt;
    p->hSuper.OnShutdown = &mmActivityMaster_OnShutdown;
    p->hSuper.OnJoin = &mmActivityMaster_OnJoin;
    p->hSuper.OnFinishLaunching = &mmActivityMaster_OnFinishLaunching;
    p->hSuper.OnBeforeTerminate = &mmActivityMaster_OnBeforeTerminate;
    p->hSuper.OnEnterBackground = &mmActivityMaster_OnEnterBackground;
    p->hSuper.OnEnterForeground = &mmActivityMaster_OnEnterForeground;
}
void mmActivityMaster_Destroy(struct mmActivityMaster* p)
{
    mmLayerDirector_Destroy(&p->hLayerDirector);

    p->pSurfaceMaster = NULL;
    p->pContextMaster = NULL;
    mmActivityInterface_Destroy(&p->hSuper);
}
// virtual function for super interface implement.
void mmActivityMaster_SetContext(struct mmActivityInterface* pSuper, struct mmContextMaster* pContextMaster)
{
    struct mmActivityMaster* p = (struct mmActivityMaster*)(pSuper);
    
    p->pContextMaster = pContextMaster;
}
void mmActivityMaster_SetSurface(struct mmActivityInterface* pSuper, struct mmSurfaceMaster* pSurfaceMaster)
{
    struct mmActivityMaster* p = (struct mmActivityMaster*)(pSuper);
    
    p->pSurfaceMaster = pSurfaceMaster;
}
void mmActivityMaster_OnStart(struct mmActivityInterface* pSuper)
{
    struct mmActivityMaster* p = (struct mmActivityMaster*)(pSuper);
}
void mmActivityMaster_OnInterrupt(struct mmActivityInterface* pSuper)
{
    struct mmActivityMaster* p = (struct mmActivityMaster*)(pSuper);
}
void mmActivityMaster_OnShutdown(struct mmActivityInterface* pSuper)
{
    struct mmActivityMaster* p = (struct mmActivityMaster*)(pSuper);
}
void mmActivityMaster_OnJoin(struct mmActivityInterface* pSuper)
{
    struct mmActivityMaster* p = (struct mmActivityMaster*)(pSuper);
}

void mmActivityMaster_OnFinishLaunching(struct mmActivityInterface* pSuper)
{
    struct mmActivityMaster* p = (struct mmActivityMaster*)(pSuper);
    mm::mmModule* pModule = &p->pContextMaster->hModule;

    pModule->SetContextMaster(p->pContextMaster);

    mmLayerDirector_SetContextMaster(&p->hLayerDirector, p->pContextMaster);
    mmLayerDirector_SetSurfaceMaster(&p->hLayerDirector, p->pSurfaceMaster);

    mmModuleFactory_Register(p->pContextMaster);
    mmLayerFactory_Register(&p->hLayerDirector);

    pModule->OnFinishLaunching();

    mmLayerConfiguration_OnFinishLaunching(&p->hLayerDirector);

    mmLayerDirector_OnFinishLaunching(&p->hLayerDirector);

    mmLayerDirector_SetRootWindow(&p->hLayerDirector, "mm/mmLayerRootWindow", "mmLayerRootWindow");
    mmLayerDirector_SceneExclusiveForeground(&p->hLayerDirector, "mm/mmLayerExplorerFinder", "mmLayerExplorerFinder");
}
void mmActivityMaster_OnBeforeTerminate(struct mmActivityInterface* pSuper)
{
    struct mmActivityMaster* p = (struct mmActivityMaster*)(pSuper);
    mm::mmModule* pModule = &p->pContextMaster->hModule;
    
    mmLayerDirector_RootWindowClear(&p->hLayerDirector);

    mmLayerDirector_OnBeforeTerminate(&p->hLayerDirector);

    mmLayerConfiguration_OnBeforeTerminate(&p->hLayerDirector);

    pModule->OnBeforeTerminate();

    mmLayerFactory_Unregister(&p->hLayerDirector);
    mmModuleFactory_Unregister(p->pContextMaster);
}

void mmActivityMaster_OnEnterBackground(struct mmActivityInterface* pSuper)
{
    struct mmActivityMaster* p = (struct mmActivityMaster*)(pSuper);
}
void mmActivityMaster_OnEnterForeground(struct mmActivityInterface* pSuper)
{
    struct mmActivityMaster* p = (struct mmActivityMaster*)(pSuper);
}

static struct mmActivityInterface* __static_mmActivityMaster_Produce(void)
{
    struct mmActivityMaster* p = (struct mmActivityMaster*)mmMalloc(sizeof(struct mmActivityMaster));
    // c++ struct can not internal structure life cycle manual, do it external.
    new (p) struct mmActivityMaster();
    mmActivityMaster_Init(p);
    return (struct mmActivityInterface*)p;
}
static void __static_mmActivityMaster_Recycle(struct mmActivityInterface* m)
{
    struct mmActivityMaster* p = (struct mmActivityMaster*)(m);
    mmActivityMaster_Destroy(p);
    // c++ struct can not internal structure life cycle manual, do it external.
    p->~mmActivityMaster();
    mmFree(p);
}
const struct mmActivityInterfaceCreator MM_ACTIVITYINTERFACECREATOR_MASTER =
{
    __static_mmActivityMaster_Produce,
    __static_mmActivityMaster_Recycle,
};
