/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmActivityMaster_h__
#define __mmActivityMaster_h__

#include "core/mmCore.h"

#include "nwsi/mmContextMaster.h"
#include "nwsi/mmSurfaceMaster.h"

#include "nwsi/mmActivityInterface.h"

#include "OgrePrerequisites.h"

#include "CEGUI/Window.h"

#include "layer/director/mmLayerDirector.h"

#include "nwsi/mmNwsiExport.h"

#include "core/mmPrefix.h"

struct mmActivityMaster
{
    struct mmActivityInterface hSuper;
    
    struct mmContextMaster* pContextMaster;
    struct mmSurfaceMaster* pSurfaceMaster;

    struct mmLayerDirector hLayerDirector;
};
extern void mmActivityMaster_Init(struct mmActivityMaster* p);
extern void mmActivityMaster_Destroy(struct mmActivityMaster* p);
// virtual function for super interface implement.
extern void mmActivityMaster_SetContext(struct mmActivityInterface* pSuper, struct mmContextMaster* pContextMaster);
extern void mmActivityMaster_SetSurface(struct mmActivityInterface* pSuper, struct mmSurfaceMaster* pSurfaceMaster);

extern void mmActivityMaster_OnStart(struct mmActivityInterface* pSuper);
extern void mmActivityMaster_OnInterrupt(struct mmActivityInterface* pSuper);
extern void mmActivityMaster_OnShutdown(struct mmActivityInterface* pSuper);
extern void mmActivityMaster_OnJoin(struct mmActivityInterface* pSuper);

extern void mmActivityMaster_OnFinishLaunching(struct mmActivityInterface* pSuper);
extern void mmActivityMaster_OnBeforeTerminate(struct mmActivityInterface* pSuper);

extern void mmActivityMaster_OnEnterBackground(struct mmActivityInterface* pSuper);
extern void mmActivityMaster_OnEnterForeground(struct mmActivityInterface* pSuper);

extern const struct mmActivityInterfaceCreator MM_ACTIVITYINTERFACECREATOR_MASTER;

#include "core/mmSuffix.h"

#endif//__mmActivityMaster_h__
