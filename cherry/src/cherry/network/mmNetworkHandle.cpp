//#include "mmNetworkHandle.h"
//#include "mmNetworkClient.h"
//
//#include "core/mmLogger.h"
//
//#include "protobuf/mmProtobuffCxx.h"
//#include "protobuf/mmProtobuffCxxNet.h"
//
////#include "application/mm_cherry.h"
////#include "application/mm_cherry_error_code.h"
//
////#include "script/mm_lua_script_error_code.h"
//
////#include "model_data/mm_model_data_logger.h"
////#include "model_data/mm_model_data_tookit.h"
//
//void mmNetworkHandle_TcpFlushSendMessage(struct mmClientTcp* p, mmUInt64_t uid, mmUInt32_t mid, ::google::protobuf::Message *message)
//{
//  struct mmLogger* gLogger = mmLogger_Instance();
//  //mm::mm_cherry* impl = (mm::mm_cherry*)p->u;
//  // note: if state not at ts_motion, is interrupt of shutdown.
//  if (MM_TS_MOTION == p->state)
//  {
//      struct mmString proto_desc;
//      struct mmPacket rq_pack;
//      mmPacket_Init(&rq_pack);
//      mmString_Init(&proto_desc);
//
//      mmProtobufCxx_QClientTcpFlushMessageAppend(p, uid, mid, message, MM_MSG_COMM_HEAD_SIZE, &rq_pack);
//      mmProtobufCxx_QClientTcpFlushSignal(p);
//
//      mmProtobufCxx_LoggerAppendPacketMessage(&proto_desc, &rq_pack, message);
//
//      mmLogger_LogI(gLogger, "%s %d %s", __FUNCTION__, __LINE__, proto_desc.s);
//      mmPacket_Destroy(&rq_pack);
//      mmString_Destroy(&proto_desc);
//  }
//  else
//  {
//        mmLogger_LogI(gLogger, "%s %d mm_client_tcp state not at ts_motion, need do nothing", __FUNCTION__, __LINE__);
//
//      //mm_network_handle_logger_error_push(impl, mm_cherry_error_code_network_anomaly);
//  }
//}
//void mmNetworkHandle_UdpFlushSendMessage(struct mmClientUdp* p, mmUInt64_t uid, mmUInt32_t mid, ::google::protobuf::Message *message, struct mmSockaddr* remote)
//{
//    struct mmLogger* gLogger = mmLogger_Instance();
//    //mm::mm_cherry* impl = (mm::mm_cherry*)p->u;
//  // note: if state not at ts_motion, is interrupt of shutdown.
//  if (MM_TS_MOTION == p->state)
//  {
//        struct mmString proto_desc;
//        struct mmPacket rq_pack;
//        mmPacket_Init(&rq_pack);
//        mmString_Init(&proto_desc);
//
//      mmProtobufCxx_QClientUdpFlushMessageAppend(p, uid, mid, message, MM_MSG_COMM_HEAD_SIZE, &rq_pack, remote);
//      mmProtobufCxx_QClientUdpFlushSignal(p);
//
//      mmProtobufCxx_LoggerAppendPacketMessage(&proto_desc, &rq_pack, message);
//
//      mmLogger_LogI(gLogger, "%s %d %s", __FUNCTION__, __LINE__, proto_desc.s);
//      mmPacket_Destroy(&rq_pack);
//      mmString_Destroy(&proto_desc);
//  }
//  else
//  {
//        mmLogger_LogI(gLogger, "%s %d mm_client_udp state not at ts_motion, need do nothing", __FUNCTION__, __LINE__);
//
//      //mm_network_handle_logger_error_push(impl, mm_cherry_error_code_network_anomaly);
//  }
//}
//extern void mmNetworkHandle_TcpsFlushSendMessage(struct mmNetworkClient* p, mmUInt64_t uid, mmUInt32_t mid, ::google::protobuf::Message* message)
//{
//    struct mmLogger* gLogger = mmLogger_Instance();
//    //mm::mm_cherry* impl = (mm::mm_cherry*)p->u;
//  // note: if state not at ts_motion, is interrupt of shutdown.
//
//  //struct mm_network_client* _network = &impl->d_network;
//
//  if (TcpsState_Finish == p->tcps.state)
//  {
//      mmNetworkHandle_TcpFlushSendMessage(&p->tcp, uid, mid, message);
//  }
//  else
//  {
//        mmLogger_LogI(gLogger, "%s %d mm_client_tcps state not at tcps_state_finish, need do nothing", __FUNCTION__, __LINE__);
//
//      //mm_network_handle_logger_error_push(impl, mm_cherry_error_code_unencrypted_network);
//  }
//}
////void mm_network_handle_logger_error_event(mm::mm_cherry* impl, b_error::info* error_info)
////{
////    mm::mm_model_data_logger* _data_logger = impl->d_data.get_data<mm::mm_model_data_logger>();
////    mm::mm_model_data_tookit* _data_tookit = impl->d_data.get_data<mm::mm_model_data_tookit>();
////    mm::mm_event_data_logger evt_ags;
////    evt_ags.code = error_info->code();
////    evt_ags.desc = error_info->desc();
////    mm_lua_script_error_code_view(_data_tookit->d_lua_context.state, evt_ags.code, evt_ags.view);
////    _data_logger->d_event_set.fire_event(mm::mm_model_data_logger::event_logger_view, evt_ags);
////}
////void mm_network_handle_logger_error_push(mm::mm_cherry* impl, mmUInt32_t error_code)
////{
////    mm::mm_model_data_logger* _data_logger = impl->d_data.get_data<mm::mm_model_data_logger>();
////    mm::mm_model_data_tookit* _data_tookit = impl->d_data.get_data<mm::mm_model_data_tookit>();
////    mm::mm_event_data_logger evt_ags;
////    evt_ags.code = error_code;
////    evt_ags.desc = mm_error_desc_string(&impl->d_error_desc, evt_ags.code);
////    mm_lua_script_error_code_view(_data_tookit->d_lua_context.state, evt_ags.code, evt_ags.view);
////    _data_logger->push(evt_ags);
////}
