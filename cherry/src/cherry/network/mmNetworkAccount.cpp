//#include "mm_network_account.h"
//#include "mm_network_client.h"
//
//#include "core/mm_logger.h"
//
//
//#include "protobuf/mm_protobuff_cxx.h"
//#include "protobuf/mm_protobuff_cxx_net.h"
//
//#include "protodef/cxx/protodef/c_business_account.pb.h"
//
//#include "model_data/mm_model_data_user_basic.h"
//#include "model_data/mm_model_data_logger.h"
//#include "model_data/mm_model_data_tookit.h"
//
//#include "application/mm_cherry.h"
//
//#include "mm_network_handle.h"
//#include "mm_network_lobby.h"
//
//
//extern void hd_n_c_business_account_signed_in_rs(void* obj, void* u, struct mm_packet* pack);
//extern void hd_n_c_business_account_register_rs(void* obj, void* u, struct mm_packet* pack);
//extern void hd_q_c_business_account_signed_in_rs(void* obj, void* u, struct mm_packet* pack);
//extern void hd_q_c_business_account_register_rs(void* obj, void* u, struct mm_packet* pack);
//
//extern void hd_q_c_business_account_search_account_rs(void* obj, void* u, struct mm_packet* pack);
//
//void mm_network_account_callback_function_registration(struct mm_network_client* p)
//{
//  mm_client_tcp_assign_n_callback(&p->tcp, c_business_account::signed_in_rs_msg_id, &hd_n_c_business_account_signed_in_rs);
//  mm_client_tcp_assign_n_callback(&p->tcp, c_business_account::register_rs_msg_id, &hd_n_c_business_account_register_rs);
//  mm_client_tcp_assign_q_callback(&p->tcp, c_business_account::signed_in_rs_msg_id, &hd_q_c_business_account_signed_in_rs);
//  mm_client_tcp_assign_q_callback(&p->tcp, c_business_account::register_rs_msg_id, &hd_q_c_business_account_register_rs);
//
//  mm_client_tcp_assign_q_callback(&p->tcp, c_business_account::search_account_rs_msg_id, &hd_q_c_business_account_search_account_rs);
//}
//
//void hd_n_c_business_account_signed_in_rs(void* obj, void* u, struct mm_packet* pack)
//{
//  c_business_account::signed_in_rs rs_msg;
//  struct mmString proto_desc;
//  b_error::info* error_info = rs_msg.mutable_error();
//
//  struct mmLogger* gLogger = mmLogger_Instance();
//  struct mm_tcp* tcp = (struct mm_tcp*)(obj);
//  mm::mm_cherry* impl = (mm::mm_cherry*)(u);
//  ////////////////////////////////
//  mmString_Init(&proto_desc);
//  //
//  do
//  {
//      // 解包错误
//      if (0 != mm_protobuf_cxx_decode_message(pack, &rs_msg))
//      {
//          mmLogger_LogE(gLogger, "%s %d mid:0x%08X message decode failure.", __FUNCTION__, __LINE__, pack->phead.mid);
//          break;
//      }
//      // logger rq.
//      mm_protobuf_cxx_logger_append_packet_message(&proto_desc, pack, &rs_msg);
//      mmLogger_LogI(gLogger, "%s %d %s", __FUNCTION__, __LINE__, proto_desc.s);
//      //////////////////////////////////////////////////////////////////////////
//      // 回包逻辑错误
//      if (0 != error_info->code())
//      {
//          mmLogger_LogE(gLogger, "%s %d (%d)%s", __FUNCTION__, __LINE__, error_info->code(), error_info->desc().c_str());
//      }
//      else
//      {
//          struct mm_network_client* _network = &impl->d_network;
//          struct mm_lobby_user_token* _lobby_user_token = &_network->lobby_token;
//          //保存登录返回的 token ，并且把网络层的 token 设置为 motion
//          mm_lobby_user_token_lock(_lobby_user_token);
//          _lobby_user_token->uid = rs_msg.user_id();
//          _lobby_user_token->token = rs_msg.token();
//          _lobby_user_token->state = mm_lobby_user_token::user_token_motion;
//          mm_lobby_user_token_unlock(_lobby_user_token);
//          //发 token 到 lobby
//          mm_network_lobby_flush_send_token_verify_rq(&_network->tcp);
//      }
//      //////////////////////////////////////////////////////////////////////////
//  } while (0);
//
//}
//void hd_n_c_business_account_register_rs(void* obj, void* u, struct mm_packet* pack)
//{
//  c_business_account::register_rs rs_msg;
//  struct mmString proto_desc;
//  b_error::info* error_info = rs_msg.mutable_error();
//
//  struct mmLogger* gLogger = mmLogger_Instance();
//  struct mm_tcp* tcp = (struct mm_tcp*)(obj);
//  mm::mm_cherry* impl = (mm::mm_cherry*)(u);
//  ////////////////////////////////
//  mmString_Init(&proto_desc);
//  //
//  do
//  {
//      // 解包错误
//      if (0 != mm_protobuf_cxx_decode_message(pack, &rs_msg))
//      {
//          mmLogger_LogE(gLogger, "%s %d mid:0x%08X message decode failure.", __FUNCTION__, __LINE__, pack->phead.mid);
//          break;
//      }
//      // logger rq.
//      mm_protobuf_cxx_logger_append_packet_message(&proto_desc, pack, &rs_msg);
//      mmLogger_LogI(gLogger, "%s %d %s", __FUNCTION__, __LINE__, proto_desc.s);
//      //////////////////////////////////////////////////////////////////////////
//      // 回包逻辑错误
//      if (0 != error_info->code())
//      {
//          mmLogger_LogE(gLogger, "%s %d (%d)%s", __FUNCTION__, __LINE__, error_info->code(), error_info->desc().c_str());
//      }
//      else
//      {
//          struct mm_network_client* _network = &impl->d_network;
//          struct mm_lobby_user_token* _lobby_user_token = &_network->lobby_token;
//          //保存登录返回的 token ，并且把网络层的 token 设置为 motion
//          mm_lobby_user_token_lock(_lobby_user_token);
//          _lobby_user_token->uid = rs_msg.user_id();
//          _lobby_user_token->token = rs_msg.token();
//          _lobby_user_token->state = mm_lobby_user_token::user_token_motion;
//          mm_lobby_user_token_unlock(_lobby_user_token);
//          //发 token 到 lobby
//          mm_network_lobby_flush_send_token_verify_rq(&_network->tcp);
//      }
//      //////////////////////////////////////////////////////////////////////////
//  } while (0);
//}
//
////tcp/////////////////////////////////////////////////////////////////////////////////
//void hd_q_c_business_account_signed_in_rs(void* obj, void* u, struct mm_packet* pack)
//{
//  c_business_account::signed_in_rs rs_msg;
//  struct mmString proto_desc;
//  b_error::info* error_info = rs_msg.mutable_error();
//
//  struct mmLogger* gLogger = mmLogger_Instance();
//  struct mm_tcp* tcp = (struct mm_tcp*)(obj);
//  mm::mm_cherry* impl = (mm::mm_cherry*)(u);
//  ////////////////////////////////
//  mmString_Init(&proto_desc);
//  //
//  mm::mm_model_data_user_basic* _data_user_basic = impl->d_data.get_data<mm::mm_model_data_user_basic>();
//  do
//  {
//      // 解包错误
//      if (0 != mm_protobuf_cxx_decode_message(pack, &rs_msg))
//      {
//          mmLogger_LogE(gLogger, "%s %d mid:0x%08X message decode failure.", __FUNCTION__, __LINE__, pack->phead.mid);
//          break;
//      }
//      // logger rq.
//      mm_protobuf_cxx_logger_append_packet_message(&proto_desc, pack, &rs_msg);
//      mmLogger_LogI(gLogger, "%s %d %s", __FUNCTION__, __LINE__, proto_desc.s);
//      //////////////////////////////////////////////////////////////////////////
//      // 回包逻辑错误
//      if (0 != error_info->code())
//      {
//          mmLogger_LogE(gLogger, "%s %d (%d)%s", __FUNCTION__, __LINE__, error_info->code(), error_info->desc().c_str());
//          mm_network_handle_logger_error_event(impl, error_info);
//      }
//      else
//      {       
//          //数据更新
//          _data_user_basic->d_basic.name = rs_msg.user_name();
//          _data_user_basic->d_basic.id = rs_msg.user_id();
//          _data_user_basic->d_basic.state = mm::mm_data_user_basic::user_basic_motion;
//          //////////////////////////////////////////////////////////////////////////
//          //数据更新以后的事件发布    发布内容  evt_ags
//          mm_event_args evt_ags;
//          _data_user_basic->d_event_set.fire_event(mm::mm_model_data_user_basic::event_userdata_user_basic_update, evt_ags);
//      }
//      //////////////////////////////////////////////////////////////////////////
//  } while (0);
//  
//}
//void hd_q_c_business_account_register_rs(void* obj, void* u, struct mm_packet* pack)
//{
//  c_business_account::register_rs rs_msg;
//  struct mmString proto_desc;
//  b_error::info* error_info = rs_msg.mutable_error();
//
//  struct mmLogger* gLogger = mmLogger_Instance();
//  struct mm_tcp* tcp = (struct mm_tcp*)(obj);
//  mm::mm_cherry* impl = (mm::mm_cherry*)(u);
//  ////////////////////////////////
//  mmString_Init(&proto_desc);
//  do
//  {
//      // 解包错误
//      if (0 != mm_protobuf_cxx_decode_message(pack, &rs_msg))
//      {
//          mmLogger_LogE(gLogger, "%s %d mid:0x%08X message decode failure.", __FUNCTION__, __LINE__, pack->phead.mid);
//          break;
//      }
//      // logger rq.
//      mm_protobuf_cxx_logger_append_packet_message(&proto_desc, pack, &rs_msg);
//      mmLogger_LogI(gLogger, "%s %d %s", __FUNCTION__, __LINE__, proto_desc.s);
//      //////////////////////////////////////////////////////////////////////////
//      // 回包逻辑错误
//      mm::mm_model_data_user_basic* _data_user_basic = impl->d_data.get_data<mm::mm_model_data_user_basic>();
//      if (0 != error_info->code())
//      {
//          mmLogger_LogE(gLogger, "%s %d (%d)%s", __FUNCTION__, __LINE__, error_info->code(), error_info->desc().c_str());
//          mm_network_handle_logger_error_event(impl, error_info);
//      }
//      else
//      {
//          //数据更新
//          _data_user_basic->d_basic.name = rs_msg.user_name();
//          _data_user_basic->d_basic.id = rs_msg.user_id();
//          _data_user_basic->d_basic.state = mm::mm_data_user_basic::user_basic_motion;
//          //////////////////////////////////////////////////////////////////////////
//          //数据更新以后的事件发布    发布内容  evt_ags
//          mm_event_args evt_ags;
//          _data_user_basic->d_event_set.fire_event(mm::mm_model_data_user_basic::event_userdata_user_basic_update, evt_ags);
//          //////////////////////////////////////////////////////////////////////////
//      }
//  } while (0);
//
//}
//
//
//void hd_q_c_business_account_search_account_rs(void* obj, void* u, struct mm_packet* pack)
//{
//  c_business_account::search_account_rs rs_msg;
//  struct mmString proto_desc;
//  b_error::info* error_info = rs_msg.mutable_error();
//
//  struct mmLogger* gLogger = mmLogger_Instance();
//  struct mm_tcp* tcp = (struct mm_tcp*)(obj);
//  mm::mm_cherry* impl = (mm::mm_cherry*)(u);
//  ////////////////////////////////
//  mmString_Init(&proto_desc);
//  do
//  {
//      // 解包错误
//      if (0 != mm_protobuf_cxx_decode_message(pack, &rs_msg))
//      {
//          mmLogger_LogE(gLogger, "%s %d mid:0x%08X message decode failure.", __FUNCTION__, __LINE__, pack->phead.mid);
//          break;
//      }
//      // logger rq.
//      mm_protobuf_cxx_logger_append_packet_message(&proto_desc, pack, &rs_msg);
//      mmLogger_LogI(gLogger, "%s %d %s", __FUNCTION__, __LINE__, proto_desc.s);
//      //////////////////////////////////////////////////////////////////////////
//      // 回包逻辑错误
//      mm::mm_model_data_user_basic* _data_user_basic = impl->d_data.get_data<mm::mm_model_data_user_basic>();
//      if (0 != error_info->code())
//      {
//          mmLogger_LogE(gLogger, "%s %d (%d)%s", __FUNCTION__, __LINE__, error_info->code(), error_info->desc().c_str());
//          mm_network_handle_logger_error_event(impl, error_info);
//      }
//      else
//      {
//          {
//              typedef ::google::protobuf::RepeatedPtrField< ::b_business_account::user_info > b_user_info_v;
//              b_user_info_v* user_info_s = rs_msg.mutable_user_info_s();
//              int index = user_info_s->size();
//              //先清空现有map(组数据 和好友数据)，遍历传回数据并添加进map
//              _data_user_basic->d_friend_search.clear();
//              b_user_info_v::iterator it;
//              for (it = user_info_s->begin(); it != user_info_s->end(); it++)
//              {
//                  mm::mm_data_basic_friend_info* e = _data_user_basic->d_friend_search.add(it->user_id());
//                  e->id = it->user_id();
//                  e->name = it->user_name();
//                  e->nick = it->user_nick();
//                  e->create_time = it->create_time();
//              }
//          }
//          ////////////////////////////////////////////////////////////////////////////
//          //数据更新以后的事件发布    发布内容  evt_ags
//          mm_event_args evt_ags;
//          _data_user_basic->d_event_set.fire_event(mm::mm_model_data_user_basic::event_data_search_friend_basic_update, evt_ags);
//          ////////////////////////////////////////////////////////////////////////////
//      }
//  } while (0);
//}