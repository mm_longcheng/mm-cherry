//#include "mmNetworkClient.h"
//#include "mmNetworkEntry.h"
//#include "mmNetworkAccount.h"
//#include "mmNetworkLobby.h"
//#include "mmNetworkState.h"
//
//#include "core/mmLogger.h"
//
//#include "net/mmDefaultHandle.h"
//
//static void __static_mmNetworkClient_MsecSendUdpHandle(struct mm_timer_heap* timer_heap, struct mm_timer_entry* entry);
//
//void mmNetworkTcpsState_Init(struct mmNetworkTcpsState* p)
//{
//  p->public_key = "";
//  mmOpensslRsa_Init(&p->openssl_rsa_client);
//  mmOpensslRsa_Init(&p->openssl_rsa_server);
//  mmOpensslRc4_Init(&p->openssl_rc4);
//  mmOpensslTcpContext_Init(&p->openssl_tcp_context);
//  p->state = TcpsState_Closed;
//  mmOpensslRc4_Srand(&p->openssl_rc4, (mmUInt64_t)time(NULL));
//}
//
//void mmNetworkTcpsState_Destroy(struct mmNetworkTcpsState *p)
//{
//  p->public_key = "";
//    mmOpensslRsa_Destroy(&p->openssl_rsa_client);
//    mmOpensslRsa_Destroy(&p->openssl_rsa_server);
//    mmOpensslRc4_Destroy(&p->openssl_rc4);
//    mmOpensslTcpContext_Destroy(&p->openssl_tcp_context);
//  p->state = TcpsState_Closed;
//}
//
//void mmLobbyUserToken_Init(struct mmLobbyUserToken* p)
//{
//  p->uid = 0;
//  p->token = "";
//  mmSpinlock_Init(&p->locker, NULL);
//  p->state = UserToken_Closed;
//}
//void mmLobbyUserToken_Destroy(struct mmLobbyUserToken* p)
//{
//  p->uid = 0;
//  p->token = "";
//  mmSpinlock_Destroy(&p->locker);
//  p->state = UserToken_Closed;
//}
//
//void mmLobbyUserToken_Lock(struct mmLobbyUserToken* p)
//{
//  mmSpinlock_Lock(&p->locker);
//}
//void mmLobbyUserToken_Unlock(struct mmLobbyUserToken* p)
//{
//  mmSpinlock_Unlock(&p->locker);
//}
//
//void mmNetworkClient_Init(struct mmNetworkClient* p)
//{
//  struct mmClientUdpCallback hClientUdpCallback;
//    struct mmClientTcpCallback hClientTcpCallback;
//    struct mmCryptoCallback hCryptoCallback;
//
//  mmTimer_Init(&p->timer);
//  mmClientUdp_Init(&p->udp);
//  mmClientTcp_Init(&p->tcp);
//  mmNetworkTcpsState_Init(&p->tcps);
//  mmLobbyUserToken_Init(&p->lobby_token);
//  p->timecode_address_udp = 0;
//  p->timecode_address_tcp = 0;
//
//    hClientUdpCallback.Handle = &mmClientUdp_QHandleDefault;
//    hClientUdpCallback.obj = p;
//    mmClientUdp_SetQDefaultCallback(&p->udp, &hClientUdpCallback);
//
//    hClientUdpCallback.Handle = &mmClientUdp_NHandleDefault;
//    hClientUdpCallback.obj = p;
//    mmClientUdp_SetNDefaultCallback(&p->udp, &hClientUdpCallback);
//
//    hClientTcpCallback.Handle = &mmClientTcp_QHandleDefault;
//    hClientTcpCallback.obj = p;
//  mmClientTcp_SetQDefaultCallback(&p->tcp, &hClientTcpCallback);
//
//    hClientTcpCallback.Handle = &mmClientTcp_NHandleDefault;
//    hClientTcpCallback.obj = p;
//    mmClientTcp_SetNDefaultCallback(&p->tcp, &hClientTcpCallback);
//
//
//    hCryptoCallback.Encrypt = &mmOpensslTcpContext_MailboxCryptoEncrypt;
//    hCryptoCallback.Decrypt = &mmOpensslTcpContext_MailboxCryptoDecrypt;
//    hCryptoCallback.obj = p;
//  mmClientTcp_SetCryptoCallback(&p->tcp, &hCryptoCallback);
//
//    mmClientTcp_SetAddrContext(&p->tcp, &p->tcps.openssl_tcp_context);
//
//  mmNetworkState_CallbackFunctionRegistration(p);
//    mmNetworkEntry_CallbackFunctionRegistration(p);
//    mmNetworkLobby_CallbackFunctionRegistration(p);
//    mmNetworkAccount_CallbackFunctionRegistration(p);
//
//    mmClientUdp_SetStateCheckFlag(&p->udp, MM_CLIENT_UDP_CHECK_INACTIVE);
//  mmClientTcp_SetStateCheckFlag(&p->tcp, MM_CLIENT_TCP_CHECK_INACTIVE);
//
//    mmTimer_Schedule(&p->timer, 5000, 5000, &__static_mmNetworkClient_MsecSendUdpHandle, p);
//}
//void mmNetworkClient_Destroy(struct mmNetworkClient* p)
//{
//  mmLobbyUserToken_Destroy(&p->lobby_token);
//  mmNetworkTcpsState_Destroy(&p->tcps);
//  mmClientTcp_Destroy(&p->tcp);
//  mmClientUdp_Destroy(&p->udp);
//  mmTimer_Destroy(&p->timer);
//  p->timecode_address_udp = 0;
//  p->timecode_address_tcp = 0;
//}
////////////////////////////////////////////////////////////////////////////
//void mmNetworkClient_SetContext(struct mmNetworkClient* p, void* u)
//{
//  mmClientUdp_SetContext(&p->udp, u);
//  mmClientTcp_SetContext(&p->tcp, u);
//}
//void mmNetworkClient_SetEntryAddress(struct mmNetworkClient* p, const char* node, mmUShort_t port)
//{
//    // Set the udp entry address and port
//    mmNetworkClient_SetUdpRemoteTarget(p, node, port);
//    // Post entry change event
//    mmNetworkState_EntryEventPublish(p, node, port);
//}
//void mmNetworkClient_SetUdpRemoteTarget(struct mmNetworkClient* p, const char* node, mmUShort_t port)
//{
//  mmUInt64_t current_usec = mmTime_CurrentUSec();
//  struct mmSocket* socket = &p->udp.net_udp.udp.socket;
//  // If the addresses are inconsistent or the address times out, do an address reset
//  if (0 != mmSockaddr_CompareAddress(&socket->ss_remote, node, port) || 
//      MM_ADDRESS_INVALID_TIMEOUT < current_usec - p->timecode_address_udp)
//  {
//      mmClientUdp_SetRemote(&p->udp, node, port);
//      // Status monitoring thread set the udp tag in the ready state
//        mmClientUdp_SetStateCheckFlag(&p->udp, MM_CLIENT_UDP_CHECK_ACTIVATE);
//      // Let the network thread activate immediately, and do a test
//        mmClientUdp_StateSignal(&p->udp);
//      // Update udp address timestamp
//      p->timecode_address_udp = current_usec;
//  }
//}
//void mmNetworkClient_SetTcpRemoteTarget(struct mmNetworkClient* p, const char* node, mmUShort_t port)
//{
//    mmUInt64_t current_usec = mmTime_CurrentUSec();
//    struct mmSocket* socket = &p->udp.net_udp.udp.socket;
//    // If the addresses are inconsistent or the address times out, do an address reset
//  if (0 != mmSockaddr_CompareAddress(&socket->ss_remote, node, port) || 
//      MM_ADDRESS_INVALID_TIMEOUT < current_usec - p->timecode_address_udp)
//  {
//      // Turn off tcp auto reconnect
//      mmClientTcp_SetStateCheckFlag(&p->tcp, MM_CLIENT_TCP_CHECK_INACTIVE);
//      // Disconnect the current tcp socket
//      mmClientTcp_ShutdownSocket(&p->tcp);
//      // Attach a new remote address to tcp
//      mmClientTcp_SetRemote(&p->tcp, node, port);
//      // Status monitoring thread Set the tcp tag in the ready state
//      mmClientTcp_SetStateCheckFlag(&p->tcp, MM_CLIENT_TCP_CHECK_ACTIVATE);
//      // Let the network thread activate immediately, and do a test
//      mmClientTcp_StateSignal(&p->tcp);
//      // Update the tcp address timestamp
//      p->timecode_address_tcp = current_usec;
//  }
//}
//void mmNetworkClient_ThreadHandleRecv(struct mmNetworkClient* p)
//{
//  mmClientUdp_ThreadHandleRecv(&p->udp);
//  mmClientTcp_ThreadHandleRecv(&p->tcp);
//}
////////////////////////////////////////////////////////////////////////////
//// start wait thread.
//void mmNetworkClient_Start(struct mmNetworkClient* p)
//{
//  mmTimer_Start(&p->timer);
//  mmClientUdp_Start(&p->udp);
//  mmClientTcp_Start(&p->tcp);
//}
//// interrupt wait thread.
//void mmNetworkClient_Interrupt(struct mmNetworkClient* p)
//{
//  mmTimer_Interrupt(&p->timer);
//  mmClientUdp_Interrupt(&p->udp);
//  mmClientTcp_Interrupt(&p->tcp);
//}
//// shutdown wait thread.
//void mmNetworkClient_Shutdown(struct mmNetworkClient* p)
//{
//  mmTimer_Shutdown(&p->timer);
//  mmClientUdp_Shutdown(&p->udp);
//  mmClientTcp_Shutdown(&p->tcp);
//}
//// join wait thread.
//void mmNetworkClient_Join(struct mmNetworkClient* p)
//{
//  mmTimer_Join(&p->timer);
//  mmClientUdp_Join(&p->udp);
//  mmClientTcp_Join(&p->tcp);
//}
////////////////////////////////////////////////////////////////////////////
//static void __static_mmNetworkClient_MsecSendUdpHandle(struct mmTimerHeap* timer_heap, struct mmTimerEntry* entry)
//{
//  struct mmNetworkClient* p = (struct mmNetworkClient*)(entry->callback.obj);
//  mmNetworkClient_TryPullTcpAddress(p);
//}
//
//void mmNetworkClient_TryPullTcpAddress(struct mmNetworkClient* p)
//{
//  int tcp_state = mmClientTcp_FinallyState(&p->tcp);
//  int udp_state = mmClientUdp_FinallyState(&p->udp);
//
//  mmUInt64_t current_usec = mmTime_CurrentUSec();
//  struct mmClientTcp* pClientTcp = &p->tcp;
//  struct mmSocket* socket = &pClientTcp->net_tcp.tcp.socket;
//
//  // 1. tcp 0, udp 1
//  // 2. tcp timeout
//  // 3. tcp Address is not just updated
//  if ((0 != tcp_state && 0 == udp_state) &&
//      (MM_ADDRESS_INVALID_TIMEOUT < current_usec - p->timecode_address_tcp) &&
//      (0 == pClientTcp->ss_native_update && 0 == pClientTcp->ss_remote_update))
//  {
//      struct mmLogger* gLogger = mmLogger_Instance();
//
//      mmNetwork_EntryFlushSendKnockRQ&p->udp);
//
//      mmLogger_LogI(gLogger, "%s %d", __FUNCTION__, __LINE__);
//  }
//}
//void mmNetworkClient_OnUpdate(struct mmNetworkClient* p, double interval)
//{
//  mmNetworkClient_ThreadHandleRecv(p);
//}
