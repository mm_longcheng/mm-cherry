//#include "mm_network_state.h"
//#include "mm_network_entry.h"
//#include "mm_network_lobby.h"
//
//#include "application/mm_cherry.h"
//
//#include "model_data/mm_model_data_net.h"
//
//static void __static_net_udp_handle_udp_mid_broken_n(void* obj, void* u, struct mm_packet* pack, struct mm_sockaddr* remote);
//static void __static_net_udp_handle_udp_mid_nready_n(void* obj, void* u, struct mm_packet* pack, struct mm_sockaddr* remote);
//static void __static_net_udp_handle_udp_mid_finish_n(void* obj, void* u, struct mm_packet* pack, struct mm_sockaddr* remote);
//
//static void __static_net_udp_handle_udp_mid_broken_q(void* obj, void* u, struct mm_packet* pack, struct mm_sockaddr* remote);
//static void __static_net_udp_handle_udp_mid_nready_q(void* obj, void* u, struct mm_packet* pack, struct mm_sockaddr* remote);
//static void __static_net_udp_handle_udp_mid_finish_q(void* obj, void* u, struct mm_packet* pack, struct mm_sockaddr* remote);
//
//static void __static_net_tcp_handle_tcp_mid_broken_n(void* obj, void* u, struct mm_packet* pack);
//static void __static_net_tcp_handle_tcp_mid_nready_n(void* obj, void* u, struct mm_packet* pack);
//static void __static_net_tcp_handle_tcp_mid_finish_n(void* obj, void* u, struct mm_packet* pack);
//
//static void __static_net_tcp_handle_tcp_mid_broken_q(void* obj, void* u, struct mm_packet* pack);
//static void __static_net_tcp_handle_tcp_mid_nready_q(void* obj, void* u, struct mm_packet* pack);
//static void __static_net_tcp_handle_tcp_mid_finish_q(void* obj, void* u, struct mm_packet* pack);
////////////////////////////////////////////////////////////////////////////
//static void __static_net_udp_event_entry_socket_change_publishing(void* obj, void* u);
////////////////////////////////////////////////////////////////////////////
//void mm_network_state_callback_function_registration(struct mm_network_client* p)
//{
//  //client_udp
//  mm_client_udp_assign_n_callback(&p->udp, udp_mid_broken, &__static_net_udp_handle_udp_mid_broken_n);
//  mm_client_udp_assign_n_callback(&p->udp, udp_mid_nready, &__static_net_udp_handle_udp_mid_nready_n);
//  mm_client_udp_assign_n_callback(&p->udp, udp_mid_finish, &__static_net_udp_handle_udp_mid_finish_n);
//
//  mm_client_udp_assign_q_callback(&p->udp, udp_mid_broken, &__static_net_udp_handle_udp_mid_broken_q);
//  mm_client_udp_assign_q_callback(&p->udp, udp_mid_nready, &__static_net_udp_handle_udp_mid_nready_q);
//  mm_client_udp_assign_q_callback(&p->udp, udp_mid_finish, &__static_net_udp_handle_udp_mid_finish_q);
//
//
//  //client_tcp
//  mm_client_tcp_assign_n_callback(&p->tcp, tcp_mid_broken, &__static_net_tcp_handle_tcp_mid_broken_n);
//  mm_client_tcp_assign_n_callback(&p->tcp, tcp_mid_nready, &__static_net_tcp_handle_tcp_mid_nready_n);
//  mm_client_tcp_assign_n_callback(&p->tcp, tcp_mid_finish, &__static_net_tcp_handle_tcp_mid_finish_n);
//
//  mm_client_tcp_assign_q_callback(&p->tcp, tcp_mid_broken, &__static_net_tcp_handle_tcp_mid_broken_q);
//  mm_client_tcp_assign_q_callback(&p->tcp, tcp_mid_nready, &__static_net_tcp_handle_tcp_mid_nready_q);
//  mm_client_tcp_assign_q_callback(&p->tcp, tcp_mid_finish, &__static_net_tcp_handle_tcp_mid_finish_q);
//}
//
//void mm_network_state_entry_event_publish(struct mm_network_client* p, const char* ip, unsigned short port)
//{
//  mm::mm_cherry* impl = (mm::mm_cherry*)(p->udp.u);
//  //数据更新
//  mm::mm_model_data_net* _data_net = impl->d_data.get_data<mm::mm_model_data_net>();
//  mm::mm_data_net_address_state* _entry = &_data_net->entry;
//  _entry->ip = ip;
//  _entry->port = port;
//  //
//  //数据更新以后的事件发布    发布内容  evt_ags
//  mm_event_args evt_ags;
//  _entry->d_event_set.fire_event(mm::mm_data_net_address_state::event_ip_port_update, evt_ags);
//}
//void mm_network_state_lobby_event_publish(struct mm_network_client* p, const char* ip, unsigned short port)
//{
//  mm::mm_cherry* impl = (mm::mm_cherry*)(p->tcp.u);
//  //数据更新
//  mm::mm_model_data_net* _data_net = impl->d_data.get_data<mm::mm_model_data_net>();
//  mm::mm_data_net_address_state* _lobby = &_data_net->lobby;
//  _lobby->ip = ip;
//  _lobby->port = port;
//  //
//  //数据更新以后的事件发布    发布内容  evt_ags
//  mm_event_args evt_ags;
//  _lobby->d_event_set.fire_event(mm::mm_data_net_address_state::event_ip_port_update, evt_ags);
//
//}
//////////////////////////////////////////////////////////////////////////////
////static udp
//static void __static_net_udp_event_entry_socket_change_publishing(void* obj, void* u)
//{
//  struct mm_udp* udp = (struct mm_udp*)(obj);
//  struct mm_net_udp* net_udp = (struct mm_net_udp*)(udp->callback.obj);
//
//  //数据更新以后的事件发布    发布内容  evt_ags
//  mm::mm_cherry* impl = (mm::mm_cherry*)(u);
//  mm::mm_model_data_net* _data_net = impl->d_data.get_data<mm::mm_model_data_net>();
//  mm::mm_data_net_address_state* _entry = &_data_net->entry;
//  _entry->socket_state = net_udp->udp_state;
//  mm_event_args evt_ags;
//  _entry->d_event_set.fire_event(mm::mm_data_net_address_state::event_socket_update, evt_ags);
//}
//static void __static_net_tcp_event_lobby_socket_change_publishing(void* obj, void* u)
//{
//  struct mm_udp* tcp = (struct mm_udp*)(obj);
//  struct mm_net_tcp* net_tcp = (struct mm_net_tcp*)(tcp->callback.obj);
//
//  //数据更新以后的事件发布    发布内容  evt_ags
//  mm::mm_cherry* impl = (mm::mm_cherry*)(u);
//  mm::mm_model_data_net* _data_net = impl->d_data.get_data<mm::mm_model_data_net>();
//  mm::mm_data_net_address_state* _lobby = &_data_net->lobby;
//  _lobby->socket_state = net_tcp->tcp_state;
//  mm_event_args evt_ags;
//  _lobby->d_event_set.fire_event(mm::mm_data_net_address_state::event_socket_update, evt_ags);
//}
//
//static void __static_net_udp_handle_udp_mid_broken_n(void* obj, void* u, struct mm_packet* pack, struct mm_sockaddr* remote)
//{
//  // udp 网络线程触发
//  __static_net_udp_event_entry_socket_change_publishing(obj, u);
//}
//static void __static_net_udp_handle_udp_mid_nready_n(void* obj, void* u, struct mm_packet* pack, struct mm_sockaddr* remote)
//{
//  // udp 网络线程触发
//  __static_net_udp_event_entry_socket_change_publishing(obj, u);
//}
//static void __static_net_udp_handle_udp_mid_finish_n(void* obj, void* u, struct mm_packet* pack, struct mm_sockaddr* remote)
//{
//  mm::mm_cherry* impl = (mm::mm_cherry*)(u);
//  struct mm_udp* udp = (struct mm_udp*)(obj);
//  struct mm_net_udp* net_udp = (struct mm_net_udp*)(udp->callback.obj);
//  struct mm_client_udp* client_udp = (struct mm_client_udp*)(net_udp->callback.obj);
//
//  // 关闭 udp 自动重连
//  mm_client_udp_assign_state_check_flag(&impl->d_network.udp, client_udp_check_inactive);
//
//  // udp 网络线程触发
//  __static_net_udp_event_entry_socket_change_publishing(obj, u);
//
//  // udp 连接完成后发送一次udp业务：获取大厅IP信息  knock_rq 请求
//  // 尝试拉取udp地址.
//  mm_network_client_try_pull_tcp_address(&impl->d_network);
//}
//
//static void __static_net_udp_handle_udp_mid_broken_q(void* obj, void* u, struct mm_packet* pack, struct mm_sockaddr* remote)
//{
//  // udp 网络线程触发
//  __static_net_udp_event_entry_socket_change_publishing(obj, u);
//}
//static void __static_net_udp_handle_udp_mid_nready_q(void* obj, void* u, struct mm_packet* pack, struct mm_sockaddr* remote)
//{
//  // udp 队列线程触发
//  __static_net_udp_event_entry_socket_change_publishing(obj, u);
//}
//static void __static_net_udp_handle_udp_mid_finish_q(void* obj, void* u, struct mm_packet* pack, struct mm_sockaddr* remote)
//{
//  // udp 队列线程触发
//  __static_net_udp_event_entry_socket_change_publishing(obj, u);
//}
////////////////////////////////////////////////////////////////////////////
////static tcp
//static void __static_net_tcp_handle_tcp_mid_broken_n(void* obj, void* u, struct mm_packet* pack)
//{
//  // tcp 网络线程触发
//  mm::mm_cherry* impl = (mm::mm_cherry*)(u);
//  struct mm_network_client* _network = &impl->d_network;
//  struct mm_network_tcps_state* _tcps = &_network->tcps;
//  struct mm_tcp* _tcp = &_network->tcp.net_tcp.tcp;
//  struct mm_openssl_tcp_context* p_openssl_tcp_context = &_tcps->openssl_tcp_context;
//
//  //清除 lobby公钥
//  _tcps->public_key.clear();
//  //清除 network tcps state , 更新变为关闭
//  _tcps->state = mm_network_tcps_state::tcps_state_closed;
//  //对称加密 状态取消
//  mm_openssl_tcp_context_lock(p_openssl_tcp_context);
//  mm_openssl_tcp_context_assign_state(p_openssl_tcp_context, CRYPTO_CONTEXT_INACTIVE);
//  mm_openssl_tcp_context_unlock(p_openssl_tcp_context);
//
//  // tcp连接断开后 再做一次 udp业务请求： 获取大厅IP信息  knock_rq 请求
//  mm_network_client_try_pull_tcp_address(_network);
//}
//static void __static_net_tcp_handle_tcp_mid_nready_n(void* obj, void* u, struct mm_packet* pack)
//{
//  // tcp 网络线程触发
//}
//static void __static_net_tcp_handle_tcp_mid_finish_n(void* obj, void* u, struct mm_packet* pack)
//{
//  // tcp 网络线程触发
//  mm::mm_cherry* impl = (mm::mm_cherry*)(u);
//  struct mm_network_client* _network = &impl->d_network;
//  // 关闭 tcp 自动重连
//  mm_client_tcp_assign_state_check_flag(&_network->tcp, client_tcp_check_inactive);
//  // tcp 连接完成后；立即请求交换 秘钥
//  mm_network_lobby_flush_send_exchange_key_rq(&_network->tcp);
//}
//
//static void __static_net_tcp_handle_tcp_mid_broken_q(void* obj, void* u, struct mm_packet* pack)
//{
//  // tcp 队列线程触发
//  mm::mm_cherry* impl = (mm::mm_cherry*)(u);
//  mm::mm_model_data_net* _data_net = impl->d_data.get_data<mm::mm_model_data_net>();
//  struct mm_network_client* _network = &impl->d_network;
//  struct mm_network_tcps_state* _tcps = &_network->tcps;
//  mm::mm_data_net_address_state* _lobby = &_data_net->lobby;
//
//  // 网络状态变更 事件分发
//  __static_net_tcp_event_lobby_socket_change_publishing(obj, u);
//
//  //数据更新
//  //队列更新网络线程完成的 tcps 状态
//  _lobby->crypto_state = _tcps->state;
//  //发布队列更新 tcps 状态 事件
//  mm_event_args evt_ags;
//  _lobby->d_event_set.fire_event(mm::mm_data_net_address_state::event_crypto_update, evt_ags);
//}
//static void __static_net_tcp_handle_tcp_mid_nready_q(void* obj, void* u, struct mm_packet* pack)
//{
//  // tcp 队列线程触发
//  __static_net_tcp_event_lobby_socket_change_publishing(obj, u);
//}
//static void __static_net_tcp_handle_tcp_mid_finish_q(void* obj, void* u, struct mm_packet* pack)
//{
//  // tcp 队列线程触发
//  __static_net_tcp_event_lobby_socket_change_publishing(obj, u);
//}
