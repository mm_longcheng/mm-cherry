//#ifndef __NetworkHandle_h__
//#define __NetworkHandle_h__
//
//#include "core/mmCore.h"
//#include <google/protobuf/message.h>
//
//#include "protodef/BError.pb.h"
//
////namespace mm
////{
////    class mm_cherry;
////}
//
//extern void mmNetworkHandle_TcpFlushSendMessage(struct mmClientTcp* p, mmUInt64_t uid, mmUInt32_t mid, ::google::protobuf::Message* message);
//extern void mmNetworkHandle_UdpFlushSendMessage(struct mmClientUdp* p, mmUInt64_t uid, mmUInt32_t mid, ::google::protobuf::Message* message, struct mmSockaddr* remote);
//
//extern void mmNetworkHandle_TcpsFlushSendMessage(struct mmNetworkClient* p, mmUInt64_t uid, mmUInt32_t mid, ::google::protobuf::Message* message);
//
////extern void mm_network_handle_logger_error_event(mm::mm_cherry* impl, b_error::info* error_info);
////extern void mm_network_handle_logger_error_push(mm::mm_cherry* impl, mmUInt32_t error_code);
//
//#endif//__NetworkHandle_h__