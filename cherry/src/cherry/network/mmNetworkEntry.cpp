//#include "mmNetworkEntry.h"
//#include "mmNetworkClient.h"
//#include "mmNetworkHandle.h"
//#include "mmNetworkState.h"
//
//#include "core/mmLogger.h"
//
//#include "protobuf/mmProtobuffCxx.h"
//#include "protobuf/mmProtobuffCxxNet.h"
//
//#include "protodef/CShuttleEntry.pb.h"
//
////#include "model_data/mm_model_data_logger.h"
////
////#include "application/mm_cherry.h"
//
//extern void hd_N_CShuttleEntry_KnockRS(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);
//extern void hd_Q_CShuttleEntry_KnockRS(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);
//
//extern void mmNetworkEntry_CallbackFunctionRegistration(struct mmNetworkClient* p)
//{
//  mmClientUdp_SetNHandle(&p->udp, CShuttleEntry::KnockRS_Msg_ID, &hd_N_CShuttleEntry_KnockRS);
//  mmClientUdp_SetQHandle(&p->udp, CShuttleEntry::KnockRS_Msg_ID, &hd_Q_CShuttleEntry_KnockRS);
//}
//
//void mmNetworkEntry_FlushSendKnockRQ(struct mmClientUdp* p)
//{
//    CShuttleEntry::KnockRQ rq;
//  BMath::Coord* coord_info = rq.mutable_coord_info();
//    coord_info->set_j(0);
//    coord_info->set_w(0);
//    rq.set_native_client_version("");
//    rq.set_native_source_version("");
//
//  mmNetworkHandle_UdpFlushSendMessage(p,0 , CShuttleEntry::KnockRQ_Msg_ID, &rq, &p->net_udp.udp.socket.ss_remote);
//}
//
//void hd_N_CShuttleEntry_KnockRS(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote)
//{
//    CShuttleEntry::KnockRS rs;
//  struct mmString proto_desc;
//  BError::Info* error = rs.mutable_error();
//
//  struct mmLogger* gLogger = mmLogger_Instance();
//  struct mm_udp* udp = (struct mm_udp*)(obj);
//  //mm::mm_cherry* impl = (mm::mm_cherry*)(u);
//  ////////////////////////////////
//    mmString_Init(&proto_desc);
//  
//  do
//  {
//
//      // 解包错误
//      if (0 != mmProtobufCxx_DecodeMessage(pack, &rs))
//      {
//            mmLogger_LogE(gLogger, "%s %d mid:0x%08X message decode failure.", __FUNCTION__, __LINE__, pack->phead.mid);
//          break;
//      }
//      // logger rq.
//      mmProtobufCxx_LoggerAppendPacketMessage(&proto_desc, pack, &rs_msg);
//        mmLogger_LogI(gLogger, "%s %d %s", __FUNCTION__, __LINE__, proto_desc.s);
//      //////////////////////////////////////////////////////////////////////////
//      // 回包逻辑错误       
//      if (0 != error->code())
//      {
//            mmLogger_LogE(gLogger, "%s %d (%d)%s", __FUNCTION__, __LINE__, error->code(), error->desc().c_str());
//      }
//      else
//      {   
//          const BNetwork::Address& g = rs_msg.addr();
//          //数据更新
//          struct mm_network_client* _network = &impl->d_network;
//          struct mm_openssl_rsa* _rsa_server = &_network->tcps.openssl_rsa_server;
//          //存放entry 返回的rsa公钥
//          const std::string& server_rsa_public_key = rs_msg.public_key();
//          //发送和接收是两个线程，都使用了rsa
//          mm_openssl_rsa_lock(_rsa_server);
//          mm_openssl_rsa_pub_mem_set(_rsa_server, (mmUInt8_t*)server_rsa_public_key.data(), 0, server_rsa_public_key.size());
//          mm_openssl_rsa_pub_mem_to_ctx(_rsa_server);
//          mm_openssl_rsa_unlock(_rsa_server);
//          mmLogger_LogI(gLogger, "%s %d lobby_address %s:%d", __FUNCTION__, __LINE__, g.host().c_str(), g.port());
//          //////////////////////////////////////////////////////////////////////////
//          //设置大厅 IP 和 端口
//          mm_network_client_assign_tcp_remote_target(_network, g.host().c_str(), g.port());
//      }
//  } while (0);
//}
//
//void hd_Q_CShuttleEntry_KnockRS(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote)
//{
//  c_shuttle_entry::knock_rs rs_msg;
//  struct mmString proto_desc;
//  b_error::info* error_info = rs_msg.mutable_error();
//
//  struct mmLogger* gLogger = mmLogger_Instance();
//  struct mm_udp* udp = (struct mm_udp*)(obj);
//  mm::mm_cherry* impl = (mm::mm_cherry*)(u);
//  ////////////////////////////////
//  mmString_Init(&proto_desc);
//  
//  do
//  {
//
//      // 解包错误
//      if (0 != mm_protobuf_cxx_decode_message(pack, &rs_msg))
//      {
//          mmLogger_LogE(gLogger, "%s %d mid:0x%08X message decode failure.", __FUNCTION__, __LINE__, pack->phead.mid);
//          break;
//      }
//      // logger rq.
//      mm_protobuf_cxx_logger_append_packet_message(&proto_desc, pack, &rs_msg);
//      mmLogger_LogI(gLogger, "%s %d %s", __FUNCTION__, __LINE__, proto_desc.s);
//      //////////////////////////////////////////////////////////////////////////
//      // 回包逻辑错误
//      
//      if (0 != error_info->code())
//      {
//          mmLogger_LogE(gLogger, "%s %d (%d)%s", __FUNCTION__, __LINE__, error_info->code(), error_info->desc().c_str()); 
//          mm_network_handle_logger_error_event(impl, error_info);
//      }
//      else
//      {   
//          struct mm_network_client* _network = &impl->d_network;
//          const b_network::address& g = rs_msg.addr();
//          mm_network_state_lobby_event_publish(_network,g.host().c_str(),g.port());
//          //////////////////////////////////////////////////////////////////////////
//      }       
//  } while (0);
//}
