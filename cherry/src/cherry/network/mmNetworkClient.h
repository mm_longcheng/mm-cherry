//#ifndef __mmNetworkClient_h__
//#define __mmNetworkClient_h__
//
//#include "core/mmTimer.h"
//
//#include "net/mmClientTcp.h"
//#include "net/mmClientUdp.h"
//
//#include "openssl/mmOpensslTcpContext.h"
//#include "openssl/mmOpensslRsa.h"
//#include "openssl/mmOpensslRc4.h"
//
//#include <string>
//
//enum mmTcpsState_t
//{
//  TcpsState_Closed = 0,// tcps is closed,Unencrypted packages are not allowed to be sent.
//  TcpsState_Finish = 1,// tcps already completed,Unencrypted packages are allowed to be sent.
//};
//
//struct mmNetworkTcpsState
//{
//  //服务端 openssl_rsa_server 的公钥
//  std::string public_key;
//  struct mmOpensslRsa openssl_rsa_client;
//  struct mmOpensslRsa openssl_rsa_server;
//  struct mmOpensslRc4 openssl_rc4;
//  struct mmOpensslTcpContext openssl_tcp_context;
//  int state;
//};
//void mmNetworkTcpsState_Init(struct mmNetworkTcpsState *p);
//void mmNetworkTcpsState_Destroy(struct mmNetworkTcpsState *p);
//
//
//enum mmLobbyUserTokenState_t
//{
//  UserToken_Closed = 0,// user token is closed
//  UserToken_Motion = 1,// user token is implementing
//  UserToken_Finish = 2,// user token has already completed.
//};
//struct mmLobbyUserToken
//{
//  mmUInt64_t uid;
//  std::string token;// token.
//  mmAtomic_t locker;
//  int state;
//};
//
//void mmLobbyUserToken_Init(struct mmLobbyUserToken* p);
//void mmLobbyUserToken_Destroy(struct mmLobbyUserToken* p);
////
//void mmLobbyUserToken_Lock(struct mmLobbyUserToken* p);
//void mmLobbyUserToken_Unlock(struct mmLobbyUserToken* p);
//
//#define MM_NETWORKCLIENT_RSA_LENGTH 1024
//#define MM_NETWORKCLIENT_RC4_LENGTH 16
//
//#define MM_ADDRESS_INVALID_TIMEOUT 500000
//
//struct mmNetworkClient
//{
//  struct mmTimer timer;
//  struct mmClientUdp udp; 
//  struct mmClientTcp tcp;
//  struct mmNetworkTcpsState tcps;
//  struct mmLobbyUserToken lobby_token;
//  // cache timecode.
//  mmUInt64_t timecode_address_udp;
//    mmUInt64_t timecode_address_tcp;
//};
//
//extern void mmNetworkClient_Init(struct mmNetworkClient* p);
//extern void mmNetworkClient_Destroy(struct mmNetworkClient* p);
//
//extern void mmNetworkClient_SetContext(struct mmNetworkClient* p, void* u);
//extern void mmNetworkClient_SetEntryAddress(struct mmNetworkClient* p, const char* node, mmUShort_t port);
//extern void mmNetworkClient_SetUdpRemoteTarget(struct mmNetworkClient* p, const char* node, mmUShort_t port);
//extern void mmNetworkClient_SetTcpRemoteTarget(struct mmNetworkClient* p, const char* node, mmUShort_t port);
//
////主线程（渲染线程）update 需要调用          ----------功能：把网络数据（网络线程）弹回主线程
//extern void mmNetworkClient_ThreadHandleRecv(struct mmNetworkClient* p);
////尝试拉取tcp远端地址
//extern void mmNetworkClient_TryPullTcpAddress(struct mmNetworkClient* p);
//
//extern void mmNetworkClient_OnUpdate(struct mmNetworkClient* p, double interval);
//
//// start wait thread.
//extern void mmNetworkClient_Start(struct mmNetworkClient* p);
//// interrupt wait thread.
//extern void mmNetworkClient_Interrupt(struct mmNetworkClient* p);
//// shutdown wait thread.
//extern void mmNetworkClient_Shutdown(struct mmNetworkClient* p);
//// join wait thread.
//extern void mmNetworkClient_Join(struct mmNetworkClient* p);
//
//#endif//__mmNetworkClient_h__