/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmIconv_h__
#define __mmIconv_h__

#include "core/mmCore.h"
#include "core/mmString.h"
#include "core/mmStreambuf.h"

#include "container/mmRbtreeString.h"

#include "iconv.h"

#include "core/mmPrefix.h"

struct mmByteBuffer;

struct mmIconv
{
    struct mmString hFormatF;
    struct mmString hFormatT;
    struct mmStreambuf hStreambuf;
    iconv_t pContext;
};
void mmIconv_Init(struct mmIconv* p);
void mmIconv_Destroy(struct mmIconv* p);

void mmIconv_SetFormatF(struct mmIconv* p, const char* pFormatF);
void mmIconv_SetFormatT(struct mmIconv* p, const char* pFormatT);

void mmIconv_CreateContext(struct mmIconv* p);
void mmIconv_DeleteContext(struct mmIconv* p);
void mmIconv_UpdateContext(struct mmIconv* p);

// this api will auto create context.
void mmIconv_UpdateFormat(struct mmIconv* p, const char* pFormatT, const char* pFormatF);

size_t mmIconv_Transform(struct mmIconv* p, mmUInt8_t* i_buff, size_t i_size);
size_t mmIconv_TransformLength(struct mmIconv* p);
mmUInt8_t* mmIconv_TransformBuffer(struct mmIconv* p);

struct mmIconvCodePage
{
    mmUInt32_t hCodePage;
    const char* pAbbreviation;
    const char* pFullName;
};

extern struct mmIconvCodePage MM_ICONV_CODE_PAGE_VECTOR[];

// locale code_page information.
const struct mmIconvCodePage* mmIconvCodePage_Information(mmUInt32_t hCodePage);
// current locale code_page.
mmUInt32_t mmIconv_LocaleCodePage(void);
// current locale format.
const char* mmIconv_LocaleFormat(void);

// most of time we need view <==> host
// view default is UTF-8
// host default is GBK
struct mmIconvContext
{
    struct mmString hFormatView;
    struct mmString hFormatHost;
    struct mmIconv hIconvViewToHost;
    struct mmIconv hIconvHostToView;
    struct mmRbtreeStringVpt hRbtree;
};
void mmIconvContext_Init(struct mmIconvContext* p);
void mmIconvContext_Destroy(struct mmIconvContext* p);
void mmIconvContext_UpdateFormat(struct mmIconvContext* p, const char* pFormatHost, const char* pFormatView);
void mmIconvContext_UpdateFormatLocale(struct mmIconvContext* p, const char* pFormatView);
// conversion
void mmIconvContext_ViewToHost(struct mmIconvContext* p, struct mmByteBuffer* i, struct mmByteBuffer* o);
void mmIconvContext_HostToView(struct mmIconvContext* p, struct mmByteBuffer* i, struct mmByteBuffer* o);
//
void mmIconvContext_Cachelist(struct mmIconvContext* p);
mmBool_t mmIconvContext_IsValidFormat(struct mmIconvContext* p, const char* format);

// iconvlist.
typedef int(*mmIconvContextListDoOneFunc) (unsigned int namescount, const char* const* names, void* data);
void mmIconvContext_List(mmIconvContextListDoOneFunc func, void* data);

#include "core/mmSuffix.h"

#endif//__mmIconv_h__
