/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmWindowsTarget.h"

#include "core/mmLogger.h"

#include "nwsi/mmCEGUISystem.h"

#include "OgreRenderTexture.h"
#include "OgreHardwareBuffer.h"
#include "OgreHardwarePixelBuffer.h"
#include "OgreTextureManager.h"
#include "OgreRenderTarget.h"

#include "CEGUI/WindowManager.h"
#include "CEGUI/ImageManager.h"
#include "CEGUI/TextureTarget.h"

#include "CEGUIOgreRenderer/Texture.h"
#include "CEGUIOgreRenderer/TextureTarget.h"

const char* MM_WINDOWSTARGET_CEGUI_RTT_IMAGERY = "mmWindowsTarget_CEGUIRTTImagery_";

void mmWindowsTarget_Init(struct mmWindowsTarget* p)
{
    p->pCEGUISystem = NULL;
    mmString_Init(&p->hName);
    p->pImageWindow = NULL;

    p->pCEGUITextureTarget = NULL;

    p->pCEGUITexture = NULL;
    p->pImage = NULL;

    p->pOgreTexture = Ogre::TexturePtr();
    p->pRenderTarget = NULL;

    p->hActive = 1;

    mmString_Assigns(&p->hName, "mm");
}
void mmWindowsTarget_Destroy(struct mmWindowsTarget* p)
{
    p->pCEGUISystem = NULL;
    mmString_Destroy(&p->hName);
    p->pImageWindow = NULL;

    p->pCEGUITextureTarget = NULL;

    p->pCEGUITexture = NULL;
    p->pImage = NULL;

    p->pOgreTexture = Ogre::TexturePtr();
    p->pRenderTarget = NULL;

    p->hActive = 1;
}

void mmWindowsTarget_OnFinishLaunching(struct mmWindowsTarget* p)
{
    assert(NULL != p->pImageWindow && "p->pImageWindow is null.");

    struct mmLogger* gLogger = mmLogger_Instance();

    mmLogger_LogV(gLogger, "%s %d begin.", __FUNCTION__, __LINE__);

    const CEGUI::Rectf& hOuterRectClipper = p->pImageWindow->getOuterRectClipper();

    struct mmString hImageName;

    float hWinW = hOuterRectClipper.getWidth();
    float hWinH = hOuterRectClipper.getHeight();
    hWinW = hWinW < 1 ? 1 : hWinW;
    hWinH = hWinH < 1 ? 1 : hWinH;

    mmString_Init(&hImageName);
    mmString_Assigns(&hImageName, MM_WINDOWSTARGET_CEGUI_RTT_IMAGERY);
    mmString_Append(&hImageName, &p->hName);

    CEGUI::CEGUIOgreRenderer* pRenderer = p->pCEGUISystem->pRenderer;
    p->pCEGUITextureTarget = pRenderer->createTextureTarget();
    p->pCEGUITextureTarget->declareRenderSize(CEGUI::Sizef(hWinW, hWinH));
    p->pCEGUITexture = &p->pCEGUITextureTarget->getTexture();
    CEGUI::CEGUIOgreTexture* pCEGUIOgreTexture = static_cast<CEGUI::CEGUIOgreTexture*>(p->pCEGUITexture);
    p->pOgreTexture = pCEGUIOgreTexture->getOgreTexture();
    CEGUI::CEGUIOgreTextureTarget* pCEGUIOgreTextureTarget = static_cast<CEGUI::CEGUIOgreTextureTarget*>(p->pCEGUITextureTarget);
    p->pRenderTarget = pCEGUIOgreTextureTarget->getOgreRenderTarget();

    CEGUI::ImageManager* pImageManager = CEGUI::ImageManager::getSingletonPtr();
    p->pImage = static_cast<CEGUI::BasicImage*>(&pImageManager->create("BasicImage", mmString_CStr(&hImageName)));
    p->pImage->setTexture(p->pCEGUITexture);

    // note : if render target is a rtt,the actual size is roundup 2^n, not the real display size.
    // here we center it to image display size.
    float hTextureRealW = (float)p->pOgreTexture->getWidth();
    float hTextureRealH = (float)p->pOgreTexture->getHeight();
    float hRealRatio = hTextureRealW / hTextureRealH;
    float hViewRatio = hWinW / hWinH;
    float hW = 0;
    float hH = 0;

    if (hRealRatio < hViewRatio)
    {
        hW = 0;
        hH = (hTextureRealH - hTextureRealW / hViewRatio) / 2.0f;
    }
    else
    {
        hW = (hTextureRealW - hTextureRealH * hViewRatio) / 2.0f;
        hH = 0;
    }

    CEGUI::Rectf hImageArea(hW, hH, hTextureRealW - hW, hTextureRealH - hH);
    p->pImage->setArea(hImageArea);
    p->pImage->setAutoScaled(CEGUI::ASM_Disabled);

    p->pImageWindow->setProperty("Image", p->pImage->getName());
    //
    mmWindowsTarget_UpdateActive(p);
    // Note: We update render target Manual.
    p->pRenderTarget->setAutoUpdated(false);
    
    mmString_Destroy(&hImageName);

    mmLogger_LogV(gLogger, "%s %d end.", __FUNCTION__, __LINE__);
}
void mmWindowsTarget_OnBeforeTerminate(struct mmWindowsTarget* p)
{
    CEGUI::ImageManager* pImageManager = CEGUI::ImageManager::getSingletonPtr();
    pImageManager->destroy(*p->pImage);
    CEGUI::CEGUIOgreRenderer* pRenderer = p->pCEGUISystem->pRenderer;
    pRenderer->destroyTextureTarget(p->pCEGUITextureTarget);
    p->pCEGUITextureTarget = NULL;
    p->pOgreTexture.reset();
}

void mmWindowsTarget_OnUpdate(struct mmWindowsTarget* p, double interval)
{
    p->pRenderTarget->update(false);
    p->pRenderTarget->swapBuffers();
    p->pImageWindow->invalidate();
}
// set active state.
void mmWindowsTarget_SetActive(struct mmWindowsTarget* p, mmUInt8_t hActive)
{
    assert(NULL != p->pRenderTarget && "p->pRenderTarget is a null.");
    p->hActive = hActive;
}
void mmWindowsTarget_UpdateActive(struct mmWindowsTarget* p)
{
    const CEGUI::Rectf& hOuterRectClipper = p->pImageWindow->getOuterRectClipper();
    float hWinW = hOuterRectClipper.getWidth();
    float hWinH = hOuterRectClipper.getHeight();
    p->pRenderTarget->setActive(p->hActive && 0 != hWinW && 0 != hWinH);
}
