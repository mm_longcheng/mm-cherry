/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmWindowsTarget_h__
#define __mmWindowsTarget_h__

#include "core/mmCore.h"

#include "nwsi/mmCEGUISystem.h"

#include "OgreRenderTarget.h"
#include "OgreTexture.h"

#include "CEGUI/Window.h"
#include "CEGUI/Texture.h"
#include "CEGUI/BasicImage.h"

#include <string>

#include "core/mmPrefix.h"

// render scene to image.
extern const char* MM_WINDOWSTARGET_CEGUI_RTT_IMAGERY;

struct mmWindowsTarget
{
    // weak ref.
    struct mmCEGUISystem* pCEGUISystem;
    // Ogre::TextureManager windows_target_ogre_rtt_texture_%s
    // CEGUI::OgreRenderer  windows_target_cegui_rtt_texture_%s
    // CEGUI::ImageManager  windows_target_cegui_rtt_image_%s
    // strong ref.name pattern for rtt resource.default is "mm".
    struct mmString hName;
    // weak ref.
    CEGUI::Window* pImageWindow;
    //
    CEGUI::TextureTarget* pCEGUITextureTarget;
    // strong ref.
    CEGUI::Texture* pCEGUITexture;
    // strong ref.
    CEGUI::BasicImage* pImage;
    // strong ref.
    Ogre::TexturePtr pOgreTexture;
    // weak ref. is target to d_ogre_texture.
    Ogre::RenderTarget* pRenderTarget;
    // default is 1.
    mmUInt8_t hActive;
};

extern void mmWindowsTarget_Init(struct mmWindowsTarget* p);
extern void mmWindowsTarget_Destroy(struct mmWindowsTarget* p);

extern void mmWindowsTarget_OnFinishLaunching(struct mmWindowsTarget* p);
extern void mmWindowsTarget_OnBeforeTerminate(struct mmWindowsTarget* p);

extern void mmWindowsTarget_OnUpdate(struct mmWindowsTarget* p, double interval);

// set active state.
extern void mmWindowsTarget_SetActive(struct mmWindowsTarget* p, mmUInt8_t hActive);
// update windows target whether need active.
extern void mmWindowsTarget_UpdateActive(struct mmWindowsTarget* p);

#include "core/mmSuffix.h"

#endif//__mmWindowsTarget_h__