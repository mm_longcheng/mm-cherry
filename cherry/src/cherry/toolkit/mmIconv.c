/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmIconv.h"

#include "core/mmByte.h"
#include "core/mmLogger.h"
#include "core/mmValueTransform.h"

#include "dish/mmFilePath.h"

struct mmIconvCodePage MM_ICONV_CODE_PAGE_VECTOR[] =
{
    { 37    , "IBM037"                  , "IBM EBCDIC (US-Canada)"                      , },
    { 437   , "IBM437"                  , "OEM United States"                           , },
    { 500   , "IBM500"                  , "IBM EBCDIC (International)"                  , },
    { 708   , "ASMO-708"                , "Arabic (ASMO 708)"                           , },
    { 720   , "DOS-720"                 , "Arabic (DOS)"                                , },
    { 737   , "ibm737"                  , "Greek (DOS)"                                 , },
    { 775   , "ibm775"                  , "Baltic (DOS)"                                , },
    { 850   , "ibm850"                  , "Western European (DOS)"                      , },
    { 852   , "ibm852"                  , "Central European (DOS)"                      , },
    { 855   , "IBM855"                  , "OEM Cyrillic"                                , },
    { 857   , "ibm857"                  , "Turkish (DOS)"                               , },
    { 858   , "IBM00858"                , "OEM Multilingual Latin I"                    , },
    { 860   , "IBM860"                  , "Portuguese (DOS)"                            , },
    { 861   , "ibm861"                  , "Icelandic (DOS)"                             , },
    { 862   , "DOS-862"                 , "Hebrew (DOS)"                                , },
    { 863   , "IBM863"                  , "French Canadian (DOS)"                       , },
    { 864   , "IBM864"                  , "Arabic (864)"                                , },
    { 865   , "IBM865"                  , "Nordic (DOS)"                                , },
    { 866   , "cp866"                   , "Cyrillic (DOS)"                              , },
    { 869   , "ibm869"                  , "Greek, Modern (DOS)"                         , },
    { 870   , "IBM870"                  , "IBM EBCDIC (Multilingual Latin-2)"           , },
    { 874   , "windows-874"             , "Thai (Windows)"                              , },
    { 875   , "cp875"                   , "IBM EBCDIC (Greek Modern)"                   , },
    { 932   , "shift_jis"               , "Japanese (Shift-JIS)"                        , },
    { 936   , "gb2312"                  , "Chinese Simplified (GB2312)"                 , },
    { 949   , "ks_c_5601-1987"          , "Korean"                                      , },
    { 950   , "big5"                    , "Chinese Traditional (Big5)"                  , },
    { 1026  , "IBM1026"                 , "IBM EBCDIC (Turkish Latin-5)"                , },
    { 1047  , "IBM01047"                , "IBM Latin-1"                                 , },
    { 1140  , "IBM01140"                , "IBM EBCDIC (US-Canada-Euro)"                 , },
    { 1141  , "IBM01141"                , "IBM EBCDIC (Germany-Euro)"                   , },
    { 1142  , "IBM01142"                , "IBM EBCDIC (Denmark-Norway-Euro)"            , },
    { 1143  , "IBM01143"                , "IBM EBCDIC (Finland-Sweden-Euro)"            , },
    { 1144  , "IBM01144"                , "IBM EBCDIC (Italy-Euro)"                     , },
    { 1145  , "IBM01145"                , "IBM EBCDIC (Spain-Euro)"                     , },
    { 1146  , "IBM01146"                , "IBM EBCDIC (UK-Euro)"                        , },
    { 1147  , "IBM01147"                , "IBM EBCDIC (France-Euro)"                    , },
    { 1148  , "IBM01148"                , "IBM EBCDIC (International-Euro)"             , },
    { 1149  , "IBM01149"                , "IBM EBCDIC (Icelandic-Euro)"                 , },
    { 1200  , "utf-16"                  , "Unicode"                                     , },
    { 1201  , "unicodeFFFE"             , "Unicode (Big-Endian)"                        , },
    { 1250  , "windows-1250"            , "Central European (Windows)"                  , },
    { 1251  , "windows-1251"            , "Cyrillic (Windows)"                          , },
    { 1252  , "Windows-1252"            , "Western European (Windows)"                  , },
    { 1253  , "windows-1253"            , "Greek (Windows)"                             , },
    { 1254  , "windows-1254"            , "Turkish (Windows)"                           , },
    { 1255  , "windows-1255"            , "Hebrew (Windows)"                            , },
    { 1256  , "windows-1256"            , "Arabic (Windows)"                            , },
    { 1257  , "windows-1257"            , "Baltic (Windows)"                            , },
    { 1258  , "windows-1258"            , "Vietnamese (Windows)"                        , },
    { 1361  , "Johab"                   , "Korean (Johab)"                              , },
    { 10000 , "macintosh"               , "Western European (Mac)"                      , },
    { 10001 , "x-mac-japanese"          , "Japanese (Mac)"                              , },
    { 10002 , "x-mac-chinesetrad"       , "Chinese Traditional (Mac)"                   , },
    { 10003 , "x-mac-korean"            , "Korean (Mac)"                                , },
    { 10004 , "x-mac-arabic"            , "Arabic (Mac)"                                , },
    { 10005 , "x-mac-hebrew"            , "Hebrew (Mac)"                                , },
    { 10006 , "x-mac-greek"             , "Greek (Mac)"                                 , },
    { 10007 , "x-mac-cyrillic"          , "Cyrillic (Mac)"                              , },
    { 10008 , "x-mac-chinesesimp"       , "Chinese Simplified (Mac)"                    , },
    { 10010 , "x-mac-romanian"          , "Romanian (Mac)"                              , },
    { 10017 , "x-mac-ukrainian"         , "Ukrainian (Mac)"                             , },
    { 10021 , "x-mac-thai"              , "Thai (Mac)"                                  , },
    { 10029 , "x-mac-ce"                , "Central European (Mac)"                      , },
    { 10079 , "x-mac-icelandic"         , "Icelandic (Mac)"                             , },
    { 10081 , "x-mac-turkish"           , "Turkish (Mac)"                               , },
    { 10082 , "x-mac-croatian"          , "Croatian (Mac)"                              , },
    { 20000 , "x-Chinese-CNS"           , "Chinese Traditional (CNS)"                   , },
    { 20001 , "x-cp20001"               , "TCA Taiwan"                                  , },
    { 20002 , "x-Chinese-Eten"          , "Chinese Traditional (Eten)"                  , },
    { 20003 , "x-cp20003"               , "IBM5550 Taiwan"                              , },
    { 20004 , "x-cp20004"               , "TeleText Taiwan"                             , },
    { 20005 , "x-cp20005"               , "Wang Taiwan"                                 , },
    { 20105 , "x-IA5"                   , "Western European (IA5)"                      , },
    { 20106 , "x-IA5-German"            , "German (IA5)"                                , },
    { 20107 , "x-IA5-Swedish"           , "Swedish (IA5)"                               , },
    { 20108 , "x-IA5-Norwegian"         , "Norwegian (IA5)"                             , },
    { 20127 , "us-ascii"                , "US-ASCII"                                    , },
    { 20261 , "x-cp20261"               , "T.61"                                        , },
    { 20269 , "x-cp20269"               , "ISO-6937"                                    , },
    { 20273 , "IBM273"                  , "IBM EBCDIC (Germany)"                        , },
    { 20277 , "IBM277"                  , "IBM EBCDIC (Denmark-Norway)"                 , },
    { 20278 , "IBM278"                  , "IBM EBCDIC (Finland-Sweden)"                 , },
    { 20280 , "IBM280"                  , "IBM EBCDIC (Italy)"                          , },
    { 20284 , "IBM284"                  , "IBM EBCDIC (Spain)"                          , },
    { 20285 , "IBM285"                  , "IBM EBCDIC (UK)"                             , },
    { 20290 , "IBM290"                  , "IBM EBCDIC (Japanese katakana)"              , },
    { 20297 , "IBM297"                  , "IBM EBCDIC (France)"                         , },
    { 20420 , "IBM420"                  , "IBM EBCDIC (Arabic)"                         , },
    { 20423 , "IBM423"                  , "IBM EBCDIC (Greek)"                          , },
    { 20424 , "IBM424"                  , "IBM EBCDIC (Hebrew)"                         , },
    { 20833 , "x-EBCDIC-KoreanExtended" , "IBM EBCDIC (Korean Extended)"                , },
    { 20838 , "IBM-Thai"                , "IBM EBCDIC (Thai)"                           , },
    { 20866 , "koi8-r"                  , "Cyrillic (KOI8-R)"                           , },
    { 20871 , "IBM871"                  , "IBM EBCDIC (Icelandic)"                      , },
    { 20880 , "IBM880"                  , "IBM EBCDIC (Cyrillic Russian)"               , },
    { 20905 , "IBM905"                  , "IBM EBCDIC (Turkish)"                        , },
    { 20924 , "IBM00924"                , "IBM Latin-1"                                 , },
    { 20932 , "EUC-JP"                  , "Japanese (JIS 0208-1990 and 0212-1990)"      , },
    { 20936 , "x-cp20936"               , "Chinese Simplified (GB2312-80)"              , },
    { 20949 , "x-cp20949"               , "Korean Wansung"                              , },
    { 21025 , "cp1025"                  , "IBM EBCDIC (Cyrillic Serbian-Bulgarian)"     , },
    { 21866 , "koi8-u"                  , "Cyrillic (KOI8-U)"                           , },
    { 28591 , "iso-8859-1"              , "Western European (ISO)"                      , },
    { 28592 , "iso-8859-2"              , "Central European (ISO)"                      , },
    { 28593 , "iso-8859-3"              , "Latin 3 (ISO)"                               , },
    { 28594 , "iso-8859-4"              , "Baltic (ISO)"                                , },
    { 28595 , "iso-8859-5"              , "Cyrillic (ISO)"                              , },
    { 28596 , "iso-8859-6"              , "Arabic (ISO)"                                , },
    { 28597 , "iso-8859-7"              , "Greek (ISO)"                                 , },
    { 28598 , "iso-8859-8"              , "Hebrew (ISO-Visual)"                         , },
    { 28599 , "iso-8859-9"              , "Turkish (ISO)"                               , },
    { 28603 , "iso-8859-13"             , "Estonian (ISO)"                              , },
    { 28605 , "iso-8859-15"             , "Latin 9 (ISO)"                               , },
    { 29001 , "x-Europa"                , "Europa"                                      , },
    { 38598 , "iso-8859-8-i"            , "Hebrew (ISO-Logical)"                        , },
    { 50220 , "iso-2022-jp"             , "Japanese (JIS)"                              , },
    { 50221 , "csISO2022JP"             , "Japanese (JIS-Allow 1 byte Kana)"            , },
    { 50222 , "iso-2022-jp"             , "Japanese (JIS-Allow 1 byte Kana - SO/SI)"    , },
    { 50225 , "iso-2022-kr"             , "Korean (ISO)"                                , },
    { 50227 , "x-cp50227"               , "Chinese Simplified (ISO-2022)"               , },
    { 51932 , "euc-jp"                  , "Japanese (EUC)"                              , },
    { 51936 , "EUC-CN"                  , "Chinese Simplified (EUC)"                    , },
    { 51949 , "euc-kr"                  , "Korean (EUC)"                                , },
    { 52936 , "hz-gb-2312"              , "Chinese Simplified (HZ)"                     , },
    { 54936 , "GB18030"                 , "Chinese Simplified (GB18030)"                , },
    { 57002 , "x-iscii-de"              , "ISCII Devanagari"                            , },
    { 57003 , "x-iscii-be"              , "ISCII Bengali"                               , },
    { 57004 , "x-iscii-ta"              , "ISCII Tamil"                                 , },
    { 57005 , "x-iscii-te"              , "ISCII Telugu"                                , },
    { 57006 , "x-iscii-as"              , "ISCII Assamese"                              , },
    { 57007 , "x-iscii-or"              , "ISCII Oriya"                                 , },
    { 57008 , "x-iscii-ka"              , "ISCII Kannada"                               , },
    { 57009 , "x-iscii-ma"              , "ISCII Malayalam"                             , },
    { 57010 , "x-iscii-gu"              , "ISCII Gujarati"                              , },
    { 57011 , "x-iscii-pa"              , "ISCII Punjabi"                               , },
    { 65000 , "utf-7"                   , "Unicode (UTF-7)"                             , },
    { 65001 , "utf-8"                   , "Unicode (UTF-8)"                             , },
    { 65005 , "utf-32"                  , "Unicode (UTF-32)"                            , },
    { 65006 , "utf-32BE"                , "Unicode (UTF-32 Big-Endian)"                 , },
};

static const size_t __IconvCodePageSize = MM_ARRAY_SIZE(MM_ICONV_CODE_PAGE_VECTOR);

static size_t __static_mmIconvCodePage_InformationBinarySerach(size_t o, size_t l, mmUInt32_t key)
{
    size_t mid = 0;
    size_t left = o;
    size_t right = o + l - 1;
    mmSInt32_t result = 0;

    if (0 == l)
    {
        return (size_t)-1;
    }
    else
    {
        while (left <= right && (size_t)(-1) != right)
        {
            // Avoid overflow problems
            mid = left + (right - left) / 2;

            result = MM_ICONV_CODE_PAGE_VECTOR[mid].hCodePage - key;

            if (result > 0)
            {
                right = mid - 1;
            }
            else if (result < 0)
            {
                left = mid + 1;
            }
            else
            {
                return mid;
            }
        }

        return -1;
    }
}

void mmIconv_Init(struct mmIconv* p)
{
    mmString_Init(&p->hFormatF);
    mmString_Init(&p->hFormatT);
    mmStreambuf_Init(&p->hStreambuf);
    p->pContext = (iconv_t)-1;

    mmString_Assigns(&p->hFormatF, "GBK");
    mmString_Assigns(&p->hFormatT, "UTF-8");
}
void mmIconv_Destroy(struct mmIconv* p)
{
    mmIconv_DeleteContext(p);

    mmString_Destroy(&p->hFormatF);
    mmString_Destroy(&p->hFormatT);
    mmStreambuf_Destroy(&p->hStreambuf);
    p->pContext = (iconv_t)-1;
}

void mmIconv_SetFormatF(struct mmIconv* p, const char* pFormatF)
{
    mmString_Assigns(&p->hFormatF, pFormatF);
}
void mmIconv_SetFormatT(struct mmIconv* p, const char* pFormatT)
{
    mmString_Assigns(&p->hFormatT, pFormatT);
}

void mmIconv_CreateContext(struct mmIconv* p)
{
    if ((iconv_t)-1 == p->pContext)
    {
        p->pContext = iconv_open(mmString_CStr(&p->hFormatT), mmString_CStr(&p->hFormatF));
    }
}
void mmIconv_DeleteContext(struct mmIconv* p)
{
    if ((iconv_t)-1 != p->pContext)
    {
        iconv_close(p->pContext);
        p->pContext = (iconv_t)-1;
    }
}
void mmIconv_UpdateContext(struct mmIconv* p)
{
    mmIconv_DeleteContext(p);
    mmIconv_CreateContext(p);
}

void mmIconv_UpdateFormat(struct mmIconv* p, const char* pFormatT, const char* pFormatF)
{
    if ((iconv_t)-1 == p->pContext ||
        0 != mmString_CompareCStr(&p->hFormatF, pFormatF) ||
        0 != mmString_CompareCStr(&p->hFormatT, pFormatT))
    {
        mmString_Assigns(&p->hFormatF, pFormatF);
        mmString_Assigns(&p->hFormatT, pFormatT);

        mmIconv_UpdateContext(p);
    }
}

size_t mmIconv_Transform(struct mmIconv* p, mmUInt8_t* i_buff, size_t i_size)
{
    size_t code = (size_t)-1;

    if ((iconv_t)-1 != p->pContext)
    {
        size_t o_size = i_size * 4;
        mmUInt8_t* o_buff = NULL;

        mmUInt8_t* i_buff_copy = NULL;
        mmUInt8_t* o_buff_copy = NULL;

        size_t i_size_copy = 0;
        size_t o_size_copy = 0;

        mmStreambuf_Reset(&p->hStreambuf);
        mmStreambuf_AlignedMemory(&p->hStreambuf, o_size);
        o_buff = (mmUInt8_t*)(p->hStreambuf.buff + p->hStreambuf.pptr);

        // iconv will change input pointer value.
        i_buff_copy = i_buff;
        o_buff_copy = o_buff;
        i_size_copy = i_size;
        o_size_copy = o_size;

        code = iconv(p->pContext, (const char**)&i_buff_copy, &i_size_copy, (char**)&o_buff_copy, &o_size_copy);

        o_size = ((size_t)-1 == code) ? 0 : o_size - o_size_copy;

        mmStreambuf_Pbump(&p->hStreambuf, o_size);
    }

    return code;
}
size_t mmIconv_TransformLength(struct mmIconv* p)
{
    return mmStreambuf_Size(&p->hStreambuf);
}
mmUInt8_t* mmIconv_TransformBuffer(struct mmIconv* p)
{
    return (mmUInt8_t*)(p->hStreambuf.buff + p->hStreambuf.gptr);
}

const struct mmIconvCodePage* mmIconvCodePage_Information(mmUInt32_t hCodePage)
{
    struct mmIconvCodePage* u = NULL;

    size_t index = 0;

    index = __static_mmIconvCodePage_InformationBinarySerach(0, __IconvCodePageSize, hCodePage);

    if ((size_t)(-1) != index && index < __IconvCodePageSize)
    {
        u = &MM_ICONV_CODE_PAGE_VECTOR[index];
    }

    return u;
}
mmUInt32_t mmIconv_LocaleCodePage(void)
{
    struct mmLogger* gLogger = mmLogger_Instance();

    // default.
    // 65001    utf-8   Unicode (UTF-8)
    mmUInt32_t hCodePage = 65001;

    struct mmString qualified_name;
    struct mmString basename;
    struct mmString suffixname;

    struct mmString ctype_locale_0;
    struct mmString ctype_locale_1;
    struct mmString ctype_locale_2;

    char* _ctype_locale_0_c_str = NULL;
    char* _ctype_locale_1_c_str = NULL;
    char* _ctype_locale_2_c_str = NULL;

    mmString_Init(&qualified_name);
    mmString_Init(&basename);
    mmString_Init(&suffixname);

    mmString_Init(&ctype_locale_0);
    mmString_Init(&ctype_locale_1);
    mmString_Init(&ctype_locale_2);

    // lang[_country_region[.code_page]]
    // Chinese (Simplified)_People's Republic of China.936
    // Chinese_People's Republic of China.936
    // zh_CN.GBK
    _ctype_locale_0_c_str = setlocale(LC_CTYPE, NULL);
    mmString_Assigns(&ctype_locale_0, NULL == _ctype_locale_0_c_str ? "" : _ctype_locale_0_c_str);
    _ctype_locale_1_c_str = setlocale(LC_CTYPE, "");
    mmString_Assigns(&ctype_locale_1, NULL == _ctype_locale_1_c_str ? "" : _ctype_locale_1_c_str);
    _ctype_locale_2_c_str = setlocale(LC_CTYPE, mmString_CStr(&ctype_locale_0));
    mmString_Assigns(&ctype_locale_2, NULL == _ctype_locale_2_c_str ? "" : _ctype_locale_2_c_str);

    mmLogger_LogI(gLogger, "%s %d %s %s %s.", __FUNCTION__, __LINE__, 
        mmString_CStr(&ctype_locale_0), mmString_CStr(&ctype_locale_1), mmString_CStr(&ctype_locale_2));

    mmString_Assign(&qualified_name, &ctype_locale_1);

    mmPathSplitSuffixName(&qualified_name, &basename, &suffixname);
    mmValue_StringToUInt32(mmString_CStr(&suffixname), &hCodePage);

    mmString_Destroy(&qualified_name);
    mmString_Destroy(&basename);
    mmString_Destroy(&suffixname);

    mmString_Destroy(&ctype_locale_0);
    mmString_Destroy(&ctype_locale_1);
    mmString_Destroy(&ctype_locale_2);

    return hCodePage;
}
const char* mmIconv_LocaleFormat(void)
{
    mmUInt32_t code_page = 65001;
    const struct mmIconvCodePage* pIconvCodePage = NULL;

    code_page = mmIconv_LocaleCodePage();
    pIconvCodePage = mmIconvCodePage_Information(code_page);

    return (NULL == pIconvCodePage) ? "UTF-8" : pIconvCodePage->pAbbreviation;
}

void mmIconvContext_Init(struct mmIconvContext* p)
{
    mmString_Init(&p->hFormatView);
    mmString_Init(&p->hFormatHost);
    mmIconv_Init(&p->hIconvViewToHost);
    mmIconv_Init(&p->hIconvHostToView);
    mmRbtreeStringVpt_Init(&p->hRbtree);

    mmString_Assigns(&p->hFormatView, "UTF-8");
    mmString_Assigns(&p->hFormatHost, "GBK");
}
void mmIconvContext_Destroy(struct mmIconvContext* p)
{
    mmString_Destroy(&p->hFormatView);
    mmString_Destroy(&p->hFormatHost);
    mmIconv_Destroy(&p->hIconvViewToHost);
    mmIconv_Destroy(&p->hIconvHostToView);
    mmRbtreeStringVpt_Destroy(&p->hRbtree);
}
void mmIconvContext_UpdateFormat(struct mmIconvContext* p, const char* pFormatHost, const char* pFormatView)
{
    const char* pRealFormatHost = iconv_canonicalize(pFormatHost);
    const char* pRealFormatView = iconv_canonicalize(pFormatView);
    //
    mmString_Assigns(&p->hFormatHost, pFormatHost);
    mmString_Assigns(&p->hFormatView, pFormatView);
    //
    mmIconv_UpdateFormat(&p->hIconvViewToHost, pRealFormatHost, pRealFormatView);
    mmIconv_UpdateFormat(&p->hIconvHostToView, pRealFormatView, pRealFormatHost);
}
void mmIconvContext_UpdateFormatLocale(struct mmIconvContext* p, const char* pFormatView)
{
    const char* pFormatHost = mmIconv_LocaleFormat();

    mmIconvContext_UpdateFormat(p, pFormatHost, pFormatView);
}
// conversion
void mmIconvContext_ViewToHost(struct mmIconvContext* p, struct mmByteBuffer* i, struct mmByteBuffer* o)
{
    mmIconv_Transform(&p->hIconvViewToHost, (mmUInt8_t*)(i->buffer + i->offset), (size_t)i->length);
    o->buffer = mmIconv_TransformBuffer(&p->hIconvViewToHost);
    o->length = mmIconv_TransformLength(&p->hIconvViewToHost);
    o->offset = 0;
}
void mmIconvContext_HostToView(struct mmIconvContext* p, struct mmByteBuffer* i, struct mmByteBuffer* o)
{
    mmIconv_Transform(&p->hIconvHostToView, (mmUInt8_t*)(i->buffer + i->offset), (size_t)i->length);
    o->buffer = mmIconv_TransformBuffer(&p->hIconvHostToView);
    o->length = mmIconv_TransformLength(&p->hIconvHostToView);
    o->offset = 0;
}
void mmIconvContext_Cachelist(struct mmIconvContext* p)
{
    const struct mmIconvCodePage* u = NULL;
    size_t i = 0;
    struct mmString hKey;

    for (i = 0; i < __IconvCodePageSize; i++)
    {
        u = &MM_ICONV_CODE_PAGE_VECTOR[i];

        mmString_MakeWeak(&hKey, u->pAbbreviation);
        mmRbtreeStringVpt_Set(&p->hRbtree, &hKey, (void*)u);
    }
}
mmBool_t mmIconvContext_IsValidFormat(struct mmIconvContext* p, const char* pFormat)
{
    struct mmString hKey;
    mmString_MakeWeak(&hKey, pFormat);
    return NULL != mmRbtreeStringVpt_GetIterator(&p->hRbtree, &hKey);
}

void mmIconvContext_List(mmIconvContextListDoOneFunc func, void* data)
{
    iconvlist(func, data);
}
