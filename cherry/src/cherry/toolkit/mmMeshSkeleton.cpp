/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmMeshSkeleton.h"

#include "OgreSkeletonManager.h"
#include "OgreSkeletonInstance.h"

void mmMeshSkeleton_Attach(
    Ogre::Skeleton* _mainSkeleton, 
    const Ogre::String& _skeletonFile, 
    const Ogre::String& _groupName /* = Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME */)
{
    Ogre::SkeletonManager& pSkeletonManager = Ogre::SkeletonManager::getSingleton();
    Ogre::ResourcePtr hResourcePtr = pSkeletonManager.load(_skeletonFile, _groupName);
    Ogre::SkeletonPtr hSkeleton = Ogre::static_pointer_cast<Ogre::Skeleton>(hResourcePtr);
    Ogre::Skeleton* pSkeletonPtr = hSkeleton.get();
    Ogre::Skeleton::BoneHandleMap hBoneHandleMap;
    pSkeletonPtr->_buildMapBoneByHandle(pSkeletonPtr, hBoneHandleMap);
    _mainSkeleton->_mergeSkeletonAnimations(pSkeletonPtr, hBoneHandleMap);
}

void mmMeshSkeleton_Detach(
    Ogre::Skeleton* _mainSkeleton, 
    const Ogre::String& _skeletonName)
{
    _mainSkeleton->removeAnimation(_skeletonName);
}
