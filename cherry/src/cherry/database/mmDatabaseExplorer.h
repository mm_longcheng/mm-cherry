/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmDatabaseExplorer_h__
#define __mmDatabaseExplorer_h__

#include "core/mmCore.h"
#include "core/mmString.h"

#include "container/mmVectorValue.h"
#include "container/mmRbtreeU64.h"

#include "assets/mmAssetsPrestored.h"

#include "sqlite3.h"

#include "core/mmPrefix.h"

struct mmDatabaseExplorerHistoryItem
{
    mmUInt64_t id;
    struct mmString url;
    mmUInt32_t time_stay;
    mmUInt32_t time_last_enter;
    mmUInt32_t time_last_leave;
    float position;
};
extern void mmDatabaseExplorerHistoryItem_Init(struct mmDatabaseExplorerHistoryItem* p);
extern void mmDatabaseExplorerHistoryItem_Destroy(struct mmDatabaseExplorerHistoryItem* p);

extern void mmDatabaseExplorerHistoryItem_EventProduce(void* obj, void* u);
extern void mmDatabaseExplorerHistoryItem_EventRecycle(void* obj, void* u);

#define MM_DATABASE_EXPLORER_STMT_NUMBER 12

struct mmDatabaseExplorer
{
    struct mmAssetsPrestored assets_prestored;
    //
    struct mmString writable_path;
    //
    struct mmString directory_database;
    struct mmString directory_template;
    struct mmString filename_database;
    struct mmString filename_template;
    //
    struct mmVectorValue vector_item;// cache the result for select.
    struct mmRbtreeU64Vpt rbtree_item;// cache the result for select.
    //
    sqlite3* db;// sqlite3 db handle.

    sqlite3_stmt* stmt[MM_DATABASE_EXPLORER_STMT_NUMBER];
};
extern void mmDatabaseExplorer_Init(struct mmDatabaseExplorer* p);
extern void mmDatabaseExplorer_Destroy(struct mmDatabaseExplorer* p);

extern void mmDatabaseExplorer_SetFileContext(struct mmDatabaseExplorer* p, struct mmFileContext* _file_context);
extern void mmDatabaseExplorer_SetWritablePath(struct mmDatabaseExplorer* p, const char* writable_path);
extern void mmDatabaseExplorer_SetForceReplace(struct mmDatabaseExplorer* p, int force_replace);

extern void mmDatabaseExplorer_SetDirectoryDatabase(struct mmDatabaseExplorer* p, const char* directory_database);
extern void mmDatabaseExplorer_SetDirectoryTemplate(struct mmDatabaseExplorer* p, const char* directory_template);
extern void mmDatabaseExplorer_SetFilenameDatabase(struct mmDatabaseExplorer* p, const char* filename_database);
extern void mmDatabaseExplorer_SetFilenameTemplate(struct mmDatabaseExplorer* p, const char* filename_template);

// if the database name not empty, and the file not exist, we will copy the template db file for init it.
extern void mmDatabaseExplorer_Fopen(struct mmDatabaseExplorer* p);
extern void mmDatabaseExplorer_Fclose(struct mmDatabaseExplorer* p);

extern void mmDatabaseExplorer_SetUrlPosition(struct mmDatabaseExplorer* p, const char* url, float position);
extern float mmDatabaseExplorer_GetUrlPosition(struct mmDatabaseExplorer* p, const char* url);

extern void mmDatabaseExplorer_UrlLast(struct mmDatabaseExplorer* p, struct mmString* url);
extern void mmDatabaseExplorer_UrlDelete(struct mmDatabaseExplorer* p, const char* url);

extern void mmDatabaseExplorer_UrlEnter(struct mmDatabaseExplorer* p, const char* url);
extern void mmDatabaseExplorer_UrlLeave(struct mmDatabaseExplorer* p, const char* url);
//
extern void mmDatabaseExplorer_UrlRankingStaySelect(struct mmDatabaseExplorer* p, int max_number);
extern void mmDatabaseExplorer_UrlRankingLeaveSelect(struct mmDatabaseExplorer* p, int max_number);
//
extern void mmDatabaseExplorer_UrlVectorClear(struct mmDatabaseExplorer* p);

extern void mmDatabaseExplorer_TransactionB(struct mmDatabaseExplorer* p);
extern void mmDatabaseExplorer_TransactionE(struct mmDatabaseExplorer* p);

#include "core/mmSuffix.h"

#endif//__mmDatabaseExplorer_h__
