/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmDatabaseExplorer.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"
#include "core/mmFileSystem.h"

#include "dish/mmFilePath.h"

// TRANSACTION BEGIN
static const char* sql_0 = "BEGIN TRANSACTION;";

// TRANSACTION END
static const char* sql_1 = "END TRANSACTION;";

// set position
static const char* sql_2 =
"UPDATE history_url_time\n"
"SET\n"
"   position = ?\n"
"WHERE url = ?;";

// get position
static const char* sql_3 =
"SELECT\n"
"   position\n"
"FROM history_url_time WHERE url = ?;";

// count url
static const char* sql_4 =
"SELECT COUNT(*) AS count\n"
"FROM history_url_time\n"
"WHERE url = ?;";

// insert url time_last_enter
static const char* sql_5 =
"INSERT\n"
"INTO history_url_time(\n"
"   url,\n"
"   time_last_enter)\n"
"VALUES(\n"
"   ?,\n"
"   datetime(CURRENT_TIMESTAMP, 'localtime')\n"
");";

// update url time_last_enter
static const char* sql_6 =
"UPDATE history_url_time\n"
"SET\n"
"   time_last_enter = datetime(CURRENT_TIMESTAMP, 'localtime')\n"
"WHERE url = ?";

// leave url
static const char* sql_7 =
"UPDATE history_url_time\n"
"SET\n"
"   time_last_leave = datetime(CURRENT_TIMESTAMP, 'localtime'),\n"
"   time_stay = time_stay + strftime('%s', 'now', 'localtime') - strftime('%s', time_last_enter)\n"
"WHERE url = ?;";

// delete url
static const char* sql_8 =
"DELETE FROM history_url_time\n"
"WHERE url = ?;";

// select the last url
static const char* sql_9 =
"SELECT\n"
"   url\n"
"FROM history_url_time\n"
"order by time_last_leave DESC LIMIT 1;";

// select the time_stay ranking
static const char* sql_10 =
"SELECT\n"
"   id,\n"
"   url,\n"
"   time_stay,\n"
"   strftime('%s', time_last_enter) AS time_last_enter,\n"
"   strftime('%s', time_last_leave) AS time_last_leave,\n"
"   position\n"
"FROM history_url_time\n"
"order by time_stay DESC LIMIT ?;";

// select the time_last_leave ranking
static const char* sql_11 =
"SELECT\n"
"   id,\n"
"   url,\n"
"   time_stay,\n"
"   strftime('%s', time_last_enter) AS time_last_enter,\n"
"   strftime('%s', time_last_leave) AS time_last_leave,\n"
"   position\n"
"FROM history_url_time\n"
"order by time_last_leave DESC LIMIT ?;";

static const char** sql_array[MM_DATABASE_EXPLORER_STMT_NUMBER] =
{
    &sql_0,
    &sql_1,
    &sql_2,
    &sql_3,
    &sql_4,
    &sql_5,
    &sql_6,
    &sql_7,
    &sql_8,
    &sql_9,
    &sql_10,
    &sql_11,
};

static int __static_mmDatabaseExplorer_StmtCheckOk(struct mmDatabaseExplorer* p, const char* func, int line, int stmt_code);
static int __static_mmDatabaseExplorer_StmtCheckReturnNone(struct mmDatabaseExplorer* p, const char* func, int line, int stmt_code);
static int __static_mmDatabaseExplorer_StmtCheckReturnHave(struct mmDatabaseExplorer* p, const char* func, int line, int stmt_code);
static int __static_mmDatabaseExplorer_StmtCheckReturnOk(struct mmDatabaseExplorer* p, const char* func, int line, int stmt_code);
//
static void __static_mmDatabaseExplorer_UrlRankingSelect(struct mmDatabaseExplorer* p, int number, sqlite3_stmt* stmt);

#define CheckStmtCodeOk(p, stmt_code) if (1 == __static_mmDatabaseExplorer_StmtCheckOk(p, __FUNCTION__, __LINE__, stmt_code)) { break; }
#define CheckStmtCodeReturnNone(p, stmt_code) if (1 == __static_mmDatabaseExplorer_StmtCheckReturnNone(p, __FUNCTION__, __LINE__, stmt_code)) { break; }
#define CheckStmtCodeReturnHave(p, stmt_code) if (1 == __static_mmDatabaseExplorer_StmtCheckReturnHave(p, __FUNCTION__, __LINE__, stmt_code)) { break; }
#define CheckStmtCodeReturnOk(p, stmt_code) if (1 == __static_mmDatabaseExplorer_StmtCheckReturnOk(p, __FUNCTION__, __LINE__, stmt_code)) { break; }

void mmDatabaseExplorerHistoryItem_Init(struct mmDatabaseExplorerHistoryItem* p)
{
    p->id = 0;
    mmString_Init(&p->url);
    p->time_stay = 0;
    p->time_last_enter = 0;
    p->time_last_leave = 0;
    p->position = 0;
}
void mmDatabaseExplorerHistoryItem_Destroy(struct mmDatabaseExplorerHistoryItem* p)
{
    p->id = 0;
    mmString_Destroy(&p->url);
    p->time_stay = 0;
    p->time_last_enter = 0;
    p->time_last_leave = 0;
    p->position = 0;
}
void mmDatabaseExplorerHistoryItem_EventProduce(void* obj, void* u)
{
    struct mmDatabaseExplorerHistoryItem* e = (struct mmDatabaseExplorerHistoryItem*)(u);
    mmDatabaseExplorerHistoryItem_Init(e);
}
void mmDatabaseExplorerHistoryItem_EventRecycle(void* obj, void* u)
{
    struct mmDatabaseExplorerHistoryItem* e = (struct mmDatabaseExplorerHistoryItem*)(u);
    mmDatabaseExplorerHistoryItem_Destroy(e);
}

void mmDatabaseExplorer_Init(struct mmDatabaseExplorer* p)
{
    struct mmVectorValueEventAllocator hEventAllocator;

    mmAssetsPrestored_Init(&p->assets_prestored);

    mmString_Init(&p->writable_path);

    mmString_Init(&p->directory_database);
    mmString_Init(&p->directory_template);
    mmString_Init(&p->filename_database);
    mmString_Init(&p->filename_template);
    mmVectorValue_Init(&p->vector_item);
    mmRbtreeU64Vpt_Init(&p->rbtree_item);
    p->db = NULL;
    mmMemset(p->stmt, 0, sizeof(sqlite3_stmt*) * MM_DATABASE_EXPLORER_STMT_NUMBER);

    mmString_Assigns(&p->directory_database, "database");
    mmString_Assigns(&p->directory_template, "template");

    mmString_Assigns(&p->filename_database, "sqlite_database_explorer.db");
    mmString_Assigns(&p->filename_template, "sqlite_template_explorer.db");

    hEventAllocator.Produce = &mmDatabaseExplorerHistoryItem_EventProduce;
    hEventAllocator.Recycle = &mmDatabaseExplorerHistoryItem_EventRecycle;
    hEventAllocator.obj = p;
    mmVectorValue_SetElemSize(&p->vector_item, sizeof(struct mmDatabaseExplorerHistoryItem));
    mmVectorValue_SetEventAllocator(&p->vector_item, &hEventAllocator);
}
void mmDatabaseExplorer_Destroy(struct mmDatabaseExplorer* p)
{
    assert(NULL == p->db && "you need destroy the db before destroy.");
    assert(0 == p->vector_item.size && "clear item url map before destroy.");

    mmAssetsPrestored_Destroy(&p->assets_prestored);

    mmString_Destroy(&p->writable_path);

    mmString_Destroy(&p->directory_database);
    mmString_Destroy(&p->directory_template);
    mmString_Destroy(&p->filename_database);
    mmString_Destroy(&p->filename_template);
    mmVectorValue_Destroy(&p->vector_item);
    mmRbtreeU64Vpt_Destroy(&p->rbtree_item);
    p->db = NULL;
    mmMemset(p->stmt, 0, sizeof(sqlite3_stmt*) * MM_DATABASE_EXPLORER_STMT_NUMBER);
}

void mmDatabaseExplorer_SetFileContext(struct mmDatabaseExplorer* p, struct mmFileContext* _file_context)
{
    mmAssetsPrestored_SetFileContext(&p->assets_prestored, _file_context);
}
void mmDatabaseExplorer_SetWritablePath(struct mmDatabaseExplorer* p, const char* writable_path)
{
    mmString_Assigns(&p->writable_path, writable_path);
    mmDirectoryNoneSuffix(&p->writable_path, mmString_CStr(&p->writable_path));
}
void mmDatabaseExplorer_SetForceReplace(struct mmDatabaseExplorer* p, int force_replace)
{
    mmAssetsPrestored_SetForceReplace(&p->assets_prestored, force_replace);
}

void mmDatabaseExplorer_SetDirectoryDatabase(struct mmDatabaseExplorer* p, const char* directory_database)
{
    mmString_Assigns(&p->directory_database, directory_database);
    mmDirectoryNoneSuffix(&p->directory_database, mmString_CStr(&p->directory_database));
}
void mmDatabaseExplorer_SetDirectoryTemplate(struct mmDatabaseExplorer* p, const char* directory_template)
{
    mmString_Assigns(&p->directory_template, directory_template);
    mmDirectoryNoneSuffix(&p->directory_database, mmString_CStr(&p->directory_database));
}
void mmDatabaseExplorer_SetFilenameDatabase(struct mmDatabaseExplorer* p, const char* filename_database)
{
    mmString_Assigns(&p->filename_database, filename_database);
}
void mmDatabaseExplorer_SetFilenameTemplate(struct mmDatabaseExplorer* p, const char* filename_template)
{
    mmString_Assigns(&p->filename_template, filename_template);
}

void mmDatabaseExplorer_Fopen(struct mmDatabaseExplorer* p)
{
    int code = SQLITE_ERROR;
    size_t i = 0;
    const char* sql_string = NULL;

    struct mmString real_directory_database;
    struct mmString real_path_database;
    struct mmString real_path_template;

    struct mmLogger* gLogger = mmLogger_Instance();

    mmString_Init(&real_directory_database);
    mmString_Init(&real_path_database);
    mmString_Init(&real_path_template);

    mmString_Assign(&real_directory_database, &p->writable_path);
    mmString_Appends(&real_directory_database, mmString_Empty(&p->directory_database) ? "" : "/");
    mmString_Append(&real_directory_database, &p->directory_database);

    mmString_Assign(&real_path_database, &real_directory_database);
    mmString_Appends(&real_path_database, mmString_Empty(&p->filename_database) ? "" : "/");
    mmString_Append(&real_path_database, &p->filename_database);

    mmString_Assign(&real_path_template, &p->directory_template);
    mmString_Appends(&real_path_template, mmString_Empty(&p->filename_template) ? "" : "/");
    mmString_Append(&real_path_template, &p->filename_template);

    if (0 != mmAccess(mmString_CStr(&real_path_database), MM_ACCESS_F_OK))
    {
        // we need try mkdir the directory.
        mmMkdirIfNotExist(mmString_CStr(&real_directory_database));

        // not find the database file, we need init from the template.
        mmAssetsPrestored_SetWritable(&p->assets_prestored, mmString_CStr(&p->writable_path), mmString_CStr(&p->directory_database));
        mmAssetsPrestored_SynchronizeCopyfile(&p->assets_prestored, mmString_CStr(&real_path_template), mmString_CStr(&p->filename_database));

        mmLogger_LogI(gLogger, "%s %d synchronize_copyfile: %s->%s", __FUNCTION__, __LINE__, 
            mmString_CStr(&real_path_template), mmString_CStr(&p->filename_database));
    }

    do
    {
        code = sqlite3_open(mmString_CStr(&real_path_database), &p->db);

        if (SQLITE_OK != code)
        {
            mmLogger_LogE(gLogger, "%s %d (%d):%s", __FUNCTION__, __LINE__,
                sqlite3_errcode(p->db), 
                sqlite3_errmsg(p->db));
            break;
        }

        for (i = 0; i < MM_DATABASE_EXPLORER_STMT_NUMBER; i++)
        {
            sql_string = *sql_array[i];

            code = sqlite3_prepare_v2(p->db, sql_string, -1, &p->stmt[i], NULL);

            if (SQLITE_OK != code)
            {
                mmLogger_LogE(gLogger, "%s %d %s (%d):%s", __FUNCTION__, __LINE__,
                    sql_string,
                    sqlite3_errcode(p->db),
                    sqlite3_errmsg(p->db));
                break;
            }           
        }

        if (SQLITE_OK != code)
        {
            mmLogger_LogE(gLogger, "%s %d prepare stmt failure.", __FUNCTION__, __LINE__);
            break;
        }

        mmLogger_LogI(gLogger, "%s %d fopen success.", __FUNCTION__, __LINE__);
    } while (0);

    mmString_Destroy(&real_directory_database);
    mmString_Destroy(&real_path_database);
    mmString_Destroy(&real_path_template);
}
void mmDatabaseExplorer_Fclose(struct mmDatabaseExplorer* p)
{
    int code = SQLITE_ERROR;
    size_t i = 0;
    const char* sql_string = NULL;

    struct mmLogger* gLogger = mmLogger_Instance();

    /* destroy and release the statement */
    for (i = 0; i < MM_DATABASE_EXPLORER_STMT_NUMBER; i++)
    {
        if (NULL != p->stmt[i])
        {
            sql_string = *sql_array[i];

            code = sqlite3_finalize(p->stmt[i]);
            if (SQLITE_OK != code)
            {
                mmLogger_LogE(gLogger, "%s %d %s (%d):%s", __FUNCTION__, __LINE__,
                    sql_string,
                    sqlite3_errcode(p->db),
                    sqlite3_errmsg(p->db));
            }

            p->stmt[i] = NULL;
        }
    }

    code = sqlite3_close(p->db);

    if (SQLITE_OK != code)
    {
        mmLogger_LogE(gLogger, "%s %d (%d):%s", __FUNCTION__, __LINE__,
            sqlite3_errcode(p->db),
            sqlite3_errmsg(p->db));
    }
    else
    {
        mmLogger_LogI(gLogger, "%s %d fclose success.", __FUNCTION__, __LINE__);
    }

    p->db = NULL;
}

void mmDatabaseExplorer_SetUrlPosition(struct mmDatabaseExplorer* p, const char* url, float position)
{
    int stmt_code = 0;

    struct mmLogger* gLogger = mmLogger_Instance();

    sqlite3_stmt* stmt = p->stmt[2];

    do
    {
        /* reset the statement so it may be used again */
        stmt_code = sqlite3_reset(stmt);
        CheckStmtCodeOk(p, stmt_code);

        stmt_code = sqlite3_bind_double(stmt, 1, (double)position);
        CheckStmtCodeOk(p, stmt_code);

        stmt_code = sqlite3_bind_text(stmt, 2, url, -1, NULL);
        CheckStmtCodeOk(p, stmt_code);

        // none return use SQLITE_DONE.
        stmt_code = sqlite3_step(stmt);
        CheckStmtCodeReturnNone(p, stmt_code);
    } while (0);
}
float mmDatabaseExplorer_GetUrlPosition(struct mmDatabaseExplorer* p, const char* url)
{
    double _sql_position = 0;

    int stmt_code = 0;

    struct mmLogger* gLogger = mmLogger_Instance();

    sqlite3_stmt* stmt = p->stmt[3];

    do
    {
        /* reset the statement so it may be used again */
        stmt_code = sqlite3_reset(stmt);
        CheckStmtCodeOk(p, stmt_code);

        stmt_code = sqlite3_bind_text(stmt, 1, url, -1, NULL);
        CheckStmtCodeOk(p, stmt_code);

        // have nor none return use check return ok.
        stmt_code = sqlite3_step(stmt);
        CheckStmtCodeReturnOk(p, stmt_code);

        if (SQLITE_ROW == stmt_code)
        {
            _sql_position = sqlite3_column_double(stmt, 0);
        }
        else
        {
            _sql_position = 0.0;
        }
    } while (0);

    return (float)_sql_position;
}

void mmDatabaseExplorer_UrlLast(struct mmDatabaseExplorer* p, struct mmString* url)
{
    int stmt_code = 0;
    const void* bf = NULL;
    int sz = 0;

    struct mmLogger* gLogger = mmLogger_Instance();

    sqlite3_stmt* stmt = p->stmt[9];

    mmString_Assigns(url, "");

    do
    {
        /* reset the statement so it may be used again */
        stmt_code = sqlite3_reset(stmt);
        CheckStmtCodeOk(p, stmt_code);

        // have nor none return use check return ok.
        stmt_code = sqlite3_step(stmt);
        CheckStmtCodeReturnOk(p, stmt_code);

        if (SQLITE_ROW == stmt_code)
        {
            bf = sqlite3_column_blob(stmt, 0);
            sz = sqlite3_column_bytes(stmt, 0);

            mmString_Assignsn(url, (const char*)bf, sz);
        }
        else
        {
            mmString_Assigns(url, "");
        }

    } while (0);
}
void mmDatabaseExplorer_UrlDelete(struct mmDatabaseExplorer* p, const char* url)
{
    int stmt_code = 0;

    struct mmLogger* gLogger = mmLogger_Instance();

    sqlite3_stmt* stmt = p->stmt[8];

    do
    {
        /* reset the statement so it may be used again */
        stmt_code = sqlite3_reset(stmt);
        CheckStmtCodeOk(p, stmt_code);

        stmt_code = sqlite3_bind_text(stmt, 1, url, -1, NULL);
        CheckStmtCodeOk(p, stmt_code);

        // none return use SQLITE_DONE.
        stmt_code = sqlite3_step(stmt);
        CheckStmtCodeReturnNone(p, stmt_code);
    } while (0);
}

void mmDatabaseExplorer_UrlEnter(struct mmDatabaseExplorer* p, const char* url)
{
    int stmt_code = 0;
    int count = 0;

    struct mmLogger* gLogger = mmLogger_Instance();

    sqlite3_stmt* stmt_6 = p->stmt[4];
    sqlite3_stmt* stmt_7 = p->stmt[5];
    sqlite3_stmt* stmt_8 = p->stmt[6];

    do
    {
        /* reset the statement so it may be used again */
        stmt_code = sqlite3_reset(stmt_6);
        CheckStmtCodeOk(p, stmt_code);

        stmt_code = sqlite3_bind_text(stmt_6, 1, url, -1, NULL);
        CheckStmtCodeOk(p, stmt_code);

        // have return use SQLITE_ROW.
        stmt_code = sqlite3_step(stmt_6);
        CheckStmtCodeReturnHave(p, stmt_code);

        count = sqlite3_column_int(stmt_6, 0);

        if (0 == count)
        {
            // we need insert.

            /* reset the statement so it may be used again */
            stmt_code = sqlite3_reset(stmt_7);
            CheckStmtCodeOk(p, stmt_code);

            stmt_code = sqlite3_bind_text(stmt_7, 1, url, -1, NULL);
            CheckStmtCodeOk(p, stmt_code);

            // none return use SQLITE_DONE.
            stmt_code = sqlite3_step(stmt_7);
            CheckStmtCodeReturnNone(p, stmt_code);
        }
        else
        {
            // we need update.

            /* reset the statement so it may be used again */
            stmt_code = sqlite3_reset(stmt_8);
            CheckStmtCodeOk(p, stmt_code);

            stmt_code = sqlite3_bind_text(stmt_8, 1, url, -1, NULL);
            CheckStmtCodeOk(p, stmt_code);

            // none return use SQLITE_DONE.
            stmt_code = sqlite3_step(stmt_8);
            CheckStmtCodeReturnNone(p, stmt_code);
        }
    } while (0);
}
void mmDatabaseExplorer_UrlLeave(struct mmDatabaseExplorer* p, const char* url)
{
    int stmt_code = 0;

    struct mmLogger* gLogger = mmLogger_Instance();

    sqlite3_stmt* stmt = p->stmt[7];

    do
    {
        /* reset the statement so it may be used again */
        stmt_code = sqlite3_reset(stmt);
        CheckStmtCodeOk(p, stmt_code);

        stmt_code = sqlite3_bind_text(stmt, 1, url, -1, NULL);
        CheckStmtCodeOk(p, stmt_code);

        // none return use SQLITE_DONE.
        stmt_code = sqlite3_step(stmt);
        CheckStmtCodeReturnNone(p, stmt_code);
    } while (0);
}
//
void mmDatabaseExplorer_UrlRankingStaySelect(struct mmDatabaseExplorer* p, int max_number)
{
    sqlite3_stmt* stmt = p->stmt[10];
    __static_mmDatabaseExplorer_UrlRankingSelect(p, max_number, stmt);
}
void mmDatabaseExplorer_UrlRankingLeaveSelect(struct mmDatabaseExplorer* p, int max_number)
{
    sqlite3_stmt* stmt = p->stmt[11];
    __static_mmDatabaseExplorer_UrlRankingSelect(p, max_number, stmt);
}
void mmDatabaseExplorer_UrlVectorClear(struct mmDatabaseExplorer* p)
{
    mmVectorValue_Clear(&p->vector_item);
    mmRbtreeU64Vpt_Clear(&p->rbtree_item);
}

void mmDatabaseExplorer_TransactionB(struct mmDatabaseExplorer* p)
{
    int stmt_code = 0;

    struct mmLogger* gLogger = mmLogger_Instance();

    sqlite3_stmt* stmt = p->stmt[0];

    do
    {
        /* reset the statement so it may be used again */
        stmt_code = sqlite3_reset(stmt);
        CheckStmtCodeOk(p, stmt_code);

        // none return use SQLITE_DONE.
        stmt_code = sqlite3_step(stmt);
        CheckStmtCodeReturnNone(p, stmt_code);
    } while (0);
}
void mmDatabaseExplorer_TransactionE(struct mmDatabaseExplorer* p)
{
    int stmt_code = 0;

    struct mmLogger* gLogger = mmLogger_Instance();

    sqlite3_stmt* stmt = p->stmt[1];

    do
    {
        /* reset the statement so it may be used again */
        stmt_code = sqlite3_reset(stmt);
        CheckStmtCodeOk(p, stmt_code);

        // none return use SQLITE_DONE.
        stmt_code = sqlite3_step(stmt);
        CheckStmtCodeReturnNone(p, stmt_code);
    } while (0);
}

static int __static_mmDatabaseExplorer_StmtCheckOk(struct mmDatabaseExplorer* p, const char* func, int line, int stmt_code)
{
    if (SQLITE_OK != stmt_code)
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        mmLogger_LogE(gLogger, "%s %d (%d):%s", func, line,
            sqlite3_errcode(p->db),
            sqlite3_errmsg(p->db));
        return 1;
    }
    else
    {
        return 0;
    }
}
static int __static_mmDatabaseExplorer_StmtCheckReturnNone(struct mmDatabaseExplorer* p, const char* func, int line, int stmt_code)
{
    if (SQLITE_DONE != stmt_code)
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        mmLogger_LogE(gLogger, "%s %d (%d):%s", func, line,
            sqlite3_errcode(p->db),
            sqlite3_errmsg(p->db));
        return 1;
    }
    else
    {
        return 0;
    }
}
static int __static_mmDatabaseExplorer_StmtCheckReturnHave(struct mmDatabaseExplorer* p, const char* func, int line, int stmt_code)
{
    if (SQLITE_ROW != stmt_code)
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        mmLogger_LogE(gLogger, "%s %d (%d):%s", func, line,
            sqlite3_errcode(p->db),
            sqlite3_errmsg(p->db));
        return 1;
    }
    else
    {
        return 0;
    }
}
static int __static_mmDatabaseExplorer_StmtCheckReturnOk(struct mmDatabaseExplorer* p, const char* func, int line, int stmt_code)
{
    if (SQLITE_ROW != stmt_code && SQLITE_DONE != stmt_code)
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        mmLogger_LogE(gLogger, "%s %d (%d):%s", func, line,
            sqlite3_errcode(p->db),
            sqlite3_errmsg(p->db));
        return 1;
    }
    else
    {
        return 0;
    }
}
static void __static_mmDatabaseExplorer_UrlRankingSelect(struct mmDatabaseExplorer* p, int max_number, sqlite3_stmt* stmt)
{
    int stmt_code = 0;

    size_t vector_size = 0;
    struct mmDatabaseExplorerHistoryItem* _item = NULL;
    struct mmRbtreeU64VptIterator* it = NULL;
    mmUInt64_t id = 0;
    const void* bf = NULL;
    int sz = 0;

    struct mmLogger* gLogger = mmLogger_Instance();

    vector_size = p->vector_item.size;

    do
    {
        /* reset the statement so it may be used again */
        stmt_code = sqlite3_reset(stmt);
        CheckStmtCodeOk(p, stmt_code);

        stmt_code = sqlite3_bind_int(stmt, 1, (int)max_number);
        CheckStmtCodeOk(p, stmt_code);

        // have return use SQLITE_ROW.
        while (SQLITE_ROW == sqlite3_step(stmt))
        {
            id = sqlite3_column_int64(stmt, 0);

            if (p->rbtree_item.size < max_number)
            {
                it = mmRbtreeU64Vpt_GetIterator(&p->rbtree_item, id);
                if (NULL == it)
                {
                    mmVectorValue_AlignedMemory(&p->vector_item, vector_size + 1);

                    _item = (struct mmDatabaseExplorerHistoryItem*)mmVectorValue_GetIndexReference(&p->vector_item, vector_size);

                    vector_size++;

                    bf = sqlite3_column_blob(stmt, 1);
                    sz = sqlite3_column_bytes(stmt, 1);

                    _item->id = sqlite3_column_int64(stmt, 0);
                    mmString_Assignsn(&_item->url, (const char*)bf, sz);
                    _item->time_stay = sqlite3_column_int(stmt, 2);
                    _item->time_last_enter = sqlite3_column_int(stmt, 3);
                    _item->time_last_leave = sqlite3_column_int(stmt, 4);
                    _item->position = (float)sqlite3_column_double(stmt, 5);

                    mmRbtreeU64Vpt_Set(&p->rbtree_item, _item->id, _item);
                }
            }
            else
            {
                // elem number is enough.
                break;
            }
        }

    } while (0);
}
