/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmAssetsPrestored.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"

#include "dish/mmFilePath.h"

static int __static_mmAssetsPrestored_IsDataExists(struct mmRbtreeStringVpt* _rbtree, struct mmString* f);

void mmAssetsPrestored_Init(struct mmAssetsPrestored* p)
{
    p->file_context = NULL;

    mmString_Init(&p->version_filename);

    mmString_Init(&p->writable_directory);
    mmString_Init(&p->writable_path);
    mmString_Init(&p->writable_base);

    mmStreambuf_Init(&p->streambuf);

    mmFileAssetsFolder_Init(&p->assets_folder);

    mmRbtreeStringVpt_Init(&p->f_rbtree_dir);
    mmRbtreeStringVpt_Init(&p->f_rbtree_reg);

    mmRbtreeStringVpt_Init(&p->t_rbtree_dir);
    mmRbtreeStringVpt_Init(&p->t_rbtree_reg);

    p->force_replace = 0;

    mmMemset(&p->s, 0, sizeof(mmStat_t));
    p->mode = MM_S_IFREG | MM_S_IRWXU | MM_S_IRWXG | MM_S_IRWXO;
    //
    mmString_Assigns(&p->version_filename, ".mm_version");
}
void mmAssetsPrestored_Destroy(struct mmAssetsPrestored* p)
{
    p->file_context = NULL;

    mmString_Destroy(&p->version_filename);

    mmString_Destroy(&p->writable_directory);
    mmString_Destroy(&p->writable_path);
    mmString_Destroy(&p->writable_base);

    mmStreambuf_Destroy(&p->streambuf);

    mmFileAssetsFolder_Destroy(&p->assets_folder);

    mmRbtreeStringVpt_Destroy(&p->f_rbtree_dir);
    mmRbtreeStringVpt_Destroy(&p->f_rbtree_reg);

    mmRbtreeStringVpt_Destroy(&p->t_rbtree_dir);
    mmRbtreeStringVpt_Destroy(&p->t_rbtree_reg);

    p->force_replace = 0;

    mmMemset(&p->s, 0, sizeof(mmStat_t));
    p->mode = MM_S_IFREG | MM_S_IRWXU | MM_S_IRWXG | MM_S_IRWXO;
}

void mmAssetsPrestored_SetFileContext(struct mmAssetsPrestored* p, struct mmFileContext* _file_context)
{
    p->file_context = _file_context;
}
void mmAssetsPrestored_SetWritable(struct mmAssetsPrestored* p, const char* writable_path, const char* writable_base)
{
    mmString_Assigns(&p->writable_path, writable_path);
    mmString_Assigns(&p->writable_base, writable_base);

    mmString_Assign(&p->writable_directory, &p->writable_path);
    mmString_Appends(&p->writable_directory, mmString_Empty(&p->writable_path) ? "" : "/");
    mmString_Append(&p->writable_directory, &p->writable_base);

    // remove the suffix if t is empty.
    mmDirectoryNoneSuffix(&p->writable_directory, mmString_CStr(&p->writable_directory));
}
void mmAssetsPrestored_SetForceReplace(struct mmAssetsPrestored* p, int force_replace)
{
    p->force_replace = force_replace;
}

void mmAssetsPrestored_Synchronize(struct mmAssetsPrestored* p, const char* f, const char* t)
{
    struct mmString f_filename;
    struct mmString t_filename;
    struct mmString hWritablePath;

    const char* separator_0 = NULL;
    const char* separator_1 = NULL;
    const char* separator_2 = NULL;

    int mode = 00776;

    assert(0 == p->f_rbtree_dir.size && "we must release item after acquire.");
    assert(0 == p->f_rbtree_reg.size && "we must release item after acquire.");
    assert(0 == p->t_rbtree_dir.size && "we must release item after acquire.");
    assert(0 == p->t_rbtree_reg.size && "we must release item after acquire.");

    mmString_Init(&f_filename);
    mmString_Init(&t_filename);
    mmString_Init(&hWritablePath);

    separator_0 = mmString_Empty(&p->writable_directory) ? "" : "/";

    mmString_Assign(&hWritablePath, &p->writable_directory);
    mmString_Appends(&hWritablePath, separator_0);
    mmString_Appends(&hWritablePath, t);
    // remove the suffix if t is empty.
    mmDirectoryNoneSuffix(&hWritablePath, mmString_CStr(&hWritablePath));

    if (0 == p->force_replace && 0 == mmAccess(mmString_CStr(&hWritablePath), MM_ACCESS_F_OK))
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogI(gLogger, "%s %d hWritablePath already exists ignore synchronize: %s", __FUNCTION__, __LINE__, t);
    }
    else
    {
        mmMkdirIfNotExist(mmString_CStr(&p->writable_directory));
        mmMkdirIfNotExist(mmString_CStr(&hWritablePath));

        separator_1 = 0 == mmStrlen(f) ? "" : "/";
        separator_2 = mmString_Empty(&hWritablePath) ? "" : "/";

        mmFileAssetsFolder_SetAssets(&p->assets_folder, mmString_CStr(&hWritablePath), mmString_CStr(&hWritablePath), "");

        mmFileAssetsFolder_AcquireEntry(&p->assets_folder);

        mmFileContext_AcquireFindFiles(p->file_context, f, "*", 1, 1, 0, &p->f_rbtree_dir);
        mmFileContext_AcquireFindFiles(p->file_context, f, "*", 1, 0, 0, &p->f_rbtree_reg);

        mmFileAssetsFolder_AcquireFindFiles(&p->assets_folder, "", "*", 1, 1, 0, &p->t_rbtree_dir);
        mmFileAssetsFolder_AcquireFindFiles(&p->assets_folder, "", "*", 1, 0, 0, &p->t_rbtree_reg);

        // t reg
        {
            struct mmFileAssetsInfo* file_info = NULL;

            struct mmRbNode* n = NULL;
            struct mmRbtreeStringVptIterator* it = NULL;

            n = mmRb_First(&p->t_rbtree_reg.rbt);
            while (NULL != n)
            {
                it = (struct mmRbtreeStringVptIterator*)mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
                n = mmRb_Next(n);
                file_info = (struct mmFileAssetsInfo*)(it->v);

                mmString_Assign(&t_filename, &hWritablePath);
                mmString_Appends(&t_filename, separator_2);
                mmString_Append(&t_filename, &file_info->filename);

                mmAssetsPrestored_CheckRemove(p, file_info, &t_filename);
            }
        }

        // t dir
        {
            struct mmFileAssetsInfo* file_info = NULL;

            struct mmRbNode* n = NULL;
            struct mmRbtreeStringVptIterator* it = NULL;

            n = mmRb_First(&p->t_rbtree_dir.rbt);
            while (NULL != n)
            {
                it = (struct mmRbtreeStringVptIterator*)mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
                n = mmRb_Next(n);
                file_info = (struct mmFileAssetsInfo*)(it->v);

                mmString_Assign(&t_filename, &hWritablePath);
                mmString_Appends(&t_filename, separator_2);
                mmString_Append(&t_filename, &file_info->filename);

                mmAssetsPrestored_CheckRemove(p, file_info, &t_filename);
            }
        }

        // f dir
        {
            struct mmFileAssetsInfo* file_info = NULL;

            struct mmRbNode* n = NULL;
            struct mmRbtreeStringVptIterator* it = NULL;

            n = mmRb_First(&p->f_rbtree_dir.rbt);
            while (NULL != n)
            {
                it = (struct mmRbtreeStringVptIterator*)mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
                n = mmRb_Next(n);
                file_info = (struct mmFileAssetsInfo*)(it->v);

                mmString_Assigns(&f_filename, f);
                mmString_Appends(&f_filename, separator_1);
                mmString_Append(&f_filename, &file_info->filename);

                mmString_Assign(&t_filename, &hWritablePath);
                mmString_Appends(&t_filename, separator_2);
                mmString_Append(&t_filename, &file_info->filename);

                mmAssetsPrestored_ExtractMkdir(p, mmString_CStr(&t_filename), mode);
            }
        }

        // f reg
        {
            struct mmFileAssetsInfo* file_info = NULL;

            struct mmRbNode* n = NULL;
            struct mmRbtreeStringVptIterator* it = NULL;

            n = mmRb_First(&p->f_rbtree_reg.rbt);
            while (NULL != n)
            {
                it = (struct mmRbtreeStringVptIterator*)mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
                n = mmRb_Next(n);
                file_info = (struct mmFileAssetsInfo*)(it->v);

                mmString_Assigns(&f_filename, f);
                mmString_Appends(&f_filename, separator_1);
                mmString_Append(&f_filename, &file_info->filename);

                mmString_Assign(&t_filename, &hWritablePath);
                mmString_Appends(&t_filename, separator_2);
                mmString_Append(&t_filename, &file_info->filename);

                mmAssetsPrestored_ExtractXcopy(p, mmString_CStr(&f_filename), mmString_CStr(&t_filename));
            }
        }

        mmFileContext_ReleaseFindFiles(p->file_context, &p->f_rbtree_dir);
        mmFileContext_ReleaseFindFiles(p->file_context, &p->f_rbtree_reg);

        mmFileAssetsFolder_ReleaseFindFiles(&p->assets_folder, &p->t_rbtree_dir);
        mmFileAssetsFolder_ReleaseFindFiles(&p->assets_folder, &p->t_rbtree_reg);

        mmFileAssetsFolder_ReleaseEntry(&p->assets_folder);
    }

    mmString_Destroy(&f_filename);
    mmString_Destroy(&t_filename);
    mmString_Destroy(&hWritablePath);
}
void mmAssetsPrestored_SynchronizeCopyfile(struct mmAssetsPrestored* p, const char* f, const char* t)
{
    struct mmString hWritablePath;

    const char* separator_0 = NULL;

    mmString_Init(&hWritablePath);

    separator_0 = mmString_Empty(&p->writable_directory) ? "" : "/";

    mmString_Assign(&hWritablePath, &p->writable_directory);
    mmString_Appends(&hWritablePath, separator_0);
    mmString_Appends(&hWritablePath, t);
    // remove the suffix if t is empty.
    mmDirectoryNoneSuffix(&hWritablePath, mmString_CStr(&hWritablePath));

    if (0 == p->force_replace && 0 == mmAccess(mmString_CStr(&hWritablePath), MM_ACCESS_F_OK))
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogI(gLogger, "%s %d hWritablePath already exists ignore synchronize: %s", __FUNCTION__, __LINE__, t);
    }
    else
    {
        mmAssetsPrestored_ExtractXcopy(p, f, mmString_CStr(&hWritablePath));
    }

    mmString_Destroy(&hWritablePath);
}
void mmAssetsPrestored_CheckRemove(struct mmAssetsPrestored* p, struct mmFileAssetsInfo* file_info, struct mmString* t)
{
    struct mmLogger* gLogger = mmLogger_Instance();

    do
    {       
        if (1 == __static_mmAssetsPrestored_IsDataExists(&p->f_rbtree_dir, &file_info->filename))
        {
            break;
        }

        if (1 == __static_mmAssetsPrestored_IsDataExists(&p->f_rbtree_reg, &file_info->filename))
        {
            break;
        }

        if (0 == mmAccess(mmString_CStr(t), MM_ACCESS_F_OK))
        {
            mmLogger_LogI(gLogger, "%s %d recursive_remove %s", __FUNCTION__, __LINE__, mmString_CStr(t));
            mmRecursiveRemove(mmString_CStr(t));
        }

    } while (0);
}
//////////////////////////////////////////////////////////////////////////
void mmAssetsPrestored_ExtractCopyfile(struct mmAssetsPrestored* p, const char* f, const char* t)
{
    FILE* file = NULL;

    size_t st_size = 0;

    struct mmByteBuffer byte_buffer;

    mmStat(t, &p->s);
    p->mode = mmStat_Mode(&p->s);

    st_size = (size_t)p->s.st_size;

    mmByteBuffer_Init(&byte_buffer);

    mmFileContext_AcquireFileByteBuffer(p->file_context, f, &byte_buffer);

    if (st_size != byte_buffer.length)
    {
        mmAssetsPrestored_ExtractBuffer(p, &byte_buffer, t);
    }
    else
    {
        mmUInt8_t* f_buffer = byte_buffer.buffer + byte_buffer.offset;
        mmUInt8_t* t_buffer = p->streambuf.buff + p->streambuf.gptr;
        size_t t_size = 0;

        mmAssetsPrestored_ReadBuffer(p, t);

        t_size = mmStreambuf_Size(&p->streambuf);

        if (st_size != t_size || 0 != mmMemcmp(f_buffer, t_buffer, st_size))
        {
            mmAssetsPrestored_ExtractBuffer(p, &byte_buffer, t);
        }
    }

    mmFileContext_ReleaseFileByteBuffer(p->file_context, &byte_buffer);

    mmByteBuffer_Destroy(&byte_buffer);
}
void mmAssetsPrestored_ExtractRecursiveCopy(struct mmAssetsPrestored* p, const char* f, const char* t)
{
    struct mmString f_filename;
    struct mmString t_filename;
    struct mmString hWritablePath;

    struct mmRbtreeStringVpt rbtree_dir;
    struct mmRbtreeStringVpt rbtree_reg;

    const char* separator_0 = NULL;
    const char* separator_1 = NULL;
    const char* separator_2 = NULL;

    int mode = 00776;

    mmString_Init(&f_filename);
    mmString_Init(&t_filename);
    mmString_Init(&hWritablePath);

    mmRbtreeStringVpt_Init(&rbtree_dir);
    mmRbtreeStringVpt_Init(&rbtree_reg);

    separator_0 = mmString_Empty(&p->writable_directory) ? "" : "/";

    mmString_Assign(&hWritablePath, &p->writable_directory);
    mmString_Appends(&hWritablePath, separator_0);
    mmString_Appends(&hWritablePath, t);
    // remove the suffix if t is empty.
    mmDirectoryNoneSuffix(&hWritablePath, mmString_CStr(&hWritablePath));

    separator_1 = 0 == mmStrlen(f) ? "" : "/";
    separator_2 = mmString_Empty(&hWritablePath) ? "" : "/";

    mmFileContext_AcquireFindFiles(p->file_context, f, "*", 1, 1, 0, &rbtree_dir);
    mmFileContext_AcquireFindFiles(p->file_context, f, "*", 1, 0, 0, &rbtree_reg);

    // dir
    {
        struct mmFileAssetsInfo* file_info = NULL;

        struct mmRbNode* n = NULL;
        struct mmRbtreeStringVptIterator* it = NULL;

        n = mmRb_First(&rbtree_dir.rbt);
        while (NULL != n)
        {
            it = (struct mmRbtreeStringVptIterator*)mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
            n = mmRb_Next(n);
            file_info = (struct mmFileAssetsInfo*)(it->v);

            mmString_Assigns(&f_filename, f);
            mmString_Appends(&f_filename, separator_1);
            mmString_Append(&f_filename, &file_info->filename);

            mmString_Assign(&t_filename, &hWritablePath);
            mmString_Appends(&t_filename, separator_2);
            mmString_Append(&t_filename, &file_info->filename);

            mmAssetsPrestored_ExtractMkdir(p, mmString_CStr(&t_filename), mode);
        }
    }

    // reg
    {
        struct mmFileAssetsInfo* file_info = NULL;

        struct mmRbNode* n = NULL;
        struct mmRbtreeStringVptIterator* it = NULL;

        n = mmRb_First(&rbtree_reg.rbt);
        while (NULL != n)
        {
            it = (struct mmRbtreeStringVptIterator*)mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
            n = mmRb_Next(n);
            file_info = (struct mmFileAssetsInfo*)(it->v);

            mmString_Assigns(&f_filename, f);
            mmString_Appends(&f_filename, separator_1);
            mmString_Append(&f_filename, &file_info->filename);

            mmString_Assign(&t_filename, &hWritablePath);
            mmString_Appends(&t_filename, separator_2);
            mmString_Append(&t_filename, &file_info->filename);

            mmAssetsPrestored_ExtractXcopy(p, mmString_CStr(&f_filename), mmString_CStr(&t_filename));
        }
    }

    mmFileContext_ReleaseFindFiles(p->file_context, &rbtree_dir);
    mmFileContext_ReleaseFindFiles(p->file_context, &rbtree_reg);

    mmRbtreeStringVpt_Destroy(&rbtree_dir);
    mmRbtreeStringVpt_Destroy(&rbtree_reg);

    mmString_Destroy(&f_filename);
    mmString_Destroy(&t_filename);
    mmString_Destroy(&hWritablePath);
}

void mmAssetsPrestored_ExtractMkdir(struct mmAssetsPrestored* p, const char* t, int mode)
{
    if (0 != mmAccess(t, MM_ACCESS_F_OK))
    {
        mmMkdir(t, mode);
    }
    else
    {
        mmStat(t, &p->s);
        p->mode = mmStat_Mode(&p->s);

        if (!MM_S_ISDIR(p->mode))
        {
            // remove the regular file.
            // mkdir after.
            mmRemove(t);
            mmMkdir(t, mode);
        }
    }
}
void mmAssetsPrestored_ExtractXcopy(struct mmAssetsPrestored* p, const char* f, const char* t)
{
    if (0 != mmAccess(t, MM_ACCESS_F_OK))
    {
        mmAssetsPrestored_ExtractCopyfile(p, f, t);
    }
    else
    {
        mmStat(t, &p->s);
        p->mode = mmStat_Mode(&p->s);

        if (!MM_S_ISREG(p->mode))
        {
            // recursive remove the directory.
            // copyfile after.
            mmRecursiveRemove(t);
            mmAssetsPrestored_ExtractCopyfile(p, f, t);
        }
        else
        {
            mmAssetsPrestored_ExtractCopyfile(p, f, t);
        }
    }
}

void mmAssetsPrestored_ExtractBuffer(struct mmAssetsPrestored* p, struct mmByteBuffer* byte_buffer, const char* t)
{
    FILE* file = NULL;
    file = fopen(t, "wb");
    if (NULL != file)
    {
        fwrite((const void*)(byte_buffer->buffer + byte_buffer->offset), 1, byte_buffer->length, file);
        fclose(file);
    }
}
void mmAssetsPrestored_ReadBuffer(struct mmAssetsPrestored* p, const char* t)
{
    FILE* file = NULL;

    size_t st_size = 0;

    size_t n = 0;

    mmStat(t, &p->s);
    p->mode = mmStat_Mode(&p->s);

    st_size = (size_t)p->s.st_size;

    mmStreambuf_Reset(&p->streambuf);
    mmStreambuf_AlignedMemory(&p->streambuf, st_size);

    file = fopen(t, "rb");
    if (NULL != file)
    {
        n = fread((void*)(p->streambuf.buff + p->streambuf.pptr), 1, st_size, file);
        mmStreambuf_Pbump(&p->streambuf, n);
        fclose(file);
    }
}

static int __static_mmAssetsPrestored_IsDataExists(struct mmRbtreeStringVpt* _rbtree, struct mmString* f)
{
    int _exists = 0;

    struct mmFileAssetsInfo* file_info = NULL;

    struct mmRbNode* n = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;

    n = mmRb_First(&_rbtree->rbt);
    while (NULL != n)
    {
        it = (struct mmRbtreeStringVptIterator*)mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
        n = mmRb_Next(n);
        file_info = (struct mmFileAssetsInfo*)(it->v);

        if (0 == mmString_Compare(f, &file_info->filename))
        {
            _exists = 1;
            break;
        }
    }

    return _exists;
}

