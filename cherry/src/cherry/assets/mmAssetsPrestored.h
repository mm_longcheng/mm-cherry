/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmAssetsPrestored_h__
#define __mmAssetsPrestored_h__

#include "core/mmCore.h"
#include "core/mmString.h"
#include "core/mmStreambuf.h"
#include "core/mmFileSystem.h"

#include "dish/mmFileContext.h"

#include "core/mmPrefix.h"

// use for prestored extract little assets.
struct mmAssetsPrestored
{
    struct mmFileContext* file_context;

    struct mmString version_filename;

    struct mmString writable_directory;
    struct mmString writable_path;
    struct mmString writable_base;

    struct mmStreambuf streambuf;

    struct mmFileAssetsFolder assets_folder;

    // cache value
    struct mmRbtreeStringVpt f_rbtree_dir;
    struct mmRbtreeStringVpt f_rbtree_reg;

    struct mmRbtreeStringVpt t_rbtree_dir;
    struct mmRbtreeStringVpt t_rbtree_reg;

    int force_replace;

    mmStat_t s;
    mmUInt32_t mode;
};
extern void mmAssetsPrestored_Init(struct mmAssetsPrestored* p);
extern void mmAssetsPrestored_Destroy(struct mmAssetsPrestored* p);

extern void mmAssetsPrestored_SetFileContext(struct mmAssetsPrestored* p, struct mmFileContext* _file_context);
extern void mmAssetsPrestored_SetWritable(struct mmAssetsPrestored* p, const char* writable_path, const char* writable_base);
extern void mmAssetsPrestored_SetForceReplace(struct mmAssetsPrestored* p, int force_replace);

extern void mmAssetsPrestored_Synchronize(struct mmAssetsPrestored* p, const char* f, const char* t);
extern void mmAssetsPrestored_SynchronizeCopyfile(struct mmAssetsPrestored* p, const char* f, const char* t);
extern void mmAssetsPrestored_CheckRemove(struct mmAssetsPrestored* p, struct mmFileAssetsInfo* file_info, struct mmString* t);

extern void mmAssetsPrestored_ExtractCopyfile(struct mmAssetsPrestored* p, const char* f, const char* t);
extern void mmAssetsPrestored_ExtractRecursiveCopy(struct mmAssetsPrestored* p, const char* f, const char* t);

extern void mmAssetsPrestored_ExtractMkdir(struct mmAssetsPrestored* p, const char* t, int mode);
extern void mmAssetsPrestored_ExtractXcopy(struct mmAssetsPrestored* p, const char* f, const char* t);

extern void mmAssetsPrestored_ExtractBuffer(struct mmAssetsPrestored* p, struct mmByteBuffer* byte_buffer, const char* t);
extern void mmAssetsPrestored_ReadBuffer(struct mmAssetsPrestored* p, const char* t);

#include "core/mmSuffix.h"

#endif//__mmAssetsPrestored_h__