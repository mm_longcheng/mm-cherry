/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmModuleAssets.h"

#include "core/mmLogger.h"

#include "toolkit/mmIconv.h"

#include "nwsi/mmContextMaster.h"

namespace mm
{
    mmModuleAssets::mmModuleAssets(void)
        : mmModuleBase()
    {
        mmAssetsPrestored_Init(&this->hAssetsPrestored);
    }
    mmModuleAssets::~mmModuleAssets(void)
    {
        mmAssetsPrestored_Destroy(&this->hAssetsPrestored);
    }
    void mmModuleAssets::OnFinishLaunching(void)
    {
        struct mmContextMaster* pContextMaster = this->pContextMaster;
        struct mmFileContext* pFileContext = &pContextMaster->hFileContext;
        struct mmPackageAssets* pPackageAssets = &pContextMaster->hPackageAssets;

        const char* pExternalFilesDirectory = mmString_CStr(&pPackageAssets->hExternalFilesDirectory);

        // assign file_context
        mmAssetsPrestored_SetFileContext(&this->hAssetsPrestored, pFileContext);
        // . README.txt
        mmAssetsPrestored_SetWritable(&this->hAssetsPrestored, pExternalFilesDirectory, "");
        mmAssetsPrestored_SynchronizeCopyfile(&this->hAssetsPrestored, "document/ReadMe.txt", "ReadMe.txt");
        // license
        mmAssetsPrestored_SetWritable(&this->hAssetsPrestored, pExternalFilesDirectory, "license");
        mmAssetsPrestored_Synchronize(&this->hAssetsPrestored, "license", "");
        // document
        mmAssetsPrestored_SetWritable(&this->hAssetsPrestored, pExternalFilesDirectory, "document");
        mmAssetsPrestored_Synchronize(&this->hAssetsPrestored, "document", "");
        // examples
        mmAssetsPrestored_SetWritable(&this->hAssetsPrestored, pExternalFilesDirectory, "examples");
        mmAssetsPrestored_Synchronize(&this->hAssetsPrestored, "examples", "");
        // archive
        mmAssetsPrestored_SetWritable(&this->hAssetsPrestored, pExternalFilesDirectory, "archive");
        mmAssetsPrestored_Synchronize(&this->hAssetsPrestored, "archive", "");
        // download
        mmAssetsPrestored_SetWritable(&this->hAssetsPrestored, pExternalFilesDirectory, "download");
        mmAssetsPrestored_Synchronize(&this->hAssetsPrestored, "download", "");
    }
    void mmModuleAssets::OnBeforeTerminate(void)
    {

    }
}