/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmModuleDatabase.h"

#include "core/mmLogger.h"

#include "toolkit/mmIconv.h"

#include "nwsi/mmContextMaster.h"

namespace mm
{
    mmModuleDatabase::mmModuleDatabase(void)
        : mmModuleBase()
    {
        mmDatabaseExplorer_Init(&this->hDatabaseExplorer);
    }
    mmModuleDatabase::~mmModuleDatabase(void)
    {
        mmDatabaseExplorer_Destroy(&this->hDatabaseExplorer);
    }
    void mmModuleDatabase::OnFinishLaunching(void)
    {
        struct mmContextMaster* pContextMaster = this->pContextMaster;
        struct mmFileContext* pFileContext = &pContextMaster->hFileContext;
        struct mmPackageAssets* pPackageAssets = &pContextMaster->hPackageAssets;

        const char* pExternalFilesDirectory = mmString_CStr(&pPackageAssets->hExternalFilesDirectory);

        // assign file_context
        mmDatabaseExplorer_SetFileContext(&this->hDatabaseExplorer, pFileContext);
        // assign writable_path
        mmDatabaseExplorer_SetWritablePath(&this->hDatabaseExplorer, pExternalFilesDirectory);
        // fopen database.
        mmDatabaseExplorer_Fopen(&this->hDatabaseExplorer);
    }
    void mmModuleDatabase::OnBeforeTerminate(void)
    {
        mmDatabaseExplorer_Fclose(&this->hDatabaseExplorer);
    }
}