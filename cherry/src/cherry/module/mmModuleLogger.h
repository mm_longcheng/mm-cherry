/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmModuleLogger_h__
#define __mmModuleLogger_h__

#include "core/mmCore.h"
#include "core/mmMessageQueue.h"
#include "core/mmSpinlock.h"

#include "dish/mmEvent.h"
#include "dish/mmPoolType.h"

#include "nwsi/mmModule.h"

#include <string>

namespace mm
{
    class mmEventDataLogger : public mmEventArgs
    {
    public:
        mmUInt32_t hLevel;
        mmUInt32_t hCode;
        std::string hDesc;
        std::string hView;
    public:
        mmEventDataLogger(void);
        virtual ~mmEventDataLogger(void);
    };

    class mmModuleLogger : public mmModuleBase
    {
    public: 
        // layer view logger event.
        static const std::string EventLoggerView;
    public:
        static const int MAX_POPER_NUMBER;
    public:
        // this member is event drive.
        mm::mmEventSet hEventSet;
    public:
        typedef mmPoolType<mmEventDataLogger> PoolType;
        PoolType hPool;
        struct mmTwoLockQuque hTwoLockQuque;
        mmAtomic_t hPoolLocker;
        // max pop to make sure current not wait long times.
        // default is MAX_POPER_NUMBER.
        size_t hMaxPop;
    public:
        mmModuleLogger(void);
        virtual ~mmModuleLogger(void);
    public:
        void SetPopNumber(size_t hPopNumber);
        void PushEventData(const mmEventDataLogger& e);
        void LogView(mmUInt32_t lvl, mmUInt32_t code, const char* desc, const char* view);
        void Pop(size_t hNumber);
        //pop queue to clear or pop number limit
        void ThreadHandle(void);
    public:
        // mmModuleBase
        virtual void OnFinishLaunching(void);
        virtual void OnBeforeTerminate(void);
        // mmModuleBase
        virtual void OnUpdate(double hInterval);
    };
}

#endif//__mmModuleLogger_h__
