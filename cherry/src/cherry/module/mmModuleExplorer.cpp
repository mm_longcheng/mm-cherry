/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmModuleExplorer.h"

#include "core/mmLogger.h"

#include "nwsi/mmEventObjectArgs.h"

#include "toolkit/mmIconv.h"

namespace mm
{
    const std::string mmModuleExplorer::EventFormatUpdate("EventFormatUpdate");

    mmModuleExplorer::mmModuleExplorer(void)
    {
        mmString_Init(&this->hFormatHost);
        mmString_Init(&this->hFormatView);
    }
    mmModuleExplorer::~mmModuleExplorer(void)
    {
        mmString_Destroy(&this->hFormatHost);
        mmString_Destroy(&this->hFormatView);
    }
    void mmModuleExplorer::OnFinishLaunching(void)
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        const char* pFormatHost = mmIconv_LocaleFormat();
        const char* pFormatView = "UTF-8";

        mmString_Assigns(&this->hFormatHost, pFormatHost);
        mmString_Assigns(&this->hFormatView, pFormatView);

        mmLogger_LogI(gLogger, "%s %d ExplorerIconv: pFormatHost: %s pFormatView: %s", 
            __FUNCTION__, __LINE__, pFormatHost, pFormatView);
    }
    void mmModuleExplorer::OnBeforeTerminate(void)
    {

    }
    void mmModuleExplorer::FormatUpdate(const char* pFormatHost, const char* pFormatView)
    {
        mmString_Assigns(&this->hFormatHost, pFormatHost);
        mmString_Assigns(&this->hFormatView, pFormatView);

        mm::mmEventObjectArgs hArgs(this);
        this->hEventSet.FireEvent(EventFormatUpdate, hArgs);
    }
}