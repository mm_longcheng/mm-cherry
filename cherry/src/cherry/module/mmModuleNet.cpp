/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmModuleNet.h"

namespace mm
{
    //ip and port update
    const std::string mmModuleNetAddressState::EventAddressUpdate("EventAddressUpdate");
    //the socket update
    const std::string mmModuleNetAddressState::EventSocketUpdate("EventSocketUpdate");
    //crypto state update
    const std::string mmModuleNetAddressState::EventCryptoUpdate("EventCryptoUpdate");

    mmModuleNetAddressState::mmModuleNetAddressState(void)
        : hNode("127.0.0.1")
        , hPort(0)
        , hSocketState(MM_NET_STATE_CLOSED)
        , hCryptoState(MM_CRYPTO_STATE_CLOSED)
    {

    }

    mmModuleNetAddressState::~mmModuleNetAddressState(void)
    {

    }

    mmModuleNet::mmModuleNet(void)
    {

    }
    mmModuleNet::~mmModuleNet(void)
    {

    }

    void mmModuleNet::OnFinishLaunching(void)
    {
        
    }
    void mmModuleNet::OnBeforeTerminate(void)
    {
        
    }

    void mmModuleNet::OnUpdate(double hInterval)
    {
        
    }
}
