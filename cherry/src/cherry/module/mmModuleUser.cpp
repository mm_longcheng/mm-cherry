/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmModuleUser.h"

namespace mm
{
    void mmModuleUserBasic_Init(struct mmModuleUserBasic* p)
    {
        p->hName = "";
        p->hId = 0;
        p->hState = MM_USER_BASIC_CLOSED;
    }
    void mmModuleUserBasic_Destroy(struct mmModuleUserBasic* p)
    {
        p->hName = "";
        p->hId = 0;
        p->hState = MM_USER_BASIC_CLOSED;
    }
    
    void mmModuleUserToken_Init(struct mmModuleUserToken* p)
    {
        p->hState = MM_USER_TOKEN_CLOSED;
    }
    void mmModuleUserToken_Destroy(struct mmModuleUserToken* p)
    {
        p->hState = MM_USER_TOKEN_CLOSED;
    }

    const std::string mmModuleUser::EventBasicUpdate("EventBasicUpdate");
    const std::string mmModuleUser::EventTokenUpdate("EventTokenUpdate");

    mmModuleUser::mmModuleUser(void)
    {
        mmModuleUserBasic_Init(&this->hBasic);
        mmModuleUserToken_Init(&this->hToken);
    }

    mmModuleUser::~mmModuleUser(void)
    {
        mmModuleUserBasic_Destroy(&this->hBasic);
        mmModuleUserToken_Destroy(&this->hToken);
    }
    void mmModuleUser::OnFinishLaunching(void)
    {
        
    }
    void mmModuleUser::OnBeforeTerminate(void)
    {
        
    }
}
