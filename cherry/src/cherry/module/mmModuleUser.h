/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmModuleUser_h__
#define __mmModuleUser_h__

#include "dish/mmEvent.h"

#include "nwsi/mmModule.h"

#include <string>
#include <vector>

enum mmModuleDataUserBasicState
{
    MM_USER_BASIC_CLOSED = 0,// user is closed, Not logged in.
    MM_USER_BASIC_MOTION = 1,// user is landing, Not logged in.
    MM_USER_BASIC_FINISH = 2,// user already logged.
};

enum mmModuleDataUserTokenState
{
    MM_USER_TOKEN_CLOSED = 0,// user token is closed
    MM_USER_TOKEN_MOTION = 1,// user token is implementing
    MM_USER_TOKEN_FINISH = 2,// user token has already completed.
};

namespace mm
{
    struct mmModuleUserBasic
    {
        // 用户名.
        std::string hName;
        // 用户id.
        mmUInt64_t hId;
        // user state.
        int hState;
    };

    void mmModuleUserBasic_Init(struct mmModuleUserBasic* p);
    void mmModuleUserBasic_Destroy(struct mmModuleUserBasic* p);

    struct mmModuleUserToken
    {
        // token state.
        int hState;
    };

    void mmModuleUserToken_Init(struct mmModuleUserToken* p);
    void mmModuleUserToken_Destroy(struct mmModuleUserToken* p);

    class mmModuleUser : public mmModuleBase
    {
    public:
        struct mmModuleUserBasic hBasic;
        struct mmModuleUserToken hToken;
    public:
        static const std::string EventBasicUpdate;
        static const std::string EventTokenUpdate;
    public:
        // this member is event drive.
        mm::mmEventSet hEventSet;
    public:
        mmModuleUser(void);
        virtual ~mmModuleUser(void);
    public:
        // mmModuleBase
        virtual void OnFinishLaunching(void);
        virtual void OnBeforeTerminate(void);
    };
}

#endif//__mmModuleUser_h__
