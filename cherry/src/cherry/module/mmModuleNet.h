/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmModuleNet_h__
#define __mmModuleNet_h__

#include "core/mmCore.h"

#include "dish/mmEvent.h"

#include "nwsi/mmModule.h"

#include <string>

enum mmModuleNetSocketState
{
    MM_NET_STATE_CLOSED = 0,// fd is closed, connect closed. invalid at all.
    MM_NET_STATE_MOTION = 1,// fd is opened, connect closed. at connecting.
    MM_NET_STATE_FINISH = 2,// fd is opened, connect opened. at connected.
    MM_NET_STATE_BROKEN = 3,// fd is opened, connect broken. connect is broken fd not closed.
};
enum mmModuleNetCryptoState
{
    MM_CRYPTO_STATE_CLOSED = 0,// tcps is closed,Unencrypted packages are not allowed to be sent.
    MM_CRYPTO_STATE_FINISH = 1,// tcps already completed,Unencrypted packages are allowed to be sent.
};

namespace mm
{
    class mmModuleNetAddressState
    {
    public:
        std::string hNode;
        mmUShort_t hPort;
        int hSocketState;
        int hCryptoState;

        //ip and port update
        static const std::string EventAddressUpdate;

        //the socket update
        static const std::string EventSocketUpdate;

        //crypto state update
        static const std::string EventCryptoUpdate;
    public:
        // this member is event drive.
        mm::mmEventSet hEventSet;
    public:
        mmModuleNetAddressState(void);
        ~mmModuleNetAddressState(void);
    };

    class mmModuleNet : public mmModuleBase
    {
    public:
        mmModuleNetAddressState hEntry;
        mmModuleNetAddressState hLobby;
    public:
        // this member is event drive.
        mm::mmEventSet hEventSet;
    public:
        mmModuleNet(void);
        virtual ~mmModuleNet(void);
    public:
        // mmModuleBase
        virtual void OnFinishLaunching(void);
        virtual void OnBeforeTerminate(void);
        // mmModuleBase
        virtual void OnUpdate(double hInterval);
    };
}

#endif//__mmModuleNet_h__
