/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmModuleLogger.h"
#include "core/mmLogger.h"

namespace mm
{
    mmEventDataLogger::mmEventDataLogger(void)
        : hLevel(MM_LOG_INFO)
        , hCode(0)
        , hDesc("")
        , hView("")
    {

    }
    mmEventDataLogger::~mmEventDataLogger(void)
    {

    }

    const std::string mmModuleLogger::EventLoggerView("EventLoggerView");
    const int mmModuleLogger::MAX_POPER_NUMBER(240);

    mmModuleLogger::mmModuleLogger(void)
        : hMaxPop(MAX_POPER_NUMBER)
    {
        mmTwoLockQuque_Init(&this->hTwoLockQuque);
        mmSpinlock_Init(&this->hPoolLocker, NULL);
    }

    mmModuleLogger::~mmModuleLogger(void)
    {
        mmTwoLockQuque_Destroy(&this->hTwoLockQuque);
        mmSpinlock_Destroy(&this->hPoolLocker);
    }

    void mmModuleLogger::SetPopNumber(size_t hPopNumber)
    {
        this->hMaxPop = hPopNumber;
    }
    void mmModuleLogger::PushEventData(const mmEventDataLogger& e)
    {
        mmEventDataLogger* data = NULL;

        mmSpinlock_Lock(&this->hPoolLocker);
        data = (mmEventDataLogger*)this->hPool.Produce();
        mmSpinlock_Unlock(&this->hPoolLocker);

        // copy value.
        *data = e;

        // push to message queue.
        mmTwoLockQuque_Enqueue(&this->hTwoLockQuque, (uintptr_t)data);
    }

    void mmModuleLogger::LogView(mmUInt32_t lvl, mmUInt32_t code, const char* desc, const char* view)
    {
        mmEventDataLogger* data = NULL;

        mmSpinlock_Lock(&this->hPoolLocker);
        data = (mmEventDataLogger*)this->hPool.Produce();
        mmSpinlock_Unlock(&this->hPoolLocker);

        // copy value.
        data->hLevel = lvl;
        data->hCode = code;
        data->hDesc = desc;
        data->hView = view;

        // push to message queue.
        mmTwoLockQuque_Enqueue(&this->hTwoLockQuque, (uintptr_t)data);
    }

    void mmModuleLogger::Pop(size_t hNumber)
    {
        mmEventDataLogger* data = NULL;
        uintptr_t v = 0;
        size_t n = 0;

        while (n < hNumber && MM_TRUE == mmTwoLockQuque_Dequeue(&this->hTwoLockQuque, &v))
        {
            data = (mmEventDataLogger*)v;

            this->hEventSet.FireEvent(EventLoggerView, *data);

            mmSpinlock_Lock(&this->hPoolLocker);
            this->hPool.Recycle(data);
            mmSpinlock_Unlock(&this->hPoolLocker);

            data = NULL;

            n++;
        }
    }
    //pop queue to clear or pop number limit
    void mmModuleLogger::ThreadHandle(void)
    {
        this->Pop(this->hMaxPop);
    }
    void mmModuleLogger::OnFinishLaunching(void)
    {
        // pool chunk 64 is enough.
        this->hPool.SetChunkSize(64);
    }
    void mmModuleLogger::OnBeforeTerminate(void)
    {
        
    }
    void mmModuleLogger::OnUpdate(double hInterval)
    {
        this->ThreadHandle();
    }
}
