/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmModuleDatabase_h__
#define __mmModuleDatabase_h__

#include "core/mmCore.h"
#include "core/mmString.h"

#include "dish/mmEvent.h"

#include "nwsi/mmModule.h"

#include "database/mmDatabaseExplorer.h"

#include <string>

namespace mm
{
    class mmModuleDatabase : public mmModuleBase
    {
    public:
        struct mmDatabaseExplorer hDatabaseExplorer;
    public:
        // this member is event drive.
        mm::mmEventSet hEventSet;
    public:
        mmModuleDatabase(void);
        virtual ~mmModuleDatabase(void);
    public:
        // mmModuleBase
        virtual void OnFinishLaunching(void);
        virtual void OnBeforeTerminate(void);
    };
}

#endif//__mmModuleDatabase_h__