/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmWidgetScrollable_h__
#define __mmWidgetScrollable_h__

#include "core/mmCore.h"
#include "core/mmClock.h"

#include "container/mmRbtreeU64.h"

#include "dish/mmEvent.h"

#include "nwsi/mmEventSurface.h"

#include "layer/director/mmLayerContext.h"

#include "CEGUI/Window.h"

#include "CEGUI/widgets/SequentialLayoutContainer.h"
#include "CEGUI/widgets/ScrollablePane.h"

#include "mmWidgetTransform.h"

namespace mm
{
    class mmWidgetScrollableContainer : public CEGUI::SequentialLayoutContainer
    {
    public:
        /*************************************************************************
        Constants
        *************************************************************************/
        //! The unique typename of this widget
        static const CEGUI::String WidgetTypeName;
    public:
        /*************************************************************************
        Construction and Destruction
        *************************************************************************/
        /*!
        \brief
        Constructor for GUISheet windows.
        */
        mmWidgetScrollableContainer(const CEGUI::String& type, const CEGUI::String& name);

        /*!
        \brief
        Destructor for GUISheet windows.
        */
        virtual ~mmWidgetScrollableContainer(void);

    public:
        //! @copydoc LayoutContainer::layout
        virtual void layout(void);
    };

    class mmWidgetScrollable;

    struct mmWidgetScrollableChildrenCreator
    {
        CEGUI::Window* (*Produce)(mmWidgetScrollable* obj, size_t index);
        void (*Recycle)(mmWidgetScrollable* obj, CEGUI::Window* w);

        void (*FinishAttach)(mmWidgetScrollable* obj, CEGUI::Window* w);
        void (*BeforeDetach)(mmWidgetScrollable* obj, CEGUI::Window* w);

        void* obj;
    };

    extern const mmWidgetScrollableChildrenCreator mmWidgetScrollable_DEFAULT_CREATOR;

    class mmWidgetScrollableAnimationAcceleration
    {
    public:
        float hDuration;
        float hElapsed;
        bool hEnable;
        CEGUI::ScrollablePane* pTargetWindow;
        CEGUI::ScrolledContainer* pScrolledContainer;
        CEGUI::Scrollbar* pVertScrollbar;
        CEGUI::Scrollbar* pHorzScrollbar;
    public:
        CEGUI::Vector2f hStartPosition;
        CEGUI::Vector2f hStartSpeed;
        float hAcceleration;
    public:
        mmWidgetScrollableAnimationAcceleration(void);
        ~mmWidgetScrollableAnimationAcceleration(void);
    public:
        void SetEnabled(bool _enable);
        void SetTargetWindow(CEGUI::ScrollablePane* _window);
        void SetStartPosition(const CEGUI::Vector2f& _position);
        void SetStartSpeed(const CEGUI::Vector2f& _speed);
        void SetAcceleration(float _acceleration);
    public:
        void Play(void);
        void Stop(void);
    public:
        void Update(double interval);
    };

    class mmWidgetScrollable : public CEGUI::ScrollablePane
    {
    public:
        /*************************************************************************
        Constants
        *************************************************************************/
        //! The unique typename of this widget
        static const CEGUI::String WidgetTypeName;
    public:
        //! The container vertical layout name of this widget
        static const CEGUI::String LayoutContainerName;
    public:
        //! ChildrenSize PropertyName.
        static const CEGUI::String ChildrenSizePropertyName;
        //! IndexSize PropertyName.
        static const CEGUI::String IndexSizePropertyName;
        //! MaxInvisibleNumber PropertyName.
        static const CEGUI::String MaxInvisibleNumberPropertyName;
        //! Acceleration PropertyName.
        static const CEGUI::String AccelerationPropertyName;
        //! MaxSpeed PropertyName.
        static const CEGUI::String MaxSpeedPropertyName;
    private:
        CEGUI::Event::Connection hEventContentPaneChangedConn;
        CEGUI::Event::Connection hEventContentPaneScrolledConn;
        CEGUI::Event::Connection hEventUpdatedConn;
    private:
        mmWidgetScrollableContainer* pLayoutContainer;
        CEGUI::ScrolledContainer* pScrolledContainer;
    protected:
        CEGUI::USize hChildrenSize;
        CEGUI::USize hLayoutSize;
        // cache pixel size.
        CEGUI::Vector2f hChildrenPixelSize;

        mmUInt32_t hIndexSize;

        // default is 20.
        mmUInt32_t hMaxInvisibleNumber;
        mmUInt32_t hMaxVisibleNumber;
        // real_max_children_number = max_invisible_number + max_visible_number.
        mmUInt32_t hRealMaxChildrenNumber;

        struct mmWidgetScrollableChildrenCreator hChildrenCreator;

        bool hNeedNotifyAreaChanged;
    private:
        size_t hIndexB;
        size_t hIndexE;
    private:
        struct mmRbtreeU64Vpt hWindowRbtree;
    protected:
        struct mmClock hClock;

        CEGUI::Vector2f hPositionBegan;
        CEGUI::Vector2f hPositionMoved;
        CEGUI::Vector2f hPositionLast;
        CEGUI::UVector2 hPositionContainerBegan;
        bool hCursorEventEnabled;
        mmULong_t hTimecodeCursor;
        double hTimeCursorTouch;
    protected:
        struct mmWidgetTransform hWidgetTransform;

        CEGUI::Rectf hAreaViewableAreaVScroll;
        CEGUI::Rectf hAreaViewableAreaHScroll;
    protected:
        // acceleration m/s^2 default is 1000.0f
        float hAcceleration;
        // max speed default is 2000.0f
        float hMaxSpeed;
        // animation acceleration for scrollable.
        mmWidgetScrollableAnimationAcceleration hAnimationAcceleration;
        // average speed for animation.
        CEGUI::Vector2f hAverageSpeed;
    public:
        /*************************************************************************
        Construction and Destruction
        *************************************************************************/
        /*!
        \brief
        Constructor for GUISheet windows.
        */
        mmWidgetScrollable(const CEGUI::String& type, const CEGUI::String& name);

        /*!
        \brief
        Destructor for GUISheet windows.
        */
        virtual ~mmWidgetScrollable(void);

    public:
        mmWidgetScrollableContainer* GetLayoutContainer(void);
        struct mmRbtreeU64Vpt* GetWindowRbtree(void);
    public:
        void SetChildrenCreator(const mmWidgetScrollableChildrenCreator* pChildrenCreator);
        const mmWidgetScrollableChildrenCreator* GetChildrenCreator(void) const;
    public:
        void SetChildrenSize(const CEGUI::USize& hSize);
        const CEGUI::USize& GetChildrenSize(void) const;

        void UpdateChildrenSize(void);
        void UpdateLayoutSize(void);
    public:
        void SetIndexSize(mmUInt32_t hIndexSize);
        mmUInt32_t GetIndexSize(void) const;
    public:
        void SetMaxInvisibleNumber(mmUInt32_t hMaxInvisibleNumber);
        mmUInt32_t GetMaxInvisibleNumber(void) const;

        mmUInt32_t GetMaxVisibleNumber(void) const;
        mmUInt32_t GetRealMaxChildrenNumber(void) const;
    public:
        void SetAcceleration(float hAcceleration);
        float GetAcceleration(void) const;

        void SetMaxSpeed(float hMaxSpeed);
        float GetMaxSpeed(void) const;
    public:
        void SetScrollPosition(float hScrollPosition);
        float GetScrollPosition(void) const;
    public:
        void CurrentIndex(size_t* b, size_t* e);
    public:
        // notify manual.
        void NotifyAttributesChanged(void);
        void NotifyTrimChildrenNode(void);
        void NotifyScreenAreaChanged(void);
    public:
        void OnFinishLaunching(void);
        void OnBeforeTerminate(void);
    public:
        void OnEventCursorMoved(struct mmSurfaceContentCursor* content);
        void OnEventCursorBegan(struct mmSurfaceContentCursor* content);
        void OnEventCursorEnded(struct mmSurfaceContentCursor* content);
    public:
        // CEGUI::Window
        virtual void notifyScreenAreaChanged(bool recursive = true);
    private:
        bool OnHandleEventContentPaneChanged(const CEGUI::EventArgs& args);
        bool OnHandleEventContentPaneScrolled(const CEGUI::EventArgs& args);
        bool OnHandleEventUpdated(const CEGUI::EventArgs& args);
    protected:
        // need implement api.
        virtual void OnCalculationMaxChildrenNumber(void);
        virtual void OnCalculationLayoutSize(void);
        virtual void OnCalculationCurrentIndex(size_t* b, size_t* e);
        virtual void OnCalculationIndexChildPosition(size_t index, CEGUI::UVector2* position);
        // 
        virtual bool OnCursorInside(const CEGUI::Vector2f& hWindowPixelPosition);
        virtual void OnUpdateMoved(const CEGUI::Vector2f& hWindowPixelPosition);
        //
        virtual void OnSetScrollPosition(float hScrollPosition);
        virtual float OnGetScrollPosition(void) const;
    private:
        // [b, e)
        void HideFrontWindow(size_t b, size_t e);
        // (b, e]
        void HideAfterWindow(size_t b, size_t e);
        // [b, e)
        void ShowFrontWindow(size_t b, size_t e);
        // (b, e]
        void ShowAfterWindow(size_t b, size_t e);

        // [b, e]
        void HideRoundFrontWindow(size_t b, size_t e);
        // [b, e]
        void HideRoundAfterWindow(size_t b, size_t e);
        // [b, e]
        void ShowRoundWindow(size_t b, size_t e);
    private:
        void TryRmvFrontWindow(void);
        void TryRmvAfterWindow(void);
    private:
        CEGUI::Window* AddIndexWindow(size_t index);
        void RmvIndexWindow(size_t index);
        void ClearIndexWindow(void);
    protected:
        void UpdateTransform(void);
        void ConvertWorldPixelToWindowPixel(double x, double y, CEGUI::Vector2f* pWindowPixelPosition);
        void AnimationContainer(void);
    private:
        void AddWidgetScrollableProperties(void);
    };

    class mmWidgetScrollableX : public mmWidgetScrollable
    {
    public:
        /*************************************************************************
        Constants
        *************************************************************************/
        //! The unique typename of this widget
        static const CEGUI::String WidgetTypeName;
    public:
        /*************************************************************************
        Construction and Destruction
        *************************************************************************/
        /*!
        \brief
        Constructor for GUISheet windows.
        */
        mmWidgetScrollableX(const CEGUI::String& type, const CEGUI::String& name);

        /*!
        \brief
        Destructor for GUISheet windows.
        */
        virtual ~mmWidgetScrollableX(void);
    protected:
        // mmWidgetScrollable
        virtual void OnCalculationMaxChildrenNumber(void);
        virtual void OnCalculationLayoutSize(void);
        virtual void OnCalculationCurrentIndex(size_t* b, size_t* e);
        virtual void OnCalculationIndexChildPosition(size_t index, CEGUI::UVector2* position);
        //
        virtual bool OnCursorInside(const CEGUI::Vector2f& hWindowPixelPosition);
        virtual void OnUpdateMoved(const CEGUI::Vector2f& hWindowPixelPosition);
        //
        virtual void OnSetScrollPosition(float hScrollPosition);
        virtual float OnGetScrollPosition(void) const;
    };

    class mmWidgetScrollableY : public mmWidgetScrollable
    {
    public:
        /*************************************************************************
        Constants
        *************************************************************************/
        //! The unique typename of this widget
        static const CEGUI::String WidgetTypeName;
    public:
        /*************************************************************************
        Construction and Destruction
        *************************************************************************/
        /*!
        \brief
        Constructor for GUISheet windows.
        */
        mmWidgetScrollableY(const CEGUI::String& type, const CEGUI::String& name);

        /*!
        \brief
        Destructor for GUISheet windows.
        */
        virtual ~mmWidgetScrollableY(void);
    protected:
        // mmWidgetScrollable
        virtual void OnCalculationMaxChildrenNumber(void);
        virtual void OnCalculationLayoutSize(void);
        virtual void OnCalculationCurrentIndex(size_t* b, size_t* e);
        virtual void OnCalculationIndexChildPosition(size_t index, CEGUI::UVector2* position);
        //
        virtual bool OnCursorInside(const CEGUI::Vector2f& hWindowPixelPosition);
        virtual void OnUpdateMoved(const CEGUI::Vector2f& hWindowPixelPosition);
        //
        virtual void OnSetScrollPosition(float hScrollPosition);
        virtual float OnGetScrollPosition(void) const;
    };
}

#endif//__mmWidgetScrollable_h__