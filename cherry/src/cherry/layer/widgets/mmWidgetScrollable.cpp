/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmWidgetScrollable.h"

#include "CEGUI/WindowManager.h"
#include "CEGUI/CoordConverter.h"
#include "CEGUI/WindowFactoryManager.h"

#include "CEGUI/falagard/WidgetLookFeel.h"

#include "CEGUI/widgets/LayoutContainer.h"
#include "CEGUI/widgets/VerticalLayoutContainer.h"
#include "CEGUI/widgets/ScrolledContainer.h"
#include "CEGUI/widgets/ScrollablePane.h"
#include "CEGUI/widgets/Scrollbar.h"
#include "CEGUI/widgets/Thumb.h"

#include <math.h>

namespace mm
{
    const CEGUI::String mmWidgetScrollableContainer::WidgetTypeName("mm/mmWidgetScrollableContainer");

    mmWidgetScrollableContainer::mmWidgetScrollableContainer(const CEGUI::String& type, const CEGUI::String& name)
        : CEGUI::SequentialLayoutContainer(type, name)
    {

    }
    mmWidgetScrollableContainer::~mmWidgetScrollableContainer(void)
    {

    }

    //! @copydoc LayoutContainer::layout
    void mmWidgetScrollableContainer::layout(void)
    {
        // do nothing here.
    }

    static CEGUI::Window* __static_mmWidgetScrollableChildrenCreator_DefaultProduce(mmWidgetScrollable* obj, size_t index)
    {
        return CEGUI_NEW_AO CEGUI::Window("", "");
    }
    static void __static_mmWidgetScrollableChildrenCreator_DefaultRecycle(mmWidgetScrollable* obj, CEGUI::Window* w)
    {
        CEGUI_DELETE_AO w;
    }

    static void __static_mmWidgetScrollableChildrenCreator_DefaultFinishAttach(mmWidgetScrollable* obj, CEGUI::Window* w)
    {

    }

    static void __static_mmWidgetScrollableChildrenCreator_DefaultBeforeDetach(mmWidgetScrollable* obj, CEGUI::Window* w)
    {

    }

    const mmWidgetScrollableChildrenCreator mmWidgetScrollable_DEFAULT_CREATOR =
    {
        &__static_mmWidgetScrollableChildrenCreator_DefaultProduce,
        &__static_mmWidgetScrollableChildrenCreator_DefaultRecycle,
        &__static_mmWidgetScrollableChildrenCreator_DefaultFinishAttach,
        &__static_mmWidgetScrollableChildrenCreator_DefaultBeforeDetach,
        NULL,
    };

    mmWidgetScrollableAnimationAcceleration::mmWidgetScrollableAnimationAcceleration(void)
        : hDuration(0.0f)
        , hElapsed(0.0f)
        , hEnable(false)
        , pTargetWindow(NULL)
        , pScrolledContainer(NULL)
        , pVertScrollbar(NULL)
        , pHorzScrollbar(NULL)
        , hStartPosition(0.0f, 0.0f)
        , hStartSpeed(0.0f, 0.0f)
        , hAcceleration(0.0f)
    {

    }
    mmWidgetScrollableAnimationAcceleration::~mmWidgetScrollableAnimationAcceleration(void)
    {

    }
    void mmWidgetScrollableAnimationAcceleration::SetEnabled(bool _enable)
    {
        this->hEnable = _enable;
    }
    void mmWidgetScrollableAnimationAcceleration::SetTargetWindow(CEGUI::ScrollablePane* _window)
    {
        this->pTargetWindow = _window;
        this->pScrolledContainer = (CEGUI::ScrolledContainer*)this->pTargetWindow->getChild(CEGUI::ScrollablePane::ScrolledContainerName);
        this->pVertScrollbar = this->pTargetWindow->getVertScrollbar();
        this->pHorzScrollbar = this->pTargetWindow->getHorzScrollbar();
    }

    void mmWidgetScrollableAnimationAcceleration::SetStartPosition(const CEGUI::Vector2f& _position)
    {
        this->hStartPosition = _position;
    }
    void mmWidgetScrollableAnimationAcceleration::SetStartSpeed(const CEGUI::Vector2f& _speed)
    {
        this->hStartSpeed = _speed;
    }
    void mmWidgetScrollableAnimationAcceleration::SetAcceleration(float _acceleration)
    {
        this->hAcceleration = _acceleration;
    }

    void mmWidgetScrollableAnimationAcceleration::Play(void)
    {
        const CEGUI::UVector2& _uposition = this->pScrolledContainer->getPosition();

        this->hStartPosition.d_x = _uposition.d_x.d_offset;
        this->hStartPosition.d_y = _uposition.d_y.d_offset;

        float tx = fabs(this->hStartSpeed.d_x / this->hAcceleration);
        float ty = fabs(this->hStartSpeed.d_y / this->hAcceleration);
        this->hDuration = tx > ty ? tx : ty;
        this->hElapsed = 0;
        this->hEnable = true;
    }
    void mmWidgetScrollableAnimationAcceleration::Stop(void)
    {
        this->hEnable = false;
    }
    void mmWidgetScrollableAnimationAcceleration::Update(double interval)
    {
        if (this->hEnable && NULL != this->pTargetWindow && this->hElapsed <= hDuration)
        {
            this->hElapsed += (float)interval;

            float svx = this->hStartSpeed.d_x;
            float svy = this->hStartSpeed.d_y;
            short sign = (svx == 0) ? (svy > 0 ? 1 : -1) : (svx > 0 ? 1 : -1);
            float xy = sign * this->hAcceleration * this->hElapsed * this->hElapsed / 2.0f;
            float dx = svx == 0 ? 0 : xy;
            float dy = svy == 0 ? 0 : xy;
            float sx = svx * this->hElapsed - dx;
            float sy = svy * this->hElapsed - dy;

            float _vert_scroll_position = 0.0f;
            float _horz_scroll_position = 0.0f;

            _vert_scroll_position = this->hStartPosition.d_y + sy;
            _horz_scroll_position = this->hStartPosition.d_x + sx;

            this->pVertScrollbar->setScrollPosition(-_vert_scroll_position);
            this->pHorzScrollbar->setScrollPosition(-_horz_scroll_position);
        }
    }

    const CEGUI::String mmWidgetScrollable::WidgetTypeName("mm/mmWidgetScrollable");
    const CEGUI::String mmWidgetScrollable::LayoutContainerName("__auto_layout_container__");

    const CEGUI::String mmWidgetScrollable::ChildrenSizePropertyName("ChildrenSize");
    const CEGUI::String mmWidgetScrollable::IndexSizePropertyName("IndexSize");
    const CEGUI::String mmWidgetScrollable::MaxInvisibleNumberPropertyName("MaxInvisibleNumber");
    const CEGUI::String mmWidgetScrollable::AccelerationPropertyName("Acceleration");
    const CEGUI::String mmWidgetScrollable::MaxSpeedPropertyName("MaxSpeed");

    mmWidgetScrollable::mmWidgetScrollable(const CEGUI::String& type, const CEGUI::String& name)
        : CEGUI::ScrollablePane(type, name)
        , pLayoutContainer(NULL)
        , pScrolledContainer(NULL)
        , hChildrenSize(CEGUI::UDim(1.0f, 0.0f), CEGUI::UDim(1.0f, 0.0f))
        , hLayoutSize(CEGUI::UDim(0.0f, 0.0f), CEGUI::UDim(0.0f, 0.0f))
        , hChildrenPixelSize(0.0f, 0.0f)
        , hIndexSize(0)
        , hMaxInvisibleNumber(20)
        , hMaxVisibleNumber(0)
        , hRealMaxChildrenNumber(0)
        , hChildrenCreator(mmWidgetScrollable_DEFAULT_CREATOR)
        , hNeedNotifyAreaChanged(false)
        , hIndexB(-1)
        , hIndexE(-1)
        , hPositionBegan(0.0f, 0.0f)
        , hPositionMoved(0.0f, 0.0f)
        , hPositionLast(0.0f, 0.0f)
        , hPositionContainerBegan(CEGUI::UDim(0.0f, 0.0f), CEGUI::UDim(0.0f, 0.0f))
        , hCursorEventEnabled(false)
        , hTimecodeCursor(0)
        , hTimeCursorTouch(0.0f)
        , hAreaViewableAreaVScroll(0.0f, 0.0f, 0.0f, 0.0f)
        , hAreaViewableAreaHScroll(0.0f, 0.0f, 0.0f, 0.0f)
        , hAcceleration(1000.0f)
        , hMaxSpeed(2000.0f)
        , hAverageSpeed(0.0f, 0.0f)
    {
        mmRbtreeU64Vpt_Init(&this->hWindowRbtree);
        mmClock_Init(&this->hClock);
        mmWidgetTransform_Init(&this->hWidgetTransform);
        //
        this->AddWidgetScrollableProperties();
    }
    mmWidgetScrollable::~mmWidgetScrollable(void)
    {
        mmRbtreeU64Vpt_Destroy(&this->hWindowRbtree);
        mmClock_Destroy(&this->hClock);
        mmWidgetTransform_Destroy(&this->hWidgetTransform);
    }
    mmWidgetScrollableContainer* mmWidgetScrollable::GetLayoutContainer(void)
    {
        return this->pLayoutContainer;
    }
    struct mmRbtreeU64Vpt* mmWidgetScrollable::GetWindowRbtree(void)
    {
        return &this->hWindowRbtree;
    }
    void mmWidgetScrollable::SetChildrenCreator(const mmWidgetScrollableChildrenCreator* pChildrenCreator)
    {
        this->hChildrenCreator = *pChildrenCreator;
    }
    const mmWidgetScrollableChildrenCreator* mmWidgetScrollable::GetChildrenCreator(void) const
    {
        return &this->hChildrenCreator;
    }
    void mmWidgetScrollable::SetChildrenSize(const CEGUI::USize& hSize)
    {
        this->hChildrenSize = hSize;

        const CEGUI::Sizef& hPixelSize = this->getPixelSize();

        this->hChildrenPixelSize.d_x = hPixelSize.d_width * this->hChildrenSize.d_width.d_scale + this->hChildrenSize.d_width.d_offset;
        this->hChildrenPixelSize.d_y = hPixelSize.d_height * this->hChildrenSize.d_height.d_scale + this->hChildrenSize.d_height.d_offset;
    }
    const CEGUI::USize& mmWidgetScrollable::GetChildrenSize(void) const
    {
        return this->hChildrenSize;
    }

    void mmWidgetScrollable::UpdateChildrenSize(void)
    {
        this->OnCalculationMaxChildrenNumber();

        this->OnCalculationLayoutSize();
    }
    void mmWidgetScrollable::UpdateLayoutSize(void)
    {
        this->pLayoutContainer->setSize(this->hLayoutSize);
    }
    void mmWidgetScrollable::SetIndexSize(mmUInt32_t index_size)
    {
        this->hIndexSize = index_size;
    }
    mmUInt32_t mmWidgetScrollable::GetIndexSize(void) const
    {
        return this->hIndexSize;
    }
    void mmWidgetScrollable::SetMaxInvisibleNumber(mmUInt32_t max_invisible_number)
    {
        this->hMaxInvisibleNumber = max_invisible_number;

        this->OnCalculationMaxChildrenNumber();
    }
    mmUInt32_t mmWidgetScrollable::GetMaxInvisibleNumber(void) const
    {
        return this->hMaxInvisibleNumber;
    }

    mmUInt32_t mmWidgetScrollable::GetMaxVisibleNumber(void) const
    {
        return this->hMaxVisibleNumber;
    }
    mmUInt32_t mmWidgetScrollable::GetRealMaxChildrenNumber(void) const
    {
        return this->hRealMaxChildrenNumber;
    }
    void mmWidgetScrollable::SetAcceleration(float _acceleration)
    {
        this->hAcceleration = _acceleration;
    }
    float mmWidgetScrollable::GetAcceleration(void) const
    {
        return this->hAcceleration;
    }
    void mmWidgetScrollable::SetMaxSpeed(float _max_speed)
    {
        this->hMaxSpeed = _max_speed;
    }
    float mmWidgetScrollable::GetMaxSpeed(void) const
    {
        return this->hMaxSpeed;
    }
    void mmWidgetScrollable::SetScrollPosition(float _scroll_position)
    {
        this->OnSetScrollPosition(_scroll_position);
    }
    float mmWidgetScrollable::GetScrollPosition(void) const
    {
        return this->OnGetScrollPosition();
    }
    void mmWidgetScrollable::CurrentIndex(size_t* b, size_t* e)
    {
        *b = this->hIndexB;
        *e = this->hIndexE;
    }
    void mmWidgetScrollable::NotifyAttributesChanged(void)
    {
        this->UpdateTransform();

        this->UpdateChildrenSize();
        this->UpdateLayoutSize();

        this->OnCalculationMaxChildrenNumber();

        this->hIndexB = (size_t)(-1);
        this->hIndexE = (size_t)(-1);

        CEGUI::WindowEventArgs args(this);
        this->OnHandleEventContentPaneScrolled(args);
    }
    void mmWidgetScrollable::NotifyTrimChildrenNode(void)
    {
        CEGUI::Window* child = NULL;
        struct mmRbNode* n = NULL;
        struct mmRbtreeU64VptIterator* it = NULL;
        // max
        n = mmRb_First(&this->hWindowRbtree.rbt);
        while (NULL != n)
        {
            it = (struct mmRbtreeU64VptIterator*)mmRb_Entry(n, struct mmRbtreeU64VptIterator, n);
            // next
            n = mmRb_Next(n);

            if (it->k >= this->hIndexSize)
            {
                child = (CEGUI::Window*)it->v;

                mmRbtreeU64Vpt_Erase(&this->hWindowRbtree, it);
                //
                this->pLayoutContainer->removeChild(child);
                (*(this->hChildrenCreator.BeforeDetach))(this, child);
                (*(this->hChildrenCreator.Recycle))(this, child);
            }
            else
            {
                break;
            }
        }
    }
    void mmWidgetScrollable::NotifyScreenAreaChanged(void)
    {
        if (NULL != this->pLayoutContainer)
        {
            // this->pLayoutContainer sometime is null.
            // we check at here.
            this->UpdateChildrenSize();
            this->UpdateLayoutSize();

            this->OnCalculationMaxChildrenNumber();

            CEGUI::WindowEventArgs args(this);
            this->OnHandleEventContentPaneScrolled(args);
        }
    }
    void mmWidgetScrollable::OnFinishLaunching(void)
    {
        this->hEventContentPaneChangedConn = this->subscribeEvent(CEGUI::ScrollablePane::EventContentPaneChanged, CEGUI::Event::Subscriber(&mmWidgetScrollable::OnHandleEventContentPaneChanged, this));
        this->hEventContentPaneScrolledConn = this->subscribeEvent(CEGUI::ScrollablePane::EventContentPaneScrolled, CEGUI::Event::Subscriber(&mmWidgetScrollable::OnHandleEventContentPaneScrolled, this));
        this->hEventUpdatedConn = this->subscribeEvent(CEGUI::Window::EventUpdated, CEGUI::Event::Subscriber(&mmWidgetScrollable::OnHandleEventUpdated, this));

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        //
        this->pLayoutContainer = (mmWidgetScrollableContainer*)pWindowManager->createWindow(mmWidgetScrollableContainer::WidgetTypeName, LayoutContainerName);
        this->pLayoutContainer->setMouseInputPropagationEnabled(true);
        this->addChild(this->pLayoutContainer);

        this->pScrolledContainer = (CEGUI::ScrolledContainer*)this->getChild(CEGUI::ScrollablePane::ScrolledContainerName);

        this->UpdateTransform();

        this->UpdateChildrenSize();
        this->UpdateLayoutSize();

        this->OnCalculationMaxChildrenNumber();

        CEGUI::WindowEventArgs args(this);
        this->OnHandleEventContentPaneScrolled(args);

        this->hAnimationAcceleration.SetAcceleration(this->hAcceleration);
        this->hAnimationAcceleration.SetTargetWindow(this);
    }
    void mmWidgetScrollable::OnBeforeTerminate(void)
    {
        this->ClearIndexWindow();

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();

        this->removeChild(this->pLayoutContainer);
        pWindowManager->destroyWindow(this->pLayoutContainer);
        this->pLayoutContainer = NULL;

        this->hEventContentPaneChangedConn->disconnect();
        this->hEventContentPaneScrolledConn->disconnect();
        this->hEventUpdatedConn->disconnect();
    }
    void mmWidgetScrollable::OnEventCursorMoved(struct mmSurfaceContentCursor* content)
    {
        if (this->hCursorEventEnabled)
        {
            mmULong_t timecode_cursor = mmClock_Microseconds(&this->hClock);

            this->hTimeCursorTouch = (timecode_cursor - this->hTimecodeCursor) / (double)(MM_USEC_PER_SEC);
            this->hTimecodeCursor = timecode_cursor;

            CEGUI::Vector2f hWindowPixelPosition;

            this->ConvertWorldPixelToWindowPixel(content->abs_x, content->abs_y, &hWindowPixelPosition);

            this->OnUpdateMoved(hWindowPixelPosition);
        }
    }
    void mmWidgetScrollable::OnEventCursorBegan(struct mmSurfaceContentCursor* content)
    {
        CEGUI::Vector2f hWindowPixelPosition;

        this->ConvertWorldPixelToWindowPixel(content->abs_x, content->abs_y, &hWindowPixelPosition);

        if (this->OnCursorInside(hWindowPixelPosition))
        {
            this->hPositionBegan = hWindowPixelPosition;
            this->hPositionLast = hWindowPixelPosition;
            this->hCursorEventEnabled = true;
            this->hPositionContainerBegan = this->pScrolledContainer->getPosition();
            this->hAverageSpeed = CEGUI::Vector2f(0.0f, 0.0f);

            mmClock_Reset(&this->hClock);
            this->hTimecodeCursor = mmClock_Milliseconds(&this->hClock);

            this->hAnimationAcceleration.Stop();
        }
    }
    void mmWidgetScrollable::OnEventCursorEnded(struct mmSurfaceContentCursor* content)
    {
        if (this->hCursorEventEnabled)
        {
            this->AnimationContainer();
            this->hCursorEventEnabled = false;
        }
    }
    void mmWidgetScrollable::notifyScreenAreaChanged(bool recursive/* = true */)
    {
        this->UpdateTransform();
        this->NotifyScreenAreaChanged();

        CEGUI::ScrollablePane::notifyScreenAreaChanged(recursive);
    }
    bool mmWidgetScrollable::OnHandleEventContentPaneChanged(const CEGUI::EventArgs& args)
    {
        const CEGUI::WindowEventArgs& evt = (const CEGUI::WindowEventArgs&)(args);
        //
        CEGUI::ScrollablePane* ScrollablePane = (CEGUI::ScrollablePane*)evt.window;
        {
            CEGUI::Scrollbar* VertScrollbar = ScrollablePane->getVertScrollbar();
            CEGUI::Thumb* thumb = VertScrollbar->getThumb();
            //
            const CEGUI::WidgetLookFeel& wlf = VertScrollbar->getWindowRenderer()->getLookNFeel();
            CEGUI::Rectf area_trackta(wlf.getNamedArea("ThumbTrackArea").getArea().getPixelRect(*VertScrollbar));
            const CEGUI::Rectf& area_context = ScrollablePane->getContentPaneArea();
            const CEGUI::USize& thubm_size = thumb->getSize();
            float h_t = area_trackta.getHeight();
            float h_c = area_context.getHeight();
            float h_thumb = h_t / h_c;
            h_thumb = h_thumb > 0.0f ? h_thumb : 0.0f;
            h_thumb = h_thumb < 1.0f ? h_thumb : 1.0f;
            thumb->setSize(CEGUI::USize(thubm_size.d_width, CEGUI::UDim(h_thumb, 0)));
        }
        {
            CEGUI::Scrollbar* HorzScrollbar = ScrollablePane->getHorzScrollbar();
            CEGUI::Thumb* thumb = HorzScrollbar->getThumb();
            //
            const CEGUI::WidgetLookFeel& wlf = HorzScrollbar->getWindowRenderer()->getLookNFeel();
            CEGUI::Rectf area_trackta(wlf.getNamedArea("ThumbTrackArea").getArea().getPixelRect(*HorzScrollbar));
            const CEGUI::Rectf& area_context = ScrollablePane->getContentPaneArea();
            const CEGUI::USize& thubm_size = thumb->getSize();
            float h_t = area_trackta.getWidth();
            float h_c = area_context.getWidth();
            float h_thumb = h_t / h_c;
            h_thumb = h_thumb > 0.0f ? h_thumb : 0.0f;
            h_thumb = h_thumb < 1.0f ? h_thumb : 1.0f;
            thumb->setSize(CEGUI::USize(CEGUI::UDim(h_thumb, 0), thubm_size.d_height));
        }
        return true;
    }
    bool mmWidgetScrollable::OnHandleEventContentPaneScrolled(const CEGUI::EventArgs& args)
    {
        size_t b = 0;
        size_t e = 0;
        size_t max_index = this->hIndexSize - 1;

        this->OnCalculationCurrentIndex(&b, &e);

        if (0 != this->hIndexSize)
        {
            b = b < max_index ? b : max_index;
            e = e < max_index ? e : max_index;

            if (this->hIndexE >= b && e >= this->hIndexB)
            {
                // [b, e)
                this->HideFrontWindow(this->hIndexB, b);
                // (b, e]
                this->HideAfterWindow(e, this->hIndexE);
                // [b, e)
                this->ShowFrontWindow(b, this->hIndexB);
                // (b, e]
                this->ShowAfterWindow(this->hIndexE, e);
            }
            else
            {
                if (this->hIndexB < b)
                {
                    // [b, e]
                    this->HideRoundFrontWindow(this->hIndexB, this->hIndexE);
                }
                else
                {
                    // [b, e]
                    this->HideRoundAfterWindow(this->hIndexB, this->hIndexE);
                }
                // [b, e]
                this->ShowRoundWindow(b, e);
            }

            this->hIndexB = b;
            this->hIndexE = e;

            if (this->hNeedNotifyAreaChanged)
            {
                this->hNeedNotifyAreaChanged = false;
                // we need manual notify screen area is changed to invalid all cache data.
                this->pScrolledContainer->notifyScreenAreaChanged(true);
            }
        }

        return true;
    }
    bool mmWidgetScrollable::OnHandleEventUpdated(const CEGUI::EventArgs& args)
    {
        const CEGUI::UpdateEventArgs& evt = (const CEGUI::UpdateEventArgs&)(args);
        this->hAnimationAcceleration.Update(evt.d_timeSinceLastFrame);
        return true;
    }
    void mmWidgetScrollable::OnCalculationMaxChildrenNumber(void)
    {
        assert(0 && "not implement api.");
    }
    void mmWidgetScrollable::OnCalculationLayoutSize(void)
    {
        assert(0 && "not implement api.");
    }
    void mmWidgetScrollable::OnCalculationCurrentIndex(size_t* b, size_t* e)
    {
        assert(0 && "not implement api.");
    }
    void mmWidgetScrollable::OnCalculationIndexChildPosition(size_t index, CEGUI::UVector2* position)
    {
        assert(0 && "not implement api.");
    }
    bool mmWidgetScrollable::OnCursorInside(const CEGUI::Vector2f& _window_pixel_position)
    {
        assert(0 && "not implement api.");
        return false;
    }
    void mmWidgetScrollable::OnUpdateMoved(const CEGUI::Vector2f& _window_pixel_position)
    {
        assert(0 && "not implement api.");
    }
    void mmWidgetScrollable::OnSetScrollPosition(float _scroll_position)
    {
        assert(0 && "not implement api.");
    }
    float mmWidgetScrollable::OnGetScrollPosition(void) const
    {
        assert(0 && "not implement api.");
        return 0;
    }
    void mmWidgetScrollable::HideFrontWindow(size_t b, size_t e)
    {
        // [b, e)
        if (b < e)
        {
            size_t i = 0;
            CEGUI::Window* child = NULL;
            struct mmRbNode* n = NULL;
            struct mmRbtreeU64VptIterator* it = NULL;

            i = b;
            it = mmRbtreeU64Vpt_GetIterator(&this->hWindowRbtree, (mmUInt64_t)i);
            if (NULL != it)
            {
                n = &it->n;

                while (i < e && i != (size_t)(-1) && NULL != n)
                {
                    it = (struct mmRbtreeU64VptIterator*)mmRb_Entry(n, struct mmRbtreeU64VptIterator, n);
                    // prev
                    n = mmRb_Prev(n);

                    child = (CEGUI::Window*)it->v;
                    child->setVisible(false);

                    this->TryRmvFrontWindow();

                    i++;
                }
            }
        }
    }
    void mmWidgetScrollable::HideAfterWindow(size_t b, size_t e)
    {
        // (b, e]
        if (b < e)
        {
            size_t i = 0;
            CEGUI::Window* child = NULL;
            struct mmRbNode* n = NULL;
            struct mmRbtreeU64VptIterator* it = NULL;

            i = e;
            it = mmRbtreeU64Vpt_GetIterator(&this->hWindowRbtree, (mmUInt64_t)i);
            if (NULL != it)
            {
                n = &it->n;

                while (i > b && i != (size_t)(-1) && NULL != n)
                {
                    it = (struct mmRbtreeU64VptIterator*)mmRb_Entry(n, struct mmRbtreeU64VptIterator, n);
                    // next
                    n = mmRb_Next(n);

                    child = (CEGUI::Window*)it->v;
                    child->setVisible(false);

                    this->TryRmvAfterWindow();

                    i--;
                }
            }
        }
    }
    void mmWidgetScrollable::ShowFrontWindow(size_t b, size_t e)
    {
        // [b, e)
        if (b < e)
        {
            size_t i = 0;
            CEGUI::Window* child = NULL;
            // struct mmRbNode* n = NULL;
            struct mmRbtreeU64VptIterator* it = NULL;

            i = b;
            while (i < e && i != (size_t)(-1))
            {
                it = mmRbtreeU64Vpt_GetIterator(&this->hWindowRbtree, (mmUInt64_t)i);
                if (NULL != it)
                {
                    child = (CEGUI::Window*)it->v;
                    child->setVisible(true);
                }
                else
                {
                    child = this->AddIndexWindow(i);
                    child->setVisible(true);
                }

                i++;
            }
        }
    }
    void mmWidgetScrollable::ShowAfterWindow(size_t b, size_t e)
    {
        // (b, e]
        if (b < e)
        {
            size_t i = 0;
            CEGUI::Window* child = NULL;
            // struct mmRbNode* n = NULL;
            struct mmRbtreeU64VptIterator* it = NULL;

            i = e;
            while (i > b && i != (size_t)(-1))
            {
                it = mmRbtreeU64Vpt_GetIterator(&this->hWindowRbtree, (mmUInt64_t)i);
                if (NULL != it)
                {
                    child = (CEGUI::Window*)it->v;
                    child->setVisible(true);
                }
                else
                {
                    child = this->AddIndexWindow(i);
                    child->setVisible(true);
                }

                i--;
            }
        }
    }
    // [b, e]
    void mmWidgetScrollable::HideRoundFrontWindow(size_t b, size_t e)
    {
        // [b, e]
        if (b <= e)
        {
            size_t i = 0;
            CEGUI::Window* child = NULL;
            struct mmRbtreeU64VptIterator* it = NULL;

            i = b;
            while (i <= e && i != (size_t)(-1))
            {
                it = mmRbtreeU64Vpt_GetIterator(&this->hWindowRbtree, (mmUInt64_t)i);
                if (NULL != it)
                {
                    child = (CEGUI::Window*)it->v;
                    child->setVisible(false);

                    this->TryRmvFrontWindow();
                }

                i++;
            }
        }
    }
    void mmWidgetScrollable::HideRoundAfterWindow(size_t b, size_t e)
    {
        // [b, e]
        if (b <= e)
        {
            size_t i = 0;
            CEGUI::Window* child = NULL;
            struct mmRbtreeU64VptIterator* it = NULL;

            i = e;
            while (i >= b && i != (size_t)(-1))
            {
                it = mmRbtreeU64Vpt_GetIterator(&this->hWindowRbtree, (mmUInt64_t)i);
                if (NULL != it)
                {
                    child = (CEGUI::Window*)it->v;
                    child->setVisible(false);

                    this->TryRmvAfterWindow();
                }

                i--;
            }
        }
    }
    void mmWidgetScrollable::ShowRoundWindow(size_t b, size_t e)
    {
        // [b, e]
        if (b <= e)
        {
            size_t i = 0;
            CEGUI::Window* child = NULL;
            struct mmRbtreeU64VptIterator* it = NULL;

            i = b;
            while (i <= e && i != (size_t)(-1))
            {
                it = mmRbtreeU64Vpt_GetIterator(&this->hWindowRbtree, (mmUInt64_t)i);
                if (NULL != it)
                {
                    child = (CEGUI::Window*)it->v;
                    child->setVisible(true);
                }
                else
                {
                    child = this->AddIndexWindow(i);
                    child->setVisible(true);
                }

                i++;
            }
        }
    }
    void mmWidgetScrollable::TryRmvFrontWindow(void)
    {
        size_t child_count = this->hWindowRbtree.size;
        if (child_count > this->hRealMaxChildrenNumber && 0 < child_count)
        {
            struct mmRbNode* n = NULL;
            struct mmRbtreeU64VptIterator* it = NULL;
            CEGUI::Window* child = NULL;
            size_t key = 0;
            // min
            n = mmRb_Last(&this->hWindowRbtree.rbt);
            it = (struct mmRbtreeU64VptIterator*)mmRb_Entry(n, struct mmRbtreeU64VptIterator, n);

            child = (CEGUI::Window*)it->v;
            key = (size_t)it->k;

            this->RmvIndexWindow(key);
        }
    }
    void mmWidgetScrollable::TryRmvAfterWindow(void)
    {
        size_t child_count = this->hWindowRbtree.size;
        if (child_count > this->hRealMaxChildrenNumber && 0 < child_count)
        {
            struct mmRbNode* n = NULL;
            struct mmRbtreeU64VptIterator* it = NULL;
            CEGUI::Window* child = NULL;
            size_t key = 0;
            // max
            n = mmRb_First(&this->hWindowRbtree.rbt);
            it = (struct mmRbtreeU64VptIterator*)mmRb_Entry(n, struct mmRbtreeU64VptIterator, n);

            child = (CEGUI::Window*)it->v;
            key = (size_t)it->k;

            this->RmvIndexWindow(key);
        }
    }
    CEGUI::Window* mmWidgetScrollable::AddIndexWindow(size_t index)
    {
        CEGUI::Window* child = NULL;
        struct mmRbtreeU64VptIterator* it = NULL;

        it = mmRbtreeU64Vpt_GetIterator(&this->hWindowRbtree, (mmUInt64_t)index);
        if (NULL != it)
        {
            child = (CEGUI::Window*)it->v;
        }
        else
        {
            CEGUI::UVector2 position;
            this->OnCalculationIndexChildPosition(index, &position);
            child = (*(this->hChildrenCreator.Produce))(this, index);
            this->pLayoutContainer->addChild(child);
            child->setPosition(position);
            child->setSize(this->hChildrenSize);
            (*(this->hChildrenCreator.FinishAttach))(this, child);
            this->hNeedNotifyAreaChanged = true;
            //
            mmRbtreeU64Vpt_Set(&this->hWindowRbtree, (mmUInt64_t)index, child);
        }

        return child;
    }
    void mmWidgetScrollable::RmvIndexWindow(size_t index)
    {
        CEGUI::Window* child = NULL;
        struct mmRbtreeU64VptIterator* it = NULL;

        it = mmRbtreeU64Vpt_GetIterator(&this->hWindowRbtree, (mmUInt64_t)index);
        if (NULL != it)
        {
            child = (CEGUI::Window*)it->v;
            //
            mmRbtreeU64Vpt_Erase(&this->hWindowRbtree, it);
            //
            this->pLayoutContainer->removeChild(child);
            (*(this->hChildrenCreator.BeforeDetach))(this, child);
            (*(this->hChildrenCreator.Recycle))(this, child);
        }
    }
    void mmWidgetScrollable::ClearIndexWindow(void)
    {
        CEGUI::Window* child = NULL;
        struct mmRbNode* n = NULL;
        struct mmRbtreeU64VptIterator* it = NULL;
        // max
        n = mmRb_First(&this->hWindowRbtree.rbt);
        while (NULL != n)
        {
            it = (struct mmRbtreeU64VptIterator*)mmRb_Entry(n, struct mmRbtreeU64VptIterator, n);
            // next
            n = mmRb_Next(n);

            child = (CEGUI::Window*)it->v;

            mmRbtreeU64Vpt_Erase(&this->hWindowRbtree, it);
            //
            this->pLayoutContainer->removeChild(child);
            (*(this->hChildrenCreator.BeforeDetach))(this, child);
            (*(this->hChildrenCreator.Recycle))(this, child);
        }
    }
    void mmWidgetScrollable::UpdateTransform(void)
    {
        mmWidgetTransform_UpdateWindow(&this->hWidgetTransform, this);

        const CEGUI::WidgetLookFeel& wlf = this->getWindowRenderer()->getLookNFeel();

        this->hAreaViewableAreaVScroll = wlf.getNamedArea("ViewableAreaVScroll").getArea().getPixelRect(*this);
        this->hAreaViewableAreaHScroll = wlf.getNamedArea("ViewableAreaHScroll").getArea().getPixelRect(*this);

        const CEGUI::Sizef& _PixelSize = this->getPixelSize();

        this->hChildrenPixelSize.d_x = _PixelSize.d_width * this->hChildrenSize.d_width.d_scale + this->hChildrenSize.d_width.d_offset;
        this->hChildrenPixelSize.d_y = _PixelSize.d_height * this->hChildrenSize.d_height.d_scale + this->hChildrenSize.d_height.d_offset;
    }
    void mmWidgetScrollable::ConvertWorldPixelToWindowPixel(double x, double y, CEGUI::Vector2f* pWindowPixelPosition)
    {
        float hPosition[2] = { 0 };
        mmWidgetTransform_WorldPixelToWindowPixelPosition(&this->hWidgetTransform, x, y, hPosition);
        pWindowPixelPosition->d_x = hPosition[0];
        pWindowPixelPosition->d_y = hPosition[1];
    }
    void mmWidgetScrollable::AnimationContainer(void)
    {
        if (0 < this->hTimeCursorTouch)
        {
            this->hAnimationAcceleration.SetStartSpeed(this->hAverageSpeed);
            this->hAnimationAcceleration.Play();
        }

        this->hPositionMoved = CEGUI::Vector2f(0.0f, 0.0f);
        this->hTimeCursorTouch = 0.0f;
    }
    void mmWidgetScrollable::AddWidgetScrollableProperties(void)
    {
        const CEGUI::String propertyOrigin("mmWidgetScrollable");

        CEGUI_DEFINE_PROPERTY
        (
            mmWidgetScrollable, CEGUI::USize,
            ChildrenSizePropertyName, "Property to get/set the window children_size. Value is a \"USize\".",
            &mmWidgetScrollable::SetChildrenSize, &mmWidgetScrollable::GetChildrenSize, CEGUI::USize(CEGUI::UDim(1.0f, 0.0f), CEGUI::UDim(1.0f, 0.0f))
        );

        CEGUI_DEFINE_PROPERTY
        (
            mmWidgetScrollable, mmUInt32_t,
            IndexSizePropertyName, "Property to get/set the window index size. Value is a uint32_t.",
            &mmWidgetScrollable::SetIndexSize, &mmWidgetScrollable::GetIndexSize, 0
        );

        CEGUI_DEFINE_PROPERTY
        (
            mmWidgetScrollable, mmUInt32_t,
            MaxInvisibleNumberPropertyName, "Property to get/set the window max invisible number. Value is a uint32_t.",
            &mmWidgetScrollable::SetMaxInvisibleNumber, &mmWidgetScrollable::GetMaxInvisibleNumber, 20
        );

        CEGUI_DEFINE_PROPERTY
        (
            mmWidgetScrollable, float,
            AccelerationPropertyName, "Property to get/set the window acceleration. Value is a float.",
            &mmWidgetScrollable::SetAcceleration, &mmWidgetScrollable::GetAcceleration, 1000.0f
        );

        CEGUI_DEFINE_PROPERTY
        (
            mmWidgetScrollable, float,
            MaxSpeedPropertyName, "Property to get/set the window max speed. Value is a float.",
            &mmWidgetScrollable::SetMaxSpeed, &mmWidgetScrollable::GetMaxSpeed, 2000.0f
        );
    }

    const CEGUI::String mmWidgetScrollableX::WidgetTypeName("mm/mmWidgetScrollableX");
    mmWidgetScrollableX::mmWidgetScrollableX(const CEGUI::String& type, const CEGUI::String& name)
        : mmWidgetScrollable(type, name)
    {

    }

    mmWidgetScrollableX::~mmWidgetScrollableX(void)
    {

    }
    void mmWidgetScrollableX::OnCalculationMaxChildrenNumber(void)
    {
        const CEGUI::Sizef& _sizef_pixel_size = this->getPixelSize();

        float h = this->hChildrenPixelSize.d_x;
        float n = (0 == h) ? 0 : (_sizef_pixel_size.d_width / h);

        this->hMaxVisibleNumber = (mmUInt32_t)floor(n) + 2;
        this->hRealMaxChildrenNumber = this->hMaxInvisibleNumber + this->hMaxVisibleNumber;
    }
    void mmWidgetScrollableX::OnCalculationLayoutSize(void)
    {
        CEGUI::UDim layoutH(0, 0);
        CEGUI::UDim layoutW(0, 0);

        layoutH = this->hChildrenSize.d_height;
        layoutW = this->hChildrenSize.d_width * (float)this->hIndexSize;

        this->hLayoutSize = CEGUI::USize(layoutW, layoutH);
    }
    void mmWidgetScrollableX::OnCalculationCurrentIndex(size_t* b, size_t* e)
    {
        const CEGUI::ScrolledContainer* _scrolled_container = this->getContentPane();

        const CEGUI::URect& _area = _scrolled_container->getArea();
        const CEGUI::Sizef& _area_pane = this->getPixelSize();

        float _position_x = _area.d_min.d_x.d_offset;
        float _panel_x = _area_pane.d_width;
        float _child_x = this->hChildrenPixelSize.d_x;

        mmSInt64_t index_b = 0;
        mmSInt64_t index_e = 0;

        double cel_v = (double)_child_x;
        double all_w = (double)(-_position_x);
        double all_h = (double)(_panel_x - _position_x - _child_x);
        double n_b = all_w / cel_v;
        double n_e = all_h / cel_v;
        index_b = (mmSInt64_t)floor(n_b);
        index_e = (mmSInt64_t)ceil(n_e);
        index_b = index_b < 0 ? 0 : index_b;
        index_e = index_e < 0 ? 0 : index_e;

        (*b) = (size_t)index_b;
        (*e) = (size_t)index_e;
    }
    void mmWidgetScrollableX::OnCalculationIndexChildPosition(size_t index, CEGUI::UVector2* position)
    {
        position->d_x = this->hChildrenSize.d_width * (float)index;
        position->d_y = CEGUI::UDim(0, 0);
    }
    bool mmWidgetScrollableX::OnCursorInside(const CEGUI::Vector2f& _window_pixel_position)
    {
        return this->hAreaViewableAreaHScroll.isPointInRect(_window_pixel_position);
    }
    void mmWidgetScrollableX::OnUpdateMoved(const CEGUI::Vector2f& _window_pixel_position)
    {
        CEGUI::Vector2f _moved_interval_position;

        _moved_interval_position = _window_pixel_position - this->hPositionBegan;
        _moved_interval_position.d_y = 0.0f;

        this->hPositionMoved = _window_pixel_position - this->hPositionLast;
        this->hPositionMoved.d_y = 0.0f;

        float viewH = this->getPixelSize().d_width;
        float listH = this->hLayoutSize.d_width.d_scale * viewH + this->hLayoutSize.d_width.d_offset;

        if (listH > viewH)
        {
            CEGUI::Scrollbar* HorzScrollbar = this->getHorzScrollbar();

            float _scroll_position = 0.0f;

            _scroll_position = this->hPositionContainerBegan.d_x.d_offset + _moved_interval_position.d_x;

            HorzScrollbar->setScrollPosition(-_scroll_position);

            if (0 < this->hTimeCursorTouch)
            {
                // average speed.
                float s = this->hPositionMoved.d_x;
                float v = s / (float)this->hTimeCursorTouch;
                short sign = v < 0 ? -1 : 1;
                v = fabs(v);
                v = (v > this->hMaxSpeed) ? this->hMaxSpeed : v;

                CEGUI::Vector2f _speed;

                _speed.d_x = v * sign;
                _speed.d_y = 0;

                this->hAverageSpeed = (this->hAverageSpeed + _speed) / 2.0f;
            }
        }
        else
        {
            this->hPositionMoved.d_x = 0;
            this->hPositionMoved.d_y = 0;
        }

        this->hPositionLast = _window_pixel_position;
    }
    void mmWidgetScrollableX::OnSetScrollPosition(float _scroll_position)
    {
        CEGUI::Scrollbar* HorzScrollbar = this->getHorzScrollbar();

        HorzScrollbar->setScrollPosition(_scroll_position);
    }
    float mmWidgetScrollableX::OnGetScrollPosition(void) const
    {
        CEGUI::Scrollbar* HorzScrollbar = this->getHorzScrollbar();

        return HorzScrollbar->getScrollPosition();
    }

    const CEGUI::String mmWidgetScrollableY::WidgetTypeName("mm/mmWidgetScrollableY");
    mmWidgetScrollableY::mmWidgetScrollableY(const CEGUI::String& type, const CEGUI::String& name)
        : mmWidgetScrollable(type, name)
    {

    }

    mmWidgetScrollableY::~mmWidgetScrollableY(void)
    {

    }
    void mmWidgetScrollableY::OnCalculationMaxChildrenNumber(void)
    {
        const CEGUI::Sizef& _sizef_pixel_size = this->getPixelSize();

        float h = this->hChildrenPixelSize.d_y;
        float n = (0 == h) ? 0 : (_sizef_pixel_size.d_height / h);

        this->hMaxVisibleNumber = (mmUInt32_t)floor(n) + 2;
        this->hRealMaxChildrenNumber = this->hMaxInvisibleNumber + this->hMaxVisibleNumber;
    }
    void mmWidgetScrollableY::OnCalculationLayoutSize(void)
    {
        CEGUI::UDim layoutH(0, 0);
        CEGUI::UDim layoutW(0, 0);

        layoutH = this->hChildrenSize.d_height * (float)this->hIndexSize;
        layoutW = this->hChildrenSize.d_width;

        this->hLayoutSize = CEGUI::USize(layoutW, layoutH);
    }
    void mmWidgetScrollableY::OnCalculationCurrentIndex(size_t* b, size_t* e)
    {
        const CEGUI::ScrolledContainer* _scrolled_container = this->getContentPane();

        const CEGUI::URect& _area = _scrolled_container->getArea();
        const CEGUI::Sizef& _area_pane = this->getPixelSize();

        float _position_y = _area.d_min.d_y.d_offset;
        float _panel_y = _area_pane.d_height;
        float _child_y = this->hChildrenPixelSize.d_y;

        mmSInt64_t index_b = 0;
        mmSInt64_t index_e = 0;

        double cel_v = (double)_child_y;
        double all_w = (double)(-_position_y);
        double all_h = (double)(_panel_y - _position_y - _child_y);
        double n_b = all_w / cel_v;
        double n_e = all_h / cel_v;
        index_b = (mmSInt64_t)floor(n_b);
        index_e = (mmSInt64_t)ceil(n_e);
        index_b = index_b < 0 ? 0 : index_b;
        index_e = index_e < 0 ? 0 : index_e;

        (*b) = (size_t)index_b;
        (*e) = (size_t)index_e;
    }
    void mmWidgetScrollableY::OnCalculationIndexChildPosition(size_t index, CEGUI::UVector2* position)
    {
        position->d_x = CEGUI::UDim(0, 0);
        position->d_y = this->hChildrenSize.d_height * (float)index;
    }
    bool mmWidgetScrollableY::OnCursorInside(const CEGUI::Vector2f& _window_pixel_position)
    {
        return this->hAreaViewableAreaVScroll.isPointInRect(_window_pixel_position);
    }
    void mmWidgetScrollableY::OnUpdateMoved(const CEGUI::Vector2f& _window_pixel_position)
    {
        CEGUI::Vector2f _moved_interval_position;

        _moved_interval_position = _window_pixel_position - this->hPositionBegan;
        _moved_interval_position.d_x = 0.0f;

        this->hPositionMoved = _window_pixel_position - this->hPositionLast;
        this->hPositionMoved.d_x = 0.0f;

        float viewH = this->getPixelSize().d_height;
        float listH = this->hLayoutSize.d_height.d_scale * viewH + this->hLayoutSize.d_height.d_offset;

        if (listH > viewH)
        {
            CEGUI::Scrollbar* VertScrollbar = this->getVertScrollbar();

            float _scroll_position = 0.0f;

            _scroll_position = this->hPositionContainerBegan.d_y.d_offset + _moved_interval_position.d_y;

            VertScrollbar->setScrollPosition(-_scroll_position);

            if (0 < this->hTimeCursorTouch)
            {
                // average speed.
                float s = this->hPositionMoved.d_y;
                float v = s / (float)this->hTimeCursorTouch;
                short sign = v < 0 ? -1 : 1;
                v = fabs(v);
                v = (v > this->hMaxSpeed) ? this->hMaxSpeed : v;

                CEGUI::Vector2f _speed;

                _speed.d_x = 0;
                _speed.d_y = v * sign;

                this->hAverageSpeed = (this->hAverageSpeed + _speed) / 2.0f;
            }
        }
        else
        {
            this->hPositionMoved.d_x = 0;
            this->hPositionMoved.d_y = 0;
        }

        this->hPositionLast = _window_pixel_position;
    }
    void mmWidgetScrollableY::OnSetScrollPosition(float _scroll_position)
    {
        CEGUI::Scrollbar* VertScrollbar = this->getVertScrollbar();

        VertScrollbar->setScrollPosition(_scroll_position);
    }
    float mmWidgetScrollableY::OnGetScrollPosition() const
    {
        CEGUI::Scrollbar* VertScrollbar = this->getVertScrollbar();

        return VertScrollbar->getScrollPosition();
    }
}
