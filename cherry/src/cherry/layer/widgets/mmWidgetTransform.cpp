/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmWidgetTransform.h"

#include "CEGUI/CoordConverter.h"

void mmWidgetTransform_Init(struct mmWidgetTransform* p)
{
    p->Matrix4x4WindowPixelToWorldPixel = Ogre::Matrix4::IDENTITY;
    p->Matrix4x4WorldPixelToWindowPixel = Ogre::Matrix4::IDENTITY;
}
void mmWidgetTransform_Destroy(struct mmWidgetTransform* p)
{
    p->Matrix4x4WindowPixelToWorldPixel = Ogre::Matrix4::IDENTITY;
    p->Matrix4x4WorldPixelToWindowPixel = Ogre::Matrix4::IDENTITY;
}
void mmWidgetTransform_UpdateWindow(struct mmWidgetTransform* p, CEGUI::Window* pWindow)
{
    p->Matrix4x4WindowPixelToWorldPixel = Ogre::Matrix4::IDENTITY;

    mmWidgetTransform_Matrix4x4WindowPixelToWorldPixel(pWindow, &p->Matrix4x4WindowPixelToWorldPixel);

    p->Matrix4x4WorldPixelToWindowPixel = p->Matrix4x4WindowPixelToWorldPixel.inverse();
}

void mmWidgetTransform_WorldPixelToWindowPixelPosition(struct mmWidgetTransform* p, double x, double y, float hPosition[2])
{
    Ogre::Vector3 hWorldPosition;
    Ogre::Vector3 hWindowPosition;

    hWorldPosition.x = (float)x;
    hWorldPosition.y = (float)y;
    hWorldPosition.z = (float)0.0f;

    hWindowPosition = p->Matrix4x4WorldPixelToWindowPixel * hWorldPosition;

    hPosition[0] = hWindowPosition.x;
    hPosition[1] = hWindowPosition.y;
}
void mmWidgetTransform_WindowPixelToWorldPixelPosition(struct mmWidgetTransform* p, double x, double y, float hPosition[2])
{
    Ogre::Vector3 hWorldPosition;
    Ogre::Vector3 hWindowPosition;

    hWindowPosition.x = (float)x;
    hWindowPosition.y = (float)y;
    hWindowPosition.z = (float)0.0f;

    hWorldPosition = p->Matrix4x4WindowPixelToWorldPixel * hWindowPosition;

    hPosition[0] = hWorldPosition.x;
    hPosition[1] = hWorldPosition.y;
}

void mmWidgetTransform_WindowPixelSize(CEGUI::Window* pWindow, float hPixelSize[2])
{
    if (NULL != pWindow)
    {
        const CEGUI::Sizef& hWindowPixelSize = pWindow->getPixelSize();
        hPixelSize[0] = hWindowPixelSize.d_width;
        hPixelSize[1] = hWindowPixelSize.d_height;
    }
    else
    {
        hPixelSize[0] = 0.0f;
        hPixelSize[1] = 0.0f;
    }
}
void mmWidgetTransform_ParentPixelSize(CEGUI::Window* pWindow, float hParentPixelSize[2])
{
    const CEGUI::Window* pParent = pWindow->getParent();

    CEGUI::Sizef hParentSize;
    if (NULL != pParent)
    {
        hParentSize = pParent->getPixelSize();
    }
    else
    {
        hParentSize = pWindow->getRootContainerSize();
    }
    hParentPixelSize[0] = hParentSize.d_width;
    hParentPixelSize[1] = hParentSize.d_height;
}

void mmWidgetTransform_WindowToParentRect(CEGUI::Window* pWindow, float hRect[4])
{
    if (NULL != pWindow)
    {
        const CEGUI::Window* pParent = pWindow->getParent();
        const CEGUI::URect& hArea = pWindow->getArea();
        const CEGUI::Sizef& hPixelSize = pWindow->getPixelSize();
        CEGUI::HorizontalAlignment hHa = pWindow->getHorizontalAlignment();
        CEGUI::VerticalAlignment hVa = pWindow->getVerticalAlignment();

        float hParentPixelSize[2] = { 0 };
        float hWindowPixelSize[2] = { 0 };
        float hAlignment[2] = { 0 };
        float hBase[2] = { 0 };

        mmWidgetTransform_ParentPixelSize(pWindow, hParentPixelSize);

        // HA_LEFT HA_CENTRE HA_RIGHT
        // 0       1         2
        hAlignment[0] = hHa * 0.5f;
        // VA_TOP  VA_CENTRE VA_BOTTOM
        // 0       1         2
        hAlignment[1] = hVa * 0.5f;

        hWindowPixelSize[0] = hPixelSize.d_width;
        hWindowPixelSize[1] = hPixelSize.d_height;

        hBase[0] = 0.0f;
        hBase[1] = 0.0f;

        hBase[0] += CEGUI::CoordConverter::asAbsolute(hArea.d_min.d_x, hParentPixelSize[0]);
        hBase[1] += CEGUI::CoordConverter::asAbsolute(hArea.d_min.d_y, hParentPixelSize[1]);

        hBase[0] += (hParentPixelSize[0] - hWindowPixelSize[0]) * hAlignment[0];
        hBase[1] += (hParentPixelSize[1] - hWindowPixelSize[1]) * hAlignment[1];

        hRect[0] = hBase[0];
        hRect[1] = hBase[1];
        hRect[2] = hWindowPixelSize[0];
        hRect[3] = hWindowPixelSize[1];
    }
    else
    {
        hRect[0] = 0.0f;
        hRect[1] = 0.0f;
        hRect[2] = 0.0f;
        hRect[3] = 0.0f;
    }
}

void mmWidgetTransform_Matrix4x4WindowScaleToWindowPixel(CEGUI::Window* pWindow, Ogre::Matrix4* pMatrix4x4)
{
    if (NULL != pWindow)
    {
        const CEGUI::Sizef& hPixelSize = pWindow->getPixelSize();

        float sx = (float)(hPixelSize.d_width);
        float sy = (float)(hPixelSize.d_height);

        Ogre::Vector3 hPosition(0.0f, 0.0f, 0.0f);
        Ogre::Vector3 hScale(sx, sy, 1.0f);

        pMatrix4x4->makeTransform(hPosition, hScale, Ogre::Quaternion::IDENTITY);
    }
    else
    {
        (*pMatrix4x4) = Ogre::Matrix4::IDENTITY;
    }
}

void mmWidgetTransform_Matrix4x4WindowPixelToWindowScale(CEGUI::Window* pWindow, Ogre::Matrix4* pMatrix4x4)
{
    if (NULL != pWindow)
    {
        const CEGUI::Sizef& hPixelSize = pWindow->getPixelSize();

        float sx = (float)(1.0f / hPixelSize.d_width );
        float sy = (float)(1.0f / hPixelSize.d_height);

        Ogre::Vector3 hPosition(0.0f, 0.0f, 0.0f);
        Ogre::Vector3 hScale(sx, sy, 1.0f);

        pMatrix4x4->makeTransform(hPosition, hScale, Ogre::Quaternion::IDENTITY);
    }
    else
    {
        (*pMatrix4x4) = Ogre::Matrix4::IDENTITY;
    }
}
// window Pixel => parent Pixel.
void mmWidgetTransform_Matrix4x4WindowPixelToParentPixel(CEGUI::Window* pWindow, Ogre::Matrix4* pMatrix4x4)
{
    if (NULL != pWindow)
    {
        const CEGUI::Quaternion& hQuat = pWindow->getRotation();

        float hWindowRect[4] = { 0 };
        mmWidgetTransform_WindowToParentRect(pWindow, hWindowRect);

        Ogre::Vector3 hPosition(hWindowRect[0], hWindowRect[1], 0.0f);
        Ogre::Vector3 hScale(1.0, 1.0, 1.0f);
        Ogre::Quaternion hQrientation(hQuat.d_w, hQuat.d_x, hQuat.d_y, hQuat.d_z);

        pMatrix4x4->makeTransform(hPosition, hScale, hQrientation);
    }
    else
    {
        (*pMatrix4x4) = Ogre::Matrix4::IDENTITY;
    }
}
// window Pixel => world Pixel.
void mmWidgetTransform_Matrix4x4WindowPixelToWorldPixel(CEGUI::Window* pWindow, Ogre::Matrix4* pMatrix4x4)
{
    if (NULL != pWindow)
    {
        CEGUI::Window* pParent = pWindow->getParent();

        Ogre::Matrix4 hMatrix4x4Window;
        Ogre::Matrix4 hMatrix4x4Parent;

        mmWidgetTransform_Matrix4x4WindowPixelToParentPixel(pWindow, &hMatrix4x4Window);
        mmWidgetTransform_Matrix4x4WindowPixelToWorldPixel(pParent, &hMatrix4x4Parent);

        (*pMatrix4x4) = hMatrix4x4Parent * hMatrix4x4Window;
    }
    else
    {
        (*pMatrix4x4) = Ogre::Matrix4::IDENTITY;
    }
}
