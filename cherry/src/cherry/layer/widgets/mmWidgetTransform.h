/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmWidgetTransform_h__
#define __mmWidgetTransform_h__

#include "CEGUI/Window.h"

#include "OgreMatrix4.h"

struct mmWidgetTransform
{
    // window pixel => world pixel.
    Ogre::Matrix4 Matrix4x4WindowPixelToWorldPixel;
    // world pixel => window pixel.
    Ogre::Matrix4 Matrix4x4WorldPixelToWindowPixel;
};
extern void mmWidgetTransform_Init(struct mmWidgetTransform* p);
extern void mmWidgetTransform_Destroy(struct mmWidgetTransform* p);
extern void mmWidgetTransform_UpdateWindow(struct mmWidgetTransform* p, CEGUI::Window* pWindow);
extern void mmWidgetTransform_WorldPixelToWindowPixelPosition(struct mmWidgetTransform* p, double x, double y, float hPosition[2]);
extern void mmWidgetTransform_WindowPixelToWorldPixelPosition(struct mmWidgetTransform* p, double x, double y, float hPosition[2]);

// window pixel size.
extern void mmWidgetTransform_WindowPixelSize(CEGUI::Window* pWindow, float hPixelSize[2]);
// window parent pixel size.
extern void mmWidgetTransform_ParentPixelSize(CEGUI::Window* pWindow, float hParentPixelSize[2]);
// Window to parent rect(x, y, w, h).
extern void mmWidgetTransform_WindowToParentRect(CEGUI::Window* pWindow, float hRect[4]);

// window scale => window pixel.
extern void mmWidgetTransform_Matrix4x4WindowScaleToWindowPixel(CEGUI::Window* pWindow, Ogre::Matrix4* pMatrix4x4);
// window pixel => window scale.
extern void mmWidgetTransform_Matrix4x4WindowPixelToWindowScale(CEGUI::Window* pWindow, Ogre::Matrix4* pMatrix4x4);
// window Pixel => parent Pixel.
extern void mmWidgetTransform_Matrix4x4WindowPixelToParentPixel(CEGUI::Window* pWindow, Ogre::Matrix4* pMatrix4x4);
// window Pixel => world Pixel.
extern void mmWidgetTransform_Matrix4x4WindowPixelToWorldPixel(CEGUI::Window* pWindow, Ogre::Matrix4* pMatrix4x4);


#endif//__mmWidgetTransform_h__

