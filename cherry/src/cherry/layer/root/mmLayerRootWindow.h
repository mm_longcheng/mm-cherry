/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmLayerRootWindow_h__
#define __mmLayerRootWindow_h__

#include "core/mmCore.h"

#include "dish/mmEvent.h"

#include "layer/director/mmLayerContext.h"

#include "CEGUI/Window.h"

namespace mm
{
    class mmLayerRootWindow : public mmLayerContext
    {
    public:
        static const CEGUI::String EventNamespace;          //!< Namespace for global events
        static const CEGUI::String WidgetTypeName;          //!< Window factory name
    public:
        CEGUI::Window* LayerRootWindow;
        CEGUI::Window* StaticImageBackground;
        CEGUI::Window* LayerRootSafety;
        CEGUI::Window* LayerRootNotify;
        
        mmLayerContext* pRootSafety;
        mmLayerContext* pRootNotify;
    public:
        mmLayerRootWindow(const CEGUI::String& type, const CEGUI::String& name);
        virtual ~mmLayerRootWindow(void);
    public:
        // mmLayerContext
        virtual void OnFinishLaunching(void);
        virtual void OnBeforeTerminate(void);
    protected:
        bool OnEventUpdatedDisplay(const mmEventArgs& args);
    };
}

#endif//__mmLayerRootWindow_h__
