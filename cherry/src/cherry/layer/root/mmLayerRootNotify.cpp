/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerRootNotify.h"

#include "CEGUI/WindowManager.h"

#include "layer/director/mmLayerDirector.h"

#include "module/mmModuleLogger.h"

static const CEGUI::Colour constLoggerViewColour[] =
{
    CEGUI::Colour(0xFFFFFFFF),
    CEGUI::Colour(0xFFFF6B68),
    CEGUI::Colour(0xFFFF6B68),
    CEGUI::Colour(0xFFFF0006),
    CEGUI::Colour(0xFFFF0006),
    CEGUI::Colour(0xFFBBBB23),
    CEGUI::Colour(0xFF48BB31),
    CEGUI::Colour(0xFF48BB31),
    CEGUI::Colour(0xFF0070BB),
    CEGUI::Colour(0xFF0070BB),
    CEGUI::Colour(0xFFBBBBBB),
};

const CEGUI::Colour& mmLayerRootNotify_GetLoggerViewColour(mmUInt32_t lvl)
{
    if (MM_LOG_FATAL <= lvl && lvl <= MM_LOG_VERBOSE)
    {
        return constLoggerViewColour[lvl];
    }
    else
    {
        return constLoggerViewColour[MM_LOG_UNKNOWN];
    }
}

namespace mm
{
    const CEGUI::String mmLayerRootNotify::EventNamespace = "mm";
    const CEGUI::String mmLayerRootNotify::WidgetTypeName = "mm/mmLayerRootNotify";

    mmLayerRootNotify::mmLayerRootNotify(const CEGUI::String& type, const CEGUI::String& name)
        : mmLayerContext(type, name)
        , LayerRootNotify(NULL)
        , StaticTextMessage(NULL)
        , hPool()
        , hTimerInterval(0.0)
        , hTimerSwitch(1.0)
    {
        mmListVpt_Init(&this->hQuque);
    }
    mmLayerRootNotify::~mmLayerRootNotify(void)
    {
        mmListVpt_Destroy(&this->hQuque);
    }

    void mmLayerRootNotify::OnFinishLaunching(void)
    {
        struct mmContextMaster* pContextMaster = this->pContextMaster;
        mm::mmModule* pModule = &pContextMaster->hModule;
        mm::mmModuleLogger* pModuleLogger = pModule->GetData<mm::mmModuleLogger>();
        
        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        this->LayerRootNotify = pWindowManager->loadLayoutFromFile("root/LayerRootNotify.layout");
        this->addChild(this->LayerRootNotify);
        
        this->StaticTextMessage = this->LayerRootNotify->getChild("StaticTextMessage");
        
        pModuleLogger->hEventSet.SubscribeEvent(mm::mmModuleLogger::EventLoggerView, &mmLayerRootNotify::OnEventLoggerView, this);
        
        this->SetTouchsPassThroughEnabled(true);
        this->SetCursorPassThroughEnabled(true);
        this->SetKeypadPassThroughEnabled(true);
        
        // pool chunk 64 is enough.
        this->hPool.SetChunkSize(64);
        
        this->subscribeEvent(CEGUI::Window::EventUpdated, CEGUI::Event::Subscriber(&mmLayerRootNotify::OnWindowEventUpdated, this));
        
        this->StaticTextMessage->setVisible(false);
        this->StaticTextMessage->deactivate();
    }

    void mmLayerRootNotify::OnBeforeTerminate(void)
    {
        struct mmContextMaster* pContextMaster = this->pContextMaster;
        mm::mmModule* pModule = &pContextMaster->hModule;
        mm::mmModuleLogger* pModuleLogger = pModule->GetData<mm::mmModuleLogger>();
        
        this->QuqueClear();
        
        pModuleLogger->hEventSet.UnsubscribeEvent(mm::mmModuleLogger::EventLoggerView, &mmLayerRootNotify::OnEventLoggerView, this);
        
        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        pWindowManager->destroyWindow(this->LayerRootNotify);
        this->LayerRootNotify = NULL;
    }
    void mmLayerRootNotify::ShowOneTips(void)
    {
        if(0 != this->hQuque.size)
        {
            void* u = mmListVpt_PopFont(&this->hQuque);
            mmEventDataLogger* data = (mmEventDataLogger*)(u);
            
            const CEGUI::Colour& hColour = mmLayerRootNotify_GetLoggerViewColour(data->hLevel);
            CEGUI::String hColourString = CEGUI::PropertyHelper<CEGUI::Colour>::toString(hColour);
            this->StaticTextMessage->setProperty("FrameColours", hColourString);
            this->StaticTextMessage->setProperty("TextColours", hColourString);
            this->StaticTextMessage->setText(data->hView);
            this->StaticTextMessage->setVisible(true);
            this->StaticTextMessage->activate();
            
            this->hPool.Recycle(u);
        }
        else
        {
            this->StaticTextMessage->setVisible(false);
            this->StaticTextMessage->deactivate();
        }
    }
    void mmLayerRootNotify::QuqueClear(void)
    {
        // queue is weak reference, and we no need to show them all.
        struct mmListHead* next = NULL;
        struct mmListHead* curr = NULL;
        struct mmListVptIterator* it = NULL;
        next = this->hQuque.l.next;
        while (next != &this->hQuque.l)
        {
            curr = next;
            next = next->next;
            it = (struct mmListVptIterator*)mmList_Entry(curr, struct mmListVptIterator, n);
            
            this->hPool.Recycle(it->v);
            
            mmListVpt_Erase(&this->hQuque, it);
        }
    }
    bool mmLayerRootNotify::OnEventLoggerView(const mmEventArgs& args)
    {
        const mmEventDataLogger& evt = (const mmEventDataLogger&)(args);
        
        mmEventDataLogger* data = NULL;
        data = (mmEventDataLogger*)this->hPool.Produce();
        
        *data = evt;
        
        mmListVpt_AddTail(&this->hQuque, data);
        return false;
    }

    bool mmLayerRootNotify::OnWindowEventUpdated(const CEGUI::EventArgs& args)
    {
        const CEGUI::UpdateEventArgs& evt = (const CEGUI::UpdateEventArgs&)(args);

        if (0 != this->hQuque.size || this->StaticTextMessage->isVisible())
        {
            this->hTimerInterval += evt.d_timeSinceLastFrame;

            if (this->hTimerInterval >= this->hTimerSwitch)
            {
                this->ShowOneTips();
                this->hTimerInterval = 0.0;
            }
        }
        else
        {
            this->hTimerInterval = this->hTimerSwitch;
        }

        return true;
    }
}
