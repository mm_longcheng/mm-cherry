/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerRootWindow.h"

#include "nwsi/mmEventSurface.h"

#include "CEGUI/WindowManager.h"

#include "layer/director/mmLayerDirector.h"

namespace mm
{
    const CEGUI::String mmLayerRootWindow::EventNamespace = "mm";
    const CEGUI::String mmLayerRootWindow::WidgetTypeName = "mm/mmLayerRootWindow";

    mmLayerRootWindow::mmLayerRootWindow(const CEGUI::String& type, const CEGUI::String& name)
        : mmLayerContext(type, name)
        , LayerRootWindow(NULL)
        , StaticImageBackground(NULL)
        , LayerRootSafety(NULL)
        , LayerRootNotify(NULL)

        , pRootSafety(NULL)
        , pRootNotify(NULL)
    {

    }
    mmLayerRootWindow::~mmLayerRootWindow(void)
    {
        
    }

    void mmLayerRootWindow::OnFinishLaunching(void)
    {
        struct mmLayerDirector* pDirector = this->pDirector;
        struct mmSurfaceMaster* pSurfaceMaster = this->pSurfaceMaster;
        
        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        this->LayerRootWindow = pWindowManager->loadLayoutFromFile("root/LayerRootWindow.layout");
        this->StaticImageBackground = this->LayerRootWindow->getChild("StaticImageBackground");
        this->LayerRootSafety = this->LayerRootWindow->getChild("LayerRootSafety");
        this->LayerRootNotify = this->LayerRootWindow->getChild("LayerRootNotify");
        this->addChild(this->LayerRootWindow);
        
        this->pRootSafety = (mmLayerContext*)this->LayerRootSafety;
        this->pRootNotify = (mmLayerContext*)this->LayerRootNotify;
        
        mmLayerDirector_SetRootSafety(pDirector, this->LayerRootSafety);
        
        this->pRootSafety->SetDirector(this->pDirector);
        this->pRootSafety->SetContextMaster(this->pContextMaster);
        this->pRootSafety->SetSurfaceMaster(this->pSurfaceMaster);
        this->pRootSafety->OnFinishLaunching();
        
        this->pRootNotify->SetDirector(this->pDirector);
        this->pRootNotify->SetContextMaster(this->pContextMaster);
        this->pRootNotify->SetSurfaceMaster(this->pSurfaceMaster);
        this->pRootNotify->OnFinishLaunching();
        
        this->AttachContextEvent(this->pRootSafety);
        this->AttachContextEvent(this->pRootNotify);
        
        pSurfaceMaster->hEventSet.SubscribeEvent(mmSurfaceMaster_EventUpdatedDisplay, &mmLayerRootWindow::OnEventUpdatedDisplay, this);
    }

    void mmLayerRootWindow::OnBeforeTerminate(void)
    {
        struct mmLayerDirector* pDirector = this->pDirector;
        struct mmSurfaceMaster* pSurfaceMaster = this->pSurfaceMaster;
        
        pSurfaceMaster->hEventSet.UnsubscribeEvent(mmSurfaceMaster_EventUpdatedDisplay, &mmLayerRootWindow::OnEventUpdatedDisplay, this);
        
        this->DetachContextEvent(this->pRootNotify);
        this->DetachContextEvent(this->pRootSafety);
        
        this->pRootNotify->OnBeforeTerminate();
        this->pRootSafety->OnBeforeTerminate();
        
        mmLayerDirector_SetRootSafety(pDirector, NULL);
        
        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        pWindowManager->destroyWindow(this->LayerRootWindow);
        this->LayerRootWindow = NULL;
        this->StaticImageBackground = NULL;
        this->LayerRootSafety = NULL;
        this->LayerRootNotify = NULL;
    }

    bool mmLayerRootWindow::OnEventUpdatedDisplay(const mmEventArgs& args)
    {
        const mmEventUpdate& evt = (const mmEventUpdate&)(args);
        
        struct mmContextMaster* pContextMaster = this->pContextMaster;
        mm::mmModule* pModule = &pContextMaster->hModule;
        
        pModule->OnUpdate(evt.content->interval);
        return false;
    }
}
