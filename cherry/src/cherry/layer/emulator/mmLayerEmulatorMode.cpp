/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerEmulatorMode.h"
#include "mmLayerEmulatorIndicator.h"

#include "CEGUI/WindowManager.h"

#include "layer/director/mmLayerDirector.h"

namespace mm
{
    const CEGUI::String mmLayerEmulatorMode::EventNamespace = "mm";
    const CEGUI::String mmLayerEmulatorMode::WidgetTypeName = "mm/mmLayerEmulatorMode";

    mmLayerEmulatorMode::mmLayerEmulatorMode(const CEGUI::String& type, const CEGUI::String& name)
        : mmLayerContext(type, name)

        , pEmulatorMachine(NULL)

        , hMode(mmLayerEmulatorIndicator::MODE_ORDINARY)

        , pCurrentMode(NULL)
    {
        mmRbtreeU32String_Init(&this->hModeLayerType);
        mmRbtreeU32Vpt_Init(&this->hModeLayer);
    }
    mmLayerEmulatorMode::~mmLayerEmulatorMode(void)
    {
        assert(0 == this->hModeLayerType.size && "0 == this->hModeLayerType.size is invalid.");
        assert(0 == this->hModeLayer.size && "0 == this->hModeLayer.size is invalid.");

        mmRbtreeU32String_Destroy(&this->hModeLayerType);
        mmRbtreeU32Vpt_Destroy(&this->hModeLayer);
    }

    void mmLayerEmulatorMode::SetEmulatorMachine(struct mmEmulatorMachine* pEmulatorMachine)
    {
        this->pEmulatorMachine = pEmulatorMachine;
    }

    void mmLayerEmulatorMode::OnFinishLaunching(void)
    {
        this->RegisterDefaultModeLayer();

        this->UpdateModeLayer();
    }

    void mmLayerEmulatorMode::OnBeforeTerminate(void)
    {
        this->ClearModeLayer();
        this->ClearModeLayerType();
    }
    void mmLayerEmulatorMode::SetMode(mmUInt32_t hMode)
    {
        this->hMode = hMode;
    }
    void mmLayerEmulatorMode::UpdateModeLayer(void)
    {
        if (NULL != this->pCurrentMode)
        {
            this->pCurrentMode->setVisible(false);
            this->pCurrentMode = NULL;
        }

        this->pCurrentMode = this->GetModeLayerInstance(this->hMode);

        if (NULL != this->pCurrentMode)
        {
            this->pCurrentMode->setVisible(true);
        }
    }
    void mmLayerEmulatorMode::AddModeLayerType(mmUInt32_t id, const char* pType)
    {
        struct mmString hWeakValue;
        mmString_MakeWeak(&hWeakValue, pType);
        mmRbtreeU32String_Set(&this->hModeLayerType, id, &hWeakValue);
    }
    void mmLayerEmulatorMode::RmvModeLayerType(mmUInt32_t id)
    {
        mmRbtreeU32String_Rmv(&this->hModeLayerType, id);
    }
    void mmLayerEmulatorMode::GetModeLayerType(mmUInt32_t id, struct mmString* pType)
    {
        struct mmRbtreeU32StringIterator* it = NULL;
        it = mmRbtreeU32String_GetIterator(&this->hModeLayerType, id);
        if (NULL != it)
        {
            mmString_Assign(pType, &it->v);
        }
        else
        {
            mmString_Assigns(pType, "mm/mmLayerEmulatorModeGeneral");
        }
    }
    void mmLayerEmulatorMode::ClearModeLayerType(void)
    {
        mmRbtreeU32String_Clear(&this->hModeLayerType);
    }
    void mmLayerEmulatorMode::RegisterDefaultModeLayer(void)
    {
        this->AddModeLayerType(mmLayerEmulatorIndicator::MODE_ORDINARY, "mm/mmLayerEmulatorModeOrdinary");
        this->AddModeLayerType(mmLayerEmulatorIndicator::MODE_JOYSTICK, "mm/mmLayerEmulatorModeJoystick");
        this->AddModeLayerType(mmLayerEmulatorIndicator::MODE_KEYBOARD, "mm/mmLayerEmulatorModeKeyboard");
    }
    mmLayerEmulatorModeBase* mmLayerEmulatorMode::AddModeLayer(mmUInt32_t id)
    {
        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();

        mmLayerEmulatorModeBase* e = NULL;

        struct mmString hType;

        mmString_Init(&hType);

        e = this->GetModeLayer(id);
        if (NULL == e)
        {
            this->GetModeLayerType(id, &hType);
            e = (mmLayerEmulatorModeBase*)pWindowManager->createWindow(mmString_CStr(&hType));
            this->addChild(e);

            // spacing common CEGUI::Window object, we need manual attach context event.
            this->AttachContextEvent(e);

            e->SetDirector(this->pDirector);
            e->SetContextMaster(this->pContextMaster);
            e->SetSurfaceMaster(this->pSurfaceMaster);
            e->SetEmulatorMachine(this->pEmulatorMachine);
            e->OnFinishLaunching();
            mmRbtreeU32Vpt_Set(&this->hModeLayer, id, e);
        }

        mmString_Destroy(&hType);

        return e;
    }
    mmLayerEmulatorModeBase* mmLayerEmulatorMode::GetModeLayer(mmUInt32_t id)
    {
        return (mmLayerEmulatorModeBase*)mmRbtreeU32Vpt_Get(&this->hModeLayer, id);
    }
    mmLayerEmulatorModeBase* mmLayerEmulatorMode::GetModeLayerInstance(mmUInt32_t id)
    {
        mmLayerEmulatorModeBase* e = NULL;
        e = this->GetModeLayer(id);
        if (NULL == e)
        {
            e = this->AddModeLayer(id);
        }
        return e;
    }
    void mmLayerEmulatorMode::RmvModeLayer(mmUInt32_t id)
    {
        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();

        mmLayerContext* e = NULL;
        struct mmRbtreeU32VptIterator* it = NULL;

        it = mmRbtreeU32Vpt_GetIterator(&this->hModeLayer, id);
        if (NULL != it)
        {
            e = (mmLayerContext*)it->v;
            mmRbtreeU32Vpt_Erase(&this->hModeLayer, it);
            e->OnBeforeTerminate();

            // spacing common CEGUI::Window object, we need manual attach context event.
            this->DetachContextEvent(e);

            this->removeChild(e);
            pWindowManager->destroyWindow(e);
        }
    }
    void mmLayerEmulatorMode::ClearModeLayer(void)
    {
        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();

        mmLayerContext* e = NULL;

        struct mmRbNode* n = NULL;
        struct mmRbtreeU32VptIterator* it = NULL;
        //
        n = mmRb_First(&this->hModeLayer.rbt);
        while (NULL != n)
        {
            it = (struct mmRbtreeU32VptIterator*)mmRb_Entry(n, struct mmRbtreeU32VptIterator, n);
            n = mmRb_Next(n);
            // 
            e = (mmLayerContext*)it->v;
            mmRbtreeU32Vpt_Erase(&this->hModeLayer, it);
            e->OnBeforeTerminate();

            // spacing common CEGUI::Window object, we need manual attach context event.
            this->DetachContextEvent(e);

            this->removeChild(e);
            pWindowManager->destroyWindow(e);
        }
    }
}
