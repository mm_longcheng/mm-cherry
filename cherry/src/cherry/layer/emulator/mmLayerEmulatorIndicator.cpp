/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerEmulatorIndicator.h"

namespace mm
{
    const CEGUI::String mmLayerEmulatorIndicator::EventNamespace("mm");
    const CEGUI::String mmLayerEmulatorIndicator::EventModeChange("EventModeChange");

    mmLayerEmulatorIndicator::mmLayerEmulatorIndicator()
        : pWindowMenu(NULL)

        , StaticImageMode0(NULL)
        , StaticImageMode1(NULL)
        , StaticImageMode2(NULL)

        , hColourSelect(0.0f, 1.0f, 0.0f, 1.0f)
        , hColourCommon(1.0f, 1.0f, 1.0f, 1.0f)

        , pItemCurrent(NULL)

        , hMode()
    {
        mmRbtreeU32Vpt_Init(&this->hItemMap);
    }
    mmLayerEmulatorIndicator::~mmLayerEmulatorIndicator()
    {
        assert(0 == this->hItemMap.size && "clear item map before destroy.");
        mmRbtreeU32Vpt_Destroy(&this->hItemMap);
    }
    void mmLayerEmulatorIndicator::SetWindowMenu(CEGUI::Window* pWindowMenu)
    {
        this->pWindowMenu = pWindowMenu;
    }
    void mmLayerEmulatorIndicator::SetMode(mmUInt32_t hMode)
    {
        this->hMode = hMode;
    }
    void mmLayerEmulatorIndicator::UpdateModeLayer()
    {
        if (NULL != this->pItemCurrent)
        {
            this->UpdateItemColour(this->pItemCurrent, false);
            this->pItemCurrent = NULL;
        }

        this->pItemCurrent = (CEGUI::Window*)mmRbtreeU32Vpt_Get(&this->hItemMap, this->hMode);

        if (NULL != this->pItemCurrent)
        {
            this->UpdateItemColour(this->pItemCurrent, true);

            mmUInt32_t mode = this->pItemCurrent->getID();
            mmLayerEmulatorIndicatorEventArgs hArgs(this->pItemCurrent, mode);
            this->pWindowMenu->fireEvent(EventModeChange, hArgs, EventNamespace);
        }
    }
    void mmLayerEmulatorIndicator::UpdateItemColour(CEGUI::Window* w, bool _illuminate)
    {
        CEGUI::Colour _colour;
        CEGUI::String hColourString;

        _colour = _illuminate ? this->hColourSelect : this->hColourCommon;
        hColourString = CEGUI::PropertyHelper<CEGUI::Colour>::toString(_colour);

        w->setProperty("ImageColours", hColourString);
    }

    void mmLayerEmulatorIndicator::OnFinishLaunching()
    {
        assert(NULL != this->pWindowMenu && "this->pWindowMenu can not be null.");

        this->StaticImageMode0 = this->pWindowMenu->getChild("StaticImageMode0");
        this->StaticImageMode1 = this->pWindowMenu->getChild("StaticImageMode1");
        this->StaticImageMode2 = this->pWindowMenu->getChild("StaticImageMode2");

        this->hItem0EventMouseClickConn = this->StaticImageMode0->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerEmulatorIndicator::OnLayerItemEventMouseClick, this));
        this->hItem1EventMouseClickConn = this->StaticImageMode1->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerEmulatorIndicator::OnLayerItemEventMouseClick, this));
        this->hItem2EventMouseClickConn = this->StaticImageMode2->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerEmulatorIndicator::OnLayerItemEventMouseClick, this));

        mmRbtreeU32Vpt_Set(&this->hItemMap, MODE_ORDINARY, this->StaticImageMode0);
        mmRbtreeU32Vpt_Set(&this->hItemMap, MODE_JOYSTICK, this->StaticImageMode1);
        mmRbtreeU32Vpt_Set(&this->hItemMap, MODE_KEYBOARD, this->StaticImageMode2);

        this->StaticImageMode0->setID(MODE_ORDINARY);
        this->StaticImageMode1->setID(MODE_JOYSTICK);
        this->StaticImageMode2->setID(MODE_KEYBOARD);

        this->UpdateModeLayer();
    }
    void mmLayerEmulatorIndicator::OnBeforeTerminate()
    {
        mmRbtreeU32Vpt_Clear(&this->hItemMap);

        this->hItem0EventMouseClickConn->disconnect();
        this->hItem1EventMouseClickConn->disconnect();
        this->hItem2EventMouseClickConn->disconnect();

        this->StaticImageMode0 = NULL;
        this->StaticImageMode1 = NULL;
        this->StaticImageMode2 = NULL;
    }
    bool mmLayerEmulatorIndicator::OnLayerItemEventMouseClick(const CEGUI::EventArgs& args)
    {
        const CEGUI::MouseEventArgs& evt = (const CEGUI::MouseEventArgs&)(args);

        mmUInt32_t mode = evt.window->getID();

        this->SetMode(mode);
        this->UpdateModeLayer();
        return true;
    }
}
