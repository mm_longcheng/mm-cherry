/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmLayerEmulatorIndicator_h__
#define __mmLayerEmulatorIndicator_h__

#include "core/mmCore.h"

#include "container/mmRbtreeU32.h"

#include "CEGUI/Window.h"

namespace mm
{
    /*!
    \brief
    EventArgs based class that is used for objects passed to input event handlers
    concerning emulator indicator event.
    */
    class mmLayerEmulatorIndicatorEventArgs : public CEGUI::WindowEventArgs
    {
    public:
        mmLayerEmulatorIndicatorEventArgs(CEGUI::Window* wnd, mmUInt32_t mode)
            : CEGUI::WindowEventArgs(wnd)
            , hMode(mode)
        {

        }

        mmUInt32_t hMode;//!< holds current emulator mode information.
    };

    class mmLayerEmulatorIndicator
    {
    public:
        enum
        {
            MODE_ORDINARY = 0,// ordinary
            MODE_JOYSTICK = 1,// joystick
            MODE_KEYBOARD = 2,// keyboard
        };
    public:
        static const CEGUI::String EventNamespace;          //!< Namespace for global events
    public:
        static const CEGUI::String EventModeChange;
    public:
        CEGUI::Window* pWindowMenu;

        CEGUI::Window* StaticImageMode0;
        CEGUI::Window* StaticImageMode1;
        CEGUI::Window* StaticImageMode2;
    public:
        CEGUI::Colour hColourSelect;
        CEGUI::Colour hColourCommon;
    public:
        CEGUI::Window* pItemCurrent;
    public:
        // weak ref. id -> CEGUI::Window
        struct mmRbtreeU32Vpt hItemMap;
    public:
        mmUInt32_t hMode;
    public:
        CEGUI::Event::Connection hItem0EventMouseClickConn;
        CEGUI::Event::Connection hItem1EventMouseClickConn;
        CEGUI::Event::Connection hItem2EventMouseClickConn;
    public:
        mmLayerEmulatorIndicator(void);
        ~mmLayerEmulatorIndicator(void);
    public:
        void SetWindowMenu(CEGUI::Window* pWindowMenu);
        void SetMode(mmUInt32_t hMode);
    public:
        void UpdateModeLayer(void);
        void UpdateItemColour(CEGUI::Window* w, bool hIlluminate);
    public:
        void OnFinishLaunching(void);
        void OnBeforeTerminate(void);
    private:
        bool OnLayerItemEventMouseClick(const CEGUI::EventArgs& args);
    };
}

#endif//__mmLayerEmulatorIndicator_h__

