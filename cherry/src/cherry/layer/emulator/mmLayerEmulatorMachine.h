/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmLayerEmulatorMachine_h__
#define __mmLayerEmulatorMachine_h__

#include "core/mmCore.h"

#include "dish/mmEvent.h"

#include "layer/director/mmLayerContext.h"

#include "CEGUI/Window.h"

#include "layer/utility/mmLayerUtilityFps.h"

#include "mmLayerEmulatorIndicator.h"

#include "emulator/mmEmulatorMachine.h"

#include "toolkit/mmIconv.h"

namespace mm
{
    class mmLayerEmulatorMode;
    class mmLayerEmulatorToolbarL;
    class mmLayerEmulatorToolbarR;

    class mmLayerEmulatorMachine : public mmLayerContext
    {
    public:
        static const CEGUI::String EventNamespace;          //!< Namespace for global events
        static const CEGUI::String WidgetTypeName;          //!< Window factory name
    public:
        static const CEGUI::String EventEmulatorMovieFinish;
        static const CEGUI::String EventEmulatorScreenMessage;
        static const CEGUI::String EventEmulatorPlay;
        static const CEGUI::String EventEmulatorStop;
    public:
        CEGUI::Window* LayerEmulatorMachine;
        CEGUI::Window* StaticImageBackground;

        CEGUI::Window* DefaultWindowMode;

        CEGUI::Window* LayerEmulatorMode;
        CEGUI::Window* LayerEmulatorToolbarL;
        CEGUI::Window* LayerEmulatorToolbarR;

        CEGUI::Window* StaticTextFps;
        CEGUI::Window* WidgetImageButtonBack;
        //
        mmLayerEmulatorMode* pEmulatorMode;
        mmLayerEmulatorToolbarL* pEmulatorToolbarL;
        mmLayerEmulatorToolbarR* pEmulatorToolbarR;
    public:
        struct mmEmulatorMachine hEmulatorMachine;
    public:
        struct mmIconvContext hIconvContext;
    public:
        mmLayerUtilityFps hUtilityFps;
    public:
        mmLayerEmulatorIndicator hIndicator;
    public:
        std::string hCurrentEmulatorAssets;
    public:
        CEGUI::Event::Connection hIndicatorEventModeChangeConn;

        CEGUI::Event::Connection hFpsEventMouseClickConn;
        CEGUI::Event::Connection hBackEventMouseClickConn;
    public:
        mmLayerEmulatorMachine(const CEGUI::String& type, const CEGUI::String& name);
        virtual ~mmLayerEmulatorMachine(void);
    public:
        // mmLayerContext
        virtual void OnFinishLaunching(void);
        virtual void OnBeforeTerminate(void);
    public:
        void UpdateLayerValue(void);
    private:
        bool OnEventEnterBackground(const mmEventArgs& args);
        bool OnEventEnterForeground(const mmEventArgs& args);
    private:
        bool OnIconvContextEventFormatUpdate(const mmEventArgs& args);
    private:
        bool OnEmulatorEvent_MM_EMU_NT_SCREENMESSAGE(const mmEventArgs& args);
        bool OnEmulatorEvent_MM_EMU_NT_MOVIE_FINISH(const mmEventArgs& args);
        bool OnEmulatorEvent_MM_EMU_NT_PLAY(const mmEventArgs& args);
        bool OnEmulatorEvent_MM_EMU_NT_STOP(const mmEventArgs& args);
    private:
        bool OnIndicatorEventModeChange(const CEGUI::EventArgs& args);
    private:
        bool OnStaticTextFpsEventMouseClick(const CEGUI::EventArgs& args);
        bool OnWidgetImageButtonBackEventMouseClick(const CEGUI::EventArgs& args);
    };
}

#endif//__mmLayerEmulatorMachine_h__