/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerEmulatorModeJoystick.h"

#include "CEGUI/WindowManager.h"

#include "layer/director/mmLayerDirector.h"

namespace mm
{
    const CEGUI::String mmLayerEmulatorModeJoystick::EventNamespace = "mm";
    const CEGUI::String mmLayerEmulatorModeJoystick::WidgetTypeName = "mm/mmLayerEmulatorModeJoystick";

    mmLayerEmulatorModeJoystick::mmLayerEmulatorModeJoystick(const CEGUI::String& type, const CEGUI::String& name)
        : mmLayerEmulatorModeBase(type, name)
        , LayerEmulatorModeJoystick(NULL)
        , StaticImageBackground(NULL)

        , StaticImageScreen(NULL)
    {

    }
    mmLayerEmulatorModeJoystick::~mmLayerEmulatorModeJoystick()
    {
        
    }
    void mmLayerEmulatorModeJoystick::OnFinishLaunching(void)
    {
        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        this->LayerEmulatorModeJoystick = pWindowManager->loadLayoutFromFile("emulator/LayerEmulatorModeJoystick.layout");
        this->addChild(this->LayerEmulatorModeJoystick);

        this->StaticImageBackground = this->LayerEmulatorModeJoystick->getChild("StaticImageBackground");

        this->StaticImageScreen = this->StaticImageBackground->getChild("StaticImageScreen");

        this->hLayerEmulatorWindow.SetEmulatorMachine(this->pEmulatorMachine);
        this->hLayerEmulatorWindow.SetLayerContext(this);
        this->hLayerEmulatorWindow.SetWindowScreen(this->StaticImageScreen);
        this->hLayerEmulatorWindow.SetFingerIsVisible(true);
        this->hLayerEmulatorWindow.OnFinishLaunching();
    }

    void mmLayerEmulatorModeJoystick::OnBeforeTerminate(void)
    {
        this->hLayerEmulatorWindow.OnBeforeTerminate();

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        pWindowManager->destroyWindow(this->LayerEmulatorModeJoystick);
        this->LayerEmulatorModeJoystick = NULL;
    }
    void mmLayerEmulatorModeJoystick::notifyScreenAreaChanged(bool recursive/*  = true */)
    {
        mmLayerContext::notifyScreenAreaChanged(recursive);
        this->hLayerEmulatorWindow.UpdateTransform();
    }
}
