/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmLayerEmulatorPadR_h__
#define __mmLayerEmulatorPadR_h__

#include "container/mmRbtreeString.h"

#include "CEGUI/Window.h"

#include "layer/director/mmLayerContext.h"

#include "layer/utility/mmLayerUtilityTransform.h"

#include "mmLayerEmulatorPadRWidget.h"

struct mmEmulatorMachine;

namespace mm
{
    class mmLayerUtilityFinger;
    class mmLayerUtilityFingerTouch;

    class mmLayerEmulatorPadR : public mmLayerContext
    {
    public:
        static const CEGUI::String EventNamespace;          //!< Namespace for global events
        static const CEGUI::String WidgetTypeName;          //!< Window factory name
    public:
        CEGUI::Window* LayerEmulatorPadR;
        CEGUI::Window* StaticImageBackground;
    public:
        mmLayerUtilityFingerTouch* pFingerTouch;
    public:
        struct mmEmulatorMachine* pEmulatorMachine;
    public:
        struct mmLayerUtilityTransform hEmulatorTransform;
    public:
        std::string hNormalName;
        std::string hLustreName;
    public:
        // strong ref.
        struct mmRbtreeStringVpt hEmuPadRbtree;
    public:
        mmLayerEmulatorPadR(const CEGUI::String& type, const CEGUI::String& name);
        virtual ~mmLayerEmulatorPadR();
    public:
        void SetFingerTouch(mmLayerUtilityFingerTouch* pFingerTouch);
    public:
        void SetEmulatorMachine(struct mmEmulatorMachine* pEmulatorMachine);
    public:
        void SetNormalName(const std::string& hNormalName);
        void SetLustreName(const std::string& hLustreName);
        void AssemblyPad(const char* pPadWindowName, mmWord_t hBit);
    public:
        mmLayerEmulatorPadRWidget* AddAssemblyPadWindow(const char* pPadWindowName);
        mmLayerEmulatorPadRWidget* GetAssemblyPadWindow(const char* pPadWindowName);
        mmLayerEmulatorPadRWidget* GetAssemblyPadWindowInstance(const char* pPadWindowName);
        void RmvAssemblyPadWindow(const char* pPadWindowName);
        void ClearAssemblyPadWindow(void);
    public:
        void UpdateTransform(void);
    public:
        // mmLayerContext
        virtual void OnFinishLaunching(void);
        virtual void OnBeforeTerminate(void);
    public:
        void OnEventWindowSizeChanged(struct mmSurfaceContentSizeChange* pContent);
    public:
        void OnEventTouchsMoved(struct mmSurfaceContentTouchs* pContent);
        void OnEventTouchsBegan(struct mmSurfaceContentTouchs* pContent);
        void OnEventTouchsEnded(struct mmSurfaceContentTouchs* pContent);
        void OnEventTouchsBreak(struct mmSurfaceContentTouchs* pContent);
    public:
        void FinishLaunchingWidget(void);
        void BeforeTerminateWidget(void);
    };
}

#endif//__mmLayerEmulatorPadR_h__

