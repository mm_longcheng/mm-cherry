/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerEmulatorWindow.h"

#include "nwsi/mmEventSurface.h"

#include "layer/director/mmLayerDirector.h"

#include "emulator/mmEmulatorMachine.h"

#include "CEGUI/RenderTarget.h"

namespace mm
{
    mmLayerEmulatorWindow::mmLayerEmulatorWindow(void)
        : pEmulatorMachine(NULL)
        , pLayerContext(NULL)
        , pWindowScreen(NULL)
    {

    }
    mmLayerEmulatorWindow::~mmLayerEmulatorWindow(void)
    {

    }
    void mmLayerEmulatorWindow::SetEmulatorMachine(struct mmEmulatorMachine* pEmulatorMachine)
    {
        this->pEmulatorMachine = pEmulatorMachine;
    }
    void mmLayerEmulatorWindow::SetLayerContext(mmLayerContext* pLayerContext)
    {
        this->pLayerContext = pLayerContext;
    }
    void mmLayerEmulatorWindow::SetWindowScreen(CEGUI::Window* pWindowScreen)
    {
        this->pWindowScreen = pWindowScreen;
    }
    void mmLayerEmulatorWindow::SetFingerIsVisible(bool hVisible)
    {
        this->hFingerTouch.SetFingerIsVisible(hVisible);
        this->hFingerTouch.UpdateLayerMountIsVisible();
    }
    void mmLayerEmulatorWindow::OnFinishLaunching(void)
    {
        struct mmSurfaceMaster* pSurfaceMaster = this->pLayerContext->pSurfaceMaster;
        mmLayerContext* pLayerContext = this->pLayerContext;

        this->hLayerEventKeypadPressedConn = pLayerContext->subscribeEvent(mmLayerContext::EventKeypadPressed, CEGUI::Event::Subscriber(&mmLayerEmulatorWindow::OnLayerEventKeypadPressed, this));
        this->hLayerEventKeypadReleaseConn = pLayerContext->subscribeEvent(mmLayerContext::EventKeypadRelease, CEGUI::Event::Subscriber(&mmLayerEmulatorWindow::OnLayerEventKeypadRelease, this));
        //
        this->hLayerEventTouchsMovedConn = pLayerContext->subscribeEvent(mmLayerContext::EventTouchsMoved, CEGUI::Event::Subscriber(&mmLayerEmulatorWindow::OnLayerEventTouchsMoved, this));
        this->hLayerEventTouchsBeganConn = pLayerContext->subscribeEvent(mmLayerContext::EventTouchsBegan, CEGUI::Event::Subscriber(&mmLayerEmulatorWindow::OnLayerEventTouchsBegan, this));
        this->hLayerEventTouchsEndedConn = pLayerContext->subscribeEvent(mmLayerContext::EventTouchsEnded, CEGUI::Event::Subscriber(&mmLayerEmulatorWindow::OnLayerEventTouchsEnded, this));
        this->hLayerEventTouchsBreakConn = pLayerContext->subscribeEvent(mmLayerContext::EventTouchsBreak, CEGUI::Event::Subscriber(&mmLayerEmulatorWindow::OnLayerEventTouchsBreak, this));
        //
        this->hLayerEventUpdatedConn = pLayerContext->subscribeEvent(CEGUI::Window::EventUpdated, CEGUI::Event::Subscriber(&mmLayerEmulatorWindow::OnLayerEventUpdated, this));

        this->hFingerTouch.SetName(pLayerContext->getName().c_str());
        this->hFingerTouch.SetRadius(256.0f);
        this->hFingerTouch.SetForceRange(0.0f, 1.0f);
        this->hFingerTouch.SetScaleRange(0.22f, 0.34f);
        this->hFingerTouch.SetImageSource("ImagesetUtility/circle_touch");
        this->hFingerTouch.SetSurfaceMaster(pSurfaceMaster);
        this->hFingerTouch.SetBackground(pLayerContext);
        this->hFingerTouch.OnFinishLaunching();

        this->hEmulatorPadZapper.SetFingerTouch(&this->hFingerTouch);
        this->hEmulatorPadZapper.SetEmulatorMachine(this->pEmulatorMachine);
        this->hEmulatorPadZapper.SetWindowScreen(this->pWindowScreen);
        this->hEmulatorPadZapper.OnFinishLaunching();

        this->hEmulatorScreen.SetEmulatorMachine(this->pEmulatorMachine);
        this->hEmulatorScreen.SetWindowScreen(this->pWindowScreen);
        this->hEmulatorScreen.OnFinishLaunching();
    }
    void mmLayerEmulatorWindow::OnBeforeTerminate(void)
    {
        struct mmSurfaceMaster* pSurfaceMaster = this->pLayerContext->pSurfaceMaster;

        this->hEmulatorScreen.OnBeforeTerminate();
        this->hEmulatorPadZapper.OnBeforeTerminate();
        this->hFingerTouch.OnBeforeTerminate();

        this->hLayerEventKeypadPressedConn->disconnect();
        this->hLayerEventKeypadReleaseConn->disconnect();
        //
        this->hLayerEventTouchsMovedConn->disconnect();
        this->hLayerEventTouchsBeganConn->disconnect();
        this->hLayerEventTouchsEndedConn->disconnect();
        this->hLayerEventTouchsBreakConn->disconnect();
        //
        this->hLayerEventUpdatedConn->disconnect();
    }
    void mmLayerEmulatorWindow::UpdateTransform(void)
    {
        this->hFingerTouch.UpdateTransform();
        this->hEmulatorPadZapper.UpdateTransform();
    }
    bool mmLayerEmulatorWindow::OnLayerEventKeypadPressed(const CEGUI::EventArgs& args)
    {
        const mmKeypadEventArgs& evt = (const mmKeypadEventArgs&)(args);
        mmEmulatorMachine_KeyboardPressed(this->pEmulatorMachine, evt.content->key);
        return true;
    }
    bool mmLayerEmulatorWindow::OnLayerEventKeypadRelease(const CEGUI::EventArgs& args)
    {
        const mmKeypadEventArgs& evt = (const mmKeypadEventArgs&)(args);
        mmEmulatorMachine_KeyboardRelease(this->pEmulatorMachine, evt.content->key);
        return true;
    }
    bool mmLayerEmulatorWindow::OnLayerEventTouchsMoved(const CEGUI::EventArgs& args)
    {
        const mmTouchsEventArgs& evt = (const mmTouchsEventArgs&)(args);

        this->hFingerTouch.OnEventTouchsMoved(evt.content);
        this->hEmulatorPadZapper.OnEventTouchsMoved(evt.content);

        return false;
    }
    bool mmLayerEmulatorWindow::OnLayerEventTouchsBegan(const CEGUI::EventArgs& args)
    {
        const mmTouchsEventArgs& evt = (const mmTouchsEventArgs&)(args);

        this->hFingerTouch.OnEventTouchsBegan(evt.content);
        this->hEmulatorPadZapper.OnEventTouchsBegan(evt.content);

        return false;
    }
    bool mmLayerEmulatorWindow::OnLayerEventTouchsEnded(const CEGUI::EventArgs& args)
    {
        const mmTouchsEventArgs& evt = (const mmTouchsEventArgs&)(args);

        this->hFingerTouch.OnEventTouchsEnded(evt.content);
        this->hEmulatorPadZapper.OnEventTouchsEnded(evt.content);

        return false;
    }
    bool mmLayerEmulatorWindow::OnLayerEventTouchsBreak(const CEGUI::EventArgs& args)
    {
        const mmTouchsEventArgs& evt = (const mmTouchsEventArgs&)(args);

        this->hFingerTouch.OnEventTouchsBreak(evt.content);
        this->hEmulatorPadZapper.OnEventTouchsBreak(evt.content);

        return false;
    }
    bool mmLayerEmulatorWindow::OnLayerEventUpdated(const CEGUI::EventArgs& args)
    {
        const CEGUI::UpdateEventArgs& evt = (const CEGUI::UpdateEventArgs&)(args);
        this->hFingerTouch.OnEventUpdated(evt.d_timeSinceLastFrame);
        mmEmulatorMachine_UpdateRenderer(this->pEmulatorMachine);
        return true;
    }
}
