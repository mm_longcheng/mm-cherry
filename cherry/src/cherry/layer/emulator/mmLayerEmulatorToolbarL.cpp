/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerEmulatorToolbarL.h"

#include "CEGUI/WindowManager.h"
#include "CEGUI/widgets/Combobox.h"
#include "CEGUI/widgets/Editbox.h"
#include "CEGUI/widgets/ListboxTextItem.h"

#include "layer/director/mmLayerDirector.h"

#include "emulator/mmEmulatorMachine.h"

#include "emu/mmEmuNes.h"
#include "emu/mmEmuPad.h"

#include "explorer/mmExplorerCommand.h"

#include "layer/utility/mmLayerUtilityIconv.h"
#include "layer/utility/mmLayerUtilityListUrlItem.h"
#include "layer/utility/mmLayerUtilityTextItem.h"

#include "layer/common/mmLayerCommonEnsureCode.h"
#include "layer/common/mmLayerCommonEnsureEvent.h"

namespace mm
{
    static const char* __MovieBaseNameDefault = "movie_0.vmv";

    const CEGUI::String mmLayerEmulatorToolbarL::EventNamespace = "mm";
    const CEGUI::String mmLayerEmulatorToolbarL::WidgetTypeName = "mm/mmLayerEmulatorToolbarL";

    mmLayerEmulatorToolbarL::mmLayerEmulatorToolbarL(const CEGUI::String& type, const CEGUI::String& name)
        : mmLayerContext(type, name)
        , LayerEmulatorToolbarL(NULL)
        , StaticImageBackground(NULL)

        , DefaultWindowWristband(NULL)

        , StaticImageHandleO(NULL)
        , StaticImageHandleI(NULL)

        , WidgetComboboxExControllerType(NULL)
        , WidgetComboboxMovie(NULL)

        , WidgetImageButtonMovieDelete(NULL)

        , WidgetImageButtonMovieRec(NULL)
        , WidgetImageButtonMovieRecPlay(NULL)
        , WidgetImageButtonMovieRecStop(NULL)

        , WidgetImageButtonHardwareReset(NULL)
        , WidgetImageButtonSoftwareReset(NULL)

        , WidgetImageButtonSnapshot(NULL)

        , StaticTextMovieMTime(NULL)

        , pWidgetComboboxExControllerType(NULL)
        , pWidgetComboboxMovie(NULL)

        , pEditboxMovie(NULL)

        , pEmulatorMachine(NULL)
        , pIconvContext(NULL)

        , hCurrentMovieIndex(0)

        , hColourLustre(0.0f, 0.0f, 1.0f, 1.0f)
        , hColourCommon(1.0f, 1.0f, 1.0f, 1.0f)
    {
        struct mmVectorValueEventAllocator hEventAllocator;

        mmVectorValue_Init(&this->hItemTypeVector);
        mmVectorValue_Init(&this->hItemMovieVector);

        mmExplorerItem_Init(&this->hMovieItem);

        mmString_Init(&this->hMovieRealPath);
        mmString_Init(&this->hMoviePathName);
        mmString_Init(&this->hMovieBaseName);
        mmString_Init(&this->hTextBaseNameHost);

        hEventAllocator.Produce = &mmVectorValue_TextItemEventProduce;
        hEventAllocator.Recycle = &mmVectorValue_TextItemEventRecycle;
        hEventAllocator.obj = this;
        mmVectorValue_SetElemSize(&this->hItemTypeVector, sizeof(mmLayerUtilityTextItem));
        mmVectorValue_SetEventAllocator(&this->hItemTypeVector, &hEventAllocator);

        hEventAllocator.Produce = &mmVectorValue_ListUrlItemEventProduce;
        hEventAllocator.Recycle = &mmVectorValue_ListUrlItemEventRecycle;
        hEventAllocator.obj = this;
        mmVectorValue_SetElemSize(&this->hItemMovieVector, sizeof(mmLayerUtilityListUrlItem));
        mmVectorValue_SetEventAllocator(&this->hItemMovieVector, &hEventAllocator);
    }
    mmLayerEmulatorToolbarL::~mmLayerEmulatorToolbarL()
    {
        assert(0 == this->hItemTypeVector.size && "clear item type map before destroy.");
        assert(0 == this->hItemMovieVector.size && "clear item movie map before destroy.");

        mmVectorValue_Destroy(&this->hItemTypeVector);
        mmVectorValue_Destroy(&this->hItemMovieVector);

        mmExplorerItem_Destroy(&this->hMovieItem);

        mmString_Destroy(&this->hMovieRealPath);
        mmString_Destroy(&this->hMoviePathName);
        mmString_Destroy(&this->hMovieBaseName);
        mmString_Destroy(&this->hTextBaseNameHost);
    }
    void mmLayerEmulatorToolbarL::SetEmulatorMachine(struct mmEmulatorMachine* pEmulatorMachine)
    {
        this->pEmulatorMachine = pEmulatorMachine;
    }
    void mmLayerEmulatorToolbarL::SetIconvContext(struct mmIconvContext* pIconvContext)
    {
        this->pIconvContext = pIconvContext;
    }
    void mmLayerEmulatorToolbarL::SetMovieBaseName(const char* pStateBaseName)
    {
        mmString_Assigns(&this->hMovieBaseName, pStateBaseName);
    }
    void mmLayerEmulatorToolbarL::OnFinishLaunching(void)
    {
        struct mmSurfaceMaster* pSurfaceMaster = this->pSurfaceMaster;

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        this->LayerEmulatorToolbarL = pWindowManager->loadLayoutFromFile("emulator/LayerEmulatorToolbarL.layout");
        this->addChild(this->LayerEmulatorToolbarL);
        // Manual set whole area.
        CEGUI::URect hWholeArea(CEGUI::UDim(0, 0), CEGUI::UDim(0, 0), CEGUI::UDim(1, 0), CEGUI::UDim(1, 0));
        this->LayerEmulatorToolbarL->setArea(hWholeArea);

        this->StaticImageBackground = this->LayerEmulatorToolbarL->getChild("StaticImageBackground");

        this->DefaultWindowWristband = this->StaticImageBackground->getChild("DefaultWindowWristband");

        this->StaticImageHandleO = this->LayerEmulatorToolbarL->getChild("StaticImageHandleO");
        this->StaticImageHandleI = this->StaticImageBackground->getChild("StaticImageHandleI");

        this->WidgetComboboxExControllerType = this->StaticImageBackground->getChild("WidgetComboboxExControllerType");
        this->WidgetComboboxMovie = this->StaticImageBackground->getChild("WidgetComboboxMovie");

        this->WidgetImageButtonMovieDelete = this->StaticImageBackground->getChild("WidgetImageButtonMovieDelete");

        this->WidgetImageButtonMovieRec = this->StaticImageBackground->getChild("WidgetImageButtonMovieRec");
        this->WidgetImageButtonMovieRecPlay = this->StaticImageBackground->getChild("WidgetImageButtonMovieRecPlay");
        this->WidgetImageButtonMovieRecStop = this->StaticImageBackground->getChild("WidgetImageButtonMovieRecStop");

        this->WidgetImageButtonHardwareReset = this->StaticImageBackground->getChild("WidgetImageButtonHardwareReset");
        this->WidgetImageButtonSoftwareReset = this->StaticImageBackground->getChild("WidgetImageButtonSoftwareReset");

        this->WidgetImageButtonSnapshot = this->StaticImageBackground->getChild("WidgetImageButtonSnapshot");

        this->StaticTextMovieMTime = this->StaticImageBackground->getChild("StaticTextMovieMTime");
        //
        this->pWidgetComboboxExControllerType = (CEGUI::Combobox*)(this->WidgetComboboxExControllerType);
        this->pWidgetComboboxMovie = (CEGUI::Combobox*)(this->WidgetComboboxMovie);
        this->pEditboxMovie = this->pWidgetComboboxMovie->getEditbox();

        this->DefaultWindowWristband->subscribeEvent(CEGUI::Window::EventMouseButtonDown, CEGUI::Event::Subscriber(&mmLayerEmulatorToolbarL::OnStaticImageBackgroundEventMouseButtonDown, this));
        this->DefaultWindowWristband->subscribeEvent(CEGUI::Window::EventMouseButtonUp, CEGUI::Event::Subscriber(&mmLayerEmulatorToolbarL::OnStaticImageBackgroundEventMouseButtonUp, this));
        this->LayerEmulatorToolbarL->subscribeEvent(CEGUI::Window::EventMouseButtonDown, CEGUI::Event::Subscriber(&mmLayerEmulatorToolbarL::OnToolbarLEventMouseButtonDown, this));
        this->LayerEmulatorToolbarL->subscribeEvent(CEGUI::Window::EventMouseButtonUp, CEGUI::Event::Subscriber(&mmLayerEmulatorToolbarL::OnToolbarLEventMouseButtonUp, this));
        //
        this->WidgetImageButtonMovieDelete->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerEmulatorToolbarL::OnWidgetImageButtonMovieDeleteEventMouseClick, this));
        //
        this->WidgetComboboxExControllerType->subscribeEvent(CEGUI::Combobox::EventTextAccepted, CEGUI::Event::Subscriber(&mmLayerEmulatorToolbarL::OnWidgetComboboxExControllerTypeEventTextAccepted, this));
        this->WidgetComboboxExControllerType->subscribeEvent(CEGUI::Combobox::EventListSelectionAccepted, CEGUI::Event::Subscriber(&mmLayerEmulatorToolbarL::OnWidgetComboboxExControllerTypeEventListSelectionAccepted, this));
        //
        this->pEditboxMovie->subscribeEvent(CEGUI::Window::EventTextChanged, CEGUI::Event::Subscriber(&mmLayerEmulatorToolbarL::OnWidgetComboboxMovieEventTextChanged, this));
        this->WidgetComboboxMovie->subscribeEvent(CEGUI::Combobox::EventTextAccepted, CEGUI::Event::Subscriber(&mmLayerEmulatorToolbarL::OnWidgetComboboxMovieEventTextAccepted, this));
        this->WidgetComboboxMovie->subscribeEvent(CEGUI::Combobox::EventListSelectionAccepted, CEGUI::Event::Subscriber(&mmLayerEmulatorToolbarL::OnWidgetComboboxMovieEventListSelectionAccepted, this));
        //
        this->WidgetImageButtonMovieRec->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerEmulatorToolbarL::OnWidgetImageButtonMovieRecEventMouseClick, this));
        this->WidgetImageButtonMovieRecPlay->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerEmulatorToolbarL::OnWidgetImageButtonMovieRecPlayEventMouseClick, this));
        this->WidgetImageButtonMovieRecStop->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerEmulatorToolbarL::OnWidgetImageButtonMovieRecStopEventMouseClick, this));
        //
        this->WidgetImageButtonHardwareReset->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerEmulatorToolbarL::OnWidgetImageButtonHardwareResetEventMouseClick, this));
        this->WidgetImageButtonSoftwareReset->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerEmulatorToolbarL::OnWidgetImageButtonSoftwareResetEventMouseClick, this));
        //
        this->WidgetImageButtonSnapshot->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerEmulatorToolbarL::OnWidgetImageButtonSnapshotEventMouseClick, this));

        mm::mmEventSet* pEventSet = &pEmulatorMachine->hEventSet;
        pEventSet->SubscribeEvent(Event_MM_EMU_RS_MOVIE_REC, &mmLayerEmulatorToolbarL::OnEmulatorEvent_MM_EMU_RS_MOVIE_REC, this);
        pEventSet->SubscribeEvent(Event_MM_EMU_RS_MOVIE_PLAY, &mmLayerEmulatorToolbarL::OnEmulatorEvent_MM_EMU_RS_MOVIE_PLAY, this);
        pEventSet->SubscribeEvent(Event_MM_EMU_RS_MOVIE_STOP, &mmLayerEmulatorToolbarL::OnEmulatorEvent_MM_EMU_RS_MOVIE_STOP, this);
        pEventSet->SubscribeEvent(Event_MM_EMU_NT_MOVIE_FINISH, &mmLayerEmulatorToolbarL::OnEmulatorEvent_MM_EMU_NT_MOVIE_FINISH, this);
        pEventSet->SubscribeEvent(Event_MM_EMU_NT_EXCONTROLLER, &mmLayerEmulatorToolbarL::OnEmulatorEvent_MM_EMU_NT_EXCONTROLLER, this);

        this->CreateComboboxTypeItem();
    }

    void mmLayerEmulatorToolbarL::OnBeforeTerminate(void)
    {
        struct mmSurfaceMaster* pSurfaceMaster = this->pSurfaceMaster;

        this->DeleteComboboxTypeItem();
        this->DeleteComboboxMovieItem();

        mm::mmEventSet* pEventSet = &pEmulatorMachine->hEventSet;
        pEventSet->UnsubscribeEvent(Event_MM_EMU_RS_MOVIE_REC, &mmLayerEmulatorToolbarL::OnEmulatorEvent_MM_EMU_RS_MOVIE_REC, this);
        pEventSet->UnsubscribeEvent(Event_MM_EMU_RS_MOVIE_PLAY, &mmLayerEmulatorToolbarL::OnEmulatorEvent_MM_EMU_RS_MOVIE_PLAY, this);
        pEventSet->UnsubscribeEvent(Event_MM_EMU_RS_MOVIE_STOP, &mmLayerEmulatorToolbarL::OnEmulatorEvent_MM_EMU_RS_MOVIE_STOP, this);
        pEventSet->UnsubscribeEvent(Event_MM_EMU_NT_MOVIE_FINISH, &mmLayerEmulatorToolbarL::OnEmulatorEvent_MM_EMU_NT_MOVIE_FINISH, this);
        pEventSet->UnsubscribeEvent(Event_MM_EMU_NT_EXCONTROLLER, &mmLayerEmulatorToolbarL::OnEmulatorEvent_MM_EMU_NT_EXCONTROLLER, this);

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        pWindowManager->destroyWindow(this->LayerEmulatorToolbarL);
        this->LayerEmulatorToolbarL = NULL;
    }
    void mmLayerEmulatorToolbarL::UpdateLayerValue(void)
    {
        this->UpdateComboboxMovieItem();

        this->UpdateUtf8View();

        this->UpdateMovieStatusView();

        this->UpdateExcontrollerView();
    }
    void mmLayerEmulatorToolbarL::UpdateUtf8View(void)
    {
        this->UpdateMovieComboboxView();
        this->UpdateMovieEditView();
    }
    void mmLayerEmulatorToolbarL::UpdateExcontrollerView(void)
    {
        mmInt_t type = mmEmulatorMachine_GetExControllerType(this->pEmulatorMachine);
        this->pWidgetComboboxExControllerType->setItemSelectState(type, true);
    }
    void mmLayerEmulatorToolbarL::UpdateMovieComboboxView(void)
    {
        struct mmIconvContext* pIconvContext = this->pIconvContext;

        mmLayerUtilityListUrlItem* pItem = NULL;

        size_t index = 0;

        while (index < this->hItemMovieVector.size)
        {
            pItem = (mmLayerUtilityListUrlItem*)mmVectorValue_GetIndexReference(&this->hItemMovieVector, index);
            pItem->UpdateUtf8View();
            index++;
        }
    }
    void mmLayerEmulatorToolbarL::UpdateMovieEditView(void)
    {
        struct mmIconvContext* pIconvContext = this->pIconvContext;
        mmLayerUtilityIconv_ViewName(pIconvContext, this->WidgetComboboxMovie, &this->hMovieItem.basename);
    }
    void mmLayerEmulatorToolbarL::UpdateMovieStatusView(void)
    {
        mmTm_t tm;
        struct mmTimeInfo tp;
        char hTimeString[64] = { 0 };
        //
        mmExplorerItem_UpdateMode(&this->hMovieItem);
        //
        if (0 == mmAccess(mmString_CStr(&this->hMovieItem.fullname), MM_ACCESS_F_OK))
        {
            this->WidgetImageButtonMovieDelete->setVisible(true);
            this->WidgetImageButtonMovieRecPlay->setVisible(true);
            this->StaticTextMovieMTime->setVisible(true);
        }
        else
        {
            this->WidgetImageButtonMovieDelete->setVisible(false);
            this->WidgetImageButtonMovieRecPlay->setVisible(false);
            this->StaticTextMovieMTime->setVisible(false);
        }
        //
        tp.sec = this->hMovieItem.s.st_mtime;
        tp.msec = 0;
        mmTime_Tm(&tp, &tm);
        // 2000/12/11 12:33
        mmSprintf(hTimeString, "%4d/%02d/%02d %02d:%02d",
            tm.mmTm_year, tm.mmTm_mon,
            tm.mmTm_mday, tm.mmTm_hour,
            tm.mmTm_min);

        this->StaticTextMovieMTime->setText(hTimeString);
    }
    void mmLayerEmulatorToolbarL::RefreshComboboxMovieLayer(void)
    {
        this->UpdateComboboxMovieItem();

        this->UpdateMovieComboboxView();
    }
    void mmLayerEmulatorToolbarL::CreateComboboxTypeItem(void)
    {
        static size_t hSize = MM_ARRAY_SIZE(MM_EMU_EXCONTROLLER_TYPE);

        size_t i = 0;

        mmLayerUtilityTextItem* pItem = NULL;
        const char* pCoder = NULL;

        mmVectorValue_AlignedMemory(&this->hItemTypeVector, hSize);

        while (i < hSize)
        {
            pCoder = MM_EMU_EXCONTROLLER_TYPE[i];
            pItem = (mmLayerUtilityTextItem*)mmVectorValue_GetIndexReference(&this->hItemTypeVector, i);
            pItem->setText(pCoder);
            pItem->setID(i);
            pItem->setSelectionBrushImage("TaharezLook/TextSelectionBrush");
            pItem->SetScaleY(1.1f);
            this->pWidgetComboboxExControllerType->addItem(pItem);
            i++;
        }
    }
    void mmLayerEmulatorToolbarL::DeleteComboboxTypeItem(void)
    {
        static size_t hSize = MM_ARRAY_SIZE(MM_EMU_EXCONTROLLER_TYPE);

        size_t i = 0;

        CEGUI::ListboxTextItem* pItem = NULL;

        while (i < hSize)
        {
            pItem = (mmLayerUtilityTextItem*)mmVectorValue_GetIndexReference(&this->hItemTypeVector, i);
            this->pWidgetComboboxExControllerType->removeItem(pItem);
            i++;
        }

        // clear the weak ref.
        mmVectorValue_Clear(&this->hItemTypeVector);
    }
    void mmLayerEmulatorToolbarL::CreateComboboxMovieItem(void)
    {
        struct mmEmulatorAssets* pEmulatorAssets = &this->pEmulatorMachine->hEmulatorAssets;
        struct mmFileContext* pFileContext = this->pEmulatorMachine->pFileContext;
        struct mmEmuEmulator* pEmulator = &this->pEmulatorMachine->hEmulator;
        struct mmEmuNes* pNes = pEmulator->emu;
        struct mmEmuNesConfig* pEmuNesConfig = &pNes->config;
        struct mmEmuPathConfig* pEmuPathConfig = &pEmuNesConfig->path;

        struct mmString hPattern;
        struct mmRbtreeStringVpt hRbtree;

        mmString_Init(&hPattern);
        mmRbtreeStringVpt_Init(&hRbtree);

        mmString_Assign(&this->hMoviePathName, &pEmulatorAssets->emulator_directory);
        mmString_Appends(&this->hMoviePathName, "/");
        mmString_Append(&this->hMoviePathName, &pEmuPathConfig->szMoviePath);

        mmString_Assign(&hPattern, &this->hMoviePathName);
        mmString_Appends(&hPattern, "/");
        mmString_Appends(&hPattern, "*");

        mmString_Assign(&this->hMovieRealPath, &pEmulatorAssets->writable_path);
        mmString_Appends(&this->hMovieRealPath, "/");
        mmString_Append(&this->hMovieRealPath, &this->hMoviePathName);

        mmFileContext_AcquireFindFiles(pFileContext, mmString_CStr(&pEmulatorAssets->writable_path), mmString_CStr(&hPattern), 0, 0, 1, &hRbtree);

        {
            size_t index = 0;
            mmLayerUtilityListUrlItem* pItem = NULL;
            mmLayerUtilityTextItem* pTextItem;

            struct mmRbNode* n = NULL;
            struct mmRbtreeStringVptIterator* it = NULL;
            struct mmFileAssetsInfo* e = NULL;

            mmVectorValue_AlignedMemory(&this->hItemMovieVector, hRbtree.size);

            n = mmRb_First(&hRbtree.rbt);
            while (NULL != n)
            {
                it = (struct mmRbtreeStringVptIterator*)mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
                n = mmRb_Next(n);
                e = (struct mmFileAssetsInfo*)(it->v);
                //
                pItem = (mmLayerUtilityListUrlItem*)mmVectorValue_GetIndexReference(&this->hItemMovieVector, index);
                pItem->SetItemId(index);
                pItem->SetUrl(mmString_CStr(&e->pathname), mmString_CStr(&e->basename));
                pItem->SetIconvContext(this->pIconvContext);
                pItem->SetHideSuffixName(MM_FALSE);
                pTextItem = &pItem->hTextItem;
                pTextItem->setSelectionBrushImage("TaharezLook/TextSelectionBrush");
                pTextItem->SetScaleY(1.1f);
                pItem->OnFinishLaunching();
                this->pWidgetComboboxMovie->addItem(pTextItem);
                index++;
            }
        }

        mmFileContext_ReleaseFindFiles(pFileContext, &hRbtree);

        mmString_Destroy(&hPattern);
        mmRbtreeStringVpt_Destroy(&hRbtree);
    }
    void mmLayerEmulatorToolbarL::DeleteComboboxMovieItem(void)
    {
        size_t hSize = this->hItemMovieVector.size;

        size_t index = 0;

        mmLayerUtilityListUrlItem* pItem = NULL;
        CEGUI::ListboxTextItem* pTextItem;

        while (index < hSize)
        {
            pItem = (mmLayerUtilityListUrlItem*)mmVectorValue_GetIndexReference(&this->hItemMovieVector, index);
            pTextItem = &pItem->hTextItem;
            this->pWidgetComboboxMovie->removeItem(pTextItem);
            pItem->OnBeforeTerminate();
            index++;
        }

        mmVectorValue_Clear(&this->hItemMovieVector);
    }
    void mmLayerEmulatorToolbarL::UpdateComboboxMovieItem(void)
    {
        this->DeleteComboboxMovieItem();
        this->CreateComboboxMovieItem();

        if (mmString_Empty(&this->hMovieBaseName))
        {
            if (0 == this->hItemMovieVector.size)
            {
                // use default.
                this->hCurrentMovieIndex = 0;

                mmString_Assigns(&this->hMovieBaseName, __MovieBaseNameDefault);

                mmExplorerItem_SetPath(&this->hMovieItem, mmString_CStr(&this->hMovieRealPath), mmString_CStr(&this->hMovieBaseName));
            }
            else
            {
                mmLayerUtilityListUrlItem* pItem = NULL;

                // choose the current one.
                // choose the first one.
                this->hCurrentMovieIndex = this->hCurrentMovieIndex < this->hItemMovieVector.size ? this->hCurrentMovieIndex : 0;

                pItem = (mmLayerUtilityListUrlItem*)mmVectorValue_GetIndexReference(&this->hItemMovieVector, this->hCurrentMovieIndex);

                mmString_Assigns(&this->hMovieBaseName, mmString_CStr(&pItem->hUrlItem.basename));

                mmExplorerItem_SetPath(&this->hMovieItem, mmString_CStr(&this->hMovieRealPath), mmString_CStr(&this->hMovieBaseName));
            }
        }
        else
        {
            // use the assign value.
            this->hCurrentMovieIndex = 0;

            mmExplorerItem_SetPath(&this->hMovieItem, mmString_CStr(&this->hMovieRealPath), mmString_CStr(&this->hMovieBaseName));
        }

        this->UpdateMovieStatusView();
    }
    bool mmLayerEmulatorToolbarL::OnEmulatorEvent_MM_EMU_RS_MOVIE_REC(const mmEventArgs& args)
    {
        this->WidgetImageButtonMovieRec->setVisible(false);
        this->WidgetImageButtonMovieRecStop->setVisible(true);

        this->RefreshComboboxMovieLayer();
        return true;
    }
    bool mmLayerEmulatorToolbarL::OnEmulatorEvent_MM_EMU_RS_MOVIE_PLAY(const mmEventArgs& args)
    {
        this->WidgetImageButtonMovieRec->setVisible(true);
        this->WidgetImageButtonMovieRecStop->setVisible(false);
        return true;
    }
    bool mmLayerEmulatorToolbarL::OnEmulatorEvent_MM_EMU_RS_MOVIE_STOP(const mmEventArgs& args)
    {
        this->WidgetImageButtonMovieRec->setVisible(true);
        this->WidgetImageButtonMovieRecStop->setVisible(false);
        return true;
    }
    bool mmLayerEmulatorToolbarL::OnEmulatorEvent_MM_EMU_NT_MOVIE_FINISH(const mmEventArgs& args)
    {
        this->WidgetImageButtonMovieRec->setVisible(true);
        this->WidgetImageButtonMovieRecStop->setVisible(false);

        mmEmulatorMachine_Pause(this->pEmulatorMachine);
        return true;
    }
    bool mmLayerEmulatorToolbarL::OnEmulatorEvent_MM_EMU_NT_EXCONTROLLER(const mmEventArgs& args)
    {
        this->UpdateExcontrollerView();
        return true;
    }
    bool mmLayerEmulatorToolbarL::OnStaticImageBackgroundEventMouseButtonDown(const CEGUI::EventArgs& args)
    {
        CEGUI::String hColourString;
        hColourString = CEGUI::PropertyHelper<CEGUI::Colour>::toString(this->hColourLustre);

        this->StaticImageHandleI->setProperty("ImageColours", hColourString);
        return true;
    }
    bool mmLayerEmulatorToolbarL::OnStaticImageBackgroundEventMouseButtonUp(const CEGUI::EventArgs& args)
    {
        CEGUI::String hColourString;
        hColourString = CEGUI::PropertyHelper<CEGUI::Colour>::toString(this->hColourCommon);

        this->StaticImageHandleI->setProperty("ImageColours", hColourString);

        this->StaticImageBackground->setVisible(false);
        this->StaticImageHandleO->setVisible(true);
        return true;
    }
    bool mmLayerEmulatorToolbarL::OnToolbarLEventMouseButtonDown(const CEGUI::EventArgs& args)
    {
        CEGUI::String hColourString;
        hColourString = CEGUI::PropertyHelper<CEGUI::Colour>::toString(this->hColourLustre);

        this->StaticImageHandleO->setProperty("ImageColours", hColourString);
        return true;
    }
    bool mmLayerEmulatorToolbarL::OnToolbarLEventMouseButtonUp(const CEGUI::EventArgs& args)
    {
        CEGUI::String hColourString;
        hColourString = CEGUI::PropertyHelper<CEGUI::Colour>::toString(this->hColourCommon);

        this->StaticImageHandleO->setProperty("ImageColours", hColourString);

        this->StaticImageBackground->setVisible(true);
        this->StaticImageHandleO->setVisible(false);
        return true;
    }
    bool mmLayerEmulatorToolbarL::OnWidgetImageButtonMovieDeleteEventMouseClick(const CEGUI::EventArgs& args)
    {
        mmLayerContext* w = mmLayerDirector_LayerAdd(this->pDirector, "mm/mmLayerCommonEnsureCode", "mmLayerEmulatorCommonEnsureCode");
        mmLayerCommonEnsureCode* pLayerEnsure = (mmLayerCommonEnsureCode*)w;
        // 0x00000001 delete assets.
        pLayerEnsure->SetDescriptionErrorcode(0x00000001);
        pLayerEnsure->RandomConfirmCode();
        pLayerEnsure->StartTimerTerminate();

        assert(false == pLayerEnsure->hEventChoiceConn.isValid() || false == pLayerEnsure->hEventChoiceConn->connected() && "we must detach_handle after event handle.");

        pLayerEnsure->hEventChoiceConn = pLayerEnsure->subscribeEvent(
            mmLayerCommonEnsureCode::EventChoice, CEGUI::Event::Subscriber(&mmLayerEmulatorToolbarL::OnLayerEnsureEventChoiceRemove, this));
        return true;
    }
    bool mmLayerEmulatorToolbarL::OnWidgetComboboxExControllerTypeEventTextAccepted(const CEGUI::EventArgs& args)
    {
        this->OnWidgetComboboxExControllerTypeEventListSelectionAccepted(args);
        return true;
    }
    bool mmLayerEmulatorToolbarL::OnWidgetComboboxExControllerTypeEventListSelectionAccepted(const CEGUI::EventArgs& args)
    {
        const CEGUI::WindowEventArgs& evt = (const CEGUI::WindowEventArgs&)(args);
        CEGUI::Combobox* pCombobox = (CEGUI::Combobox*)(evt.window);
        do
        {
            mmInt_t ntype = 0;
            mmInt_t otype = 0;

            CEGUI::ListboxItem* pSelectItem = pCombobox->getSelectedItem();
            if (NULL == pSelectItem)
            {
                break;
            }
            ntype = (mmInt_t)pSelectItem->getID();
            otype = mmEmulatorMachine_GetExControllerType(this->pEmulatorMachine);
            if (ntype == otype)
            {
                break;
            }
            mmEmulatorMachine_SetExControllerType(this->pEmulatorMachine, (int)ntype);
        } while (0);
        return true;
    }
    bool mmLayerEmulatorToolbarL::OnWidgetComboboxMovieEventTextChanged(const CEGUI::EventArgs& args)
    {
        this->OnWidgetComboboxMovieEventTextAccepted(args);
        return true;
    }
    bool mmLayerEmulatorToolbarL::OnWidgetComboboxMovieEventTextAccepted(const CEGUI::EventArgs& args)
    {
        const CEGUI::WindowEventArgs& evt = (const CEGUI::WindowEventArgs&)(args);
        CEGUI::Combobox* pCombobox = (CEGUI::Combobox*)(evt.window);

        struct mmIconvContext* pIconvContext = this->pIconvContext;

        mmLayerUtilityIconv_HostName(pIconvContext, pCombobox, &this->hTextBaseNameHost);

        mmString_Assign(&this->hMovieBaseName, &this->hTextBaseNameHost);

        mmExplorerItem_SetPath(&this->hMovieItem, mmString_CStr(&this->hMovieRealPath), mmString_CStr(&this->hMovieBaseName));

        this->UpdateMovieStatusView();
        return true;
    }
    bool mmLayerEmulatorToolbarL::OnWidgetComboboxMovieEventListSelectionAccepted(const CEGUI::EventArgs& args)
    {
        const CEGUI::WindowEventArgs& evt = (const CEGUI::WindowEventArgs&)(args);
        CEGUI::Combobox* pCombobox = (CEGUI::Combobox*)(evt.window);

        do
        {
            size_t index = 0;
            mmLayerUtilityListUrlItem* pItem = NULL;

            CEGUI::ListboxItem* _select_item = pCombobox->getSelectedItem();
            if (NULL == _select_item)
            {
                break;
            }
            index = (size_t)_select_item->getID();
            pItem = (mmLayerUtilityListUrlItem*)mmVectorValue_GetIndexReference(&this->hItemMovieVector, index);

            this->hCurrentMovieIndex = index;

            mmString_Assign(&this->hMovieBaseName, &pItem->hUrlItem.basename);

            mmExplorerItem_SetPath(&this->hMovieItem, mmString_CStr(&this->hMovieRealPath), mmString_CStr(&this->hMovieBaseName));
        } while (0);

        return true;
    }
    bool mmLayerEmulatorToolbarL::OnWidgetImageButtonMovieRecEventMouseClick(const CEGUI::EventArgs& args)
    {
        mmEmulatorMachine_MovieRec(this->pEmulatorMachine, mmString_CStr(&this->hMovieBaseName));
        return true;
    }
    bool mmLayerEmulatorToolbarL::OnWidgetImageButtonMovieRecPlayEventMouseClick(const CEGUI::EventArgs& args)
    {
        mmEmulatorMachine_Play(this->pEmulatorMachine);
        mmEmulatorMachine_MoviePlay(this->pEmulatorMachine, mmString_CStr(&this->hMovieBaseName));
        return true;
    }
    bool mmLayerEmulatorToolbarL::OnWidgetImageButtonMovieRecStopEventMouseClick(const CEGUI::EventArgs& args)
    {
        mmEmulatorMachine_MovieStop(this->pEmulatorMachine);
        return true;
    }
    
    bool mmLayerEmulatorToolbarL::OnWidgetImageButtonHardwareResetEventMouseClick(const CEGUI::EventArgs& args)
    {
        mmEmulatorMachine_HardwareReset(this->pEmulatorMachine);
        return true;
    }
    bool mmLayerEmulatorToolbarL::OnWidgetImageButtonSoftwareResetEventMouseClick(const CEGUI::EventArgs& args)
    {
        mmEmulatorMachine_SoftwareReset(this->pEmulatorMachine);
        return true;
    }

    bool mmLayerEmulatorToolbarL::OnWidgetImageButtonSnapshotEventMouseClick(const CEGUI::EventArgs& args)
    {
        mmEmulatorMachine_Snapshot(this->pEmulatorMachine);
        return true;
    }
    bool mmLayerEmulatorToolbarL::OnLayerEnsureEventChoiceRemove(const CEGUI::EventArgs& args)
    {
        const mmChoiceEventArgs& evt = (const mmChoiceEventArgs&)(args);

        if (mmChoiceEventArgs::ENSURE == evt.hCode)
        {
            mmExplorerRemove(mmString_CStr(&this->hMovieItem.fullname));

            mmString_Clear(&this->hMovieBaseName);

            this->RefreshComboboxMovieLayer();
        }
        return true;
    }
}
