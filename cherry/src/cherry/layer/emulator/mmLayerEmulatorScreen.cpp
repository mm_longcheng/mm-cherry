/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerEmulatorScreen.h"

#include "CEGUIOgreRenderer/Texture.h"
#include "CEGUIOgreRenderer/GeometryBuffer.h"

#include "emulator/mmEmulatorMachine.h"

namespace mm
{
    mmLayerEmulatorScreen::mmLayerEmulatorScreen(void)
        : pWindowScreen(NULL)

        , hScreenEventUpdatedConn()

        , pEmulatorMachine(NULL)
        , hSourceMaterialPtr()
        , hSourceImageName("")
    {

    }
    mmLayerEmulatorScreen::~mmLayerEmulatorScreen(void)
    {

    }

    void mmLayerEmulatorScreen::SetWindowScreen(CEGUI::Window* pWindowScreen)
    {
        this->pWindowScreen = pWindowScreen;
    }
    void mmLayerEmulatorScreen::SetEmulatorMachine(struct mmEmulatorMachine* pEmulatorMachine)
    {
        this->pEmulatorMachine = pEmulatorMachine;
    }
    void mmLayerEmulatorScreen::OnFinishLaunching()
    {
        CEGUI::GeometryBuffer* pGeometryBuffer = &this->pWindowScreen->getGeometryBuffer();
        CEGUI::CEGUIOgreGeometryBuffer* pCEGUIOgreGeometryBuffer = (CEGUI::CEGUIOgreGeometryBuffer*)pGeometryBuffer;
        Ogre::MaterialPtr hMaterialPtr = mmEmulatorMachine_GetScreenMaterial(this->pEmulatorMachine);
        this->hSourceMaterialPtr = pCEGUIOgreGeometryBuffer->getMaterial();
        pCEGUIOgreGeometryBuffer->setMaterial(hMaterialPtr);

        this->hSourceImageName = this->pWindowScreen->getProperty("Image");
        const std::string& hImageName = mmEmulatorMachine_GetImageName(this->pEmulatorMachine);
        this->pWindowScreen->setProperty("Image", hImageName);

        this->hScreenEventUpdatedConn = this->pWindowScreen->subscribeEvent(CEGUI::Window::EventUpdated, CEGUI::Event::Subscriber(&mmLayerEmulatorScreen::OnScreenEventUpdated, this));
    }
    void mmLayerEmulatorScreen::OnBeforeTerminate()
    {
        this->hScreenEventUpdatedConn->disconnect();

        this->pWindowScreen->setProperty("Image", this->hSourceImageName);
        this->hSourceImageName = "";

        CEGUI::GeometryBuffer* pGeometryBuffer = &this->pWindowScreen->getGeometryBuffer();
        CEGUI::CEGUIOgreGeometryBuffer* pCEGUIOgreGeometryBuffer = (CEGUI::CEGUIOgreGeometryBuffer*)pGeometryBuffer;
        pCEGUIOgreGeometryBuffer->setMaterial(this->hSourceMaterialPtr);
        this->hSourceMaterialPtr.reset();
    }
    bool mmLayerEmulatorScreen::OnScreenEventUpdated(const CEGUI::EventArgs& args)
    {
        const CEGUI::WindowEventArgs& evt = (const CEGUI::WindowEventArgs&)(args);
        evt.window->invalidate();
        return true;
    }
}
