/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmLayerEmulatorToolbarL_h__
#define __mmLayerEmulatorToolbarL_h__

#include "core/mmCore.h"

#include "container/mmVectorVpt.h"
#include "container/mmVectorValue.h"

#include "dish/mmEvent.h"

#include "layer/director/mmLayerContext.h"

#include "CEGUI/Window.h"

#include "explorer/mmExplorerItem.h"

struct mmEmulatorMachine;
struct mmIconvContext;

namespace mm
{
    class mmLayerEmulatorToolbarL : public mmLayerContext
    {
    public:
        static const CEGUI::String EventNamespace;          //!< Namespace for global events
        static const CEGUI::String WidgetTypeName;          //!< Window factory name
    public:
        CEGUI::Window* LayerEmulatorToolbarL;
        CEGUI::Window* StaticImageBackground;

        CEGUI::Window* DefaultWindowWristband;

        CEGUI::Window* StaticImageHandleO;
        CEGUI::Window* StaticImageHandleI;

        CEGUI::Window* WidgetComboboxExControllerType;
        CEGUI::Window* WidgetComboboxMovie;

        CEGUI::Window* WidgetImageButtonMovieDelete;

        CEGUI::Window* WidgetImageButtonMovieRec;
        CEGUI::Window* WidgetImageButtonMovieRecPlay;
        CEGUI::Window* WidgetImageButtonMovieRecStop;

        CEGUI::Window* WidgetImageButtonHardwareReset;
        CEGUI::Window* WidgetImageButtonSoftwareReset;

        CEGUI::Window* WidgetImageButtonSnapshot;

        CEGUI::Window* StaticTextMovieMTime;
        // weak ref.
        CEGUI::Combobox* pWidgetComboboxExControllerType;
        // weak ref.
        CEGUI::Combobox* pWidgetComboboxMovie;
        //
        CEGUI::Editbox* pEditboxMovie;
    public:
        struct mmEmulatorMachine* pEmulatorMachine;
    public:
        struct mmIconvContext* pIconvContext;// weak ref.
    public:
        struct mmVectorValue hItemTypeVector;
        struct mmVectorValue hItemMovieVector;
    public:
        struct mmExplorerItem hMovieItem;
        //
        struct mmString hMovieRealPath;
        struct mmString hMoviePathName;
        struct mmString hMovieBaseName;
        // cache
        struct mmString hTextBaseNameHost;
    public:
        size_t hCurrentMovieIndex;
    public:
        CEGUI::Colour hColourLustre;
        CEGUI::Colour hColourCommon;
    public:
        mmLayerEmulatorToolbarL(const CEGUI::String& type, const CEGUI::String& name);
        virtual ~mmLayerEmulatorToolbarL(void);
    public:
        void SetEmulatorMachine(struct mmEmulatorMachine* pEmulatorMachine);
        void SetIconvContext(struct mmIconvContext* pIconvContext);
        void SetMovieBaseName(const char* pStateBaseName);
    public:
        // mmLayerContext
        virtual void OnFinishLaunching(void);
        virtual void OnBeforeTerminate(void);
    public:
        void UpdateLayerValue(void);
        void UpdateUtf8View(void);
        void UpdateExcontrollerView(void);
        void UpdateMovieComboboxView(void);
        void UpdateMovieEditView(void);
        void UpdateMovieStatusView(void);
    public:
        void RefreshComboboxMovieLayer(void);
    private:
        void CreateComboboxTypeItem(void);
        void DeleteComboboxTypeItem(void);
    private:
        void CreateComboboxMovieItem(void);
        void DeleteComboboxMovieItem(void);
        void UpdateComboboxMovieItem(void);
    private:
        bool OnEmulatorEvent_MM_EMU_RS_MOVIE_REC(const mmEventArgs& args);
        bool OnEmulatorEvent_MM_EMU_RS_MOVIE_PLAY(const mmEventArgs& args);
        bool OnEmulatorEvent_MM_EMU_RS_MOVIE_STOP(const mmEventArgs& args);
        bool OnEmulatorEvent_MM_EMU_NT_MOVIE_FINISH(const mmEventArgs& args);
        bool OnEmulatorEvent_MM_EMU_NT_EXCONTROLLER(const mmEventArgs& args);
    private:
        bool OnStaticImageBackgroundEventMouseButtonDown(const CEGUI::EventArgs& args);
        bool OnStaticImageBackgroundEventMouseButtonUp(const CEGUI::EventArgs& args);
        bool OnToolbarLEventMouseButtonDown(const CEGUI::EventArgs& args);
        bool OnToolbarLEventMouseButtonUp(const CEGUI::EventArgs& args);
        //
        bool OnWidgetImageButtonMovieDeleteEventMouseClick(const CEGUI::EventArgs& args);
        //
        bool OnWidgetComboboxExControllerTypeEventTextAccepted(const CEGUI::EventArgs& args);
        bool OnWidgetComboboxExControllerTypeEventListSelectionAccepted(const CEGUI::EventArgs& args);
        //
        bool OnWidgetComboboxMovieEventTextChanged(const CEGUI::EventArgs& args);
        bool OnWidgetComboboxMovieEventTextAccepted(const CEGUI::EventArgs& args);
        bool OnWidgetComboboxMovieEventListSelectionAccepted(const CEGUI::EventArgs& args);
        //
        bool OnWidgetImageButtonMovieRecEventMouseClick(const CEGUI::EventArgs& args);
        bool OnWidgetImageButtonMovieRecPlayEventMouseClick(const CEGUI::EventArgs& args);
        bool OnWidgetImageButtonMovieRecStopEventMouseClick(const CEGUI::EventArgs& args);
        //
        bool OnWidgetImageButtonHardwareResetEventMouseClick(const CEGUI::EventArgs& args);
        bool OnWidgetImageButtonSoftwareResetEventMouseClick(const CEGUI::EventArgs& args);
        //
        bool OnWidgetImageButtonSnapshotEventMouseClick(const CEGUI::EventArgs& args);
    private:
        bool OnLayerEnsureEventChoiceRemove(const CEGUI::EventArgs& args);
    };
}

#endif//__mmLayerEmulatorToolbarL_h__