/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmLayerEmulatorWindow_h__
#define __mmLayerEmulatorWindow_h__

#include "core/mmCore.h"

#include "dish/mmEvent.h"

#include "nwsi/mmSurfaceMaster.h"

#include "layer/director/mmLayerContext.h"

#include "layer/utility/mmLayerUtilityFinger.h"

#include "mmLayerEmulatorPadZapper.h"
#include "mmLayerEmulatorScreen.h"

struct mmEmulatorMachine;

namespace mm
{
    class mmLayerContext;

    class mmLayerEmulatorWindow
    {
    public:
        struct mmEmulatorMachine* pEmulatorMachine;
        mmLayerContext* pLayerContext;
        CEGUI::Window* pWindowScreen;
    public:
        mmLayerUtilityFingerTouch hFingerTouch;
        //
        mmLayerEmulatorPadZapper hEmulatorPadZapper;
        mmLayerEmulatorScreen hEmulatorScreen;
    public:
        CEGUI::Event::Connection hLayerEventKeypadPressedConn;
        CEGUI::Event::Connection hLayerEventKeypadReleaseConn;
        //
        CEGUI::Event::Connection hLayerEventTouchsMovedConn;
        CEGUI::Event::Connection hLayerEventTouchsBeganConn;
        CEGUI::Event::Connection hLayerEventTouchsEndedConn;
        CEGUI::Event::Connection hLayerEventTouchsBreakConn;
        //
        CEGUI::Event::Connection hLayerEventUpdatedConn;
    public:
        mmLayerEmulatorWindow(void);
        ~mmLayerEmulatorWindow(void);
    public:
        void SetEmulatorMachine(struct mmEmulatorMachine* pEmulatorMachine);
        void SetLayerContext(mmLayerContext* pLayerContext);
        void SetWindowScreen(CEGUI::Window* pWindowScreen);
    public:
        void SetFingerIsVisible(bool hVisible);
    public:
        void OnFinishLaunching(void);
        void OnBeforeTerminate(void);
    public:
        void UpdateTransform(void);
    private:
        bool OnLayerEventKeypadPressed(const CEGUI::EventArgs& args);
        bool OnLayerEventKeypadRelease(const CEGUI::EventArgs& args);
    private:
        bool OnLayerEventTouchsMoved(const CEGUI::EventArgs& args);
        bool OnLayerEventTouchsBegan(const CEGUI::EventArgs& args);
        bool OnLayerEventTouchsEnded(const CEGUI::EventArgs& args);
        bool OnLayerEventTouchsBreak(const CEGUI::EventArgs& args);
    private:
        bool OnLayerEventUpdated(const CEGUI::EventArgs& args);
    };
}

#endif//__mmLayerEmulatorWindow_h__