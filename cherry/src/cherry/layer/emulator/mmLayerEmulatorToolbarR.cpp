/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerEmulatorToolbarR.h"

#include "CEGUI/WindowManager.h"
#include "CEGUI/widgets/Combobox.h"
#include "CEGUI/widgets/Editbox.h"
#include "CEGUI/widgets/ListboxTextItem.h"

#include "layer/director/mmLayerDirector.h"

#include "emulator/mmEmulatorMachine.h"

#include "emu/mmEmuNes.h"
#include "emu/mmEmuNesConfig.h"

#include "explorer/mmExplorerCommand.h"

#include "layer/utility/mmLayerUtilityIconv.h"
#include "layer/utility/mmLayerUtilityListUrlItem.h"
#include "layer/utility/mmLayerUtilityTextItem.h"

#include "layer/common/mmLayerCommonEnsureCode.h"
#include "layer/common/mmLayerCommonEnsureEvent.h"

namespace mm
{
    static const char* gStateBaseNameDefault = "state_0.sta";

    const CEGUI::String mmLayerEmulatorToolbarR::EventNamespace = "mm";
    const CEGUI::String mmLayerEmulatorToolbarR::WidgetTypeName = "mm/mmLayerEmulatorToolbarR";

    mmLayerEmulatorToolbarR::mmLayerEmulatorToolbarR(const CEGUI::String& type, const CEGUI::String& name)
        : mmLayerContext(type, name)
        , LayerEmulatorToolbarR(NULL)
        , StaticImageBackground(NULL)

        , DefaultWindowWristband(NULL)

        , StaticImageHandleO(NULL)
        , StaticImageHandleI(NULL)

        , WidgetComboboxVideoMode(NULL)
        , WidgetComboboxState(NULL)
        , WidgetComboboxURapidAB(NULL)

        , LabelRapidAB(NULL)

        , WidgetImageButtonStateDelete(NULL)

        , WidgetImageButtonAudioHorn(NULL)
        , WidgetImageButtonAudioMute(NULL)

        , WidgetImageButtonEmuPlay(NULL)
        , WidgetImageButtonEmuStop(NULL)

        , WidgetImageButtonStateLoad(NULL)
        , WidgetImageButtonStateSave(NULL)

        , StaticTextStateMTime(NULL)

        , pWidgetComboboxVideoMode(NULL)
        , pWidgetComboboxState(NULL)
        , pWidgetComboboxURapidAB(NULL)

        , pEditboxState(NULL)

        , pEmulatorMachine(NULL)
        , pIconvContext(NULL)

        , hCurrentStateIndex(0)

        , hColourLustre(0.0f, 0.0f, 1.0f, 1.0f)
        , hColourCommon(1.0f, 1.0f, 1.0f, 1.0f)
    {
        struct mmVectorValueEventAllocator hEventAllocator;

        mmVectorValue_Init(&this->hItemModeVector);
        mmVectorValue_Init(&this->hItemStateVector);
        mmVectorValue_Init(&this->hItemRapidVector);

        mmExplorerItem_Init(&this->hStateItem);

        mmString_Init(&this->hStateRealPath);
        mmString_Init(&this->hStatePathName);
        mmString_Init(&this->hStateBaseName);
        mmString_Init(&this->hTextBaseNameHost);

        hEventAllocator.Produce = &mmVectorValue_TextItemEventProduce;
        hEventAllocator.Recycle = &mmVectorValue_TextItemEventRecycle;
        hEventAllocator.obj = this;
        mmVectorValue_SetElemSize(&this->hItemModeVector, sizeof(mmLayerUtilityTextItem));
        mmVectorValue_SetEventAllocator(&this->hItemModeVector, &hEventAllocator);

        hEventAllocator.Produce = &mmVectorValue_ListUrlItemEventProduce;
        hEventAllocator.Recycle = &mmVectorValue_ListUrlItemEventRecycle;
        hEventAllocator.obj = this;
        mmVectorValue_SetElemSize(&this->hItemStateVector, sizeof(mmLayerUtilityListUrlItem));
        mmVectorValue_SetEventAllocator(&this->hItemStateVector, &hEventAllocator);

        hEventAllocator.Produce = &mmVectorValue_TextItemEventProduce;
        hEventAllocator.Recycle = &mmVectorValue_TextItemEventRecycle;
        hEventAllocator.obj = this;
        mmVectorValue_SetElemSize(&this->hItemRapidVector, sizeof(mmLayerUtilityTextItem));
        mmVectorValue_SetEventAllocator(&this->hItemRapidVector, &hEventAllocator);
    }
    mmLayerEmulatorToolbarR::~mmLayerEmulatorToolbarR(void)
    {
        assert(0 == this->hItemModeVector.size && "clear item mode map before destroy.");
        assert(0 == this->hItemStateVector.size && "clear item state map before destroy.");
        assert(0 == this->hItemRapidVector.size && "clear item state map before destroy.");

        mmVectorValue_Destroy(&this->hItemModeVector);
        mmVectorValue_Destroy(&this->hItemStateVector);
        mmVectorValue_Destroy(&this->hItemRapidVector);

        mmExplorerItem_Destroy(&this->hStateItem);

        mmString_Destroy(&this->hStateRealPath);
        mmString_Destroy(&this->hStatePathName);
        mmString_Destroy(&this->hStateBaseName);
        mmString_Destroy(&this->hTextBaseNameHost);
    }
    void mmLayerEmulatorToolbarR::SetEmulatorMachine(struct mmEmulatorMachine* pEmulatorMachine)
    {
        this->pEmulatorMachine = pEmulatorMachine;
    }
    void mmLayerEmulatorToolbarR::SetIconvContext(struct mmIconvContext* pIconvContext)
    {
        this->pIconvContext = pIconvContext;
    }
    void mmLayerEmulatorToolbarR::SetStateBaseName(const char* pStateBaseName)
    {
        mmString_Assigns(&this->hStateBaseName, pStateBaseName);
    }
    void mmLayerEmulatorToolbarR::OnFinishLaunching(void)
    {
        struct mmSurfaceMaster* pSurfaceMaster = this->pSurfaceMaster;

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        this->LayerEmulatorToolbarR = pWindowManager->loadLayoutFromFile("emulator/LayerEmulatorToolbarR.layout");
        this->addChild(this->LayerEmulatorToolbarR);
        // Manual set whole area.
        CEGUI::URect hWholeArea(CEGUI::UDim(0, 0), CEGUI::UDim(0, 0), CEGUI::UDim(1, 0), CEGUI::UDim(1, 0));
        this->LayerEmulatorToolbarR->setArea(hWholeArea);

        this->StaticImageBackground = this->LayerEmulatorToolbarR->getChild("StaticImageBackground");

        this->DefaultWindowWristband = this->StaticImageBackground->getChild("DefaultWindowWristband");

        this->StaticImageHandleO = this->LayerEmulatorToolbarR->getChild("StaticImageHandleO");
        this->StaticImageHandleI = this->StaticImageBackground->getChild("StaticImageHandleI");

        this->WidgetComboboxVideoMode = this->StaticImageBackground->getChild("WidgetComboboxVideoMode");
        this->WidgetComboboxState = this->StaticImageBackground->getChild("WidgetComboboxState");
        this->WidgetComboboxURapidAB = this->StaticImageBackground->getChild("WidgetComboboxURapidAB");

        this->LabelRapidAB = this->StaticImageBackground->getChild("LabelRapidAB");

        this->WidgetImageButtonStateDelete = this->StaticImageBackground->getChild("WidgetImageButtonStateDelete");

        this->WidgetImageButtonAudioHorn = this->StaticImageBackground->getChild("WidgetImageButtonAudioHorn");
        this->WidgetImageButtonAudioMute = this->StaticImageBackground->getChild("WidgetImageButtonAudioMute");

        this->WidgetImageButtonEmuPlay = this->StaticImageBackground->getChild("WidgetImageButtonEmuPlay");
        this->WidgetImageButtonEmuStop = this->StaticImageBackground->getChild("WidgetImageButtonEmuStop");

        this->WidgetImageButtonStateLoad = this->StaticImageBackground->getChild("WidgetImageButtonStateLoad");
        this->WidgetImageButtonStateSave = this->StaticImageBackground->getChild("WidgetImageButtonStateSave");
        
        this->StaticTextStateMTime = this->StaticImageBackground->getChild("StaticTextStateMTime");
        //
        this->pWidgetComboboxVideoMode = (CEGUI::Combobox*)(this->WidgetComboboxVideoMode);
        this->pWidgetComboboxState = (CEGUI::Combobox*)(this->WidgetComboboxState);
        this->pWidgetComboboxURapidAB = (CEGUI::Combobox*)(this->WidgetComboboxURapidAB);
        this->pEditboxState = this->pWidgetComboboxState->getEditbox();

        this->DefaultWindowWristband->subscribeEvent(CEGUI::Window::EventMouseButtonDown, CEGUI::Event::Subscriber(&mmLayerEmulatorToolbarR::OnStaticImageBackgroundEventMouseButtonDown, this));
        this->DefaultWindowWristband->subscribeEvent(CEGUI::Window::EventMouseButtonUp, CEGUI::Event::Subscriber(&mmLayerEmulatorToolbarR::OnStaticImageBackgroundEventMouseButtonUp, this));
        this->LayerEmulatorToolbarR->subscribeEvent(CEGUI::Window::EventMouseButtonDown, CEGUI::Event::Subscriber(&mmLayerEmulatorToolbarR::OnToolbarREventMouseButtonDown, this));
        this->LayerEmulatorToolbarR->subscribeEvent(CEGUI::Window::EventMouseButtonUp, CEGUI::Event::Subscriber(&mmLayerEmulatorToolbarR::OnToolbarREventMouseButtonUp, this));
        //
        this->WidgetImageButtonStateDelete->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerEmulatorToolbarR::OnWidgetImageButtonStateDeleteEventMouseClick, this));
        //
        this->WidgetComboboxVideoMode->subscribeEvent(CEGUI::Combobox::EventTextAccepted, CEGUI::Event::Subscriber(&mmLayerEmulatorToolbarR::OnWidgetComboboxVideoModeEventTextAccepted, this));
        this->WidgetComboboxVideoMode->subscribeEvent(CEGUI::Combobox::EventListSelectionAccepted, CEGUI::Event::Subscriber(&mmLayerEmulatorToolbarR::OnWidgetComboboxVideoModeEventListSelectionAccepted, this));
        //
        this->pEditboxState->subscribeEvent(CEGUI::Window::EventTextChanged, CEGUI::Event::Subscriber(&mmLayerEmulatorToolbarR::OnWidgetComboboxStateEventTextChanged, this));
        this->WidgetComboboxState->subscribeEvent(CEGUI::Combobox::EventTextAccepted, CEGUI::Event::Subscriber(&mmLayerEmulatorToolbarR::OnWidgetComboboxStateEventTextAccepted, this));
        this->WidgetComboboxState->subscribeEvent(CEGUI::Combobox::EventListSelectionAccepted, CEGUI::Event::Subscriber(&mmLayerEmulatorToolbarR::OnWidgetComboboxStateEventListSelectionAccepted, this));
        //
        this->WidgetComboboxURapidAB->subscribeEvent(CEGUI::Combobox::EventTextAccepted, CEGUI::Event::Subscriber(&mmLayerEmulatorToolbarR::OnWidgetComboboxRapidABEventTextAccepted, this));
        this->WidgetComboboxURapidAB->subscribeEvent(CEGUI::Combobox::EventListSelectionAccepted, CEGUI::Event::Subscriber(&mmLayerEmulatorToolbarR::OnWidgetComboboxRapidABEventListSelectionAccepted, this));
        //
        this->WidgetImageButtonAudioHorn->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerEmulatorToolbarR::OnWidgetImageButtonAudioHornEventMouseClick, this));
        this->WidgetImageButtonAudioMute->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerEmulatorToolbarR::OnWidgetImageButtonAudioMuteEventMouseClick, this));
        //
        this->WidgetImageButtonEmuPlay->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerEmulatorToolbarR::OnWidgetImageButtonEmuPlayEventMouseClick, this));
        this->WidgetImageButtonEmuStop->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerEmulatorToolbarR::OnWidgetImageButtonEmuStopEventMouseClick, this));
        //
        this->WidgetImageButtonStateLoad->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerEmulatorToolbarR::OnWidgetImageButtonStateLoadEventMouseClick, this));
        this->WidgetImageButtonStateSave->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerEmulatorToolbarR::OnWidgetImageButtonStateSaveEventMouseClick, this));

        mm::mmEventSet* pEventSet = &pEmulatorMachine->hEventSet;
        pEventSet->SubscribeEvent(Event_MM_EMU_RS_STATE_SAVE, &mmLayerEmulatorToolbarR::OnEmulatorEvent_MM_EMU_RS_STATE_SAVE, this);
        pEventSet->SubscribeEvent(Event_MM_EMU_NT_PLAY, &mmLayerEmulatorToolbarR::OnEmulatorEvent_MM_EMU_NT_PLAY, this);
        pEventSet->SubscribeEvent(Event_MM_EMU_NT_STOP, &mmLayerEmulatorToolbarR::OnEmulatorEvent_MM_EMU_NT_STOP, this);
        pEventSet->SubscribeEvent(Event_MM_EMU_NT_VIDEOMODE, &mmLayerEmulatorToolbarR::OnEmulatorEvent_MM_EMU_NT_VIDEOMODE, this);

        this->CreateComboboxModeItem();
        this->CreateComboboxRapidItem();
    }

    void mmLayerEmulatorToolbarR::OnBeforeTerminate(void)
    {
        struct mmSurfaceMaster* pSurfaceMaster = this->pSurfaceMaster;

        this->DeleteComboboxRapidItem();
        this->DeleteComboboxModeItem();
        this->DeleteComboboxStateItem();

        mm::mmEventSet* pEventSet = &pEmulatorMachine->hEventSet;
        pEventSet->UnsubscribeEvent(Event_MM_EMU_RS_STATE_SAVE, &mmLayerEmulatorToolbarR::OnEmulatorEvent_MM_EMU_RS_STATE_SAVE, this);
        pEventSet->UnsubscribeEvent(Event_MM_EMU_NT_PLAY, &mmLayerEmulatorToolbarR::OnEmulatorEvent_MM_EMU_NT_PLAY, this);
        pEventSet->UnsubscribeEvent(Event_MM_EMU_NT_STOP, &mmLayerEmulatorToolbarR::OnEmulatorEvent_MM_EMU_NT_STOP, this);
        pEventSet->UnsubscribeEvent(Event_MM_EMU_NT_VIDEOMODE, &mmLayerEmulatorToolbarR::OnEmulatorEvent_MM_EMU_NT_VIDEOMODE, this);

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        pWindowManager->destroyWindow(this->LayerEmulatorToolbarR);
        this->LayerEmulatorToolbarR = NULL;
    }
    void mmLayerEmulatorToolbarR::UpdateLayerValue(void)
    {
        this->UpdateComboboxStateItem();

        this->UpdateUtf8View();

        this->UpdateStateStatusView();

        this->UpdateVideoModeView();

        this->UpdateRapidView();

        mmInt_t state = mmEmulatorMachine_GetState(this->pEmulatorMachine);
        this->WidgetImageButtonEmuPlay->setVisible(MM_EMULATOR_PLAYING != state);
        this->WidgetImageButtonEmuStop->setVisible(MM_EMULATOR_PLAYING == state);
    }
    void mmLayerEmulatorToolbarR::UpdateUtf8View(void)
    {
        this->UpdateStateComboboxView();
        this->UpdateStateEditView();
    }
    void mmLayerEmulatorToolbarR::UpdateVideoModeView(void)
    {
        mmInt_t mode = mmEmulatorMachine_GetVideoMode(this->pEmulatorMachine);
        this->pWidgetComboboxVideoMode->setItemSelectState(mode, true);
    }
    void mmLayerEmulatorToolbarR::UpdateRapidView(void)
    {
        mmWord_t ospeed[2] = { 0 };

        mmEmulatorMachine_GetRapidSpeed(this->pEmulatorMachine, 0, ospeed);

        // In most cases, the rapid speed AB is same, we just use Rapid A for init.
        this->pWidgetComboboxURapidAB->setItemSelectState((size_t)ospeed[0], true);
    }
    void mmLayerEmulatorToolbarR::UpdateStateComboboxView(void)
    {
        struct mmIconvContext* pIconvContext = this->pIconvContext;

        mmLayerUtilityListUrlItem* pItem = NULL;

        size_t index = 0;

        while (index < this->hItemStateVector.size)
        {
            pItem = (mmLayerUtilityListUrlItem*)mmVectorValue_GetIndexReference(&this->hItemStateVector, index);
            pItem->UpdateUtf8View();
            index++;
        }
    }
    void mmLayerEmulatorToolbarR::UpdateStateEditView(void)
    {
        struct mmIconvContext* pIconvContext = this->pIconvContext;
        mmLayerUtilityIconv_ViewName(pIconvContext, this->WidgetComboboxState, &this->hStateItem.basename);
    }
    void mmLayerEmulatorToolbarR::UpdateStateStatusView(void)
    {
        mmTm_t tm;
        struct mmTimeInfo tp;
        char hTimeString[64] = { 0 };
        //
        mmExplorerItem_UpdateMode(&this->hStateItem);
        //
        if (0 == mmAccess(mmString_CStr(&this->hStateItem.fullname), MM_ACCESS_F_OK))
        {
            this->WidgetImageButtonStateDelete->setVisible(true);
            this->WidgetImageButtonStateLoad->setVisible(true);
            this->StaticTextStateMTime->setVisible(true);
        }
        else
        {
            this->WidgetImageButtonStateDelete->setVisible(false);
            this->WidgetImageButtonStateLoad->setVisible(false);
            this->StaticTextStateMTime->setVisible(false);
        }
        //
        tp.sec = this->hStateItem.s.st_mtime;
        tp.msec = 0;
        mmTime_Tm(&tp, &tm);
        // 2000/12/11 12:33
        mmSprintf(hTimeString, "%4d/%02d/%02d %02d:%02d",
            tm.mmTm_year, tm.mmTm_mon,
            tm.mmTm_mday, tm.mmTm_hour,
            tm.mmTm_min);

        this->StaticTextStateMTime->setText(hTimeString);
    }
    void mmLayerEmulatorToolbarR::RefreshComboboxStateLayer(void)
    {
        this->UpdateComboboxStateItem();

        this->UpdateStateComboboxView();
    }
    void mmLayerEmulatorToolbarR::CreateComboboxModeItem(void)
    {
        static size_t hSize = MM_ARRAY_SIZE(MM_EMU_VIDEO_MODE);

        size_t i = 0;

        mmLayerUtilityTextItem* pItem = NULL;
        const char* pCoder = NULL;

        mmVectorValue_Resize(&this->hItemModeVector, hSize);

        while (i < hSize)
        {
            pCoder = MM_EMU_VIDEO_MODE[i];
            pItem = (mmLayerUtilityTextItem*)mmVectorValue_GetIndexReference(&this->hItemModeVector, i);
            pItem->setText(pCoder);
            pItem->setID(i);
            pItem->setSelectionBrushImage("TaharezLook/TextSelectionBrush");
            pItem->SetScaleY(1.1f);
            this->pWidgetComboboxVideoMode->addItem(pItem);
            i++;
        }
    }
    void mmLayerEmulatorToolbarR::DeleteComboboxModeItem(void)
    {
        static size_t hSize = MM_ARRAY_SIZE(MM_EMU_VIDEO_MODE);

        size_t i = 0;

        mmLayerUtilityTextItem* pItem = NULL;

        while (i < hSize)
        {
            pItem = (mmLayerUtilityTextItem*)mmVectorValue_GetIndexReference(&this->hItemModeVector, i);
            this->pWidgetComboboxVideoMode->removeItem(pItem);
            i++;
        }

        // clear the weak ref.
        mmVectorValue_Clear(&this->hItemModeVector);
    }
    void mmLayerEmulatorToolbarR::CreateComboboxStateItem(void)
    {
        struct mmEmulatorAssets* pEmulatorAssets = &this->pEmulatorMachine->hEmulatorAssets;
        struct mmFileContext* pFileContext = this->pEmulatorMachine->pFileContext;
        struct mmEmuEmulator* pEmulator = &this->pEmulatorMachine->hEmulator;
        struct mmEmuNes* pNes = pEmulator->emu;
        struct mmEmuNesConfig* pEmuNesConfig = &pNes->config;
        struct mmEmuPathConfig* pEmuPathConfig = &pEmuNesConfig->path;

        struct mmString hPattern;
        struct mmRbtreeStringVpt hRbtree;

        mmString_Init(&hPattern);
        mmRbtreeStringVpt_Init(&hRbtree);

        mmString_Assign(&this->hStatePathName, &pEmulatorAssets->emulator_directory);
        mmString_Appends(&this->hStatePathName, "/");
        mmString_Append(&this->hStatePathName, &pEmuPathConfig->szStatePath);

        mmString_Assign(&hPattern, &this->hStatePathName);
        mmString_Appends(&hPattern, "/");
        mmString_Appends(&hPattern, "*");

        mmString_Assign(&this->hStateRealPath, &pEmulatorAssets->writable_path);
        mmString_Appends(&this->hStateRealPath, "/");
        mmString_Append(&this->hStateRealPath, &this->hStatePathName);

        mmFileContext_AcquireFindFiles(pFileContext, mmString_CStr(&pEmulatorAssets->writable_path), mmString_CStr(&hPattern), 0, 0, 1, &hRbtree);

        {
            size_t index = 0;
            mmLayerUtilityListUrlItem* pItem = NULL;
            mmLayerUtilityTextItem* pTextItem;

            struct mmRbNode* n = NULL;
            struct mmRbtreeStringVptIterator* it = NULL;
            struct mmFileAssetsInfo* e = NULL;

            mmVectorValue_Resize(&this->hItemStateVector, hRbtree.size);

            n = mmRb_First(&hRbtree.rbt);
            while (NULL != n)
            {
                it = (struct mmRbtreeStringVptIterator*)mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
                n = mmRb_Next(n);
                e = (struct mmFileAssetsInfo*)(it->v);
                //
                pItem = (mmLayerUtilityListUrlItem*)mmVectorValue_GetIndexReference(&this->hItemStateVector, index);
                pItem->SetItemId(index);
                pItem->SetUrl(mmString_CStr(&e->pathname), mmString_CStr(&e->basename));
                pItem->SetIconvContext(this->pIconvContext);
                pItem->SetHideSuffixName(MM_FALSE);
                pTextItem = &pItem->hTextItem;
                pTextItem->setSelectionBrushImage("TaharezLook/TextSelectionBrush");
                pTextItem->SetScaleY(1.1f);
                pItem->OnFinishLaunching();
                this->pWidgetComboboxState->addItem(pTextItem);
                index++;
            }
        }

        mmFileContext_ReleaseFindFiles(pFileContext, &hRbtree);

        mmString_Destroy(&hPattern);
        mmRbtreeStringVpt_Destroy(&hRbtree);
    }
    void mmLayerEmulatorToolbarR::DeleteComboboxStateItem(void)
    {
        size_t _size = this->hItemStateVector.size;

        size_t index = 0;

        mmLayerUtilityListUrlItem* pItem = NULL;
        CEGUI::ListboxTextItem* pTextItem;

        while (index < _size)
        {
            pItem = (mmLayerUtilityListUrlItem*)mmVectorValue_GetIndexReference(&this->hItemStateVector, index);
            pTextItem = &pItem->hTextItem;
            this->pWidgetComboboxState->removeItem(pTextItem);
            pItem->OnBeforeTerminate();
            index++;
        }

        // clear the weak ref.
        mmVectorValue_Clear(&this->hItemStateVector);
    }
    void mmLayerEmulatorToolbarR::UpdateComboboxStateItem(void)
    {
        this->DeleteComboboxStateItem();
        this->CreateComboboxStateItem();

        if (mmString_Empty(&this->hStateBaseName))
        {
            if (0 == this->hItemStateVector.size)
            {
                // use default.
                this->hCurrentStateIndex = 0;

                mmString_Assigns(&this->hStateBaseName, gStateBaseNameDefault);

                mmExplorerItem_SetPath(&this->hStateItem, mmString_CStr(&this->hStateRealPath), mmString_CStr(&this->hStateBaseName));
            }
            else
            {
                mmLayerUtilityListUrlItem* pItem = NULL;

                // choose the current one.
                // choose the first one.
                this->hCurrentStateIndex = this->hCurrentStateIndex < this->hItemStateVector.size ? this->hCurrentStateIndex : 0;

                pItem = (mmLayerUtilityListUrlItem*)mmVectorValue_GetIndexReference(&this->hItemStateVector, this->hCurrentStateIndex);

                mmString_Assigns(&this->hStateBaseName, mmString_CStr(&pItem->hUrlItem.basename));

                mmExplorerItem_SetPath(&this->hStateItem, mmString_CStr(&this->hStateRealPath), mmString_CStr(&this->hStateBaseName));
            }
        }
        else
        {
            // use the assign value.
            this->hCurrentStateIndex = 0;

            mmExplorerItem_SetPath(&this->hStateItem, mmString_CStr(&this->hStateRealPath), mmString_CStr(&this->hStateBaseName));
        }

        this->UpdateStateStatusView();
    }
    void mmLayerEmulatorToolbarR::CreateComboboxRapidItem(void)
    {
        static size_t hSize = MM_ARRAY_SIZE(MM_EMU_PAD_RAPID_SPEED);

        size_t i = 0;

        mmLayerUtilityTextItem* pItem = NULL;
        const char* pCoder = NULL;

        mmVectorValue_Resize(&this->hItemRapidVector, hSize);

        while (i < hSize)
        {
            pCoder = MM_EMU_PAD_RAPID_SPEED[i];
            pItem = (mmLayerUtilityTextItem*)mmVectorValue_GetIndexReference(&this->hItemRapidVector, i);
            pItem->setText(pCoder);
            pItem->setID(i);
            pItem->setSelectionBrushImage("TaharezLook/TextSelectionBrush");
            pItem->SetScaleY(1.1f);
            this->pWidgetComboboxURapidAB->addItem(pItem);
            i++;
        }
    }
    void mmLayerEmulatorToolbarR::DeleteComboboxRapidItem(void)
    {
        static size_t hSize = MM_ARRAY_SIZE(MM_EMU_PAD_RAPID_SPEED);

        size_t i = 0;

        mmLayerUtilityTextItem* pItem = NULL;

        while (i < hSize)
        {
            pItem = (mmLayerUtilityTextItem*)mmVectorValue_GetIndexReference(&this->hItemRapidVector, i);
            this->pWidgetComboboxURapidAB->removeItem(pItem);
            i++;
        }

        // clear the weak ref.
        mmVectorValue_Clear(&this->hItemRapidVector);
    }
    bool mmLayerEmulatorToolbarR::OnEmulatorEvent_MM_EMU_RS_STATE_SAVE(const mmEventArgs& args)
    {
        this->RefreshComboboxStateLayer();
        return true;
    }
    bool mmLayerEmulatorToolbarR::OnEmulatorEvent_MM_EMU_NT_PLAY(const mmEventArgs& args)
    {
        this->WidgetImageButtonEmuPlay->setVisible(false);
        this->WidgetImageButtonEmuStop->setVisible(true);
        return true;
    }
    bool mmLayerEmulatorToolbarR::OnEmulatorEvent_MM_EMU_NT_STOP(const mmEventArgs& args)
    {
        this->WidgetImageButtonEmuPlay->setVisible(true);
        this->WidgetImageButtonEmuStop->setVisible(false);
        return true;
    }
    bool mmLayerEmulatorToolbarR::OnEmulatorEvent_MM_EMU_NT_VIDEOMODE(const mmEventArgs& args)
    {
        this->UpdateVideoModeView();
        return true;
    }
    bool mmLayerEmulatorToolbarR::OnStaticImageBackgroundEventMouseButtonDown(const CEGUI::EventArgs& args)
    {
        CEGUI::String hColourString;
        hColourString = CEGUI::PropertyHelper<CEGUI::Colour>::toString(this->hColourLustre);

        this->StaticImageHandleI->setProperty("ImageColours", hColourString);
        return true;
    }
    bool mmLayerEmulatorToolbarR::OnStaticImageBackgroundEventMouseButtonUp(const CEGUI::EventArgs& args)
    {
        CEGUI::String hColourString;
        hColourString = CEGUI::PropertyHelper<CEGUI::Colour>::toString(this->hColourCommon);

        this->StaticImageHandleI->setProperty("ImageColours", hColourString);

        this->StaticImageBackground->setVisible(false);
        this->StaticImageHandleO->setVisible(true);
        return true;
    }
    bool mmLayerEmulatorToolbarR::OnToolbarREventMouseButtonDown(const CEGUI::EventArgs& args)
    {
        CEGUI::String hColourString;
        hColourString = CEGUI::PropertyHelper<CEGUI::Colour>::toString(this->hColourLustre);

        this->StaticImageHandleO->setProperty("ImageColours", hColourString);
        return true;
    }
    bool mmLayerEmulatorToolbarR::OnToolbarREventMouseButtonUp(const CEGUI::EventArgs& args)
    {
        CEGUI::String hColourString;
        hColourString = CEGUI::PropertyHelper<CEGUI::Colour>::toString(this->hColourCommon);

        this->StaticImageHandleO->setProperty("ImageColours", hColourString);

        this->StaticImageBackground->setVisible(true);
        this->StaticImageHandleO->setVisible(false);
        return true;
    }
    bool mmLayerEmulatorToolbarR::OnWidgetImageButtonStateDeleteEventMouseClick(const CEGUI::EventArgs& args)
    {
        mmLayerContext* w = mmLayerDirector_LayerAdd(this->pDirector, "mm/mmLayerCommonEnsureCode", "mmLayerEmulatorCommonEnsureCode");
        mmLayerCommonEnsureCode* pLayerEnsure = (mmLayerCommonEnsureCode*)w;
        // 0x00000001 delete assets.
        pLayerEnsure->SetDescriptionErrorcode(0x00000001);
        pLayerEnsure->RandomConfirmCode();
        pLayerEnsure->StartTimerTerminate();

        assert(false == pLayerEnsure->hEventChoiceConn.isValid() || false == pLayerEnsure->hEventChoiceConn->connected() && "we must detach_handle after event handle.");

        pLayerEnsure->hEventChoiceConn = pLayerEnsure->subscribeEvent(
            mmLayerCommonEnsureCode::EventChoice, 
            CEGUI::Event::Subscriber(&mmLayerEmulatorToolbarR::OnLayerEnsureEventChoiceRemove, this));
        return true;
    }
    bool mmLayerEmulatorToolbarR::OnWidgetComboboxVideoModeEventTextAccepted(const CEGUI::EventArgs& args)
    {
        this->OnWidgetComboboxVideoModeEventListSelectionAccepted(args);
        return true;
    }
    bool mmLayerEmulatorToolbarR::OnWidgetComboboxVideoModeEventListSelectionAccepted(const CEGUI::EventArgs& args)
    {
        const CEGUI::WindowEventArgs& evt = (const CEGUI::WindowEventArgs&)(args);
        CEGUI::Combobox* pCombobox = (CEGUI::Combobox*)(evt.window);
        do
        {
            mmInt_t nmode = 0;
            mmInt_t omode = 0;

            CEGUI::ListboxItem* pSelectItem = pCombobox->getSelectedItem();
            if (NULL == pSelectItem)
            {
                break;
            }
            nmode = (mmInt_t)pSelectItem->getID();
            omode = mmEmulatorMachine_GetVideoMode(this->pEmulatorMachine);
            if (nmode == omode)
            {
                break;
            }
            mmEmulatorMachine_SetVideoMode(this->pEmulatorMachine, (int)nmode);
        } while (0);
        return true;
    }
    bool mmLayerEmulatorToolbarR::OnWidgetComboboxStateEventTextChanged(const CEGUI::EventArgs& args)
    {
        this->OnWidgetComboboxStateEventTextAccepted(args);
        return true;
    }
    bool mmLayerEmulatorToolbarR::OnWidgetComboboxStateEventTextAccepted(const CEGUI::EventArgs& args)
    {
        const CEGUI::WindowEventArgs& evt = (const CEGUI::WindowEventArgs&)(args);
        CEGUI::Combobox* pCombobox = (CEGUI::Combobox*)(evt.window);

        struct mmIconvContext* pIconvContext = this->pIconvContext;

        mmLayerUtilityIconv_HostName(pIconvContext, pCombobox, &this->hTextBaseNameHost);

        mmString_Assign(&this->hStateBaseName, &this->hTextBaseNameHost);

        mmExplorerItem_SetPath(&this->hStateItem, mmString_CStr(&this->hStateRealPath), mmString_CStr(&this->hStateBaseName));

        this->UpdateStateStatusView();
        return true;
    }
    bool mmLayerEmulatorToolbarR::OnWidgetComboboxStateEventListSelectionAccepted(const CEGUI::EventArgs& args)
    {
        const CEGUI::WindowEventArgs& evt = (const CEGUI::WindowEventArgs&)(args);
        CEGUI::Combobox* pCombobox = (CEGUI::Combobox*)(evt.window);

        do
        {
            size_t index = 0;
            mmLayerUtilityListUrlItem* pItem = NULL;

            CEGUI::ListboxItem* pSelectItem = pCombobox->getSelectedItem();
            if (NULL == pSelectItem)
            {
                break;
            }
            index = (size_t)pSelectItem->getID();
            pItem = (mmLayerUtilityListUrlItem*)mmVectorValue_GetIndexReference(&this->hItemStateVector, index);

            this->hCurrentStateIndex = index;

            mmString_Assign(&this->hStateBaseName, &pItem->hUrlItem.basename);

            mmExplorerItem_SetPath(&this->hStateItem, mmString_CStr(&this->hStateRealPath), mmString_CStr(&this->hStateBaseName));
        } while (0);

        return true;
    }
    bool mmLayerEmulatorToolbarR::OnWidgetComboboxRapidABEventTextAccepted(const CEGUI::EventArgs& args)
    {
        this->OnWidgetComboboxRapidABEventListSelectionAccepted(args);
        return true;
    }
    bool mmLayerEmulatorToolbarR::OnWidgetComboboxRapidABEventListSelectionAccepted(const CEGUI::EventArgs& args)
    {
        const CEGUI::WindowEventArgs& evt = (const CEGUI::WindowEventArgs&)(args);
        CEGUI::Combobox* pCombobox = (CEGUI::Combobox*)(evt.window);

        do
        {
            mmWord_t nspeed[2] = { 0 };
            mmWord_t ospeed[2] = { 0 };

            CEGUI::ListboxItem* pSelectItem = pCombobox->getSelectedItem();
            if (NULL == pSelectItem)
            {
                break;
            }

            // In most cases, the rapid speed AB is same.
            nspeed[0] = (mmWord_t)pSelectItem->getID();
            nspeed[1] = (mmWord_t)pSelectItem->getID();

            mmEmulatorMachine_GetRapidSpeed(this->pEmulatorMachine, 0, ospeed);

            if (nspeed[0] == ospeed[0] && nspeed[1] == ospeed[1])
            {
                break;
            }

            mmEmulatorMachine_SetRapidSpeed(this->pEmulatorMachine, 0, nspeed);
        } while (0);

        return true;
    }
    bool mmLayerEmulatorToolbarR::OnWidgetImageButtonAudioHornEventMouseClick(const CEGUI::EventArgs& args)
    {
        this->WidgetImageButtonAudioHorn->setVisible(false);
        this->WidgetImageButtonAudioMute->setVisible(true);

        mmEmulatorMachine_SetMute(this->pEmulatorMachine, false);
        return true;
    }
    bool mmLayerEmulatorToolbarR::OnWidgetImageButtonAudioMuteEventMouseClick(const CEGUI::EventArgs& args)
    {
        this->WidgetImageButtonAudioHorn->setVisible(true);
        this->WidgetImageButtonAudioMute->setVisible(false);

        mmEmulatorMachine_SetMute(this->pEmulatorMachine, true);
        return true;
    }

    bool mmLayerEmulatorToolbarR::OnWidgetImageButtonEmuPlayEventMouseClick(const CEGUI::EventArgs& args)
    {
        mmEmulatorMachine_Play(this->pEmulatorMachine);
        return true;
    }
    bool mmLayerEmulatorToolbarR::OnWidgetImageButtonEmuStopEventMouseClick(const CEGUI::EventArgs& args)
    {
        mmEmulatorMachine_Pause(this->pEmulatorMachine);
        return true;
    }
    //
    bool mmLayerEmulatorToolbarR::OnWidgetImageButtonStateLoadEventMouseClick(const CEGUI::EventArgs& args)
    {
        mmEmulatorMachine_StateLoad(this->pEmulatorMachine, mmString_CStr(&this->hStateBaseName));
        return true;
    }
    bool mmLayerEmulatorToolbarR::OnWidgetImageButtonStateSaveEventMouseClick(const CEGUI::EventArgs& args)
    {
        mmEmulatorMachine_StateSave(this->pEmulatorMachine, mmString_CStr(&this->hStateBaseName));
        return true;
    }
    bool mmLayerEmulatorToolbarR::OnLayerEnsureEventChoiceRemove(const CEGUI::EventArgs& args)
    {
        const mmChoiceEventArgs& evt = (const mmChoiceEventArgs&)(args);

        if (mmChoiceEventArgs::ENSURE == evt.hCode)
        {
            mmExplorerRemove(mmString_CStr(&this->hStateItem.fullname));

            mmString_Clear(&this->hStateBaseName);

            this->RefreshComboboxStateLayer();
        }
        return true;
    }
}
