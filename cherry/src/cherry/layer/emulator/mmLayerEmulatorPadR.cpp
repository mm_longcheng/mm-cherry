/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerEmulatorPadR.h"
#include "mmLayerEmulatorTransform.h"

#include "CEGUI/WindowManager.h"

#include "layer/utility/mmLayerUtilityFinger.h"

#include "emulator/mmEmulatorMachine.h"

namespace mm
{
    const CEGUI::String mmLayerEmulatorPadR::EventNamespace = "mm";
    const CEGUI::String mmLayerEmulatorPadR::WidgetTypeName = "mm/mmLayerEmulatorPadR";

    mmLayerEmulatorPadR::mmLayerEmulatorPadR(const CEGUI::String& type, const CEGUI::String& name)
        : mmLayerContext(type, name)
        , LayerEmulatorPadR(NULL)
        , StaticImageBackground(NULL)

        , pFingerTouch(NULL)

        , pEmulatorMachine(NULL)

        , hNormalName("StaticImage_normal")
        , hLustreName("StaticImage_lustre")
    {
        mmRbtreeStringVpt_Init(&this->hEmuPadRbtree);
        mmLayerUtilityTransform_Init(&this->hEmulatorTransform);
    }
    mmLayerEmulatorPadR::~mmLayerEmulatorPadR(void)
    {
        mmLayerUtilityTransform_Destroy(&this->hEmulatorTransform);
        mmRbtreeStringVpt_Destroy(&this->hEmuPadRbtree);
    }
    void mmLayerEmulatorPadR::SetFingerTouch(mmLayerUtilityFingerTouch* pFingerTouch)
    {
        this->pFingerTouch = pFingerTouch;
    }
    void mmLayerEmulatorPadR::SetEmulatorMachine(struct mmEmulatorMachine* pEmulatorMachine)
    {
        this->pEmulatorMachine = pEmulatorMachine;
    }
    void mmLayerEmulatorPadR::SetNormalName(const std::string& hNormalName)
    {
        this->hNormalName = hNormalName;

        mmLayerEmulatorPadRWidget* u = NULL;
        struct mmRbNode* n = NULL;
        struct mmRbtreeStringVptIterator* it = NULL;
        //
        n = mmRb_First(&this->hEmuPadRbtree.rbt);
        while (NULL != n)
        {
            it = (struct mmRbtreeStringVptIterator*)mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
            n = mmRb_Next(n);
            // 
            u = (mmLayerEmulatorPadRWidget*)it->v;
            u->SetNormalName(this->hNormalName);
        }
    }
    void mmLayerEmulatorPadR::SetLustreName(const std::string& hLustreName)
    {
        this->hLustreName = hLustreName;

        mmLayerEmulatorPadRWidget* u = NULL;
        struct mmRbNode* n = NULL;
        struct mmRbtreeStringVptIterator* it = NULL;
        //
        n = mmRb_First(&this->hEmuPadRbtree.rbt);
        while (NULL != n)
        {
            it = (struct mmRbtreeStringVptIterator*)mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
            n = mmRb_Next(n);
            // 
            u = (mmLayerEmulatorPadRWidget*)it->v;
            u->SetLustreName(this->hLustreName);
        }
    }
    void mmLayerEmulatorPadR::AssemblyPad(const char* pPadWindowName, mmWord_t hBit)
    {
        mmLayerEmulatorPadRWidget* u = this->GetAssemblyPadWindowInstance(pPadWindowName);
        u->SetPadBit(hBit);
    }
    mmLayerEmulatorPadRWidget* mmLayerEmulatorPadR::AddAssemblyPadWindow(const char* pPadWindowName)
    {
        struct mmString hWeakValue;
        mmString_MakeWeak(&hWeakValue, pPadWindowName);

        mmLayerEmulatorPadRWidget* u = NULL;
        struct mmRbtreeStringVptIterator* it = NULL;
        it = mmRbtreeStringVpt_GetIterator(&this->hEmuPadRbtree, &hWeakValue);
        if (NULL == it)
        {
            u = new mmLayerEmulatorPadRWidget;
            u->SetNormalName(this->hNormalName);
            u->SetLustreName(this->hLustreName);
            mmRbtreeStringVpt_Set(&this->hEmuPadRbtree, &hWeakValue, u);
        }
        else
        {
            u = (mmLayerEmulatorPadRWidget*)it->v;
        }

        return u;
    }
    mmLayerEmulatorPadRWidget* mmLayerEmulatorPadR::GetAssemblyPadWindow(const char* pPadWindowName)
    {
        struct mmString hWeakValue;
        mmString_MakeWeak(&hWeakValue, pPadWindowName);

        mmLayerEmulatorPadRWidget* u = NULL;
        struct mmRbtreeStringVptIterator* it = NULL;
        it = mmRbtreeStringVpt_GetIterator(&this->hEmuPadRbtree, &hWeakValue);
        if (NULL != it)
        {
            u = (mmLayerEmulatorPadRWidget*)it->v;
        }
        return u;
    }
    mmLayerEmulatorPadRWidget* mmLayerEmulatorPadR::GetAssemblyPadWindowInstance(const char* pPadWindowName)
    {
        mmLayerEmulatorPadRWidget* u = this->GetAssemblyPadWindow(pPadWindowName);
        if (NULL == u)
        {
            u = this->AddAssemblyPadWindow(pPadWindowName);
        }
        return u;
    }
    void mmLayerEmulatorPadR::RmvAssemblyPadWindow(const char* pPadWindowName)
    {
        struct mmString hWeakValue;
        mmString_MakeWeak(&hWeakValue, pPadWindowName);

        mmLayerEmulatorPadRWidget* u = NULL;
        struct mmRbtreeStringVptIterator* it = NULL;
        it = mmRbtreeStringVpt_GetIterator(&this->hEmuPadRbtree, &hWeakValue);
        if (NULL != it)
        {
            u = (mmLayerEmulatorPadRWidget*)it->v;
            mmRbtreeStringVpt_Erase(&this->hEmuPadRbtree, it);
            delete u;
        }
    }
    void mmLayerEmulatorPadR::ClearAssemblyPadWindow(void)
    {
        mmLayerEmulatorPadRWidget* u = NULL;
        struct mmRbNode* n = NULL;
        struct mmRbtreeStringVptIterator* it = NULL;
        //
        n = mmRb_First(&this->hEmuPadRbtree.rbt);
        while (NULL != n)
        {
            it = (struct mmRbtreeStringVptIterator*)mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
            n = mmRb_Next(n);
            // 
            u = (mmLayerEmulatorPadRWidget*)it->v;
            mmRbtreeStringVpt_Erase(&this->hEmuPadRbtree, it);
            delete u;
        }
    }
    void mmLayerEmulatorPadR::UpdateTransform(void)
    {
        mmLayerUtilityTransform_UpdateTransform(&this->hEmulatorTransform, this);
    }
    void mmLayerEmulatorPadR::OnFinishLaunching(void)
    {
        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        this->LayerEmulatorPadR = pWindowManager->loadLayoutFromFile("emulator/LayerEmulatorPadR.layout");
        this->addChild(this->LayerEmulatorPadR);
        //
        // Manual set whole area.
        CEGUI::URect hWholeArea(CEGUI::UDim(0, 0), CEGUI::UDim(0, 0), CEGUI::UDim(1, 0), CEGUI::UDim(1, 0));
        this->LayerEmulatorPadR->setArea(hWholeArea);
        //
        this->StaticImageBackground = this->LayerEmulatorPadR->getChild("StaticImageBackground");

        this->UpdateTransform();

        this->SetNormalName("StaticImageNormal");
        this->SetLustreName("StaticImageLustre");
        //
        this->AssemblyPad("DefaultWindowA" , 1 << MM_EMU_PAD_JOYPAD_A      );
        this->AssemblyPad("DefaultWindowB" , 1 << MM_EMU_PAD_JOYPAD_B      );
        this->AssemblyPad("DefaultWindowRA", 1 << MM_EMU_PAD_JOYPAD_A_RAPID);
        this->AssemblyPad("DefaultWindowRB", 1 << MM_EMU_PAD_JOYPAD_B_RAPID);

        this->FinishLaunchingWidget();
    }
    void mmLayerEmulatorPadR::OnBeforeTerminate(void)
    {
        this->BeforeTerminateWidget();

        this->ClearAssemblyPadWindow();

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        this->removeChild(this->LayerEmulatorPadR);
        pWindowManager->destroyWindow(this->LayerEmulatorPadR);
        this->LayerEmulatorPadR = NULL;
    }
    void mmLayerEmulatorPadR::OnEventWindowSizeChanged(struct mmSurfaceContentSizeChange* pContent)
    {
        this->UpdateTransform();

        mmLayerEmulatorPadRWidget* u = NULL;
        struct mmRbNode* n = NULL;
        struct mmRbtreeStringVptIterator* it = NULL;
        //
        n = mmRb_First(&this->hEmuPadRbtree.rbt);
        while (NULL != n)
        {
            it = (struct mmRbtreeStringVptIterator*)mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
            n = mmRb_Next(n);
            // 
            u = (mmLayerEmulatorPadRWidget*)it->v;
            u->OnEventWindowSizeChanged(pContent);
        }
    }
    void mmLayerEmulatorPadR::OnEventTouchsMoved(struct mmSurfaceContentTouchs* pContent)
    {
        mmLayerEmulatorPadRWidget* u = NULL;
        struct mmRbNode* n = NULL;
        struct mmRbtreeStringVptIterator* it = NULL;
        //
        n = mmRb_First(&this->hEmuPadRbtree.rbt);
        while (NULL != n)
        {
            it = (struct mmRbtreeStringVptIterator*)mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
            n = mmRb_Next(n);
            // 
            u = (mmLayerEmulatorPadRWidget*)it->v;
            u->OnEventTouchsMoved(pContent);
        }
    }
    void mmLayerEmulatorPadR::OnEventTouchsBegan(struct mmSurfaceContentTouchs* pContent)
    {
        mmLayerEmulatorPadRWidget* u = NULL;
        struct mmRbNode* n = NULL;
        struct mmRbtreeStringVptIterator* it = NULL;
        //
        n = mmRb_First(&this->hEmuPadRbtree.rbt);
        while (NULL != n)
        {
            it = (struct mmRbtreeStringVptIterator*)mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
            n = mmRb_Next(n);
            // 
            u = (mmLayerEmulatorPadRWidget*)it->v;
            u->OnEventTouchsBegan(pContent);
        }
    }
    void mmLayerEmulatorPadR::OnEventTouchsEnded(struct mmSurfaceContentTouchs* pContent)
    {
        mmLayerEmulatorPadRWidget* u = NULL;
        struct mmRbNode* n = NULL;
        struct mmRbtreeStringVptIterator* it = NULL;
        //
        n = mmRb_First(&this->hEmuPadRbtree.rbt);
        while (NULL != n)
        {
            it = (struct mmRbtreeStringVptIterator*)mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
            n = mmRb_Next(n);
            // 
            u = (mmLayerEmulatorPadRWidget*)it->v;
            u->OnEventTouchsEnded(pContent);
        }
    }
    void mmLayerEmulatorPadR::OnEventTouchsBreak(struct mmSurfaceContentTouchs* pContent)
    {
        mmLayerEmulatorPadRWidget* u = NULL;
        struct mmRbNode* n = NULL;
        struct mmRbtreeStringVptIterator* it = NULL;
        //
        n = mmRb_First(&this->hEmuPadRbtree.rbt);
        while (NULL != n)
        {
            it = (struct mmRbtreeStringVptIterator*)mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
            n = mmRb_Next(n);
            // 
            u = (mmLayerEmulatorPadRWidget*)it->v;
            u->OnEventTouchsBreak(pContent);
        }
    }
    void mmLayerEmulatorPadR::FinishLaunchingWidget(void)
    {
        CEGUI::Window* pPadWindow = NULL;

        mmLayerEmulatorPadRWidget* u = NULL;
        struct mmRbNode* n = NULL;
        struct mmRbtreeStringVptIterator* it = NULL;
        //
        n = mmRb_First(&this->hEmuPadRbtree.rbt);
        while (NULL != n)
        {
            it = (struct mmRbtreeStringVptIterator*)mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
            n = mmRb_Next(n);
            // 
            u = (mmLayerEmulatorPadRWidget*)it->v;

            pPadWindow = this->StaticImageBackground->getChild(mmString_CStr(&it->k));

            u->SetLayerContext(this);
            u->SetFingerTouch(this->pFingerTouch);
            u->SetPadWindow(pPadWindow);
            u->SetEmulatorMachine(this->pEmulatorMachine);
            u->OnFinishLaunching();
        }
    }
    void mmLayerEmulatorPadR::BeforeTerminateWidget(void)
    {
        mmLayerEmulatorPadRWidget* u = NULL;
        struct mmRbNode* n = NULL;
        struct mmRbtreeStringVptIterator* it = NULL;
        //
        n = mmRb_First(&this->hEmuPadRbtree.rbt);
        while (NULL != n)
        {
            it = (struct mmRbtreeStringVptIterator*)mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
            n = mmRb_Next(n);
            // 
            u = (mmLayerEmulatorPadRWidget*)it->v;

            u->OnBeforeTerminate();
        }
    }
}
