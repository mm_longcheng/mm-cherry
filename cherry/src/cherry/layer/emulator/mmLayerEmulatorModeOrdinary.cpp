/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerEmulatorModeOrdinary.h"
#include "mmLayerEmulatorPadL.h"
#include "mmLayerEmulatorPadR.h"

#include "nwsi/mmEventSurface.h"

#include "CEGUI/WindowManager.h"

#include "layer/director/mmLayerDirector.h"

#include "emulator/mmEmulatorMachine.h"

namespace mm
{
    const CEGUI::String mmLayerEmulatorModeOrdinary::EventNamespace = "mm";
    const CEGUI::String mmLayerEmulatorModeOrdinary::WidgetTypeName = "mm/mmLayerEmulatorModeOrdinary";

    mmLayerEmulatorModeOrdinary::mmLayerEmulatorModeOrdinary(const CEGUI::String& type, const CEGUI::String& name)
        : mmLayerEmulatorModeBase(type, name)
        , LayerEmulatorModeOrdinary(NULL)
        , StaticImageBackground(NULL)

        , StaticImageScreen(NULL)

        , DefaultWindowBottomArea(NULL)
        , ButtonControlL(NULL)
        , ButtonControlR(NULL)

        , LayerEmulatorPadL(NULL)
        , LayerEmulatorPadR(NULL)

        , hEmulatorPadL(NULL)
        , hEmulatorPadR(NULL)
    {

    }
    mmLayerEmulatorModeOrdinary::~mmLayerEmulatorModeOrdinary()
    {
        
    }
    void mmLayerEmulatorModeOrdinary::OnFinishLaunching()
    {
        struct mmSurfaceMaster* pSurfaceMaster = this->pSurfaceMaster;

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        this->LayerEmulatorModeOrdinary = pWindowManager->loadLayoutFromFile("emulator/LayerEmulatorModeOrdinary.layout");
        this->addChild(this->LayerEmulatorModeOrdinary);

        this->StaticImageBackground = this->LayerEmulatorModeOrdinary->getChild("StaticImageBackground");

        this->DefaultWindowBottomArea = this->StaticImageBackground->getChild("DefaultWindowBottomArea");
        this->ButtonControlL = this->DefaultWindowBottomArea->getChild("ButtonControlL");
        this->ButtonControlR = this->DefaultWindowBottomArea->getChild("ButtonControlR");

        this->StaticImageScreen = this->StaticImageBackground->getChild("StaticImageScreen");

        this->LayerEmulatorPadL = this->StaticImageBackground->getChild("LayerEmulatorPadL");
        this->LayerEmulatorPadR = this->StaticImageBackground->getChild("LayerEmulatorPadR");

        this->hEmulatorPadL = (mmLayerEmulatorPadL*)this->LayerEmulatorPadL;
        this->hEmulatorPadR = (mmLayerEmulatorPadR*)this->LayerEmulatorPadR;

        this->ButtonControlL->subscribeEvent(CEGUI::Window::EventMouseButtonDown, CEGUI::Event::Subscriber(&mmLayerEmulatorModeOrdinary::OnButtonControlLEventMouseButtonDown, this));
        this->ButtonControlL->subscribeEvent(CEGUI::Window::EventMouseButtonUp, CEGUI::Event::Subscriber(&mmLayerEmulatorModeOrdinary::OnButtonControlLEventMouseButtonUp, this));
        this->ButtonControlR->subscribeEvent(CEGUI::Window::EventMouseButtonDown, CEGUI::Event::Subscriber(&mmLayerEmulatorModeOrdinary::OnButtonControlREventMouseButtonDown, this));
        this->ButtonControlR->subscribeEvent(CEGUI::Window::EventMouseButtonUp, CEGUI::Event::Subscriber(&mmLayerEmulatorModeOrdinary::OnButtonControlREventMouseButtonUp, this));

        this->hLayerEmulatorWindow.SetEmulatorMachine(this->pEmulatorMachine);
        this->hLayerEmulatorWindow.SetLayerContext(this);
        this->hLayerEmulatorWindow.SetWindowScreen(this->StaticImageScreen);
        this->hLayerEmulatorWindow.SetFingerIsVisible(true);
        this->hLayerEmulatorWindow.OnFinishLaunching();

        this->hEmulatorPadL->SetDirector(this->pDirector);
        this->hEmulatorPadL->SetContextMaster(this->pContextMaster);
        this->hEmulatorPadL->SetSurfaceMaster(this->pSurfaceMaster);
        this->hEmulatorPadL->SetFingerTouch(&this->hLayerEmulatorWindow.hFingerTouch);
        this->hEmulatorPadL->SetEmulatorMachine(this->pEmulatorMachine);
        this->hEmulatorPadL->OnFinishLaunching();

        this->hEmulatorPadR->SetDirector(this->pDirector);
        this->hEmulatorPadR->SetContextMaster(this->pContextMaster);
        this->hEmulatorPadR->SetSurfaceMaster(this->pSurfaceMaster);
        this->hEmulatorPadR->SetFingerTouch(&this->hLayerEmulatorWindow.hFingerTouch);
        this->hEmulatorPadR->SetEmulatorMachine(this->pEmulatorMachine);
        this->hEmulatorPadR->OnFinishLaunching();

        pSurfaceMaster->hEventSet.SubscribeEvent(mmSurfaceMaster_EventWindowSizeChanged, &mmLayerEmulatorModeOrdinary::OnEventWindowSizeChanged, this);

        this->hLayerEventTouchsMovedConn = this->subscribeEvent(mmLayerContext::EventTouchsMoved, CEGUI::Event::Subscriber(&mmLayerEmulatorModeOrdinary::OnLayerEventTouchsMoved, this));
        this->hLayerEventTouchsBeganConn = this->subscribeEvent(mmLayerContext::EventTouchsBegan, CEGUI::Event::Subscriber(&mmLayerEmulatorModeOrdinary::OnLayerEventTouchsBegan, this));
        this->hLayerEventTouchsEndedConn = this->subscribeEvent(mmLayerContext::EventTouchsEnded, CEGUI::Event::Subscriber(&mmLayerEmulatorModeOrdinary::OnLayerEventTouchsEnded, this));
        this->hLayerEventTouchsBreakConn = this->subscribeEvent(mmLayerContext::EventTouchsBreak, CEGUI::Event::Subscriber(&mmLayerEmulatorModeOrdinary::OnLayerEventTouchsBreak, this));
    }

    void mmLayerEmulatorModeOrdinary::OnBeforeTerminate()
    {
        struct mmSurfaceMaster* pSurfaceMaster = this->pSurfaceMaster;

        this->hLayerEventTouchsMovedConn->disconnect();
        this->hLayerEventTouchsBeganConn->disconnect();
        this->hLayerEventTouchsEndedConn->disconnect();
        this->hLayerEventTouchsBreakConn->disconnect();

        pSurfaceMaster->hEventSet.UnsubscribeEvent(mmSurfaceMaster_EventWindowSizeChanged, &mmLayerEmulatorModeOrdinary::OnEventWindowSizeChanged, this);

        this->hEmulatorPadL->OnBeforeTerminate();
        this->hEmulatorPadR->OnBeforeTerminate();

        this->hLayerEmulatorWindow.OnBeforeTerminate();

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        pWindowManager->destroyWindow(this->LayerEmulatorModeOrdinary);
        this->LayerEmulatorModeOrdinary = NULL;
    }
    void mmLayerEmulatorModeOrdinary::notifyScreenAreaChanged(bool recursive/*  = true */)
    {
        mmLayerContext::notifyScreenAreaChanged(recursive);
        this->hLayerEmulatorWindow.UpdateTransform();
    }

    bool mmLayerEmulatorModeOrdinary::OnEventWindowSizeChanged(const mmEventArgs& args)
    {
        const struct mmEventSizeChange& evt = (const struct mmEventSizeChange&)(args);

        this->hEmulatorPadL->OnEventWindowSizeChanged(evt.content);
        this->hEmulatorPadR->OnEventWindowSizeChanged(evt.content);
        return true;
    }
    bool mmLayerEmulatorModeOrdinary::OnLayerEventTouchsMoved(const CEGUI::EventArgs& args)
    {
        const mmTouchsEventArgs& evt = (const mmTouchsEventArgs&)(args);

        this->hEmulatorPadL->OnEventTouchsMoved(evt.content);
        this->hEmulatorPadR->OnEventTouchsMoved(evt.content);
        return true;
    }
    bool mmLayerEmulatorModeOrdinary::OnLayerEventTouchsBegan(const CEGUI::EventArgs& args)
    {
        const mmTouchsEventArgs& evt = (const mmTouchsEventArgs&)(args);

        this->hEmulatorPadL->OnEventTouchsBegan(evt.content);
        this->hEmulatorPadR->OnEventTouchsBegan(evt.content);
        return true;
    }
    bool mmLayerEmulatorModeOrdinary::OnLayerEventTouchsEnded(const CEGUI::EventArgs& args)
    {
        const mmTouchsEventArgs& evt = (const mmTouchsEventArgs&)(args);

        this->hEmulatorPadL->OnEventTouchsEnded(evt.content);
        this->hEmulatorPadR->OnEventTouchsEnded(evt.content);
        return true;
    }
    bool mmLayerEmulatorModeOrdinary::OnLayerEventTouchsBreak(const CEGUI::EventArgs& args)
    {
        const mmTouchsEventArgs& evt = (const mmTouchsEventArgs&)(args);

        this->hEmulatorPadL->OnEventTouchsBreak(evt.content);
        this->hEmulatorPadR->OnEventTouchsBreak(evt.content);
        return true;
    }
    bool mmLayerEmulatorModeOrdinary::OnButtonControlLEventMouseButtonDown(const CEGUI::EventArgs& args)
    {
        mmEmulatorMachine_JoypadBitPressed(this->pEmulatorMachine, 0, MM_EMU_PAD_JOYPAD_CONTROL_L);
        return true;
    }
    bool mmLayerEmulatorModeOrdinary::OnButtonControlLEventMouseButtonUp(const CEGUI::EventArgs& args)
    {
        mmEmulatorMachine_JoypadBitRelease(this->pEmulatorMachine, 0, MM_EMU_PAD_JOYPAD_CONTROL_L);
        return true;
    }
    bool mmLayerEmulatorModeOrdinary::OnButtonControlREventMouseButtonDown(const CEGUI::EventArgs& args)
    {
        mmEmulatorMachine_JoypadBitPressed(this->pEmulatorMachine, 0, MM_EMU_PAD_JOYPAD_CONTROL_R);
        return true;
    }
    bool mmLayerEmulatorModeOrdinary::OnButtonControlREventMouseButtonUp(const CEGUI::EventArgs& args)
    {
        mmEmulatorMachine_JoypadBitRelease(this->pEmulatorMachine, 0, MM_EMU_PAD_JOYPAD_CONTROL_R);
        return true;
    }
}
