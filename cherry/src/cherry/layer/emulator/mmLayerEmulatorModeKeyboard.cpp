/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerEmulatorModeKeyboard.h"

#include "CEGUI/WindowManager.h"

#include "layer/director/mmLayerDirector.h"

#include "layer/keyboard/mmLayerKeyboardComplete.h"
#include "layer/keyboard/mmLayerKeyboardItemWidget.h"

#include "emulator/mmEmulatorMachine.h"

namespace mm
{
    const CEGUI::String mmLayerEmulatorModeKeyboard::EventNamespace = "mm";
    const CEGUI::String mmLayerEmulatorModeKeyboard::WidgetTypeName = "mm/mmLayerEmulatorModeKeyboard";

    mmLayerEmulatorModeKeyboard::mmLayerEmulatorModeKeyboard(const CEGUI::String& type, const CEGUI::String& name)
        : mmLayerEmulatorModeBase(type, name)
        , LayerEmulatorModeKeyboard(NULL)
        , StaticImageBackground(NULL)

        , StaticImageScreen(NULL)

        , LayerKeyboardComplete(NULL)

        , pKeyboardComplete(NULL)
    {

    }
    mmLayerEmulatorModeKeyboard::~mmLayerEmulatorModeKeyboard(void)
    {
        
    }

    void mmLayerEmulatorModeKeyboard::OnFinishLaunching(void)
    {
        struct mmSurfaceMaster* pSurfaceMaster = this->pSurfaceMaster;

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        this->LayerEmulatorModeKeyboard = pWindowManager->loadLayoutFromFile("emulator/LayerEmulatorModeKeyboard.layout");
        this->addChild(this->LayerEmulatorModeKeyboard);

        this->StaticImageBackground = this->LayerEmulatorModeKeyboard->getChild("StaticImageBackground");

        this->StaticImageScreen = this->StaticImageBackground->getChild("StaticImageScreen");

        this->LayerKeyboardComplete = this->StaticImageBackground->getChild("LayerKeyboardComplete");

        this->pKeyboardComplete = (mmLayerKeyboardComplete*)(this->LayerKeyboardComplete);

        this->pKeyboardComplete->SetDirector(this->pDirector);
        this->pKeyboardComplete->SetContextMaster(this->pContextMaster);
        this->pKeyboardComplete->SetSurfaceMaster(this->pSurfaceMaster);
        this->pKeyboardComplete->OnFinishLaunching();

        // spacing common CEGUI::Window object, we need manual attach context event.
        this->AttachContextEvent(this->pKeyboardComplete);

        this->hLayerEmulatorWindow.SetEmulatorMachine(this->pEmulatorMachine);
        this->hLayerEmulatorWindow.SetLayerContext(this);
        this->hLayerEmulatorWindow.SetWindowScreen(this->StaticImageScreen);
        this->hLayerEmulatorWindow.SetFingerIsVisible(true);
        this->hLayerEmulatorWindow.OnFinishLaunching();

        this->pKeyboardComplete->hEventSet.SubscribeEvent(mmLayerKeyboardComplete::EventKeyboardItemRelease, &mmLayerEmulatorModeKeyboard::OnEventKeyboardItemRelease, this);
        this->pKeyboardComplete->hEventSet.SubscribeEvent(mmLayerKeyboardComplete::EventKeyboardItemPressed, &mmLayerEmulatorModeKeyboard::OnEventKeyboardItemPressed, this);
    }

    void mmLayerEmulatorModeKeyboard::OnBeforeTerminate(void)
    {
        struct mmSurfaceMaster* pSurfaceMaster = this->pSurfaceMaster;

        this->pKeyboardComplete->hEventSet.UnsubscribeEvent(mmLayerKeyboardComplete::EventKeyboardItemRelease, &mmLayerEmulatorModeKeyboard::OnEventKeyboardItemRelease, this);
        this->pKeyboardComplete->hEventSet.UnsubscribeEvent(mmLayerKeyboardComplete::EventKeyboardItemPressed, &mmLayerEmulatorModeKeyboard::OnEventKeyboardItemPressed, this);

        this->hLayerEmulatorWindow.OnBeforeTerminate();

        // spacing common CEGUI::Window object, we need manual attach context event.
        this->DetachContextEvent(this->pKeyboardComplete);

        this->pKeyboardComplete->OnBeforeTerminate();

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        pWindowManager->destroyWindow(this->LayerEmulatorModeKeyboard);
        this->LayerEmulatorModeKeyboard = NULL;
    }
    void mmLayerEmulatorModeKeyboard::notifyScreenAreaChanged(bool recursive/*  = true */)
    {
        mmLayerContext::notifyScreenAreaChanged(recursive);
        this->hLayerEmulatorWindow.UpdateTransform();
    }

    bool mmLayerEmulatorModeKeyboard::OnEventKeyboardItemRelease(const mmEventArgs& args)
    {
        const mmLayerKeyboardItemEventArgs& evt = (const mmLayerKeyboardItemEventArgs&)(args);
        mmEmulatorMachine_KeyboardRelease(this->pEmulatorMachine, evt.keycode);
        return true;
    }
    bool mmLayerEmulatorModeKeyboard::OnEventKeyboardItemPressed(const mmEventArgs& args)
    {
        const mmLayerKeyboardItemEventArgs& evt = (const mmLayerKeyboardItemEventArgs&)(args);
        mmEmulatorMachine_KeyboardPressed(this->pEmulatorMachine, evt.keycode);
        return true;
    }
}
