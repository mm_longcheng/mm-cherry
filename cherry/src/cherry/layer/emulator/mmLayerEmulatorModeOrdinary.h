/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmLayerEmulatorModeOrdinary_h__
#define __mmLayerEmulatorModeOrdinary_h__

#include "core/mmCore.h"

#include "dish/mmEvent.h"

#include "layer/director/mmLayerContext.h"

#include "CEGUI/Window.h"

#include "mmLayerEmulatorWindow.h"
#include "mmLayerEmulatorModeBase.h"

namespace mm
{
    class mmLayerEmulatorPadL;
    class mmLayerEmulatorPadR;

    class mmLayerEmulatorModeOrdinary : public mmLayerEmulatorModeBase
    {
    public:
        static const CEGUI::String EventNamespace;          //!< Namespace for global events
        static const CEGUI::String WidgetTypeName;          //!< Window factory name
    public:
        CEGUI::Window* LayerEmulatorModeOrdinary;
        CEGUI::Window* StaticImageBackground;

        CEGUI::Window* StaticImageScreen;

        CEGUI::Window* DefaultWindowBottomArea;
        CEGUI::Window* ButtonControlL;
        CEGUI::Window* ButtonControlR;

        CEGUI::Window* LayerEmulatorPadL;
        CEGUI::Window* LayerEmulatorPadR;

        mmLayerEmulatorPadL* hEmulatorPadL;
        mmLayerEmulatorPadR* hEmulatorPadR;
    public:
        mmLayerEmulatorWindow hLayerEmulatorWindow;
    public:
        CEGUI::Event::Connection hLayerEventTouchsMovedConn;
        CEGUI::Event::Connection hLayerEventTouchsBeganConn;
        CEGUI::Event::Connection hLayerEventTouchsEndedConn;
        CEGUI::Event::Connection hLayerEventTouchsBreakConn;
    public:
        mmLayerEmulatorModeOrdinary(const CEGUI::String& type, const CEGUI::String& name);
        virtual ~mmLayerEmulatorModeOrdinary(void);
    public:
        // mmLayerContext
        virtual void OnFinishLaunching(void);
        virtual void OnBeforeTerminate(void);
    public:
        // CEGUI::Window
        virtual void notifyScreenAreaChanged(bool recursive = true);
    private:
        bool OnEventWindowSizeChanged(const mmEventArgs& args);
    private:
        bool OnLayerEventTouchsMoved(const CEGUI::EventArgs& args);
        bool OnLayerEventTouchsBegan(const CEGUI::EventArgs& args);
        bool OnLayerEventTouchsEnded(const CEGUI::EventArgs& args);
        bool OnLayerEventTouchsBreak(const CEGUI::EventArgs& args);
    private:
        bool OnButtonControlLEventMouseButtonDown(const CEGUI::EventArgs& args);
        bool OnButtonControlLEventMouseButtonUp(const CEGUI::EventArgs& args);
        bool OnButtonControlREventMouseButtonDown(const CEGUI::EventArgs& args);
        bool OnButtonControlREventMouseButtonUp(const CEGUI::EventArgs& args);
    };
}

#endif//__mmLayerEmulatorModeOrdinary_h__