/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmLayerEmulatorPadZapper_h__
#define __mmLayerEmulatorPadZapper_h__

#include "container/mmRbtreeU64.h"

#include "CEGUI/Window.h"

#include "layer/utility/mmLayerUtilityTransform.h"

struct mmEmulatorMachine;

namespace mm
{
    class mmLayerUtilityFinger;
    class mmLayerUtilityFingerTouch;

    class mmLayerEmulatorPadZapper
    {
    public:
        mmLayerUtilityFingerTouch* pFingerTouch;
    public:
        CEGUI::Window* pWindowScreen;
    public:
        struct mmEmulatorMachine* pEmulatorMachine;
    public:
        struct mmLayerUtilityTransform hEmulatorTransform;
    public:
        double hRotation;
    public:
        // window => world pixel => emulator pixel screen.
        Ogre::Matrix4 Matrix4x4WorldPixelToEmulatorPixelScreen;

        mmLong_t hLx;
        mmLong_t hLy;
        int hButtonMask;
    public:
        struct mmRbtreeU64Vpt hRbtreeTouchs;
    public:
        mmLayerEmulatorPadZapper(void);
        ~mmLayerEmulatorPadZapper(void);
    public:
        void SetFingerTouch(mmLayerUtilityFingerTouch* pFingerTouch);
    public:
        void SetWindowScreen(CEGUI::Window* pWindowScreen);
        void SetEmulatorMachine(struct mmEmulatorMachine* pEmulatorMachine);
    public:
        void UpdateTransform(void);
    public:
        void OnFinishLaunching(void);
        void OnBeforeTerminate(void);
    public:
        void OnEventWindowSizeChanged(struct mmSurfaceContentSizeChange* pContent);
    public:
        void OnEventTouchsMoved(struct mmSurfaceContentTouchs* pContent);
        void OnEventTouchsBegan(struct mmSurfaceContentTouchs* pContent);
        void OnEventTouchsEnded(struct mmSurfaceContentTouchs* pContent);
        void OnEventTouchsBreak(struct mmSurfaceContentTouchs* pContent);
    private:
        void AddTouchsFinger(struct mmSurfaceContentTouchs* pContent);
        void RmvTouchsFinger(struct mmSurfaceContentTouchs* pContent);
        void MovTouchsFinger(struct mmSurfaceContentTouchs* pContent);
    private:
        void AddTouchsFingerCache(mmLayerUtilityFinger* pFinger);
        void RmvTouchsFingerCache(uintptr_t hMotionId);
        void RmvTouchsFingerCache(struct mmRbtreeU64VptIterator* it);
    private:
        void UpdateTouchsFinger(void);
        void UpdateTouchsFingerZapper(mmLayerUtilityFinger* pFinger);
    };
}

#endif//__mmLayerEmulatorPadZapper_h__

