/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerEmulatorPadZapper.h"
#include "mmLayerEmulatorTransform.h"

#include "layer/utility/mmLayerUtilityFinger.h"

#include "emulator/mmEmulatorMachine.h"

typedef void(*__static_MachineMouseFunc)(struct mmEmulatorMachine* p, mmLong_t x, mmLong_t y, int hButtonMask);
// mmSurfaceContentTouchPhaseType => __static_MachineMouseFunc.
static const __static_MachineMouseFunc __static_MachineMouseFuncTable[4] =
{
    &mmEmulatorMachine_MouseBegan,
    &mmEmulatorMachine_MouseMoved,
    &mmEmulatorMachine_MouseEnded,
    &mmEmulatorMachine_MouseEnded,
};

namespace mm
{
    mmLayerEmulatorPadZapper::mmLayerEmulatorPadZapper(void)
        : pFingerTouch(NULL)

        , pWindowScreen(NULL)
        , pEmulatorMachine(NULL)

        , hRotation(0)

        , hLx(0)
        , hLy(0)
        , hButtonMask(0)
    {
        mmRbtreeU64Vpt_Init(&this->hRbtreeTouchs);
        mmLayerUtilityTransform_Init(&this->hEmulatorTransform);
    }
    mmLayerEmulatorPadZapper::~mmLayerEmulatorPadZapper()
    {
        mmLayerUtilityTransform_Destroy(&this->hEmulatorTransform);
        mmRbtreeU64Vpt_Destroy(&this->hRbtreeTouchs);
    }
    void mmLayerEmulatorPadZapper::SetFingerTouch(mmLayerUtilityFingerTouch* pFingerTouch)
    {
        this->pFingerTouch = pFingerTouch;
    }
    void mmLayerEmulatorPadZapper::SetWindowScreen(CEGUI::Window* pWindowScreen)
    {
        this->pWindowScreen = pWindowScreen;
    }
    void mmLayerEmulatorPadZapper::SetEmulatorMachine(struct mmEmulatorMachine* pEmulatorMachine)
    {
        this->pEmulatorMachine = pEmulatorMachine;
    }
    void mmLayerEmulatorPadZapper::UpdateTransform(void)
    {
        mmLayerUtilityTransform_UpdateTransform(&this->hEmulatorTransform, this->pWindowScreen);

        // Matrix4x4WorldPixelToWindowPixel
        const Ogre::Matrix4& hMat0 = this->hEmulatorTransform.Matrix4x4WorldPixelToWindowPixel;
        // Matrix4x4WindowPixelToWindowScale
        Ogre::Matrix4 hMat1 = Ogre::Matrix4::IDENTITY;
        // WindowScaleToEmulatorPixelScreen
        Ogre::Matrix4 hMat2 = Ogre::Matrix4::IDENTITY;

        mmWidgetTransform_Matrix4x4WindowPixelToWindowScale(this->pWindowScreen, &hMat1);
        mmLayerEmulatorTransform_Matrix4x4WindowScaleToEmulatorPixelScreen(&hMat2);

        this->Matrix4x4WorldPixelToEmulatorPixelScreen = hMat2 * hMat1 * hMat0;
    }
    void mmLayerEmulatorPadZapper::OnFinishLaunching(void)
    {
        this->UpdateTransform();
    }
    void mmLayerEmulatorPadZapper::OnBeforeTerminate(void)
    {
        mmRbtreeU64Vpt_Clear(&this->hRbtreeTouchs);
    }
    void mmLayerEmulatorPadZapper::OnEventWindowSizeChanged(struct mmSurfaceContentSizeChange* pContent)
    {
        this->UpdateTransform();
    }
    void mmLayerEmulatorPadZapper::OnEventTouchsMoved(struct mmSurfaceContentTouchs* pContent)
    {
        this->AddTouchsFinger(pContent);
        this->MovTouchsFinger(pContent);
        this->UpdateTouchsFinger();
    }
    void mmLayerEmulatorPadZapper::OnEventTouchsBegan(struct mmSurfaceContentTouchs* pContent)
    {
        this->AddTouchsFinger(pContent);
        this->UpdateTouchsFinger();
    }
    void mmLayerEmulatorPadZapper::OnEventTouchsEnded(struct mmSurfaceContentTouchs* pContent)
    {
        this->RmvTouchsFinger(pContent);
        this->UpdateTouchsFinger();
    }
    void mmLayerEmulatorPadZapper::OnEventTouchsBreak(struct mmSurfaceContentTouchs* pContent)
    {
        this->RmvTouchsFinger(pContent);
        this->UpdateTouchsFinger();
    }
    void mmLayerEmulatorPadZapper::AddTouchsFinger(struct mmSurfaceContentTouchs* pContent)
    {
        struct mmRbtreeU64Vpt* pTouchs = &this->pFingerTouch->hRbtreeTouchs;

        // world position.
        Ogre::Vector3 hTouchPosition;
        Ogre::Vector3 hCenterToTouch;
        double dx = 0;
        double dy = 0;

        mmLayerUtilityFinger* u = NULL;

        struct mmRbNode* n = NULL;
        struct mmRbtreeU32VptIterator* it = NULL;
        //
        n = mmRb_First(&pTouchs->rbt);
        while (NULL != n)
        {
            it = (struct mmRbtreeU32VptIterator*)mmRb_Entry(n, struct mmRbtreeU32VptIterator, n);
            n = mmRb_Next(n);
            // 
            u = (mmLayerUtilityFinger*)it->v;

            hTouchPosition.x = (float)u->hAbsXW;
            hTouchPosition.y = (float)u->hAbsYW;
            hTouchPosition.z = (float)0.0f;

            hCenterToTouch = hTouchPosition - this->hEmulatorTransform.hWorldWindowCenter;

            if (fabs(hCenterToTouch.x) < this->hEmulatorTransform.hWorldWindowA &&
                fabs(hCenterToTouch.y) < this->hEmulatorTransform.hWorldWindowB)
            {
                double dx = hCenterToTouch.x;
                double dy = hCenterToTouch.y;

                this->hRotation = atan2(dy, dx);

                this->AddTouchsFingerCache(u);
            }
        }
    }
    void mmLayerEmulatorPadZapper::RmvTouchsFinger(struct mmSurfaceContentTouchs* pContent)
    {
        size_t i = 0;
        struct mmSurfaceContentTouch* pTouchI = NULL;
        mmLayerUtilityFinger* pFinger = NULL;

        struct mmVectorValue* pVectorValue = &pContent->context;

        for (i = 0; i < pVectorValue->size; i++)
        {
            pTouchI = (struct mmSurfaceContentTouch*)mmVectorValue_GetIndexReference(pVectorValue, i);
            this->RmvTouchsFingerCache(pTouchI->motion_id);
        }
    }
    void mmLayerEmulatorPadZapper::MovTouchsFinger(struct mmSurfaceContentTouchs* pContent)
    {
        // world position.
        Ogre::Vector3 hTouchPosition;
        Ogre::Vector3 hCenterToTouch;
        double dx = 0;
        double dy = 0;

        mmLayerUtilityFinger* u = NULL;

        struct mmRbNode* n = NULL;
        struct mmRbtreeU64VptIterator* it = NULL;
        //
        n = mmRb_First(&this->hRbtreeTouchs.rbt);
        while (NULL != n)
        {
            it = (struct mmRbtreeU64VptIterator*)mmRb_Entry(n, struct mmRbtreeU64VptIterator, n);
            n = mmRb_Next(n);
            // 
            u = (mmLayerUtilityFinger*)it->v;
            //
            this->UpdateTouchsFingerZapper(u);

            hTouchPosition.x = (float)u->hAbsXW;
            hTouchPosition.y = (float)u->hAbsYW;
            hTouchPosition.z = (float)0.0f;

            hCenterToTouch = hTouchPosition - this->hEmulatorTransform.hWorldWindowCenter;

            if (fabs(hCenterToTouch.x) < this->hEmulatorTransform.hWorldWindowA &&
                fabs(hCenterToTouch.y) < this->hEmulatorTransform.hWorldWindowB)
            {
                dx = hCenterToTouch.x;
                dy = hCenterToTouch.y;

                this->hRotation = atan2(dy, dx);
            }
            else
            {
                this->RmvTouchsFingerCache(it);
                continue;
            }
        }
    }
    void mmLayerEmulatorPadZapper::AddTouchsFingerCache(mmLayerUtilityFinger* pFinger)
    {
        mmRbtreeU64Vpt_Set(&this->hRbtreeTouchs, pFinger->hMotionId, pFinger);
        this->UpdateTouchsFingerZapper(pFinger);
    }
    void mmLayerEmulatorPadZapper::RmvTouchsFingerCache(uintptr_t hMotionId)
    {
        mmLayerUtilityFinger* u = NULL;
        struct mmRbtreeU64VptIterator* it = NULL;

        it = mmRbtreeU64Vpt_GetIterator(&this->hRbtreeTouchs, hMotionId);
        if (NULL != it)
        {
            u = (mmLayerUtilityFinger*)it->v;
            this->UpdateTouchsFingerZapper(u);
            mmRbtreeU64Vpt_Erase(&this->hRbtreeTouchs, it);
        }
    }
    void mmLayerEmulatorPadZapper::RmvTouchsFingerCache(struct mmRbtreeU64VptIterator* it)
    {
        mmLayerUtilityFinger* u = NULL;
        if (NULL != it)
        {
            u = (mmLayerUtilityFinger*)it->v;
            this->UpdateTouchsFingerZapper(u);
            mmRbtreeU64Vpt_Erase(&this->hRbtreeTouchs, it);
        }
    }
    void mmLayerEmulatorPadZapper::UpdateTouchsFinger(void)
    {

    }
    void mmLayerEmulatorPadZapper::UpdateTouchsFingerZapper(mmLayerUtilityFinger* pFinger)
    {
        Ogre::Vector3 hWorldPosition((float)pFinger->hAbsXW, (float)pFinger->hAbsYW, 0.0f);
        Ogre::Vector3 hEmulatorPixelScreen;

        hEmulatorPixelScreen = this->Matrix4x4WorldPixelToEmulatorPixelScreen * hWorldPosition;

        this->hLx = (mmLong_t)hEmulatorPixelScreen.x;
        this->hLy = (mmLong_t)hEmulatorPixelScreen.y;
        this->hButtonMask = pFinger->hTouch.button_mask;

        __static_MachineMouseFunc hFunc = __static_MachineMouseFuncTable[pFinger->hTouch.phase & 0x03];

        (*hFunc)(this->pEmulatorMachine, this->hLx, this->hLy, this->hButtonMask);
    }
}
