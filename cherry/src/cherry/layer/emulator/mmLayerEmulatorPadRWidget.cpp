/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerEmulatorPadRWidget.h"
#include "mmLayerEmulatorTransform.h"

#include "layer/utility/mmLayerUtilityFinger.h"
#include "layer/utility/mmLayerUtilityIntersect.h"

#include "emulator/mmEmulatorMachine.h"

namespace mm
{
    mmLayerEmulatorPadRWidget::mmLayerEmulatorPadRWidget(void)
        : pLayerContext(NULL)

        , pFingerTouch(NULL)

        , pPadWindow(NULL)
        , pNormalWindow(NULL)
        , pLustreWindow(NULL)
        , pEmulatorMachine(NULL)

        , hRotation(0)

        , hNormalName("")
        , hLustreName("")
        , hBit(0)
    {
        mmRbtreeU64Vpt_Init(&this->hRbtreeTouchs);
        mmLayerUtilityTransform_Init(&this->hEmulatorTransform);
    }
    mmLayerEmulatorPadRWidget::~mmLayerEmulatorPadRWidget(void)
    {
        mmLayerUtilityTransform_Destroy(&this->hEmulatorTransform);
        mmRbtreeU64Vpt_Destroy(&this->hRbtreeTouchs);
    }
    void mmLayerEmulatorPadRWidget::SetLayerContext(mmLayerContext* pLayerContext)
    {
        this->pLayerContext = pLayerContext;
    }
    void mmLayerEmulatorPadRWidget::SetFingerTouch(mmLayerUtilityFingerTouch* pFingerTouch)
    {
        this->pFingerTouch = pFingerTouch;
    }
    void mmLayerEmulatorPadRWidget::SetPadWindow(CEGUI::Window* pPadWindow)
    {
        this->pPadWindow = pPadWindow;
    }
    void mmLayerEmulatorPadRWidget::SetEmulatorMachine(struct mmEmulatorMachine* pEmulatorMachine)
    {
        this->pEmulatorMachine = pEmulatorMachine;
    }
    void mmLayerEmulatorPadRWidget::SetNormalName(const std::string& hNormalName)
    {
        this->hNormalName = hNormalName;
    }
    void mmLayerEmulatorPadRWidget::SetLustreName(const std::string& hLustreName)
    {
        this->hLustreName = hLustreName;
    }
    void mmLayerEmulatorPadRWidget::SetPadBit(mmWord_t hBit)
    {
        this->hBit = hBit;
    }
    void mmLayerEmulatorPadRWidget::UpdateTransform(void)
    {
        mmLayerUtilityTransform_UpdateTransform(&this->hEmulatorTransform, this->pPadWindow);
    }
    void mmLayerEmulatorPadRWidget::OnFinishLaunching(void)
    {
        this->UpdateTransform();
        //
        this->pNormalWindow = this->pPadWindow->getChild(this->hNormalName);
        this->pLustreWindow = this->pPadWindow->getChild(this->hLustreName);
    }
    void mmLayerEmulatorPadRWidget::OnBeforeTerminate(void)
    {
        this->pNormalWindow = NULL;
        this->pLustreWindow = NULL;
        //
        mmRbtreeU64Vpt_Clear(&this->hRbtreeTouchs);
    }
    void mmLayerEmulatorPadRWidget::OnEventWindowSizeChanged(struct mmSurfaceContentSizeChange* pContent)
    {
        this->UpdateTransform();
    }
    void mmLayerEmulatorPadRWidget::OnEventTouchsMoved(struct mmSurfaceContentTouchs* pContent)
    {
        this->AddTouchsFinger(pContent);
        this->MovTouchsFinger(pContent);
        this->UpdateTouchsFinger();
    }
    void mmLayerEmulatorPadRWidget::OnEventTouchsBegan(struct mmSurfaceContentTouchs* pContent)
    {
        this->AddTouchsFinger(pContent);
        this->UpdateTouchsFinger();
    }
    void mmLayerEmulatorPadRWidget::OnEventTouchsEnded(struct mmSurfaceContentTouchs* pContent)
    {
        this->RmvTouchsFinger(pContent);
        this->UpdateTouchsFinger();
    }
    void mmLayerEmulatorPadRWidget::OnEventTouchsBreak(struct mmSurfaceContentTouchs* pContent)
    {
        this->RmvTouchsFinger(pContent);
        this->UpdateTouchsFinger();
    }
    void mmLayerEmulatorPadRWidget::AddTouchsFinger(struct mmSurfaceContentTouchs* pContent)
    {
        struct mmRbtreeU64Vpt* pTouchs = &this->pFingerTouch->hRbtreeTouchs;

        // world position.
        Ogre::Vector3 hCenterToTouch;

        mmLayerUtilityFinger* u = NULL;

        struct mmRbNode* n = NULL;
        struct mmRbtreeU64VptIterator* it = NULL;
        //
        n = mmRb_First(&pTouchs->rbt);
        while (NULL != n)
        {
            it = (struct mmRbtreeU64VptIterator*)mmRb_Entry(n, struct mmRbtreeU64VptIterator, n);
            n = mmRb_Next(n);
            // 
            u = (mmLayerUtilityFinger*)it->v;

            if (this->IntersectFinger(u, hCenterToTouch))
            {
                this->hRotation = (double)atan2((double)hCenterToTouch.y, (double)hCenterToTouch.x);

                mmRbtreeU64Vpt_Set(&this->hRbtreeTouchs, u->hMotionId, u);
            }
        }
    }
    void mmLayerEmulatorPadRWidget::RmvTouchsFinger(struct mmSurfaceContentTouchs* pContent)
    {
        size_t i = 0;

        struct mmSurfaceContentTouch* pTouchI = NULL;
        mmLayerUtilityFinger* pFinger = NULL;
        struct mmVectorValue* pVectorValue = &pContent->context;

        for (i = 0; i < pVectorValue->size; i++)
        {
            pTouchI = (struct mmSurfaceContentTouch*)mmVectorValue_GetIndexReference(pVectorValue, i);
            // remove.
            mmRbtreeU64Vpt_Rmv(&this->hRbtreeTouchs, pTouchI->motion_id);
        }
    }
    void mmLayerEmulatorPadRWidget::MovTouchsFinger(struct mmSurfaceContentTouchs* pContent)
    {
        // world position.
        Ogre::Vector3 hCenterToTouch;
        double dx = 0;
        double dy = 0;

        mmLayerUtilityFinger* u = NULL;

        struct mmRbNode* n = NULL;
        struct mmRbtreeU64VptIterator* it = NULL;
        //
        n = mmRb_First(&this->hRbtreeTouchs.rbt);
        while (NULL != n)
        {
            it = (struct mmRbtreeU64VptIterator*)mmRb_Entry(n, struct mmRbtreeU64VptIterator, n);
            n = mmRb_Next(n);
            // 
            u = (mmLayerUtilityFinger*)it->v;
            //
            if (this->IntersectFinger(u, hCenterToTouch))
            {
                this->hRotation = (double)atan2((double)hCenterToTouch.y, (double)hCenterToTouch.x);
            }
            else
            {
                // remove.
                mmRbtreeU64Vpt_Erase(&this->hRbtreeTouchs, it);
                continue;
            }
        }
    }
    void mmLayerEmulatorPadRWidget::UpdateTouchsFinger()
    {
        if (0 == this->hRbtreeTouchs.size)
        {
            // JoypadBitRelease.
            mmWord_t bit = mmEmulatorMachine_JoypadBitGetData(this->pEmulatorMachine, 0);
            bit &= ~this->hBit;
            mmEmulatorMachine_JoypadBitSetData(this->pEmulatorMachine, 0, bit);

            this->pNormalWindow->setVisible(true);
            this->pLustreWindow->setVisible(false);
        }
        else
        {
            // JoypadBitPressed.
            mmWord_t bit = mmEmulatorMachine_JoypadBitGetData(this->pEmulatorMachine, 0);
            bit |= this->hBit;
            mmEmulatorMachine_JoypadBitSetData(this->pEmulatorMachine, 0, bit);

            this->pNormalWindow->setVisible(false);
            this->pLustreWindow->setVisible(true);
        }
    }
    bool mmLayerEmulatorPadRWidget::IntersectFinger(mmLayerUtilityFinger* pFinger, Ogre::Vector3& hCenterToTouch)
    {
        return mmLayerUtilityIntersect_FingerCircleEllipse(&this->hEmulatorTransform, pFinger, hCenterToTouch);
    }
}
