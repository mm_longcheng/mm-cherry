/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmLayerEmulatorMode_h__
#define __mmLayerEmulatorMode_h__

#include "core/mmCore.h"

#include "container/mmRbtreeU32.h"

#include "dish/mmEvent.h"

#include "layer/director/mmLayerContext.h"

#include "CEGUI/Window.h"

#include "mmLayerEmulatorModeBase.h"

struct mmEmulatorMachine;

namespace mm
{
    class mmLayerEmulatorMode : public mmLayerContext
    {
    public:
        static const CEGUI::String EventNamespace;          //!< Namespace for global events
        static const CEGUI::String WidgetTypeName;          //!< Window factory name
    public:
        struct mmEmulatorMachine* pEmulatorMachine;
    public:
        mmUInt32_t hMode;
    public:
        struct mmRbtreeU32String hModeLayerType;
        struct mmRbtreeU32Vpt hModeLayer;

        mmLayerContext* pCurrentMode;
    public:
        mmLayerEmulatorMode(const CEGUI::String& type, const CEGUI::String& name);
        virtual ~mmLayerEmulatorMode(void);
    public:
        // mmLayerContext
        virtual void OnFinishLaunching(void);
        virtual void OnBeforeTerminate(void);
    public:
        void SetEmulatorMachine(struct mmEmulatorMachine* pEmulatorMachine);
    public:
        void SetMode(mmUInt32_t hMode);
    public:
        void UpdateModeLayer(void);
    private:
        void AddModeLayerType(mmUInt32_t id, const char* pType);
        void RmvModeLayerType(mmUInt32_t id);
        void GetModeLayerType(mmUInt32_t id, struct mmString* pType);
        void ClearModeLayerType(void);
    private:
        void RegisterDefaultModeLayer(void);

        mmLayerEmulatorModeBase* AddModeLayer(mmUInt32_t id);
        mmLayerEmulatorModeBase* GetModeLayer(mmUInt32_t id);
        mmLayerEmulatorModeBase* GetModeLayerInstance(mmUInt32_t id);
        void RmvModeLayer(mmUInt32_t id);
        void ClearModeLayer(void);
    };
}

#endif//__mmLayerEmulatorMode_h__