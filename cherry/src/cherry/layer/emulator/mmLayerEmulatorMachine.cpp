/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerEmulatorMachine.h"
#include "mmLayerEmulatorMode.h"
#include "mmLayerEmulatorToolbarL.h"
#include "mmLayerEmulatorToolbarR.h"

#include "CEGUI/WindowManager.h"

#include "layer/director/mmLayerDirector.h"

#include "nwsi/mmEventObjectArgs.h"

#include "module/mmModuleExplorer.h"

namespace mm
{
    const CEGUI::String mmLayerEmulatorMachine::EventNamespace = "mm";
    const CEGUI::String mmLayerEmulatorMachine::WidgetTypeName = "mm/mmLayerEmulatorMachine";

    const CEGUI::String mmLayerEmulatorMachine::EventEmulatorMovieFinish = "EventEmulatorMovieFinish";
    const CEGUI::String mmLayerEmulatorMachine::EventEmulatorScreenMessage = "EventEmulatorScreenMessage";
    const CEGUI::String mmLayerEmulatorMachine::EventEmulatorPlay = "EventEmulatorPlay";
    const CEGUI::String mmLayerEmulatorMachine::EventEmulatorStop = "EventEmulatorStop";

    mmLayerEmulatorMachine::mmLayerEmulatorMachine(const CEGUI::String& type, const CEGUI::String& name)
        : mmLayerContext(type, name)
        , LayerEmulatorMachine(NULL)
        , StaticImageBackground(NULL)

        , DefaultWindowMode(NULL)

        , LayerEmulatorMode(NULL)
        , LayerEmulatorToolbarL(NULL)
        , LayerEmulatorToolbarR(NULL)

        , StaticTextFps(NULL)
        , WidgetImageButtonBack(NULL)

        , pEmulatorMode(NULL)
        , pEmulatorToolbarL(NULL)
        , pEmulatorToolbarR(NULL)

        , hCurrentEmulatorAssets("")
    {
        mmEmulatorMachine_Init(&this->hEmulatorMachine);
        mmIconvContext_Init(&this->hIconvContext);
    }
    mmLayerEmulatorMachine::~mmLayerEmulatorMachine(void)
    {
        mmEmulatorMachine_Destroy(&this->hEmulatorMachine);
        mmIconvContext_Destroy(&this->hIconvContext);
    }

    void mmLayerEmulatorMachine::OnFinishLaunching(void)
    {
        struct mmContextMaster* pContextMaster = this->pContextMaster;
        struct mmSurfaceMaster* pSurfaceMaster = this->pSurfaceMaster;
        mm::mmModule* pModule = &pContextMaster->hModule;

        mm::mmModuleExplorer* pModuleExplorer = pModule->GetData<mm::mmModuleExplorer>();
        struct mmFrameScheduler* pFrameScheduler = &pSurfaceMaster->hFrameScheduler;

        pModuleExplorer->hEventSet.SubscribeEvent(mm::mmModuleExplorer::EventFormatUpdate, &mmLayerEmulatorMachine::OnIconvContextEventFormatUpdate, this);

        mmIconvContext_Cachelist(&this->hIconvContext);
        mmIconvContext_UpdateFormat(&this->hIconvContext, mmString_CStr(&pModuleExplorer->hFormatHost), mmString_CStr(&pModuleExplorer->hFormatView));

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        this->LayerEmulatorMachine = pWindowManager->loadLayoutFromFile("emulator/LayerEmulatorMachine.layout");
        this->addChild(this->LayerEmulatorMachine);

        this->StaticImageBackground = this->LayerEmulatorMachine->getChild("StaticImageBackground");

        this->DefaultWindowMode = this->StaticImageBackground->getChild("DefaultWindowMode");

        this->LayerEmulatorMode = this->StaticImageBackground->getChild("LayerEmulatorMode");
        this->LayerEmulatorToolbarL = this->StaticImageBackground->getChild("LayerEmulatorToolbarL");
        this->LayerEmulatorToolbarR = this->StaticImageBackground->getChild("LayerEmulatorToolbarR");

        this->StaticTextFps = this->StaticImageBackground->getChild("StaticTextFps");
        this->WidgetImageButtonBack = this->StaticImageBackground->getChild("WidgetImageButtonBack");
        //
        this->pEmulatorMode = (mmLayerEmulatorMode*)(this->LayerEmulatorMode);
        this->pEmulatorToolbarL = (mmLayerEmulatorToolbarL*)(this->LayerEmulatorToolbarL);
        this->pEmulatorToolbarR = (mmLayerEmulatorToolbarR*)(this->LayerEmulatorToolbarR);

        mmEmulatorMachine_SetName(&this->hEmulatorMachine, this->getName().c_str());
        mmEmulatorMachine_SetContextMaster(&this->hEmulatorMachine, this->pContextMaster);
        mmEmulatorMachine_SetDataPath(&this->hEmulatorMachine, "emulator");
        mmEmulatorMachine_SetWritablePath(&this->hEmulatorMachine, "emulator");
        mmEmulatorMachine_SetTaskBackground(&this->hEmulatorMachine, MM_FRAME_TIMER_BACKGROUND_PAUSED);
        mmEmulatorMachine_UpdateAssetsPath(&this->hEmulatorMachine);
        mmEmulatorMachine_FinishLaunching(&this->hEmulatorMachine);

        this->pEmulatorMode->SetDirector(this->pDirector);
        this->pEmulatorMode->SetContextMaster(this->pContextMaster);
        this->pEmulatorMode->SetSurfaceMaster(this->pSurfaceMaster);
        this->pEmulatorMode->SetEmulatorMachine(&this->hEmulatorMachine);
        this->pEmulatorMode->OnFinishLaunching();

        this->pEmulatorToolbarL->SetDirector(this->pDirector);
        this->pEmulatorToolbarL->SetContextMaster(this->pContextMaster);
        this->pEmulatorToolbarL->SetSurfaceMaster(this->pSurfaceMaster);
        this->pEmulatorToolbarL->SetEmulatorMachine(&this->hEmulatorMachine);
        this->pEmulatorToolbarL->SetIconvContext(&this->hIconvContext);
        this->pEmulatorToolbarL->OnFinishLaunching();

        this->pEmulatorToolbarR->SetDirector(this->pDirector);
        this->pEmulatorToolbarR->SetContextMaster(this->pContextMaster);
        this->pEmulatorToolbarR->SetSurfaceMaster(this->pSurfaceMaster);
        this->pEmulatorToolbarR->SetEmulatorMachine(&this->hEmulatorMachine);
        this->pEmulatorToolbarR->SetIconvContext(&this->hIconvContext);
        this->pEmulatorToolbarR->OnFinishLaunching();

        this->hUtilityFps.SetFrameTimer(&pFrameScheduler->frame_timer);
        this->hUtilityFps.SetWindow(this->StaticTextFps);
        this->hUtilityFps.OnFinishLaunching();

        this->hIndicator.SetWindowMenu(this->DefaultWindowMode);
        this->hIndicator.OnFinishLaunching();

        mmEmulatorMachine_Start(&this->hEmulatorMachine);

        this->hIndicatorEventModeChangeConn = this->DefaultWindowMode->subscribeEvent(mmLayerEmulatorIndicator::EventModeChange, CEGUI::Event::Subscriber(&mmLayerEmulatorMachine::OnIndicatorEventModeChange, this));
        //
        this->hFpsEventMouseClickConn = this->StaticTextFps->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerEmulatorMachine::OnStaticTextFpsEventMouseClick, this));
        this->hBackEventMouseClickConn = this->WidgetImageButtonBack->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerEmulatorMachine::OnWidgetImageButtonBackEventMouseClick, this));

        pSurfaceMaster->hEventSet.SubscribeEvent(mmSurfaceMaster_EventEnterBackground, &mmLayerEmulatorMachine::OnEventEnterBackground, this);
        pSurfaceMaster->hEventSet.SubscribeEvent(mmSurfaceMaster_EventEnterForeground, &mmLayerEmulatorMachine::OnEventEnterForeground, this);

        this->hEmulatorMachine.hEventSet.SubscribeEvent(Event_MM_EMU_NT_SCREENMESSAGE, &mmLayerEmulatorMachine::OnEmulatorEvent_MM_EMU_NT_SCREENMESSAGE, this);
        this->hEmulatorMachine.hEventSet.SubscribeEvent(Event_MM_EMU_NT_MOVIE_FINISH, &mmLayerEmulatorMachine::OnEmulatorEvent_MM_EMU_NT_MOVIE_FINISH, this);
        this->hEmulatorMachine.hEventSet.SubscribeEvent(Event_MM_EMU_NT_PLAY, &mmLayerEmulatorMachine::OnEmulatorEvent_MM_EMU_NT_PLAY, this);
        this->hEmulatorMachine.hEventSet.SubscribeEvent(Event_MM_EMU_NT_STOP, &mmLayerEmulatorMachine::OnEmulatorEvent_MM_EMU_NT_STOP, this);

        // spacing common CEGUI::Window object, we need manual attach context event.
        this->AttachContextEvent(this->pEmulatorMode);
        this->AttachContextEvent(this->pEmulatorToolbarL);
        this->AttachContextEvent(this->pEmulatorToolbarR);
    }

    void mmLayerEmulatorMachine::OnBeforeTerminate(void)
    {
        struct mmContextMaster* pContextMaster = this->pContextMaster;
        struct mmSurfaceMaster* pSurfaceMaster = this->pSurfaceMaster;
        mm::mmModule* pModule = &pContextMaster->hModule;

        mm::mmModuleExplorer* pModuleExplorer = pModule->GetData<mm::mmModuleExplorer>();

        // spacing common CEGUI::Window object, we need manual attach context event.
        this->DetachContextEvent(this->pEmulatorMode);
        this->DetachContextEvent(this->pEmulatorToolbarL);
        this->DetachContextEvent(this->pEmulatorToolbarR);

        this->hEmulatorMachine.hEventSet.UnsubscribeEvent(Event_MM_EMU_NT_SCREENMESSAGE, &mmLayerEmulatorMachine::OnEmulatorEvent_MM_EMU_NT_SCREENMESSAGE, this);
        this->hEmulatorMachine.hEventSet.UnsubscribeEvent(Event_MM_EMU_NT_MOVIE_FINISH, &mmLayerEmulatorMachine::OnEmulatorEvent_MM_EMU_NT_MOVIE_FINISH, this);
        this->hEmulatorMachine.hEventSet.UnsubscribeEvent(Event_MM_EMU_NT_PLAY, &mmLayerEmulatorMachine::OnEmulatorEvent_MM_EMU_NT_PLAY, this);
        this->hEmulatorMachine.hEventSet.UnsubscribeEvent(Event_MM_EMU_NT_STOP, &mmLayerEmulatorMachine::OnEmulatorEvent_MM_EMU_NT_STOP, this);

        pSurfaceMaster->hEventSet.UnsubscribeEvent(mmSurfaceMaster_EventEnterBackground, &mmLayerEmulatorMachine::OnEventEnterBackground, this);
        pSurfaceMaster->hEventSet.UnsubscribeEvent(mmSurfaceMaster_EventEnterForeground, &mmLayerEmulatorMachine::OnEventEnterForeground, this);

        this->hIndicatorEventModeChangeConn->disconnect();

        this->hFpsEventMouseClickConn->disconnect();
        this->hBackEventMouseClickConn->disconnect();

        this->hIndicator.OnBeforeTerminate();
        this->hUtilityFps.OnBeforeTerminate();

        this->pEmulatorMode->OnBeforeTerminate();
        this->pEmulatorToolbarL->OnBeforeTerminate();
        this->pEmulatorToolbarR->OnBeforeTerminate();

        mmEmulatorMachine_BeforeTerminate(&this->hEmulatorMachine);
        mmEmulatorMachine_Shutdown(&this->hEmulatorMachine);
        mmEmulatorMachine_Join(&this->hEmulatorMachine);

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        pWindowManager->destroyWindow(this->LayerEmulatorMachine);
        this->LayerEmulatorMachine = NULL;

        pModuleExplorer->hEventSet.UnsubscribeEvent(mm::mmModuleExplorer::EventFormatUpdate, &mmLayerEmulatorMachine::OnIconvContextEventFormatUpdate, this);
    }
    void mmLayerEmulatorMachine::UpdateLayerValue(void)
    {
        this->pEmulatorToolbarL->SetMovieBaseName("");
        this->pEmulatorToolbarR->SetStateBaseName("");

        this->pEmulatorToolbarL->UpdateLayerValue();
        this->pEmulatorToolbarR->UpdateLayerValue();
    }
    bool mmLayerEmulatorMachine::OnEventEnterBackground(const mmEventArgs& args)
    {
        mmEmulatorMachine_EnterBackground(&this->hEmulatorMachine);
        return true;
    }
    bool mmLayerEmulatorMachine::OnEventEnterForeground(const mmEventArgs& args)
    {
        mmEmulatorMachine_EnterForeground(&this->hEmulatorMachine);
        return true;
    }
    bool mmLayerEmulatorMachine::OnIconvContextEventFormatUpdate(const mmEventArgs& args)
    {
        const mmEventObjectArgs& evt = (const mmEventObjectArgs&)(args);
        mmModuleExplorer* pModuleExplorer = (mmModuleExplorer*)evt.pObject;
        mmIconvContext_UpdateFormat(&this->hIconvContext, mmString_CStr(&pModuleExplorer->hFormatHost), mmString_CStr(&pModuleExplorer->hFormatView));
        
        this->pEmulatorToolbarL->UpdateUtf8View();
        this->pEmulatorToolbarR->UpdateUtf8View();
        return true;
    }
    bool mmLayerEmulatorMachine::OnEmulatorEvent_MM_EMU_NT_SCREENMESSAGE(const mmEventArgs& args)
    {
        const mmEventEmulatorArgs& evt = (const mmEventEmulatorArgs&)(args);

        mmEmulatorMachineEventArgs hArgs(this);

        hArgs.mid = evt.mid;
        hArgs.p0 = evt.p0;
        hArgs.p1 = evt.p1;
        hArgs.ps = evt.ps;

        this->fireEvent(EventEmulatorScreenMessage, hArgs, EventNamespace);
        return true;
    }
    bool mmLayerEmulatorMachine::OnEmulatorEvent_MM_EMU_NT_MOVIE_FINISH(const mmEventArgs& args)
    {
        const mmEventEmulatorArgs& evt = (const mmEventEmulatorArgs&)(args);

        mmEmulatorMachineEventArgs hArgs(this);

        hArgs.mid = evt.mid;
        hArgs.p0 = evt.p0;
        hArgs.p1 = evt.p1;
        hArgs.ps = evt.ps;

        this->fireEvent(EventEmulatorMovieFinish, hArgs, EventNamespace);
        return true;
    }
    bool mmLayerEmulatorMachine::OnEmulatorEvent_MM_EMU_NT_PLAY(const mmEventArgs& args)
    {
        const mmEventEmulatorArgs& evt = (const mmEventEmulatorArgs&)(args);

        mmEmulatorMachineEventArgs hArgs(this);

        hArgs.mid = evt.mid;
        hArgs.p0 = evt.p0;
        hArgs.p1 = evt.p1;
        hArgs.ps = evt.ps;

        this->fireEvent(EventEmulatorPlay, hArgs, EventNamespace);
        return true;
    }
    bool mmLayerEmulatorMachine::OnEmulatorEvent_MM_EMU_NT_STOP(const mmEventArgs& args)
    {
        const mmEventEmulatorArgs& evt = (const mmEventEmulatorArgs&)(args);

        mmEmulatorMachineEventArgs hArgs(this);

        hArgs.mid = evt.mid;
        hArgs.p0 = evt.p0;
        hArgs.p1 = evt.p1;
        hArgs.ps = evt.ps;

        this->fireEvent(EventEmulatorStop, hArgs, EventNamespace);
        return true;
    }
    bool mmLayerEmulatorMachine::OnIndicatorEventModeChange(const CEGUI::EventArgs& args)
    {
        const mmLayerEmulatorIndicatorEventArgs& evt = (const mmLayerEmulatorIndicatorEventArgs&)(args);

        this->pEmulatorMode->SetMode(evt.hMode);
        this->pEmulatorMode->UpdateModeLayer();
        return true;
    }
    bool mmLayerEmulatorMachine::OnStaticTextFpsEventMouseClick(const CEGUI::EventArgs& args)
    {
        mmLayerDirector_SceneExclusiveForeground(this->pDirector, "mm/mmLayerCommonTextureManager", "mmLayerCommonTextureManager");
        return true;
    }
    bool mmLayerEmulatorMachine::OnWidgetImageButtonBackEventMouseClick(const CEGUI::EventArgs& args)
    {
        mmLayerDirector_SceneExclusiveEnterBackground(this->pDirector, this->getName().c_str());
        return true;
    }
}
