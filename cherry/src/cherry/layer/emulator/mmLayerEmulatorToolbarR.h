/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmLayerEmulatorToolbarR_h__
#define __mmLayerEmulatorToolbarR_h__

#include "core/mmCore.h"

#include "container/mmVectorVpt.h"
#include "container/mmVectorValue.h"

#include "dish/mmEvent.h"

#include "layer/director/mmLayerContext.h"

#include "CEGUI/Window.h"

#include "explorer/mmExplorerItem.h"

struct mmEmulatorMachine;
struct mmIconvContext;

namespace mm
{
    class mmLayerEmulatorToolbarR : public mmLayerContext
    {
    public:
        static const CEGUI::String EventNamespace;          //!< Namespace for global events
        static const CEGUI::String WidgetTypeName;          //!< Window factory name
    public:
        CEGUI::Window* LayerEmulatorToolbarR;
        CEGUI::Window* StaticImageBackground;

        CEGUI::Window* DefaultWindowWristband;

        CEGUI::Window* StaticImageHandleO;
        CEGUI::Window* StaticImageHandleI;

        CEGUI::Window* WidgetComboboxVideoMode;
        CEGUI::Window* WidgetComboboxState;
        CEGUI::Window* WidgetComboboxURapidAB;

        CEGUI::Window* LabelRapidAB;

        CEGUI::Window* WidgetImageButtonStateDelete;

        CEGUI::Window* WidgetImageButtonAudioHorn;
        CEGUI::Window* WidgetImageButtonAudioMute;

        CEGUI::Window* WidgetImageButtonEmuPlay;
        CEGUI::Window* WidgetImageButtonEmuStop;

        CEGUI::Window* WidgetImageButtonStateLoad;
        CEGUI::Window* WidgetImageButtonStateSave;

        CEGUI::Window* StaticTextStateMTime;
        
        // weak ref.
        CEGUI::Combobox* pWidgetComboboxVideoMode;
        // weak ref.
        CEGUI::Combobox* pWidgetComboboxState;
        // weak ref.
        CEGUI::Combobox* pWidgetComboboxURapidAB;
        //
        CEGUI::Editbox* pEditboxState;
    public:
        struct mmEmulatorMachine* pEmulatorMachine;
    public:
        // weak ref.
        struct mmIconvContext* pIconvContext;
    public:
        struct mmVectorValue hItemModeVector;
        struct mmVectorValue hItemStateVector;
        struct mmVectorValue hItemRapidVector;
    public:
        struct mmExplorerItem hStateItem;
        //
        struct mmString hStateRealPath;
        struct mmString hStatePathName;
        struct mmString hStateBaseName;
        
        // cache
        struct mmString hTextBaseNameHost;
    public:
        size_t hCurrentStateIndex;
    public:
        CEGUI::Colour hColourLustre;
        CEGUI::Colour hColourCommon;
    public:
        mmLayerEmulatorToolbarR(const CEGUI::String& type, const CEGUI::String& name);
        virtual ~mmLayerEmulatorToolbarR(void);
    public:
        void SetEmulatorMachine(struct mmEmulatorMachine* pEmulatorMachine);
        void SetIconvContext(struct mmIconvContext* pIconvContext);
        void SetStateBaseName(const char* pStateBaseName);
    public:
        // mmLayerContext
        virtual void OnFinishLaunching(void);
        virtual void OnBeforeTerminate(void);
    public:
        void UpdateLayerValue(void);
        void UpdateUtf8View(void);
        void UpdateVideoModeView(void);
        void UpdateRapidView(void);
        void UpdateStateComboboxView(void);
        void UpdateStateEditView(void);
        void UpdateStateStatusView(void);
    public:
        void RefreshComboboxStateLayer(void);
    private:
        void CreateComboboxModeItem(void);
        void DeleteComboboxModeItem(void);
    private:
        void CreateComboboxStateItem(void);
        void DeleteComboboxStateItem(void);
        void UpdateComboboxStateItem(void);
    private:
        void CreateComboboxRapidItem(void);
        void DeleteComboboxRapidItem(void);
    private:
        bool OnEmulatorEvent_MM_EMU_RS_STATE_SAVE(const mmEventArgs& args);
        bool OnEmulatorEvent_MM_EMU_NT_PLAY(const mmEventArgs& args);
        bool OnEmulatorEvent_MM_EMU_NT_STOP(const mmEventArgs& args);
        bool OnEmulatorEvent_MM_EMU_NT_VIDEOMODE(const mmEventArgs& args);
    private:
        bool OnStaticImageBackgroundEventMouseButtonDown(const CEGUI::EventArgs& args);
        bool OnStaticImageBackgroundEventMouseButtonUp(const CEGUI::EventArgs& args);
        bool OnToolbarREventMouseButtonDown(const CEGUI::EventArgs& args);
        bool OnToolbarREventMouseButtonUp(const CEGUI::EventArgs& args);
        //
        bool OnWidgetImageButtonStateDeleteEventMouseClick(const CEGUI::EventArgs& args);
        //
        bool OnWidgetComboboxVideoModeEventTextAccepted(const CEGUI::EventArgs& args);
        bool OnWidgetComboboxVideoModeEventListSelectionAccepted(const CEGUI::EventArgs& args);
        //
        bool OnWidgetComboboxStateEventTextChanged(const CEGUI::EventArgs& args);
        bool OnWidgetComboboxStateEventTextAccepted(const CEGUI::EventArgs& args);
        bool OnWidgetComboboxStateEventListSelectionAccepted(const CEGUI::EventArgs& args);
        //
        bool OnWidgetComboboxRapidABEventTextAccepted(const CEGUI::EventArgs& args);
        bool OnWidgetComboboxRapidABEventListSelectionAccepted(const CEGUI::EventArgs& args);
        //
        bool OnWidgetImageButtonAudioHornEventMouseClick(const CEGUI::EventArgs& args);
        bool OnWidgetImageButtonAudioMuteEventMouseClick(const CEGUI::EventArgs& args);
        //
        bool OnWidgetImageButtonEmuPlayEventMouseClick(const CEGUI::EventArgs& args);
        bool OnWidgetImageButtonEmuStopEventMouseClick(const CEGUI::EventArgs& args);
        //
        bool OnWidgetImageButtonStateLoadEventMouseClick(const CEGUI::EventArgs& args);
        bool OnWidgetImageButtonStateSaveEventMouseClick(const CEGUI::EventArgs& args);
    private:
        bool OnLayerEnsureEventChoiceRemove(const CEGUI::EventArgs& args);
    };
}

#endif//__mmLayerEmulatorToolbarR_h__