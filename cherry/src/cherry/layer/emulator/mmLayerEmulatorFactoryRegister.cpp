/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerEmulatorFactoryRegister.h"
#include "layer/director/mmLayerDirector.h"
#include "CEGUI/WindowFactoryManager.h"

#include "mmLayerEmulatorPadL.h"
#include "mmLayerEmulatorPadR.h"

#include "mmLayerEmulatorToolbarL.h"
#include "mmLayerEmulatorToolbarR.h"
#include "mmLayerEmulatorMode.h"
#include "mmLayerEmulatorModeOrdinary.h"
#include "mmLayerEmulatorModeJoystick.h"
#include "mmLayerEmulatorModeKeyboard.h"

#include "mmLayerEmulatorMachine.h"

void mmLayerEmulatorFactory_Register(struct mmLayerDirector* pDirector)
{
    CEGUI::WindowFactoryManager* pWindowFactoryManager = CEGUI::WindowFactoryManager::getSingletonPtr();

    pWindowFactoryManager->addWindowType<mm::mmLayerEmulatorPadL>();
    pWindowFactoryManager->addWindowType<mm::mmLayerEmulatorPadR>();

    pWindowFactoryManager->addWindowType<mm::mmLayerEmulatorToolbarL>();
    pWindowFactoryManager->addWindowType<mm::mmLayerEmulatorToolbarR>();
    pWindowFactoryManager->addWindowType<mm::mmLayerEmulatorMode>();
    pWindowFactoryManager->addWindowType<mm::mmLayerEmulatorModeOrdinary>();
    pWindowFactoryManager->addWindowType<mm::mmLayerEmulatorModeJoystick>();
    pWindowFactoryManager->addWindowType<mm::mmLayerEmulatorModeKeyboard>();

    pWindowFactoryManager->addWindowType<mm::mmLayerEmulatorMachine>();
}


void mmLayerEmulatorFactory_Unregister(struct mmLayerDirector* pDirector)
{
    CEGUI::WindowFactoryManager* pWindowFactoryManager = CEGUI::WindowFactoryManager::getSingletonPtr();

    pWindowFactoryManager->removeFactory(mm::mmLayerEmulatorPadL::WidgetTypeName);
    pWindowFactoryManager->removeFactory(mm::mmLayerEmulatorPadR::WidgetTypeName);

    pWindowFactoryManager->removeFactory(mm::mmLayerEmulatorToolbarL::WidgetTypeName);
    pWindowFactoryManager->removeFactory(mm::mmLayerEmulatorToolbarR::WidgetTypeName);
    pWindowFactoryManager->removeFactory(mm::mmLayerEmulatorMode::WidgetTypeName);
    pWindowFactoryManager->removeFactory(mm::mmLayerEmulatorModeOrdinary::WidgetTypeName);
    pWindowFactoryManager->removeFactory(mm::mmLayerEmulatorModeJoystick::WidgetTypeName);
    pWindowFactoryManager->removeFactory(mm::mmLayerEmulatorModeKeyboard::WidgetTypeName);

    pWindowFactoryManager->removeFactory(mm::mmLayerEmulatorMachine::WidgetTypeName);
}

