/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerEmulatorPadRudder.h"
#include "mmLayerEmulatorTransform.h"

#include "layer/utility/mmLayerUtilityFinger.h"

#include "emulator/mmEmulatorMachine.h"

#include "math/mmMathConst.h"

static const mmWord_t __static_JoypadWheelMask =
1 << MM_EMU_PAD_JOYPAD_ARROW_U |
1 << MM_EMU_PAD_JOYPAD_ARROW_D |
1 << MM_EMU_PAD_JOYPAD_ARROW_L |
1 << MM_EMU_PAD_JOYPAD_ARROW_R;

static const mmWord_t __static_JoypadWheelBitTable[8] =
{
    1 << MM_EMU_PAD_JOYPAD_ARROW_R,                                  // R
    1 << MM_EMU_PAD_JOYPAD_ARROW_U | 1 << MM_EMU_PAD_JOYPAD_ARROW_R, // UR
    1 << MM_EMU_PAD_JOYPAD_ARROW_U,                                  // U
    1 << MM_EMU_PAD_JOYPAD_ARROW_U | 1 << MM_EMU_PAD_JOYPAD_ARROW_L, // UL
    1 << MM_EMU_PAD_JOYPAD_ARROW_L,                                  // L
    1 << MM_EMU_PAD_JOYPAD_ARROW_D | 1 << MM_EMU_PAD_JOYPAD_ARROW_L, // DL
    1 << MM_EMU_PAD_JOYPAD_ARROW_D,                                  // D
    1 << MM_EMU_PAD_JOYPAD_ARROW_D | 1 << MM_EMU_PAD_JOYPAD_ARROW_R, // DR
};

static void __static_EmulatorPadRudderCallback_Default(void* obj, mmWord_t v)
{

}
void mmLayerEmulatorPadRudderCallback_Init(struct mmLayerEmulatorPadRudderCallback* p)
{
    p->UpdateLight = &__static_EmulatorPadRudderCallback_Default;
    p->obj = NULL;
}
void mmLayerEmulatorPadRudderCallback_Destroy(struct mmLayerEmulatorPadRudderCallback* p)
{
    p->UpdateLight = &__static_EmulatorPadRudderCallback_Default;
    p->obj = NULL;
}

namespace mm
{
    mmLayerEmulatorPadRudder::mmLayerEmulatorPadRudder()
        : pLayerContext(NULL)

        , pFingerTouch(NULL)

        , pPadWindow(NULL)
        , pEmulatorMachine(NULL)

        , hRotation(0)

        , hInvalidRadiusX(0)
        , hInvalidRadiusY(0)
        , hInvalidRadiusXK(0)
        , hInvalidRadiusYK(0)
    {
        mmLayerEmulatorPadRudderCallback_Init(&this->hCallback);
        mmRbtreeU64Vpt_Init(&this->hRbtreeTouchs);
        mmLayerUtilityTransform_Init(&this->hEmulatorTransform);
    }
    mmLayerEmulatorPadRudder::~mmLayerEmulatorPadRudder()
    {
        mmLayerUtilityTransform_Destroy(&this->hEmulatorTransform);
        mmLayerEmulatorPadRudderCallback_Destroy(&this->hCallback);
        mmRbtreeU64Vpt_Destroy(&this->hRbtreeTouchs);
    }
    void mmLayerEmulatorPadRudder::SetLayerContext(mmLayerContext* pLayerContext)
    {
        this->pLayerContext = pLayerContext;
    }
    void mmLayerEmulatorPadRudder::SetFingerTouch(mmLayerUtilityFingerTouch* pFingerTouch)
    {
        this->pFingerTouch = pFingerTouch;
    }
    void mmLayerEmulatorPadRudder::SetPadWindow(CEGUI::Window* pPadWindow)
    {
        this->pPadWindow = pPadWindow;
    }
    void mmLayerEmulatorPadRudder::SetEmulatorMachine(struct mmEmulatorMachine* pEmulatorMachine)
    {
        this->pEmulatorMachine = pEmulatorMachine;
    }
    void mmLayerEmulatorPadRudder::SetInvalidRadiusK(double hInvalidRadiusXK, double hInvalidRadiusYK)
    {
        this->hInvalidRadiusXK = hInvalidRadiusXK;
        this->hInvalidRadiusYK = hInvalidRadiusYK;
    }
    void mmLayerEmulatorPadRudder::SetCallback(struct mmLayerEmulatorPadRudderCallback* pCallback)
    {
        this->hCallback = *pCallback;
    }
    void mmLayerEmulatorPadRudder::UpdateTransform(void)
    {
        this->hRotation = 0.0f;
        this->UpdatePadWindowRotation();

        this->pPadWindow->setVisible(false);

        mmLayerUtilityTransform_UpdateTransform(&this->hEmulatorTransform, this->pPadWindow);

        this->hInvalidRadiusX = this->hInvalidRadiusXK * this->hEmulatorTransform.hWorldWindowA;
        this->hInvalidRadiusY = this->hInvalidRadiusYK * this->hEmulatorTransform.hWorldWindowB;
    }
    void mmLayerEmulatorPadRudder::UpdatePadWindowRotation(void)
    {
        static const CEGUI::Vector3f hAxisZ(0.0f, 0.0f, 1.0f);
        CEGUI::Quaternion hCEGUIQuaternion = CEGUI::Quaternion::axisAngleRadians(hAxisZ, (float)(this->hRotation));
        this->pPadWindow->setRotation(hCEGUIQuaternion);
    }
    void mmLayerEmulatorPadRudder::OnFinishLaunching(void)
    {
        this->UpdateTransform();
    }
    void mmLayerEmulatorPadRudder::OnBeforeTerminate(void)
    {
        mmRbtreeU64Vpt_Clear(&this->hRbtreeTouchs);
    }
    void mmLayerEmulatorPadRudder::OnEventWindowSizeChanged(struct mmSurfaceContentSizeChange* pContent)
    {
        this->UpdateTransform();
    }
    void mmLayerEmulatorPadRudder::OnEventTouchsMoved(struct mmSurfaceContentTouchs* pContent)
    {
        this->AddTouchsFinger(pContent);
        this->MovTouchsFinger(pContent);
        this->UpdateTouchsFinger();
    }
    void mmLayerEmulatorPadRudder::OnEventTouchsBegan(struct mmSurfaceContentTouchs* pContent)
    {
        this->AddTouchsFinger(pContent);
        this->UpdateTouchsFinger();
    }
    void mmLayerEmulatorPadRudder::OnEventTouchsEnded(struct mmSurfaceContentTouchs* pContent)
    {
        this->RmvTouchsFinger(pContent);
        this->UpdateTouchsFinger();
    }
    void mmLayerEmulatorPadRudder::OnEventTouchsBreak(struct mmSurfaceContentTouchs* pContent)
    {
        this->RmvTouchsFinger(pContent);
        this->UpdateTouchsFinger();
    }
    void mmLayerEmulatorPadRudder::AddTouchsFinger(struct mmSurfaceContentTouchs* pContent)
    {
        struct mmRbtreeU64Vpt* pTouchs = &this->pFingerTouch->hRbtreeTouchs;

        // world position.
        Ogre::Vector3 hCenterToTouch;

        mmLayerUtilityFinger* u = NULL;

        struct mmRbNode* n = NULL;
        struct mmRbtreeU64VptIterator* it = NULL;
        //
        n = mmRb_First(&pTouchs->rbt);
        while (NULL != n)
        {
            it = (struct mmRbtreeU64VptIterator*)mmRb_Entry(n, struct mmRbtreeU64VptIterator, n);
            n = mmRb_Next(n);
            // 
            u = (mmLayerUtilityFinger*)it->v;

            if (this->IntersectFinger(u, hCenterToTouch))
            {
                this->hRotation = (double)atan2((double)hCenterToTouch.y, (double)hCenterToTouch.x);

                this->UpdatePadWindowRotation();

                mmRbtreeU64Vpt_Set(&this->hRbtreeTouchs, u->hMotionId, u);
            }
        }
    }
    void mmLayerEmulatorPadRudder::RmvTouchsFinger(struct mmSurfaceContentTouchs* pContent)
    {
        size_t i = 0;

        struct mmSurfaceContentTouch* pTouchI = NULL;
        mmLayerUtilityFinger* pFinger = NULL;
        struct mmVectorValue* pVectorValue = &pContent->context;

        for (i = 0; i < pVectorValue->size; i++)
        {
            pTouchI = (struct mmSurfaceContentTouch*)mmVectorValue_GetIndexReference(pVectorValue, i);
            // remove.
            mmRbtreeU64Vpt_Rmv(&this->hRbtreeTouchs, pTouchI->motion_id);
        }
    }
    void mmLayerEmulatorPadRudder::MovTouchsFinger(struct mmSurfaceContentTouchs* pContent)
    {
        // world position.
        Ogre::Vector3 hCenterToTouch;
        double dx = 0;
        double dy = 0;

        mmLayerUtilityFinger* u = NULL;

        struct mmRbNode* n = NULL;
        struct mmRbtreeU64VptIterator* it = NULL;
        //
        n = mmRb_First(&this->hRbtreeTouchs.rbt);
        while (NULL != n)
        {
            it = (struct mmRbtreeU64VptIterator*)mmRb_Entry(n, struct mmRbtreeU64VptIterator, n);
            n = mmRb_Next(n);
            // 
            u = (mmLayerUtilityFinger*)it->v;
            //
            if (this->IntersectFingerMov(u, hCenterToTouch))
            {
                this->hRotation = (double)atan2((double)hCenterToTouch.y, (double)hCenterToTouch.x);

                this->UpdatePadWindowRotation();
            }
            else
            {
                // remove.
                mmRbtreeU64Vpt_Erase(&this->hRbtreeTouchs, it);
                continue;
            }
        }
    }

    void mmLayerEmulatorPadRudder::UpdateTouchsFinger(void)
    {
        if (0 == this->hRbtreeTouchs.size)
        {
            // JoypadBitRelease.

            mmWord_t bit = mmEmulatorMachine_JoypadBitGetData(this->pEmulatorMachine, 0);
            bit &= ~__static_JoypadWheelMask;
            mmEmulatorMachine_JoypadBitSetData(this->pEmulatorMachine, 0, bit);

            this->pPadWindow->setVisible(false);

            (*(this->hCallback.UpdateLight))(this, 0);
        }
        else
        {
            // JoypadBitPressed.

            static const double __PI_DIV_4 = MM_PI_4;
            static const double __PI_DIV_8 = MM_PI_4 / 2.0;

            // x0[0, 8)
            //    x0 = 0;
            // x0[0, 8) => (0, 8]
            //    x1 = 8 - x0;
            // x0[0, 8) => (0, 2 * MM_PI]
            //    x2 = x1 * (MM_PI / 4.0);
            // x0[0, 8) => (MM_PI / 8, 2 * MM_PI + MM_PI / 8]
            //    x3 = x2 + MM_PI / 8.0;
            // y
            //    y = 8 - ((x3 - (MM_PI / 8.0)) / (MM_PI / 4.0));
            // Simplification
            //    y = 8 - (x3 / (MM_PI / 4.0) - 0.5);

            double y = 8 - (this->hRotation / __PI_DIV_4 - 0.5);
            mmUInt8_t index = (mmUInt8_t)(y) & 0x07;

            mmWord_t v = __static_JoypadWheelBitTable[index];

            mmWord_t bit = mmEmulatorMachine_JoypadBitGetData(this->pEmulatorMachine, 0);
            bit &= ~__static_JoypadWheelMask;
            bit |= v;
            mmEmulatorMachine_JoypadBitSetData(this->pEmulatorMachine, 0, bit);

            this->pPadWindow->setVisible(true);

            (*(this->hCallback.UpdateLight))(this, v);
        }
    }
    bool mmLayerEmulatorPadRudder::IntersectFinger(mmLayerUtilityFinger* pFinger, Ogre::Vector3& hCenterToTouch)
    {
        // intersect circle ellipse.
        Ogre::Vector3 hTouchPosition;

        hTouchPosition.x = (float)pFinger->hAbsXW;
        hTouchPosition.y = (float)pFinger->hAbsYW;
        hTouchPosition.z = (float)0.0f;

        hCenterToTouch = hTouchPosition - this->hEmulatorTransform.hWorldWindowCenter;

        // (x0 * x0 / (a + r) * (a + r)) + (y0 * y0 / (b + r) * (b + r)) < 1

        double x_square = hCenterToTouch.x * hCenterToTouch.x;
        double y_square = hCenterToTouch.y * hCenterToTouch.y;

        bool b0 = false;
        bool b1 = false;

        {
            double ar = this->hEmulatorTransform.hWorldWindowA + pFinger->hWorldWindowRadius;
            double br = this->hEmulatorTransform.hWorldWindowB + pFinger->hWorldWindowRadius;

            double ar_square = ar * ar;
            double br_square = br * br;

            b0 = 1 > (x_square / ar_square) + (y_square / br_square);
        }

        {
            double ar = this->hInvalidRadiusX - pFinger->hWorldWindowRadius;
            double br = this->hInvalidRadiusY - pFinger->hWorldWindowRadius;

            ar = fabs(ar);
            br = fabs(br);

            double ar_square = ar * ar;
            double br_square = br * br;

            b1 = 1 > (x_square / ar_square) + (y_square / br_square);
        }
        return b0 && !b1;
    }
    bool mmLayerEmulatorPadRudder::IntersectFingerMov(mmLayerUtilityFinger* pFinger, Ogre::Vector3& hCenterToTouch)
    {
        // intersect circle ellipse.
        Ogre::Vector3 hTouchPosition;

        hTouchPosition.x = (float)pFinger->hAbsXW;
        hTouchPosition.y = (float)pFinger->hAbsYW;
        hTouchPosition.z = (float)0.0f;

        hCenterToTouch = hTouchPosition - this->hEmulatorTransform.hWorldWindowCenter;

        // (x0 * x0 / (a + r) * (a + r)) + (y0 * y0 / (b + r) * (b + r)) < 1

        double x_square = hCenterToTouch.x * hCenterToTouch.x;
        double y_square = hCenterToTouch.y * hCenterToTouch.y;

        bool b1 = false;

        {
            double ar = this->hInvalidRadiusX - pFinger->hWorldWindowRadius;
            double br = this->hInvalidRadiusY - pFinger->hWorldWindowRadius;

            ar = fabs(ar);
            br = fabs(br);

            double ar_square = ar * ar;
            double br_square = br * br;

            b1 = 1 > (x_square / ar_square) + (y_square / br_square);
        }
        return !b1;
    }
    void mmLayerEmulatorPadRudder::UpdateLight(mmWord_t v)
    {

    }
}
