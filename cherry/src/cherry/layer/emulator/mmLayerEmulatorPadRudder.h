/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmLayerEmulatorPadRudder_h__
#define __mmLayerEmulatorPadRudder_h__

#include "container/mmRbtreeU64.h"

#include "CEGUI/Window.h"

#include "layer/director/mmLayerContext.h"

#include "layer/utility/mmLayerUtilityTransform.h"

struct mmLayerEmulatorPadRudderCallback
{
    void(*UpdateLight)(void* obj, mmWord_t v);
    void* obj;
};
extern void mmLayerEmulatorPadRudderCallback_Init(struct mmLayerEmulatorPadRudderCallback* p);
extern void mmLayerEmulatorPadRudderCallback_Destroy(struct mmLayerEmulatorPadRudderCallback* p);

struct mmEmulatorMachine;

namespace mm
{
    class mmLayerUtilityFinger;
    class mmLayerUtilityFingerTouch;

    class mmLayerEmulatorPadRudder
    {
    public:
        mmLayerContext* pLayerContext;
    public:
        mmLayerUtilityFingerTouch* pFingerTouch;
    public:
        CEGUI::Window* pPadWindow;
    public:
        struct mmEmulatorMachine* pEmulatorMachine;
    public:
        struct mmLayerUtilityTransform hEmulatorTransform;
    public:
        double hRotation;

        double hInvalidRadiusX;
        double hInvalidRadiusY;
        double hInvalidRadiusXK;
        double hInvalidRadiusYK;
    public:
        struct mmLayerEmulatorPadRudderCallback hCallback;
    public:
        struct mmRbtreeU64Vpt hRbtreeTouchs;
    public:
        mmLayerEmulatorPadRudder(void);
        ~mmLayerEmulatorPadRudder(void);
    public:
        void SetLayerContext(mmLayerContext* pLayerContext);
    public:
        void SetFingerTouch(mmLayerUtilityFingerTouch* pFingerTouch);
    public:
        void SetPadWindow(CEGUI::Window* pPadWindow);
        void SetEmulatorMachine(struct mmEmulatorMachine* pEmulatorMachine);
    public:
        // invalid radius k.
        void SetInvalidRadiusK(double hInvalidRadiusXK, double hInvalidRadiusYK);
    public:
        void SetCallback(struct mmLayerEmulatorPadRudderCallback* pCallback);
    public:
        void UpdateTransform(void);
        void UpdatePadWindowRotation(void);
    public:
        void OnFinishLaunching(void);
        void OnBeforeTerminate(void);
    public:
        void OnEventWindowSizeChanged(struct mmSurfaceContentSizeChange* pContent);
    public:
        void OnEventTouchsMoved(struct mmSurfaceContentTouchs* pContent);
        void OnEventTouchsBegan(struct mmSurfaceContentTouchs* pContent);
        void OnEventTouchsEnded(struct mmSurfaceContentTouchs* pContent);
        void OnEventTouchsBreak(struct mmSurfaceContentTouchs* pContent);
    private:
        void AddTouchsFinger(struct mmSurfaceContentTouchs* pContent);
        void RmvTouchsFinger(struct mmSurfaceContentTouchs* pContent);
        void MovTouchsFinger(struct mmSurfaceContentTouchs* pContent);
    private:
        void UpdateTouchsFinger(void);
    private:
        // r0 < v && v < r1
        bool IntersectFinger(mmLayerUtilityFinger* pFinger, Ogre::Vector3& hCenterToTouch);
        // r0 < v
        bool IntersectFingerMov(mmLayerUtilityFinger* pFinger, Ogre::Vector3& hCenterToTouch);
    private:
        void UpdateLight(mmWord_t v);
    };
}

#endif//__mmLayerEmulatorPadRudder_h__

