/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerEmulatorPadLWidget.h"

namespace mm
{
    mmLayerEmulatorPadLWidget::mmLayerEmulatorPadLWidget(void)
        : pPadWindow(NULL)
        , pNormalWindow(NULL)
        , pLustreWindow(NULL)

        , hNormalName("")
        , hLustreName("")
        , hBit(0)
    {

    }
    void mmLayerEmulatorPadLWidget::SetPadWindow(CEGUI::Window* pPadWindow)
    {
        this->pPadWindow = pPadWindow;
    }
    void mmLayerEmulatorPadLWidget::SetNormalName(const std::string& hNormalName)
    {
        this->hNormalName = hNormalName;
    }
    void mmLayerEmulatorPadLWidget::SetLustreName(const std::string& hLustreName)
    {
        this->hLustreName = hLustreName;
    }
    void mmLayerEmulatorPadLWidget::SetPadBit(mmWord_t hBit)
    {
        this->hBit = hBit;
    }
    void mmLayerEmulatorPadLWidget::OnFinishLaunching(void)
    {
        this->pNormalWindow = this->pPadWindow->getChild(this->hNormalName);
        this->pLustreWindow = this->pPadWindow->getChild(this->hLustreName);
    }
    void mmLayerEmulatorPadLWidget::OnBeforeTerminate(void)
    {
        this->pNormalWindow = NULL;
        this->pLustreWindow = NULL;
    }
    void mmLayerEmulatorPadLWidget::UpdateLight(mmWord_t v)
    {
        this->pNormalWindow->setVisible((0 == (v & this->hBit)));
        this->pLustreWindow->setVisible((0 != (v & this->hBit)));
    }
}

