/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerEmulatorPadL.h"
#include "mmLayerEmulatorTransform.h"

#include "CEGUI/WindowManager.h"

#include "layer/utility/mmLayerUtilityFinger.h"

#include "emulator/mmEmulatorMachine.h"

#include "math/mmMathConst.h"

static void __static_EmulatorPadL_RudderCallback(void* obj, mmWord_t v)
{
    mm::mmLayerEmulatorPadRudder* pPadRudder = (mm::mmLayerEmulatorPadRudder*)(obj);
    mm::mmLayerEmulatorPadL* p = (mm::mmLayerEmulatorPadL*)(pPadRudder->hCallback.obj);
    p->UpdateLight(v);
}

namespace mm
{
    const CEGUI::String mmLayerEmulatorPadL::EventNamespace = "mm";
    const CEGUI::String mmLayerEmulatorPadL::WidgetTypeName = "mm/mmLayerEmulatorPadL";

    mmLayerEmulatorPadL::mmLayerEmulatorPadL(const CEGUI::String& type, const CEGUI::String& name)
        : mmLayerContext(type, name)
        , LayerEmulatorPadL(NULL)
        , StaticImageBackground(NULL)

        , StaticImageRudder(NULL)
        , StaticImageInvalid(NULL)

        , pFingerTouch(NULL)

        , pEmulatorMachine(NULL)

        , hNormalName("StaticImageNormal")
        , hLustreName("StaticImageLustre")
    {
        mmRbtreeStringVpt_Init(&this->hEmuPadRbtree);
        mmLayerUtilityTransform_Init(&this->hEmulatorTransform);
    }
    mmLayerEmulatorPadL::~mmLayerEmulatorPadL()
    {
        mmLayerUtilityTransform_Destroy(&this->hEmulatorTransform);
        mmRbtreeStringVpt_Destroy(&this->hEmuPadRbtree);
    }
    void mmLayerEmulatorPadL::SetFingerTouch(mmLayerUtilityFingerTouch* pFingerTouch)
    {
        this->pFingerTouch = pFingerTouch;
    }
    void mmLayerEmulatorPadL::SetEmulatorMachine(struct mmEmulatorMachine* pEmulatorMachine)
    {
        this->pEmulatorMachine = pEmulatorMachine;
    }
    void mmLayerEmulatorPadL::SetNormalName(const std::string& hNormalName)
    {
        this->hNormalName = hNormalName;

        mmLayerEmulatorPadLWidget* u = NULL;
        struct mmRbNode* n = NULL;
        struct mmRbtreeStringVptIterator* it = NULL;
        //
        n = mmRb_First(&this->hEmuPadRbtree.rbt);
        while (NULL != n)
        {
            it = (struct mmRbtreeStringVptIterator*)mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
            n = mmRb_Next(n);
            // 
            u = (mmLayerEmulatorPadLWidget*)it->v;
            u->SetNormalName(this->hNormalName);
        }
    }
    void mmLayerEmulatorPadL::SetLustreName(const std::string& hLustreName)
    {
        this->hLustreName = hLustreName;

        mmLayerEmulatorPadLWidget* u = NULL;
        struct mmRbNode* n = NULL;
        struct mmRbtreeStringVptIterator* it = NULL;
        //
        n = mmRb_First(&this->hEmuPadRbtree.rbt);
        while (NULL != n)
        {
            it = (struct mmRbtreeStringVptIterator*)mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
            n = mmRb_Next(n);
            // 
            u = (mmLayerEmulatorPadLWidget*)it->v;
            u->SetLustreName(this->hLustreName);
        }
    }
    void mmLayerEmulatorPadL::SetInvalidRadiusK(double hInvalidRadiusXK, double hInvalidRadiusYK)
    {
        this->hPadRudder.SetInvalidRadiusK(hInvalidRadiusXK, hInvalidRadiusYK);
    }
    void mmLayerEmulatorPadL::AssemblyPad(const char* pPadWindowName, mmWord_t hBit)
    {
        mmLayerEmulatorPadLWidget* u = this->GetAssemblyPadWindowInstance(pPadWindowName);
        u->SetPadBit(hBit);
    }
    mmLayerEmulatorPadLWidget* mmLayerEmulatorPadL::AddAssemblyPadWindow(const char* pPadWindowName)
    {
        struct mmString hWeakValue;
        mmString_MakeWeak(&hWeakValue, pPadWindowName);

        mmLayerEmulatorPadLWidget* u = NULL;
        struct mmRbtreeStringVptIterator* it = NULL;
        it = mmRbtreeStringVpt_GetIterator(&this->hEmuPadRbtree, &hWeakValue);
        if (NULL == it)
        {
            u = new mmLayerEmulatorPadLWidget;
            u->SetNormalName(this->hNormalName);
            u->SetLustreName(this->hLustreName);
            mmRbtreeStringVpt_Set(&this->hEmuPadRbtree, &hWeakValue, u);
        }
        else
        {
            u = (mmLayerEmulatorPadLWidget*)it->v;
        }

        return u;
    }
    mmLayerEmulatorPadLWidget* mmLayerEmulatorPadL::GetAssemblyPadWindow(const char* pPadWindowName)
    {
        struct mmString hWeakValue;
        mmString_MakeWeak(&hWeakValue, pPadWindowName);

        mmLayerEmulatorPadLWidget* u = NULL;
        struct mmRbtreeStringVptIterator* it = NULL;
        it = mmRbtreeStringVpt_GetIterator(&this->hEmuPadRbtree, &hWeakValue);
        if (NULL != it)
        {
            u = (mmLayerEmulatorPadLWidget*)it->v;
        }
        return u;
    }
    mmLayerEmulatorPadLWidget* mmLayerEmulatorPadL::GetAssemblyPadWindowInstance(const char* pPadWindowName)
    {
        mmLayerEmulatorPadLWidget* u = this->GetAssemblyPadWindow(pPadWindowName);
        if (NULL == u)
        {
            u = this->AddAssemblyPadWindow(pPadWindowName);
        }
        return u;
    }
    void mmLayerEmulatorPadL::RmvAssemblyPadWindow(const char* pPadWindowName)
    {
        struct mmString hWeakValue;
        mmString_MakeWeak(&hWeakValue, pPadWindowName);

        mmLayerEmulatorPadLWidget* u = NULL;
        struct mmRbtreeStringVptIterator* it = NULL;
        it = mmRbtreeStringVpt_GetIterator(&this->hEmuPadRbtree, &hWeakValue);
        if (NULL != it)
        {
            u = (mmLayerEmulatorPadLWidget*)it->v;
            mmRbtreeStringVpt_Erase(&this->hEmuPadRbtree, it);
            delete u;
        }
    }
    void mmLayerEmulatorPadL::ClearAssemblyPadWindow(void)
    {
        mmLayerEmulatorPadLWidget* u = NULL;
        struct mmRbNode* n = NULL;
        struct mmRbtreeStringVptIterator* it = NULL;
        //
        n = mmRb_First(&this->hEmuPadRbtree.rbt);
        while (NULL != n)
        {
            it = (struct mmRbtreeStringVptIterator*)mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
            n = mmRb_Next(n);
            // 
            u = (mmLayerEmulatorPadLWidget*)it->v;
            mmRbtreeStringVpt_Erase(&this->hEmuPadRbtree, it);
            delete u;
        }
    }
    void mmLayerEmulatorPadL::UpdateTransform(void)
    {
        mmLayerUtilityTransform_UpdateTransform(&this->hEmulatorTransform, this);
    }
    void mmLayerEmulatorPadL::OnFinishLaunching(void)
    {
        struct mmSurfaceMaster* pSurfaceMaster = this->pSurfaceMaster;

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        this->LayerEmulatorPadL = pWindowManager->loadLayoutFromFile("emulator/LayerEmulatorPadL.layout");
        this->addChild(this->LayerEmulatorPadL);
        //
        // Manual set whole area.
        CEGUI::URect hWholeArea(CEGUI::UDim(0, 0), CEGUI::UDim(0, 0), CEGUI::UDim(1, 0), CEGUI::UDim(1, 0));
        this->LayerEmulatorPadL->setArea(hWholeArea);
        //
        this->StaticImageBackground = this->LayerEmulatorPadL->getChild("StaticImageBackground");
        //
        this->StaticImageRudder = this->StaticImageBackground->getChild("StaticImageRudder");
        this->StaticImageInvalid = this->StaticImageBackground->getChild("StaticImageInvalid");

        this->UpdateTransform();

        double hRadiusKX = 0;
        double hRadiusKY = 0;
        float hWindowSizeR[2] = { 0 };
        float hWindowSizeI[2] = { 0 };
        mmWidgetTransform_WindowPixelSize(this->StaticImageRudder, hWindowSizeR);
        mmWidgetTransform_WindowPixelSize(this->StaticImageInvalid, hWindowSizeI);
        hRadiusKX = hWindowSizeI[0] / hWindowSizeR[0];
        hRadiusKY = hWindowSizeI[1] / hWindowSizeR[1];

        this->AssemblyPad("DefaultWindowU", 1 << MM_EMU_PAD_JOYPAD_ARROW_U);
        this->AssemblyPad("DefaultWindowD", 1 << MM_EMU_PAD_JOYPAD_ARROW_D);
        this->AssemblyPad("DefaultWindowL", 1 << MM_EMU_PAD_JOYPAD_ARROW_L);
        this->AssemblyPad("DefaultWindowR", 1 << MM_EMU_PAD_JOYPAD_ARROW_R);
        //
        this->SetInvalidRadiusK(hRadiusKX, hRadiusKY);

        this->FinishLaunchingWidget();

        struct mmLayerEmulatorPadRudderCallback hRudderCallback;
        hRudderCallback.UpdateLight = &__static_EmulatorPadL_RudderCallback;
        hRudderCallback.obj = this;
        this->hPadRudder.SetLayerContext(this);
        this->hPadRudder.SetFingerTouch(this->pFingerTouch);
        this->hPadRudder.SetPadWindow(this->StaticImageRudder);
        this->hPadRudder.SetEmulatorMachine(this->pEmulatorMachine);
        this->hPadRudder.SetCallback(&hRudderCallback);
        this->hPadRudder.OnFinishLaunching();
    }
    void mmLayerEmulatorPadL::OnBeforeTerminate(void)
    {
        this->hPadRudder.OnBeforeTerminate();

        this->BeforeTerminateWidget();

        this->ClearAssemblyPadWindow();

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        this->removeChild(this->LayerEmulatorPadL);
        pWindowManager->destroyWindow(this->LayerEmulatorPadL);
        this->LayerEmulatorPadL = NULL;
    }
    void mmLayerEmulatorPadL::OnEventWindowSizeChanged(struct mmSurfaceContentSizeChange* pContent)
    {
        this->UpdateTransform();
        this->hPadRudder.OnEventWindowSizeChanged(pContent);
    }
    void mmLayerEmulatorPadL::OnEventTouchsMoved(struct mmSurfaceContentTouchs* pContent)
    {
        this->hPadRudder.OnEventTouchsMoved(pContent);
    }
    void mmLayerEmulatorPadL::OnEventTouchsBegan(struct mmSurfaceContentTouchs* pContent)
    {
        this->hPadRudder.OnEventTouchsBegan(pContent);
    }
    void mmLayerEmulatorPadL::OnEventTouchsEnded(struct mmSurfaceContentTouchs* pContent)
    {
        this->hPadRudder.OnEventTouchsEnded(pContent);
    }
    void mmLayerEmulatorPadL::OnEventTouchsBreak(struct mmSurfaceContentTouchs* pContent)
    {
        this->hPadRudder.OnEventTouchsBreak(pContent);
    }
    void mmLayerEmulatorPadL::FinishLaunchingWidget()
    {
        CEGUI::Window* pPadWindow = NULL;

        mmLayerEmulatorPadLWidget* u = NULL;
        struct mmRbNode* n = NULL;
        struct mmRbtreeStringVptIterator* it = NULL;
        //
        n = mmRb_First(&this->hEmuPadRbtree.rbt);
        while (NULL != n)
        {
            it = (struct mmRbtreeStringVptIterator*)mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
            n = mmRb_Next(n);
            // 
            u = (mmLayerEmulatorPadLWidget*)it->v;

            pPadWindow = this->StaticImageBackground->getChild(mmString_CStr(&it->k));

            u->SetPadWindow(pPadWindow);
            u->OnFinishLaunching();
        }
    }
    void mmLayerEmulatorPadL::BeforeTerminateWidget()
    {
        mmLayerEmulatorPadLWidget* u = NULL;
        struct mmRbNode* n = NULL;
        struct mmRbtreeStringVptIterator* it = NULL;
        //
        n = mmRb_First(&this->hEmuPadRbtree.rbt);
        while (NULL != n)
        {
            it = (struct mmRbtreeStringVptIterator*)mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
            n = mmRb_Next(n);
            // 
            u = (mmLayerEmulatorPadLWidget*)it->v;

            u->OnBeforeTerminate();
        }
    }
    void mmLayerEmulatorPadL::UpdateLight(mmWord_t v)
    {
        mmLayerEmulatorPadLWidget* u = NULL;
        struct mmRbNode* n = NULL;
        struct mmRbtreeStringVptIterator* it = NULL;
        //
        n = mmRb_First(&this->hEmuPadRbtree.rbt);
        while (NULL != n)
        {
            it = (struct mmRbtreeStringVptIterator*)mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
            n = mmRb_Next(n);
            // 
            u = (mmLayerEmulatorPadLWidget*)it->v;

            u->UpdateLight(v);
        }

        this->StaticImageBackground->invalidate();
    }
}

