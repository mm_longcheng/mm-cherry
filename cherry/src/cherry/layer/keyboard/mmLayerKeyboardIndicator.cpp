/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerKeyboardIndicator.h"
#include "mmLayerKeyboardMenu.h"

#include "core/mmKeyCode.h"

#include "CEGUI/WindowManager.h"

#include "nwsi/mmEventSurface.h"

#include "layer/director/mmLayerDirector.h"

namespace mm
{
    mmLayerKeyboardIndicatorLight::mmLayerKeyboardIndicatorLight(void)
        : pLight(NULL)

        , hColourSelect(0.0f, 0.0f, 1.0f, 1.0f)
        , hColourCommon(1.0f, 1.0f, 1.0f, 1.0f)

        , hIlluminate(false)

        , hKeycode(0)
    {

    }
    mmLayerKeyboardIndicatorLight::~mmLayerKeyboardIndicatorLight(void)
    {

    }
    void mmLayerKeyboardIndicatorLight::SetWindowLight(CEGUI::Window* pWindowLight)
    {
        this->pLight = pWindowLight;
    }
    void mmLayerKeyboardIndicatorLight::SetKeycode(mmUInt32_t hKeycode)
    {
        this->hKeycode = hKeycode;
    }
    void mmLayerKeyboardIndicatorLight::OnFinishLaunching(void)
    {
        // do nothing here.
    }
    void mmLayerKeyboardIndicatorLight::OnBeforeTerminate(void)
    {
        // do nothing here.
    }
    void mmLayerKeyboardIndicatorLight::OnEventItemRelease(mmUInt32_t hKeycode)
    {
        // do nothing here.
    }
    void mmLayerKeyboardIndicatorLight::OnEventItemPressed(mmUInt32_t hKeycode)
    {
        if (hKeycode == this->hKeycode)
        {
            CEGUI::Colour hColour;
            CEGUI::String hColourString;

            this->hIlluminate = !this->hIlluminate;

            hColour = this->hIlluminate ? this->hColourSelect : this->hColourCommon;
            hColourString = CEGUI::PropertyHelper<CEGUI::Colour>::toString(hColour);

            this->pLight->setProperty("ImageColours", hColourString);
        }
    }

    const CEGUI::String mmLayerKeyboardIndicator::EventNamespace = "mm";
    const CEGUI::String mmLayerKeyboardIndicator::WidgetTypeName = "mm/mmLayerKeyboardIndicator";

    mmLayerKeyboardIndicator::mmLayerKeyboardIndicator(const CEGUI::String& type, const CEGUI::String& name)
        : mmLayerContext(type, name)
        , LayerKeyboardIndicator(NULL)
        , StaticImageBackground(NULL)

        , StaticImage_LIGHT_CAPS(NULL)
        , StaticImage_LIGHT_NUM(NULL)
        , StaticImage_LIGHT_SCROLL(NULL)
    {
        
    }
    mmLayerKeyboardIndicator::~mmLayerKeyboardIndicator(void)
    {
        
    }

    void mmLayerKeyboardIndicator::OnFinishLaunching(void)
    {
        struct mmSurfaceMaster* pSurfaceMaster = this->pSurfaceMaster;

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        this->LayerKeyboardIndicator = pWindowManager->loadLayoutFromFile("keyboard/LayerKeyboardIndicator.layout");
        this->addChild(this->LayerKeyboardIndicator);
        // Manual set whole area.
        CEGUI::URect hWholeArea(CEGUI::UDim(0, 0), CEGUI::UDim(0, 0), CEGUI::UDim(1, 0), CEGUI::UDim(1, 0));
        this->LayerKeyboardIndicator->setArea(hWholeArea);

        this->StaticImageBackground = this->LayerKeyboardIndicator->getChild("StaticImageBackground");

        this->StaticImage_LIGHT_CAPS = this->StaticImageBackground->getChild("StaticImage_LIGHT_CAPS");
        this->StaticImage_LIGHT_NUM = this->StaticImageBackground->getChild("StaticImage_LIGHT_NUM");
        this->StaticImage_LIGHT_SCROLL = this->StaticImageBackground->getChild("StaticImage_LIGHT_SCROLL");

        this->hLightCaps.SetKeycode(MM_KC_CAPITAL);
        this->hLightCaps.SetWindowLight(this->StaticImage_LIGHT_CAPS);
        this->hLightCaps.OnFinishLaunching();

        this->hLightNum.SetKeycode(MM_KC_NUMLOCK);
        this->hLightNum.SetWindowLight(this->StaticImage_LIGHT_NUM);
        this->hLightNum.OnFinishLaunching();

        this->hLightScroll.SetKeycode(MM_KC_SCROLL);
        this->hLightScroll.SetWindowLight(this->StaticImage_LIGHT_SCROLL);
        this->hLightScroll.OnFinishLaunching();

        mm::mmEventSet* pEventSet = &pSurfaceMaster->hEventSet;
        pEventSet->SubscribeEvent(mmSurfaceMaster_EventKeypadPressed, &mmLayerKeyboardIndicator::OnEventKeypadPressed, this);
        pEventSet->SubscribeEvent(mmSurfaceMaster_EventKeypadRelease, &mmLayerKeyboardIndicator::OnEventKeypadRelease, this);
    }

    void mmLayerKeyboardIndicator::OnBeforeTerminate()
    {
        struct mmSurfaceMaster* pSurfaceMaster = this->pSurfaceMaster;

        mm::mmEventSet* pEventSet = &pSurfaceMaster->hEventSet;
        pEventSet->UnsubscribeEvent(mmSurfaceMaster_EventKeypadPressed, &mmLayerKeyboardIndicator::OnEventKeypadPressed, this);
        pEventSet->UnsubscribeEvent(mmSurfaceMaster_EventKeypadRelease, &mmLayerKeyboardIndicator::OnEventKeypadRelease, this);

        this->hLightCaps.OnBeforeTerminate();
        this->hLightNum.OnBeforeTerminate();
        this->hLightScroll.OnBeforeTerminate();

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        pWindowManager->destroyWindow(this->LayerKeyboardIndicator);
        this->LayerKeyboardIndicator = NULL;
    }
    void mmLayerKeyboardIndicator::OnEventItemRelease(mmUInt32_t hKeycode)
    {
        this->hLightCaps.OnEventItemRelease(hKeycode);
        this->hLightNum.OnEventItemRelease(hKeycode);
        this->hLightScroll.OnEventItemRelease(hKeycode);
    }
    void mmLayerKeyboardIndicator::OnEventItemPressed(mmUInt32_t hKeycode)
    {
        this->hLightCaps.OnEventItemPressed(hKeycode);
        this->hLightNum.OnEventItemPressed(hKeycode);
        this->hLightScroll.OnEventItemPressed(hKeycode);
    }
    bool mmLayerKeyboardIndicator::OnEventKeypadRelease(const mmEventArgs& args)
    {
        const mm::mmEventKeypad& evt = (const mm::mmEventKeypad&)(args);

        this->OnEventItemRelease(evt.content->key);

        return false;
    }
    bool mmLayerKeyboardIndicator::OnEventKeypadPressed(const mmEventArgs& args)
    {
        const mm::mmEventKeypad& evt = (const mm::mmEventKeypad&)(args);

        this->OnEventItemPressed(evt.content->key);

        return false;
    }
}
