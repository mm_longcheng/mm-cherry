/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerKeyboardFunction.h"

#include "CEGUI/WindowManager.h"

#include "layer/director/mmLayerDirector.h"

namespace mm
{
    const CEGUI::String mmLayerKeyboardFunction::EventNamespace = "mm";
    const CEGUI::String mmLayerKeyboardFunction::WidgetTypeName = "mm/mmLayerKeyboardFunction";

    const std::string mmLayerKeyboardFunction::EventKeyboardItemRelease("EventKeyboardItemRelease");
    const std::string mmLayerKeyboardFunction::EventKeyboardItemPressed("EventKeyboardItemPressed");

    mmLayerKeyboardFunction::mmLayerKeyboardFunction(const CEGUI::String& type, const CEGUI::String& name)
        : mmLayerContext(type, name)
        , hEventSet()
        , LayerKeyboardFunction(NULL)
        , StaticImageBackground(NULL)

        , pFingerTouch(NULL)
    {
        
    }
    mmLayerKeyboardFunction::~mmLayerKeyboardFunction()
    {
        
    }
    void mmLayerKeyboardFunction::SetFingerTouch(mmLayerUtilityFingerTouch* pFingerTouch)
    {
        this->pFingerTouch = pFingerTouch;
        this->hKeyboardMenu.SetFingerTouch(this->pFingerTouch);
    }
    void mmLayerKeyboardFunction::OnFinishLaunching()
    {
        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        this->LayerKeyboardFunction = pWindowManager->loadLayoutFromFile("keyboard/LayerKeyboardFunction.layout");
        this->addChild(this->LayerKeyboardFunction);
        // Manual set whole area.
        CEGUI::URect hWholeArea(CEGUI::UDim(0, 0), CEGUI::UDim(0, 0), CEGUI::UDim(1, 0), CEGUI::UDim(1, 0));
        this->LayerKeyboardFunction->setArea(hWholeArea);

        this->StaticImageBackground = this->LayerKeyboardFunction->getChild("StaticImageBackground");

        this->hKeyboardMenu.SetWindowMenu(this->StaticImageBackground);
        this->hKeyboardMenu.OnFinishLaunching();

        this->hKeyboardMenu.hEventSet.SubscribeEvent(mmLayerKeyboardMenu::EventKeyboardItemRelease, &mmLayerKeyboardFunction::OnEventKeyboardItemRelease, this);
        this->hKeyboardMenu.hEventSet.SubscribeEvent(mmLayerKeyboardMenu::EventKeyboardItemPressed, &mmLayerKeyboardFunction::OnEventKeyboardItemPressed, this);
    }

    void mmLayerKeyboardFunction::OnBeforeTerminate()
    {
        this->hKeyboardMenu.hEventSet.UnsubscribeEvent(mmLayerKeyboardMenu::EventKeyboardItemRelease, &mmLayerKeyboardFunction::OnEventKeyboardItemRelease, this);
        this->hKeyboardMenu.hEventSet.UnsubscribeEvent(mmLayerKeyboardMenu::EventKeyboardItemPressed, &mmLayerKeyboardFunction::OnEventKeyboardItemPressed, this);

        this->hKeyboardMenu.OnBeforeTerminate();

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        pWindowManager->destroyWindow(this->LayerKeyboardFunction);
        this->LayerKeyboardFunction = NULL;
    }
    void mmLayerKeyboardFunction::UpdateTransform(void)
    {
        this->hKeyboardMenu.UpdateTransform();
    }
    void mmLayerKeyboardFunction::OnEventWindowSizeChanged(struct mmSurfaceContentSizeChange* pContent)
    {
        this->hKeyboardMenu.OnEventWindowSizeChanged(pContent);
    }
    void mmLayerKeyboardFunction::OnEventTouchsMoved(struct mmSurfaceContentTouchs* pContent)
    {
        this->hKeyboardMenu.OnEventTouchsMoved(pContent);
    }
    void mmLayerKeyboardFunction::OnEventTouchsBegan(struct mmSurfaceContentTouchs* pContent)
    {
        this->hKeyboardMenu.OnEventTouchsBegan(pContent);
    }
    void mmLayerKeyboardFunction::OnEventTouchsEnded(struct mmSurfaceContentTouchs* pContent)
    {
        this->hKeyboardMenu.OnEventTouchsEnded(pContent);
    }
    void mmLayerKeyboardFunction::OnEventTouchsBreak(struct mmSurfaceContentTouchs* pContent)
    {
        this->hKeyboardMenu.OnEventTouchsBreak(pContent);
    }
    bool mmLayerKeyboardFunction::OnEventKeyboardItemRelease(const mmEventArgs& args)
    {
        this->hEventSet.FireEvent(EventKeyboardItemRelease, (mmEventArgs&)args);
        return true;
    }
    bool mmLayerKeyboardFunction::OnEventKeyboardItemPressed(const mmEventArgs& args)
    {
        this->hEventSet.FireEvent(EventKeyboardItemPressed, (mmEventArgs&)args);
        return true;
    }
}
