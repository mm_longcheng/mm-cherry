/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmLayerKeyboardItemWidget_h__
#define __mmLayerKeyboardItemWidget_h__

#include "CEGUI/Window.h"

#include "layer/utility/mmLayerUtilityTransform.h"

#include "dish/mmEvent.h"

struct mmEmulatorMachine;

namespace mm
{
    /*!
    \brief
    EventArgs based class that is used for objects passed to input event handlers
    concerning layer keyboard item event.
    */
    class mmLayerKeyboardItemEventArgs : public mm::mmEventArgs
    {
    public:
        mmLayerKeyboardItemEventArgs(CEGUI::Window* wnd, mmUInt32_t _keycode)
            : window(wnd)
            , keycode(_keycode)
        {

        }

        CEGUI::Window* window;//!< holds CEGUI::Window.

        mmUInt32_t keycode;   //!< holds current keycode information.
    };

    class mmLayerUtilityFinger;

    class mmLayerKeyboardItemWidget
    {
    public:
        static const CEGUI::String EventNamespace;          //!< Namespace for global events
    public:
        static const std::string EventKeyboardItemRelease;
        static const std::string EventKeyboardItemPressed;
    public:
        mm::mmEventSet hEventSet;
    public:
        CEGUI::Window* pItemWindow;
    public:
        CEGUI::Colour hColourSelect;
        CEGUI::Colour hColourCommon;
        //
        int hSelect;
    public:
        mmLayerUtilityTransform hTransform;
    public:
        mmLayerKeyboardItemWidget(void);
    public:
        void SetItemWindow(CEGUI::Window* pItemWindow);
    public:
        void UpdateTransform(void);
    public:
        void OnFinishLaunching(void);
        void OnBeforeTerminate(void);
    public:
        void OnEventWindowSizeChanged(struct mmSurfaceContentSizeChange* pContent);
    public:
        void UpdateSelect(int hSelect);
        void UpdateSelectColour(int hSelect);
    public:
        bool IntersectFinger(mmLayerUtilityFinger* pFinger, Ogre::Vector3& hCenterToTouch);
    };
}

#endif//__mmLayerKeyboardItemWidget_h__

