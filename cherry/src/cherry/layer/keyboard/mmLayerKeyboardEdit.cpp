/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerKeyboardEdit.h"

#include "CEGUI/WindowManager.h"

#include "layer/director/mmLayerDirector.h"

namespace mm
{
    const CEGUI::String mmLayerKeyboardEdit::EventNamespace = "mm";
    const CEGUI::String mmLayerKeyboardEdit::WidgetTypeName = "mm/mmLayerKeyboardEdit";

    const std::string mmLayerKeyboardEdit::EventKeyboardItemRelease("EventKeyboardItemRelease");
    const std::string mmLayerKeyboardEdit::EventKeyboardItemPressed("EventKeyboardItemPressed");

    mmLayerKeyboardEdit::mmLayerKeyboardEdit(const CEGUI::String& type, const CEGUI::String& name)
        : mmLayerContext(type, name)
        , hEventSet()
        , LayerKeyboardEdit(NULL)
        , StaticImageBackground(NULL)

        , pFingerTouch(NULL)
    {
        
    }
    mmLayerKeyboardEdit::~mmLayerKeyboardEdit(void)
    {
        
    }
    void mmLayerKeyboardEdit::SetFingerTouch(mmLayerUtilityFingerTouch* pFingerTouch)
    {
        this->pFingerTouch = pFingerTouch;
        this->hKeyboardMenu.SetFingerTouch(this->pFingerTouch);
    }
    void mmLayerKeyboardEdit::OnFinishLaunching(void)
    {
        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        this->LayerKeyboardEdit = pWindowManager->loadLayoutFromFile("keyboard/LayerKeyboardEdit.layout");
        this->addChild(this->LayerKeyboardEdit);
        // Manual set whole area.
        CEGUI::URect hWholeArea(CEGUI::UDim(0, 0), CEGUI::UDim(0, 0), CEGUI::UDim(1, 0), CEGUI::UDim(1, 0));
        this->LayerKeyboardEdit->setArea(hWholeArea);

        this->StaticImageBackground = this->LayerKeyboardEdit->getChild("StaticImageBackground");

        this->hKeyboardMenu.SetWindowMenu(this->StaticImageBackground);
        this->hKeyboardMenu.OnFinishLaunching();

        this->hKeyboardMenu.hEventSet.SubscribeEvent(mmLayerKeyboardMenu::EventKeyboardItemRelease, &mmLayerKeyboardEdit::OnEventKeyboardItemRelease, this);
        this->hKeyboardMenu.hEventSet.SubscribeEvent(mmLayerKeyboardMenu::EventKeyboardItemPressed, &mmLayerKeyboardEdit::OnEventKeyboardItemPressed, this);
    }

    void mmLayerKeyboardEdit::OnBeforeTerminate(void)
    {
        this->hKeyboardMenu.hEventSet.UnsubscribeEvent(mmLayerKeyboardMenu::EventKeyboardItemRelease, &mmLayerKeyboardEdit::OnEventKeyboardItemRelease, this);
        this->hKeyboardMenu.hEventSet.UnsubscribeEvent(mmLayerKeyboardMenu::EventKeyboardItemPressed, &mmLayerKeyboardEdit::OnEventKeyboardItemPressed, this);

        this->hKeyboardMenu.OnBeforeTerminate();

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        pWindowManager->destroyWindow(this->LayerKeyboardEdit);
        this->LayerKeyboardEdit = NULL;
    }
    void mmLayerKeyboardEdit::UpdateTransform(void)
    {
        this->hKeyboardMenu.UpdateTransform();
    }
    void mmLayerKeyboardEdit::OnEventWindowSizeChanged(struct mmSurfaceContentSizeChange* pContent)
    {
        this->hKeyboardMenu.OnEventWindowSizeChanged(pContent);
    }
    void mmLayerKeyboardEdit::OnEventTouchsMoved(struct mmSurfaceContentTouchs* pContent)
    {
        this->hKeyboardMenu.OnEventTouchsMoved(pContent);
    }
    void mmLayerKeyboardEdit::OnEventTouchsBegan(struct mmSurfaceContentTouchs* pContent)
    {
        this->hKeyboardMenu.OnEventTouchsBegan(pContent);
    }
    void mmLayerKeyboardEdit::OnEventTouchsEnded(struct mmSurfaceContentTouchs* pContent)
    {
        this->hKeyboardMenu.OnEventTouchsEnded(pContent);
    }
    void mmLayerKeyboardEdit::OnEventTouchsBreak(struct mmSurfaceContentTouchs* pContent)
    {
        this->hKeyboardMenu.OnEventTouchsBreak(pContent);
    }
    bool mmLayerKeyboardEdit::OnEventKeyboardItemRelease(const mmEventArgs& args)
    {
        this->hEventSet.FireEvent(EventKeyboardItemRelease, (mmEventArgs&)args);
        return true;
    }
    bool mmLayerKeyboardEdit::OnEventKeyboardItemPressed(const mmEventArgs& args)
    {
        this->hEventSet.FireEvent(EventKeyboardItemPressed, (mmEventArgs&)args);
        return true;
    }
}
