/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmLayerKeyboardIndicator_h__
#define __mmLayerKeyboardIndicator_h__

#include "core/mmCore.h"

#include "dish/mmEvent.h"

#include "layer/director/mmLayerContext.h"

#include "CEGUI/Window.h"

namespace mm
{
    class mmLayerKeyboardIndicatorLight
    {
    public:
        CEGUI::Window* pLight;
    public:
        CEGUI::Colour hColourSelect;
        CEGUI::Colour hColourCommon;
    public:
        bool hIlluminate;
    public:
        mmUInt32_t hKeycode;
    public:
        mmLayerKeyboardIndicatorLight(void);
        ~mmLayerKeyboardIndicatorLight(void);
    public:
        void SetWindowLight(CEGUI::Window* pWindowLight);
        void SetKeycode(mmUInt32_t hKeycode);
    public:
        void OnFinishLaunching(void);
        void OnBeforeTerminate(void);
    public:
        void OnEventItemRelease(mmUInt32_t hKeycode);
        void OnEventItemPressed(mmUInt32_t hKeycode);
    };
    class mmLayerKeyboardIndicator : public mmLayerContext
    {
    public:
        static const CEGUI::String EventNamespace;          //!< Namespace for global events
        static const CEGUI::String WidgetTypeName;          //!< Window factory name
    public:
        CEGUI::Window* LayerKeyboardIndicator;
        CEGUI::Window* StaticImageBackground;

        CEGUI::Window* StaticImage_LIGHT_CAPS;
        CEGUI::Window* StaticImage_LIGHT_NUM;
        CEGUI::Window* StaticImage_LIGHT_SCROLL;
    public:
        mmLayerKeyboardIndicatorLight hLightCaps;
        mmLayerKeyboardIndicatorLight hLightNum;
        mmLayerKeyboardIndicatorLight hLightScroll;
    public:
        mmLayerKeyboardIndicator(const CEGUI::String& type, const CEGUI::String& name);
        virtual ~mmLayerKeyboardIndicator(void);
    public:
        // mmLayerContext
        virtual void OnFinishLaunching(void);
        virtual void OnBeforeTerminate(void);
    public:
        void OnEventItemRelease(mmUInt32_t hKeycode);
        void OnEventItemPressed(mmUInt32_t hKeycode);
    private:
        bool OnEventKeypadRelease(const mmEventArgs& args);
        bool OnEventKeypadPressed(const mmEventArgs& args);
    };
}

#endif//__mmLayerKeyboardIndicator_h__