/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerKeyboardBracket.h"
#include "mmLayerKeyboardComplete.h"
#include "mmLayerKeyboardMenu.h"

#include "CEGUI/WindowManager.h"

#include "layer/director/mmLayerDirector.h"

namespace mm
{
    const CEGUI::String mmLayerKeyboardBracket::EventNamespace = "mm";
    const CEGUI::String mmLayerKeyboardBracket::WidgetTypeName = "mm/mmLayerKeyboardBracket";

    const std::string mmLayerKeyboardBracket::EventKeyboardItemRelease("EventKeyboardItemRelease");
    const std::string mmLayerKeyboardBracket::EventKeyboardItemPressed("EventKeyboardItemPressed");

    mmLayerKeyboardBracket::mmLayerKeyboardBracket(const CEGUI::String& type, const CEGUI::String& name)
        : mmLayerContext(type, name)
        , hEventSet()
        , LayerKeyboardBracket(NULL)

        , LayerKeyboardComplete(NULL)

        , pKeyboardComplete(NULL)
    {
        
    }
    mmLayerKeyboardBracket::~mmLayerKeyboardBracket(void)
    {
        
    }
    void mmLayerKeyboardBracket::OnFinishLaunching(void)
    {
        struct mmSurfaceMaster* pSurfaceMaster = this->pSurfaceMaster;

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        this->LayerKeyboardBracket = pWindowManager->loadLayoutFromFile("keyboard/LayerKeyboardBracket.layout");
        this->addChild(this->LayerKeyboardBracket);

        this->LayerKeyboardComplete = this->LayerKeyboardBracket->getChild("LayerKeyboardComplete");

        this->pKeyboardComplete = (mmLayerKeyboardComplete*)this->LayerKeyboardComplete;

        this->pKeyboardComplete->SetDirector(this->pDirector);
        this->pKeyboardComplete->SetContextMaster(this->pContextMaster);
        this->pKeyboardComplete->SetSurfaceMaster(this->pSurfaceMaster);
        this->pKeyboardComplete->OnFinishLaunching();

        // spacing common CEGUI::Window object, we need manual attach context event.
        this->AttachContextEvent(this->pKeyboardComplete);

        this->setMousePassThroughEnabled(true);
        this->setAlwaysOnTop(true);

        this->pKeyboardComplete->hEventSet.SubscribeEvent(mmLayerKeyboardComplete::EventKeyboardItemRelease, &mmLayerKeyboardBracket::OnEventKeyboardItemRelease, this);
        this->pKeyboardComplete->hEventSet.SubscribeEvent(mmLayerKeyboardComplete::EventKeyboardItemPressed, &mmLayerKeyboardBracket::OnEventKeyboardItemPressed, this);
    }

    void mmLayerKeyboardBracket::OnBeforeTerminate(void)
    {
        struct mmSurfaceMaster* pSurfaceMaster = this->pSurfaceMaster;

        this->pKeyboardComplete->hEventSet.UnsubscribeEvent(mmLayerKeyboardComplete::EventKeyboardItemRelease, &mmLayerKeyboardBracket::OnEventKeyboardItemRelease, this);
        this->pKeyboardComplete->hEventSet.UnsubscribeEvent(mmLayerKeyboardComplete::EventKeyboardItemPressed, &mmLayerKeyboardBracket::OnEventKeyboardItemPressed, this);

        // spacing common CEGUI::Window object, we need manual attach context event.
        this->DetachContextEvent(this->pKeyboardComplete);

        this->pKeyboardComplete->OnBeforeTerminate();

        this->pKeyboardComplete = NULL;

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        pWindowManager->destroyWindow(this->LayerKeyboardBracket);
        this->LayerKeyboardBracket = NULL;
    }
    bool mmLayerKeyboardBracket::OnEventKeyboardItemRelease(const mmEventArgs& args)
    {
        this->hEventSet.FireEvent(EventKeyboardItemRelease, (mmEventArgs&)args);
        return true;
    }
    bool mmLayerKeyboardBracket::OnEventKeyboardItemPressed(const mmEventArgs& args)
    {
        this->hEventSet.FireEvent(EventKeyboardItemPressed, (mmEventArgs&)args);
        return true;
    }
}
