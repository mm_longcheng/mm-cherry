/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmLayerKeyboardComplete_h__
#define __mmLayerKeyboardComplete_h__

#include "core/mmCore.h"

#include "dish/mmEvent.h"

#include "layer/director/mmLayerContext.h"

#include "CEGUI/Window.h"

#include "layer/utility/mmLayerUtilityFinger.h"

namespace mm
{
    class mmLayerUtilityFingerTouch;

    class mmLayerKeyboardCharacter;
    class mmLayerKeyboardEdit;
    class mmLayerKeyboardFunction;
    class mmLayerKeyboardNumeric;
    class mmLayerKeyboardIndicator;

    class mmLayerKeyboardComplete : public mmLayerContext
    {
    public:
        static const CEGUI::String EventNamespace;          //!< Namespace for global events
        static const CEGUI::String WidgetTypeName;          //!< Window factory name
    public:
        static const std::string EventKeyboardItemRelease;
        static const std::string EventKeyboardItemPressed;
    public:
        mm::mmEventSet hEventSet;
    public:
        CEGUI::Window* LayerKeyboardComplete;
        CEGUI::Window* StaticImageBackground;

        CEGUI::Window* LayerKeyboardCharacter;
        CEGUI::Window* LayerKeyboardEdit;
        CEGUI::Window* LayerKeyboardFunction;
        CEGUI::Window* LayerKeyboardNumeric;
        CEGUI::Window* LayerKeyboardIndicator;
        // 
        mmLayerKeyboardCharacter* pCharacter;
        mmLayerKeyboardEdit* pEdit;
        mmLayerKeyboardFunction* pFunction;
        mmLayerKeyboardNumeric* pNumeric;
        mmLayerKeyboardIndicator* pIndicator;
    public:
        mmLayerUtilityFingerTouch hFingerTouch;
    public:
        CEGUI::Event::Connection hLayerEventTouchsMovedConn;
        CEGUI::Event::Connection hLayerEventTouchsBeganConn;
        CEGUI::Event::Connection hLayerEventTouchsEndedConn;
        CEGUI::Event::Connection hLayerEventTouchsBreakConn;
        //
        CEGUI::Event::Connection hLayerEventUpdatedConn;
    public:
        mmLayerKeyboardComplete(const CEGUI::String& type, const CEGUI::String& name);
        virtual ~mmLayerKeyboardComplete(void);
    public:
        // mmLayerContext
        virtual void OnFinishLaunching(void);
        virtual void OnBeforeTerminate(void);
    public:
        void UpdateTransform(void);
    public:
        // CEGUI::Window
        virtual void notifyScreenAreaChanged(bool recursive = true);
    private:
        bool OnLayerEventTouchsMoved(const CEGUI::EventArgs& args);
        bool OnLayerEventTouchsBegan(const CEGUI::EventArgs& args);
        bool OnLayerEventTouchsEnded(const CEGUI::EventArgs& args);
        bool OnLayerEventTouchsBreak(const CEGUI::EventArgs& args);
    private:
        bool OnLayerEventUpdated(const CEGUI::EventArgs& args);
    private:
        bool OnEventKeyboardItemRelease(const mmEventArgs& args);
        bool OnEventKeyboardItemPressed(const mmEventArgs& args);
    };
}

#endif//__mmLayerKeyboardComplete_h__