/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerKeyboardItemWidget.h"

#include "layer/utility/mmLayerUtilityFinger.h"
#include "layer/utility/mmLayerUtilityIntersect.h"

#include "emulator/mmEmulatorMachine.h"

namespace mm
{
    const CEGUI::String mmLayerKeyboardItemWidget::EventNamespace("mm");

    const std::string mmLayerKeyboardItemWidget::EventKeyboardItemRelease("EventKeyboardItemRelease");
    const std::string mmLayerKeyboardItemWidget::EventKeyboardItemPressed("EventKeyboardItemPressed");

    mmLayerKeyboardItemWidget::mmLayerKeyboardItemWidget(void)
        : hEventSet()
        , pItemWindow(NULL)

        , hColourSelect(0.0f, 0.0f, 1.0f, 1.0f)
        , hColourCommon(1.0f, 1.0f, 1.0f, 1.0f)

        , hSelect(0)
    {

    }
    void mmLayerKeyboardItemWidget::SetItemWindow(CEGUI::Window* pItemWindow)
    {
        this->pItemWindow = pItemWindow;
    }
    void mmLayerKeyboardItemWidget::UpdateTransform(void)
    {
        mmLayerUtilityTransform_UpdateTransform(&this->hTransform, this->pItemWindow);
    }
    void mmLayerKeyboardItemWidget::OnFinishLaunching(void)
    {
        this->UpdateTransform();
    }
    void mmLayerKeyboardItemWidget::OnBeforeTerminate(void)
    {

    }
    void mmLayerKeyboardItemWidget::OnEventWindowSizeChanged(struct mmSurfaceContentSizeChange* pContent)
    {
        this->UpdateTransform();
    }
    void mmLayerKeyboardItemWidget::UpdateSelect(int hSelect)
    {
        if (this->hSelect != hSelect)
        {
            mmUInt32_t hKeycode = this->pItemWindow->getID();
            mmLayerKeyboardItemEventArgs hArgs(this->pItemWindow, hKeycode);
            std::string hEventName = (0 == hSelect) ? EventKeyboardItemRelease : EventKeyboardItemPressed;
            this->hEventSet.FireEvent(hEventName, hArgs);

            this->hSelect = hSelect;
            this->UpdateSelectColour(hSelect);
        }
    }
    void mmLayerKeyboardItemWidget::UpdateSelectColour(int hSelect)
    {
        CEGUI::Colour hColour;
        CEGUI::String hColourString;

        hColour = (1 == hSelect) ? this->hColourSelect : this->hColourCommon;
        hColourString = CEGUI::PropertyHelper<CEGUI::Colour>::toString(hColour);

        this->pItemWindow->setProperty("ImageColours", hColourString);
    }
    bool mmLayerKeyboardItemWidget::IntersectFinger(mmLayerUtilityFinger* pFinger, Ogre::Vector3& hCenterToTouch)
    {
        return mmLayerUtilityIntersect_FingerPointRectangle(&this->hTransform, pFinger, hCenterToTouch);
    }
}
