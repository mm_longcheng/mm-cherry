/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerKeyboardMenu.h"
#include "mmLayerKeyboardItemWidget.h"

#include "layer/utility/mmLayerUtilityFinger.h"
#include "layer/utility/mmLayerUtilityIntersect.h"

namespace mm
{
    const std::string mmLayerKeyboardMenu::EventKeyboardItemRelease("EventKeyboardItemRelease");
    const std::string mmLayerKeyboardMenu::EventKeyboardItemPressed("EventKeyboardItemPressed");

    mmLayerKeyboardMenu::mmLayerKeyboardMenu(void)
        : hEventSet()
        , pWindowMenu(NULL)
        , pFingerTouch(NULL)

        , hRotation(0)
    {
        mmRbtreeU64Vpt_Init(&this->hRbtreeWindowItem);
        mmRbtreeU64Vpt_Init(&this->hRbtreeTouchs);
        mmRbtsetVpt_Init(&this->hRbtsetRelease);
        mmRbtsetVpt_Init(&this->hRbtsetPressed);
    }
    mmLayerKeyboardMenu::~mmLayerKeyboardMenu(void)
    {
        assert(0 == this->hRbtreeWindowItem.size && "clear item map before destroy.");

        mmRbtreeU64Vpt_Destroy(&this->hRbtreeWindowItem);
        mmRbtreeU64Vpt_Destroy(&this->hRbtreeTouchs);
        mmRbtsetVpt_Destroy(&this->hRbtsetRelease);
        mmRbtsetVpt_Destroy(&this->hRbtsetPressed);
    }
    void mmLayerKeyboardMenu::SetFingerTouch(mmLayerUtilityFingerTouch* pFingerTouch)
    {
        this->pFingerTouch = pFingerTouch;
    }
    void mmLayerKeyboardMenu::SetWindowMenu(CEGUI::Window* pWindowMenu)
    {
        this->pWindowMenu = pWindowMenu;
    }
    void mmLayerKeyboardMenu::OnFinishLaunching(void)
    {
        this->UpdateTransform();

        this->AttachMouseClickMenuEvent();
    }

    void mmLayerKeyboardMenu::OnBeforeTerminate(void)
    {
        mmRbtsetVpt_Clear(&this->hRbtsetRelease);
        mmRbtsetVpt_Clear(&this->hRbtsetPressed);
        mmRbtreeU64Vpt_Clear(&this->hRbtreeTouchs);

        this->DetachMouseClickMenuEvent();
    }
    void mmLayerKeyboardMenu::UpdateTransform(void)
    {
        mmLayerUtilityTransform_UpdateTransform(&this->hTransform, this->pWindowMenu);

        this->UpdateTransformItemWidget();
    }
    void mmLayerKeyboardMenu::UpdateTransformItemWidget(void)
    {
        mmLayerKeyboardItemWidget* e = NULL;
        struct mmRbNode* n = NULL;
        struct mmRbtreeU64VptIterator* it = NULL;
        //
        n = mmRb_First(&this->hRbtreeWindowItem.rbt);
        while (NULL != n)
        {
            it = (struct mmRbtreeU64VptIterator*)mmRb_Entry(n, struct mmRbtreeU64VptIterator, n);
            n = mmRb_Next(n);
            // 
            e = (mmLayerKeyboardItemWidget*)it->v;

            e->UpdateTransform();
        }
    }
    void mmLayerKeyboardMenu::OnEventWindowSizeChanged(struct mmSurfaceContentSizeChange* pContent)
    {
        this->UpdateTransform();
    }

    void mmLayerKeyboardMenu::OnEventTouchsMoved(struct mmSurfaceContentTouchs* pContent)
    {
        this->AddTouchsFinger(pContent);
        this->MovTouchsFinger(pContent);
        this->UpdateTouchsFinger();
    }
    void mmLayerKeyboardMenu::OnEventTouchsBegan(struct mmSurfaceContentTouchs* pContent)
    {
        this->AddTouchsFinger(pContent);
        this->UpdateTouchsFinger();
    }
    void mmLayerKeyboardMenu::OnEventTouchsEnded(struct mmSurfaceContentTouchs* pContent)
    {
        this->RmvTouchsFinger(pContent);
        this->UpdateTouchsFinger();
    }
    void mmLayerKeyboardMenu::OnEventTouchsBreak(struct mmSurfaceContentTouchs* pContent)
    {
        this->RmvTouchsFinger(pContent);
        this->UpdateTouchsFinger();
    }

    void mmLayerKeyboardMenu::AttachMouseClickMenuEvent(void)
    {
        size_t i = 0;
        size_t count = 0;
        CEGUI::Window* w = NULL;

        assert(NULL != this->pWindowMenu && "this->pWindowMenu can not be null.");

        count = this->pWindowMenu->getChildCount();

        while (i < count)
        {
            w = this->pWindowMenu->getChildAtIdx(i);
            this->AttachMouseClickEvent(w);

            i++;
        }
    }
    void mmLayerKeyboardMenu::DetachMouseClickMenuEvent(void)
    {
        size_t i = 0;
        size_t count = 0;
        CEGUI::Window* w = NULL;

        assert(NULL != this->pWindowMenu && "this->pWindowMenu can not be null.");

        count = this->pWindowMenu->getChildCount();

        while (i < count)
        {
            w = this->pWindowMenu->getChildAtIdx(i);
            this->DetachMouseClickEvent(w);

            i++;
        }
    }
    void mmLayerKeyboardMenu::AttachMouseClickEvent(CEGUI::Window* w)
    {
        mmLayerKeyboardItemWidget* e = this->AddItem(w);

        e->hEventSet.SubscribeEvent(mmLayerKeyboardItemWidget::EventKeyboardItemRelease, &mmLayerKeyboardMenu::OnEventKeyboardItemRelease, this);
        e->hEventSet.SubscribeEvent(mmLayerKeyboardItemWidget::EventKeyboardItemPressed, &mmLayerKeyboardMenu::OnEventKeyboardItemPressed, this);
    }
    void mmLayerKeyboardMenu::DetachMouseClickEvent(CEGUI::Window* w)
    {
        mmLayerKeyboardItemWidget* e = this->GetItem(w);

        assert(NULL != e && "can not detach item whitch not attach.");

        e->hEventSet.UnsubscribeEvent(mmLayerKeyboardItemWidget::EventKeyboardItemRelease, &mmLayerKeyboardMenu::OnEventKeyboardItemRelease, this);
        e->hEventSet.UnsubscribeEvent(mmLayerKeyboardItemWidget::EventKeyboardItemPressed, &mmLayerKeyboardMenu::OnEventKeyboardItemPressed, this);

        this->RmvItem(w);
    }
    mmLayerKeyboardItemWidget* mmLayerKeyboardMenu::AddItem(CEGUI::Window* w)
    {
        mmLayerKeyboardItemWidget* e = this->GetItem(w);
        if (NULL == e)
        {
            e = new mmLayerKeyboardItemWidget;
            e->SetItemWindow(w);
            e->OnFinishLaunching();
            mmRbtreeU64Vpt_Set(&this->hRbtreeWindowItem, (mmUIntptr_t)w, e);
        }
        return e;
    }
    mmLayerKeyboardItemWidget* mmLayerKeyboardMenu::GetItem(CEGUI::Window* w)
    {
        mmLayerKeyboardItemWidget* e = NULL;
        struct mmRbtreeU64VptIterator* it = NULL;
        it = mmRbtreeU64Vpt_GetIterator(&this->hRbtreeWindowItem, (mmUIntptr_t)w);
        if (NULL != it)
        {
            e = (mmLayerKeyboardItemWidget*)it->v;
        }
        return e;
    }
    mmLayerKeyboardItemWidget* mmLayerKeyboardMenu::GetItemInstance(CEGUI::Window* w)
    {
        mmLayerKeyboardItemWidget* e = this->GetItem(w);
        if (NULL == e)
        {
            e = this->AddItem(w);
        }
        return e;
    }
    void mmLayerKeyboardMenu::RmvItem(CEGUI::Window* w)
    {
        mmLayerKeyboardItemWidget* e = NULL;
        struct mmRbtreeU64VptIterator* it = NULL;
        it = mmRbtreeU64Vpt_GetIterator(&this->hRbtreeWindowItem, (mmUIntptr_t)w);
        if (NULL != it)
        {
            e = (mmLayerKeyboardItemWidget*)it->v;
            mmRbtreeU64Vpt_Erase(&this->hRbtreeWindowItem, it);
            e->OnBeforeTerminate();
            delete e;
        }
    }
    void mmLayerKeyboardMenu::ClearItem(void)
    {
        mmLayerKeyboardItemWidget* e = NULL;

        struct mmRbNode* n = NULL;
        struct mmRbtreeU64VptIterator* it = NULL;
        //
        n = mmRb_First(&this->hRbtreeWindowItem.rbt);
        while (NULL != n)
        {
            it = (struct mmRbtreeU64VptIterator*)mmRb_Entry(n, struct mmRbtreeU64VptIterator, n);
            n = mmRb_Next(n);
            // 
            e = (mmLayerKeyboardItemWidget*)it->v;

            mmRbtreeU64Vpt_Erase(&this->hRbtreeWindowItem, it);
            e->OnBeforeTerminate();
            delete e;
        }
    }
    void mmLayerKeyboardMenu::AddTouchsFinger(struct mmSurfaceContentTouchs* pContent)
    {
        struct mmRbtreeU64Vpt* pTouchs = &this->pFingerTouch->hRbtreeTouchs;

        // world position.
        Ogre::Vector3 hCenterToTouch;

        mmLayerUtilityFinger* u = NULL;

        struct mmRbNode* n = NULL;
        struct mmRbtreeU64VptIterator* it = NULL;
        //
        n = mmRb_First(&pTouchs->rbt);
        while (NULL != n)
        {
            it = (struct mmRbtreeU64VptIterator*)mmRb_Entry(n, struct mmRbtreeU64VptIterator, n);
            n = mmRb_Next(n);
            // 
            u = (mmLayerUtilityFinger*)it->v;

            if (this->IntersectFinger(u, hCenterToTouch))
            {
                this->hRotation = (double)atan2((double)hCenterToTouch.y, (double)hCenterToTouch.x);

                mmRbtreeU64Vpt_Set(&this->hRbtreeTouchs, u->hMotionId, u);
            }
        }
    }
    void mmLayerKeyboardMenu::RmvTouchsFinger(struct mmSurfaceContentTouchs* pContent)
    {
        size_t i = 0;

        struct mmSurfaceContentTouch* pTouchI = NULL;
        mmLayerUtilityFinger* pFinger = NULL;
        struct mmVectorValue* pVectorValue = &pContent->context;

        for (i = 0; i < pVectorValue->size; i++)
        {
            pTouchI = (struct mmSurfaceContentTouch*)mmVectorValue_GetIndexReference(pVectorValue, i);
            // remove.
            mmRbtreeU64Vpt_Rmv(&this->hRbtreeTouchs, pTouchI->motion_id);
        }
    }
    void mmLayerKeyboardMenu::MovTouchsFinger(struct mmSurfaceContentTouchs* pContent)
    {
        // world position.
        Ogre::Vector3 hCenterToTouch;
        double dx = 0;
        double dy = 0;

        mmLayerUtilityFinger* u = NULL;

        struct mmRbNode* n = NULL;
        struct mmRbtreeU64VptIterator* it = NULL;
        //
        n = mmRb_First(&this->hRbtreeTouchs.rbt);
        while (NULL != n)
        {
            it = (struct mmRbtreeU64VptIterator*)mmRb_Entry(n, struct mmRbtreeU64VptIterator, n);
            n = mmRb_Next(n);
            // 
            u = (mmLayerUtilityFinger*)it->v;
            //
            if (this->IntersectFinger(u, hCenterToTouch))
            {
                this->hRotation = (double)atan2((double)hCenterToTouch.y, (double)hCenterToTouch.x);
            }
            else
            {
                // remove.
                mmRbtreeU64Vpt_Erase(&this->hRbtreeTouchs, it);
                continue;
            }
        }
    }
    void mmLayerKeyboardMenu::UpdateTouchsFinger(void)
    {
        {
            // copy pressed -> release.
            mmLayerKeyboardItemWidget* pWidget = NULL;

            struct mmRbNode* n = NULL;
            struct mmRbtsetVptIterator* it = NULL;
            //
            n = mmRb_First(&this->hRbtsetPressed.rbt);
            while (NULL != n)
            {
                it = (struct mmRbtsetVptIterator*)mmRb_Entry(n, struct mmRbtsetVptIterator, n);
                n = mmRb_Next(n);
                // 
                pWidget = (mmLayerKeyboardItemWidget*)it->k;
                //
                mmRbtsetVpt_Add(&this->hRbtsetRelease, pWidget);
                mmRbtsetVpt_Erase(&this->hRbtsetPressed, it);
            }
        }

        {
            mmLayerUtilityFinger* u = NULL;

            struct mmRbNode* n = NULL;
            struct mmRbtreeU64VptIterator* it = NULL;
            //
            n = mmRb_First(&this->hRbtreeTouchs.rbt);
            while (NULL != n)
            {
                it = (struct mmRbtreeU64VptIterator*)mmRb_Entry(n, struct mmRbtreeU64VptIterator, n);
                n = mmRb_Next(n);
                // 
                u = (mmLayerUtilityFinger*)it->v;
                //
                this->WidgetTouchsFinger(u);
            }
        }

        {
            // appay release.
            mmLayerKeyboardItemWidget* pWidget = NULL;

            struct mmRbNode* n = NULL;
            struct mmRbtsetVptIterator* it = NULL;
            //
            n = mmRb_First(&this->hRbtsetRelease.rbt);
            while (NULL != n)
            {
                it = (struct mmRbtsetVptIterator*)mmRb_Entry(n, struct mmRbtsetVptIterator, n);
                n = mmRb_Next(n);
                // 
                pWidget = (mmLayerKeyboardItemWidget*)it->k;
                //
                pWidget->UpdateSelect(0);
                //
                mmRbtsetVpt_Erase(&this->hRbtsetRelease, it);
            }
        }

        {
            // appay pressed.
            mmLayerKeyboardItemWidget* pWidget = NULL;

            struct mmRbNode* n = NULL;
            struct mmRbtsetVptIterator* it = NULL;
            //
            n = mmRb_First(&this->hRbtsetPressed.rbt);
            while (NULL != n)
            {
                it = (struct mmRbtsetVptIterator*)mmRb_Entry(n, struct mmRbtsetVptIterator, n);
                n = mmRb_Next(n);
                // 
                pWidget = (mmLayerKeyboardItemWidget*)it->k;
                //
                pWidget->UpdateSelect(1);
            }
        }
    }
    void mmLayerKeyboardMenu::WidgetTouchsFinger(mmLayerUtilityFinger* pFinger)
    {
        // world position.
        Ogre::Vector3 hCenterToTouch;

        mmLayerKeyboardItemWidget* e = NULL;

        struct mmRbNode* n = NULL;
        struct mmRbtreeU64VptIterator* it = NULL;
        //
        n = mmRb_First(&this->hRbtreeWindowItem.rbt);
        while (NULL != n)
        {
            it = (struct mmRbtreeU64VptIterator*)mmRb_Entry(n, struct mmRbtreeU64VptIterator, n);
            n = mmRb_Next(n);
            // 
            e = (mmLayerKeyboardItemWidget*)it->v;

            if (e->IntersectFinger(pFinger, hCenterToTouch))
            {
                // we find the first widget.
                mmRbtsetVpt_Add(&this->hRbtsetPressed, e);
                mmRbtsetVpt_Rmv(&this->hRbtsetRelease, e);
                break;
            }
            else
            {
                e = NULL;
            }
        }
    }
    bool mmLayerKeyboardMenu::IntersectFinger(mmLayerUtilityFinger* pFinger, Ogre::Vector3& hCenterToTouch)
    {
        return mmLayerUtilityIntersect_FingerPointRectangle(&this->hTransform, pFinger, hCenterToTouch);
    }

    bool mmLayerKeyboardMenu::OnEventKeyboardItemRelease(const mmEventArgs& args)
    {
        this->hEventSet.FireEvent(EventKeyboardItemRelease, (mmEventArgs&)args);
        return true;
    }
    bool mmLayerKeyboardMenu::OnEventKeyboardItemPressed(const mmEventArgs& args)
    {
        this->hEventSet.FireEvent(EventKeyboardItemPressed, (mmEventArgs&)args);
        return true;
    }
}
