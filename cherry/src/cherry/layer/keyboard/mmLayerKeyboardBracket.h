/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmLayerKeyboardBracket_h__
#define __mmLayerKeyboardBracket_h__

#include "core/mmCore.h"

#include "dish/mmEvent.h"

#include "layer/director/mmLayerContext.h"

#include "CEGUI/Window.h"

namespace mm
{
    class mmLayerKeyboardComplete;

    class mmLayerKeyboardBracket : public mmLayerContext
    {
    public:
        static const CEGUI::String EventNamespace;          //!< Namespace for global events
        static const CEGUI::String WidgetTypeName;          //!< Window factory name
    public:
        static const std::string EventKeyboardItemRelease;
        static const std::string EventKeyboardItemPressed;
    public:
        mm::mmEventSet hEventSet;
    public:
        CEGUI::Window* LayerKeyboardBracket;

        CEGUI::Window* LayerKeyboardComplete;

        mmLayerKeyboardComplete* pKeyboardComplete;
    public:
        mmLayerKeyboardBracket(const CEGUI::String& type, const CEGUI::String& name);
        virtual ~mmLayerKeyboardBracket(void);
    public:
        // mmLayerContext
        virtual void OnFinishLaunching(void);
        virtual void OnBeforeTerminate(void);
    private:
        bool OnEventKeyboardItemRelease(const mmEventArgs& args);
        bool OnEventKeyboardItemPressed(const mmEventArgs& args);
    };
}

#endif//__mmLayerKeyboardBracket_h__