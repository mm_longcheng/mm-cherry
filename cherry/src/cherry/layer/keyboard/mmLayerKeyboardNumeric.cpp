/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerKeyboardNumeric.h"

#include "CEGUI/WindowManager.h"

#include "layer/director/mmLayerDirector.h"

namespace mm
{
    const CEGUI::String mmLayerKeyboardNumeric::EventNamespace = "mm";
    const CEGUI::String mmLayerKeyboardNumeric::WidgetTypeName = "mm/mmLayerKeyboardNumeric";

    const std::string mmLayerKeyboardNumeric::EventKeyboardItemRelease("EventKeyboardItemRelease");
    const std::string mmLayerKeyboardNumeric::EventKeyboardItemPressed("EventKeyboardItemPressed");

    mmLayerKeyboardNumeric::mmLayerKeyboardNumeric(const CEGUI::String& type, const CEGUI::String& name)
        : mmLayerContext(type, name)
        , hEventSet()
        , LayerKeyboardNumeric(NULL)
        , StaticImageBackground(NULL)

        , pFingerTouch(NULL)
    {
        
    }
    mmLayerKeyboardNumeric::~mmLayerKeyboardNumeric(void)
    {
        
    }
    void mmLayerKeyboardNumeric::SetFingerTouch(mmLayerUtilityFingerTouch* pFingerTouch)
    {
        this->pFingerTouch = pFingerTouch;
        this->hKeyboardMenu.SetFingerTouch(this->pFingerTouch);
    }
    void mmLayerKeyboardNumeric::OnFinishLaunching(void)
    {
        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        this->LayerKeyboardNumeric = pWindowManager->loadLayoutFromFile("keyboard/LayerKeyboardNumeric.layout");
        this->addChild(this->LayerKeyboardNumeric);
        // Manual set whole area.
        CEGUI::URect hWholeArea(CEGUI::UDim(0, 0), CEGUI::UDim(0, 0), CEGUI::UDim(1, 0), CEGUI::UDim(1, 0));
        this->LayerKeyboardNumeric->setArea(hWholeArea);

        this->StaticImageBackground = this->LayerKeyboardNumeric->getChild("StaticImageBackground");

        this->hKeyboardMenu.SetWindowMenu(this->StaticImageBackground);
        this->hKeyboardMenu.OnFinishLaunching();

        this->hKeyboardMenu.hEventSet.SubscribeEvent(mmLayerKeyboardMenu::EventKeyboardItemRelease, &mmLayerKeyboardNumeric::OnEventKeyboardItemRelease, this);
        this->hKeyboardMenu.hEventSet.SubscribeEvent(mmLayerKeyboardMenu::EventKeyboardItemPressed, &mmLayerKeyboardNumeric::OnEventKeyboardItemPressed, this);
    }

    void mmLayerKeyboardNumeric::OnBeforeTerminate(void)
    {
        this->hKeyboardMenu.hEventSet.UnsubscribeEvent(mmLayerKeyboardMenu::EventKeyboardItemRelease, &mmLayerKeyboardNumeric::OnEventKeyboardItemRelease, this);
        this->hKeyboardMenu.hEventSet.UnsubscribeEvent(mmLayerKeyboardMenu::EventKeyboardItemPressed, &mmLayerKeyboardNumeric::OnEventKeyboardItemPressed, this);

        this->hKeyboardMenu.OnBeforeTerminate();

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        pWindowManager->destroyWindow(this->LayerKeyboardNumeric);
        this->LayerKeyboardNumeric = NULL;
    }
    void mmLayerKeyboardNumeric::UpdateTransform(void)
    {
        this->hKeyboardMenu.UpdateTransform();
    }
    void mmLayerKeyboardNumeric::OnEventWindowSizeChanged(struct mmSurfaceContentSizeChange* pContent)
    {
        this->hKeyboardMenu.OnEventWindowSizeChanged(pContent);
    }
    void mmLayerKeyboardNumeric::OnEventTouchsMoved(struct mmSurfaceContentTouchs* pContent)
    {
        this->hKeyboardMenu.OnEventTouchsMoved(pContent);
    }
    void mmLayerKeyboardNumeric::OnEventTouchsBegan(struct mmSurfaceContentTouchs* pContent)
    {
        this->hKeyboardMenu.OnEventTouchsBegan(pContent);
    }
    void mmLayerKeyboardNumeric::OnEventTouchsEnded(struct mmSurfaceContentTouchs* pContent)
    {
        this->hKeyboardMenu.OnEventTouchsEnded(pContent);
    }
    void mmLayerKeyboardNumeric::OnEventTouchsBreak(struct mmSurfaceContentTouchs* pContent)
    {
        this->hKeyboardMenu.OnEventTouchsBreak(pContent);
    }
    bool mmLayerKeyboardNumeric::OnEventKeyboardItemRelease(const mmEventArgs& args)
    {
        this->hEventSet.FireEvent(EventKeyboardItemRelease, (mmEventArgs&)args);
        return true;
    }
    bool mmLayerKeyboardNumeric::OnEventKeyboardItemPressed(const mmEventArgs& args)
    {
        this->hEventSet.FireEvent(EventKeyboardItemPressed, (mmEventArgs&)args);
        return true;
    }
}
