/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmLayerKeyboardMenu_h__
#define __mmLayerKeyboardMenu_h__

#include "core/mmCore.h"

#include "container/mmRbtreeU64.h"
#include "container/mmRbtsetVpt.h"

#include "dish/mmEvent.h"

#include "layer/director/mmLayerContext.h"

#include "CEGUI/Window.h"

#include "layer/utility/mmLayerUtilityTransform.h"

namespace mm
{
    class mmLayerUtilityFinger;
    class mmLayerUtilityFingerTouch;

    class mmLayerKeyboardItemWidget;

    class mmLayerKeyboardMenu
    {
    public:
        static const std::string EventKeyboardItemRelease;
        static const std::string EventKeyboardItemPressed;
    public:
        mm::mmEventSet hEventSet;
    public:
        CEGUI::Window* pWindowMenu;
    public:
        // strong ref.
        // <CEGUI::Window*, mmLayerKeyboardItemWidget*>
        struct mmRbtreeU64Vpt hRbtreeWindowItem;
    public:
        mmLayerUtilityFingerTouch* pFingerTouch;
    public:
        struct mmRbtreeU64Vpt hRbtreeTouchs;
        struct mmRbtsetVpt hRbtsetRelease;
        struct mmRbtsetVpt hRbtsetPressed;
    public:
        struct mmLayerUtilityTransform hTransform;
    public:
        double hRotation;
    public:
        mmLayerKeyboardMenu(void);
        ~mmLayerKeyboardMenu(void);
    public:
        void SetFingerTouch(mmLayerUtilityFingerTouch* pFingerTouch);
        void SetWindowMenu(CEGUI::Window* pWindowMenu);
    public:
        // mmLayerContext
        virtual void OnFinishLaunching(void);
        virtual void OnBeforeTerminate(void);
    public:
        void UpdateTransform(void);
        void UpdateTransformItemWidget(void);
    public:
        void OnEventWindowSizeChanged(struct mmSurfaceContentSizeChange* pContent);
    public:
        void OnEventTouchsMoved(struct mmSurfaceContentTouchs* pContent);
        void OnEventTouchsBegan(struct mmSurfaceContentTouchs* pContent);
        void OnEventTouchsEnded(struct mmSurfaceContentTouchs* pContent);
        void OnEventTouchsBreak(struct mmSurfaceContentTouchs* pContent);
    private:
        void AttachMouseClickMenuEvent(void);
        void DetachMouseClickMenuEvent(void);
        //
        void AttachMouseClickEvent(CEGUI::Window* w);
        void DetachMouseClickEvent(CEGUI::Window* w);
    private:
        mmLayerKeyboardItemWidget* AddItem(CEGUI::Window* w);
        mmLayerKeyboardItemWidget* GetItem(CEGUI::Window* w);
        mmLayerKeyboardItemWidget* GetItemInstance(CEGUI::Window* w);
        void RmvItem(CEGUI::Window* w);
        void ClearItem();
    private:
        void AddTouchsFinger(struct mmSurfaceContentTouchs* pContent);
        void RmvTouchsFinger(struct mmSurfaceContentTouchs* pContent);
        void MovTouchsFinger(struct mmSurfaceContentTouchs* pContent);
    private:
        void UpdateTouchsFinger(void);
        void WidgetTouchsFinger(mmLayerUtilityFinger* pFinger);
    private:
        bool IntersectFinger(mmLayerUtilityFinger* pFinger, Ogre::Vector3& hCenterToTouch);
    private:
        bool OnEventKeyboardItemRelease(const mmEventArgs& args);
        bool OnEventKeyboardItemPressed(const mmEventArgs& args);
    };
}

#endif//__mmLayerKeyboardMenu_h__