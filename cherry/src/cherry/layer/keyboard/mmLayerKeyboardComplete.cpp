/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerKeyboardComplete.h"
#include "mmLayerKeyboardItemWidget.h"

#include "mmLayerKeyboardCharacter.h"
#include "mmLayerKeyboardEdit.h"
#include "mmLayerKeyboardFunction.h"
#include "mmLayerKeyboardNumeric.h"
#include "mmLayerKeyboardIndicator.h"

#include "nwsi/mmEventSurface.h"

#include "CEGUI/WindowManager.h"
#include "CEGUI/RenderTarget.h"

#include "layer/director/mmLayerDirector.h"

namespace mm
{
    const CEGUI::String mmLayerKeyboardComplete::EventNamespace = "mm";
    const CEGUI::String mmLayerKeyboardComplete::WidgetTypeName = "mm/mmLayerKeyboardComplete";

    const std::string mmLayerKeyboardComplete::EventKeyboardItemRelease("EventKeyboardItemRelease");
    const std::string mmLayerKeyboardComplete::EventKeyboardItemPressed("EventKeyboardItemPressed");

    mmLayerKeyboardComplete::mmLayerKeyboardComplete(const CEGUI::String& type, const CEGUI::String& name)
        : mmLayerContext(type, name)
        , hEventSet()
        , LayerKeyboardComplete(NULL)
        , StaticImageBackground(NULL)

        , LayerKeyboardCharacter(NULL)
        , LayerKeyboardEdit(NULL)
        , LayerKeyboardFunction(NULL)
        , LayerKeyboardNumeric(NULL)
        , LayerKeyboardIndicator(NULL)

        , pCharacter(NULL)
        , pEdit(NULL)
        , pFunction(NULL)
        , pNumeric(NULL)
        , pIndicator(NULL)
    {
        
    }
    mmLayerKeyboardComplete::~mmLayerKeyboardComplete(void)
    {
        
    }
    void mmLayerKeyboardComplete::OnFinishLaunching(void)
    {
        struct mmSurfaceMaster* pSurfaceMaster = this->pSurfaceMaster;

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        this->LayerKeyboardComplete = pWindowManager->loadLayoutFromFile("keyboard/LayerKeyboardComplete.layout");
        this->addChild(this->LayerKeyboardComplete);

        // Manual set whole area.
        CEGUI::URect hWholeArea(CEGUI::UDim(0, 0), CEGUI::UDim(0, 0), CEGUI::UDim(1, 0), CEGUI::UDim(1, 0));
        this->LayerKeyboardComplete->setArea(hWholeArea);

        this->StaticImageBackground = this->LayerKeyboardComplete->getChild("StaticImageBackground");

        this->LayerKeyboardCharacter = this->StaticImageBackground->getChild("LayerKeyboardCharacter");
        this->LayerKeyboardEdit = this->StaticImageBackground->getChild("LayerKeyboardEdit");
        this->LayerKeyboardFunction = this->StaticImageBackground->getChild("LayerKeyboardFunction");
        this->LayerKeyboardNumeric = this->StaticImageBackground->getChild("LayerKeyboardNumeric");
        this->LayerKeyboardIndicator = this->StaticImageBackground->getChild("LayerKeyboardIndicator");

        this->pCharacter = (mmLayerKeyboardCharacter*)this->LayerKeyboardCharacter;
        this->pEdit = (mmLayerKeyboardEdit*)this->LayerKeyboardEdit;
        this->pFunction = (mmLayerKeyboardFunction*)this->LayerKeyboardFunction;
        this->pNumeric = (mmLayerKeyboardNumeric*)this->LayerKeyboardNumeric;
        this->pIndicator = (mmLayerKeyboardIndicator*)this->LayerKeyboardIndicator;

        this->hFingerTouch.SetName(this->getName().c_str());
        this->hFingerTouch.SetRadius(256.0f);
        this->hFingerTouch.SetForceRange(0.0f, 1.0f);
        this->hFingerTouch.SetScaleRange(0.22f, 0.34f);
        this->hFingerTouch.SetImageSource("ImagesetUtility/circle_touch");
        this->hFingerTouch.SetFingerIsVisible(true);
        this->hFingerTouch.SetSurfaceMaster(pSurfaceMaster);
        this->hFingerTouch.SetBackground(this);
        this->hFingerTouch.OnFinishLaunching();

        this->pCharacter->SetDirector(this->pDirector);
        this->pCharacter->SetContextMaster(this->pContextMaster);
        this->pCharacter->SetSurfaceMaster(this->pSurfaceMaster);
        this->pCharacter->SetFingerTouch(&this->hFingerTouch);
        this->pCharacter->OnFinishLaunching();

        this->pEdit->SetDirector(this->pDirector);
        this->pEdit->SetContextMaster(this->pContextMaster);
        this->pEdit->SetSurfaceMaster(this->pSurfaceMaster);
        this->pEdit->SetFingerTouch(&this->hFingerTouch);
        this->pEdit->OnFinishLaunching();

        this->pFunction->SetDirector(this->pDirector);
        this->pFunction->SetContextMaster(this->pContextMaster);
        this->pFunction->SetSurfaceMaster(this->pSurfaceMaster);
        this->pFunction->SetFingerTouch(&this->hFingerTouch);
        this->pFunction->OnFinishLaunching();

        this->pNumeric->SetDirector(this->pDirector);
        this->pNumeric->SetContextMaster(this->pContextMaster);
        this->pNumeric->SetSurfaceMaster(this->pSurfaceMaster);
        this->pNumeric->SetFingerTouch(&this->hFingerTouch);
        this->pNumeric->OnFinishLaunching();

        this->pIndicator->SetDirector(this->pDirector);
        this->pIndicator->SetContextMaster(this->pContextMaster);
        this->pIndicator->SetSurfaceMaster(this->pSurfaceMaster);
        this->pIndicator->OnFinishLaunching();

        // spacing common CEGUI::Window object, we need manual attach context event.
        this->AttachContextEvent(this->pCharacter);
        this->AttachContextEvent(this->pEdit);
        this->AttachContextEvent(this->pFunction);
        this->AttachContextEvent(this->pNumeric);
        this->AttachContextEvent(this->pIndicator);

        this->pCharacter->hEventSet.SubscribeEvent(mmLayerKeyboardCharacter::EventKeyboardItemRelease, &mmLayerKeyboardComplete::OnEventKeyboardItemRelease, this);
        this->pCharacter->hEventSet.SubscribeEvent(mmLayerKeyboardCharacter::EventKeyboardItemPressed, &mmLayerKeyboardComplete::OnEventKeyboardItemPressed, this);
        this->pEdit->hEventSet.SubscribeEvent(mmLayerKeyboardEdit::EventKeyboardItemRelease, &mmLayerKeyboardComplete::OnEventKeyboardItemRelease, this);
        this->pEdit->hEventSet.SubscribeEvent(mmLayerKeyboardEdit::EventKeyboardItemPressed, &mmLayerKeyboardComplete::OnEventKeyboardItemPressed, this);
        this->pFunction->hEventSet.SubscribeEvent(mmLayerKeyboardFunction::EventKeyboardItemRelease, &mmLayerKeyboardComplete::OnEventKeyboardItemRelease, this);
        this->pFunction->hEventSet.SubscribeEvent(mmLayerKeyboardFunction::EventKeyboardItemPressed, &mmLayerKeyboardComplete::OnEventKeyboardItemPressed, this);
        this->pNumeric->hEventSet.SubscribeEvent(mmLayerKeyboardNumeric::EventKeyboardItemRelease, &mmLayerKeyboardComplete::OnEventKeyboardItemRelease, this);
        this->pNumeric->hEventSet.SubscribeEvent(mmLayerKeyboardNumeric::EventKeyboardItemPressed, &mmLayerKeyboardComplete::OnEventKeyboardItemPressed, this);

        this->hLayerEventTouchsMovedConn = this->subscribeEvent(mmLayerContext::EventTouchsMoved, CEGUI::Event::Subscriber(&mmLayerKeyboardComplete::OnLayerEventTouchsMoved, this));
        this->hLayerEventTouchsBeganConn = this->subscribeEvent(mmLayerContext::EventTouchsBegan, CEGUI::Event::Subscriber(&mmLayerKeyboardComplete::OnLayerEventTouchsBegan, this));
        this->hLayerEventTouchsEndedConn = this->subscribeEvent(mmLayerContext::EventTouchsEnded, CEGUI::Event::Subscriber(&mmLayerKeyboardComplete::OnLayerEventTouchsEnded, this));
        this->hLayerEventTouchsBreakConn = this->subscribeEvent(mmLayerContext::EventTouchsBreak, CEGUI::Event::Subscriber(&mmLayerKeyboardComplete::OnLayerEventTouchsBreak, this));
        //
        this->hLayerEventUpdatedConn = this->subscribeEvent(CEGUI::Window::EventUpdated, CEGUI::Event::Subscriber(&mmLayerKeyboardComplete::OnLayerEventUpdated, this));
    }

    void mmLayerKeyboardComplete::OnBeforeTerminate(void)
    {
        struct mmSurfaceMaster* pSurfaceMaster = this->pSurfaceMaster;

        this->hLayerEventTouchsMovedConn->disconnect();
        this->hLayerEventTouchsBeganConn->disconnect();
        this->hLayerEventTouchsEndedConn->disconnect();
        this->hLayerEventTouchsBreakConn->disconnect();
        //
        this->hLayerEventUpdatedConn->disconnect();

        this->pCharacter->OnBeforeTerminate();
        this->pEdit->OnBeforeTerminate();
        this->pFunction->OnBeforeTerminate();
        this->pNumeric->OnBeforeTerminate();
        this->pIndicator->OnBeforeTerminate();

        // spacing common CEGUI::Window object, we need manual attach context event.
        this->DetachContextEvent(this->pCharacter);
        this->DetachContextEvent(this->pEdit);
        this->DetachContextEvent(this->pFunction);
        this->DetachContextEvent(this->pNumeric);
        this->DetachContextEvent(this->pIndicator);

        this->pCharacter->hEventSet.UnsubscribeEvent(mmLayerKeyboardCharacter::EventKeyboardItemRelease, &mmLayerKeyboardComplete::OnEventKeyboardItemRelease, this);
        this->pCharacter->hEventSet.UnsubscribeEvent(mmLayerKeyboardCharacter::EventKeyboardItemPressed, &mmLayerKeyboardComplete::OnEventKeyboardItemPressed, this);
        this->pEdit->hEventSet.UnsubscribeEvent(mmLayerKeyboardEdit::EventKeyboardItemRelease, &mmLayerKeyboardComplete::OnEventKeyboardItemRelease, this);
        this->pEdit->hEventSet.UnsubscribeEvent(mmLayerKeyboardEdit::EventKeyboardItemPressed, &mmLayerKeyboardComplete::OnEventKeyboardItemPressed, this);
        this->pFunction->hEventSet.UnsubscribeEvent(mmLayerKeyboardFunction::EventKeyboardItemRelease, &mmLayerKeyboardComplete::OnEventKeyboardItemRelease, this);
        this->pFunction->hEventSet.UnsubscribeEvent(mmLayerKeyboardFunction::EventKeyboardItemPressed, &mmLayerKeyboardComplete::OnEventKeyboardItemPressed, this);
        this->pNumeric->hEventSet.UnsubscribeEvent(mmLayerKeyboardNumeric::EventKeyboardItemRelease, &mmLayerKeyboardComplete::OnEventKeyboardItemRelease, this);
        this->pNumeric->hEventSet.UnsubscribeEvent(mmLayerKeyboardNumeric::EventKeyboardItemPressed, &mmLayerKeyboardComplete::OnEventKeyboardItemPressed, this);

        this->hFingerTouch.OnBeforeTerminate();

        this->LayerKeyboardCharacter = NULL;
        this->LayerKeyboardEdit = NULL;
        this->LayerKeyboardFunction = NULL;
        this->LayerKeyboardIndicator = NULL;
        this->LayerKeyboardNumeric = NULL;

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        pWindowManager->destroyWindow(this->LayerKeyboardComplete);
        this->LayerKeyboardComplete = NULL;
    }
    void mmLayerKeyboardComplete::UpdateTransform(void)
    {
        if (NULL != this->pCharacter &&
            NULL != this->pEdit &&
            NULL != this->pFunction &&
            NULL != this->pNumeric)
        {
            this->hFingerTouch.UpdateTransform();
            //
            this->pCharacter->UpdateTransform();
            this->pEdit->UpdateTransform();
            this->pFunction->UpdateTransform();
            this->pNumeric->UpdateTransform();
        }
    }
    void mmLayerKeyboardComplete::notifyScreenAreaChanged(bool recursive/*  = true */)
    {
        mmLayerContext::notifyScreenAreaChanged(recursive);
        this->UpdateTransform();
    }
    bool mmLayerKeyboardComplete::OnLayerEventTouchsMoved(const CEGUI::EventArgs& args)
    {
        const mmTouchsEventArgs& evt = (const mmTouchsEventArgs&)(args);

        this->hFingerTouch.OnEventTouchsMoved(evt.content);

        this->pCharacter->OnEventTouchsMoved(evt.content);
        this->pEdit->OnEventTouchsMoved(evt.content);
        this->pFunction->OnEventTouchsMoved(evt.content);
        this->pNumeric->OnEventTouchsMoved(evt.content);

        return false;
    }
    bool mmLayerKeyboardComplete::OnLayerEventTouchsBegan(const CEGUI::EventArgs& args)
    {
        const mmTouchsEventArgs& evt = (const mmTouchsEventArgs&)(args);

        this->hFingerTouch.OnEventTouchsBegan(evt.content);

        this->pCharacter->OnEventTouchsBegan(evt.content);
        this->pEdit->OnEventTouchsBegan(evt.content);
        this->pFunction->OnEventTouchsBegan(evt.content);
        this->pNumeric->OnEventTouchsBegan(evt.content);

        return false;
    }
    bool mmLayerKeyboardComplete::OnLayerEventTouchsEnded(const CEGUI::EventArgs& args)
    {
        const mmTouchsEventArgs& evt = (const mmTouchsEventArgs&)(args);

        this->hFingerTouch.OnEventTouchsEnded(evt.content);

        this->pCharacter->OnEventTouchsEnded(evt.content);
        this->pEdit->OnEventTouchsEnded(evt.content);
        this->pFunction->OnEventTouchsEnded(evt.content);
        this->pNumeric->OnEventTouchsEnded(evt.content);

        return false;
    }
    bool mmLayerKeyboardComplete::OnLayerEventTouchsBreak(const CEGUI::EventArgs& args)
    {
        const mmTouchsEventArgs& evt = (const mmTouchsEventArgs&)(args);

        this->hFingerTouch.OnEventTouchsBreak(evt.content);

        this->pCharacter->OnEventTouchsBreak(evt.content);
        this->pEdit->OnEventTouchsBreak(evt.content);
        this->pFunction->OnEventTouchsBreak(evt.content);
        this->pNumeric->OnEventTouchsBreak(evt.content);

        return false;
    }
    bool mmLayerKeyboardComplete::OnLayerEventUpdated(const CEGUI::EventArgs& args)
    {
        const CEGUI::UpdateEventArgs& evt = (const CEGUI::UpdateEventArgs&)(args);

        this->hFingerTouch.OnEventUpdated(evt.d_timeSinceLastFrame);

        return true;
    }

    bool mmLayerKeyboardComplete::OnEventKeyboardItemRelease(const mmEventArgs& args)
    {
        const mmLayerKeyboardItemEventArgs& evt = (const mmLayerKeyboardItemEventArgs&)(args);

        this->hEventSet.FireEvent(EventKeyboardItemRelease, (mmEventArgs&)args);

        this->pIndicator->OnEventItemRelease(evt.keycode);

        return true;
    }

    bool mmLayerKeyboardComplete::OnEventKeyboardItemPressed(const mmEventArgs& args)
    {
        const mmLayerKeyboardItemEventArgs& evt = (const mmLayerKeyboardItemEventArgs&)(args);

        this->hEventSet.FireEvent(EventKeyboardItemPressed, (mmEventArgs&)args);

        this->pIndicator->OnEventItemPressed(evt.keycode);

        return true;
    }
}
