/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerUtilityScrollbar.h"

#include "CEGUI/widgets/Listbox.h"
#include "CEGUI/widgets/Scrollbar.h"
#include "CEGUI/widgets/Thumb.h"

#include "CEGUI/falagard/WidgetLookFeel.h"

#include "CEGUI/WindowRendererSets/Core/StaticText.h"

namespace mm
{
    mmLayerUtilityScrollbarListbox::mmLayerUtilityScrollbarListbox(void)
        : pWindow(NULL)
    {

    }
    void mmLayerUtilityScrollbarListbox::SetWindow(CEGUI::Window* _window)
    {
        this->pWindow = _window;
    }
    void mmLayerUtilityScrollbarListbox::OnFinishLaunching(void)
    {
        this->hEventListContentsChangedConn = this->pWindow->subscribeEvent(CEGUI::Listbox::EventListContentsChanged, CEGUI::Event::Subscriber(&mmLayerUtilityScrollbarListbox::OnLayerEventListContentsChanged, this));
    }
    void mmLayerUtilityScrollbarListbox::OnBeforeTerminate(void)
    {
        this->hEventListContentsChangedConn->disconnect();
    }
    bool mmLayerUtilityScrollbarListbox::OnLayerEventListContentsChanged(const CEGUI::EventArgs& args)
    {
        const CEGUI::WindowEventArgs& evt = (const CEGUI::WindowEventArgs&)(args);
        //
        CEGUI::Listbox* _Listbox = (CEGUI::Listbox*)evt.window;
        {
            CEGUI::Scrollbar* VertScrollbar = _Listbox->getVertScrollbar();
            CEGUI::Thumb* thumb = VertScrollbar->getThumb();
            //
            const CEGUI::WidgetLookFeel& wlf = VertScrollbar->getWindowRenderer()->getLookNFeel();
            CEGUI::Rectf area_trackta(wlf.getNamedArea("ThumbTrackArea").getArea().getPixelRect(*VertScrollbar));
            const CEGUI::Rectf& area_context = _Listbox->getListRenderArea();
            const CEGUI::USize& thubm_size = thumb->getSize();
            float h_t = area_trackta.getHeight();
            float h_c = area_context.getHeight();
            float h_thumb = h_t / h_c;
            h_thumb = h_thumb > 0.0f ? h_thumb : 0.0f;
            h_thumb = h_thumb < 1.0f ? h_thumb : 1.0f;
            thumb->setSize(CEGUI::USize(thubm_size.d_width, CEGUI::UDim(h_thumb, 0)));
        }
        {
            CEGUI::Scrollbar* HorzScrollbar = _Listbox->getHorzScrollbar();
            CEGUI::Thumb* thumb = HorzScrollbar->getThumb();
            //
            const CEGUI::WidgetLookFeel& wlf = HorzScrollbar->getWindowRenderer()->getLookNFeel();
            CEGUI::Rectf area_trackta(wlf.getNamedArea("ThumbTrackArea").getArea().getPixelRect(*HorzScrollbar));
            const CEGUI::Rectf& area_context = _Listbox->getListRenderArea();
            const CEGUI::USize& thubm_size = thumb->getSize();
            float h_t = area_trackta.getWidth();
            float h_c = area_context.getWidth();
            float h_thumb = h_t / h_c;
            h_thumb = h_thumb > 0.0f ? h_thumb : 0.0f;
            h_thumb = h_thumb < 1.0f ? h_thumb : 1.0f;
            thumb->setSize(CEGUI::USize(CEGUI::UDim(h_thumb, 0), thubm_size.d_height));
        }
        return true;
    }

    mmLayerUtilityScrollbarStaticText::mmLayerUtilityScrollbarStaticText(void)
        : pWindow(NULL)
    {

    }
    void mmLayerUtilityScrollbarStaticText::SetWindow(CEGUI::Window* _window)
    {
        this->pWindow = _window;
    }
    void mmLayerUtilityScrollbarStaticText::OnFinishLaunching(void)
    {
        this->hEventTextChangedConn = this->pWindow->subscribeEvent(CEGUI::Window::EventTextChanged, CEGUI::Event::Subscriber(&mmLayerUtilityScrollbarStaticText::OnLayerEventTextChanged, this));
    }
    void mmLayerUtilityScrollbarStaticText::OnBeforeTerminate(void)
    {
        this->hEventTextChangedConn->disconnect();
    }
    bool mmLayerUtilityScrollbarStaticText::OnLayerEventTextChanged(const CEGUI::EventArgs& args)
    {
        const CEGUI::WindowEventArgs& evt = (const CEGUI::WindowEventArgs&)(args);
        //
        CEGUI::Window* _StaticText = (CEGUI::Window*)evt.window;
        CEGUI::WindowRenderer* _WindowRenderer = _StaticText->getWindowRenderer();
        CEGUI::FalagardStaticText* _FalagardStaticText = (CEGUI::FalagardStaticText*)_WindowRenderer;
        {
            CEGUI::Scrollbar* VertScrollbar = (CEGUI::Scrollbar*)_StaticText->getChild(CEGUI::FalagardStaticText::VertScrollbarName);
            CEGUI::Thumb* thumb = VertScrollbar->getThumb();
            //
            const CEGUI::WidgetLookFeel& wlf = VertScrollbar->getWindowRenderer()->getLookNFeel();
            CEGUI::Rectf area_trackta(wlf.getNamedArea("ThumbTrackArea").getArea().getPixelRect(*VertScrollbar));
            const CEGUI::USize& thubm_size = thumb->getSize();
            float h_t = area_trackta.getHeight();
            float h_c = _FalagardStaticText->getVerticalTextExtent();
            float h_thumb = h_t / h_c;
            h_thumb = h_thumb > 0.0f ? h_thumb : 0.0f;
            h_thumb = h_thumb < 1.0f ? h_thumb : 1.0f;
            thumb->setSize(CEGUI::USize(thubm_size.d_width, CEGUI::UDim(h_thumb, 0)));
        }
        {
            CEGUI::Scrollbar* HorzScrollbar = (CEGUI::Scrollbar*)_StaticText->getChild(CEGUI::FalagardStaticText::HorzScrollbarName);
            CEGUI::Thumb* thumb = HorzScrollbar->getThumb();
            //
            const CEGUI::WidgetLookFeel& wlf = HorzScrollbar->getWindowRenderer()->getLookNFeel();
            CEGUI::Rectf area_trackta(wlf.getNamedArea("ThumbTrackArea").getArea().getPixelRect(*HorzScrollbar));
            const CEGUI::USize& thubm_size = thumb->getSize();
            float h_t = area_trackta.getWidth();
            float h_c = _FalagardStaticText->getHorizontalTextExtent();
            float h_thumb = h_t / h_c;
            h_thumb = h_thumb > 0.0f ? h_thumb : 0.0f;
            h_thumb = h_thumb < 1.0f ? h_thumb : 1.0f;
            thumb->setSize(CEGUI::USize(CEGUI::UDim(h_thumb, 0), thubm_size.d_height));
        }
        return true;
    }
}
