/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerUtilityFps.h"

#include "core/mmString.h"
#include "core/mmFrameTimer.h"

namespace mm
{
    mmLayerUtilityFps::mmLayerUtilityFps()
        : pWindow(NULL)

        , pFrameTimer(NULL)
        , hTimerInterval(0.0)
    {

    }
    void mmLayerUtilityFps::SetWindow(CEGUI::Window* pWindow)
    {
        this->pWindow = pWindow;
    }
    void mmLayerUtilityFps::SetFrameTimer(struct mmFrameTimer* pFrameTimer)
    {
        this->pFrameTimer = pFrameTimer;
    }
    void mmLayerUtilityFps::OnFinishLaunching()
    {
        this->hEventUpdatedConn = this->pWindow->subscribeEvent(CEGUI::Window::EventUpdated, CEGUI::Event::Subscriber(&mmLayerUtilityFps::OnLayerEventUpdated, this));
    }
    void mmLayerUtilityFps::OnBeforeTerminate()
    {
        this->hEventUpdatedConn->disconnect();
    }
    bool mmLayerUtilityFps::OnLayerEventUpdated(const CEGUI::EventArgs& args)
    {
        const CEGUI::UpdateEventArgs& evt = (const CEGUI::UpdateEventArgs&)(args);

        this->hTimerInterval += evt.d_timeSinceLastFrame;

        if (this->hTimerInterval >= 1.0)
        {
            char _fps_string[64] = { 0 };

            this->hTimerInterval = 0.0;

            mmSprintf(_fps_string, "%.02lf", this->pFrameTimer->status.fps_avge);
            this->pWindow->setText(_fps_string);
        }

        return true;
    }
}
