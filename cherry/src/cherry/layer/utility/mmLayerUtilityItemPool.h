/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmLayerUtilityItemPool_h__
#define __mmLayerUtilityItemPool_h__

#include "core/mmCore.h"

#include "container/mmVectorValue.h"
#include "container/mmRbtsetVpt.h"

namespace mm
{
    class mmLayerContext;
}

#define MM_LAYERUTILITYITEMPOOL_MAX_NUMBER 128

struct mmLayerUtilityItemPool
{
    struct mmRbtsetVpt pool;// strong ref.
    struct mmRbtsetVpt used;// weak ref.
    struct mmRbtsetVpt idle;// weak ref.
    struct mmString type;
    size_t max_item_number;
};
extern void mmLayerUtilityItemPool_Init(struct mmLayerUtilityItemPool* p);
extern void mmLayerUtilityItemPool_Destroy(struct mmLayerUtilityItemPool* p);
//
extern void mmLayerUtilityItemPool_SetType(struct mmLayerUtilityItemPool* p, const char* type);
//
extern void mmLayerUtilityItemPool_OnFinishLaunching(struct mmLayerUtilityItemPool* p);
extern void mmLayerUtilityItemPool_OnBeforeTerminate(struct mmLayerUtilityItemPool* p);

extern mm::mmLayerContext* mmLayerUtilityItemPool_Produce(struct mmLayerUtilityItemPool* p);
extern void mmLayerUtilityItemPool_Recycle(struct mmLayerUtilityItemPool* p, mm::mmLayerContext* e);

extern mm::mmLayerContext* mmLayerUtilityItemPool_AddItem(struct mmLayerUtilityItemPool* p);
extern void mmLayerUtilityItemPool_RmvItem(struct mmLayerUtilityItemPool* p, mm::mmLayerContext* e);
extern void mmLayerUtilityItemPool_ClearItem(struct mmLayerUtilityItemPool* p);

#endif//__mmLayerUtilityItemPool_h__