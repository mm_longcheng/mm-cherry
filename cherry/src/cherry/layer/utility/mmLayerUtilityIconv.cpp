/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerUtilityIconv.h"

#include "core/mmByte.h"

#include "CEGUI/widgets/ListboxItem.h"

void mmLayerUtilityIconv_HostName(struct mmIconvContext* iconv_context, CEGUI::Window* w, struct mmString* s)
{
    const CEGUI::String& _text_utf8 = w->getText();

    struct mmByteBuffer i_byte_buffer;
    struct mmByteBuffer o_byte_buffer;

    i_byte_buffer.buffer = (mmUInt8_t*)_text_utf8.c_str();
    i_byte_buffer.offset = 0;
    i_byte_buffer.length = _text_utf8.utf8_stream_len();
    mmIconvContext_ViewToHost(iconv_context, &i_byte_buffer, &o_byte_buffer);
    mmString_Assignsn(s, (const char*)(o_byte_buffer.buffer + o_byte_buffer.offset), o_byte_buffer.length);
    // 
    mmString_ReplaceChar(s, '\\', '/');
}
void mmLayerUtilityIconv_ViewName(struct mmIconvContext* iconv_context, CEGUI::Window* w, struct mmString* s)
{
    CEGUI::String _text_utf8;
    struct mmByteBuffer i_byte_buffer;
    struct mmByteBuffer o_byte_buffer;

    i_byte_buffer.buffer = (mmUInt8_t*)mmString_Data(s);
    i_byte_buffer.offset = 0;
    i_byte_buffer.length = mmString_Size(s);
    mmIconvContext_HostToView(iconv_context, &i_byte_buffer, &o_byte_buffer);
    _text_utf8.assign((const CEGUI::utf8*)(o_byte_buffer.buffer + o_byte_buffer.offset), o_byte_buffer.length);

    w->setText(_text_utf8);
}
void mmLayerUtilityIconv_ListItemHostName(struct mmIconvContext* iconv_context, CEGUI::ListboxItem* w, struct mmString* s)
{
    const CEGUI::String& _text_utf8 = w->getText();

    struct mmByteBuffer i_byte_buffer;
    struct mmByteBuffer o_byte_buffer;

    i_byte_buffer.buffer = (mmUInt8_t*)_text_utf8.c_str();
    i_byte_buffer.offset = 0;
    i_byte_buffer.length = _text_utf8.utf8_stream_len();
    mmIconvContext_ViewToHost(iconv_context, &i_byte_buffer, &o_byte_buffer);
    mmString_Assignsn(s, (const char*)(o_byte_buffer.buffer + o_byte_buffer.offset), o_byte_buffer.length);
    // 
    mmString_ReplaceChar(s, '\\', '/');
}
void mmLayerUtilityIconv_ListItemViewName(struct mmIconvContext* iconv_context, CEGUI::ListboxItem* w, struct mmString* s)
{
    CEGUI::String _text_utf8;
    struct mmByteBuffer i_byte_buffer;
    struct mmByteBuffer o_byte_buffer;

    i_byte_buffer.buffer = (mmUInt8_t*)mmString_Data(s);
    i_byte_buffer.offset = 0;
    i_byte_buffer.length = mmString_Size(s);
    mmIconvContext_HostToView(iconv_context, &i_byte_buffer, &o_byte_buffer);
    _text_utf8.assign((const CEGUI::utf8*)(o_byte_buffer.buffer + o_byte_buffer.offset), o_byte_buffer.length);

    w->setText(_text_utf8);
}