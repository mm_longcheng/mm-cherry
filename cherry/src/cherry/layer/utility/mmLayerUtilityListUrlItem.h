/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmLayerUtilityListUrlItem_h__
#define __mmLayerUtilityListUrlItem_h__

#include "core/mmString.h"

#include "CEGUI/Window.h"

#include "CEGUI/widgets/ListboxTextItem.h"

#include "explorer/mmExplorerItem.h"

#include "mmLayerUtilityTextItem.h"

struct mmIconvContext;

namespace mm
{
    // CEGUI::ListboxItem for string
    class mmLayerUtilityListUrlItem
    {
    public:
        mmLayerUtilityTextItem hTextItem;
    public:
        struct mmExplorerItem hUrlItem;
    public:
        struct mmIconvContext* pIconvContext;// weak ref.
    public:
        mmBool_t hHideSuffixName;
    public:
        mmLayerUtilityListUrlItem(void);
        ~mmLayerUtilityListUrlItem(void);
    public:
        void SetUrl(const char* _path, const char* _base);
        void SetItemId(CEGUI::uint _item_id);
        void SetIconvContext(struct mmIconvContext* _iconv_context);
        void SetHideSuffixName(mmBool_t _hide_suffixname);
    public:
        void OnFinishLaunching(void);
        void OnBeforeTerminate(void);
    public:
        void UpdateLayerValue(void);
        void UpdateUtf8View(void);
    };
}

extern void mmVectorValue_ListUrlItemEventProduce(void* obj, void* u);
extern void mmVectorValue_ListUrlItemEventRecycle(void* obj, void* u);

#endif//__mmLayerUtilityListUrlItem_h__

