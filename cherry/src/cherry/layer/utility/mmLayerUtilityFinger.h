/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmLayerUtilityFinger_h__
#define __mmLayerUtilityFinger_h__

#include "nwsi/mmSurfaceInterface.h"

#include "CEGUI/Window.h"

#include "OgreMatrix4.h"

#include "layer/widgets/mmWidgetTransform.h"

#include "container/mmRbtreeU64.h"

struct mmSurfaceMaster;

namespace mm
{
    class mmLayerUtilityFingerTouch;

    class mmLayerUtilityFinger
    {
    public:
        static const int FORCE_VALUE_SIZE = 8;
    public:
        mmLayerUtilityFingerTouch* pFingerTouch;

        // strong ref.
        CEGUI::Window* pImageWindow;
        mmUIntptr_t hMotionId;
        struct mmSurfaceContentTouch hTouch;

        bool hEventUpdated;

        double hHalfXW;
        double hHalfYW;
        double hAbsXW;
        double hAbsYW;

        double hWorldWindowRadius;

        double hForceCurr;
        double hForceAvge;
        double hForceTotal;
        //
        mmUInt8_t hCachePptr;
        mmUInt8_t hCacheGptr;
        //
        double hCacheForce[FORCE_VALUE_SIZE];
    public:
        mmLayerUtilityFinger(void);
    public:
        void SetFingerTouch(mmLayerUtilityFingerTouch* pFingerTouch);
        void SetMotionId(mmUIntptr_t hMotionId);
    public:
        void OnFinishLaunching(void);
        void OnBeforeTerminate(void);
    public:
        void OnEventTouchsMoved(struct mmSurfaceContentTouch* pTouch);
        void OnEventTouchsBegan(struct mmSurfaceContentTouch* pTouch);
        void OnEventTouchsEnded(struct mmSurfaceContentTouch* pTouch);
        void OnEventTouchsBreak(struct mmSurfaceContentTouch* pTouch);
    public:
        void UpdateCache(void);
        void OnEventUpdated(double hInterval);
    private:
        void UpdateEventTouchs(struct mmSurfaceContentTouch* pTouch);
        void UpdateForce(double hForceValue);
        void ResetForce(void);
    };

    class mmLayerUtilityFingerTouch
    {
    public:
        struct mmSurfaceMaster* pSurfaceMaster;
    public:
        std::string hName;
    public:
        // weak ref.
        CEGUI::Window* pBackground;
    public:
        // strong ref.
        CEGUI::Window* pLayerMount;
    public:
        struct mmWidgetTransform hWidgetTransform;
    public:
        std::string hImageSource;
    public:
        float hImageRadius;
        //
        float hForceRangeL;
        float hForceRangeR;
        float hScaleRangeL;
        float hScaleRangeR;
    public:
        // strong ref.
        struct mmRbtreeU64Vpt hRbtreeFinger;
        // weak ref, pointer to finger_map_type.
        struct mmRbtreeU64Vpt hRbtreeTouchs;
    public:
        // cache value for model_data_finger.
        bool hFingerIsVisible;
    public:
        mmLayerUtilityFingerTouch(void);
        ~mmLayerUtilityFingerTouch(void);
    public:
        void SetSurfaceMaster(struct mmSurfaceMaster* pSurfaceMaster);
    public:
        void SetName(const std::string& hName);
        void SetRadius(float hImageRadius);
        void SetForceRange(float hForceRangeL, float hForceRangeR);
        void SetScaleRange(float hScaleRangeL, float hScaleRangeR);
    public:
        void SetBackground(CEGUI::Window* pBackground);
        void SetImageSource(const std::string& hImageSource);
    public:
        void SetFingerIsVisible(bool hVisible);
    public:
        void CreateLayerMount(void);
        void DeleteLayerMount(void);
        void UpdateLayerMountIsVisible(void);

        void UpdateTransform(void);
        void ConvertWorldPixelToWindowPixel(double x, double y, float hPosition[2]);
    public:
        void OnFinishLaunching(void);
        void OnBeforeTerminate(void);
    public:
        void OnEventWindowSizeChanged(struct mmSurfaceContentSizeChange* pContent);
    public:
        void OnEventTouchsMoved(struct mmSurfaceContentTouchs* pTouchs);
        void OnEventTouchsBegan(struct mmSurfaceContentTouchs* pTouchs);
        void OnEventTouchsEnded(struct mmSurfaceContentTouchs* pTouchs);
        void OnEventTouchsBreak(struct mmSurfaceContentTouchs* pTouchs);
    public:
        void OnEventUpdated(double hInterval);
    private:
        mmLayerUtilityFinger* AddFinger(mmUIntptr_t hMotionId);
        mmLayerUtilityFinger* GetFinger(mmUIntptr_t hMotionId);
        mmLayerUtilityFinger* GetFingerInstance(mmUIntptr_t hMotionId);
        void RmvFinger(mmUIntptr_t hMotionId);
        void ClearFinger(void);
    };
}

#endif//__mmLayerUtilityFinger_h__

