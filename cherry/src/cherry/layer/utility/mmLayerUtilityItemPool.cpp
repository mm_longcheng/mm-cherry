/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerUtilityItemPool.h"

#include "CEGUI/Window.h"
#include "CEGUI/WindowManager.h"

#include "layer/director/mmLayerContext.h"

void mmLayerUtilityItemPool_Init(struct mmLayerUtilityItemPool* p)
{
    mmRbtsetVpt_Init(&p->pool);
    mmRbtsetVpt_Init(&p->used);
    mmRbtsetVpt_Init(&p->idle);
    mmString_Init(&p->type);
    p->max_item_number = MM_LAYERUTILITYITEMPOOL_MAX_NUMBER;
    //
    mmString_Assigns(&p->type, "DefaultWindow");
}
void mmLayerUtilityItemPool_Destroy(struct mmLayerUtilityItemPool* p)
{
    assert(0 == p->pool.size && "you must clear all elem before destroy");
    //
    mmRbtsetVpt_Destroy(&p->pool);
    mmRbtsetVpt_Destroy(&p->used);
    mmRbtsetVpt_Destroy(&p->idle);
    mmString_Destroy(&p->type);
    p->max_item_number = MM_LAYERUTILITYITEMPOOL_MAX_NUMBER;
}
//
void mmLayerUtilityItemPool_SetType(struct mmLayerUtilityItemPool* p, const char* type)
{
    mmString_Assigns(&p->type, type);
}
//
void mmLayerUtilityItemPool_OnFinishLaunching(struct mmLayerUtilityItemPool* p)
{

}
void mmLayerUtilityItemPool_OnBeforeTerminate(struct mmLayerUtilityItemPool* p)
{
    mmLayerUtilityItemPool_ClearItem(p);
}

mm::mmLayerContext* mmLayerUtilityItemPool_Produce(struct mmLayerUtilityItemPool* p)
{
    mm::mmLayerContext* e = NULL;

    struct mmRbNode* n = NULL;
    struct mmRbtsetVptIterator* it = NULL;
    // min.
    n = mmRb_Last(&p->idle.rbt);
    if (NULL != n)
    {
        it = (struct mmRbtsetVptIterator*)mmRb_Entry(n, struct mmRbtsetVptIterator, n);
        e = (mm::mmLayerContext*)it->k;
        // rmv chunk to idle set.
        mmRbtsetVpt_Rmv(&p->idle, e);
        // add chunk to used set.
        mmRbtsetVpt_Add(&p->used, e);
    }
    if (NULL == e)
    {
        // add new chunk.
        e = mmLayerUtilityItemPool_AddItem(p);
        // add chunk to used set.
        mmRbtsetVpt_Add(&p->used, e);
    }
    return e;
}
void mmLayerUtilityItemPool_Recycle(struct mmLayerUtilityItemPool* p, mm::mmLayerContext* e)
{
    if (p->pool.size > p->max_item_number)
    {
        // rmv chunk to used set.
        mmRbtsetVpt_Rmv(&p->used, e);
        // rmv.
        mmLayerUtilityItemPool_RmvItem(p, e);
    }
    else
    {
        // rmv chunk to used set.
        mmRbtsetVpt_Rmv(&p->used, e);
        // add chunk to idle set.
        mmRbtsetVpt_Add(&p->idle, e);
    }
}

mm::mmLayerContext* mmLayerUtilityItemPool_AddItem(struct mmLayerUtilityItemPool* p)
{
    CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
    CEGUI::Window* w = pWindowManager->createWindow(mmString_CStr(&p->type));
    mm::mmLayerContext* e = (mm::mmLayerContext*)(w);
    e->OnFinishLaunching();
    mmRbtsetVpt_Add(&p->pool, e);
    return e;
}
void mmLayerUtilityItemPool_RmvItem(struct mmLayerUtilityItemPool* p, mm::mmLayerContext* e)
{
    CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
    mmRbtsetVpt_Rmv(&p->pool, e);
    e->OnBeforeTerminate();
    pWindowManager->destroyWindow(e);
}
void mmLayerUtilityItemPool_ClearItem(struct mmLayerUtilityItemPool* p)
{
    mm::mmLayerContext* e = NULL;
    //
    CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
    // 
    struct mmRbNode* n = NULL;
    struct mmRbtsetVptIterator* it = NULL;
    // max
    n = mmRb_First(&p->pool.rbt);
    while (NULL != n)
    {
        it = (struct mmRbtsetVptIterator*)mmRb_Entry(n, struct mmRbtsetVptIterator, n);
        e = (mm::mmLayerContext*)it->k;
        n = mmRb_Next(n);
        mmRbtsetVpt_Erase(&p->pool, it);
        e->OnBeforeTerminate();
        pWindowManager->destroyWindow(e);
    }
}
