/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerUtilityTextItem.h"

#include "CEGUI/Image.h"
#include "CEGUI/CoordConverter.h"
#include "CEGUI/Font.h"

namespace mm
{
    mmLayerUtilityTextItem::mmLayerUtilityTextItem(void)
        : CEGUI::ListboxTextItem("0", 0, NULL, false, false)
        , hScaleY(1.5f)
    {

    }
    mmLayerUtilityTextItem::mmLayerUtilityTextItem(
        const CEGUI::String& text, 
        CEGUI::uint item_id /* = 0 */, 
        void* item_data /* = 0 */,
        bool disabled /* = false */,
        bool auto_delete /* = true */)
        : CEGUI::ListboxTextItem(text, item_id, item_data, disabled, auto_delete)
        , hScaleY(2.0f)
    {

    }
    mmLayerUtilityTextItem::~mmLayerUtilityTextItem(void)
    {

    }
    void mmLayerUtilityTextItem::SetScaleY(float _scale_y)
    {
        this->hScaleY = _scale_y;
    }
    CEGUI::Sizef mmLayerUtilityTextItem::getPixelSize(void) const
    {
        CEGUI::Sizef _string_pixel_size = CEGUI::ListboxTextItem::getPixelSize();

        _string_pixel_size.d_height *= this->hScaleY;

        return _string_pixel_size;
    }

    void mmLayerUtilityTextItem::draw(
        CEGUI::GeometryBuffer& buffer, 
        const CEGUI::Rectf& targetRect,
        float alpha, 
        const CEGUI::Rectf* clipper) const
    {
        if (d_selected && d_selectBrush != 0)
            d_selectBrush->render(buffer, targetRect, clipper,
                getModulateAlphaColourRect(d_selectCols, alpha));

        const CEGUI::Font* font = getFont();

        if (!font)
            return;

        CEGUI::Vector2f draw_pos(targetRect.getPosition());

        float _font_height = font->getFontHeight();
        draw_pos += CEGUI::Vector2f(0.0f, _font_height * (this->hScaleY - 1.0f) / 2.0f);

        draw_pos.d_y += CEGUI::CoordConverter::alignToPixels(
            (font->getLineSpacing() - font->getFontHeight()) * 0.5f);

        if (!d_renderedStringValid)
            parseTextString();

        const CEGUI::ColourRect final_colours(
            getModulateAlphaColourRect(CEGUI::ColourRect(0xFFFFFFFF), alpha));

        for (size_t i = 0; i < d_renderedString.getLineCount(); ++i)
        {
            d_renderedString.draw(d_owner, i, buffer, draw_pos, &final_colours, clipper, 0.0f);
            draw_pos.d_y += d_renderedString.getPixelSize(d_owner, i).d_height;
        }
    }
}

void mmVectorValue_ListboxItemEventProduce(void* obj, void* u)
{
    CEGUI::ListboxTextItem* e = (CEGUI::ListboxTextItem*)(u);
    new (e) CEGUI::ListboxTextItem("0", 0, NULL, false, false);
}
void mmVectorValue_ListboxItemEventRecycle(void* obj, void* u)
{
    CEGUI::ListboxTextItem* e = (CEGUI::ListboxTextItem*)(u);
    e->~ListboxTextItem();
}

void mmVectorValue_TextItemEventProduce(void* obj, void* u)
{
    mm::mmLayerUtilityTextItem* e = (mm::mmLayerUtilityTextItem*)(u);
    new (e) mm::mmLayerUtilityTextItem("0", 0, NULL, false, false);
}
void mmVectorValue_TextItemEventRecycle(void* obj, void* u)
{
    mm::mmLayerUtilityTextItem* e = (mm::mmLayerUtilityTextItem*)(u);
    e->~mmLayerUtilityTextItem();
}

void mmVectorValue_CEGUIStringEventProduce(void* obj, void* u)
{
    CEGUI::String* e = (CEGUI::String*)(u);
    new (e) CEGUI::String();
}
void mmVectorValue_CEGUIStringEventRecycle(void* obj, void* u)
{
    CEGUI::String* e = (CEGUI::String*)(u);
    e->~String();
}

