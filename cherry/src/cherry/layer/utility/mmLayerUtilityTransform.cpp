/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerUtilityTransform.h"

#include "layer/widgets/mmWidgetTransform.h"

void mmLayerUtilityTransform_Init(struct mmLayerUtilityTransform* p)
{
    p->Matrix4x4WorldPixelToWindowPixel = Ogre::Matrix4::IDENTITY;
    p->Matrix4x4WindowPixelToWorldPixel = Ogre::Matrix4::IDENTITY;
    p->hWorldWindowCenter = Ogre::Vector3::ZERO;
    p->hWorldWindowOA = Ogre::Vector3::ZERO;
    p->hWorldWindowOB = Ogre::Vector3::ZERO;
    p->hWorldWindowA = 0.0;
    p->hWorldWindowB = 0.0;
}
void mmLayerUtilityTransform_Destroy(struct mmLayerUtilityTransform* p)
{
    p->Matrix4x4WorldPixelToWindowPixel = Ogre::Matrix4::IDENTITY;
    p->Matrix4x4WindowPixelToWorldPixel = Ogre::Matrix4::IDENTITY;
    p->hWorldWindowCenter = Ogre::Vector3::ZERO;
    p->hWorldWindowOA = Ogre::Vector3::ZERO;
    p->hWorldWindowOB = Ogre::Vector3::ZERO;
    p->hWorldWindowA = 0.0;
    p->hWorldWindowB = 0.0;
}

void mmLayerUtilityTransform_UpdateTransform(struct mmLayerUtilityTransform* p, CEGUI::Window* pWindow)
{
    Ogre::Vector3 hC(0.5f, 0.5f, 0.0f);
    Ogre::Vector3 hA(1.0f, 0.5f, 0.0f);
    Ogre::Vector3 hB(0.5f, 1.0f, 0.0f);

    p->Matrix4x4WindowPixelToWorldPixel = Ogre::Matrix4::IDENTITY;

    Ogre::Matrix4 Matrix4x4WindowScaleToWindowPixel = Ogre::Matrix4::IDENTITY;
    Ogre::Matrix4 Matrix4x4WindowScaleToWorldPixel = Ogre::Matrix4::IDENTITY;

    mmWidgetTransform_Matrix4x4WindowScaleToWindowPixel(pWindow, &Matrix4x4WindowScaleToWindowPixel);
    mmWidgetTransform_Matrix4x4WindowPixelToWorldPixel(pWindow, &p->Matrix4x4WindowPixelToWorldPixel);

    p->Matrix4x4WorldPixelToWindowPixel = p->Matrix4x4WindowPixelToWorldPixel.inverse();

    Matrix4x4WindowScaleToWorldPixel = p->Matrix4x4WindowPixelToWorldPixel * Matrix4x4WindowScaleToWindowPixel;

    p->hWorldWindowCenter = Matrix4x4WindowScaleToWorldPixel * hC;

    p->hWorldWindowOA = Matrix4x4WindowScaleToWorldPixel * hA - p->hWorldWindowCenter;
    p->hWorldWindowOB = Matrix4x4WindowScaleToWorldPixel * hB - p->hWorldWindowCenter;

    p->hWorldWindowA = p->hWorldWindowOA.length();
    p->hWorldWindowB = p->hWorldWindowOB.length();
}
