/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerUtilityListUrlItem.h"

#include "toolkit/mmIconv.h"

#include "layer/utility/mmLayerUtilityIconv.h"

namespace mm
{
    mmLayerUtilityListUrlItem::mmLayerUtilityListUrlItem(void)
        : hTextItem("0", 0, NULL, false, false)
        , pIconvContext(NULL)
        , hHideSuffixName(MM_FALSE)
    {
        mmExplorerItem_Init(&this->hUrlItem);
    }
    mmLayerUtilityListUrlItem::~mmLayerUtilityListUrlItem(void)
    {
        mmExplorerItem_Destroy(&this->hUrlItem);
    }
    void mmLayerUtilityListUrlItem::SetUrl(const char* _path, const char* _base)
    {
        mmExplorerItem_SetPath(&this->hUrlItem, _path, _base);
        mmExplorerItem_UpdateMode(&this->hUrlItem);
    }
    void mmLayerUtilityListUrlItem::SetItemId(CEGUI::uint _item_id)
    {
        this->hTextItem.setID(_item_id);
    }
    void mmLayerUtilityListUrlItem::SetIconvContext(struct mmIconvContext* _iconv_context)
    {
        this->pIconvContext = _iconv_context;
    }
    void mmLayerUtilityListUrlItem::SetHideSuffixName(mmBool_t _hide_suffixname)
    {
        this->hHideSuffixName = _hide_suffixname;
    }
    void mmLayerUtilityListUrlItem::OnFinishLaunching(void)
    {
        // need do hothing here.
    }
    void mmLayerUtilityListUrlItem::OnBeforeTerminate(void)
    {
        // need do hothing here.
    }
    void mmLayerUtilityListUrlItem::UpdateLayerValue(void)
    {
        this->UpdateUtf8View();
    }
    void mmLayerUtilityListUrlItem::UpdateUtf8View(void)
    {
        if (MM_TRUE == this->hHideSuffixName)
        {
            mmLayerUtilityIconv_ListItemViewName(this->pIconvContext, &this->hTextItem, &this->hUrlItem.prefixname);
        }
        else
        {
            mmLayerUtilityIconv_ListItemViewName(this->pIconvContext, &this->hTextItem, &this->hUrlItem.basename);
        }
    }
}

void mmVectorValue_ListUrlItemEventProduce(void* obj, void* u)
{
    mm::mmLayerUtilityListUrlItem* e = (mm::mmLayerUtilityListUrlItem*)(u);
    new (e) mm::mmLayerUtilityListUrlItem();
}
void mmVectorValue_ListUrlItemEventRecycle(void* obj, void* u)
{
    mm::mmLayerUtilityListUrlItem* e = (mm::mmLayerUtilityListUrlItem*)(u);
    e->~mmLayerUtilityListUrlItem();
}

