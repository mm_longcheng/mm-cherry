/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerUtilityFinger.h"

#include "core/mmAlloc.h"

#include "dish/mmValue.h"

#include "nwsi/mmSurfaceMaster.h"

#include "CEGUI/WindowManager.h"

#include "layer/widgets/mmWidgetTransform.h"

namespace mm
{
    mmLayerUtilityFinger::mmLayerUtilityFinger(void)
        : pFingerTouch(NULL)
        , pImageWindow(NULL)

        , hMotionId(0)
        , hTouch()

        , hEventUpdated(false)

        , hHalfXW(0.0)
        , hHalfYW(0.0)
        , hAbsXW(0.0)
        , hAbsYW(0.0)

        , hWorldWindowRadius(0)

        , hForceCurr(0.0)
        , hForceAvge(0.0)
        , hForceTotal(0.0)

        , hCachePptr(0)
        , hCacheGptr(0)
    {
        mmMemset(this->hCacheForce, 0, sizeof(double) * FORCE_VALUE_SIZE);
    }
    void mmLayerUtilityFinger::SetFingerTouch(mmLayerUtilityFingerTouch* pFingerTouch)
    {
        this->pFingerTouch = pFingerTouch;
    }
    void mmLayerUtilityFinger::SetMotionId(mmUIntptr_t hMotionId)
    {
        this->hMotionId = hMotionId;
    }
    void mmLayerUtilityFinger::OnFinishLaunching(void)
    {
        mmLayerUtilityFingerTouch* pFingerTouch = this->pFingerTouch;

        std::string hIndexString = "__mmFinger";
        hIndexString += "_" + pFingerTouch->hName;
        hIndexString += "_" + mm::mmValue::ToString(this->hMotionId);

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        this->pImageWindow = pWindowManager->createWindow("TaharezLook/StaticImage", hIndexString);

        this->pImageWindow->setProperty("Image", pFingerTouch->hImageSource);
        this->pImageWindow->setProperty("BackgroundEnabled", CEGUI::PropertyHelper<bool>::toString(false));
        this->pImageWindow->setProperty("FrameEnabled", CEGUI::PropertyHelper<bool>::toString(false));
        this->pImageWindow->setProperty("RiseOnClickEnabled", CEGUI::PropertyHelper<bool>::toString(false));
        this->pImageWindow->setProperty("WantsMultiClickEvents", CEGUI::PropertyHelper<bool>::toString(false));
        this->pImageWindow->setProperty("ZOrderingEnabled", CEGUI::PropertyHelper<bool>::toString(false));
        this->pImageWindow->setProperty("MousePassThroughEnabled", CEGUI::PropertyHelper<bool>::toString(true));
        this->pImageWindow->setVisible(false);
        this->pImageWindow->deactivate();
        this->pImageWindow->disable();

        assert(NULL != pFingerTouch->pLayerMount && "pFingerTouch->pLayerMount is a null.");

        pFingerTouch->pLayerMount->addChild(this->pImageWindow);
    }
    void mmLayerUtilityFinger::OnBeforeTerminate(void)
    {
        mmLayerUtilityFingerTouch* pFingerTouch = this->pFingerTouch;
        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        pFingerTouch->pLayerMount->removeChild(this->pImageWindow);
        pWindowManager->destroyWindow(this->pImageWindow);
        this->pImageWindow = NULL;
    }
    void mmLayerUtilityFinger::OnEventTouchsMoved(struct mmSurfaceContentTouch* pTouch)
    {
        this->hTouch = *pTouch;
        this->UpdateEventTouchs(&this->hTouch);
    }
    void mmLayerUtilityFinger::OnEventTouchsBegan(struct mmSurfaceContentTouch* pTouch)
    {
        this->ResetForce();
        //
        this->hTouch = *pTouch;
        this->pImageWindow->setVisible(true);
        this->UpdateEventTouchs(&this->hTouch);
    }
    void mmLayerUtilityFinger::OnEventTouchsEnded(struct mmSurfaceContentTouch* pTouch)
    {
        this->hTouch = *pTouch;
        this->pImageWindow->setVisible(false);
        this->UpdateEventTouchs(&this->hTouch);
    }
    void mmLayerUtilityFinger::OnEventTouchsBreak(struct mmSurfaceContentTouch* pTouch)
    {
        this->hTouch = *pTouch;
        this->pImageWindow->setVisible(false);
        this->UpdateEventTouchs(&this->hTouch);
    }
    void mmLayerUtilityFinger::OnEventUpdated(double interval)
    {
        if (!this->hEventUpdated)
        {
            this->UpdateEventTouchs(&this->hTouch);
        }
        this->hEventUpdated = false;
    }
    void mmLayerUtilityFinger::UpdateEventTouchs(struct mmSurfaceContentTouch* pTouch)
    {
        mmLayerUtilityFingerTouch* pFingerTouch = this->pFingerTouch;
        struct mmSurfaceMaster* pSurfaceMaster = pFingerTouch->pSurfaceMaster;

        double hTransformScale = 1.0;

        float hWindowPixelPosition[2] = { 0 };

        double hForceValue = pTouch->force_value / pTouch->force_maximum;

        // We need check the major_radius and minor_radius.
        hForceValue = (0 == pTouch->major_radius && 0 == pTouch->minor_radius) ? 0.25 : hForceValue;

        this->UpdateForce(hForceValue);
        this->hEventUpdated = true;

        hForceValue = this->hForceAvge;

        double hForceRange = pFingerTouch->hForceRangeR - pFingerTouch->hForceRangeL;
        double hScaleRange = pFingerTouch->hScaleRangeR - pFingerTouch->hScaleRangeL;

        double hForceK = (hForceValue - pFingerTouch->hForceRangeL) / hForceRange;
        double hScaleK = pFingerTouch->hScaleRangeL + hScaleRange * hForceK;

        this->hHalfXW = (pFingerTouch->hImageRadius * hScaleK * hTransformScale);
        this->hHalfYW = (pFingerTouch->hImageRadius * hScaleK * hTransformScale);

        pFingerTouch->ConvertWorldPixelToWindowPixel(pTouch->abs_x, pTouch->abs_y, hWindowPixelPosition);

        this->hAbsXW = pTouch->abs_x;
        this->hAbsYW = pTouch->abs_y;

        this->hWorldWindowRadius = this->hHalfXW;

        CEGUI::UDim hHalfX(0, (float)this->hHalfXW);
        CEGUI::UDim hHalfY(0, (float)this->hHalfYW);
        CEGUI::UVector2 hHalf(hHalfX, hHalfY);

        CEGUI::UDim hDx(0, (float)hWindowPixelPosition[0]);
        CEGUI::UDim hDy(0, (float)hWindowPixelPosition[1]);
        CEGUI::UVector2 hCenter(hDx, hDy);

        CEGUI::URect hArea;
        hArea.d_min = hCenter - hHalf;
        hArea.d_max = hCenter + hHalf;

        this->pImageWindow->setArea(hArea);
        this->pImageWindow->setAlpha((float)hScaleK);
    }

    void mmLayerUtilityFinger::UpdateCache(void)
    {
        size_t idx = 0;
        // assign interval cache for avge interval.
        // we preset the buffer to a full queue average.
        for (idx = 0; idx < FORCE_VALUE_SIZE; ++idx)
        {
            this->hCacheForce[idx] = this->hForceCurr;
        }
        // calculate total time.
        this->hForceTotal = this->hForceCurr * FORCE_VALUE_SIZE;
    }
    void mmLayerUtilityFinger::UpdateForce(double hForceValue)
    {
        // the queue is full cache_length is always (FORCE_VALUE_SIZE - 1).
        static const mmUInt8_t CacheLength = (mmUInt8_t)(FORCE_VALUE_SIZE - 1);

        mmUInt8_t gptr_index = 0;
        mmUInt8_t pptr_index = 0;

        // cache the last frame interval.
        this->hForceCurr = hForceValue;

        // gptr_index = p->cache_gptr % MM_FRAME_STATS_INTERVAL_CACHE_SIZE;
        gptr_index = this->hCacheGptr & CacheLength;
        // gptr++
        this->hCacheGptr++;
        // sub the front interval.
        // interval_total value update.
        this->hForceTotal -= this->hCacheForce[gptr_index];

        // pptr_index = p->cache_pptr % MM_FRAME_STATS_INTERVAL_CACHE_SIZE;
        pptr_index = this->hCachePptr & CacheLength;
        // pptr++
        this->hCachePptr++;
        // add the front interval.
        // hForceTotal value update.
        this->hForceTotal += this->hForceCurr;

        // insert to back current interval.
        this->hCacheForce[pptr_index] = this->hForceCurr;

        // fps value update.
        this->hForceAvge = this->hForceTotal / CacheLength;
    }
    void mmLayerUtilityFinger::ResetForce(void)
    {
        this->hForceCurr = 0;
        this->hForceAvge = 0;
        this->hForceTotal = 0;

        this->hCachePptr = 0;
        this->hCacheGptr = 0;

        mmMemset(this->hCacheForce, 0, sizeof(double) * FORCE_VALUE_SIZE);
    }

    mmLayerUtilityFingerTouch::mmLayerUtilityFingerTouch(void)
        : pSurfaceMaster(NULL)

        , hName("mm")

        , pBackground(NULL)
        , pLayerMount(NULL)

        , hImageSource("")

        , hImageRadius(256.0f)

        , hForceRangeL(0.0f)
        , hForceRangeR(1.0f)
        , hScaleRangeL(0.0f)
        , hScaleRangeR(1.0f)

        , hFingerIsVisible(false)
    {
        mmWidgetTransform_Init(&this->hWidgetTransform);
        mmRbtreeU64Vpt_Init(&this->hRbtreeFinger);
        mmRbtreeU64Vpt_Init(&this->hRbtreeTouchs);
    }
    mmLayerUtilityFingerTouch::~mmLayerUtilityFingerTouch(void)
    {
        assert(NULL == this->pLayerMount && "you must destroy the layer mount before terminate.");

        mmWidgetTransform_Destroy(&this->hWidgetTransform);
        mmRbtreeU64Vpt_Destroy(&this->hRbtreeFinger);
        mmRbtreeU64Vpt_Destroy(&this->hRbtreeTouchs);
    }
    void mmLayerUtilityFingerTouch::SetSurfaceMaster(struct mmSurfaceMaster* pSurfaceMaster)
    {
        this->pSurfaceMaster = pSurfaceMaster;
    }
    void mmLayerUtilityFingerTouch::SetName(const std::string& hName)
    {
        this->hName = hName;
    }
    void mmLayerUtilityFingerTouch::SetRadius(float hImageRadius)
    {
        this->hImageRadius = hImageRadius;
    }
    void mmLayerUtilityFingerTouch::SetForceRange(float hForceRangeL, float hForceRangeR)
    {
        this->hForceRangeL = hForceRangeL;
        this->hForceRangeR = hForceRangeR;
    }
    void mmLayerUtilityFingerTouch::SetScaleRange(float hScaleRangeL, float hScaleRangeR)
    {
        this->hScaleRangeL = hScaleRangeL;
        this->hScaleRangeR = hScaleRangeR;
    }
    void mmLayerUtilityFingerTouch::SetBackground(CEGUI::Window* pBackground)
    {
        this->pBackground = pBackground;
    }
    void mmLayerUtilityFingerTouch::SetImageSource(const std::string& hImageSource)
    {
        this->hImageSource = hImageSource;
    }
    void mmLayerUtilityFingerTouch::SetFingerIsVisible(bool hVisible)
    {
        // not need unlink before assign operate.

        this->hFingerIsVisible = hVisible;
    }
    void mmLayerUtilityFingerTouch::CreateLayerMount(void)
    {
        if (NULL != this->pBackground)
        {
            std::string hIndexString = "__mmLayerMountFinger";
            hIndexString += "_" + this->hName;

            CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
            this->pLayerMount = pWindowManager->createWindow("DefaultWindow", hIndexString);
            this->pLayerMount->setProperty("RiseOnClickEnabled", CEGUI::PropertyHelper<bool>::toString(false));
            this->pLayerMount->setProperty("WantsMultiClickEvents", CEGUI::PropertyHelper<bool>::toString(false));
            this->pLayerMount->setProperty("ZOrderingEnabled", CEGUI::PropertyHelper<bool>::toString(false));
            this->pLayerMount->setProperty("MousePassThroughEnabled", CEGUI::PropertyHelper<bool>::toString(true));
            this->pLayerMount->setAlwaysOnTop(true);
            this->pLayerMount->deactivate();
            this->pLayerMount->disable();
            this->pLayerMount->setVisible(this->hFingerIsVisible);
            this->pBackground->addChild(this->pLayerMount);
            // transform.
            this->UpdateTransform();
        }
    }
    void mmLayerUtilityFingerTouch::DeleteLayerMount(void)
    {
        if (NULL != this->pBackground)
        {
            this->ClearFinger();

            CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
            this->pBackground->removeChild(this->pLayerMount);
            pWindowManager->destroyWindow(this->pLayerMount);
            this->pLayerMount = NULL;
        }
    }
    void mmLayerUtilityFingerTouch::UpdateLayerMountIsVisible(void)
    {
        if (NULL != this->pLayerMount)
        {
            this->pLayerMount->setVisible(this->hFingerIsVisible);
        }
    }
    void mmLayerUtilityFingerTouch::UpdateTransform(void)
    {
        mmWidgetTransform_UpdateWindow(&this->hWidgetTransform, this->pLayerMount);
    }
    void mmLayerUtilityFingerTouch::ConvertWorldPixelToWindowPixel(double x, double y, float hPosition[2])
    {
        mmWidgetTransform_WorldPixelToWindowPixelPosition(&this->hWidgetTransform, x, y, hPosition);
    }
    void mmLayerUtilityFingerTouch::OnFinishLaunching(void)
    {
        this->CreateLayerMount();

        this->UpdateLayerMountIsVisible();
    }
    void mmLayerUtilityFingerTouch::OnBeforeTerminate(void)
    {
        this->DeleteLayerMount();
    }
    void mmLayerUtilityFingerTouch::OnEventWindowSizeChanged(struct mmSurfaceContentSizeChange* pContent)
    {
        this->UpdateTransform();
    }
    void mmLayerUtilityFingerTouch::OnEventTouchsMoved(struct mmSurfaceContentTouchs* pTouchs)
    {
        size_t i = 0;

        struct mmSurfaceContentTouch* pTouch = NULL;

        mmLayerUtilityFinger* pFinger = NULL;

        struct mmRbtreeU64VptIterator* it = NULL;

        for (i = 0; i < pTouchs->context.size; i++)
        {
            pTouch = (struct mmSurfaceContentTouch*)mmVectorValue_GetIndex(&pTouchs->context, i);

            it = mmRbtreeU64Vpt_GetIterator(&this->hRbtreeTouchs, pTouch->motion_id);

            if (NULL != it)
            {
                pFinger = (mmLayerUtilityFinger*)it->v;
                pFinger->OnEventTouchsMoved(pTouch);
            }
        }
    }
    void mmLayerUtilityFingerTouch::OnEventTouchsBegan(struct mmSurfaceContentTouchs* pTouchs)
    {
        size_t i = 0;

        struct mmSurfaceContentTouch* pTouch = NULL;

        mmLayerUtilityFinger* pFinger = NULL;

        struct mmRbtreeU64VptIterator* it = NULL;

        for (i = 0; i < pTouchs->context.size; i++)
        {
            pTouch = (struct mmSurfaceContentTouch*)mmVectorValue_GetIndex(&pTouchs->context, i);

            it = mmRbtreeU64Vpt_GetIterator(&this->hRbtreeTouchs, pTouch->motion_id);

            if (NULL != it)
            {
                pFinger = (mmLayerUtilityFinger*)it->v;
                pFinger->OnEventTouchsBegan(pTouch);
            }
            else
            {
                pFinger = this->GetFingerInstance(pTouch->motion_id);
                pFinger->OnEventTouchsBegan(pTouch);

                // new unit.
                mmRbtreeU64Vpt_Set(&this->hRbtreeTouchs, pTouch->motion_id, pFinger);
            }
        }
    }
    void mmLayerUtilityFingerTouch::OnEventTouchsEnded(struct mmSurfaceContentTouchs* pTouchs)
    {
        size_t i = 0;

        struct mmSurfaceContentTouch* pTouch = NULL;

        mmLayerUtilityFinger* pFinger = NULL;

        for (i = 0; i < pTouchs->context.size; i++)
        {
            pTouch = (struct mmSurfaceContentTouch*)mmVectorValue_GetIndex(&pTouchs->context, i);

            pFinger = this->GetFingerInstance(pTouch->motion_id);
            pFinger->OnEventTouchsEnded(pTouch);

            // remove.
            mmRbtreeU64Vpt_Rmv(&this->hRbtreeTouchs, pTouch->motion_id);
        }
    }
    void mmLayerUtilityFingerTouch::OnEventTouchsBreak(struct mmSurfaceContentTouchs* pTouchs)
    {
        size_t i = 0;

        struct mmSurfaceContentTouch* pTouch = NULL;

        mmLayerUtilityFinger* pFinger = NULL;

        for (i = 0; i < pTouchs->context.size; i++)
        {
            pTouch = (struct mmSurfaceContentTouch*)mmVectorValue_GetIndex(&pTouchs->context, i);

            pFinger = this->GetFingerInstance(pTouch->motion_id);
            pFinger->OnEventTouchsBreak(pTouch);

            // remove.
            mmRbtreeU64Vpt_Rmv(&this->hRbtreeTouchs, pTouch->motion_id);
        }
    }
    void mmLayerUtilityFingerTouch::OnEventUpdated(double hInterval)
    {
        mmLayerUtilityFinger* u = NULL;

        struct mmRbNode* n = NULL;
        struct mmRbtreeU64VptIterator* it = NULL;
        //
        n = mmRb_First(&this->hRbtreeTouchs.rbt);
        while (NULL != n)
        {
            it = (struct mmRbtreeU64VptIterator*)mmRb_Entry(n, struct mmRbtreeU64VptIterator, n);
            n = mmRb_Next(n);
            // 
            u = (mmLayerUtilityFinger*)it->v;
            //
            u->OnEventUpdated(hInterval);
        }
    }
    mmLayerUtilityFinger* mmLayerUtilityFingerTouch::AddFinger(mmUIntptr_t hMotionId)
    {
        mmLayerUtilityFinger* u = NULL;
        struct mmRbtreeU64VptIterator* it = NULL;

        it = mmRbtreeU64Vpt_GetIterator(&this->hRbtreeFinger, hMotionId);
        if (NULL == it)
        {
            u = new mmLayerUtilityFinger;
            u->SetFingerTouch(this);
            u->SetMotionId(hMotionId);
            u->OnFinishLaunching();
            mmRbtreeU64Vpt_Set(&this->hRbtreeFinger, hMotionId, u);
        }
        else
        {
            u = (mmLayerUtilityFinger*)it->v;
        }
        return u;
    }
    mmLayerUtilityFinger* mmLayerUtilityFingerTouch::GetFinger(mmUIntptr_t hMotionId)
    {
        mmLayerUtilityFinger* u = NULL;
        struct mmRbtreeU64VptIterator* it = NULL;

        it = mmRbtreeU64Vpt_GetIterator(&this->hRbtreeFinger, hMotionId);
        if (NULL != it)
        {
            u = (mmLayerUtilityFinger*)it->v;
        }
        return u;
    }
    mmLayerUtilityFinger* mmLayerUtilityFingerTouch::GetFingerInstance(mmUIntptr_t hMotionId)
    {
        mmLayerUtilityFinger* u = this->GetFinger(hMotionId);
        if (NULL == u)
        {
            u = this->AddFinger(hMotionId);
        }
        return u;
    }
    void mmLayerUtilityFingerTouch::RmvFinger(mmUIntptr_t hMotionId)
    {
        mmLayerUtilityFinger* u = NULL;
        struct mmRbtreeU64VptIterator* it = NULL;

        it = mmRbtreeU64Vpt_GetIterator(&this->hRbtreeFinger, hMotionId);
        if (NULL != it)
        {
            u = (mmLayerUtilityFinger*)it->v;
            u->OnBeforeTerminate();
            mmRbtreeU64Vpt_Erase(&this->hRbtreeFinger, it);
            delete u;
        }
    }
    void mmLayerUtilityFingerTouch::ClearFinger(void)
    {
        mmLayerUtilityFinger* u = NULL;

        struct mmRbNode* n = NULL;
        struct mmRbtreeU64VptIterator* it = NULL;
        //
        n = mmRb_First(&this->hRbtreeTouchs.rbt);
        while (NULL != n)
        {
            it = (struct mmRbtreeU64VptIterator*)mmRb_Entry(n, struct mmRbtreeU64VptIterator, n);
            n = mmRb_Next(n);
            // 
            u = (mmLayerUtilityFinger*)it->v;
            u->OnBeforeTerminate();
            mmRbtreeU64Vpt_Erase(&this->hRbtreeFinger, it);
            delete u;
        }
    }
}
