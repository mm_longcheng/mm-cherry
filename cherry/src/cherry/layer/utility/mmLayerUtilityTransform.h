/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmLayerUtilityTransform_h__
#define __mmLayerUtilityTransform_h__

#include "CEGUI/Window.h"

#include "OgreMatrix4.h"

#include "layer/director/mmLayerContext.h"

struct mmLayerUtilityTransform
{
    Ogre::Matrix4 Matrix4x4WorldPixelToWindowPixel;
    Ogre::Matrix4 Matrix4x4WindowPixelToWorldPixel;

    Ogre::Vector3 hWorldWindowCenter;
    Ogre::Vector3 hWorldWindowOA;
    Ogre::Vector3 hWorldWindowOB;

    double hWorldWindowA;
    double hWorldWindowB;
};
extern void mmLayerUtilityTransform_Init(struct mmLayerUtilityTransform* p);
extern void mmLayerUtilityTransform_Destroy(struct mmLayerUtilityTransform* p);

extern void mmLayerUtilityTransform_UpdateTransform(struct mmLayerUtilityTransform* p, CEGUI::Window* pWindow);

#endif//__mmLayerUtilityTransform_h__

