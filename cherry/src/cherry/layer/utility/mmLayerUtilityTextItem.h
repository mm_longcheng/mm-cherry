/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmLayerUtilityTextItem_h__
#define __mmLayerUtilityTextItem_h__

#include "CEGUI/widgets/ListboxTextItem.h"
#include "CEGUI/Vector.h"

namespace mm
{
    class mmLayerUtilityTextItem : public CEGUI::ListboxTextItem
    {
    public:
        float hScaleY;
    public:
        mmLayerUtilityTextItem(void);

        mmLayerUtilityTextItem(
            const CEGUI::String& text, 
            CEGUI::uint item_id = 0, 
            void* item_data = 0, 
            bool disabled = false, 
            bool auto_delete = true);

        virtual ~mmLayerUtilityTextItem(void);
    public:
        void SetScaleY(float _scale_y);
    public:
        // CEGUI::ListboxTextItem
        virtual CEGUI::Sizef getPixelSize(void) const;
        virtual void draw(
            CEGUI::GeometryBuffer& buffer, 
            const CEGUI::Rectf& targetRect,
            float alpha, 
            const CEGUI::Rectf* clipper) const;
    };
}

extern void mmVectorValue_ListboxItemEventProduce(void* obj, void* u);
extern void mmVectorValue_ListboxItemEventRecycle(void* obj, void* u);

extern void mmVectorValue_TextItemEventProduce(void* obj, void* u);
extern void mmVectorValue_TextItemEventRecycle(void* obj, void* u);

extern void mmVectorValue_CEGUIStringEventProduce(void* obj, void* u);
extern void mmVectorValue_CEGUIStringEventRecycle(void* obj, void* u);

#endif//__mmLayerUtilityTextItem_h__

