/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerUtilityIntersect.h"

bool mmLayerUtilityIntersect_FingerCircleEllipse(struct mmLayerUtilityTransform* pTransform, mm::mmLayerUtilityFinger* pFinger, Ogre::Vector3& hCenterToTouch)
{
    // intersect circle ellipse.
    Ogre::Vector3 hTouchPosition;

    hTouchPosition.x = (float)pFinger->hAbsXW;
    hTouchPosition.y = (float)pFinger->hAbsYW;
    hTouchPosition.z = (float)0.0f;

    hCenterToTouch = hTouchPosition - pTransform->hWorldWindowCenter;

    // (x0 * x0 / (a + r) * (a + r)) + (y0 * y0 / (b + r) * (b + r)) < 1

    double hXSquare = hCenterToTouch.x * hCenterToTouch.x;
    double hYSquare = hCenterToTouch.y * hCenterToTouch.y;

    double hWorldWindowRadius = pFinger->hWorldWindowRadius;

    bool b0 = false;

    double ar = pTransform->hWorldWindowA + hWorldWindowRadius;
    double br = pTransform->hWorldWindowB + hWorldWindowRadius;

    double hArSquare = ar * ar;
    double hBrSquare = br * br;

    b0 = 1 > (hXSquare / hArSquare) + (hYSquare / hBrSquare);

    return b0;
}
bool mmLayerUtilityIntersect_FingerCircleRectangle(struct mmLayerUtilityTransform* pTransform, mm::mmLayerUtilityFinger* pFinger, Ogre::Vector3& hCenterToTouch)
{
    // intersect circle rectangle.
    Ogre::Vector3 hTouchPosition;

    hTouchPosition.x = (float)pFinger->hAbsXW;
    hTouchPosition.y = (float)pFinger->hAbsYW;
    hTouchPosition.z = (float)0.0f;

    hCenterToTouch = hTouchPosition - pTransform->hWorldWindowCenter;

    Ogre::Vector3 _v(fabs(hCenterToTouch.x), fabs(hCenterToTouch.y), 0.0f);

    Ogre::Vector3 _h;
    _h.x = (float)pTransform->hWorldWindowA;
    _h.y = (float)pTransform->hWorldWindowB;
    _h.z = (float)0.0f;

    Ogre::Vector3 _ac = _v - _h;
    _ac.x = _ac.x < 0 ? 0 : _ac.x;
    _ac.y = _ac.y < 0 ? 0 : _ac.y;
    _ac.z = _ac.z < 0 ? 0 : _ac.z;

    double hWorldWindowRadius = pFinger->hWorldWindowRadius;

    double _square_radius = hWorldWindowRadius * hWorldWindowRadius;

    return _ac.squaredLength() < _square_radius;
}
bool mmLayerUtilityIntersect_FingerPointEllipse(struct mmLayerUtilityTransform* pTransform, mm::mmLayerUtilityFinger* pFinger, Ogre::Vector3& hCenterToTouch)
{
    // intersect point ellipse.
    Ogre::Vector3 hTouchPosition;

    hTouchPosition.x = (float)pFinger->hAbsXW;
    hTouchPosition.y = (float)pFinger->hAbsYW;
    hTouchPosition.z = (float)0.0f;

    hCenterToTouch = hTouchPosition - pTransform->hWorldWindowCenter;

    // (x0 * x0 / (a + r) * (a + r)) + (y0 * y0 / (b + r) * (b + r)) < 1

    double x_square = hCenterToTouch.x * hCenterToTouch.x;
    double y_square = hCenterToTouch.y * hCenterToTouch.y;

    double hWorldWindowRadius = 1.0;

    bool b0 = false;

    double ar = pTransform->hWorldWindowA + hWorldWindowRadius;
    double br = pTransform->hWorldWindowB + hWorldWindowRadius;

    double ar_square = ar * ar;
    double br_square = br * br;

    b0 = 1 > (x_square / ar_square) + (y_square / br_square);

    return b0;
}
bool mmLayerUtilityIntersect_FingerPointRectangle(struct mmLayerUtilityTransform* pTransform, mm::mmLayerUtilityFinger* pFinger, Ogre::Vector3& hCenterToTouch)
{
    // intersect point rectangle.
    Ogre::Vector3 hTouchPosition;

    hTouchPosition.x = (float)pFinger->hAbsXW;
    hTouchPosition.y = (float)pFinger->hAbsYW;
    hTouchPosition.z = (float)0.0f;

    hCenterToTouch = hTouchPosition - pTransform->hWorldWindowCenter;

    Ogre::Vector3 _v(fabs(hCenterToTouch.x), fabs(hCenterToTouch.y), 0.0f);

    Ogre::Vector3 _h;
    _h.x = (float)pTransform->hWorldWindowA;
    _h.y = (float)pTransform->hWorldWindowB;
    _h.z = (float)0.0f;

    Ogre::Vector3 _ac = _v - _h;
    _ac.x = _ac.x < 0 ? 0 : _ac.x;
    _ac.y = _ac.y < 0 ? 0 : _ac.y;
    _ac.z = _ac.z < 0 ? 0 : _ac.z;

    double hWorldWindowRadius = 1.0;

    double _square_radius = hWorldWindowRadius * hWorldWindowRadius;

    return _ac.squaredLength() < _square_radius;
}
