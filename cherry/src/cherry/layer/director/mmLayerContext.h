/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmLayerContext_h__
#define __mmLayerContext_h__

#include "nwsi/mmContextMaster.h"
#include "nwsi/mmSurfaceMaster.h"

#include "nwsi/mmSurfaceInterface.h"

#include "CEGUI/widgets/DefaultWindow.h"

#include <string>
#include <vector>

struct mmLayerDirector;

namespace mm
{
    /*!
    \brief
    EventArgs based class that is used for objects passed to input event handlers
    concerning touchs input.
    */
    class mmTouchsEventArgs : public CEGUI::WindowEventArgs
    {
    public:
        mmTouchsEventArgs(CEGUI::Window* wnd, struct mmSurfaceContentTouchs* _content)
            : CEGUI::WindowEventArgs(wnd) 
            , content(_content)
        {

        }

        struct mmSurfaceContentTouchs* content;//!< holds current touchs information.
    };

    /*!
    \brief
    EventArgs based class that is used for objects passed to input event handlers
    concerning touchs input.
    */
    class mmCursorEventArgs : public CEGUI::WindowEventArgs
    {
    public:
        mmCursorEventArgs(CEGUI::Window* wnd, struct mmSurfaceContentCursor* _content)
            : CEGUI::WindowEventArgs(wnd) 
            , content(_content)
        {

        }

        struct mmSurfaceContentCursor* content;//!< holds current touchs information.
    };

    /*!
    \brief
    EventArgs based class that is used for objects passed to input event handlers
    concerning touchs input.
    */
    class mmKeypadEventArgs : public CEGUI::WindowEventArgs
    {
    public:
        mmKeypadEventArgs(CEGUI::Window* wnd, struct mmSurfaceContentKeypad* _content)
            : CEGUI::WindowEventArgs(wnd) 
            , content(_content)
        {

        }

        struct mmSurfaceContentKeypad* content;//!< holds current touchs information.
    };

    class mmLayerContext : public CEGUI::DefaultWindow
    {
    public:
        static const CEGUI::String EventNamespace;          //!< Namespace for global events
        static const CEGUI::String WidgetTypeName;          //!< Window factory name
    public:
        /**
        * Event fired when the touchs moves within the area of the Window.
        * Handlers are passed a const mmTouchsEventArgs reference with all fields
        * valid.
        */
        static const CEGUI::String EventTouchsMoved;

        /**
        * Event fired when the touchs moves began the area of the Window.
        * Handlers are passed a const mmTouchsEventArgs reference with all fields
        * valid.
        */
        static const CEGUI::String EventTouchsBegan;

        /**
        * Event fired when the touchs moves ended the area of the Window.
        * Handlers are passed a const mmTouchsEventArgs reference with all fields
        * valid.
        */
        static const CEGUI::String EventTouchsEnded;

        /**
        * Event fired when the touchs moves cancel the area of the Window.
        * Handlers are passed a const mmTouchsEventArgs reference with all fields
        * valid.
        */
        static const CEGUI::String EventTouchsBreak;

        /**
        * Event fired when the cursor moves moved the area of the Window.
        * Handlers are passed a const mmCursorEventArgs reference with all fields
        * valid.
        */
        static const CEGUI::String EventCursorMoved;

        /**
        * Event fired when the cursor moves began the area of the Window.
        * Handlers are passed a const mmCursorEventArgs reference with all fields
        * valid.
        */
        static const CEGUI::String EventCursorBegan;

        /**
        * Event fired when the cursor moves ended the area of the Window.
        * Handlers are passed a const mmCursorEventArgs reference with all fields
        * valid.
        */
        static const CEGUI::String EventCursorEnded;

        /**
        * Event fired when the cursor moves ended the area of the Window.
        * Handlers are passed a const mmCursorEventArgs reference with all fields
        * valid.
        */
        static const CEGUI::String EventCursorBreak;

        /**
        * Event fired when the keypad pressed the area of the Window.
        * Handlers are passed a const mmKeypadEventArgs reference with all fields
        * valid.
        */
        static const CEGUI::String EventKeypadPressed;

        /**
        * Event fired when the keypad release the area of the Window.
        * Handlers are passed a const mmKeypadEventArgs reference with all fields
        * valid.
        */
        static const CEGUI::String EventKeypadRelease;

    public:
        //! Name of property to access for the window ignores touchs events and pass them through to any windows behind it.
        static const CEGUI::String TouchsPassThroughEnabledPropertyName;
        //! Name of property to access for the window ignores cursor events and pass them through to any windows behind it.
        static const CEGUI::String CursorPassThroughEnabledPropertyName;
        //! Name of property to access for the window ignores keypad events and pass them through to any windows behind it.
        static const CEGUI::String KeypadPassThroughEnabledPropertyName;
    public:
        struct mmLayerDirector* pDirector;

        struct mmContextMaster* pContextMaster;
        struct mmSurfaceMaster* pSurfaceMaster;
    public:
        //! definition of type used for the list of child windows to be receive context event.
        typedef std::vector<mmLayerContext*> ChildContextVectorType;
        ChildContextVectorType hChildContextVector;
    public:
        //! whether (most) touchs events pass through this window.
        bool hTouchsPassThroughEnabled;
        //! whether (most) cursor events pass through this window.
        bool hCursorPassThroughEnabled;
        //! whether (most) keypad events pass through this window.
        bool hKeypadPassThroughEnabled;
    public:
        mmLayerContext(const CEGUI::String& type, const CEGUI::String& name);
        virtual ~mmLayerContext();
    public:
        void SetDirector(struct mmLayerDirector* pDirector);
        void SetContextMaster(struct mmContextMaster* pContextMaster);
        void SetSurfaceMaster(struct mmSurfaceMaster* pSurfaceMaster);
    public:
        void SetTouchsPassThroughEnabled(bool _enable);
        bool GetTouchsPassThroughEnabled() const;

        void SetCursorPassThroughEnabled(bool _enable);
        bool GetCursorPassThroughEnabled() const;

        void SetKeypadPassThroughEnabled(bool _enable);
        bool GetKeypadPassThroughEnabled() const;
    public:
        // context is ours customize events, most of time will break if spacing common CEGUI::Window object.
        void AttachContextEvent(mmLayerContext* _layer_context);
        void DetachContextEvent(mmLayerContext* _layer_context);
    public:
        virtual void OnFinishLaunching();
        virtual void OnBeforeTerminate();
    public:
        virtual void OnTouchsMoved(mmTouchsEventArgs& args);
        virtual void OnTouchsBegan(mmTouchsEventArgs& args);
        virtual void OnTouchsEnded(mmTouchsEventArgs& args);
        virtual void OnTouchsBreak(mmTouchsEventArgs& args);

        virtual void OnCursorMoved(mmCursorEventArgs& args);
        virtual void OnCursorBegan(mmCursorEventArgs& args);
        virtual void OnCursorEnded(mmCursorEventArgs& args);
        virtual void OnCursorBreak(mmCursorEventArgs& args);

        virtual void OnKeypadPressed(mmKeypadEventArgs& args);
        virtual void OnKeypadRelease(mmKeypadEventArgs& args);
    protected:
        //! helper to update touchs input handled state
        void UpdateTouchsEventHandled(CEGUI::EventArgs& args) const;
        //! helper to update cursor input handled state
        void UpdateCursorEventHandled(CEGUI::EventArgs& args) const;
        //! helper to update keypad input handled state
        void UpdateKeypadEventHandled(CEGUI::EventArgs& args) const;
    protected:
        bool OnElementEventChildAdded(const CEGUI::EventArgs& args);
        bool OnElementEventChildRemoved(const CEGUI::EventArgs& args);
    };
}

void mmCEGUIWindow_OnFireEvent(
    CEGUI::Window* pWindow,
    const CEGUI::String& hEventName,
    CEGUI::WindowEventArgs& hArgs,
    const CEGUI::String& hEventNamespace,
    const CEGUI::String& hWidgetTypeName);

void mmCEGUIWindow_OnHandleEvent(
    CEGUI::Window* pWindow,
    const CEGUI::String& hEventName,
    CEGUI::WindowEventArgs& hArgs,
    const CEGUI::String& hEventNamespace,
    const CEGUI::String& hWidgetTypeName);

#endif//__mmLayerContext_h__

