/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerConfiguration.h"
#include "layer/director/mmLayerDirector.h"

#include "CEGUI/SchemeManager.h"
#include "CEGUI/FontManager.h"
#include "CEGUI/WindowManager.h"

void mmLayerConfiguration_OnFinishLaunching(struct mmLayerDirector* pLayerDirector)
{
    struct mmContextMaster* pContextMaster = pLayerDirector->pContextMaster;
    struct mmSurfaceMaster* pSurfaceMaster = pLayerDirector->pSurfaceMaster;
    struct mmCEGUISystem* pCEGUISystem = &pContextMaster->hCEGUISystem;
    CEGUI::GUIContext* pGUIContext = pSurfaceMaster->pGUIContext;

    /*----------------------------------------------------------------------*/
    CEGUI::SchemeManager* pSchemeManager = CEGUI::SchemeManager::getSingletonPtr();
    CEGUI::FontManager* pFontManager = CEGUI::FontManager::getSingletonPtr();
    /*----------------------------------------------------------------------*/
    // CEGUI relies on various systems being set-up, so this is what we do
    // here first.
    //
    // The first thing to do is load a CEGUI 'scheme' this is basically a file
    // that groups all the required resources and definitions for a particular
    // skin so they can be loaded / initialised easily
    //
    // So, we use the SchemeManager singleton to load in a scheme that loads the
    // imagery and registers widgets for the TaharezLook skin.  This scheme also
    // loads in a font that gets used as the system default.
    pSchemeManager->createFromFile("TaharezLook.scheme");
    //
    pSchemeManager->createFromFile("mmCherry.scheme");
    pSchemeManager->createFromFile("mmWidgets.scheme");
    pSchemeManager->createFromFile("mmRoot.scheme");
    pSchemeManager->createFromFile("mmCommon.scheme");
    pSchemeManager->createFromFile("mmKeyboard.scheme");
    pSchemeManager->createFromFile("mmExplorer.scheme");
    pSchemeManager->createFromFile("mmEmulator.scheme");
    pSchemeManager->createFromFile("mmText.scheme");

    // create the default surface font.
    CEGUI::Font& hDefaultFont = pFontManager->createFromFile("wqy-microhei-15.font");

    // The next thing we do is to set a default mouse cursor image.  This is
    // not strictly essential, although it is nice to always have a visible
    // cursor if a window or widget does not explicitly set one of its own.
    //
    // The TaharezLook Imageset contains an Image named "MouseArrow" which is
    // the ideal thing to have as a defult mouse cursor image.
    CEGUI::MouseCursor& hMouseCursor = pGUIContext->getMouseCursor();
    hMouseCursor.setDefaultImage("TaharezLook/MouseArrow");
    hMouseCursor.setVisible(false);

    // load font and setup default if not loaded via scheme
    // Set default font for the gui context
    pGUIContext->setDefaultFont(&hDefaultFont);
}
void mmLayerConfiguration_OnBeforeTerminate(struct mmLayerDirector* pLayerDirector)
{
    // destroy all windows manual.
    CEGUI::WindowManager* WindowManager = CEGUI::WindowManager::getSingletonPtr();
    // destroy all windows.
    WindowManager->destroyAllWindows();
    // clear dead pool immediately,make sure malloc can free.
    WindowManager->cleanDeadPool();
}

