/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmLayerDirector_h__
#define __mmLayerDirector_h__

#include "core/mmCore.h"

#include "container/mmRbtreeString.h"
#include "container/mmListVpt.h"

#include "mmLayerContext.h"

struct mmLayerDirector
{
    struct mmRbtreeStringVpt hExclusiveWindowPool;
    struct mmListVpt hExclusiveWindowUsed;
    struct mmListVpt hExclusiveWindowIdle;

    struct mmRbtreeStringVpt hMultiplexWindowPool;
    struct mmListVpt hMultiplexWindowUsed;
    struct mmListVpt hMultiplexWindowIdle;

    struct mmRbtreeStringVpt hLayerPool;

    struct mmContextMaster* pContextMaster;
    struct mmSurfaceMaster* pSurfaceMaster;

    // current window.
    mm::mmLayerContext* pCurrentWindow;
    // root window.
    mm::mmLayerContext* pRootWindow;
    // root safety.
    CEGUI::Window* pRootSafety;
};

void mmLayerDirector_Init(struct mmLayerDirector* p);
void mmLayerDirector_Destroy(struct mmLayerDirector* p);

void mmLayerDirector_SetContextMaster(struct mmLayerDirector* p, struct mmContextMaster* pContextMaster);
void mmLayerDirector_SetSurfaceMaster(struct mmLayerDirector* p, struct mmSurfaceMaster* pSurfaceMaster);

void mmLayerDirector_OnFinishLaunching(struct mmLayerDirector* p);
void mmLayerDirector_OnBeforeTerminate(struct mmLayerDirector* p);

// assign mmLayerContext attributes for this director.
void mmLayerDirector_AssignmentLayerContext(struct mmLayerDirector* p, mm::mmLayerContext* w);

void mmLayerDirector_SetRootSafety(struct mmLayerDirector* p, CEGUI::Window* pRootSafety);
void mmLayerDirector_SetRootWindowLayer(struct mmLayerDirector* p, mm::mmLayerContext* pRootWindow);
void mmLayerDirector_SetRootWindow(struct mmLayerDirector* p, const char* type, const char* name);
void mmLayerDirector_RootWindowClear(struct mmLayerDirector* p);

mm::mmLayerContext* mmLayerDirector_SceneExclusiveBackground(struct mmLayerDirector* p, const char* type, const char* name);
mm::mmLayerContext* mmLayerDirector_SceneExclusiveForeground(struct mmLayerDirector* p, const char* type, const char* name);

void mmLayerDirector_SceneExclusiveEnterBackground(struct mmLayerDirector* p, const char* name);
void mmLayerDirector_SceneExclusiveEnterForeground(struct mmLayerDirector* p, const char* name);

mm::mmLayerContext* mmLayerDirector_SceneMultiplexForeground(struct mmLayerDirector* p, const char* type, const char* name);
mm::mmLayerContext* mmLayerDirector_SceneMultiplexBackground(struct mmLayerDirector* p, const char* name);
void mmLayerDirector_SceneMultiplexTerminate(struct mmLayerDirector* p, const char* name);

mm::mmLayerContext* mmLayerDirector_ScenePush(struct mmLayerDirector* p, const char* type, const char* name);
mm::mmLayerContext* mmLayerDirector_ScenePop(struct mmLayerDirector* p);
mm::mmLayerContext* mmLayerDirector_SceneGoto(struct mmLayerDirector* p, const char* name);
mm::mmLayerContext* mmLayerDirector_SceneGet(struct mmLayerDirector* p, const char* name);
mm::mmLayerContext* mmLayerDirector_SceneGetCurrent(struct mmLayerDirector* p);
void mmLayerDirector_SceneSetCurrent(struct mmLayerDirector* p, mm::mmLayerContext* w);
void mmLayerDirector_ScenePopUntil(struct mmLayerDirector* p, const char* name);
void mmLayerDirector_SceneClear(struct mmLayerDirector* p);

mm::mmLayerContext* mmLayerDirector_LayerAdd(struct mmLayerDirector* p, const char* type, const char* name);
mm::mmLayerContext* mmLayerDirector_LayerRmv(struct mmLayerDirector* p, const char* name);
mm::mmLayerContext* mmLayerDirector_LayerGet(struct mmLayerDirector* p, const char* name);
void mmLayerDirector_LayerClear(struct mmLayerDirector* p);

mm::mmLayerContext* mmLayerDirector_ProduceLayerContext(struct mmLayerDirector* p, const char* type, const char* name);
void mmLayerDirector_RecycleLayerContext(struct mmLayerDirector* p, mm::mmLayerContext* w);

#endif//__mmLayerDirector_h__

