/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerFactoryRegister.h"
#include "layer/director/mmLayerDirector.h"

#include "layer/widgets/mmWidgetFactoryRegister.h"
#include "layer/root/mmLayerRootFactoryRegister.h"
#include "layer/keyboard/mmLayerKeyboardFactoryRegister.h"
#include "layer/common/mmLayerCommonFactoryRegister.h"
#include "layer/emulator/mmLayerEmulatorFactoryRegister.h"
#include "layer/explorer/mmLayerExplorerFactoryRegister.h"
#include "layer/imagery/mmLayerImageryFactoryRegister.h"
#include "layer/text/mmLayerTextFactoryRegister.h"

void mmLayerFactory_Register(struct mmLayerDirector* pLayerDirector)
{
    mmWidgetFactory_Register(pLayerDirector);
    mmLayerRootFactory_Register(pLayerDirector);
    mmLayerKeyboardFactory_Register(pLayerDirector);
    mmLayerCommonFactory_Register(pLayerDirector);
    mmLayerEmulatorFactory_Register(pLayerDirector);
    mmLayerExplorerFactory_Register(pLayerDirector);
    mmLayerImageryFactory_Register(pLayerDirector);
    mmLayerTextFactory_Register(pLayerDirector);
}
void mmLayerFactory_Unregister(struct mmLayerDirector* pLayerDirector)
{
    mmWidgetFactory_Unregister(pLayerDirector);
    mmLayerRootFactory_Unregister(pLayerDirector);
    mmLayerKeyboardFactory_Unregister(pLayerDirector);
    mmLayerCommonFactory_Unregister(pLayerDirector);
    mmLayerEmulatorFactory_Unregister(pLayerDirector);
    mmLayerExplorerFactory_Unregister(pLayerDirector);
    mmLayerImageryFactory_Unregister(pLayerDirector);
    mmLayerTextFactory_Unregister(pLayerDirector);
}

