/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerDirector.h"
#include "CEGUI/WindowManager.h"

#include "nwsi/mmEventSurface.h"

static bool __static_mmLayerDirector_OnEventKeypadPressed(void* obj, const mm::mmEventArgs& args);
static bool __static_mmLayerDirector_OnEventKeypadRelease(void* obj, const mm::mmEventArgs& args);

static bool __static_mmLayerDirector_OnEventCursorBegan(void* obj, const mm::mmEventArgs& args);
static bool __static_mmLayerDirector_OnEventCursorMoved(void* obj, const mm::mmEventArgs& args);
static bool __static_mmLayerDirector_OnEventCursorEnded(void* obj, const mm::mmEventArgs& args);
static bool __static_mmLayerDirector_OnEventCursorBreak(void* obj, const mm::mmEventArgs& args);

static bool __static_mmLayerDirector_OnEventTouchsBegan(void* obj, const mm::mmEventArgs& args);
static bool __static_mmLayerDirector_OnEventTouchsMoved(void* obj, const mm::mmEventArgs& args);
static bool __static_mmLayerDirector_OnEventTouchsEnded(void* obj, const mm::mmEventArgs& args);
static bool __static_mmLayerDirector_OnEventTouchsBreak(void* obj, const mm::mmEventArgs& args);

void mmLayerDirector_Init(struct mmLayerDirector* p)
{
    mmRbtreeStringVpt_Init(&p->hExclusiveWindowPool);
    mmListVpt_Init(&p->hExclusiveWindowUsed);
    mmListVpt_Init(&p->hExclusiveWindowIdle);

    mmRbtreeStringVpt_Init(&p->hMultiplexWindowPool);
    mmListVpt_Init(&p->hMultiplexWindowUsed);
    mmListVpt_Init(&p->hMultiplexWindowIdle);

    mmRbtreeStringVpt_Init(&p->hLayerPool);

    p->pContextMaster = NULL;
    p->pSurfaceMaster = NULL;

    p->pCurrentWindow = NULL;
    
    p->pRootWindow = NULL;
    p->pRootSafety = NULL;
}
void mmLayerDirector_Destroy(struct mmLayerDirector* p)
{
    mmRbtreeStringVpt_Destroy(&p->hExclusiveWindowPool);
    mmListVpt_Destroy(&p->hExclusiveWindowUsed);
    mmListVpt_Destroy(&p->hExclusiveWindowIdle);

    mmRbtreeStringVpt_Destroy(&p->hMultiplexWindowPool);
    mmListVpt_Destroy(&p->hMultiplexWindowUsed);
    mmListVpt_Destroy(&p->hMultiplexWindowIdle);

    mmRbtreeStringVpt_Destroy(&p->hLayerPool);

    p->pContextMaster = NULL;
    p->pSurfaceMaster = NULL;

    p->pCurrentWindow = NULL;
    
    p->pRootWindow = NULL;
    p->pRootSafety = NULL;
}

void mmLayerDirector_SetContextMaster(struct mmLayerDirector* p, struct mmContextMaster* pContextMaster)
{
    p->pContextMaster = pContextMaster;
}
void mmLayerDirector_SetSurfaceMaster(struct mmLayerDirector* p, struct mmSurfaceMaster* pSurfaceMaster)
{
    p->pSurfaceMaster = pSurfaceMaster;
}

void mmLayerDirector_OnFinishLaunching(struct mmLayerDirector* p)
{
    mm::mmEventSet* pEventSet = &p->pSurfaceMaster->hEventSet;

    pEventSet->SubscribeEvent(mmSurfaceMaster_EventKeypadPressed, &__static_mmLayerDirector_OnEventKeypadPressed, p);
    pEventSet->SubscribeEvent(mmSurfaceMaster_EventKeypadRelease, &__static_mmLayerDirector_OnEventKeypadRelease, p);

    pEventSet->SubscribeEvent(mmSurfaceMaster_EventCursorBegan, &__static_mmLayerDirector_OnEventCursorBegan, p);
    pEventSet->SubscribeEvent(mmSurfaceMaster_EventCursorMoved, &__static_mmLayerDirector_OnEventCursorMoved, p);
    pEventSet->SubscribeEvent(mmSurfaceMaster_EventCursorEnded, &__static_mmLayerDirector_OnEventCursorEnded, p);
    pEventSet->SubscribeEvent(mmSurfaceMaster_EventCursorBreak, &__static_mmLayerDirector_OnEventCursorBreak, p);

    pEventSet->SubscribeEvent(mmSurfaceMaster_EventTouchsBegan, &__static_mmLayerDirector_OnEventTouchsBegan, p);
    pEventSet->SubscribeEvent(mmSurfaceMaster_EventTouchsMoved, &__static_mmLayerDirector_OnEventTouchsMoved, p);
    pEventSet->SubscribeEvent(mmSurfaceMaster_EventTouchsEnded, &__static_mmLayerDirector_OnEventTouchsEnded, p);
    pEventSet->SubscribeEvent(mmSurfaceMaster_EventTouchsBreak, &__static_mmLayerDirector_OnEventTouchsBreak, p);
}
void mmLayerDirector_OnBeforeTerminate(struct mmLayerDirector* p)
{
    mm::mmEventSet* pEventSet = &p->pSurfaceMaster->hEventSet;

    pEventSet->UnsubscribeEvent(mmSurfaceMaster_EventKeypadPressed, &__static_mmLayerDirector_OnEventKeypadPressed, p);
    pEventSet->UnsubscribeEvent(mmSurfaceMaster_EventKeypadRelease, &__static_mmLayerDirector_OnEventKeypadRelease, p);

    pEventSet->UnsubscribeEvent(mmSurfaceMaster_EventCursorBegan, &__static_mmLayerDirector_OnEventCursorBegan, p);
    pEventSet->UnsubscribeEvent(mmSurfaceMaster_EventCursorMoved, &__static_mmLayerDirector_OnEventCursorMoved, p);
    pEventSet->UnsubscribeEvent(mmSurfaceMaster_EventCursorEnded, &__static_mmLayerDirector_OnEventCursorEnded, p);
    pEventSet->UnsubscribeEvent(mmSurfaceMaster_EventCursorBreak, &__static_mmLayerDirector_OnEventCursorBreak, p);

    pEventSet->UnsubscribeEvent(mmSurfaceMaster_EventTouchsBegan, &__static_mmLayerDirector_OnEventTouchsBegan, p);
    pEventSet->UnsubscribeEvent(mmSurfaceMaster_EventTouchsMoved, &__static_mmLayerDirector_OnEventTouchsMoved, p);
    pEventSet->UnsubscribeEvent(mmSurfaceMaster_EventTouchsEnded, &__static_mmLayerDirector_OnEventTouchsEnded, p);
    pEventSet->UnsubscribeEvent(mmSurfaceMaster_EventTouchsBreak, &__static_mmLayerDirector_OnEventTouchsBreak, p);
}

void mmLayerDirector_AssignmentLayerContext(struct mmLayerDirector* p, mm::mmLayerContext* w)
{
    assert(NULL != w && "w is a invalid null pointer.");

    w->SetDirector(p);
    w->SetContextMaster(p->pContextMaster);
    w->SetSurfaceMaster(p->pSurfaceMaster);
}

void mmLayerDirector_SetRootSafety(struct mmLayerDirector* p, CEGUI::Window* pRootSafety)
{
    p->pRootSafety = pRootSafety;
    mmSurfaceMaster_SetRootSafety(p->pSurfaceMaster, p->pRootSafety);
}
void mmLayerDirector_SetRootWindowLayer(struct mmLayerDirector* p, mm::mmLayerContext* pRootWindow)
{
    CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
    
    if (NULL != p->pRootWindow)
    {
        mmSurfaceMaster_SetRootWindow(p->pSurfaceMaster, NULL);
        
        p->pRootWindow->OnBeforeTerminate();
        pWindowManager->destroyWindow(p->pRootWindow);
        p->pRootWindow = NULL;
    }
    
    p->pRootWindow = pRootWindow;
    
    if (NULL != p->pRootWindow)
    {
        mmLayerDirector_AssignmentLayerContext(p, p->pRootWindow);
        p->pRootWindow->OnFinishLaunching();
        
        mmSurfaceMaster_SetRootWindow(p->pSurfaceMaster, p->pRootWindow);
    }
}
void mmLayerDirector_SetRootWindow(struct mmLayerDirector* p, const char* type, const char* name)
{
    CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
    mm::mmLayerContext* pRootDirector = (mm::mmLayerContext*)pWindowManager->createWindow(type, name);
    mmLayerDirector_SetRootWindowLayer(p, pRootDirector);
}
void mmLayerDirector_RootWindowClear(struct mmLayerDirector* p)
{
    mmLayerDirector_LayerClear(p);
    mmLayerDirector_SceneClear(p);

    mmLayerDirector_SetRootWindowLayer(p, NULL);
}

mm::mmLayerContext* mmLayerDirector_SceneExclusiveBackground(struct mmLayerDirector* p, const char* type, const char* name)
{
    mm::mmLayerContext* current = mmLayerDirector_SceneGetCurrent(p);
    mm::mmLayerContext* w = mmLayerDirector_ScenePush(p, type, name);
    mmLayerDirector_SceneExclusiveEnterBackground(p, name);
    return w;
}
mm::mmLayerContext* mmLayerDirector_SceneExclusiveForeground(struct mmLayerDirector* p, const char* type, const char* name)
{
    return mmLayerDirector_ScenePush(p, type, name);
}

void mmLayerDirector_SceneExclusiveEnterBackground(struct mmLayerDirector* p, const char* name)
{
    struct mmString hNameWeak;

    mm::mmLayerContext* w = NULL;
    mm::mmLayerContext* window = NULL;

    struct mmListHead* prev = NULL;
    struct mmListVptIterator* it = NULL;
    struct mmRbtreeStringVptIterator* itor = NULL;

    mmString_MakeWeak(&hNameWeak, name);
    itor = mmRbtreeStringVpt_GetIterator(&p->hExclusiveWindowPool, &hNameWeak);
    if (NULL != itor)
    {
        w = (mm::mmLayerContext*)itor->v;

        mmListVpt_Remove(&p->hExclusiveWindowUsed, w);
        mmListVpt_AddTail(&p->hExclusiveWindowIdle, w);

        p->pRootSafety->removeChild(w);
        w->setVisible(false);
        w->deactivate();
        
        // reverse begin.
        prev = p->hExclusiveWindowUsed.l.prev;
        it = (struct mmListVptIterator*)mmList_Entry(prev, struct mmListVptIterator, n);
        if (prev != &p->hExclusiveWindowUsed.l)
        {
            window = (mm::mmLayerContext*)it->v;
            mmLayerDirector_SceneSetCurrent(p, window);
        }
        else
        {
            mmLayerDirector_SceneSetCurrent(p, NULL);
        }
    }
}
void mmLayerDirector_SceneExclusiveEnterForeground(struct mmLayerDirector* p, const char* name)
{
    struct mmString hNameWeak;
    mm::mmLayerContext* w = NULL;
    struct mmRbtreeStringVptIterator* itor = NULL;

    mmString_MakeWeak(&hNameWeak, name);
    itor = mmRbtreeStringVpt_GetIterator(&p->hExclusiveWindowPool, &hNameWeak);
    if (NULL != itor)
    {
        w = (mm::mmLayerContext*)itor->v;

        mmListVpt_AddTail(&p->hExclusiveWindowUsed, w);
        mmListVpt_Remove(&p->hExclusiveWindowIdle, w);

        mmLayerDirector_SceneSetCurrent(p, w);
    }
}

mm::mmLayerContext* mmLayerDirector_SceneMultiplexForeground(struct mmLayerDirector* p, const char* type, const char* name)
{
    struct mmString hNameWeak;
    mm::mmLayerContext* w = NULL;
    struct mmRbtreeStringVptIterator* itor = NULL;

    mmString_MakeWeak(&hNameWeak, name);
    itor = mmRbtreeStringVpt_GetIterator(&p->hMultiplexWindowPool, &hNameWeak);
    if (NULL == itor)
    {
        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();

        w = (mm::mmLayerContext*)pWindowManager->createWindow(type, name);
        mmLayerDirector_AssignmentLayerContext(p, w);
        w->OnFinishLaunching();

        mmRbtreeStringVpt_Set(&p->hMultiplexWindowPool, &hNameWeak, w);
        mmListVpt_AddTail(&p->hMultiplexWindowUsed, w);
    }
    else
    {
        w = (mm::mmLayerContext*)itor->v;

        mmListVpt_Remove(&p->hMultiplexWindowUsed, w);
        mmListVpt_AddTail(&p->hMultiplexWindowUsed, w);
        mmListVpt_Remove(&p->hMultiplexWindowIdle, w);
    }

    if (NULL != w)
    {
        p->pRootSafety->removeChild(w);
        p->pRootSafety->addChild(w);
        w->setVisible(true);
        w->activate();
        w->notifyScreenAreaChanged(true);
        w->invalidate(true);
    }
    return w;
}
mm::mmLayerContext* mmLayerDirector_SceneMultiplexBackground(struct mmLayerDirector* p, const char* name)
{
    struct mmString hNameWeak;
    mm::mmLayerContext* w = NULL;
    struct mmRbtreeStringVptIterator* itor = NULL;

    mmString_MakeWeak(&hNameWeak, name);
    itor = mmRbtreeStringVpt_GetIterator(&p->hMultiplexWindowPool, &hNameWeak);
    if (NULL != itor)
    {
        w = (mm::mmLayerContext*)itor->v;

        p->pRootSafety->removeChild(w);
        w->setVisible(false);
        w->deactivate();

        mmListVpt_Remove(&p->hMultiplexWindowUsed, w);
        mmListVpt_AddTail(&p->hMultiplexWindowIdle, w);
    }
    return w;
}
void mmLayerDirector_SceneMultiplexTerminate(struct mmLayerDirector* p, const char* name)
{
    struct mmString hNameWeak;
    mm::mmLayerContext* w = NULL;
    struct mmRbtreeStringVptIterator* itor = NULL;

    mmString_MakeWeak(&hNameWeak, name);
    itor = mmRbtreeStringVpt_GetIterator(&p->hMultiplexWindowPool, &hNameWeak);
    if (NULL != itor)
    {
        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();

        w = (mm::mmLayerContext*)itor->v;

        p->pRootSafety->removeChild(w);

        mmListVpt_Remove(&p->hMultiplexWindowUsed, w);
        mmListVpt_Remove(&p->hMultiplexWindowIdle, w);
        mmRbtreeStringVpt_Erase(&p->hMultiplexWindowPool, itor);

        w->OnBeforeTerminate();
        pWindowManager->destroyWindow(w);
    }
}

mm::mmLayerContext* mmLayerDirector_ScenePush(struct mmLayerDirector* p, const char* type, const char* name)
{
    struct mmString hNameWeak;
    mm::mmLayerContext* w = NULL;
    struct mmRbtreeStringVptIterator* itor = NULL;

    mmString_MakeWeak(&hNameWeak, name);
    itor = mmRbtreeStringVpt_GetIterator(&p->hExclusiveWindowPool, &hNameWeak);
    if (NULL == itor)
    {
        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        w = (mm::mmLayerContext*)pWindowManager->createWindow(type, name);
        mmLayerDirector_AssignmentLayerContext(p, w);
        w->OnFinishLaunching();

        mmRbtreeStringVpt_Set(&p->hExclusiveWindowPool, &hNameWeak, w);
        mmListVpt_AddTail(&p->hExclusiveWindowUsed, w);
    }
    else
    {
        w = (mm::mmLayerContext*)itor->v;

        mmListVpt_Remove(&p->hExclusiveWindowUsed, w);
        mmListVpt_AddTail(&p->hExclusiveWindowUsed, w);
        mmListVpt_Remove(&p->hExclusiveWindowIdle, w);
    }
    mmLayerDirector_SceneSetCurrent(p, w);
    return w;
}
mm::mmLayerContext* mmLayerDirector_ScenePop(struct mmLayerDirector* p)
{
    mm::mmLayerContext* w = NULL;
    struct mmListHead* prev = NULL;
    struct mmListVptIterator* it = NULL;

    if (NULL != p->pCurrentWindow)
    {
        struct mmString hNameWeak;

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        p->pRootSafety->removeChild(p->pCurrentWindow);

        mmString_MakeWeak(&hNameWeak, p->pCurrentWindow->getName().c_str());

        mmRbtreeStringVpt_Rmv(&p->hExclusiveWindowPool, &hNameWeak);
        mmListVpt_Remove(&p->hExclusiveWindowUsed, p->pCurrentWindow);
        mmListVpt_Remove(&p->hExclusiveWindowIdle, p->pCurrentWindow);

        p->pCurrentWindow->OnBeforeTerminate();
        pWindowManager->destroyWindow(p->pCurrentWindow);
        p->pCurrentWindow = NULL;
    }

    // reverse begin.
    prev = p->hExclusiveWindowUsed.l.prev;
    it = (struct mmListVptIterator*)mmList_Entry(prev, struct mmListVptIterator, n);
    if (prev != &p->hExclusiveWindowUsed.l)
    {
        w = (mm::mmLayerContext*)it->v;
    }

    mmLayerDirector_SceneSetCurrent(p, w);
    return w;
}
mm::mmLayerContext* mmLayerDirector_SceneGoto(struct mmLayerDirector* p, const char* name)
{
    mm::mmLayerContext* w = mmLayerDirector_SceneGet(p, name);
    mmLayerDirector_SceneSetCurrent(p, w);
    return w;
}
mm::mmLayerContext* mmLayerDirector_SceneGet(struct mmLayerDirector* p, const char* name)
{
    struct mmString hNameWeak;
    mm::mmLayerContext* w = NULL;
    struct mmRbtreeStringVptIterator* itor = NULL;

    mmString_MakeWeak(&hNameWeak, name);
    itor = mmRbtreeStringVpt_GetIterator(&p->hExclusiveWindowPool, &hNameWeak);
    if (NULL != itor)
    {
        w = (mm::mmLayerContext*)itor->v;
    }
    return w;
}
mm::mmLayerContext* mmLayerDirector_SceneGetCurrent(struct mmLayerDirector* p)
{
    return p->pCurrentWindow;
}
void mmLayerDirector_SceneSetCurrent(struct mmLayerDirector* p, mm::mmLayerContext* w)
{
    if (NULL != p->pCurrentWindow)
    {
        p->pRootSafety->removeChild(p->pCurrentWindow);
        p->pCurrentWindow->setVisible(false);
        p->pCurrentWindow->deactivate();
    }
    p->pCurrentWindow = w;
    if (NULL != p->pCurrentWindow)
    {
        assert(p->pRootSafety && "p->pRootSafety is a null.");
        p->pRootSafety->addChild(p->pCurrentWindow);
        p->pCurrentWindow->setVisible(true);
        p->pCurrentWindow->activate();
        p->pCurrentWindow->notifyScreenAreaChanged(true);
        p->pCurrentWindow->invalidate(true);
    }
}
void mmLayerDirector_ScenePopUntil(struct mmLayerDirector* p, const char* name)
{
    while (p->pCurrentWindow && name != p->pCurrentWindow->getName())
    {
        mmLayerDirector_ScenePop(p);
    }
}
void mmLayerDirector_SceneClear(struct mmLayerDirector* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    mm::mmLayerContext* w = NULL;

    CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();

    n = mmRb_First(&p->hExclusiveWindowPool.rbt);
    while (NULL != n)
    {
        it = (struct mmRbtreeStringVptIterator*)mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
        n = mmRb_Next(n);
        w = (mm::mmLayerContext*)(it->v);
        mmRbtreeStringVpt_Erase(&p->hExclusiveWindowPool, it);

        mmListVpt_Remove(&p->hExclusiveWindowUsed, w);
        mmListVpt_Remove(&p->hExclusiveWindowIdle, w);

        w->OnBeforeTerminate();
        pWindowManager->destroyWindow(w);
    }

    n = mmRb_First(&p->hMultiplexWindowPool.rbt);
    while (NULL != n)
    {
        it = (struct mmRbtreeStringVptIterator*)mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
        n = mmRb_Next(n);
        w = (mm::mmLayerContext*)(it->v);
        mmRbtreeStringVpt_Erase(&p->hMultiplexWindowPool, it);

        mmListVpt_Remove(&p->hMultiplexWindowUsed, w);
        mmListVpt_Remove(&p->hMultiplexWindowIdle, w);

        w->OnBeforeTerminate();
        pWindowManager->destroyWindow(w);
    }
}

mm::mmLayerContext* mmLayerDirector_LayerAdd(struct mmLayerDirector* p, const char* type, const char* name)
{
    struct mmString hNameWeak;
    mm::mmLayerContext* w = NULL;
    struct mmRbtreeStringVptIterator* itor = NULL;

    mmString_MakeWeak(&hNameWeak, name);
    itor = mmRbtreeStringVpt_GetIterator(&p->hLayerPool, &hNameWeak);
    if (NULL == itor)
    {
        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        w = (mm::mmLayerContext*)pWindowManager->createWindow(type, name);
        mmLayerDirector_AssignmentLayerContext(p, w);
        w->OnFinishLaunching();

        mmRbtreeStringVpt_Set(&p->hLayerPool, &hNameWeak, w);

        assert(p->pCurrentWindow && "this->pCurrentWindow is a null.");
        p->pCurrentWindow->addChild(w);
    }
    else
    {
        w = (mm::mmLayerContext*)itor->v;
        p->pCurrentWindow->removeChild(w);
        p->pCurrentWindow->addChild(w);
        w->setVisible(true);
        w->activate();
        w->notifyScreenAreaChanged(true);
        w->invalidate(true);
    }
    return w;
}
mm::mmLayerContext* mmLayerDirector_LayerRmv(struct mmLayerDirector* p, const char* name)
{
    struct mmString hNameWeak;
    mm::mmLayerContext* w = NULL;
    struct mmRbtreeStringVptIterator* itor = NULL;

    mmString_MakeWeak(&hNameWeak, name);
    itor = mmRbtreeStringVpt_GetIterator(&p->hLayerPool, &hNameWeak);
    if (NULL != itor)
    {
        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        w = (mm::mmLayerContext*)itor->v;
        p->pCurrentWindow->removeChild(w);
        w->OnBeforeTerminate();
        pWindowManager->destroyWindow(w);

        mmRbtreeStringVpt_Erase(&p->hLayerPool, itor);
    }
    return w;
}
mm::mmLayerContext* mmLayerDirector_LayerGet(struct mmLayerDirector* p, const char* name)
{
    struct mmString hNameWeak;
    mm::mmLayerContext* w = NULL;
    struct mmRbtreeStringVptIterator* itor = NULL;

    mmString_MakeWeak(&hNameWeak, name);
    itor = mmRbtreeStringVpt_GetIterator(&p->hLayerPool, &hNameWeak);
    if (NULL != itor)
    {
        w = (mm::mmLayerContext*)itor->v;
    }
    return w;
}
void mmLayerDirector_LayerClear(struct mmLayerDirector* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    mm::mmLayerContext* w = NULL;

    CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();

    n = mmRb_First(&p->hLayerPool.rbt);
    while (NULL != n)
    {
        it = (struct mmRbtreeStringVptIterator*)mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
        n = mmRb_Next(n);
        w = (mm::mmLayerContext*)(it->v);
        mmRbtreeStringVpt_Erase(&p->hLayerPool, it);

        p->pCurrentWindow->removeChild(w);
        w->OnBeforeTerminate();
        pWindowManager->destroyWindow(w);
    }
}

mm::mmLayerContext* mmLayerDirector_ProduceLayerContext(struct mmLayerDirector* p, const char* type, const char* name)
{
    mm::mmLayerContext* w = NULL;
    CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
    w = (mm::mmLayerContext*)pWindowManager->createWindow(type, name);
    mmLayerDirector_AssignmentLayerContext(p, w);
    return w;
}
void mmLayerDirector_RecycleLayerContext(struct mmLayerDirector* p, mm::mmLayerContext* w)
{
    CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
    pWindowManager->destroyWindow(w);
}

static bool __static_mmLayerDirector_OnEventKeypadPressed(void* obj, const mm::mmEventArgs& args)
{
    struct mmLayerDirector* p = (struct mmLayerDirector*)(obj);

    const mm::mmEventKeypad& evt = (const mm::mmEventKeypad&)(args);

    if (NULL != p->pRootWindow)
    {
        mm::mmKeypadEventArgs hEventArgs(p->pRootWindow, evt.content);

        p->pRootWindow->OnKeypadPressed(hEventArgs);
    }

    return true;
}
static bool __static_mmLayerDirector_OnEventKeypadRelease(void* obj, const mm::mmEventArgs& args)
{
    struct mmLayerDirector* p = (struct mmLayerDirector*)(obj);

    const mm::mmEventKeypad& evt = (const mm::mmEventKeypad&)(args);

    if (NULL != p->pRootWindow)
    {
        mm::mmKeypadEventArgs hEventArgs(p->pRootWindow, evt.content);

        p->pRootWindow->OnKeypadRelease(hEventArgs);
    }

    return true;
}

static bool __static_mmLayerDirector_OnEventCursorBegan(void* obj, const mm::mmEventArgs& args)
{
    struct mmLayerDirector* p = (struct mmLayerDirector*)(obj);

    const mm::mmEventCursor& evt = (const mm::mmEventCursor&)(args);

    if (NULL != p->pRootWindow)
    {
        mm::mmCursorEventArgs hEventArgs(p->pRootWindow, evt.content);

        p->pRootWindow->OnCursorBegan(hEventArgs);
    }

    return true;
}
static bool __static_mmLayerDirector_OnEventCursorMoved(void* obj, const mm::mmEventArgs& args)
{
    struct mmLayerDirector* p = (struct mmLayerDirector*)(obj);

    const mm::mmEventCursor& evt = (const mm::mmEventCursor&)(args);

    if (NULL != p->pRootWindow)
    {
        mm::mmCursorEventArgs hEventArgs(p->pRootWindow, evt.content);

        p->pRootWindow->OnCursorMoved(hEventArgs);
    }

    return true;
}
static bool __static_mmLayerDirector_OnEventCursorEnded(void* obj, const mm::mmEventArgs& args)
{
    struct mmLayerDirector* p = (struct mmLayerDirector*)(obj);

    const mm::mmEventCursor& evt = (const mm::mmEventCursor&)(args);

    if (NULL != p->pRootWindow)
    {
        mm::mmCursorEventArgs hEventArgs(p->pRootWindow, evt.content);

        p->pRootWindow->OnCursorEnded(hEventArgs);
    }

    return true;
}
static bool __static_mmLayerDirector_OnEventCursorBreak(void* obj, const mm::mmEventArgs& args)
{
    struct mmLayerDirector* p = (struct mmLayerDirector*)(obj);

    const mm::mmEventCursor& evt = (const mm::mmEventCursor&)(args);

    if (NULL != p->pRootWindow)
    {
        mm::mmCursorEventArgs hEventArgs(p->pRootWindow, evt.content);

        p->pRootWindow->OnCursorBreak(hEventArgs);
    }

    return true;
}

static bool __static_mmLayerDirector_OnEventTouchsBegan(void* obj, const mm::mmEventArgs& args)
{
    struct mmLayerDirector* p = (struct mmLayerDirector*)(obj);

    const mm::mmEventTouchs& evt = (const mm::mmEventTouchs&)(args);

    if (NULL != p->pRootWindow)
    {
        mm::mmTouchsEventArgs hEventArgs(p->pRootWindow, evt.content);

        p->pRootWindow->OnTouchsBegan(hEventArgs);
    }

    return true;
}
static bool __static_mmLayerDirector_OnEventTouchsMoved(void* obj, const mm::mmEventArgs& args)
{
    struct mmLayerDirector* p = (struct mmLayerDirector*)(obj);

    const mm::mmEventTouchs& evt = (const mm::mmEventTouchs&)(args);

    if (NULL != p->pRootWindow)
    {
        mm::mmTouchsEventArgs hEventArgs(p->pRootWindow, evt.content);

        p->pRootWindow->OnTouchsMoved(hEventArgs);
    }

    return true;
}
static bool __static_mmLayerDirector_OnEventTouchsEnded(void* obj, const mm::mmEventArgs& args)
{
    struct mmLayerDirector* p = (struct mmLayerDirector*)(obj);

    const mm::mmEventTouchs& evt = (const mm::mmEventTouchs&)(args);

    if (NULL != p->pRootWindow)
    {
        mm::mmTouchsEventArgs hEventArgs(p->pRootWindow, evt.content);

        p->pRootWindow->OnTouchsEnded(hEventArgs);
    }

    return true;
}
static bool __static_mmLayerDirector_OnEventTouchsBreak(void* obj, const mm::mmEventArgs& args)
{
    struct mmLayerDirector* p = (struct mmLayerDirector*)(obj);

    const mm::mmEventTouchs& evt = (const mm::mmEventTouchs&)(args);

    if (NULL != p->pRootWindow)
    {
        mm::mmTouchsEventArgs hEventArgs(p->pRootWindow, evt.content);

        p->pRootWindow->OnTouchsBreak(hEventArgs);
    }

    return true;
}
