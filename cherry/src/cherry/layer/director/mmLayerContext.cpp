/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerContext.h"
#include <algorithm>

namespace mm
{

    const CEGUI::String mmLayerContext::EventNamespace = "mm";
    const CEGUI::String mmLayerContext::WidgetTypeName = "mm/";

    const CEGUI::String mmLayerContext::EventTouchsMoved = "EventTouchsMoved";
    const CEGUI::String mmLayerContext::EventTouchsBegan = "EventTouchsBegan";
    const CEGUI::String mmLayerContext::EventTouchsEnded = "EventTouchsEnded";
    const CEGUI::String mmLayerContext::EventTouchsBreak = "EventTouchsBreak";

    const CEGUI::String mmLayerContext::EventCursorMoved = "EventCursorMoved";
    const CEGUI::String mmLayerContext::EventCursorBegan = "EventCursorBegan";
    const CEGUI::String mmLayerContext::EventCursorEnded = "EventCursorEnded";
    const CEGUI::String mmLayerContext::EventCursorBreak = "EventCursorBreak";

    const CEGUI::String mmLayerContext::EventKeypadPressed = "EventKeypadPressed";
    const CEGUI::String mmLayerContext::EventKeypadRelease = "EventKeypadRelease";

    const CEGUI::String mmLayerContext::TouchsPassThroughEnabledPropertyName = "TouchsPassThroughEnabled";
    const CEGUI::String mmLayerContext::CursorPassThroughEnabledPropertyName = "CursorPassThroughEnabled";
    const CEGUI::String mmLayerContext::KeypadPassThroughEnabledPropertyName = "KeypadPassThroughEnabled";

    mmLayerContext::mmLayerContext(const CEGUI::String& type, const CEGUI::String& name)
        : CEGUI::DefaultWindow(type, name)
        , pDirector(NULL)
        , pContextMaster(NULL)
        , pSurfaceMaster(NULL)

        , hChildContextVector()

        , hTouchsPassThroughEnabled(false)
        , hCursorPassThroughEnabled(false)
        , hKeypadPassThroughEnabled(false)
    {
        const CEGUI::String propertyOrigin("mmLayerContext");

        if (type.substr(0, mmLayerContext::WidgetTypeName.size()) != mmLayerContext::WidgetTypeName)
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            // will not trigger mmLayerContext special event.
            mmLogger_LogI(gLogger, "mmLayerContext type: %s not base on base on mmLayerContext::WidgetTypeName.", type.c_str());
        }

        CEGUI_DEFINE_PROPERTY
        (
            mmLayerContext, bool,
            TouchsPassThroughEnabledPropertyName, "Property to get/set whether the window ignores touchs events and pass them through to any windows behind it. Value is either \"true\" or \"false\".",
            &mmLayerContext::SetTouchsPassThroughEnabled, &mmLayerContext::GetTouchsPassThroughEnabled, false
        );

        CEGUI_DEFINE_PROPERTY
        (
            mmLayerContext, bool,
            CursorPassThroughEnabledPropertyName, "Property to get/set whether the window ignores cursor events and pass them through to any windows behind it. Value is either \"true\" or \"false\".",
            &mmLayerContext::SetCursorPassThroughEnabled, &mmLayerContext::GetCursorPassThroughEnabled, false
        );

        CEGUI_DEFINE_PROPERTY
        (
            mmLayerContext, bool,
            KeypadPassThroughEnabledPropertyName, "Property to get/set whether the window ignores keypad events and pass them through to any windows behind it. Value is either \"true\" or \"false\".",
            &mmLayerContext::SetKeypadPassThroughEnabled, &mmLayerContext::GetKeypadPassThroughEnabled, false
        );

        this->subscribeEvent(CEGUI::Element::EventChildAdded, CEGUI::Event::Subscriber(&mmLayerContext::OnElementEventChildAdded, this));
        this->subscribeEvent(CEGUI::Element::EventChildRemoved, CEGUI::Event::Subscriber(&mmLayerContext::OnElementEventChildRemoved, this));
    }
    mmLayerContext::~mmLayerContext()
    {

    }
    void mmLayerContext::SetDirector(struct mmLayerDirector* pDirector)
    {
        this->pDirector = pDirector;
    }
    void mmLayerContext::SetContextMaster(struct mmContextMaster* pContextMaster)
    {
        this->pContextMaster = pContextMaster;
    }
    void mmLayerContext::SetSurfaceMaster(struct mmSurfaceMaster* pSurfaceMaster)
    {
        this->pSurfaceMaster = pSurfaceMaster;
    }
    void mmLayerContext::SetTouchsPassThroughEnabled(bool _enable)
    {
        this->hTouchsPassThroughEnabled = _enable;
    }
    bool mmLayerContext::GetTouchsPassThroughEnabled() const
    {
        return this->hTouchsPassThroughEnabled;
    }

    void mmLayerContext::SetCursorPassThroughEnabled(bool _enable)
    {
        this->hCursorPassThroughEnabled = _enable;
    }
    bool mmLayerContext::GetCursorPassThroughEnabled() const
    {
        return this->hCursorPassThroughEnabled;
    }

    void mmLayerContext::SetKeypadPassThroughEnabled(bool _enable)
    {
        this->hKeypadPassThroughEnabled = _enable;
    }
    bool mmLayerContext::GetKeypadPassThroughEnabled() const
    {
        return this->hKeypadPassThroughEnabled;
    }
    void mmLayerContext::AttachContextEvent(mmLayerContext* _layer_context)
    {
        this->hChildContextVector.push_back(_layer_context);
    }
    void mmLayerContext::DetachContextEvent(mmLayerContext* _layer_context)
    {
        ChildContextVectorType::iterator it;

        // find this element in the child list
        it = std::find(this->hChildContextVector.begin(), this->hChildContextVector.end(), _layer_context);

        // if the element was found in the child list
        if (it != this->hChildContextVector.end())
        {
            // remove element from child list
            this->hChildContextVector.erase(it);
        }
    }
    void mmLayerContext::OnFinishLaunching()
    {
        // need implement.
    }
    void mmLayerContext::OnBeforeTerminate()
    {
        // need implement.
    }
    void mmLayerContext::OnTouchsMoved(mmTouchsEventArgs& args)
    {
        // parent -> child event.
        if (!args.handled && this->isVisible() && !this->isDisabled())
        {
            args.window = this;

            this->fireEvent(EventTouchsMoved, args, EventNamespace);
        }

        // child event.
        mmLayerContext* child = NULL;
        ChildContextVectorType::iterator it = this->hChildContextVector.begin();
        while (it != this->hChildContextVector.end())
        {
            child = *it;

            it++;

            child->OnTouchsMoved(args);
        }

        // child -> parent event.
        if (!args.handled && this->isVisible() && !this->isDisabled())
        {
            args.window = this;

            this->fireEvent(EventTouchsMoved, args, EventNamespace);
        }

        this->UpdateTouchsEventHandled(args);
    }
    void mmLayerContext::OnTouchsBegan(mmTouchsEventArgs& args)
    {
        // parent -> child event.
        if (!args.handled && this->isVisible() && !this->isDisabled())
        {
            args.window = this;

            this->fireEvent(EventTouchsBegan, args, EventNamespace);
        }

        // child event.
        mmLayerContext* child = NULL;
        ChildContextVectorType::iterator it = this->hChildContextVector.begin();
        while (it != this->hChildContextVector.end())
        {
            child = *it;

            it++;

            child->OnTouchsBegan(args);
        }

        // child -> parent event.
        if (!args.handled && this->isVisible() && !this->isDisabled())
        {
            args.window = this;

            this->fireEvent(EventTouchsBegan, args, EventNamespace);
        }

        this->UpdateTouchsEventHandled(args);
    }
    void mmLayerContext::OnTouchsEnded(mmTouchsEventArgs& args)
    {
        // parent -> child event.
        if (!args.handled && this->isVisible() && !this->isDisabled())
        {
            args.window = this;

            this->fireEvent(EventTouchsEnded, args, EventNamespace);
        }

        // child event.
        mmLayerContext* child = NULL;
        ChildContextVectorType::iterator it = this->hChildContextVector.begin();
        while (it != this->hChildContextVector.end())
        {
            child = *it;

            it++;

            child->OnTouchsEnded(args);
        }

        // child -> parent event.
        if (!args.handled && this->isVisible() && !this->isDisabled())
        {
            args.window = this;

            this->fireEvent(EventTouchsEnded, args, EventNamespace);
        }

        this->UpdateTouchsEventHandled(args);
    }
    void mmLayerContext::OnTouchsBreak(mmTouchsEventArgs& args)
    {
        // parent -> child event.
        if (!args.handled && this->isVisible() && !this->isDisabled())
        {
            args.window = this;

            this->fireEvent(EventTouchsBreak, args, EventNamespace);
        }

        // child event.
        mmLayerContext* child = NULL;
        ChildContextVectorType::iterator it = this->hChildContextVector.begin();
        while (it != this->hChildContextVector.end())
        {
            child = *it;

            it++;

            child->OnTouchsBreak(args);
        }

        // child -> parent event.
        if (!args.handled && this->isVisible() && !this->isDisabled())
        {
            args.window = this;

            this->fireEvent(EventTouchsBreak, args, EventNamespace);
        }

        this->UpdateTouchsEventHandled(args);
    }
    void mmLayerContext::OnCursorMoved(mmCursorEventArgs& args)
    {
        // parent -> child event.
        if (!args.handled && this->isVisible() && !this->isDisabled())
        {
            args.window = this;

            this->fireEvent(EventCursorMoved, args, EventNamespace);
        }

        // child event.
        mmLayerContext* child = NULL;
        ChildContextVectorType::iterator it = this->hChildContextVector.begin();
        while (it != this->hChildContextVector.end())
        {
            child = *it;

            it++;

            child->OnCursorMoved(args);
        }

        // child -> parent event.
        if (!args.handled && this->isVisible() && !this->isDisabled())
        {
            args.window = this;

            this->fireEvent(EventCursorMoved, args, EventNamespace);
        }

        this->UpdateCursorEventHandled(args);
    }
    void mmLayerContext::OnCursorBegan(mmCursorEventArgs& args)
    {
        // parent -> child event.
        if (!args.handled && this->isVisible() && !this->isDisabled())
        {
            args.window = this;

            this->fireEvent(EventCursorBegan, args, EventNamespace);
        }

        // child event.
        mmLayerContext* child = NULL;
        ChildContextVectorType::iterator it = this->hChildContextVector.begin();
        while (it != this->hChildContextVector.end())
        {
            child = *it;

            it++;

            child->OnCursorBegan(args);
        }

        // child -> parent event.
        if (!args.handled && this->isVisible() && !this->isDisabled())
        {
            args.window = this;

            this->fireEvent(EventCursorBegan, args, EventNamespace);
        }

        this->UpdateCursorEventHandled(args);
    }
    void mmLayerContext::OnCursorEnded(mmCursorEventArgs& args)
    {
        // parent -> child event.
        if (!args.handled && this->isVisible() && !this->isDisabled())
        {
            args.window = this;

            this->fireEvent(EventCursorEnded, args, EventNamespace);
        }

        // child event.
        mmLayerContext* child = NULL;
        ChildContextVectorType::iterator it = this->hChildContextVector.begin();
        while (it != this->hChildContextVector.end())
        {
            child = *it;

            it++;

            child->OnCursorEnded(args);
        }

        // child -> parent event.
        if (!args.handled && this->isVisible() && !this->isDisabled())
        {
            args.window = this;

            this->fireEvent(EventCursorEnded, args, EventNamespace);
        }

        this->UpdateCursorEventHandled(args);
    }
    void mmLayerContext::OnCursorBreak(mmCursorEventArgs& args)
    {
        // parent -> child event.
        if (!args.handled && this->isVisible() && !this->isDisabled())
        {
            args.window = this;

            this->fireEvent(EventCursorBreak, args, EventNamespace);
        }

        // child event.
        mmLayerContext* child = NULL;
        ChildContextVectorType::iterator it = this->hChildContextVector.begin();
        while (it != this->hChildContextVector.end())
        {
            child = *it;

            it++;

            child->OnCursorBreak(args);
        }

        // child -> parent event.
        if (!args.handled && this->isVisible() && !this->isDisabled())
        {
            args.window = this;

            this->fireEvent(EventCursorBreak, args, EventNamespace);
        }

        this->UpdateCursorEventHandled(args);
    }

    void mmLayerContext::OnKeypadPressed(mmKeypadEventArgs& args)
    {
        // parent -> child event.
        if (!args.handled && this->isVisible() && !this->isDisabled())
        {
            args.window = this;

            this->fireEvent(EventKeypadPressed, args, EventNamespace);
        }

        // child event.
        mmLayerContext* child = NULL;
        ChildContextVectorType::iterator it = this->hChildContextVector.begin();
        while (it != this->hChildContextVector.end())
        {
            child = *it;

            it++;

            child->OnKeypadPressed(args);
        }

        // child -> parent event.
        if (!args.handled && this->isVisible() && !this->isDisabled())
        {
            args.window = this;

            this->fireEvent(EventKeypadPressed, args, EventNamespace);
        }

        this->UpdateKeypadEventHandled(args);
    }
    void mmLayerContext::OnKeypadRelease(mmKeypadEventArgs& args)
    {
        // parent -> child event.
        if (!args.handled && this->isVisible() && !this->isDisabled())
        {
            args.window = this;

            this->fireEvent(EventKeypadRelease, args, EventNamespace);
        }

        // child event.
        mmLayerContext* child = NULL;
        ChildContextVectorType::iterator it = this->hChildContextVector.begin();
        while (it != this->hChildContextVector.end())
        {
            child = *it;

            it++;

            child->OnKeypadRelease(args);
        }

        // child -> parent event.
        if (!args.handled && this->isVisible() && !this->isDisabled())
        {
            args.window = this;

            this->fireEvent(EventKeypadRelease, args, EventNamespace);
        }

        this->UpdateKeypadEventHandled(args);
    }

    void mmLayerContext::UpdateTouchsEventHandled(CEGUI::EventArgs& args) const
    {
        // by default, if we are a root window (no parent) with pass-though enabled
        // we do /not/ mark touchs events as handled.
        if (!this->d_parent && args.handled && this->hTouchsPassThroughEnabled)
        {
            --args.handled;
        }
    }
    void mmLayerContext::UpdateCursorEventHandled(CEGUI::EventArgs& args) const
    {
        // by default, if we are a root window (no parent) with pass-though enabled
        // we do /not/ mark mouse events as handled.
        if (!this->d_parent && args.handled && this->hCursorPassThroughEnabled)
        {
            --args.handled;
        }
    }
    void mmLayerContext::UpdateKeypadEventHandled(CEGUI::EventArgs& args) const
    {
        // by default, if we are a root window (no parent) with pass-though enabled
        // we do /not/ mark key events as handled.
        if (!this->d_parent && args.handled && this->hKeypadPassThroughEnabled)
        {
            --args.handled;
        }
    }
    bool mmLayerContext::OnElementEventChildAdded(const CEGUI::EventArgs& args)
    {
        const CEGUI::ElementEventArgs& evt = (const CEGUI::ElementEventArgs&)(args);

        // evt.element must be a CEGUI::Window.
        CEGUI::Window* window = (CEGUI::Window*)(evt.element);

        const CEGUI::String& type = window->getType();

        if (type.substr(0, mmLayerContext::WidgetTypeName.size()) == mmLayerContext::WidgetTypeName)
        {
            mmLayerContext* child = (mmLayerContext*)(evt.element);

            this->AttachContextEvent(child);
        }
        return true;
    }
    bool mmLayerContext::OnElementEventChildRemoved(const CEGUI::EventArgs& args)
    {
        const CEGUI::ElementEventArgs& evt = (const CEGUI::ElementEventArgs&)(args);

        // evt.element must be a CEGUI::Window.
        CEGUI::Window* window = (CEGUI::Window*)(evt.element);

        const CEGUI::String& type = window->getType();

        if (type.substr(0, mmLayerContext::WidgetTypeName.size()) == mmLayerContext::WidgetTypeName)
        {
            mmLayerContext* child = (mmLayerContext*)(evt.element);

            this->DetachContextEvent(child);
        }
        return true;
    }
}


void mmCEGUIWindow_OnFireEvent(
    CEGUI::Window* pWindow,
    const CEGUI::String& hEventName,
    CEGUI::WindowEventArgs& hArgs,
    const CEGUI::String& hEventNamespace,
    const CEGUI::String& hWidgetTypeName)
{
    do
    {
        if (0 < hArgs.handled)
        {
            break;
        }

        if (NULL == pWindow)
        {
            break;
        }

        CEGUI::GUIContext& hGUIContext = pWindow->getGUIContext();
        CEGUI::Window* pModalWindow = hGUIContext.getModalWindow();

        if (pModalWindow == pWindow)
        {
            break;
        }

        const CEGUI::String& hType = pWindow->getType();
        if (CEGUI::String::npos == hType.find(hWidgetTypeName))
        {
            break;
        }

        if (!pWindow->isVisible())
        {
            break;
        }

        if (pWindow->isDisabled())
        {
            break;
        }

        hArgs.window = pWindow;
        pWindow->fireEvent(hEventName, hArgs, hEventNamespace);
    } while (0);
}

void mmCEGUIWindow_OnHandleEvent(
    CEGUI::Window* pWindow,
    const CEGUI::String& hEventName,
    CEGUI::WindowEventArgs& hArgs,
    const CEGUI::String& hEventNamespace,
    const CEGUI::String& hWidgetTypeName)
{
    CEGUI::Window* pChildWindow = NULL;
    size_t i = 0;

    // parent -> child event.
    mmCEGUIWindow_OnFireEvent(pWindow, hEventName, hArgs, hEventNamespace, hWidgetTypeName);

    // child event.
    while (i < pWindow->getChildCount() && 0 >= hArgs.handled)
    {
        pChildWindow = pWindow->getChildAtIdx(i);
        mmCEGUIWindow_OnHandleEvent(pChildWindow, hEventName, hArgs, hEventNamespace, hWidgetTypeName);
        i++;
    }

    // child -> parent event.
    mmCEGUIWindow_OnFireEvent(pWindow, hEventName, hArgs, hEventNamespace, hWidgetTypeName);
}
