/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerTextItem.h"

#include "CEGUI/WindowManager.h"

#include "layer/utility/mmLayerUtilityIconv.h"

namespace mm
{
    const CEGUI::String mmLayerTextItem::EventNamespace = "mm";
    const CEGUI::String mmLayerTextItem::WidgetTypeName = "mm/mmLayerTextItem";

    mmLayerTextItem::mmLayerTextItem(const CEGUI::String& type, const CEGUI::String& name)
        : mmLayerContext(type, name)

        , hIndex(0)

        , LayerTextItem(NULL)

        , LabelText(NULL)

        , pText(NULL)

        , pIconvContext(NULL)
    {

    }
    mmLayerTextItem::~mmLayerTextItem(void)
    {

    }
    void mmLayerTextItem::OnFinishLaunching(void)
    {
        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        this->LayerTextItem = pWindowManager->loadLayoutFromFile("text/LayerTextItem.layout");
        this->addChild(this->LayerTextItem);

        // Manual set whole area.
        CEGUI::URect hWholeArea(CEGUI::UDim(0, 0), CEGUI::UDim(0, 0), CEGUI::UDim(1, 0), CEGUI::UDim(1, 0));
        this->LayerTextItem->setArea(hWholeArea);

        this->LabelText = this->LayerTextItem->getChild("LabelText");
    }
    void mmLayerTextItem::OnBeforeTerminate(void)
    {
        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        pWindowManager->destroyWindow(this->LayerTextItem);
        this->LayerTextItem = NULL;
    }
    void mmLayerTextItem::SetIndex(size_t hIndex)
    {
        this->hIndex = hIndex;
    }
    void mmLayerTextItem::SetItem(CEGUI::String* pText)
    {
        this->pText = pText;
    }
    void mmLayerTextItem::SetIconvContext(struct mmIconvContext* pIconvContext)
    {
        this->pIconvContext = pIconvContext;
    }
    void mmLayerTextItem::OnFinishAttach(void)
    {

    }
    void mmLayerTextItem::OnBeforeDetach(void)
    {

    }
    void mmLayerTextItem::UpdateLayerValue(void)
    {
        this->UpdateUtf8View();
    }
    void mmLayerTextItem::UpdateUtf8View(void)
    {
        this->LabelText->setText(*this->pText);
    }
}
