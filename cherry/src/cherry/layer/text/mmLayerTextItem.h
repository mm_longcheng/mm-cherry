/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmLayerTextItem_h__
#define __mmLayerTextItem_h__

#include "CEGUI/Window.h"

#include "layer/director/mmLayerContext.h"

struct mmIconvContext;

namespace mm
{
    class mmLayerTextItem : public mmLayerContext
    {
    public:
        static const CEGUI::String EventNamespace;          //!< Namespace for global events
        static const CEGUI::String WidgetTypeName;          //!< Window factory name
    public:
        size_t hIndex;
    public:
        CEGUI::Window* LayerTextItem;

        CEGUI::Window* LabelText;
    public:
        CEGUI::String* pText;
    public:
        // weak ref.
        struct mmIconvContext* pIconvContext;
    public:
        mmLayerTextItem(const CEGUI::String& type, const CEGUI::String& name);
        virtual ~mmLayerTextItem(void);
    public:
        // mmLayerContext
        virtual void OnFinishLaunching(void);
        virtual void OnBeforeTerminate(void);
    public:
        void SetIndex(size_t hIndex);
        void SetItem(CEGUI::String* pText);
        void SetIconvContext(struct mmIconvContext* pIconvContext);
    public:
        void OnFinishAttach(void);
        void OnBeforeDetach(void);
    public:
        void UpdateLayerValue(void);
        void UpdateUtf8View(void);
    };
}

#endif//__mmLayerTextItem_h__
