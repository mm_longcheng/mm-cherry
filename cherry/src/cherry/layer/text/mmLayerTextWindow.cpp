/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerTextWindow.h"
#include "mmLayerTextItem.h"

#include "core/mmLogger.h"
#include "core/mmByte.h"

#include "CEGUI/WindowManager.h"
#include "CEGUI/Font.h"

#include "CEGUI/falagard/WidgetLookFeel.h"

#include "CEGUI/widgets/Scrollbar.h"
#include "CEGUI/widgets/Thumb.h"

#include "explorer/mmExplorerItem.h"

#include "layer/utility/mmLayerUtilityTextItem.h"

#include "layer/widgets/mmWidgetScrollable.h"

#include "toolkit/mmIconv.h"


namespace mm
{
    static CEGUI::Window* __static_ChildrenCreatorTextItem_Produce(mmWidgetScrollable* obj, size_t index)
    {
        const mmWidgetScrollableChildrenCreator* pChildrenCreator = obj->GetChildrenCreator();
        mmLayerTextWindow* pLayerTextView = (mmLayerTextWindow*)(pChildrenCreator->obj);
        struct mmLayerUtilityItemPool* pItemPool = &pLayerTextView->hItemPool;;
        mmLayerTextItem* pLayerItem = (mmLayerTextItem*)mmLayerUtilityItemPool_Produce(pItemPool);
        pLayerItem->SetIndex(index);
        pLayerTextView->CreateItemEvent(pLayerItem);
        return pLayerItem;
    }
    static void __static_ChildrenCreatorTextItem_Recycle(mmWidgetScrollable* obj, CEGUI::Window* w)
    {
        const mmWidgetScrollableChildrenCreator* pChildrenCreator = obj->GetChildrenCreator();
        mmLayerTextWindow* pLayerTextView = (mmLayerTextWindow*)(pChildrenCreator->obj);
        struct mmLayerUtilityItemPool* pItemPool = &pLayerTextView->hItemPool;;
        mmLayerTextItem* pLayerItem = (mmLayerTextItem*)(w);
        pLayerTextView->DeleteItemEvent(pLayerItem);
        mmLayerUtilityItemPool_Recycle(pItemPool, pLayerItem);
    }

    static void __static_ChildrenCreatorTextItem_FinishAttach(mmWidgetScrollable* obj, CEGUI::Window* w)
    {
        mmLayerTextItem* pLayerItem = (mmLayerTextItem*)(w);
        pLayerItem->OnFinishAttach();
    }

    static void __static_ChildrenCreatorTextItem_BeforeDetach(mmWidgetScrollable* obj, CEGUI::Window* w)
    {
        mmLayerTextItem* pLayerItem = (mmLayerTextItem*)(w);
        pLayerItem->OnBeforeDetach();
    }

    const CEGUI::String mmLayerTextWindow::EventNamespace = "mm";
    const CEGUI::String mmLayerTextWindow::WidgetTypeName = "mm/mmLayerTextWindow";

    const CEGUI::String mmLayerTextWindow::EventUpdateAnalyse = "EventUpdateAnalyse";
    const CEGUI::String mmLayerTextWindow::EventContentPaneScrolled = "EventContentPaneScrolled";

    const size_t mmLayerTextWindow::FILE_CACHE_BUFFER_LENGTH = 1024;

    mmLayerTextWindow::mmLayerTextWindow(const CEGUI::String& type, const CEGUI::String& name)
        : mmLayerContext(type, name)
        , pView(NULL)
        , pItem(NULL)

        , pIconvContext(NULL)

        , hTextCursorIndex(0)
        , hTextLineNumber(0)

        , hViewableArea(0, 0)
        , hChildrenPixelSize(0, 0)

        , pFile(NULL)
        , hTimerInterval(0.0)
        , hPeriodInterval(0.01)
        , hNumberLineOnce(40)
        , hAnalyseSize(0)
        , hAnalyseCode(AC_closed)
        , hAnalyseStatus(1)
        , hNeedInvalidate(1)
    {
        struct mmVectorValueEventAllocator hEventAllocator;

        mmStreambuf_Init(&this->hStreambuf);
        mmVectorValue_Init(&this->hItemTextVector);
        mmLayerUtilityItemPool_Init(&this->hItemPool);

        hEventAllocator.Produce = &mmVectorValue_CEGUIStringEventProduce;
        hEventAllocator.Recycle = &mmVectorValue_CEGUIStringEventRecycle;
        hEventAllocator.obj = this;
        mmVectorValue_SetElemSize(&this->hItemTextVector, sizeof(CEGUI::String));
        mmVectorValue_SetEventAllocator(&this->hItemTextVector, &hEventAllocator);
        //
        mmLayerUtilityItemPool_SetType(&this->hItemPool, "mm/mmLayerTextItem");
    }
    mmLayerTextWindow::~mmLayerTextWindow(void)
    {
        mmStreambuf_Destroy(&this->hStreambuf);
        mmVectorValue_Destroy(&this->hItemTextVector);
    }
    void mmLayerTextWindow::OnFinishLaunching(void)
    {
        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        this->pView = (mmWidgetScrollable*)pWindowManager->createWindow("mmWidgets/WidgetScrollableY");
        this->addChild(this->pView);

        // Manual set whole area.
        CEGUI::URect hWholeArea(CEGUI::UDim(0, 0), CEGUI::UDim(0, 0), CEGUI::UDim(1, 0), CEGUI::UDim(1, 0));
        this->pView->setArea(hWholeArea);

        // attributes.
        this->pView->SetMaxInvisibleNumber(1);

        mmLayerUtilityItemPool_OnFinishLaunching(&this->hItemPool);

        struct mmWidgetScrollableChildrenCreator hChildrenCreator;
        hChildrenCreator.obj = this;
        hChildrenCreator.Produce = &__static_ChildrenCreatorTextItem_Produce;
        hChildrenCreator.Recycle = &__static_ChildrenCreatorTextItem_Recycle;
        hChildrenCreator.FinishAttach = &__static_ChildrenCreatorTextItem_FinishAttach;
        hChildrenCreator.BeforeDetach = &__static_ChildrenCreatorTextItem_BeforeDetach;
        //
        const CEGUI::Sizef& hPixelSize = this->pView->getPixelSize();
        const CEGUI::Font* pFont = this->pView->getFont();
        float hFontHeight = pFont->getFontHeight();
        //
        this->hChildrenPixelSize.d_x = hPixelSize.d_width;
        this->hChildrenPixelSize.d_y = hFontHeight * 1.1f / hPixelSize.d_height;
        //
        CEGUI::USize hChildrenSize;
        hChildrenSize.d_height.d_scale = hFontHeight * 1.1f / hPixelSize.d_height;
        hChildrenSize.d_height.d_offset = 0.0f;
        hChildrenSize.d_width.d_scale = 1.0f;
        hChildrenSize.d_width.d_offset = 0.0f;
        //
        this->pView->SetChildrenCreator(&hChildrenCreator);
        this->pView->SetChildrenSize(hChildrenSize);
        this->pView->OnFinishLaunching();

        this->hLayerEventSizedConn = this->subscribeEvent(CEGUI::Element::EventSized, CEGUI::Event::Subscriber(&mmLayerTextWindow::OnLayerEventSized, this));
        this->hLayerEventUpdatedConn = this->subscribeEvent(CEGUI::Window::EventUpdated, CEGUI::Event::Subscriber(&mmLayerTextWindow::OnLayerEventUpdated, this));

        this->hEventCursorMovedConn = this->subscribeEvent(mmLayerContext::EventCursorMoved, CEGUI::Event::Subscriber(&mmLayerTextWindow::OnHandleEventCursorMoved, this));
        this->hEventCursorBeganConn = this->subscribeEvent(mmLayerContext::EventCursorBegan, CEGUI::Event::Subscriber(&mmLayerTextWindow::OnHandleEventCursorBegan, this));
        this->hEventCursorEndedConn = this->subscribeEvent(mmLayerContext::EventCursorEnded, CEGUI::Event::Subscriber(&mmLayerTextWindow::OnHandleEventCursorEnded, this));

        this->hEventContentPaneScrolledConn = this->pView->subscribeEvent(CEGUI::ScrollablePane::EventContentPaneScrolled, CEGUI::Event::Subscriber(&mmLayerTextWindow::OnHandleEventContentPaneScrolled, this));
    }
    void mmLayerTextWindow::OnBeforeTerminate(void)
    {
        this->hEventCursorMovedConn->disconnect();
        this->hEventCursorBeganConn->disconnect();
        this->hEventCursorEndedConn->disconnect();

        this->hLayerEventSizedConn->disconnect();
        this->hLayerEventUpdatedConn->disconnect();

        this->hEventContentPaneScrolledConn->disconnect();

        this->pView->OnBeforeTerminate();
        this->pView = NULL;

        mmLayerUtilityItemPool_OnBeforeTerminate(&this->hItemPool);

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        pWindowManager->destroyWindow(this->pView);
        this->pView = NULL;
    }
    void mmLayerTextWindow::SetItem(struct mmExplorerItem* pItem)
    {
        this->pItem = pItem;
    }
    void mmLayerTextWindow::SetIconvContext(struct mmIconvContext* pIconvContext)
    {
        this->pIconvContext = pIconvContext;
    }
    void mmLayerTextWindow::SetScrollPosition(float hScrollPosition)
    {
        assert(NULL != this->pView && "NULL != this->pView is invalid.");
        this->pView->SetScrollPosition(hScrollPosition);
    }
    float mmLayerTextWindow::GetScrollPosition(void) const
    {
        assert(NULL != this->pView && "NULL != this->pView is invalid.");
        return this->pView->GetScrollPosition();
    }
    void mmLayerTextWindow::CurrentIndex(size_t* b, size_t* e)
    {
        assert(NULL != this->pView && "NULL != this->pView is invalid.");
        this->pView->CurrentIndex(b, e);
    }
    int mmLayerTextWindow::GetAnalyseCode(void) const
    {
        return this->hAnalyseCode;
    }
    void mmLayerTextWindow::SetAnalyseStatus(int hAnalyseStatus)
    {
        this->hAnalyseStatus = hAnalyseStatus;
    }
    int mmLayerTextWindow::GetAnalyseStatus(void) const
    {
        return this->hAnalyseStatus;
    }
    void mmLayerTextWindow::ReloadAnalyse(void)
    {
        this->RefreshViewableArea();
        this->RefreshText();
        this->RefreshView();
    }
    void mmLayerTextWindow::UpdateLayerValue(void)
    {
        this->UpdateUtf8View();

        this->hNeedInvalidate = 1;
    }
    void mmLayerTextWindow::UpdateUtf8View(void)
    {
        this->RefreshView();
    }
    void mmLayerTextWindow::UpdateAnalyseFile(void)
    {
        if (1 == this->hAnalyseStatus)
        {
            this->AnalyseFile();
        }
        //this->pView->invalidate();
    }
    void mmLayerTextWindow::UpdateScrollableAttributes(void)
    {
        this->pView->SetIndexSize(this->hItemTextVector.size);
        this->pView->NotifyAttributesChanged();
        this->pView->NotifyTrimChildrenNode();
    }
    void mmLayerTextWindow::RefreshViewableArea(void)
    {
        CEGUI::Scrollbar* VertScrollbar = this->pView->getVertScrollbar();
        CEGUI::Scrollbar* HorzScrollbar = this->pView->getHorzScrollbar();
        //
        const CEGUI::WidgetLookFeel& wlf_VertScrollbar = VertScrollbar->getWindowRenderer()->getLookNFeel();
        CEGUI::Rectf _area_trackta_v(wlf_VertScrollbar.getNamedArea("ThumbTrackArea").getArea().getPixelRect(*VertScrollbar));

        const CEGUI::WidgetLookFeel& wlf_HorzScrollbar = HorzScrollbar->getWindowRenderer()->getLookNFeel();
        CEGUI::Rectf _area_trackta_h(wlf_HorzScrollbar.getNamedArea("ThumbTrackArea").getArea().getPixelRect(*HorzScrollbar));

        const CEGUI::WidgetLookFeel& wlf_view = this->pView->getWindowRenderer()->getLookNFeel();
        CEGUI::Rectf _area_ViewableAreaVScroll = wlf_view.getNamedArea("ViewableAreaVScroll").getArea().getPixelRect(*this->pView);
        CEGUI::Rectf _area_ViewableAreaHScroll = wlf_view.getNamedArea("ViewableAreaHScroll").getArea().getPixelRect(*this->pView);

        this->hViewableArea.d_x = _area_ViewableAreaVScroll.getWidth() - _area_trackta_v.getWidth();
        this->hViewableArea.d_y = _area_ViewableAreaHScroll.getHeight() - _area_trackta_h.getHeight();
    }
    void mmLayerTextWindow::RefreshView(void)
    {
        this->UpdateScrollableAttributes();
        this->UpdateItemEventCurrent();
    }
    void mmLayerTextWindow::AnalyseText(const CEGUI::String& hText)
    {
        size_t index = 0;
        CEGUI::String* pTextRef = NULL;

        index = this->hItemTextVector.size;
        mmVectorValue_AlignedMemory(&this->hItemTextVector, index + 1);
        pTextRef = (CEGUI::String*)mmVectorValue_GetIndexReference(&this->hItemTextVector, index);
        pTextRef->assign(hText);
    }
    void mmLayerTextWindow::AnalyseLine(mmUInt8_t* buffer, size_t offset, size_t length)
    {
        const CEGUI::Font* pFont = this->pView->getFont();

        struct mmIconvContext* pIconvContext = this->pIconvContext;

        const CEGUI::FontGlyph* glyph = NULL;
        float cur_extent = 0, adv_extent = 0, width;
        float x_scale = 1.0f;
        float _extent = 0.0f;
        float _extent_next = 0.0f;
        size_t c = 0;

        CEGUI::String _text_utf8_line;
        CEGUI::utf32 codepoint = 0;

        CEGUI::String _text_utf8;
        struct mmByteBuffer i_byte_buffer;
        struct mmByteBuffer o_byte_buffer;

        i_byte_buffer.buffer = (mmUInt8_t*)buffer;
        i_byte_buffer.offset = offset;
        i_byte_buffer.length = length;
        mmIconvContext_HostToView(pIconvContext, &i_byte_buffer, &o_byte_buffer);
        _text_utf8.assign((const CEGUI::utf8*)(o_byte_buffer.buffer + o_byte_buffer.offset), o_byte_buffer.length);

        _text_utf8_line.reserve(_text_utf8.size());

        for (c = 0; c < _text_utf8.length(); ++c)
        {
            codepoint = _text_utf8[c];

            glyph = pFont->getGlyphData(codepoint);

            if (glyph)
            {
                width = glyph->getRenderedAdvance(x_scale);

                if (adv_extent + width > cur_extent)
                {
                    cur_extent = adv_extent + width;
                }

                adv_extent += glyph->getAdvance(x_scale);

                _extent_next = ceguimax(adv_extent, cur_extent);

                if (_extent_next >= this->hViewableArea.d_x)
                {
                    this->AnalyseText(_text_utf8_line);

                    cur_extent = 0;
                    adv_extent = 0;

                    _text_utf8_line.clear();
                }
            }

            _text_utf8_line.push_back(codepoint);

            _extent = ceguimax(adv_extent, cur_extent);
        }

        if (!_text_utf8_line.empty())
        {
            this->AnalyseText(_text_utf8_line);

            cur_extent = 0;
            adv_extent = 0;

            _text_utf8_line.clear();
        }
    }
    void mmLayerTextWindow::AnalyseBuff(void)
    {
        mmUInt8_t* buffer = NULL;
        size_t length = 0;

        while (this->hTextCursorIndex < this->hStreambuf.pptr)
        {
            if (this->hStreambuf.buff[this->hTextCursorIndex] == '\n')
            {
                this->hTextCursorIndex++;

                this->hTextLineNumber++;

                buffer = (this->hStreambuf.buff);
                length = this->hTextCursorIndex - this->hStreambuf.gptr;

                this->AnalyseLine(buffer, this->hStreambuf.gptr, length);

                mmStreambuf_Gbump(&this->hStreambuf, length);
            }
            else
            {
                this->hTextCursorIndex++;
            }
        }
    }
    void mmLayerTextWindow::AnalyseFile(void)
    {
        if (NULL != this->pFile)
        {
            size_t n = 0;

            mmUInt8_t* buffer = NULL;
            size_t length = FILE_CACHE_BUFFER_LENGTH;

            size_t _line_number = this->hTextLineNumber;
            int _need_next = 0;

            do
            {
                mmStreambuf_AlignedMemory(&this->hStreambuf, length);
                buffer = (this->hStreambuf.buff + this->hStreambuf.pptr);

                n = fread(buffer, 1, FILE_CACHE_BUFFER_LENGTH, this->pFile);
                while (0 < n && n <= FILE_CACHE_BUFFER_LENGTH)
                {
                    mmStreambuf_Pbump(&this->hStreambuf, n);

                    this->AnalyseBuff();

                    this->hAnalyseSize += n;

                    mmStreambuf_Removeget(&this->hStreambuf);
                    this->hTextCursorIndex = 0;

                    if (this->hTextLineNumber - _line_number > this->hNumberLineOnce)
                    {
                        _need_next = 1;
                        break;
                    }
                    else
                    {
                        _need_next = 0;
                    }

                    mmStreambuf_AlignedMemory(&this->hStreambuf, length);
                    buffer = (this->hStreambuf.buff + this->hStreambuf.pptr);

                    n = fread(buffer, 1, FILE_CACHE_BUFFER_LENGTH, this->pFile);
                }

                if (1 == _need_next)
                {
                    // not complete we need next once.
                    break;
                }

                if (0 != mmStreambuf_Size(&this->hStreambuf))
                {
                    this->hTextLineNumber++;

                    buffer = (this->hStreambuf.buff);
                    length = mmStreambuf_Size(&this->hStreambuf);

                    this->AnalyseLine(buffer, this->hStreambuf.gptr, length);

                    mmStreambuf_Removeget(&this->hStreambuf);
                    this->hTextCursorIndex = 0;

                    mmStreambuf_Gbump(&this->hStreambuf, length);
                }

                // analyse complete.
                fclose(this->pFile);
                this->pFile = NULL;

                this->hAnalyseCode = AC_finish;
            } while (0);

            this->UpdateScrollableAttributes();

            CEGUI::WindowEventArgs hArgs(this);
            this->fireEvent(EventUpdateAnalyse, hArgs, EventNamespace);
        }
    }
    void mmLayerTextWindow::RefreshText(void)
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        do
        {
            mmVectorValue_AlignedMemory(&this->hItemTextVector, 0);

            if (NULL == this->pItem)
            {
                // need do nothing here.
                break;
            }

            if (mmString_Size(&this->pItem->fullname) == 0)
            {
                mmLogger_LogW(gLogger, "%s %d filename is invalid.", __FUNCTION__, __LINE__);
                break;
            }

            if (NULL != this->pFile)
            {
                fclose(this->pFile);
                this->pFile = NULL;
            }

            this->hAnalyseCode = AC_closed;

            this->pFile = fopen(mmString_CStr(&this->pItem->fullname), "rb");

            if (NULL == this->pFile)
            {
                mmLogger_LogW(gLogger, "%s %d can not fopen file: %s", __FUNCTION__, __LINE__, mmString_CStr(&this->pItem->fullname));
                break;
            }

            this->hAnalyseCode = AC_motion;

            mmStreambuf_Reset(&this->hStreambuf);
            this->hTextCursorIndex = this->hStreambuf.gptr;
            this->hTextLineNumber = 0;
            this->hAnalyseSize = 0;

            // the first time.
            this->AnalyseFile();
        } while (0);
    }
    void mmLayerTextWindow::CreateItemEvent(mmLayerTextItem* pLayerItem)
    {
        pLayerItem->SetIconvContext(this->pIconvContext);

        this->UpdateItemEvent(pLayerItem);
    }
    void mmLayerTextWindow::DeleteItemEvent(mmLayerTextItem* pLayerItem)
    {
        pLayerItem->SetItem(NULL);
    }
    void mmLayerTextWindow::UpdateItemEvent(mmLayerTextItem* pLayerItem)
    {
        struct mmVectorValue* pVectorItem = &this->hItemTextVector;
        CEGUI::String* pItem = (CEGUI::String*)mmVectorValue_GetIndexReference(pVectorItem, pLayerItem->hIndex);

        pLayerItem->SetItem(pItem);
        pLayerItem->UpdateLayerValue();
    }
    void mmLayerTextWindow::UpdateItemEventCurrent(void)
    {
        // item view.
        struct mmRbtreeU64Vpt* pWindowRbtree = this->pView->GetWindowRbtree();

        CEGUI::Window* pChild = NULL;
        struct mmRbNode* n = NULL;
        struct mmRbtreeU64VptIterator* it = NULL;
        mmLayerTextItem* pLayerItem = NULL;
        // max
        n = mmRb_First(&pWindowRbtree->rbt);
        while (NULL != n)
        {
            it = (struct mmRbtreeU64VptIterator*)mmRb_Entry(n, struct mmRbtreeU64VptIterator, n);
            // next
            n = mmRb_Next(n);

            pChild = (CEGUI::Window*)it->v;

            pLayerItem = (mmLayerTextItem*)pChild;

            this->UpdateItemEvent(pLayerItem);
        }
    }
    bool mmLayerTextWindow::OnLayerEventSized(const CEGUI::EventArgs& args)
    {
        this->UpdateLayerValue();
        return false;
    }
    bool mmLayerTextWindow::OnLayerEventUpdated(const CEGUI::EventArgs& args)
    {
        const CEGUI::UpdateEventArgs& evt = (const CEGUI::UpdateEventArgs&)(args);

        this->hTimerInterval += evt.d_timeSinceLastFrame;

        if (this->hTimerInterval >= this->hPeriodInterval)
        {
            this->hTimerInterval = 0.0;

            this->UpdateAnalyseFile();
        }

        if (1 == this->hNeedInvalidate)
        {
            // we use cache for force AutoRenderingSurface, need manual invalidate.
            this->invalidate();

            this->hNeedInvalidate = 0;
        }
        return true;
    }
    bool mmLayerTextWindow::OnHandleEventCursorMoved(const CEGUI::EventArgs& args)
    {
        const mmCursorEventArgs& evt = (const mmCursorEventArgs&)(args);
        this->pView->OnEventCursorMoved(evt.content);
        return false;
    }
    bool mmLayerTextWindow::OnHandleEventCursorBegan(const CEGUI::EventArgs& args)
    {
        const mmCursorEventArgs& evt = (const mmCursorEventArgs&)(args);
        this->pView->OnEventCursorBegan(evt.content);
        return false;
    }
    bool mmLayerTextWindow::OnHandleEventCursorEnded(const CEGUI::EventArgs& args)
    {
        const mmCursorEventArgs& evt = (const mmCursorEventArgs&)(args);
        this->pView->OnEventCursorEnded(evt.content);
        return false;
    }
    bool mmLayerTextWindow::OnHandleEventContentPaneScrolled(const CEGUI::EventArgs& args)
    {
        CEGUI::WindowEventArgs hArgs(this);
        this->fireEvent(EventContentPaneScrolled, hArgs, EventNamespace);
        return false;
    }
}
