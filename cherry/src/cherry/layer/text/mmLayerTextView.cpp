/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerTextView.h"
#include "mmLayerTextWindow.h"

#include "layer/common/mmLayerCommonIconvEncode.h"

#include "layer/utility/mmLayerUtilityIconv.h"

#include "CEGUI/WindowManager.h"

#include "layer/director/mmLayerDirector.h"

#include "explorer/mmExplorerItem.h"

namespace mm
{
    const CEGUI::String mmLayerTextView::EventNamespace = "mm";
    const CEGUI::String mmLayerTextView::WidgetTypeName = "mm/mmLayerTextView";

    const size_t mmLayerTextView::FILE_CACHE_BUFFER_LENGTH = 1024;

    mmLayerTextView::mmLayerTextView(const CEGUI::String& type, const CEGUI::String& name)
        : mmLayerContext(type, name)
        , LayerTextView(NULL)
        , StaticImageBackground(NULL)

        , StaticTextFps(NULL)
        , WidgetImageButtonBack(NULL)

        , LayerTextWindowScreen(NULL)

        , LayerCommonIconvEncodeWindow(NULL)

        , StaticTextSize(NULL)
        , StaticTextLineNumber(NULL)

        , ProgressBarAnalyse(NULL)
        , StaticImageAnalyseStop(NULL)
        , StaticImageAnalysePlay(NULL)
        , StaticTextStatus(NULL)

        , pCommonIconvEncode(NULL)
        , pLayerTextWindow(NULL)

        , pItem(NULL)

        , hColourLustre(0.0f, 1.0f, 0.0f, 1.0f)
        , hColourCommon(1.0f, 1.0f, 1.0f, 1.0f)
    {
        mmIconvContext_Init(&this->hIconvContext);

        mmIconvContext_Cachelist(&this->hIconvContext);
        mmIconvContext_UpdateFormatLocale(&this->hIconvContext, "utf-8");
    }
    mmLayerTextView::~mmLayerTextView(void)
    {
        mmIconvContext_Destroy(&this->hIconvContext);
    }

    void mmLayerTextView::OnFinishLaunching(void)
    {
        struct mmSurfaceMaster* pSurfaceMaster = this->pSurfaceMaster;

        struct mmFrameScheduler* pFrameScheduler = &pSurfaceMaster->hFrameScheduler;

        // most of time the text format will be "utf-8".
        mmIconvContext_UpdateFormat(&this->hIconvContext, "utf-8", "utf-8");

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        this->LayerTextView = pWindowManager->loadLayoutFromFile("text/LayerTextView.layout");
        this->addChild(this->LayerTextView);

        this->StaticImageBackground = this->LayerTextView->getChild("StaticImageBackground");

        this->StaticTextFps = this->StaticImageBackground->getChild("StaticTextFps");
        this->WidgetImageButtonBack = this->StaticImageBackground->getChild("WidgetImageButtonBack");

        this->LayerTextWindowScreen = this->StaticImageBackground->getChild("LayerTextWindowScreen");

        this->LayerCommonIconvEncodeWindow = this->StaticImageBackground->getChild("LayerCommonIconvEncodeWindow");

        this->StaticTextSize = this->StaticImageBackground->getChild("StaticTextSize");
        this->StaticTextLineNumber = this->StaticImageBackground->getChild("StaticTextLineNumber");

        this->ProgressBarAnalyse = this->StaticImageBackground->getChild("ProgressBarAnalyse");
        this->StaticImageAnalyseStop = this->ProgressBarAnalyse->getChild("StaticImageAnalyseStop");
        this->StaticImageAnalysePlay = this->ProgressBarAnalyse->getChild("StaticImageAnalysePlay");

        this->StaticTextStatus = this->StaticImageBackground->getChild("StaticTextStatus");

        this->pCommonIconvEncode = (mmLayerCommonIconvEncode*)this->LayerCommonIconvEncodeWindow;
        this->pLayerTextWindow = (mmLayerTextWindow*)this->LayerTextWindowScreen;

        this->hUtilityFps.SetFrameTimer(&pFrameScheduler->frame_timer);
        this->hUtilityFps.SetWindow(this->StaticTextFps);
        this->hUtilityFps.OnFinishLaunching();

        this->hFpsEventMouseClickConn = this->StaticTextFps->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerTextView::OnStaticTextFpsEventMouseClick, this));
        this->hBackEventMouseClickConn = this->WidgetImageButtonBack->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerTextView::OnWidgetImageButtonBackEventMouseClick, this));

        this->pCommonIconvEncode->SetDirector(this->pDirector);
        this->pCommonIconvEncode->SetContextMaster(this->pContextMaster);
        this->pCommonIconvEncode->SetSurfaceMaster(this->pSurfaceMaster);
        this->pCommonIconvEncode->SetIconvContext(&this->hIconvContext);
        this->pCommonIconvEncode->OnFinishLaunching();
        //
        this->LayerCommonIconvEncodeWindow->subscribeEvent(mmLayerCommonIconvEncode::EventIconvCoderChanged, CEGUI::Event::Subscriber(&mmLayerTextView::OnLayerCommonIconvEncodeWindowEventIconvCoderChanged, this));

        this->pLayerTextWindow->SetDirector(this->pDirector);
        this->pLayerTextWindow->SetContextMaster(this->pContextMaster);
        this->pLayerTextWindow->SetSurfaceMaster(this->pSurfaceMaster);
        this->pLayerTextWindow->SetIconvContext(&this->hIconvContext);
        this->pLayerTextWindow->SetItem(this->pItem);
        this->pLayerTextWindow->OnFinishLaunching();
        //
        this->hTextEventUpdateAnalyseConn = this->pLayerTextWindow->subscribeEvent(mmLayerTextWindow::EventUpdateAnalyse, CEGUI::Event::Subscriber(&mmLayerTextView::OnLayerTextWindowScreenEventUpdateAnalyse, this));
        this->hTextEventContentPaneScrolledConn = this->pLayerTextWindow->subscribeEvent(mmLayerTextWindow::EventContentPaneScrolled, CEGUI::Event::Subscriber(&mmLayerTextView::OnLayerTextWindowScreenEventContentPaneScrolled, this));

        this->hAnalyseEventMouseClickConn = this->ProgressBarAnalyse->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerTextView::OnAnalyseEventMouseClick, this));

        // spacing common CEGUI::Window object, we need manual attach context event.
        this->AttachContextEvent(this->pCommonIconvEncode);
        this->AttachContextEvent(this->pLayerTextWindow);
    }

    void mmLayerTextView::OnBeforeTerminate(void)
    {
        struct mmSurfaceMaster* pSurfaceMaster = this->pSurfaceMaster;

        // spacing common CEGUI::Window object, we need manual attach context event.
        this->DetachContextEvent(this->pCommonIconvEncode);
        this->DetachContextEvent(this->pLayerTextWindow);

        this->hFpsEventMouseClickConn->disconnect();
        this->hBackEventMouseClickConn->disconnect();
        //
        this->hTextEventUpdateAnalyseConn->disconnect();
        this->hTextEventContentPaneScrolledConn->disconnect();
        //
        this->hAnalyseEventMouseClickConn->disconnect();

        this->pCommonIconvEncode->OnBeforeTerminate();
        this->pLayerTextWindow->OnBeforeTerminate();
        this->hUtilityFps.OnBeforeTerminate();

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        pWindowManager->destroyWindow(this->LayerTextView);
        this->LayerTextView = NULL;
    }
    void mmLayerTextView::SetItem(struct mmExplorerItem* pItem)
    {
        this->pItem = pItem;
        this->pLayerTextWindow->SetItem(this->pItem);
    }
    int mmLayerTextView::GetAnalyseCode(void) const
    {
        return this->pLayerTextWindow->GetAnalyseCode();
    }
    void mmLayerTextView::ReloadAnalyse(void)
    {
        this->pLayerTextWindow->ReloadAnalyse();
    }
    void mmLayerTextView::UpdateLayerValue(void)
    {
        char hSizeString[32] = { 0 };

        mmExplorerItem_SizeString(this->pItem, hSizeString);
        this->StaticTextSize->setText(hSizeString);

        this->UpdateUtf8View();

        this->UpdateAnalyseStatus();
        this->UpdateLineNumber();

        this->pLayerTextWindow->UpdateLayerValue();
    }
    void mmLayerTextView::UpdateUtf8View(void)
    {
        this->pLayerTextWindow->ReloadAnalyse();
        this->pLayerTextWindow->UpdateUtf8View();
    }
    void mmLayerTextView::UpdateLineNumber(void)
    {
        char hLineNumberString[128] = { 0 };
        size_t _index_b = 0;
        size_t _index_e = 0;
        size_t _index_n = 0;
        size_t _vector_size = 0;

        this->pLayerTextWindow->CurrentIndex(&_index_b, &_index_e);

        _index_n = _index_e - _index_b;
        _vector_size = this->pLayerTextWindow->hItemTextVector.size;

        mmSprintf(hLineNumberString, "[%" PRIuPTR ", %" PRIuPTR "]/%" PRIuPTR "", _index_b, _index_n, (size_t)_vector_size);
        this->StaticTextLineNumber->setText(hLineNumberString);
    }
    void mmLayerTextView::UpdateAnalyseStatus(void)
    {
        char hStatusString[128] = { 0 };
        CEGUI::String _current_progress_string;
        CEGUI::Colour _colour;
        CEGUI::String hColourString;

        float _current_progress = (float)(this->pLayerTextWindow->hAnalyseSize) / (float)(this->pItem->s.st_size);

        mmSprintf(hStatusString, "%" PRIuPTR "/%" PRIuPTR "", this->pLayerTextWindow->hAnalyseSize, (size_t)this->pItem->s.st_size);
        _current_progress_string = CEGUI::PropertyHelper<float>::toString(_current_progress);

        if (this->pLayerTextWindow->hAnalyseSize == this->pItem->s.st_size)
        {
            _colour = this->hColourLustre;
            hColourString = CEGUI::PropertyHelper<CEGUI::Colour>::toString(_colour);

            this->ProgressBarAnalyse->setProperty("CurrentProgress", _current_progress_string);
            this->StaticTextStatus->setText(hStatusString);
            this->StaticTextStatus->setProperty("TextColours", hColourString);
        }
        else
        {
            _colour = this->hColourCommon;
            hColourString = CEGUI::PropertyHelper<CEGUI::Colour>::toString(_colour);

            this->ProgressBarAnalyse->setProperty("CurrentProgress", _current_progress_string);
            this->StaticTextStatus->setText(hStatusString);
            this->StaticTextStatus->setProperty("TextColours", hColourString);
        }
        //
        this->UpdateAnalyseStatusToolbar();
    }
    void mmLayerTextView::UpdateAnalyseStatusToolbar(void)
    {
        int hAnalyseStatus = this->pLayerTextWindow->GetAnalyseStatus();

        this->StaticImageAnalyseStop->setVisible(0 == hAnalyseStatus);
        this->StaticImageAnalysePlay->setVisible(1 == hAnalyseStatus);
    }
    bool mmLayerTextView::OnLayerCommonIconvEncodeWindowEventIconvCoderChanged(const CEGUI::EventArgs& args)
    {
        this->UpdateUtf8View();
        return true;
    }
    bool mmLayerTextView::OnLayerTextWindowScreenEventUpdateAnalyse(const CEGUI::EventArgs& args)
    {
        this->UpdateAnalyseStatus();
        this->UpdateLineNumber();
        return true;
    }
    bool mmLayerTextView::OnLayerTextWindowScreenEventContentPaneScrolled(const CEGUI::EventArgs& args)
    {
        this->UpdateLineNumber();
        return true;
    }
    bool mmLayerTextView::OnAnalyseEventMouseClick(const CEGUI::EventArgs& args)
    {
        int hAnalyseStatus = this->pLayerTextWindow->GetAnalyseStatus();
        this->pLayerTextWindow->SetAnalyseStatus(0 == hAnalyseStatus ? 1 : 0);

        this->UpdateAnalyseStatusToolbar();
        return true;
    }
    bool mmLayerTextView::OnStaticTextFpsEventMouseClick(const CEGUI::EventArgs& args)
    {
        mmLayerDirector_SceneExclusiveForeground(this->pDirector, "mm/mmLayerCommonTextureManager", "mmLayerCommonTextureManager");
        return true;
    }
    bool mmLayerTextView::OnWidgetImageButtonBackEventMouseClick(const CEGUI::EventArgs& args)
    {
        mmLayerDirector_SceneExclusiveEnterBackground(this->pDirector, this->getName().c_str());
        return true;
    }
}
