/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmLayerTextView_h__
#define __mmLayerTextView_h__

#include "core/mmCore.h"

#include "dish/mmEvent.h"

#include "layer/director/mmLayerContext.h"

#include "CEGUI/Window.h"

#include "layer/utility/mmLayerUtilityFps.h"

#include "toolkit/mmIconv.h"

struct mmExplorerItem;

namespace mm
{
    class mmLayerCommonIconvEncode;
    class mmLayerTextWindow;

    class mmLayerTextView : public mmLayerContext
    {
    public:
        static const CEGUI::String EventNamespace;          //!< Namespace for global events
        static const CEGUI::String WidgetTypeName;          //!< Window factory name
    public:
        static const size_t FILE_CACHE_BUFFER_LENGTH;
    public:
        CEGUI::Window* LayerTextView;
        CEGUI::Window* StaticImageBackground;

        CEGUI::Window* StaticTextFps;
        CEGUI::Window* WidgetImageButtonBack;

        CEGUI::Window* LayerTextWindowScreen;

        CEGUI::Window* LayerCommonIconvEncodeWindow;

        CEGUI::Window* StaticTextSize;
        CEGUI::Window* StaticTextLineNumber;

        CEGUI::Window* ProgressBarAnalyse;
        CEGUI::Window* StaticImageAnalyseStop;
        CEGUI::Window* StaticImageAnalysePlay;
        CEGUI::Window* StaticTextStatus;

        mmLayerCommonIconvEncode* pCommonIconvEncode;
        mmLayerTextWindow* pLayerTextWindow;
    public:
        struct mmIconvContext hIconvContext;
    public:
        // weak ref.
        struct mmExplorerItem* pItem;
    public:
        std::string hCurrentItemAssets;
    public:
        mmLayerUtilityFps hUtilityFps;
    public:
        CEGUI::Colour hColourLustre;
        CEGUI::Colour hColourCommon;
    public:
        CEGUI::Event::Connection hFpsEventMouseClickConn;
        CEGUI::Event::Connection hBackEventMouseClickConn;
        //
        CEGUI::Event::Connection hTextEventUpdateAnalyseConn;
        CEGUI::Event::Connection hTextEventContentPaneScrolledConn;
        //
        CEGUI::Event::Connection hAnalyseEventMouseClickConn;
    public:
        mmLayerTextView(const CEGUI::String& type, const CEGUI::String& name);
        virtual ~mmLayerTextView(void);
    public:
        // mmLayerContext
        virtual void OnFinishLaunching(void);
        virtual void OnBeforeTerminate(void);
    public:
        void SetItem(struct mmExplorerItem* _item);
    public:
        int GetAnalyseCode(void) const;
    public:
        void ReloadAnalyse(void);
    public:
        void UpdateLayerValue(void);
        void UpdateUtf8View(void);
    private:
        void UpdateLineNumber(void);
        void UpdateAnalyseStatus(void);
        void UpdateAnalyseStatusToolbar(void);
    private:
        bool OnLayerCommonIconvEncodeWindowEventIconvCoderChanged(const CEGUI::EventArgs& args);
        bool OnLayerTextWindowScreenEventUpdateAnalyse(const CEGUI::EventArgs& args);
        bool OnLayerTextWindowScreenEventContentPaneScrolled(const CEGUI::EventArgs& args);
        bool OnAnalyseEventMouseClick(const CEGUI::EventArgs& args);
    private:
        bool OnStaticTextFpsEventMouseClick(const CEGUI::EventArgs& args);
        bool OnWidgetImageButtonBackEventMouseClick(const CEGUI::EventArgs& args);
    };
}

#endif//__mmLayerTextView_h__