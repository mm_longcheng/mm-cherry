/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmLayerTextWindow_h__
#define __mmLayerTextWindow_h__

#include "core/mmStreambuf.h"

#include "container/mmVectorValue.h"

#include "CEGUI/Window.h"

#include "layer/director/mmLayerContext.h"

#include "layer/utility/mmLayerUtilityItemPool.h"

struct mmExplorerItem;

struct mmIconvContext;

namespace mm
{
    class mmWidgetScrollable;

    class mmLayerTextItem;

    class mmLayerTextWindow : public mmLayerContext
    {
    public:
        static const CEGUI::String EventNamespace;          //!< Namespace for global events
        static const CEGUI::String WidgetTypeName;          //!< Window factory name
    public:
        static const CEGUI::String EventUpdateAnalyse;
        static const CEGUI::String EventContentPaneScrolled;
    public:
        static const size_t FILE_CACHE_BUFFER_LENGTH;
    public:
        enum AnalyseCode_t
        {
            AC_closed = 0,// analyse not start or be closed or be interrupt.
            AC_motion = 1,// analyse running.
            AC_finish = 2,// analyse finisht.
        };
    public:
        mmWidgetScrollable* pView;
    public:
        // weak ref.
        struct mmExplorerItem* pItem;
    public:
        struct mmIconvContext* pIconvContext;
    public:
        // cache streambuf
        struct mmStreambuf hStreambuf;
        //
        size_t hTextCursorIndex;
        size_t hTextLineNumber;
        // cache value.
        CEGUI::Vector2f hViewableArea;
        // cache pixel size.
        CEGUI::Vector2f hChildrenPixelSize;
    public:
        // FILE
        FILE* pFile;
        // time
        double hTimerInterval;
        double hPeriodInterval;
        // default is 40.
        size_t hNumberLineOnce;
        //
        size_t hAnalyseSize;
        //
        int hAnalyseCode;
        // 
        int hAnalyseStatus;
        //
        int hNeedInvalidate;
    public:
        struct mmVectorValue hItemTextVector;
    public:
        struct mmLayerUtilityItemPool hItemPool;
    public:
        CEGUI::Event::Connection hLayerEventSizedConn;
        CEGUI::Event::Connection hLayerEventUpdatedConn;
    private:
        CEGUI::Event::Connection hEventCursorMovedConn;
        CEGUI::Event::Connection hEventCursorBeganConn;
        CEGUI::Event::Connection hEventCursorEndedConn;
    public:
        CEGUI::Event::Connection hEventContentPaneScrolledConn;
    public:
        mmLayerTextWindow(const CEGUI::String& type, const CEGUI::String& name);
        virtual ~mmLayerTextWindow(void);
    public:
        void OnFinishLaunching(void);
        void OnBeforeTerminate(void);
    public:
        void SetItem(struct mmExplorerItem* pItem);
        void SetIconvContext(struct mmIconvContext* pIconvContext);
    public:
        void SetScrollPosition(float hScrollPosition);
        float GetScrollPosition(void) const;
    public:
        void CurrentIndex(size_t* b, size_t* e);
    public:
        int GetAnalyseCode(void) const;
    public:
        void SetAnalyseStatus(int hAnalyseStatus);
        int GetAnalyseStatus(void) const;
    public:
        void ReloadAnalyse(void);
    public:
        void UpdateLayerValue(void);
        void UpdateUtf8View(void);

        void UpdateAnalyseFile(void);
        void UpdateScrollableAttributes(void);
    private:
        void RefreshViewableArea(void);
    private:
        void RefreshView(void);
        void AnalyseText(const CEGUI::String& hText);
        void AnalyseLine(mmUInt8_t* buffer, size_t offset, size_t length);
        void AnalyseBuff(void);
        void AnalyseFile(void);
        void RefreshText(void);
    public:
        void CreateItemEvent(mmLayerTextItem* pLayerItem);
        void DeleteItemEvent(mmLayerTextItem* pLayerItem);
        void UpdateItemEvent(mmLayerTextItem* pLayerItem);
        void UpdateItemEventCurrent(void);
    private:
        bool OnLayerEventSized(const CEGUI::EventArgs& args);
        bool OnLayerEventUpdated(const CEGUI::EventArgs& args);
    private:
        bool OnHandleEventCursorMoved(const CEGUI::EventArgs& args);
        bool OnHandleEventCursorBegan(const CEGUI::EventArgs& args);
        bool OnHandleEventCursorEnded(const CEGUI::EventArgs& args);
    private:
        bool OnHandleEventContentPaneScrolled(const CEGUI::EventArgs& args);
    };
}

#endif//__mmLayerTextWindow_h__
