/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerImageryView.h"

#include "CEGUI/WindowManager.h"
#include "CEGUI/ImageManager.h"
#include "CEGUI/Texture.h"

#include "OgreTextureManager.h"

#include "CEGUIOgreRenderer/Texture.h"

#include "layer/director/mmLayerDirector.h"

#include "explorer/mmExplorerItem.h"

namespace mm
{
    const CEGUI::String mmLayerImageryView::EventNamespace = "mm";
    const CEGUI::String mmLayerImageryView::WidgetTypeName = "mm/mmLayerImageryView";

    const std::string mmLayerImageryView::CEGUI_RTT_IMAGERY = "mmLayerImageryView_CEGUIRTTImagery_";
    const std::string mmLayerImageryView::CEGUI_RTT_TEXTURE = "mmLayerImageryView_CEGUIRTTTexture_";

    mmLayerImageryView::mmLayerImageryView(const CEGUI::String& type, const CEGUI::String& name)
        : mmLayerContext(type, name)
        , LayerImageryView(NULL)
        , StaticImageBackground(NULL)

        , StaticTextFps(NULL)
        , WidgetImageButtonBack(NULL)

        , StaticImageScreen(NULL)
        , StaticImageEmbellish(NULL)

        , StaticTextResolution(NULL)
        , StaticTextSize(NULL)

        , pItem(NULL)

        , hOgreTexture()
        , pImage(NULL)
        , pCEGUIOgreTexture(NULL)
        , hImageryIcon("")
        , hImageryName("")
        , hTextureName("")

        , hMaxImageryFileSize(67108864)
    {

    }
    mmLayerImageryView::~mmLayerImageryView(void)
    {
        
    }

    void mmLayerImageryView::OnFinishLaunching(void)
    {
        struct mmContextMaster* pContextMaster = this->pContextMaster;
        struct mmSurfaceMaster* pSurfaceMaster = this->pSurfaceMaster;

        struct mmFrameScheduler* pFrameScheduler = &pSurfaceMaster->hFrameScheduler;

        pSurfaceMaster->hEventSet.SubscribeEvent(mmSurfaceMaster_EventWindowSizeChanged, &mmLayerImageryView::OnEventWindowSizeChanged, this);

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        this->LayerImageryView = pWindowManager->loadLayoutFromFile("imagery/LayerImageryView.layout");
        this->addChild(this->LayerImageryView);

        this->StaticImageBackground = this->LayerImageryView->getChild("StaticImageBackground");

        this->StaticTextFps = this->StaticImageBackground->getChild("StaticTextFps");
        this->WidgetImageButtonBack = this->StaticImageBackground->getChild("WidgetImageButtonBack");

        this->StaticImageScreen = this->StaticImageBackground->getChild("StaticImageScreen");
        this->StaticImageEmbellish = this->StaticImageScreen->getChild("StaticImageEmbellish");

        this->StaticTextResolution = this->StaticImageBackground->getChild("StaticTextResolution");
        this->StaticTextSize = this->StaticImageBackground->getChild("StaticTextSize");

        this->hUtilityFps.SetFrameTimer(&pFrameScheduler->frame_timer);
        this->hUtilityFps.SetWindow(this->StaticTextFps);
        this->hUtilityFps.OnFinishLaunching();

        this->hFpsEventMouseClickConn = this->StaticTextFps->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerImageryView::OnStaticTextFpsEventMouseClick, this));
        this->hBackEventMouseClickConn = this->WidgetImageButtonBack->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerImageryView::OnWidgetImageButtonBackEventMouseClick, this));

        this->hImageryIcon = this->StaticImageScreen->getProperty("Image");
        this->hImageryName = CEGUI_RTT_IMAGERY + this->getName();
        this->hTextureName = CEGUI_RTT_TEXTURE + this->getName();
        //
        struct mmCEGUISystem* pCEGUISystem = &pContextMaster->hCEGUISystem;
        CEGUI::CEGUIOgreRenderer* pCEGUIOgreRenderer = pCEGUISystem->pRenderer;
        //
        this->pCEGUIOgreTexture = (CEGUI::CEGUIOgreTexture*)&pCEGUIOgreRenderer->createTexture(this->hTextureName);
        //
        CEGUI::ImageManager* pImageManager = CEGUI::ImageManager::getSingletonPtr();
        this->pImage = static_cast<CEGUI::BasicImage*>(&pImageManager->create("BasicImage", this->hImageryName));
        //
        struct mmAdaptiveSize* pAdaptiveSize = &pSurfaceMaster->hAdaptiveSize;
        Ogre::uint hPixelW = (Ogre::uint)(pAdaptiveSize->hActuallySafeRect[2]);
        Ogre::uint hPixelH = (Ogre::uint)(pAdaptiveSize->hActuallySafeRect[3]);

        this->pImage->setNativeResolution(CEGUI::Sizef((float)hPixelW, (float)hPixelH));
        this->pImage->setAutoScaled(CEGUI::ASM_Min);
    }

    void mmLayerImageryView::OnBeforeTerminate(void)
    {
        struct mmSurfaceMaster* pSurfaceMaster = this->pSurfaceMaster;

        this->RecyclePreviewTexture();

        CEGUI::ImageManager* pImageManager = CEGUI::ImageManager::getSingletonPtr();
        pImageManager->destroy(*this->pImage);
        this->pImage = NULL;
        //
        struct mmCEGUISystem* pCEGUISystem = &pContextMaster->hCEGUISystem;
        CEGUI::CEGUIOgreRenderer* pCEGUIOgreRenderer = pCEGUISystem->pRenderer;
        //
        pCEGUIOgreRenderer->destroyTexture(*this->pCEGUIOgreTexture);
        this->pCEGUIOgreTexture = NULL;

        this->hFpsEventMouseClickConn->disconnect();
        this->hBackEventMouseClickConn->disconnect();

        this->hUtilityFps.OnBeforeTerminate();

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        pWindowManager->destroyWindow(this->LayerImageryView);
        this->LayerImageryView = NULL;

        pSurfaceMaster->hEventSet.UnsubscribeEvent(mmSurfaceMaster_EventWindowSizeChanged, &mmLayerImageryView::OnEventWindowSizeChanged, this);
    }
    void mmLayerImageryView::SetItem(struct mmExplorerItem* pItem)
    {
        this->pItem = pItem;
    }
    void mmLayerImageryView::UpdateLayerValue(void)
    {
        char hSizeString[32] = { 0 };

        mmExplorerItem_SizeString(this->pItem, hSizeString);
        this->StaticTextSize->setText(hSizeString);

        this->RefreshPreviewTexture();
    }
    void mmLayerImageryView::UpdateImageAttributes(void)
    {
        struct mmSurfaceMaster* pSurfaceMaster = this->pSurfaceMaster;
        
        struct mmAdaptiveSize* pAdaptiveSize = &pSurfaceMaster->hAdaptiveSize;
        Ogre::uint hPixelW = (Ogre::uint)(pAdaptiveSize->hActuallySafeRect[2]);
        Ogre::uint hPixelH = (Ogre::uint)(pAdaptiveSize->hActuallySafeRect[3]);

        const CEGUI::Rectf& _rectf = this->StaticImageScreen->getOuterRectClipper();
        float _win_w = _rectf.getWidth();
        float _win_h = _rectf.getHeight();
        _win_w = _win_w < 1 ? 1 : _win_w;
        _win_h = _win_h < 1 ? 1 : _win_h;

        const CEGUI::Sizef& _tex_size = this->pCEGUIOgreTexture->getSize();

        float _tex_real_w = (float)_tex_size.d_width;
        float _tex_real_h = (float)_tex_size.d_height;

        float _native_resolution_w = _tex_real_w * (float)hPixelW / _win_w;
        float _native_resolution_h = _tex_real_h * (float)hPixelH / _win_h;

        CEGUI::Rectf _imageArea(0, 0, _tex_real_w, _tex_real_h);
        CEGUI::Sizef _imageNativeResolution(_native_resolution_w, _native_resolution_h);
        this->pImage->setArea(_imageArea);
        this->pImage->setNativeResolution(_imageNativeResolution);
        this->pImage->setAutoScaled(CEGUI::ASM_Min);
    }
    void mmLayerImageryView::ProducePreviewTexture(void)
    {
        Ogre::TextureManager* pTextureManager = Ogre::TextureManager::getSingletonPtr();

        assert(!this->hOgreTexture && "you must destroy ogre texture before load the new one.");

        this->hOgreTexture = pTextureManager->load(mmString_CStr(&this->pItem->basename), mmString_CStr(&this->pItem->pathname));

        Ogre::uint32 _w = this->hOgreTexture->getWidth();
        Ogre::uint32 _h = this->hOgreTexture->getHeight();

        CEGUI::Rectf _texture_area(0, 0, (float)_w, (float)_h);

        // weak reference mode.
        this->pCEGUIOgreTexture->setOgreTexture(this->hOgreTexture, _texture_area);

        this->pImage->setTexture(this->pCEGUIOgreTexture);

        this->UpdateImageAttributes();
    }
    void mmLayerImageryView::RecyclePreviewTexture(void)
    {
        if (this->hOgreTexture)
        {
            Ogre::TextureManager* pTextureManager = Ogre::TextureManager::getSingletonPtr();

            pTextureManager->remove(this->hOgreTexture);
            this->hOgreTexture.reset();

            CEGUI::Rectf _texture_area(0, 0, 0, 0);
            this->pCEGUIOgreTexture->setOgreTexture(this->hOgreTexture, _texture_area);
        }
    }
    void mmLayerImageryView::RefreshPreviewTexture(void)
    {
        if (this->hMaxImageryFileSize >= this->pItem->s.st_size)
        {
            this->StaticImageScreen->setProperty("Image", this->hImageryName);

            this->RecyclePreviewTexture();
            this->ProducePreviewTexture();

            char hTextureResolutionString[64] = { 0 };
            const CEGUI::Sizef& hTextureSize = this->pCEGUIOgreTexture->getSize();
            mmSprintf(hTextureResolutionString, "%ux%u", (mmUInt32_t)hTextureSize.d_width, (mmUInt32_t)hTextureSize.d_height);

            this->StaticTextResolution->setText(hTextureResolutionString);

            this->StaticImageScreen->setVisible(true);
            this->StaticImageEmbellish->setVisible(false);
        }
        else
        {
            this->StaticImageScreen->setProperty("Image", this->hImageryIcon);

            this->StaticTextResolution->setText("???x???");

            this->StaticImageScreen->setVisible(false);
            this->StaticImageEmbellish->setVisible(true);
        }
    }
    bool mmLayerImageryView::OnEventWindowSizeChanged(const mmEventArgs& args)
    {
        this->UpdateImageAttributes();
        return true;
    }
    bool mmLayerImageryView::OnStaticTextFpsEventMouseClick(const CEGUI::EventArgs& args)
    {
        mmLayerDirector_SceneExclusiveForeground(this->pDirector, "mm/mmLayerCommonTextureManager", "mmLayerCommonTextureManager");
        return true;
    }
    bool mmLayerImageryView::OnWidgetImageButtonBackEventMouseClick(const CEGUI::EventArgs& args)
    {
        mmLayerDirector_SceneExclusiveEnterBackground(this->pDirector, this->getName().c_str());
        return true;
    }
}
