/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerImageryWindow.h"

#include "CEGUI/WindowManager.h"
#include "CEGUI/ImageManager.h"
#include "CEGUI/Texture.h"

#include "OgreTextureManager.h"

#include "CEGUIOgreRenderer/Texture.h"

#include "layer/director/mmLayerDirector.h"

#include "explorer/mmExplorerItem.h"

namespace mm
{
    const CEGUI::String mmLayerImageryWindow::EventNamespace = "mm";
    const CEGUI::String mmLayerImageryWindow::WidgetTypeName = "mm/mmLayerImageryWindow";

    const std::string mmLayerImageryWindow::CEGUI_RTT_IMAGERY = "mmLayerImageryWindow_CEGUIRTTImagery_";
    const std::string mmLayerImageryWindow::CEGUI_RTT_TEXTURE = "mmLayerImageryWindow_CEGUIRTTTexture_";

    mmLayerImageryWindow::mmLayerImageryWindow(const CEGUI::String& type, const CEGUI::String& name)
        : mmLayerContext(type, name)

        , pView(NULL)

        , hOgreTexture()
        , pImage(NULL)
        , pCEGUIOgreTexture(NULL)
        , hImageryIcon("")
        , hImageryName("")
        , hTextureName("")
    {

    }
    mmLayerImageryWindow::~mmLayerImageryWindow(void)
    {
        
    }

    void mmLayerImageryWindow::OnFinishLaunching(void)
    {
        struct mmContextMaster* pContextMaster = this->pContextMaster;
        struct mmSurfaceMaster* pSurfaceMaster = this->pSurfaceMaster;

        pSurfaceMaster->hEventSet.SubscribeEvent(mmSurfaceMaster_EventWindowSizeChanged, &mmLayerImageryWindow::OnEventWindowSizeChanged, this);

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        this->pView = pWindowManager->createWindow("TaharezLook/StaticImage");
        this->addChild(this->pView);

        this->pView->setProperty("HorzFormatting", "CentreAligned");
        this->pView->setProperty("VertFormatting", "CentreAligned");

        this->hImageryIcon = this->pView->getProperty("Image");
        this->hImageryName = CEGUI_RTT_IMAGERY + this->getName();
        this->hTextureName = CEGUI_RTT_TEXTURE + this->getName();
        //
        struct mmCEGUISystem* pCEGUISystem = &pContextMaster->hCEGUISystem;
        CEGUI::CEGUIOgreRenderer* pCEGUIOgreRenderer = pCEGUISystem->pRenderer;
        //
        this->pCEGUIOgreTexture = (CEGUI::CEGUIOgreTexture*)&pCEGUIOgreRenderer->createTexture(this->hTextureName);
        //
        CEGUI::ImageManager* pImageManager = CEGUI::ImageManager::getSingletonPtr();
        this->pImage = static_cast<CEGUI::BasicImage*>(&pImageManager->create("BasicImage", this->hImageryName));
        //
        struct mmAdaptiveSize* pAdaptiveSize = &pSurfaceMaster->hAdaptiveSize;
        Ogre::uint hPixelW = (Ogre::uint)(pAdaptiveSize->hActuallySafeRect[2]);
        Ogre::uint hPixelH = (Ogre::uint)(pAdaptiveSize->hActuallySafeRect[3]);

        this->pImage->setNativeResolution(CEGUI::Sizef((float)hPixelW, (float)hPixelH));
        this->pImage->setAutoScaled(CEGUI::ASM_Min);
    }

    void mmLayerImageryWindow::OnBeforeTerminate(void)
    {
        struct mmSurfaceMaster* pSurfaceMaster = this->pSurfaceMaster;

        this->RecyclePreviewTexture();

        CEGUI::ImageManager* pImageManager = CEGUI::ImageManager::getSingletonPtr();
        pImageManager->destroy(*this->pImage);
        this->pImage = NULL;
        //
        struct mmCEGUISystem* pCEGUISystem = &pContextMaster->hCEGUISystem;
        CEGUI::CEGUIOgreRenderer* pCEGUIOgreRenderer = pCEGUISystem->pRenderer;
        //
        pCEGUIOgreRenderer->destroyTexture(*this->pCEGUIOgreTexture);
        this->pCEGUIOgreTexture = NULL;

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        pWindowManager->destroyWindow(this->pView);
        this->pView = NULL;

        pSurfaceMaster->hEventSet.UnsubscribeEvent(mmSurfaceMaster_EventWindowSizeChanged, &mmLayerImageryWindow::OnEventWindowSizeChanged, this);
    }
    void mmLayerImageryWindow::SetOgreTexture(Ogre::TexturePtr hOgreTexture)
    {
        this->hOgreTexture = hOgreTexture;
    }
    void mmLayerImageryWindow::UpdateLayerValue(void)
    {
        this->RefreshPreviewTexture();
    }
    void mmLayerImageryWindow::UpdateImageAttributes(void)
    {
        struct mmAdaptiveSize* pAdaptiveSize = &pSurfaceMaster->hAdaptiveSize;
        Ogre::uint hPixelW = (Ogre::uint)(pAdaptiveSize->hActuallySafeRect[2]);
        Ogre::uint hPixelH = (Ogre::uint)(pAdaptiveSize->hActuallySafeRect[3]);

        const CEGUI::Rectf& _rectf = this->pView->getOuterRectClipper();
        float _win_w = _rectf.getWidth();
        float _win_h = _rectf.getHeight();
        _win_w = _win_w < 1 ? 1 : _win_w;
        _win_h = _win_h < 1 ? 1 : _win_h;

        const CEGUI::Sizef& _tex_size = this->pCEGUIOgreTexture->getSize();

        float _tex_real_w = (float)_tex_size.d_width;
        float _tex_real_h = (float)_tex_size.d_height;

        float _native_resolution_w = _tex_real_w * (float)hPixelW / _win_w;
        float _native_resolution_h = _tex_real_h * (float)hPixelH / _win_h;

        CEGUI::Rectf _imageArea(0, 0, _tex_real_w, _tex_real_h);
        CEGUI::Sizef _imageNativeResolution(_native_resolution_w, _native_resolution_h);
        this->pImage->setArea(_imageArea);
        this->pImage->setNativeResolution(_imageNativeResolution);
        this->pImage->setAutoScaled(CEGUI::ASM_Min);
    }
    void mmLayerImageryWindow::ProducePreviewTexture(void)
    {
        Ogre::TextureManager* TextureManager = Ogre::TextureManager::getSingletonPtr();

        assert(!this->hOgreTexture && "you must destroy ogre texture before load the new one.");

        Ogre::uint32 _w = this->hOgreTexture->getWidth();
        Ogre::uint32 _h = this->hOgreTexture->getHeight();

        CEGUI::Rectf _texture_area(0, 0, (float)_w, (float)_h);

        // weak reference mode.
        this->pCEGUIOgreTexture->setOgreTexture(this->hOgreTexture, _texture_area);

        this->pImage->setTexture(this->pCEGUIOgreTexture);

        this->UpdateImageAttributes();
    }
    void mmLayerImageryWindow::RecyclePreviewTexture(void)
    {
        if (this->hOgreTexture)
        {
            Ogre::TextureManager* pTextureManager = Ogre::TextureManager::getSingletonPtr();

            pTextureManager->remove(this->hOgreTexture);
            this->hOgreTexture.reset();

            CEGUI::Rectf _texture_area(0, 0, 0, 0);
            this->pCEGUIOgreTexture->setOgreTexture(this->hOgreTexture, _texture_area);
        }
    }
    void mmLayerImageryWindow::RefreshPreviewTexture(void)
    {
        if (this->hOgreTexture)
        {
            this->pView->setProperty("Image", this->hImageryName);

            this->RecyclePreviewTexture();
            this->ProducePreviewTexture();
        }
        else
        {
            this->pView->setProperty("Image", this->hImageryIcon);
        }
    }
    bool mmLayerImageryWindow::OnEventWindowSizeChanged(const mmEventArgs& args)
    {
        this->UpdateImageAttributes();
        return true;
    }
}
