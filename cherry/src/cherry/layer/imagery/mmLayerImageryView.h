/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmLayerImageryView_h__
#define __mmLayerImageryView_h__

#include "core/mmCore.h"

#include "dish/mmEvent.h"

#include "layer/director/mmLayerContext.h"

#include "CEGUI/Window.h"
#include "CEGUI/BasicImage.h"

#include "layer/utility/mmLayerUtilityFps.h"

struct mmExplorerItem;

namespace mm
{
    class mmLayerImageryView : public mmLayerContext
    {
    public:
        static const CEGUI::String EventNamespace;          //!< Namespace for global events
        static const CEGUI::String WidgetTypeName;          //!< Window factory name
    public:
        static const std::string CEGUI_RTT_IMAGERY;
        static const std::string CEGUI_RTT_TEXTURE;
    public:
        CEGUI::Window* LayerImageryView;
        CEGUI::Window* StaticImageBackground;

        CEGUI::Window* StaticTextFps;
        CEGUI::Window* WidgetImageButtonBack;

        CEGUI::Window* StaticImageScreen;
        CEGUI::Window* StaticImageEmbellish;

        CEGUI::Window* StaticTextResolution;
        CEGUI::Window* StaticTextSize;
    public:
        // weak ref.
        struct mmExplorerItem* pItem;
    public:
        mmLayerUtilityFps hUtilityFps;
    public:
        Ogre::TexturePtr hOgreTexture;
        // strong ref.
        CEGUI::BasicImage* pImage;
        // strong ref.
        CEGUI::CEGUIOgreTexture* pCEGUIOgreTexture;
        CEGUI::String hImageryIcon;
        CEGUI::String hImageryName;
        CEGUI::String hTextureName;
    public:
        // default is 64m
        size_t hMaxImageryFileSize;
    public:
        CEGUI::Event::Connection hFpsEventMouseClickConn;
        CEGUI::Event::Connection hBackEventMouseClickConn;
    public:
        mmLayerImageryView(const CEGUI::String& type, const CEGUI::String& name);
        virtual ~mmLayerImageryView(void);
    public:
        // mmLayerContext
        virtual void OnFinishLaunching(void);
        virtual void OnBeforeTerminate(void);
    public:
        void SetItem(struct mmExplorerItem* pItem);
    public:
        void UpdateLayerValue(void);
    private:
        void UpdateImageAttributes(void);
    private:
        void ProducePreviewTexture(void);
        void RecyclePreviewTexture(void);

        void RefreshPreviewTexture(void);
    private:
        bool OnEventWindowSizeChanged(const mmEventArgs& args);
    private:
        bool OnStaticTextFpsEventMouseClick(const CEGUI::EventArgs& args);
        bool OnWidgetImageButtonBackEventMouseClick(const CEGUI::EventArgs& args);
    };
}

#endif//__mmLayerImageryView_h__