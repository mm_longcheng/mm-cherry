/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerCommonTextureManager.h"

#include "CEGUI/WindowManager.h"
#include "CEGUI/widgets/Combobox.h"
#include "CEGUI/ImageManager.h"
#include "CEGUI/BasicImage.h"
#include "CEGUI/Texture.h"

#include "layer/director/mmLayerDirector.h"

#include "CEGUIOgreRenderer/Texture.h"
#include "CEGUIOgreRenderer/TextureManager.h"

#include "OgreTextureManager.h"

namespace mm
{
    mmLayerCommonTextureSeriesItem::mmLayerCommonTextureSeriesItem()
        : hTextItem("0", 0, NULL, false, false)
        , hAttribute()
    {

    }
    mmLayerCommonTextureSeriesItem::~mmLayerCommonTextureSeriesItem()
    {

    }
    void mmLayerCommonTextureSeriesItem::SetAttribute(CEGUI::CEGUIOgreTextureSeriesAttribute* pAttribute)
    {
        this->hAttribute = *pAttribute;
    }
    void mmLayerCommonTextureSeriesItem::SetItemId(CEGUI::uint hItemId)
    {
        this->hTextItem.setID(hItemId);
    }
    void mmLayerCommonTextureSeriesItem::OnFinishLaunching()
    {

    }
    void mmLayerCommonTextureSeriesItem::OnBeforeTerminate()
    {

    }

    void mmVectorValue_TextureSeriesItemEventProduce(void* obj, void* u)
    {
        mmLayerCommonTextureSeriesItem* e = (mmLayerCommonTextureSeriesItem*)(u);
        new (e) mmLayerCommonTextureSeriesItem();
    }
    void mmVectorValue_TextureSeriesItemEventRecycle(void* obj, void* u)
    {
        mmLayerCommonTextureSeriesItem* e = (mmLayerCommonTextureSeriesItem*)(u);
        e->~mmLayerCommonTextureSeriesItem();
    }

    mmLayerCommonTexturePageItem::mmLayerCommonTexturePageItem()
        : hTextItem("0", 0, NULL, false, false)
        , hPageId(0)
        , hTextureName("")
        , hTexturePage(NULL)
    {

    }
    mmLayerCommonTexturePageItem::~mmLayerCommonTexturePageItem()
    {

    }
    void mmLayerCommonTexturePageItem::SetPageId(Ogre::uint32 hPageId)
    {
        this->hPageId = hPageId;
    }
    void mmLayerCommonTexturePageItem::SetItemId(CEGUI::uint hItemId)
    {
        this->hTextItem.setID(hItemId);
    }
    void mmLayerCommonTexturePageItem::SetTextureName(const Ogre::String& hTextureName)
    {
        this->hTextureName = hTextureName;
    }
    void mmLayerCommonTexturePageItem::SetTexturePage(CEGUI::CEGUIOgreTexturePage* pTexturePage)
    {
        this->hTexturePage = pTexturePage;
    }
    void mmLayerCommonTexturePageItem::OnFinishLaunching()
    {

    }
    void mmLayerCommonTexturePageItem::OnBeforeTerminate()
    {

    }

    void mmVectorValue_TexturePageItemEventProduce(void* obj, void* u)
    {
        mmLayerCommonTexturePageItem* e = (mmLayerCommonTexturePageItem*)(u);
        new (e) mmLayerCommonTexturePageItem();
    }
    void mmVectorValue_TexturePageItemEventRecycle(void* obj, void* u)
    {
        mmLayerCommonTexturePageItem* e = (mmLayerCommonTexturePageItem*)(u);
        e->~mmLayerCommonTexturePageItem();
    }

    const CEGUI::String mmLayerCommonTextureManager::EventNamespace = "mm";
    const CEGUI::String mmLayerCommonTextureManager::WidgetTypeName = "mm/mmLayerCommonTextureManager";

    mmLayerCommonTextureManager::mmLayerCommonTextureManager(const CEGUI::String& type, const CEGUI::String& name)
        : mmLayerContext(type, name)
        , LayerCommonTextureManager(NULL)
        , StaticImageBackground(NULL)

        , DefaultWindowSeries(NULL)
        , ComboboxSeries(NULL)
        , StaticTextSeries(NULL)

        , DefaultWindowPage(NULL)
        , ComboboxPage(NULL)
        , StaticTextPage(NULL)

        , WidgetImageButtonBack(NULL)
        , StaticTextFps(NULL)

        , StaticImageTexture(NULL)

        , pWidgetComboboxSeries(NULL)
        , pWidgetComboboxPage(NULL)

        , hImageOName("")
        , hImageNName("")

        , pImage(NULL)
        , pCEGUIOgreTexture(NULL)
        , hOgreTexture()

        , hCurrentSeriesIndex(0)
        , hCurrentPageIndex(0)
    {
        struct mmVectorValueEventAllocator hEventAllocatorSeries;
        struct mmVectorValueEventAllocator hEventAllocatorPage;

        mmVectorValue_Init(&this->hItemSeriesVector);
        mmVectorValue_Init(&this->hItemPageVector);

        hEventAllocatorSeries.Produce = &mmVectorValue_TextureSeriesItemEventProduce;
        hEventAllocatorSeries.Recycle = &mmVectorValue_TextureSeriesItemEventRecycle;
        hEventAllocatorSeries.obj = this;
        mmVectorValue_SetElemSize(&this->hItemSeriesVector, sizeof(mmLayerCommonTextureSeriesItem));
        mmVectorValue_SetEventAllocator(&this->hItemSeriesVector, &hEventAllocatorSeries);

        hEventAllocatorPage.Produce = &mmVectorValue_TexturePageItemEventProduce;
        hEventAllocatorPage.Recycle = &mmVectorValue_TexturePageItemEventRecycle;
        hEventAllocatorPage.obj = this;
        mmVectorValue_SetElemSize(&this->hItemPageVector, sizeof(mmLayerCommonTexturePageItem));
        mmVectorValue_SetEventAllocator(&this->hItemPageVector, &hEventAllocatorPage);
    }
    mmLayerCommonTextureManager::~mmLayerCommonTextureManager()
    {
        mmVectorValue_Destroy(&this->hItemSeriesVector);
        mmVectorValue_Destroy(&this->hItemPageVector);
    }

    void mmLayerCommonTextureManager::OnFinishLaunching()
    {
        struct mmContextMaster* pContextMaster = this->pContextMaster;
        struct mmSurfaceMaster* pSurfaceMaster = this->pSurfaceMaster;

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        this->LayerCommonTextureManager = pWindowManager->loadLayoutFromFile("common/LayerCommonTextureManager.layout");
        this->addChild(this->LayerCommonTextureManager);

        this->StaticImageBackground = this->LayerCommonTextureManager->getChild("StaticImageBackground");

        this->DefaultWindowSeries = this->StaticImageBackground->getChild("DefaultWindowSeries");
        this->ComboboxSeries = this->DefaultWindowSeries->getChild("ComboboxSeries");
        this->StaticTextSeries = this->DefaultWindowSeries->getChild("StaticTextSeries");

        this->DefaultWindowPage = this->StaticImageBackground->getChild("DefaultWindowPage");
        this->ComboboxPage = this->DefaultWindowPage->getChild("ComboboxPage");
        this->StaticTextPage = this->DefaultWindowPage->getChild("StaticTextPage");

        this->WidgetImageButtonBack = this->StaticImageBackground->getChild("WidgetImageButtonBack");
        this->StaticTextFps = this->StaticImageBackground->getChild("StaticTextFps");

        this->StaticImageTexture = this->StaticImageBackground->getChild("StaticImageTexture");
        //
        this->pWidgetComboboxSeries = (CEGUI::Combobox*)(this->ComboboxSeries);
        this->pWidgetComboboxPage = (CEGUI::Combobox*)(this->ComboboxPage);

        struct mmFrameScheduler* pFrameScheduler = &pSurfaceMaster->hFrameScheduler;
        struct mmFrameTimer* pFrameTimer = &pFrameScheduler->frame_timer;
        this->hUtilityFps.SetFrameTimer(pFrameTimer);
        this->hUtilityFps.SetWindow(this->StaticTextFps);
        this->hUtilityFps.OnFinishLaunching();

        this->hImageOName = this->StaticImageTexture->getProperty("Image");
        this->hImageNName = this->getName() + "_manager_imagery__";

        CEGUI::ImageManager* pImageManager = CEGUI::ImageManager::getSingletonPtr();
        struct mmCEGUISystem* pCEGUISystem = &this->pContextMaster->hCEGUISystem;
        CEGUI::CEGUIOgreRenderer* pCEGUIOgreRenderer = pCEGUISystem->pRenderer;

        this->pImage = static_cast<CEGUI::BasicImage*>(&pImageManager->create("BasicImage", this->hImageNName));
        this->pImage->setAutoScaled(CEGUI::ASM_Disabled);

        CEGUI::Texture* _texture = &pCEGUIOgreRenderer->createTexture(this->getName() + "_manager_texture__");
        this->pCEGUIOgreTexture = (CEGUI::CEGUIOgreTexture*)(_texture);
        this->pImage->setTexture(_texture);

        this->hLayerEventShownConn = this->subscribeEvent(CEGUI::Window::EventShown, CEGUI::Event::Subscriber(&mmLayerCommonTextureManager::OnLayerEventShown, this));
        this->hLayerEventHiddenConn = this->subscribeEvent(CEGUI::Window::EventHidden, CEGUI::Event::Subscriber(&mmLayerCommonTextureManager::OnLayerEventHidden, this));
        this->hTextureEventMouseClickConn = this->StaticImageTexture->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerCommonTextureManager::OnTextureEventMouseClick, this));
        this->hTextureEventUpdatedConn = this->StaticImageTexture->subscribeEvent(CEGUI::Window::EventUpdated, CEGUI::Event::Subscriber(&mmLayerCommonTextureManager::OnTextureEventUpdated, this));
        //
        this->hBackEventMouseClickConn = this->WidgetImageButtonBack->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerCommonTextureManager::OnWidgetImageButtonBackEventMouseClick, this));
        //
        this->pWidgetComboboxSeries->subscribeEvent(CEGUI::Combobox::EventListSelectionAccepted, CEGUI::Event::Subscriber(&mmLayerCommonTextureManager::OnWidgetComboboxSeriesEventListSelectionAccepted, this));
        this->pWidgetComboboxPage->subscribeEvent(CEGUI::Combobox::EventListSelectionAccepted, CEGUI::Event::Subscriber(&mmLayerCommonTextureManager::OnWidgetComboboxPageEventListSelectionAccepted, this));

        // we update once at OnFinishLaunching.
        this->UpdateLayerView();
    }

    void mmLayerCommonTextureManager::OnBeforeTerminate()
    {
        this->DeleteLayerView();

        this->hLayerEventShownConn->disconnect();
        this->hLayerEventHiddenConn->disconnect();
        this->hTextureEventMouseClickConn->disconnect();
        this->hTextureEventUpdatedConn->disconnect();
        //
        this->hBackEventMouseClickConn->disconnect();

        CEGUI::ImageManager* pImageManager = CEGUI::ImageManager::getSingletonPtr();
        struct mmCEGUISystem* pCEGUISystem = &this->pContextMaster->hCEGUISystem;
        CEGUI::CEGUIOgreRenderer* pCEGUIOgreRenderer = pCEGUISystem->pRenderer;
        // when we destroy the cegui ogre texture, we must reset the ogre texture null.
        // because we use the weak reference mode.
        this->hOgreTexture.reset();
        CEGUI::Rectf _texture_area(0, 0, 0, 0);
        this->pCEGUIOgreTexture->setOgreTexture(this->hOgreTexture, _texture_area);
        //
        pCEGUIOgreRenderer->destroyTexture(*this->pCEGUIOgreTexture);
        this->pCEGUIOgreTexture = NULL;
        pImageManager->destroy(*this->pImage);
        this->pImage = NULL;

        this->hUtilityFps.OnBeforeTerminate();

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        pWindowManager->destroyWindow(this->LayerCommonTextureManager);
        this->LayerCommonTextureManager = NULL;
    }
    void mmLayerCommonTextureManager::UpdateLayerView()
    {
        this->UpdateComboboxSeriesItem();
    }
    void mmLayerCommonTextureManager::DeleteLayerView()
    {
        this->StaticImageTexture->setProperty("Image", this->hImageOName);
        this->DeleteComboboxPageItem();
        this->DeleteComboboxSeriesItem();
    }
    void mmLayerCommonTextureManager::ShowSeries(size_t _series_index)
    {
        struct mmCEGUISystem* pCEGUISystem = &this->pContextMaster->hCEGUISystem;
        CEGUI::CEGUIOgreRenderer* pCEGUIOgreRenderer = pCEGUISystem->pRenderer;
        CEGUI::CEGUIOgreTextureManager* pCEGUIOgreTextureManager = pCEGUIOgreRenderer->getOgreTextureManager();
        //
        CEGUI::CEGUIOgreTextureSeries* _texture_series = NULL;
        //
        mmLayerCommonTextureSeriesItem* _item = NULL;
        _item = (mmLayerCommonTextureSeriesItem*)mmVectorValue_GetIndexReference(&this->hItemSeriesVector, _series_index);
        //
        _texture_series = pCEGUIOgreTextureManager->getSeries(&_item->hAttribute);
        if (NULL != _texture_series)
        {
            // logger information.
            this->LoggerSeriesInformation(_texture_series);
            //
            this->UpdateComboboxPageItem(_texture_series);
        }
        else
        {
            // current item is invalid, we update the view.
            this->UpdateComboboxSeriesItem();
        }
    }
    void mmLayerCommonTextureManager::ShowPage(size_t _page_index)
    {
        Ogre::TextureManager* _TextureManager = Ogre::TextureManager::getSingletonPtr();
        //
        mmLayerCommonTexturePageItem* _item = NULL;
        _item = (mmLayerCommonTexturePageItem*)mmVectorValue_GetIndexReference(&this->hItemPageVector, _page_index);
        // we get the shared reference ogre texture.
        this->hOgreTexture = _TextureManager->getByName(_item->hTextureName);
        //
        if (this->hOgreTexture)
        {
            // ok, the textrue page is valid.
            // logger information.
            this->LoggerPageInformation(_item->hTexturePage);

            Ogre::uint32 _w = this->hOgreTexture->getWidth();
            Ogre::uint32 _h = this->hOgreTexture->getHeight();

            CEGUI::Rectf _texture_area(0, 0, (float)_w, (float)_h);

            // weak reference mode.
            this->pCEGUIOgreTexture->setOgreTexture(this->hOgreTexture, _texture_area);

            this->pImage->setArea(_texture_area);

            this->StaticImageTexture->setProperty("Image", this->hImageNName);
        }
        else
        {
            this->StaticImageTexture->setProperty("Image", this->hImageOName);
        }
    }
    void mmLayerCommonTextureManager::CreateComboboxSeriesItem(void)
    {
        typedef CEGUI::CEGUIOgreTextureManager::TextureSeriesMapType map_type;

        struct mmCEGUISystem* pCEGUISystem = &this->pContextMaster->hCEGUISystem;
        CEGUI::CEGUIOgreRenderer* pCEGUIOgreRenderer = pCEGUISystem->pRenderer;
        CEGUI::CEGUIOgreTextureManager* pCEGUIOgreTextureManager = pCEGUIOgreRenderer->getOgreTextureManager();

        size_t index = 0;
        mmLayerCommonTextureSeriesItem* _item = NULL;
        CEGUI::ListboxTextItem* _item_text = NULL;

        CEGUI::CEGUIOgreTextureSeries* _texture_series = NULL;

        CEGUI::String _series_name;

        map_type::iterator it;

        map_type& _series_map = pCEGUIOgreTextureManager->d_texture_series_map;

        mmVectorValue_Resize(&this->hItemSeriesVector, _series_map.size());

        it = _series_map.begin();
        while (it != _series_map.end())
        {
            _texture_series = it->second;

            this->GenerateSeriesName(_texture_series, &_series_name);
            _item = (mmLayerCommonTextureSeriesItem*)mmVectorValue_GetIndexReference(&this->hItemSeriesVector, index);
            _item->SetAttribute(&_texture_series->d_attribute);
            _item->SetItemId(index);
            _item_text = &_item->hTextItem;
            _item_text->setText(_series_name);
            _item_text->setSelectionBrushImage("TaharezLook/TextSelectionBrush");
            _item->OnFinishLaunching();
            this->pWidgetComboboxSeries->addItem(_item_text);

            index++;

            it++;
        }
    }
    void mmLayerCommonTextureManager::DeleteComboboxSeriesItem()
    {
        size_t _size = this->hItemSeriesVector.size;

        size_t index = 0;

        mmLayerCommonTextureSeriesItem* _item = NULL;
        CEGUI::ListboxTextItem* _text_item;

        while (index < _size)
        {
            _item = (mmLayerCommonTextureSeriesItem*)mmVectorValue_GetIndexReference(&this->hItemSeriesVector, index);
            _text_item = &_item->hTextItem;
            this->pWidgetComboboxSeries->removeItem(_text_item);
            _item->OnBeforeTerminate();
            index++;
        }

        mmVectorValue_Clear(&this->hItemSeriesVector);
    }
    void mmLayerCommonTextureManager::UpdateComboboxSeriesItem()
    {
        this->DeleteComboboxSeriesItem();
        this->CreateComboboxSeriesItem();
        //
        if (0 == this->hItemSeriesVector.size)
        {
            // use default.
            this->hCurrentSeriesIndex = 0;
            this->StaticTextSeries->setText("");
        }
        else
        {
            // choose the current one.
            // choose the first one.
            this->hCurrentSeriesIndex = this->hCurrentSeriesIndex < this->hItemSeriesVector.size ? this->hCurrentSeriesIndex : 0;
            //
            this->ShowSeries(this->hCurrentSeriesIndex);
        }
        //
        this->pWidgetComboboxSeries->setItemSelectState(this->hCurrentSeriesIndex, false);
        this->pWidgetComboboxSeries->setItemSelectState(this->hCurrentSeriesIndex, true);
    }
    void mmLayerCommonTextureManager::CreateComboboxPageItem(CEGUI::CEGUIOgreTextureSeries* _texture_series)
    {
        typedef CEGUI::CEGUIOgreTextureSeries::page_map_type page_map_type;

        size_t index = 0;
        mmLayerCommonTexturePageItem* _item = NULL;
        CEGUI::ListboxTextItem* _item_text = NULL;

        CEGUI::CEGUIOgreTexturePage* _texture_page = NULL;

        CEGUI::String _page_name;

        page_map_type::iterator it;

        page_map_type& _page_pool = _texture_series->d_page_pool;

        mmVectorValue_Resize(&this->hItemPageVector, _page_pool.size());

        it = _page_pool.begin();
        while (it != _page_pool.end())
        {
            _texture_page = it->second;

            this->GeneratePageName(_texture_page, &_page_name);
            _item = (mmLayerCommonTexturePageItem*)mmVectorValue_GetIndexReference(&this->hItemPageVector, index);
            _item->SetPageId(_texture_page->d_page_id);
            _item->SetItemId(index);
            _item->SetTextureName(_texture_page->d_name);
            _item->SetTexturePage(_texture_page);
            _item_text = &_item->hTextItem;
            _item_text->setText(_page_name);
            _item_text->setSelectionBrushImage("TaharezLook/TextSelectionBrush");
            _item->OnFinishLaunching();
            this->pWidgetComboboxPage->addItem(_item_text);

            index++;

            it++;
        }
    }
    void mmLayerCommonTextureManager::DeleteComboboxPageItem()
    {
        size_t _size = this->hItemPageVector.size;

        size_t index = 0;

        mmLayerCommonTexturePageItem* _item = NULL;
        CEGUI::ListboxTextItem* _text_item;

        while (index < _size)
        {
            _item = (mmLayerCommonTexturePageItem*)mmVectorValue_GetIndexReference(&this->hItemPageVector, index);
            _text_item = &_item->hTextItem;
            this->pWidgetComboboxPage->removeItem(_text_item);
            _item->OnBeforeTerminate();
            index++;
        }

        mmVectorValue_Clear(&this->hItemPageVector);
    }
    void mmLayerCommonTextureManager::UpdateComboboxPageItem(CEGUI::CEGUIOgreTextureSeries* _texture_series)
    {
        this->DeleteComboboxPageItem();
        this->CreateComboboxPageItem(_texture_series);
        //
        if (0 == this->hItemPageVector.size)
        {
            // use default.
            this->hCurrentPageIndex = 0;
            this->StaticTextPage->setText("");
        }
        else
        {
            // choose the current one.
            // choose the first one.
            this->hCurrentPageIndex = this->hCurrentPageIndex < this->hItemPageVector.size ? this->hCurrentPageIndex : 0;
            //
            this->ShowPage(this->hCurrentPageIndex);
        }
        //
        this->pWidgetComboboxPage->setItemSelectState(this->hCurrentPageIndex, false);
        this->pWidgetComboboxPage->setItemSelectState(this->hCurrentPageIndex, true);
    }
    void mmLayerCommonTextureManager::GenerateSeriesName(CEGUI::CEGUIOgreTextureSeries* _texture_series, CEGUI::String* _series_name)
    {
        CEGUI::CEGUIOgreTextureSeriesAttribute* _attribute = &_texture_series->d_attribute;

        Ogre::uint _w = _texture_series->d_w;
        Ogre::uint _h = _texture_series->d_h;
        Ogre::uint _d = _attribute->d_depth;
        int _m = _attribute->d_numMipmaps;
        Ogre::uint _u = (Ogre::uint)_attribute->d_usage;

        const Ogre::String& _formatName = Ogre::PixelUtil::getFormatName(_attribute->d_format);

        char _attribute_string[128] = { 0 };
        sprintf(_attribute_string, "(%s,%ux%ux%u,%d %u)", _formatName.c_str(), _w, _h, _d, _m, _u);

        _series_name->assign(_attribute_string);
    }
    void mmLayerCommonTextureManager::GeneratePageName(CEGUI::CEGUIOgreTexturePage* _texture_page, CEGUI::String* _page_name)
    {
        _page_name->assign(_texture_page->d_name.c_str());
    }
    void mmLayerCommonTextureManager::LoggerSeriesInformation(CEGUI::CEGUIOgreTextureSeries* _texture_series)
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        struct mmString _info;
        mmString_Init(&_info);

        mmString_Sprintf(&_info, "d_w              : %u" "\n", _texture_series->d_w);
        mmString_Sprintf(&_info, "d_h              : %u" "\n", _texture_series->d_h);
        mmString_Sprintf(&_info, "d_page_pool.size : %" PRIuPTR "\n", _texture_series->d_page_pool.size());
        mmString_Sprintf(&_info, "d_pixel_used     : %" PRIu64 "\n", (mmUInt64_t)_texture_series->d_pixel_used);
        mmString_Sprintf(&_info, "d_pixel_idle     : %" PRIu64 "\n", (mmUInt64_t)_texture_series->d_pixel_idle);
        mmString_Sprintf(&_info, "d_page_counter   : %" PRIu32 "\n", (mmUInt32_t)_texture_series->d_page_counter);

        this->StaticTextSeries->setText(mmString_CStr(&_info));

        mmString_Destroy(&_info);
    }
    void mmLayerCommonTextureManager::LoggerPageInformation(CEGUI::CEGUIOgreTexturePage* _texture_page)
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        struct mmString _info;
        mmString_Init(&_info);

        mmString_Sprintf(&_info, "d_PageId         : %u" "\n", _texture_page->d_page_id);
        mmString_Sprintf(&_info, "d_w              : %u" "\n", _texture_page->d_w);
        mmString_Sprintf(&_info, "d_h              : %u" "\n", _texture_page->d_h);
        mmString_Sprintf(&_info, "d_max_w          : %u" "\n", _texture_page->d_max_w);
        mmString_Sprintf(&_info, "d_max_h          : %u" "\n", _texture_page->d_max_h);
        mmString_Sprintf(&_info, "d_area_pool.size : %" PRIuPTR "\n", _texture_page->d_area_pool.size());
        mmString_Sprintf(&_info, "d_area_used.size : %" PRIuPTR "\n", _texture_page->d_area_used.size());
        mmString_Sprintf(&_info, "d_area_idle.size : %" PRIuPTR "\n", _texture_page->d_area_idle.size());
        mmString_Sprintf(&_info, "d_area_hide.size : %" PRIuPTR "\n", _texture_page->d_area_hide.size());
        mmString_Sprintf(&_info, "d_pixel_used     : %" PRIu64 "\n", (mmUInt64_t)_texture_page->d_pixel_used);
        mmString_Sprintf(&_info, "d_pixel_idle     : %" PRIu64 "\n", (mmUInt64_t)_texture_page->d_pixel_idle);

        this->StaticTextPage->setText(mmString_CStr(&_info));

        mmString_Destroy(&_info);
    }
    bool mmLayerCommonTextureManager::OnWidgetComboboxSeriesEventListSelectionAccepted(const CEGUI::EventArgs& args)
    {
        const CEGUI::WindowEventArgs& evt = (const CEGUI::WindowEventArgs&)(args);
        CEGUI::Combobox* _combobox = (CEGUI::Combobox*)(evt.window);

        do
        {
            size_t index = 0;

            CEGUI::ListboxItem* _select_item = _combobox->getSelectedItem();
            if (NULL == _select_item)
            {
                break;
            }
            index = (size_t)_select_item->getID();

            this->hCurrentSeriesIndex = index;

            this->ShowSeries(index);
        } while (0);

        return true;
    }
    bool mmLayerCommonTextureManager::OnWidgetComboboxPageEventListSelectionAccepted(const CEGUI::EventArgs& args)
    {
        const CEGUI::WindowEventArgs& evt = (const CEGUI::WindowEventArgs&)(args);
        CEGUI::Combobox* _combobox = (CEGUI::Combobox*)(evt.window);

        do
        {
            size_t index = 0;

            CEGUI::ListboxItem* _select_item = _combobox->getSelectedItem();
            if (NULL == _select_item)
            {
                break;
            }
            index = (size_t)_select_item->getID();

            this->hCurrentPageIndex = index;

            this->ShowPage(index);
        } while (0);

        return true;
    }
    bool mmLayerCommonTextureManager::OnLayerEventShown(const CEGUI::EventArgs& args)
    {
        this->UpdateLayerView();
        return true;
    }
    bool mmLayerCommonTextureManager::OnLayerEventHidden(const CEGUI::EventArgs& args)
    {
        this->DeleteLayerView();
        return true;
    }
    bool mmLayerCommonTextureManager::OnTextureEventMouseClick(const CEGUI::EventArgs& args)
    {
        this->hCurrentPageIndex++;

        size_t _index_x = this->hItemSeriesVector.size;
        size_t _index_y = this->hItemPageVector.size;

        if (this->hCurrentPageIndex < this->hItemPageVector.size)
        {
            this->ShowPage(this->hCurrentPageIndex);

            this->pWidgetComboboxPage->setItemSelectState(this->hCurrentPageIndex, false);
            this->pWidgetComboboxPage->setItemSelectState(this->hCurrentPageIndex, true);
        }
        else
        {
            this->hCurrentPageIndex = 0;

            this->hCurrentSeriesIndex++;

            if (this->hCurrentSeriesIndex < _index_x)
            {
                this->ShowSeries(this->hCurrentSeriesIndex);
            }
            else
            {
                this->hCurrentSeriesIndex = 0;

                this->ShowSeries(this->hCurrentSeriesIndex);
            }

            this->pWidgetComboboxSeries->setItemSelectState(this->hCurrentSeriesIndex, false);
            this->pWidgetComboboxSeries->setItemSelectState(this->hCurrentSeriesIndex, true);
        }

        return true;
    }
    bool mmLayerCommonTextureManager::OnTextureEventUpdated(const CEGUI::EventArgs& args)
    {
        this->StaticImageTexture->invalidate();
        return true;
    }
    bool mmLayerCommonTextureManager::OnWidgetImageButtonBackEventMouseClick(const CEGUI::EventArgs& args)
    {
        struct mmLayerDirector* pDirector = this->pDirector;

        mmLayerDirector_SceneExclusiveEnterBackground(pDirector, this->getName().c_str());
        return true;
    }
}
