/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmLayerCommonTextureManager_h__
#define __mmLayerCommonTextureManager_h__

#include "core/mmCore.h"

#include "container/mmVectorValue.h"

#include "dish/mmEvent.h"

#include "layer/director/mmLayerContext.h"

#include "CEGUI/Window.h"
#include "CEGUI/BasicImage.h"

#include "CEGUI/widgets/ListboxTextItem.h"

#include "CEGUIOgreRenderer/TextureManager.h"

#include "layer/utility/mmLayerUtilityFps.h"
#include "layer/utility/mmLayerUtilityTextItem.h"

namespace mm
{
    class mmLayerCommonTextureSeriesItem
    {
    public:
        mmLayerUtilityTextItem hTextItem;
    public:
        CEGUI::CEGUIOgreTextureSeriesAttribute hAttribute;
    public:
        mmLayerCommonTextureSeriesItem(void);
        ~mmLayerCommonTextureSeriesItem(void);
    public:
        void SetAttribute(CEGUI::CEGUIOgreTextureSeriesAttribute* pAttribute);
        void SetItemId(CEGUI::uint hItemId);
    public:
        void OnFinishLaunching(void);
        void OnBeforeTerminate(void);
    };

    extern void mmVectorValue_TextureSeriesItemEventProduce(void* obj, void* u);
    extern void mmVectorValue_TextureSeriesItemEventRecycle(void* obj, void* u);

    class mmLayerCommonTexturePageItem
    {
    public:
        mmLayerUtilityTextItem hTextItem;
    public:
        Ogre::uint32 hPageId;
        Ogre::String hTextureName;
    public:
        // we must check the texture name is valid, the texture page can be valid. weak ref.
        CEGUI::CEGUIOgreTexturePage* hTexturePage;
    public:
        mmLayerCommonTexturePageItem(void);
        ~mmLayerCommonTexturePageItem(void);
    public:
        void SetPageId(Ogre::uint32 hPageId);
        void SetItemId(CEGUI::uint hItemId);
        void SetTextureName(const Ogre::String& hTextureName);
        void SetTexturePage(CEGUI::CEGUIOgreTexturePage* pTexturePage);
    public:
        void OnFinishLaunching(void);
        void OnBeforeTerminate(void);
    };

    extern void mmVectorValue_TexturePageItemEventProduce(void* obj, void* u);
    extern void mmVectorValue_TexturePageItemEventRecycle(void* obj, void* u);

    class mmLayerCommonTextureManager : public mmLayerContext
    {
    public:
        static const CEGUI::String EventNamespace;          //!< Namespace for global events
        static const CEGUI::String WidgetTypeName;          //!< Window factory name
    public:
        CEGUI::Window* LayerCommonTextureManager;
        CEGUI::Window* StaticImageBackground;

        Window* DefaultWindowSeries;
        Window* ComboboxSeries;
        Window* StaticTextSeries;

        Window* DefaultWindowPage;
        Window* ComboboxPage;
        Window* StaticTextPage;

        Window* WidgetImageButtonBack;
        Window* StaticTextFps;

        Window* StaticImageTexture;
        //
        CEGUI::Combobox* pWidgetComboboxSeries;// weak ref.
        CEGUI::Combobox* pWidgetComboboxPage;// weak ref.
    public:
        mmLayerUtilityFps hUtilityFps;
    public:
        CEGUI::String hImageOName;
        CEGUI::String hImageNName;
        CEGUI::BasicImage* pImage;
        CEGUI::CEGUIOgreTexture* pCEGUIOgreTexture;
        //
        Ogre::TexturePtr hOgreTexture;
    public:
        struct mmVectorValue hItemSeriesVector;
        struct mmVectorValue hItemPageVector;
    public:
        size_t hCurrentSeriesIndex;
        size_t hCurrentPageIndex;
    public:
        CEGUI::Event::Connection hLayerEventShownConn;
        CEGUI::Event::Connection hLayerEventHiddenConn;
        CEGUI::Event::Connection hTextureEventMouseClickConn;
        CEGUI::Event::Connection hTextureEventUpdatedConn;
        //
        CEGUI::Event::Connection hBackEventMouseClickConn;
    public:
        mmLayerCommonTextureManager(const CEGUI::String& type, const CEGUI::String& name);
        virtual ~mmLayerCommonTextureManager(void);
    public:
        // mmLayerContext
        virtual void OnFinishLaunching(void);
        virtual void OnBeforeTerminate(void);
    public:
        void UpdateLayerView(void);
        void DeleteLayerView(void);
    private:
        void ShowSeries(size_t hSeriesIndex);
        void ShowPage(size_t hPageIndex);
    private:
        void CreateComboboxSeriesItem(void);
        void DeleteComboboxSeriesItem(void);
        void UpdateComboboxSeriesItem(void);
    private:
        void CreateComboboxPageItem(CEGUI::CEGUIOgreTextureSeries* pTextureSeries);
        void DeleteComboboxPageItem();
        void UpdateComboboxPageItem(CEGUI::CEGUIOgreTextureSeries* pTextureSeries);
    private:
        void GenerateSeriesName(CEGUI::CEGUIOgreTextureSeries* pTextureSeries, CEGUI::String* pSeriesName);
        void GeneratePageName(CEGUI::CEGUIOgreTexturePage* pTexturePage, CEGUI::String* pPageName);
    private:
        void LoggerSeriesInformation(CEGUI::CEGUIOgreTextureSeries* pTextureSeries);
        void LoggerPageInformation(CEGUI::CEGUIOgreTexturePage* pTexturePage);
    private:
        bool OnWidgetComboboxSeriesEventListSelectionAccepted(const CEGUI::EventArgs& args);
        bool OnWidgetComboboxPageEventListSelectionAccepted(const CEGUI::EventArgs& args);
    private:
        bool OnLayerEventShown(const CEGUI::EventArgs& args);
        bool OnLayerEventHidden(const CEGUI::EventArgs& args);
        bool OnTextureEventMouseClick(const CEGUI::EventArgs& args);
        bool OnTextureEventUpdated(const CEGUI::EventArgs& args);
    private:
        bool OnWidgetImageButtonBackEventMouseClick(const CEGUI::EventArgs& args);
    };
}

#endif//__mmLayerCommonTextureManager_h__