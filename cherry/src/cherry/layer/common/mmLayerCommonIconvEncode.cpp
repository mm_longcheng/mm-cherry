/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerCommonIconvEncode.h"

#include "CEGUI/WindowManager.h"
#include "CEGUI/InputEvent.h"
#include "CEGUI/widgets/Combobox.h"
#include "CEGUI/widgets/Editbox.h"
#include "CEGUI/widgets/ComboDropList.h"
#include "CEGUI/widgets/PushButton.h"
#include "CEGUI/widgets/ListboxTextItem.h"

#include "toolkit/mmIconv.h"

#include "nwsi/mmContextMaster.h"
#include "module/mmModuleTookit.h"
#include "module/mmModuleLogger.h"

#include "layer/utility/mmLayerUtilityTextItem.h"

namespace mm
{
    static const char* __coder_preset[] =
    {
        "utf-8",
        "gb2312",
        "big5",
        "euc-jp",
    };
    static const size_t __coder_preset_size = MM_ARRAY_SIZE(__coder_preset);

    const CEGUI::String mmLayerCommonIconvEncode::EventNamespace = "mm";
    const CEGUI::String mmLayerCommonIconvEncode::WidgetTypeName = "mm/mmLayerCommonIconvEncode";

    const CEGUI::String mmLayerCommonIconvEncode::EventIconvCoderChanged = "EventIconvCoderChanged";

    mmLayerCommonIconvEncode::mmLayerCommonIconvEncode(const CEGUI::String& type, const CEGUI::String& name)
        : mmLayerContext(type, name)
        , LayerCommonIconvEncode(NULL)

        , ButtonRefresh(NULL)
        , WidgetComboboxEncode(NULL)

        , pWidgetComboboxEncode(NULL)

        , pIconvContext(NULL)
    {
        struct mmVectorValueEventAllocator hEventAllocator;

        mmVectorValue_Init(&this->hItemVector);

        hEventAllocator.Produce = &mmVectorValue_TextItemEventProduce;
        hEventAllocator.Recycle = &mmVectorValue_TextItemEventRecycle;
        hEventAllocator.obj = this;
        mmVectorValue_SetElemSize(&this->hItemVector, sizeof(mmLayerUtilityTextItem));
        mmVectorValue_SetEventAllocator(&this->hItemVector, &hEventAllocator);
    }
    mmLayerCommonIconvEncode::~mmLayerCommonIconvEncode()
    {
        assert(0 == this->hItemVector.size && "clear item map before destroy.");

        mmVectorValue_Destroy(&this->hItemVector);
    }
    void mmLayerCommonIconvEncode::SetIconvContext(struct mmIconvContext* pIconvContext)
    {
        this->pIconvContext = pIconvContext;
    }
    void mmLayerCommonIconvEncode::OnFinishLaunching()
    {
        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        this->LayerCommonIconvEncode = pWindowManager->loadLayoutFromFile("common/LayerCommonIconvEncode.layout");
        this->addChild(this->LayerCommonIconvEncode);

        // Manual set whole area.
        CEGUI::URect hWholeArea(CEGUI::UDim(0, 0), CEGUI::UDim(0, 0), CEGUI::UDim(1, 0), CEGUI::UDim(1, 0));
        this->LayerCommonIconvEncode->setArea(hWholeArea);

        this->ButtonRefresh = this->LayerCommonIconvEncode->getChild("ButtonRefresh");
        this->WidgetComboboxEncode = this->LayerCommonIconvEncode->getChild("WidgetComboboxEncode");

        this->pWidgetComboboxEncode = (CEGUI::Combobox*)(this->WidgetComboboxEncode);

        this->WidgetComboboxEncode->setText(mmString_CStr(&this->pIconvContext->hFormatHost));

        this->CreateCoderItem();

        this->ButtonRefresh->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerCommonIconvEncode::OnButtonRefreshEventMouseClick, this));
        this->WidgetComboboxEncode->subscribeEvent(CEGUI::Combobox::EventTextAccepted, CEGUI::Event::Subscriber(&mmLayerCommonIconvEncode::OnWidgetComboboxEncodeEventTextAccepted, this));
        this->WidgetComboboxEncode->subscribeEvent(CEGUI::Combobox::EventListSelectionAccepted, CEGUI::Event::Subscriber(&mmLayerCommonIconvEncode::OnWidgetComboboxEncodeEventListSelectionAccepted, this));
    }

    void mmLayerCommonIconvEncode::OnBeforeTerminate()
    {
        this->DeleteCoderItem();

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        this->removeChild(this->LayerCommonIconvEncode);
        pWindowManager->destroyWindow(this->LayerCommonIconvEncode);
        this->LayerCommonIconvEncode = NULL;
    }
    void mmLayerCommonIconvEncode::UpdateLayerValue()
    {
        this->WidgetComboboxEncode->setText(mmString_CStr(&this->pIconvContext->hFormatHost));
    }
    void mmLayerCommonIconvEncode::CreateCoderItem()
    {
        size_t i = 0;

        mmLayerUtilityTextItem* _item = NULL;
        const char* _coder = NULL;

        mmVectorValue_Resize(&this->hItemVector, __coder_preset_size);

        while (i < __coder_preset_size)
        {
            _coder = __coder_preset[i];
            _item = (mmLayerUtilityTextItem*)mmVectorValue_GetIndexReference(&this->hItemVector, i);
            _item->setText(_coder);
            _item->setID(i);
            _item->setSelectionBrushImage("TaharezLook/TextSelectionBrush");
            _item->SetScaleY(1.0f);
            this->pWidgetComboboxEncode->addItem(_item);
            i++;
        }
    }
    void mmLayerCommonIconvEncode::DeleteCoderItem()
    {
        size_t i = 0;

        mmLayerUtilityTextItem* _item = NULL;

        while (i < __coder_preset_size)
        {
            _item = (mmLayerUtilityTextItem*)mmVectorValue_GetIndexReference(&this->hItemVector, i);
            this->pWidgetComboboxEncode->removeItem(_item);
            i++;
        }
        // clear the weak ref.
        mmVectorValue_Clear(&this->hItemVector);
    }
    void mmLayerCommonIconvEncode::CheckCurrentCoder()
    {
        const CEGUI::String& _coder = this->WidgetComboboxEncode->getText();

        struct mmContextMaster* pContextMaster = this->pContextMaster;
        mm::mmModule* pModule = &pContextMaster->hModule;

        struct mmIconvContext* pIconvContext = this->pIconvContext;

        const char* _coder_n_c_str = _coder.c_str();
        const char* _coder_o_c_str = mmString_CStr(&pIconvContext->hFormatHost);

        if (0 != mmStrcmp(_coder_n_c_str, _coder_o_c_str))
        {
            if (MM_TRUE == mmIconvContext_IsValidFormat(pIconvContext, _coder_n_c_str))
            {
                mmIconvContext_UpdateFormat(pIconvContext, _coder_n_c_str, "utf-8");

                CEGUI::WindowEventArgs hArgs(this);
                this->fireEvent(EventIconvCoderChanged, hArgs, EventNamespace);
            }
            else
            {
                mm::mmModuleLogger* pModuleLogger = pModule->GetData<mm::mmModuleLogger>();
                
                char message[128] = { 0 };
                mmSprintf(message, "Iconv coder: %s is invalid.", _coder_n_c_str);
                pModuleLogger->LogView(MM_LOG_INFO, 0, message, message);
            }
        }
    }
    bool mmLayerCommonIconvEncode::OnButtonRefreshEventMouseClick(const CEGUI::EventArgs& args)
    {
        this->CheckCurrentCoder();
        return true;
    }
    bool mmLayerCommonIconvEncode::OnWidgetComboboxEncodeEventTextAccepted(const CEGUI::EventArgs& args)
    {
        this->CheckCurrentCoder();
        return true;
    }
    bool mmLayerCommonIconvEncode::OnWidgetComboboxEncodeEventListSelectionAccepted(const CEGUI::EventArgs& args)
    {
        this->CheckCurrentCoder();
        return true;
    }
}
