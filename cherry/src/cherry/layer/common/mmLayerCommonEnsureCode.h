/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmLayerCommonEnsureCode_h__
#define __mmLayerCommonEnsureCode_h__

#include "core/mmCore.h"

#include "random/mmXoshiro.h"

#include "dish/mmEvent.h"

#include "layer/director/mmLayerContext.h"

#include "CEGUI/Window.h"

namespace mm
{
    class mmLayerCommonEnsureCode : public mmLayerContext
    {
    public:
        static const CEGUI::String EventNamespace;          //!< Namespace for global events
        static const CEGUI::String WidgetTypeName;          //!< Window factory name
    public:
        // mm_choice_event_args
        static const CEGUI::String EventChoice;
    public:
        CEGUI::Window* LayerCommonEnsureCode;
        CEGUI::Window* StaticImageBackground;

        CEGUI::Window* StaticTextDesc;
        CEGUI::Window* LabelTime;

        CEGUI::Window* ButtonSafety;

        CEGUI::Window* DefaultWindowSafety;
        CEGUI::Window* StaticTextCode;
        CEGUI::Window* EditboxCode;

        CEGUI::Window* WidgetImageButtonEnsure;
        CEGUI::Window* WidgetImageButtonCancel;

        CEGUI::Window* StaticImageAttention;
    public:
        double hTimerInterval;
        double hTimerUpdate;
        double hTimerTerminate;
        double hTimerTerminateSecond;
    public:
        int hChoiceCode;
    public:
        CEGUI::Colour hColourCorrect;
        CEGUI::Colour hColourMistake;
    public:
        struct mmXoshiro256starstar hRandmon;
    public:
        struct mmString hLanguage;
        struct mmString hCountry;
        struct mmString hLanguageCountry;
    public:
        CEGUI::Event::Connection hEventChoiceConn;
    public:
        mmLayerCommonEnsureCode(const CEGUI::String& type, const CEGUI::String& name);
        virtual ~mmLayerCommonEnsureCode(void);
    public:
        // mmLayerContext
        virtual void OnFinishLaunching(void);
        virtual void OnBeforeTerminate(void);
    public:
        void SetTerminateSecond(double hTerminateSecond);
        void SetDescription(const char* pDescription);
        void SetDescriptionErrorcode(mmUInt32_t hErrorcode);
    public:
        void StartTimerTerminate(void);
        void RandomConfirmCode(void);
    private:
        void RefreshTimeView(void);
        void FireLayerEvent(void);
        void UpdateSafetyStatus(void);
    private:
        bool OnLayerEventUpdated(const CEGUI::EventArgs& args);
    private:
        bool OnEditboxCodeEventTextChanged(const CEGUI::EventArgs& args);
    private:
        bool OnButtonSafetyEventMouseClick(const CEGUI::EventArgs& args);
    private:
        bool OnWidgetImageButtonEnsureEventMouseClick(const CEGUI::EventArgs& args);
        bool OnWidgetImageButtonCancelEventMouseClick(const CEGUI::EventArgs& args);
    private:
        bool OnLayerCommonEnsureCodeEventMouseClick(const CEGUI::EventArgs& args);
    };
}

#endif//__mmLayerCommonEnsureCode_h__