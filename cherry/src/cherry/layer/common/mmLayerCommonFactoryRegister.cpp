/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerCommonFactoryRegister.h"
#include "layer/director/mmLayerDirector.h"
#include "CEGUI/WindowFactoryManager.h"

#include "mmLayerCommonEnsure.h"
#include "mmLayerCommonEnsureCode.h"
#include "mmLayerCommonTextureManager.h"
#include "mmLayerCommonIconvEncode.h"

void mmLayerCommonFactory_Register(struct mmLayerDirector* pDirector)
{
    CEGUI::WindowFactoryManager* pWindowFactoryManager = CEGUI::WindowFactoryManager::getSingletonPtr();

    pWindowFactoryManager->addWindowType<mm::mmLayerCommonEnsure>();
    pWindowFactoryManager->addWindowType<mm::mmLayerCommonEnsureCode>();
    pWindowFactoryManager->addWindowType<mm::mmLayerCommonTextureManager>();
    pWindowFactoryManager->addWindowType<mm::mmLayerCommonIconvEncode>();
}
void mmLayerCommonFactory_Unregister(struct mmLayerDirector* pDirector)
{
    CEGUI::WindowFactoryManager* pWindowFactoryManager = CEGUI::WindowFactoryManager::getSingletonPtr();

    pWindowFactoryManager->removeFactory(mm::mmLayerCommonEnsure::WidgetTypeName);
    pWindowFactoryManager->removeFactory(mm::mmLayerCommonEnsureCode::WidgetTypeName);
    pWindowFactoryManager->removeFactory(mm::mmLayerCommonTextureManager::WidgetTypeName);
    pWindowFactoryManager->removeFactory(mm::mmLayerCommonIconvEncode::WidgetTypeName);
}
