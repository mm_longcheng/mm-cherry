/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerCommonEnsureCode.h"

#include "CEGUI/WindowManager.h"
#include "CEGUI/widgets/Editbox.h"

#include "layer/director/mmLayerDirector.h"

#include "script/mmLuaScriptLanguage.h"

#include "nwsi/mmLocale.h"
#include "nwsi/mmContextMaster.h"
#include "module/mmModuleTookit.h"

#include "mmLayerCommonEnsureEvent.h"

namespace mm
{
    const CEGUI::String mmLayerCommonEnsureCode::EventNamespace = "mm";
    const CEGUI::String mmLayerCommonEnsureCode::WidgetTypeName = "mm/mmLayerCommonEnsureCode";

    const CEGUI::String mmLayerCommonEnsureCode::EventChoice = "EventChoice";

    mmLayerCommonEnsureCode::mmLayerCommonEnsureCode(const CEGUI::String& type, const CEGUI::String& name)
        : mmLayerContext(type, name)
        , LayerCommonEnsureCode(NULL)
        , StaticImageBackground(NULL)

        , StaticTextDesc(NULL)
        , LabelTime(NULL)

        , ButtonSafety(NULL)

        , DefaultWindowSafety(NULL)
        , StaticTextCode(NULL)
        , EditboxCode(NULL)

        , WidgetImageButtonEnsure(NULL)
        , WidgetImageButtonCancel(NULL)

        , StaticImageAttention(NULL)

        , hTimerInterval(0.0)
        , hTimerUpdate(1.0)
        , hTimerTerminate(60.0)
        , hTimerTerminateSecond(60.0)

        , hChoiceCode(mmChoiceEventArgs::CANCEL)

        , hColourCorrect(0.0f, 1.0f, 0.0f, 1.0f)
        , hColourMistake(1.0f, 0.0f, 0.0f, 1.0f)
    {
        CEGUI::Colour d_ColourCorrect;
        CEGUI::Colour d_ColourMistake;

        mmXoshiro256starstar_Init(&this->hRandmon);

        mmUInt64_t seed = (mmUInt64_t)time(NULL);
        mmXoshiro256starstar_Srand(&this->hRandmon, seed);

        mmString_Init(&this->hLanguage);
        mmString_Init(&this->hCountry);
        mmString_Init(&this->hLanguageCountry);

        mmString_Assigns(&this->hLanguage, "zh");
        mmString_Assigns(&this->hCountry, "CN");
        mmString_Assigns(&this->hLanguageCountry, "zh_CN");
    }
    mmLayerCommonEnsureCode::~mmLayerCommonEnsureCode(void)
    {
        mmXoshiro256starstar_Destroy(&this->hRandmon);

        mmString_Destroy(&this->hLanguage);
        mmString_Destroy(&this->hCountry);
        mmString_Destroy(&this->hLanguageCountry);
    }

    void mmLayerCommonEnsureCode::OnFinishLaunching(void)
    {
        struct mmSurfaceMaster* pSurfaceMaster = this->pSurfaceMaster;

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        this->LayerCommonEnsureCode = pWindowManager->loadLayoutFromFile("common/LayerCommonEnsureCode.layout");
        this->addChild(this->LayerCommonEnsureCode);

        this->StaticImageBackground = this->LayerCommonEnsureCode->getChild("StaticImageBackground");

        this->StaticTextDesc = this->StaticImageBackground->getChild("StaticTextDesc");
        this->LabelTime = this->StaticImageBackground->getChild("LabelTime");

        this->ButtonSafety = this->StaticImageBackground->getChild("ButtonSafety");

        this->DefaultWindowSafety = this->StaticImageBackground->getChild("DefaultWindowSafety");
        this->StaticTextCode = this->DefaultWindowSafety->getChild("StaticTextCode");
        this->EditboxCode = this->DefaultWindowSafety->getChild("EditboxCode");

        this->WidgetImageButtonEnsure = this->StaticImageBackground->getChild("WidgetImageButtonEnsure");
        this->WidgetImageButtonCancel = this->StaticImageBackground->getChild("WidgetImageButtonCancel");

        this->StaticImageAttention = this->StaticImageBackground->getChild("StaticImageAttention");

        this->ButtonSafety->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerCommonEnsureCode::OnButtonSafetyEventMouseClick, this));
        //
        this->WidgetImageButtonEnsure->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerCommonEnsureCode::OnWidgetImageButtonEnsureEventMouseClick, this));
        this->WidgetImageButtonCancel->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerCommonEnsureCode::OnWidgetImageButtonCancelEventMouseClick, this));
        //
        this->EditboxCode->subscribeEvent(CEGUI::Window::EventTextChanged, CEGUI::Event::Subscriber(&mmLayerCommonEnsureCode::OnEditboxCodeEventTextChanged, this));

        this->subscribeEvent(CEGUI::Window::EventUpdated, CEGUI::Event::Subscriber(&mmLayerCommonEnsureCode::OnLayerEventUpdated, this));

        this->LayerCommonEnsureCode->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerCommonEnsureCode::OnLayerCommonEnsureCodeEventMouseClick, this));

        mmLocale_LanguageCountry(&this->hLanguage, &this->hCountry);

        mmString_Assign(&this->hLanguageCountry, &this->hLanguage);
        mmString_Appends(&this->hLanguageCountry, "_");
        mmString_Append(&this->hLanguageCountry, &this->hCountry);
    }

    void mmLayerCommonEnsureCode::OnBeforeTerminate(void)
    {
        struct mmSurfaceMaster* pSurfaceMaster = this->pSurfaceMaster;

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        pWindowManager->destroyWindow(this->LayerCommonEnsureCode);
        this->LayerCommonEnsureCode = NULL;
    }
    void mmLayerCommonEnsureCode::SetTerminateSecond(double hTerminateSecond)
    {
        this->hTimerTerminateSecond = hTerminateSecond;
        this->hTimerTerminate = this->hTimerTerminateSecond;
    }
    void mmLayerCommonEnsureCode::SetDescription(const char* pDescription)
    {
        // must be code for utf-8.
        this->StaticTextDesc->setText(pDescription);
    }
    void mmLayerCommonEnsureCode::SetDescriptionErrorcode(mmUInt32_t hErrorcode)
    {
        struct mmContextMaster* pContextMaster = this->pContextMaster;
        mm::mmModule* pModule = &pContextMaster->hModule;

        std::string hDesc;
        CEGUI::String hDescUtf8;
        //
        mm::mmModuleTookit* pModuleTookit = pModule->GetData<mm::mmModuleTookit>();
        struct lua_State* L = pModuleTookit->hLuaContext.state;
        mmLuaScriptLanguage_DescByCode(L, mmString_CStr(&this->hLanguageCountry), 0x00000001, hDesc);
        //
        hDescUtf8.assign((const CEGUI::utf8*)hDesc.data(), hDesc.size());
        this->StaticTextDesc->setText(hDescUtf8);
    }
    void mmLayerCommonEnsureCode::StartTimerTerminate(void)
    {
        this->hTimerTerminate = this->hTimerTerminateSecond;
        this->EditboxCode->setText("");
        this->UpdateSafetyStatus();
    }
    void mmLayerCommonEnsureCode::RandomConfirmCode(void)
    {
        mmUInt64_t n = 0;
        mmUInt32_t v = 0;
        char n_string[32] = { 0 };

        n = mmXoshiro256starstar_Next(&this->hRandmon);

        v = n % 999999;

        mmSprintf(n_string, "%06u", v);

        this->StaticTextCode->setText(n_string);
    }
    void mmLayerCommonEnsureCode::RefreshTimeView(void)
    {
        char hTimeString[64] = { 0 };
        mmSprintf(hTimeString, "%u", (mmUInt32_t)this->hTimerTerminate);
        this->LabelTime->setText(hTimeString);
    }
    void mmLayerCommonEnsureCode::FireLayerEvent(void)
    {
        mmChoiceEventArgs _args(this, this->hChoiceCode);
        this->fireEvent(EventChoice, _args, EventNamespace);
        this->setVisible(false);
        this->deactivate();
        this->hEventChoiceConn->disconnect();
    }
    void mmLayerCommonEnsureCode::UpdateSafetyStatus(void)
    {
        if (this->DefaultWindowSafety->isVisible())
        {
            this->WidgetImageButtonEnsure->setDisabled(true);
            this->ButtonSafety->setText("================");
        }
        else
        {
            this->WidgetImageButtonEnsure->setDisabled(false);
            this->ButtonSafety->setText("----------------");
        }
    }
    bool mmLayerCommonEnsureCode::OnEditboxCodeEventTextChanged(const CEGUI::EventArgs& args)
    {
        CEGUI::String hColourString;

        if (this->StaticTextCode->getText() == this->EditboxCode->getText())
        {
            hColourString = CEGUI::PropertyHelper<CEGUI::Colour>::toString(this->hColourCorrect);

            this->EditboxCode->setProperty("NormalTextColour", hColourString);
            this->WidgetImageButtonEnsure->setDisabled(false);
        }
        else
        {
            hColourString = CEGUI::PropertyHelper<CEGUI::Colour>::toString(this->hColourMistake);

            this->EditboxCode->setProperty("NormalTextColour", hColourString);
            this->WidgetImageButtonEnsure->setDisabled(true);
        }
        return true;
    }
    bool mmLayerCommonEnsureCode::OnLayerEventUpdated(const CEGUI::EventArgs& args)
    {
        const CEGUI::UpdateEventArgs& evt = (const CEGUI::UpdateEventArgs&)(args);

        this->hTimerInterval += evt.d_timeSinceLastFrame;
        this->hTimerTerminate -= evt.d_timeSinceLastFrame;
        this->hTimerTerminate = 0 < this->hTimerTerminate ? this->hTimerTerminate : 0;

        if (this->hTimerInterval >= this->hTimerUpdate)
        {
            this->RefreshTimeView();
            this->hTimerInterval = 0.0;
        }

        if (0 >= this->hTimerTerminate)
        {
            this->hChoiceCode = mmChoiceEventArgs::CANCEL;
            this->FireLayerEvent();
        }
        return true;
    }
    bool mmLayerCommonEnsureCode::OnButtonSafetyEventMouseClick(const CEGUI::EventArgs& args)
    {
        bool _is_visible = this->DefaultWindowSafety->isVisible();
        this->DefaultWindowSafety->setVisible(!_is_visible);
        this->UpdateSafetyStatus();
        return true;
    }
    bool mmLayerCommonEnsureCode::OnWidgetImageButtonEnsureEventMouseClick(const CEGUI::EventArgs& args)
    {
        this->hChoiceCode = mmChoiceEventArgs::ENSURE;
        this->FireLayerEvent();
        return true;
    }
    bool mmLayerCommonEnsureCode::OnWidgetImageButtonCancelEventMouseClick(const CEGUI::EventArgs& args)
    {
        this->hChoiceCode = mmChoiceEventArgs::CANCEL;
        this->FireLayerEvent();
        return true;
    }
    bool mmLayerCommonEnsureCode::OnLayerCommonEnsureCodeEventMouseClick(const CEGUI::EventArgs& args)
    {
        this->hChoiceCode = mmChoiceEventArgs::CANCEL;
        this->FireLayerEvent();
        return true;
    }
}
