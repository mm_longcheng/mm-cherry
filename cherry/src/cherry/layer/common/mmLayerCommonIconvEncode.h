/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmLayerCommonIconvEncode_h__
#define __mmLayerCommonIconvEncode_h__

#include "core/mmCore.h"

#include "container/mmVectorValue.h"

#include "dish/mmEvent.h"

#include "layer/director/mmLayerContext.h"

#include "CEGUI/Window.h"

struct mmIconvContext;

namespace mm
{
    class mmLayerCommonIconvEncode : public mmLayerContext
    {
    public:
        static const CEGUI::String EventNamespace;          //!< Namespace for global events
        static const CEGUI::String WidgetTypeName;          //!< Window factory name
    public:
        static const CEGUI::String EventIconvCoderChanged;
    public:
        CEGUI::Window* LayerCommonIconvEncode;

        CEGUI::Window* ButtonRefresh;
        CEGUI::Window* WidgetComboboxEncode;

        CEGUI::Combobox* pWidgetComboboxEncode;// weak ref.
    public:
        struct mmIconvContext* pIconvContext;// weak ref.
    public:
        struct mmVectorValue hItemVector;
    public:
        mmLayerCommonIconvEncode(const CEGUI::String& type, const CEGUI::String& name);
        virtual ~mmLayerCommonIconvEncode(void);
    public:
        void SetIconvContext(struct mmIconvContext* pIconvContext);
    public:
        // mmLayerContext
        virtual void OnFinishLaunching(void);
        virtual void OnBeforeTerminate(void);
    public:
        void UpdateLayerValue(void);
    private:
        void CreateCoderItem(void);
        void DeleteCoderItem(void);
    private:
        void CheckCurrentCoder(void);
    private:
        bool OnButtonRefreshEventMouseClick(const CEGUI::EventArgs& args);
        bool OnWidgetComboboxEncodeEventTextAccepted(const CEGUI::EventArgs& args);
        bool OnWidgetComboboxEncodeEventListSelectionAccepted(const CEGUI::EventArgs& args);
    };
}

#endif//__mmLayerCommonIconvEncode_h__