/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerCommonEnsure.h"

#include "CEGUI/WindowManager.h"

#include "layer/director/mmLayerDirector.h"

#include "script/mmLuaScriptLanguage.h"

#include "nwsi/mmLocale.h"
#include "nwsi/mmContextMaster.h"
#include "module/mmModuleTookit.h"

#include "mmLayerCommonEnsureEvent.h"

namespace mm
{
    const CEGUI::String mmLayerCommonEnsure::EventNamespace = "mm";
    const CEGUI::String mmLayerCommonEnsure::WidgetTypeName = "mm/mmLayerCommonEnsure";

    const CEGUI::String mmLayerCommonEnsure::EventChoice = "EventChoice";

    mmLayerCommonEnsure::mmLayerCommonEnsure(const CEGUI::String& type, const CEGUI::String& name)
        : mmLayerContext(type, name)
        , LayerCommonEnsure(NULL)
        , StaticImageBackground(NULL)

        , StaticTextDesc(NULL)
        , LabelTime(NULL)

        , WidgetImageButtonEnsure(NULL)
        , WidgetImageButtonCancel(NULL)

        , StaticImageAttention(NULL)

        , hTimerInterval(0.0)
        , hTimerUpdate(1.0)
        , hTimerTerminate(60.0)
        , hTimerTerminateSecond(60.0)

        , hChoiceCode(mmChoiceEventArgs::CANCEL)
    {
        mmString_Init(&this->hLanguage);
        mmString_Init(&this->hCountry);
        mmString_Init(&this->hLanguageCountry);

        mmString_Assigns(&this->hLanguage, "zh");
        mmString_Assigns(&this->hCountry, "CN");
        mmString_Assigns(&this->hLanguageCountry, "zh_CN");
    }
    mmLayerCommonEnsure::~mmLayerCommonEnsure()
    {
        mmString_Destroy(&this->hLanguage);
        mmString_Destroy(&this->hCountry);
        mmString_Destroy(&this->hLanguageCountry);
    }

    void mmLayerCommonEnsure::OnFinishLaunching()
    {
        struct mmSurfaceMaster* pSurfaceMaster = this->pSurfaceMaster;

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        this->LayerCommonEnsure = pWindowManager->loadLayoutFromFile("common/LayerCommonEnsure.layout");
        this->addChild(this->LayerCommonEnsure);

        this->StaticImageBackground = this->LayerCommonEnsure->getChild("StaticImageBackground");

        this->StaticTextDesc = this->StaticImageBackground->getChild("StaticTextDesc");
        this->LabelTime = this->StaticImageBackground->getChild("LabelTime");

        this->WidgetImageButtonEnsure = this->StaticImageBackground->getChild("WidgetImageButtonEnsure");
        this->WidgetImageButtonCancel = this->StaticImageBackground->getChild("WidgetImageButtonCancel");

        this->StaticImageAttention = this->StaticImageBackground->getChild("StaticImageAttention");

        this->subscribeEvent(CEGUI::Window::EventUpdated, CEGUI::Event::Subscriber(&mmLayerCommonEnsure::OnLayerEventUpdated, this));
        
        this->WidgetImageButtonEnsure->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerCommonEnsure::OnWidgetImageButtonEnsureEventMouseClick, this));
        this->WidgetImageButtonCancel->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerCommonEnsure::OnWidgetImageButtonCancelEventMouseClick, this));

        this->LayerCommonEnsure->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerCommonEnsure::OnLayerCommonEnsureCodeEventMouseClick, this));

        mmLocale_LanguageCountry(&this->hLanguage, &this->hCountry);

        mmString_Assign(&this->hLanguageCountry, &this->hLanguage);
        mmString_Appends(&this->hLanguageCountry, "_");
        mmString_Append(&this->hLanguageCountry, &this->hCountry);
    }

    void mmLayerCommonEnsure::OnBeforeTerminate()
    {
        struct mmSurfaceMaster* pSurfaceMaster = this->pSurfaceMaster;

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        pWindowManager->destroyWindow(this->LayerCommonEnsure);
        this->LayerCommonEnsure = NULL;
    }
    void mmLayerCommonEnsure::SetTerminateSecond(double hTerminateSecond)
    {
        this->hTimerTerminateSecond = hTerminateSecond;
        this->hTimerTerminate = this->hTimerTerminateSecond;
    }
    void mmLayerCommonEnsure::SetDescription(const char* pDescription)
    {
        // must be code for utf-8.
        this->StaticTextDesc->setText(pDescription);
    }
    void mmLayerCommonEnsure::SetDescriptionErrorcode(mmUInt32_t hErrorcode)
    {
        struct mmContextMaster* pContextMaster = this->pContextMaster;
        mm::mmModule* pModule = &pContextMaster->hModule;

        std::string hDesc;
        CEGUI::String hDescUtf8;
        //
        mm::mmModuleTookit* pModuleTookit = pModule->GetData<mm::mmModuleTookit>();
        struct lua_State* L = pModuleTookit->hLuaContext.state;
        mmLuaScriptLanguage_DescByCode(L, mmString_CStr(&this->hLanguageCountry), hErrorcode, hDesc);
        //
        hDescUtf8.assign((const CEGUI::utf8*)hDesc.data(), hDesc.size());
        this->StaticTextDesc->setText(hDescUtf8);
    }
    void mmLayerCommonEnsure::StartTimerTerminate()
    {
        this->hTimerTerminate = this->hTimerTerminateSecond;
    }
    void mmLayerCommonEnsure::RefreshTimeView()
    {
        char hTimeString[64] = { 0 };
        mmSprintf(hTimeString, "%u", (mmUInt32_t)this->hTimerTerminate);
        this->LabelTime->setText(hTimeString);
    }
    void mmLayerCommonEnsure::FireLayerEvent()
    {
        mmChoiceEventArgs hArgs(this, this->hChoiceCode);
        this->fireEvent(EventChoice, hArgs, EventNamespace);
        this->setVisible(false);
        this->deactivate();
        this->hEventChoiceConn->disconnect();
    }
    bool mmLayerCommonEnsure::OnLayerEventUpdated(const CEGUI::EventArgs& args)
    {
        const CEGUI::UpdateEventArgs& evt = (const CEGUI::UpdateEventArgs&)(args);

        this->hTimerInterval += evt.d_timeSinceLastFrame;
        this->hTimerTerminate -= evt.d_timeSinceLastFrame;
        this->hTimerTerminate = 0 < this->hTimerTerminate ? this->hTimerTerminate : 0;

        if (this->hTimerInterval >= this->hTimerUpdate)
        {
            this->RefreshTimeView();
            this->hTimerInterval = 0.0;
        }

        if (0 >= this->hTimerTerminate)
        {
            this->hChoiceCode = mmChoiceEventArgs::CANCEL;
            this->FireLayerEvent();
        }
        return true;
    }
    bool mmLayerCommonEnsure::OnWidgetImageButtonEnsureEventMouseClick(const CEGUI::EventArgs& args)
    {
        this->hChoiceCode = mmChoiceEventArgs::ENSURE;
        this->FireLayerEvent();
        return true;
    }
    bool mmLayerCommonEnsure::OnWidgetImageButtonCancelEventMouseClick(const CEGUI::EventArgs& args)
    {
        this->hChoiceCode = mmChoiceEventArgs::CANCEL;
        this->FireLayerEvent();
        return true;
    }
    bool mmLayerCommonEnsure::OnLayerCommonEnsureCodeEventMouseClick(const CEGUI::EventArgs& args)
    {
        this->hChoiceCode = mmChoiceEventArgs::CANCEL;
        this->FireLayerEvent();
        return true;
    }
}
