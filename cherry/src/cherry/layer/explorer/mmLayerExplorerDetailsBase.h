/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmLayerExplorerDetailsBase_h__
#define __mmLayerExplorerDetailsBase_h__

#include "core/mmCore.h"

#include "dish/mmEvent.h"

#include "layer/director/mmLayerContext.h"

#include "CEGUI/Window.h"

struct mmExplorerCommand;
struct mmExplorerFinder;
struct mmExplorerImage;
struct mmExplorerItem;

struct mmIconvContext;

struct mmXoshiro256starstar;

namespace mm
{
    class mmLayerExplorerDetailsBase : public mmLayerContext
    {
    public:
        static const CEGUI::String EventNamespace;          //!< Namespace for global events
        static const CEGUI::String WidgetTypeName;          //!< Window factory name
    public:
        struct mmExplorerCommand* pExplorerCommand;
        struct mmExplorerFinder* pExplorerFinder;
        struct mmExplorerImage* pExplorerImage;
    public:
        struct mmString hDirectory;
        // weak ref.
        struct mmIconvContext* pIconvContext;
        // weak ref.
        struct mmExplorerItem* pItem;
        struct mmRbtsetVpt* pItemSelect;
    public:
        struct mmXoshiro256starstar* pRandmon;
    public:
        typedef bool (mmLayerExplorerDetailsBase::*RemoveEnsureCallback)(const CEGUI::EventArgs&);
    public:
        mmLayerExplorerDetailsBase(const CEGUI::String& type, const CEGUI::String& name);
        virtual ~mmLayerExplorerDetailsBase(void);
    public:
        // mmLayerContext
        virtual void OnFinishLaunching(void);
        virtual void OnBeforeTerminate(void);
    public:
        void SetDirectory(const char* pDirectory);
        void SetItem(struct mmExplorerItem* pItem);
        void SetItemSelect(struct mmRbtsetVpt* pItemSelect);
        void SetIconvContext(struct mmIconvContext* pIconvContext);
        void SetExplorerCommand(struct mmExplorerCommand* pExplorerCommand);
        void SetExplorerFinder(struct mmExplorerFinder* pExplorerFinder);
        void SetExplorerImage(struct mmExplorerImage* pExplorerImage);
        void SetXoshiroRandmon(struct mmXoshiro256starstar* pRandmon);
    public:
        virtual void UpdateLayerValue(void);
        virtual void UpdateUtf8View(void);
    public:
        void RandomEmbellishImage(CEGUI::Window* pImageVindow);
    public:
        virtual void OnEnterBackground(void);
        virtual void OnEnterForeground(void);
    public:
        void RemoveItemLayerEnsure(RemoveEnsureCallback hCallback);

        void LoggerView(mmUInt32_t lvl, mmUInt32_t code, const char* desc, const char* view);
    public:
        bool OnLayerEnsureEventChoiceRmdir(const CEGUI::EventArgs& args);
        bool OnLayerEnsureEventChoiceRemove(const CEGUI::EventArgs& args);
        bool OnLayerEnsureEventChoiceRemoveVector(const CEGUI::EventArgs& args);
    };
}

#endif//__mmLayerExplorerDetailsBase_h__