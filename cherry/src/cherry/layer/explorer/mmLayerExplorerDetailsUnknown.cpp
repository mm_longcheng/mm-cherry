/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerExplorerDetailsUnknown.h"

#include "CEGUI/WindowManager.h"
#include "CEGUI/widgets/Editbox.h"

#include "explorer/mmExplorerCommand.h"

#include "layer/utility/mmLayerUtilityIconv.h"

namespace mm
{
    const CEGUI::String mmLayerExplorerDetailsUnknown::EventNamespace = "mm";
    const CEGUI::String mmLayerExplorerDetailsUnknown::WidgetTypeName = "mm/mmLayerExplorerDetailsUnknown";

    mmLayerExplorerDetailsUnknown::mmLayerExplorerDetailsUnknown(const CEGUI::String& type, const CEGUI::String& name)
        : mmLayerExplorerDetailsBase(type, name)
        , LayerExplorerDetailsUnknown(NULL)
        , StaticImageBackground(NULL)

        , EditboxBasename(NULL)
        , StaticImageIcon(NULL)
        , StaticImageEmbellish(NULL)

        , WidgetImageButton00(NULL)
        , WidgetImageButton10(NULL)
        , WidgetImageButton11(NULL)
        , WidgetImageButton20(NULL)

        , pEditboxBasename(NULL)
    {
        mmString_Init(&this->hTextBaseNameHost);
    }
    mmLayerExplorerDetailsUnknown::~mmLayerExplorerDetailsUnknown(void)
    {
        mmString_Destroy(&this->hTextBaseNameHost);
    }
    void mmLayerExplorerDetailsUnknown::OnFinishLaunching(void)
    {
        struct mmSurfaceMaster* pSurfaceMaster = this->pSurfaceMaster;

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        this->LayerExplorerDetailsUnknown = pWindowManager->loadLayoutFromFile("explorer/LayerExplorerDetailsUnknown.layout");
        this->addChild(this->LayerExplorerDetailsUnknown);
        // Manual set whole area.
        CEGUI::URect hWholeArea(CEGUI::UDim(0, 0), CEGUI::UDim(0, 0), CEGUI::UDim(1, 0), CEGUI::UDim(1, 0));
        this->LayerExplorerDetailsUnknown->setArea(hWholeArea);

        this->StaticImageBackground = this->LayerExplorerDetailsUnknown->getChild("StaticImageBackground");

        this->EditboxBasename = this->StaticImageBackground->getChild("EditboxBasename");
        this->StaticImageIcon = this->StaticImageBackground->getChild("StaticImageIcon");
        this->StaticImageEmbellish = this->StaticImageBackground->getChild("StaticImageEmbellish");

        this->WidgetImageButton00 = this->StaticImageBackground->getChild("WidgetImageButton00");
        this->WidgetImageButton10 = this->StaticImageBackground->getChild("WidgetImageButton10");
        this->WidgetImageButton11 = this->StaticImageBackground->getChild("WidgetImageButton11");
        this->WidgetImageButton20 = this->StaticImageBackground->getChild("WidgetImageButton20");

        this->WidgetImageButton00->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsUnknown::OnWidgetImageButton00EventMouseClick, this));
        this->WidgetImageButton10->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsUnknown::OnWidgetImageButton10EventMouseClick, this));
        this->WidgetImageButton11->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsUnknown::OnWidgetImageButton11EventMouseClick, this));
        this->WidgetImageButton20->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsUnknown::OnWidgetImageButton20EventMouseClick, this));

        this->pEditboxBasename = (CEGUI::Editbox*)this->EditboxBasename;

        this->pEditboxBasename->subscribeEvent(CEGUI::Editbox::EventTextAccepted, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsUnknown::OnEditboxBasenameEventTextAccepted, this));
    }

    void mmLayerExplorerDetailsUnknown::OnBeforeTerminate(void)
    {
        struct mmSurfaceMaster* pSurfaceMaster = this->pSurfaceMaster;

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        this->removeChild(this->LayerExplorerDetailsUnknown);
        pWindowManager->destroyWindow(this->LayerExplorerDetailsUnknown);
        this->LayerExplorerDetailsUnknown = NULL;
    }
    void mmLayerExplorerDetailsUnknown::UpdateLayerValue(void)
    {
        this->UpdateUtf8View();
    }
    void mmLayerExplorerDetailsUnknown::UpdateUtf8View(void)
    {
        struct mmIconvContext* pIconvContext = this->pIconvContext;

        mmLayerUtilityIconv_ViewName(pIconvContext, this->EditboxBasename, &this->pItem->basename);
    }
    bool mmLayerExplorerDetailsUnknown::OnWidgetImageButton00EventMouseClick(const CEGUI::EventArgs& args)
    {
        struct mmIconvContext* pIconvContext = this->pIconvContext;
        mmLayerUtilityIconv_HostName(pIconvContext, this->EditboxBasename, &this->hTextBaseNameHost);

        mmExplorerCommand_RenameItem(this->pExplorerCommand, this->pItem, mmString_CStr(&this->hTextBaseNameHost));
        mmExplorerCommand_Refresh(this->pExplorerCommand, mmString_CStr(&this->hDirectory));

        static const char* message = "Rename File";
        this->LoggerView(MM_LOG_INFO, 0, message, message);
        return true;
    }
    bool mmLayerExplorerDetailsUnknown::OnWidgetImageButton10EventMouseClick(const CEGUI::EventArgs& args)
    {
        mmExplorerCommand_Copy(this->pExplorerCommand, this->pItemSelect);

        static const char* message = "Copy File";
        this->LoggerView(MM_LOG_INFO, 0, message, message);
        return true;
    }
    bool mmLayerExplorerDetailsUnknown::OnWidgetImageButton11EventMouseClick(const CEGUI::EventArgs& args)
    {
        mmExplorerCommand_Cut(this->pExplorerCommand, this->pItemSelect);

        static const char* message = "Cut File";
        this->LoggerView(MM_LOG_INFO, 0, message, message);
        return true;
    }
    bool mmLayerExplorerDetailsUnknown::OnWidgetImageButton20EventMouseClick(const CEGUI::EventArgs& args)
    {
        this->RemoveItemLayerEnsure(&mmLayerExplorerDetailsBase::OnLayerEnsureEventChoiceRemove);
        return true;
    }
    bool mmLayerExplorerDetailsUnknown::OnEditboxBasenameEventTextAccepted(const CEGUI::EventArgs& args)
    {
        this->OnWidgetImageButton00EventMouseClick(args);
        return true;
    }
}
