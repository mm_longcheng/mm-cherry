/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmLayerExplorerItem_h__
#define __mmLayerExplorerItem_h__

#include "core/mmCore.h"

#include "container/mmVectorValue.h"
#include "container/mmRbtsetVpt.h"

#include "dish/mmEvent.h"

#include "layer/director/mmLayerContext.h"

#include "toolkit/mmIconv.h"

#include "CEGUI/Window.h"

struct mmExplorerItem;
struct mmExplorerImage;

namespace mm
{
    class mmLayerExplorerItem : public mmLayerContext
    {
    public:
        static const CEGUI::String EventNamespace;          //!< Namespace for global events
        static const CEGUI::String WidgetTypeName;          //!< Window factory name
    public:
        static const CEGUI::String EventLayerItemMouseClick;
        static const CEGUI::String EventLayerItemImageMouseClick;
    public:
        CEGUI::Window* LayerExplorerItem;
        CEGUI::Window* StaticImageBackground;

        CEGUI::Window* StaticImageType;
        CEGUI::Window* StaticTextBasename;
        CEGUI::Window* StaticTextAuthority;
        CEGUI::Window* StaticTextMTime;
        CEGUI::Window* StaticTextSize;
    public:
        size_t hIndex;
    public:
        // weak ref.
        struct mmExplorerItem* pItem;
        // weak ref.
        struct mmIconvContext* pIconvContext;
        struct mmExplorerImage* pExplorerImage;
    public:
        CEGUI::Colour hColourLustre;
        CEGUI::Colour hColourSelect;
        CEGUI::Colour hColourCommon;
        bool hIsSelect;
    public:
        CEGUI::Event::Connection hItemEventMouseClickConn;
        CEGUI::Event::Connection hTypeEventMouseClickConn;
        //
        CEGUI::Event::Connection hEventLayerItemMouseClickConn;
        CEGUI::Event::Connection hEventLayerItemImageMouseClickConn;
    public:
        mmLayerExplorerItem(const CEGUI::String& type, const CEGUI::String& name);
        virtual ~mmLayerExplorerItem(void);
    public:
        // mmLayerContext
        virtual void OnFinishLaunching(void);
        virtual void OnBeforeTerminate(void);
    public:
        void SetIndex(size_t hIndex);
        void SetItem(struct mmExplorerItem* pItem);
        void SetIconvContext(struct mmIconvContext* pIconvContext);
        void SetExplorerImage(struct mmExplorerImage* pExplorerImage);
    public:
        void SetIsSelect(bool hIsSelect);
    public:
        void UpdateLayerValue(void);
        void UpdateUtf8View(void);
    public:
        void OnFinishAttach(void);
        void OnBeforeDetach(void);
    private:
        bool OnLayerItemEventMouseClick(const CEGUI::EventArgs& args);
        bool OnLayerTypeEventMouseClick(const CEGUI::EventArgs& args);
    };
}

#endif//__mmLayerExplorerItem_h__