/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmLayerExplorerDetailsEmu_h__
#define __mmLayerExplorerDetailsEmu_h__

#include "core/mmCore.h"

#include "dish/mmEvent.h"

#include "layer/director/mmLayerContext.h"

#include "CEGUI/Window.h"
#include "CEGUI/BasicImage.h"

#include "mmLayerExplorerDetailsBase.h"

#include "explorer/mmExplorerItemEmu.h"

#include "emulator/mmEmulatorAssets.h"

#include "toolkit/mmIconv.h"

#include "layer/emulator/mmLayerEmulatorWindow.h"

#include "layer/utility/mmLayerUtilityScrollbar.h"

namespace mm
{
    class mmLayerExplorerDetailsEmu : public mmLayerExplorerDetailsBase
    {
    public:
        static const CEGUI::String EventNamespace;          //!< Namespace for global events
        static const CEGUI::String WidgetTypeName;          //!< Window factory name
    public:
        static const std::string CEGUI_RTT_IMAGERY;
        static const std::string CEGUI_RTT_TEXTURE;
    public:
        CEGUI::Window* LayerExplorerDetailsEmu;
        CEGUI::Window* StaticImageBackground;

        CEGUI::Window* EditboxBasename;
        CEGUI::Window* StaticImageIcon;
        CEGUI::Window* StaticImagePreview;

        CEGUI::Window* WidgetImageButton00;
        CEGUI::Window* WidgetImageButton10;
        CEGUI::Window* WidgetImageButton11;
        CEGUI::Window* WidgetImageButton12;
        CEGUI::Window* WidgetImageButton20;
        CEGUI::Window* WidgetImageButton21;
        CEGUI::Window* WidgetImageButton22;
        CEGUI::Window* WidgetImageButton23;

        CEGUI::Window* DefaultWindowInformation;
        CEGUI::Window* StaticTextSourceName;
        CEGUI::Window* StaticTextSourceVersion;
        CEGUI::Window* StaticTextAuthor;
        CEGUI::Window* StaticTextPlayerNumber;
        CEGUI::Window* StaticTextIntroduction;

        CEGUI::Window* LabelIndex;

        CEGUI::Editbox* pEditboxBasename;
    public:
        struct mmString hTextBaseNameHost;
    public:
        struct mmEmulatorAssets hEmulatorAssets;
    public:
        struct mmExplorerItemEmu hItemEmu;
    public:
        double hTimerInterval;
        double hTimerSwitch;
        size_t hSnapshotIndex;
    public:
        Ogre::TexturePtr hOgreTexture;
        // strong ref.
        CEGUI::BasicImage* pImage;
        // strong ref.
        CEGUI::CEGUIOgreTexture* pCEGUIOgreTexture;
        CEGUI::String hImageryIcon;
        CEGUI::String hImageryName;
        CEGUI::String hTextureName;
    public:
        struct mmIconvContext pIconvContextInformation;
    public:
        mmLayerEmulatorWindow hLayerEmulatorWindow;
    public:
        mmLayerUtilityScrollbarStaticText hScrollbar;
    public:
        // mm_event_handler d_event_window_size_changed_conn;
    public:
        CEGUI::Event::Connection hEventEmulatorPlayConn;
        CEGUI::Event::Connection hEventEmulatorStopConn;
    public:
        mmLayerExplorerDetailsEmu(const CEGUI::String& type, const CEGUI::String& name);
        virtual ~mmLayerExplorerDetailsEmu(void);
    public:
        // mmLayerContext
        virtual void OnFinishLaunching(void);
        virtual void OnBeforeTerminate(void);
    public:
        // mmLayerExplorerDetailsBase
        virtual void UpdateLayerValue(void);
        virtual void UpdateUtf8View(void);
    public:
        // mmLayerExplorerDetailsBase
        virtual void OnEnterBackground(void);
        virtual void OnEnterForeground(void);
    public:
        // CEGUI::Window
        virtual void notifyScreenAreaChanged(bool recursive = true);
    private:
        void UpdateInformation(void);

        void UpdateIconvContextInformation(void);
        void UpdateUtf8InformationView(void);

        void UpdateImageAttributes(void);

        void ProduceSnapshotTexture(void);
        void RecycleSnapshotTexture(void);

        void RefreshSnapshotTexture(void);
        
        void CreateEmuResourcesGroup(void);
        void DeleteEmuResourcesGroup(void);
    private:
        void EmulatorPreviewPlay(void);
        void EmulatorPreviewPause(void);
        void EmulatorPreviewStop(void);
    private:
        bool OnEventWindowSizeChanged(const mmEventArgs& args);
    private:
        bool OnWidgetImageButton00EventMouseClick(const CEGUI::EventArgs& args);
        bool OnWidgetImageButton10EventMouseClick(const CEGUI::EventArgs& args);
        bool OnWidgetImageButton11EventMouseClick(const CEGUI::EventArgs& args);
        bool OnWidgetImageButton12EventMouseClick(const CEGUI::EventArgs& args);
        bool OnWidgetImageButton20EventMouseClick(const CEGUI::EventArgs& args);
        bool OnWidgetImageButton21EventMouseClick(const CEGUI::EventArgs& args);
        bool OnWidgetImageButton22EventMouseClick(const CEGUI::EventArgs& args);
        bool OnWidgetImageButton23EventMouseClick(const CEGUI::EventArgs& args);
    private:
        bool OnEditboxBasenameEventTextAccepted(const CEGUI::EventArgs& args);
    private:
        bool OnStaticImageIconEventUpdated(const CEGUI::EventArgs& args);
        bool OnStaticImageIconEventMouseClick(const CEGUI::EventArgs& args);
    private:
        bool OnLayerEmulatorMachineEventEmulatorPlay(const CEGUI::EventArgs& args);
        bool OnLayerEmulatorMachineEventEmulatorStop(const CEGUI::EventArgs& args);
    };
}

#endif//__mmLayerExplorerDetailsEmu_h__
