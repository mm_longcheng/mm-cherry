/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerExplorerDetailsBase.h"

#include "CEGUI/WindowManager.h"

#include "layer/utility/mmLayerUtilityIconv.h"

#include "layer/common/mmLayerCommonEnsureCode.h"
#include "layer/common/mmLayerCommonEnsureEvent.h"

#include "layer/director/mmLayerDirector.h"

#include "explorer/mmExplorerCommand.h"
#include "explorer/mmExplorerItem.h"

#include "toolkit/mmIconv.h"

#include "module/mmModuleLogger.h"

namespace mm
{
    const CEGUI::String mmLayerExplorerDetailsBase::EventNamespace = "mm";
    const CEGUI::String mmLayerExplorerDetailsBase::WidgetTypeName = "mm/mmLayerExplorerDetailsBase";

    mmLayerExplorerDetailsBase::mmLayerExplorerDetailsBase(const CEGUI::String& type, const CEGUI::String& name)
        : mmLayerContext(type, name)

        , pExplorerCommand(NULL)
        , pExplorerFinder(NULL)
        , pExplorerImage(NULL)

        , pIconvContext(NULL)
        , pItem(NULL)
        , pItemSelect(NULL)

        , pRandmon(NULL)
    {
        mmString_Init(&this->hDirectory);
    }
    mmLayerExplorerDetailsBase::~mmLayerExplorerDetailsBase(void)
    {
        mmString_Destroy(&this->hDirectory);
    }
    void mmLayerExplorerDetailsBase::OnFinishLaunching(void)
    {

    }

    void mmLayerExplorerDetailsBase::OnBeforeTerminate(void)
    {

    }
    void mmLayerExplorerDetailsBase::SetDirectory(const char* pDirectory)
    {
        mmString_Assigns(&this->hDirectory, pDirectory);
    }
    void mmLayerExplorerDetailsBase::SetItem(struct mmExplorerItem* pItem)
    {
        this->pItem = pItem;
    }
    void mmLayerExplorerDetailsBase::SetItemSelect(struct mmRbtsetVpt* pItemSelect)
    {
        this->pItemSelect = pItemSelect;
    }
    void mmLayerExplorerDetailsBase::SetIconvContext(struct mmIconvContext* pIconvContext)
    {
        this->pIconvContext = pIconvContext;
    }
    void mmLayerExplorerDetailsBase::SetExplorerCommand(struct mmExplorerCommand* pExplorerCommand)
    {
        this->pExplorerCommand = pExplorerCommand;
    }
    void mmLayerExplorerDetailsBase::SetExplorerFinder(struct mmExplorerFinder* pExplorerFinder)
    {
        this->pExplorerFinder = pExplorerFinder;
    }
    void mmLayerExplorerDetailsBase::SetExplorerImage(struct mmExplorerImage* pExplorerImage)
    {
        this->pExplorerImage = pExplorerImage;
    }
    void mmLayerExplorerDetailsBase::SetXoshiroRandmon(struct mmXoshiro256starstar* pRandmon)
    {
        this->pRandmon = pRandmon;
    }
    void mmLayerExplorerDetailsBase::UpdateLayerValue(void)
    {
        // do nothing here.
    }
    void mmLayerExplorerDetailsBase::UpdateUtf8View(void)
    {
        // do nothing here.
    }
    void mmLayerExplorerDetailsBase::RandomEmbellishImage(CEGUI::Window* pImageVindow)
    {
        mmUInt64_t n = 0;
        mmUInt32_t v = 0;
        char n_string[64] = { 0 };

        n = mmXoshiro256starstar_Next(this->pRandmon);

        v = n % 4;

        mmSprintf(n_string, "ImagesetExplorer/embellish_%u", v);

        pImageVindow->setProperty("Image", n_string);
    }
    void mmLayerExplorerDetailsBase::OnEnterBackground(void)
    {
        // do nothing here.
    }
    void mmLayerExplorerDetailsBase::OnEnterForeground(void)
    {
        // do nothing here.
    }
    void mmLayerExplorerDetailsBase::RemoveItemLayerEnsure(RemoveEnsureCallback hCallback)
    {
        mmLayerContext* w = mmLayerDirector_LayerAdd(this->pDirector, "mm/mmLayerCommonEnsureCode", "mmLayerExplorerDetailsCommonEnsureCode");
        mmLayerCommonEnsureCode* pLayerEnsure = (mmLayerCommonEnsureCode*)w;
        // 0x00000001 delete assets.
        pLayerEnsure->SetDescriptionErrorcode(0x00000001);
        pLayerEnsure->RandomConfirmCode();
        pLayerEnsure->StartTimerTerminate();

        assert(false == pLayerEnsure->hEventChoiceConn.isValid() || false == pLayerEnsure->hEventChoiceConn->connected() && "we must detach_handle after event handle.");

        pLayerEnsure->hEventChoiceConn = pLayerEnsure->subscribeEvent(
            mmLayerCommonEnsureCode::EventChoice, CEGUI::Event::Subscriber(hCallback, this));
    }
    void mmLayerExplorerDetailsBase::LoggerView(mmUInt32_t lvl, mmUInt32_t code, const char* desc, const char* view)
    {
        mm::mmModule* pModule = &pContextMaster->hModule;
        mm::mmModuleLogger* pModuleLogger = pModule->GetData<mm::mmModuleLogger>();

        pModuleLogger->LogView(lvl, code, desc, view);
    }
    bool mmLayerExplorerDetailsBase::OnLayerEnsureEventChoiceRmdir(const CEGUI::EventArgs& args)
    {
        const mmChoiceEventArgs& evt = (const mmChoiceEventArgs&)(args);
        mmLayerCommonEnsureCode* pLayerEnsure = (mmLayerCommonEnsureCode*)evt.window;
        if (mmChoiceEventArgs::ENSURE == evt.hCode)
        {
            mmExplorerCommand_Rmdir(this->pExplorerCommand, mmString_CStr(&this->pItem->fullname));
            mmExplorerCommand_Refresh(this->pExplorerCommand, mmString_CStr(&this->hDirectory));
        }
        return true;
    }
    bool mmLayerExplorerDetailsBase::OnLayerEnsureEventChoiceRemove(const CEGUI::EventArgs& args)
    {
        const mmChoiceEventArgs& evt = (const mmChoiceEventArgs&)(args);
        mmLayerCommonEnsureCode* pLayerEnsure = (mmLayerCommonEnsureCode*)evt.window;
        if (mmChoiceEventArgs::ENSURE == evt.hCode)
        {
            mmExplorerCommand_Remove(this->pExplorerCommand, mmString_CStr(&this->pItem->fullname));
            mmExplorerCommand_Refresh(this->pExplorerCommand, mmString_CStr(&this->hDirectory));
        }
        return true;
    }
    bool mmLayerExplorerDetailsBase::OnLayerEnsureEventChoiceRemoveVector(const CEGUI::EventArgs& args)
    {
        const mmChoiceEventArgs& evt = (const mmChoiceEventArgs&)(args);

        if (mmChoiceEventArgs::ENSURE == evt.hCode)
        {
            mmExplorerCommand_RemoveVector(this->pExplorerCommand, this->pItemSelect);
            mmExplorerCommand_Refresh(this->pExplorerCommand, mmString_CStr(&this->hDirectory));
        }
        return true;
    }
}
