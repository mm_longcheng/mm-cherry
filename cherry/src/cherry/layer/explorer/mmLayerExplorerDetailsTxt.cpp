/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerExplorerDetailsTxt.h"

#include "layer/common/mmLayerCommonIconvEncode.h"

#include "CEGUI/WindowManager.h"
#include "CEGUI/widgets/Editbox.h"

#include "explorer/mmExplorerCommand.h"

#include "layer/utility/mmLayerUtilityIconv.h"

#include "layer/text/mmLayerTextView.h"
#include "layer/text/mmLayerTextWindow.h"

#include "layer/director/mmLayerDirector.h"

namespace mm
{
    const CEGUI::String mmLayerExplorerDetailsTxt::EventNamespace = "mm";
    const CEGUI::String mmLayerExplorerDetailsTxt::WidgetTypeName = "mm/mmLayerExplorerDetailsTxt";

    const size_t mmLayerExplorerDetailsTxt::FILE_CACHE_BUFFER_LENGTH = 1024;

    mmLayerExplorerDetailsTxt::mmLayerExplorerDetailsTxt(const CEGUI::String& type, const CEGUI::String& name)
        : mmLayerExplorerDetailsBase(type, name)
        , LayerExplorerDetailsTxt(NULL)
        , StaticImageBackground(NULL)

        , EditboxBasename(NULL)
        , StaticImageIcon(NULL)
        , StaticTextPreview(NULL)

        , WidgetImageButton00(NULL)
        , WidgetImageButton10(NULL)
        , WidgetImageButton11(NULL)
        , WidgetImageButton12(NULL)
        , WidgetImageButton20(NULL)

        , LayerCommonIconvEncodeWindow(NULL)

        , pEditboxBasename(NULL)

        , pCommonIconvEncode(NULL)

        , hTextIndex(0)
        , hTextLineSize(20)
        , hTextLineIndex(0)
    {
        mmString_Init(&this->hTextBaseNameHost);
        mmStreambuf_Init(&this->hStreambuf);
        mmIconvContext_Init(&this->hIconvContextText);
        mmVectorVpt_Init(&this->hItemVector);

        mmIconvContext_Cachelist(&this->hIconvContextText);
        mmIconvContext_UpdateFormatLocale(&this->hIconvContextText, "utf-8");
    }
    mmLayerExplorerDetailsTxt::~mmLayerExplorerDetailsTxt(void)
    {
        mmString_Destroy(&this->hTextBaseNameHost);
        mmStreambuf_Destroy(&this->hStreambuf);
        mmIconvContext_Destroy(&this->hIconvContextText);
        mmVectorVpt_Destroy(&this->hItemVector);
    }
    void mmLayerExplorerDetailsTxt::OnFinishLaunching(void)
    {
        struct mmSurfaceMaster* pSurfaceMaster = this->pSurfaceMaster;

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        this->LayerExplorerDetailsTxt = pWindowManager->loadLayoutFromFile("explorer/LayerExplorerDetailsTxt.layout");
        this->addChild(this->LayerExplorerDetailsTxt);
        // Manual set whole area.
        CEGUI::URect hWholeArea(CEGUI::UDim(0, 0), CEGUI::UDim(0, 0), CEGUI::UDim(1, 0), CEGUI::UDim(1, 0));
        this->LayerExplorerDetailsTxt->setArea(hWholeArea);

        this->StaticImageBackground = this->LayerExplorerDetailsTxt->getChild("StaticImageBackground");

        this->EditboxBasename = this->StaticImageBackground->getChild("EditboxBasename");
        this->StaticImageIcon = this->StaticImageBackground->getChild("StaticImageIcon");
        this->StaticTextPreview = this->StaticImageBackground->getChild("StaticTextPreview");

        this->WidgetImageButton00 = this->StaticImageBackground->getChild("WidgetImageButton00");
        this->WidgetImageButton10 = this->StaticImageBackground->getChild("WidgetImageButton10");
        this->WidgetImageButton11 = this->StaticImageBackground->getChild("WidgetImageButton11");
        this->WidgetImageButton12 = this->StaticImageBackground->getChild("WidgetImageButton12");
        this->WidgetImageButton20 = this->StaticImageBackground->getChild("WidgetImageButton20");

        this->LayerCommonIconvEncodeWindow = this->StaticImageBackground->getChild("LayerCommonIconvEncodeWindow");

        this->WidgetImageButton00->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsTxt::OnWidgetImageButton00EventMouseClick, this));
        this->WidgetImageButton10->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsTxt::OnWidgetImageButton10EventMouseClick, this));
        this->WidgetImageButton11->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsTxt::OnWidgetImageButton11EventMouseClick, this));
        this->WidgetImageButton12->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsTxt::OnWidgetImageButton12EventMouseClick, this));
        this->WidgetImageButton20->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsTxt::OnWidgetImageButton20EventMouseClick, this));

        this->pEditboxBasename = (CEGUI::Editbox*)this->EditboxBasename;
        this->pCommonIconvEncode = (mmLayerCommonIconvEncode*)this->LayerCommonIconvEncodeWindow;

        this->pEditboxBasename->subscribeEvent(CEGUI::Editbox::EventTextAccepted, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsTxt::OnEditboxBasenameEventTextAccepted, this));

        // most of time the text format will be "utf-8".
        mmIconvContext_UpdateFormat(&this->hIconvContextText, "utf-8", "utf-8");

        this->pCommonIconvEncode->SetDirector(this->pDirector);
        this->pCommonIconvEncode->SetContextMaster(this->pContextMaster);
        this->pCommonIconvEncode->SetSurfaceMaster(this->pSurfaceMaster);
        this->pCommonIconvEncode->SetIconvContext(&this->hIconvContextText);
        this->pCommonIconvEncode->OnFinishLaunching();
        //
        this->LayerCommonIconvEncodeWindow->subscribeEvent(mmLayerCommonIconvEncode::EventIconvCoderChanged, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsTxt::OnLayerCommonIconvEncodeWindowEventIconvCoderChanged, this));

        this->hScrollbar.SetWindow(this->StaticTextPreview);
        this->hScrollbar.OnFinishLaunching();
    }

    void mmLayerExplorerDetailsTxt::OnBeforeTerminate(void)
    {
        struct mmSurfaceMaster* pSurfaceMaster = this->pSurfaceMaster;

        this->hScrollbar.OnBeforeTerminate();

        this->pCommonIconvEncode->OnBeforeTerminate();

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        this->removeChild(this->LayerExplorerDetailsTxt);
        pWindowManager->destroyWindow(this->LayerExplorerDetailsTxt);
        this->LayerExplorerDetailsTxt = NULL;
    }
    void mmLayerExplorerDetailsTxt::UpdateLayerValue(void)
    {
        this->UpdateUtf8View();

        this->RefreshPreviewText();
    }
    void mmLayerExplorerDetailsTxt::UpdateUtf8View(void)
    {
        struct mmIconvContext* pIconvContext = this->pIconvContext;

        mmLayerUtilityIconv_ViewName(pIconvContext, this->EditboxBasename, &this->pItem->basename);
    }
    void mmLayerExplorerDetailsTxt::RefreshPreviewView(void)
    {
        struct mmIconvContext* pIconvContext = &this->hIconvContextText;

        const char* s = (const char*)(this->hStreambuf.buff + this->hStreambuf.gptr);

        struct mmString hWeakString;

        mmString_MakeWeak(&hWeakString, s);
        mmString_SetSizeValue(&hWeakString, this->hTextIndex);

        mmLayerUtilityIconv_ViewName(pIconvContext, this->StaticTextPreview, &hWeakString);
    }
    bool mmLayerExplorerDetailsTxt::AnalysePreviewText(void)
    {
        bool code = false;

        size_t sz = 0;

        while (this->hTextIndex < this->hStreambuf.pptr)
        {
            if (this->hStreambuf.buff[this->hTextIndex] == '\n')
            {
                this->hTextIndex++;

                this->hTextLineIndex++;

                sz = this->hTextIndex - this->hStreambuf.gptr;

                if ((this->hTextLineIndex >= this->hTextLineSize) ||
                    (this->hTextLineIndex >= 1 && sz >= FILE_CACHE_BUFFER_LENGTH))
                {
                    code = true;
                    break;
                }
            }
            else
            {
                this->hTextIndex++;
            }
        }

        return code;
    }
    void mmLayerExplorerDetailsTxt::RefreshPreviewText(void)
    {
        size_t n = 0;

        FILE* f = NULL;
        mmUInt8_t* buffer = NULL;
        size_t length = FILE_CACHE_BUFFER_LENGTH;
        bool enough = false;

        do
        {
            f = fopen(mmString_CStr(&this->pItem->fullname), "rb");

            if (NULL == f)
            {
                // can not open the file.
                break;
            }

            mmStreambuf_Reset(&this->hStreambuf);
            this->hTextIndex = this->hStreambuf.gptr;
            this->hTextLineIndex = 0;

            mmStreambuf_AlignedMemory(&this->hStreambuf, length);
            buffer = (this->hStreambuf.buff + this->hStreambuf.pptr);

            n = fread(buffer, 1, FILE_CACHE_BUFFER_LENGTH, f);
            while (0 < n && n <= FILE_CACHE_BUFFER_LENGTH)
            {
                mmStreambuf_Pbump(&this->hStreambuf, n);

                if (this->AnalysePreviewText())
                {
                    this->RefreshPreviewView();
                    enough = true;
                    break;
                }
                else
                {
                    mmStreambuf_AlignedMemory(&this->hStreambuf, length);
                    buffer = (this->hStreambuf.buff + this->hStreambuf.pptr);

                    n = fread(buffer, 1, FILE_CACHE_BUFFER_LENGTH, f);
                }
            }

            if (false == enough)
            {
                this->AnalysePreviewText();
                this->RefreshPreviewView();
            }

            mmStreambuf_Reset(&this->hStreambuf);

            fclose(f);
            f = NULL;
        } while (0);
    }
    bool mmLayerExplorerDetailsTxt::OnWidgetImageButton00EventMouseClick(const CEGUI::EventArgs& args)
    {
        struct mmIconvContext* pIconvContext = this->pIconvContext;
        mmLayerUtilityIconv_HostName(pIconvContext, this->EditboxBasename, &this->hTextBaseNameHost);

        mmExplorerCommand_RenameItem(this->pExplorerCommand, this->pItem, mmString_CStr(&this->hTextBaseNameHost));
        mmExplorerCommand_Refresh(this->pExplorerCommand, mmString_CStr(&this->hDirectory));

        static const char* message = "Rename File";
        this->LoggerView(MM_LOG_INFO, 0, message, message);
        return true;
    }
    bool mmLayerExplorerDetailsTxt::OnWidgetImageButton10EventMouseClick(const CEGUI::EventArgs& args)
    {
        mmExplorerCommand_Copy(this->pExplorerCommand, this->pItemSelect);
                
        static const char* message = "Copy File";
        this->LoggerView(MM_LOG_INFO, 0, message, message);
        return true;
    }
    bool mmLayerExplorerDetailsTxt::OnWidgetImageButton11EventMouseClick(const CEGUI::EventArgs& args)
    {
        mmExplorerCommand_Cut(this->pExplorerCommand, this->pItemSelect);

        static const char* message = "Cut File";
        this->LoggerView(MM_LOG_INFO, 0, message, message);
        return true;
    }
    bool mmLayerExplorerDetailsTxt::OnWidgetImageButton12EventMouseClick(const CEGUI::EventArgs& args)
    {
        mmLayerTextView* pLayerText = NULL;

        pLayerText = (mmLayerTextView*)mmLayerDirector_SceneExclusiveForeground(this->pDirector, "mm/mmLayerTextView", "mmLayerTextView");
        pLayerText->SetItem(this->pItem);

        if (pLayerText->hCurrentItemAssets != mmString_CStr(&this->pItem->fullname) ||
            mmLayerTextWindow::AC_closed == pLayerText->GetAnalyseCode())
        {
            pLayerText->ReloadAnalyse();

            pLayerText->hCurrentItemAssets = mmString_CStr(&this->pItem->fullname);
        }

        pLayerText->UpdateLayerValue();

        mmExplorerCommand_OpenReg(this->pExplorerCommand, this->pItem);
        return true;
    }
    bool mmLayerExplorerDetailsTxt::OnWidgetImageButton20EventMouseClick(const CEGUI::EventArgs& args)
    {
        this->RemoveItemLayerEnsure(&mmLayerExplorerDetailsBase::OnLayerEnsureEventChoiceRemove);
        return true;
    }
    bool mmLayerExplorerDetailsTxt::OnEditboxBasenameEventTextAccepted(const CEGUI::EventArgs& args)
    {
        this->OnWidgetImageButton00EventMouseClick(args);
        return true;
    }
    bool mmLayerExplorerDetailsTxt::OnLayerCommonIconvEncodeWindowEventIconvCoderChanged(const CEGUI::EventArgs& args)
    {
        this->RefreshPreviewView();
        return true;
    }
}
