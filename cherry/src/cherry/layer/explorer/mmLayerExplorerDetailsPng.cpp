/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerExplorerDetailsPng.h"

#include "CEGUI/WindowManager.h"
#include "CEGUI/ImageManager.h"
#include "CEGUI/Texture.h"
#include "CEGUI/widgets/Editbox.h"

#include "CEGUIOgreRenderer/Texture.h"

#include "OgreTextureManager.h"

#include "explorer/mmExplorerCommand.h"

#include "layer/utility/mmLayerUtilityIconv.h"

#include "layer/imagery/mmLayerImageryView.h"

#include "layer/director/mmLayerDirector.h"

namespace mm
{
    const CEGUI::String mmLayerExplorerDetailsPng::EventNamespace = "mm";
    const CEGUI::String mmLayerExplorerDetailsPng::WidgetTypeName = "mm/mmLayerExplorerDetailsPng";

    const std::string mmLayerExplorerDetailsPng::CEGUI_RTT_IMAGERY = "layer_explorer_details_png_cegui_rtt_imagery_";
    const std::string mmLayerExplorerDetailsPng::CEGUI_RTT_TEXTURE = "layer_explorer_details_png_cegui_rtt_texture_";

    mmLayerExplorerDetailsPng::mmLayerExplorerDetailsPng(const CEGUI::String& type, const CEGUI::String& name)
        : mmLayerExplorerDetailsBase(type, name)
        , LayerExplorerDetailsPng(NULL)
        , StaticImageBackground(NULL)

        , EditboxBasename(NULL)
        , StaticImageIcon(NULL)
        , StaticImagePreview(NULL)
        , StaticImageEmbellish(NULL)
        , StaticTextResolution(NULL)

        , WidgetImageButton00(NULL)
        , WidgetImageButton10(NULL)
        , WidgetImageButton11(NULL)
        , WidgetImageButton12(NULL)
        , WidgetImageButton20(NULL)

        , pEditboxBasename(NULL)

        , hOgreTexture()
        , pImage(NULL)
        , pCEGUIOgreTexture(NULL)
        , hImageryIcon("")
        , hImageryName("")
        , hTextureName("")

        , hMaxPreviewSize(8388608)
    {
        mmString_Init(&this->hTextBaseNameHost);
    }
    mmLayerExplorerDetailsPng::~mmLayerExplorerDetailsPng(void)
    {
        mmString_Destroy(&this->hTextBaseNameHost);
    }
    void mmLayerExplorerDetailsPng::OnFinishLaunching(void)
    {
        struct mmSurfaceMaster* pSurfaceMaster = this->pSurfaceMaster;
        mm::mmEventSet* pEventSet = &pSurfaceMaster->hEventSet;

        pEventSet->SubscribeEvent(mmSurfaceMaster_EventWindowSizeChanged, &mmLayerExplorerDetailsPng::OnEventWindowSizeChanged, this);

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        this->LayerExplorerDetailsPng = pWindowManager->loadLayoutFromFile("explorer/LayerExplorerDetailsPng.layout");
        this->addChild(this->LayerExplorerDetailsPng);
        // Manual set whole area.
        CEGUI::URect hWholeArea(CEGUI::UDim(0, 0), CEGUI::UDim(0, 0), CEGUI::UDim(1, 0), CEGUI::UDim(1, 0));
        this->LayerExplorerDetailsPng->setArea(hWholeArea);

        this->StaticImageBackground = this->LayerExplorerDetailsPng->getChild("StaticImageBackground");

        this->EditboxBasename = this->StaticImageBackground->getChild("EditboxBasename");
        this->StaticImageIcon = this->StaticImageBackground->getChild("StaticImageIcon");
        this->StaticImagePreview = this->StaticImageBackground->getChild("StaticImagePreview");
        this->StaticImageEmbellish = this->StaticImageBackground->getChild("StaticImageEmbellish");
        this->StaticTextResolution = this->StaticImageBackground->getChild("StaticTextResolution");

        this->WidgetImageButton00 = this->StaticImageBackground->getChild("WidgetImageButton00");
        this->WidgetImageButton10 = this->StaticImageBackground->getChild("WidgetImageButton10");
        this->WidgetImageButton11 = this->StaticImageBackground->getChild("WidgetImageButton11");
        this->WidgetImageButton12 = this->StaticImageBackground->getChild("WidgetImageButton12");
        this->WidgetImageButton20 = this->StaticImageBackground->getChild("WidgetImageButton20");

        this->WidgetImageButton00->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsPng::OnWidgetImageButton00EventMouseClick, this));
        this->WidgetImageButton10->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsPng::OnWidgetImageButton10EventMouseClick, this));
        this->WidgetImageButton11->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsPng::OnWidgetImageButton11EventMouseClick, this));
        this->WidgetImageButton12->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsPng::OnWidgetImageButton12EventMouseClick, this));
        this->WidgetImageButton20->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsPng::OnWidgetImageButton20EventMouseClick, this));

        this->pEditboxBasename = (CEGUI::Editbox*)this->EditboxBasename;

        this->pEditboxBasename->subscribeEvent(CEGUI::Editbox::EventTextAccepted, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsPng::OnEditboxBasenameEventTextAccepted, this));

        this->hImageryIcon = this->StaticImagePreview->getProperty("Image");
        this->hImageryName = CEGUI_RTT_IMAGERY + this->getName();
        this->hTextureName = CEGUI_RTT_TEXTURE + this->getName();
        //
        struct mmCEGUISystem* pCEGUISystem = &this->pContextMaster->hCEGUISystem;
        CEGUI::CEGUIOgreRenderer* pCEGUIOgreRenderer = pCEGUISystem->pRenderer;
        //
        this->pCEGUIOgreTexture = (CEGUI::CEGUIOgreTexture*)&pCEGUIOgreRenderer->createTexture(this->hTextureName);
        //
        CEGUI::ImageManager* pImageManager = CEGUI::ImageManager::getSingletonPtr();
        this->pImage = static_cast<CEGUI::BasicImage*>(&pImageManager->create("BasicImage", this->hImageryName));
        //
        struct mmAdaptiveSize* pAdaptiveSize = &pSurfaceMaster->hAdaptiveSize;
        Ogre::uint hPixelW = (Ogre::uint)(pAdaptiveSize->hActuallySafeRect[2]);
        Ogre::uint hPixelH = (Ogre::uint)(pAdaptiveSize->hActuallySafeRect[3]);

        this->pImage->setNativeResolution(CEGUI::Sizef((float)hPixelW, (float)hPixelH));
        this->pImage->setAutoScaled(CEGUI::ASM_Min);
    }

    void mmLayerExplorerDetailsPng::OnBeforeTerminate(void)
    {
        struct mmSurfaceMaster* pSurfaceMaster = this->pSurfaceMaster;
        mm::mmEventSet* pEventSet = &pSurfaceMaster->hEventSet;

        this->RecyclePreviewTexture();

        CEGUI::ImageManager* pImageManager = CEGUI::ImageManager::getSingletonPtr();
        pImageManager->destroy(*this->pImage);
        this->pImage = NULL;
        //
        struct mmCEGUISystem* pCEGUISystem = &this->pContextMaster->hCEGUISystem;
        CEGUI::CEGUIOgreRenderer* pCEGUIOgreRenderer = pCEGUISystem->pRenderer;
        //
        pCEGUIOgreRenderer->destroyTexture(*this->pCEGUIOgreTexture);
        this->pCEGUIOgreTexture = NULL;

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        this->removeChild(this->LayerExplorerDetailsPng);
        pWindowManager->destroyWindow(this->LayerExplorerDetailsPng);
        this->LayerExplorerDetailsPng = NULL;

        pEventSet->UnsubscribeEvent(mmSurfaceMaster_EventWindowSizeChanged, &mmLayerExplorerDetailsPng::OnEventWindowSizeChanged, this);
    }
    void mmLayerExplorerDetailsPng::UpdateLayerValue(void)
    {
        this->UpdateUtf8View();

        this->RefreshPreviewTexture();
    }
    void mmLayerExplorerDetailsPng::UpdateUtf8View(void)
    {
        struct mmIconvContext* pIconvContext = this->pIconvContext;

        mmLayerUtilityIconv_ViewName(pIconvContext, this->EditboxBasename, &this->pItem->basename);
    }
    void mmLayerExplorerDetailsPng::UpdateImageAttributes(void)
    {
        struct mmSurfaceMaster* pSurfaceMaster = this->pSurfaceMaster;

        struct mmAdaptiveSize* pAdaptiveSize = &pSurfaceMaster->hAdaptiveSize;
        Ogre::uint hPixelW = (Ogre::uint)(pAdaptiveSize->hActuallySafeRect[2]);
        Ogre::uint hPixelH = (Ogre::uint)(pAdaptiveSize->hActuallySafeRect[3]);
        
        const CEGUI::Rectf& _rectf = this->StaticImagePreview->getOuterRectClipper();
        float _win_w = _rectf.getWidth();
        float _win_h = _rectf.getHeight();
        _win_w = _win_w < 1 ? 1 : _win_w;
        _win_h = _win_h < 1 ? 1 : _win_h;

        const CEGUI::Sizef& _tex_size = this->pCEGUIOgreTexture->getSize();

        float _tex_real_w = (float)_tex_size.d_width;
        float _tex_real_h = (float)_tex_size.d_height;

        float _native_resolution_w = _tex_real_w * (float)hPixelW / _win_w;
        float _native_resolution_h = _tex_real_h * (float)hPixelH / _win_h;

        CEGUI::Rectf _imageArea(0, 0, _tex_real_w, _tex_real_h);
        CEGUI::Sizef _imageNativeResolution(_native_resolution_w, _native_resolution_h);
        this->pImage->setArea(_imageArea);
        this->pImage->setNativeResolution(_imageNativeResolution);
        this->pImage->setAutoScaled(CEGUI::ASM_Min);
    }
    void mmLayerExplorerDetailsPng::ProducePreviewTexture(void)
    {
        Ogre::TextureManager* pTextureManager = Ogre::TextureManager::getSingletonPtr();

        assert(!this->hOgreTexture && "you must destroy ogre texture before load the new one.");

        this->hOgreTexture = pTextureManager->load(mmString_CStr(&this->pItem->basename), mmString_CStr(&this->pItem->pathname));

        Ogre::uint32 _w = this->hOgreTexture->getWidth();
        Ogre::uint32 _h = this->hOgreTexture->getHeight();

        CEGUI::Rectf _texture_area(0, 0, (float)_w, (float)_h);

        // weak reference mode.
        this->pCEGUIOgreTexture->setOgreTexture(this->hOgreTexture, _texture_area);

        this->pImage->setTexture(this->pCEGUIOgreTexture);

        this->UpdateImageAttributes();
    }
    void mmLayerExplorerDetailsPng::RecyclePreviewTexture(void)
    {
        if (this->hOgreTexture)
        {
            Ogre::TextureManager* pTextureManager = Ogre::TextureManager::getSingletonPtr();

            pTextureManager->remove(this->hOgreTexture);
            this->hOgreTexture.reset();

            CEGUI::Rectf _texture_area(0, 0, 0, 0);
            this->pCEGUIOgreTexture->setOgreTexture(this->hOgreTexture, _texture_area);
        }
    }
    void mmLayerExplorerDetailsPng::RefreshPreviewTexture(void)
    {
        if (this->hMaxPreviewSize >= this->pItem->s.st_size)
        {
            this->StaticImagePreview->setProperty("Image", this->hImageryName);

            this->RecyclePreviewTexture();
            this->ProducePreviewTexture();

            char _tex_resolution_string[64] = { 0 };
            const CEGUI::Sizef& _tex_size = this->pCEGUIOgreTexture->getSize();
            mmSprintf(_tex_resolution_string, "%ux%u", (mmUInt32_t)_tex_size.d_width, (mmUInt32_t)_tex_size.d_height);

            this->StaticTextResolution->setText(_tex_resolution_string);

            this->StaticImagePreview->setVisible(true);
            this->StaticImageEmbellish->setVisible(false);
        }
        else
        {
            this->StaticImagePreview->setProperty("Image", this->hImageryIcon);

            this->StaticTextResolution->setText("???x???");

            this->StaticImagePreview->setVisible(false);
            this->StaticImageEmbellish->setVisible(true);
        }
    }
    bool mmLayerExplorerDetailsPng::OnEventWindowSizeChanged(const mmEventArgs& args)
    {
        this->UpdateImageAttributes();
        return true;
    }
    bool mmLayerExplorerDetailsPng::OnWidgetImageButton00EventMouseClick(const CEGUI::EventArgs& args)
    {
        struct mmIconvContext* pIconvContext = this->pIconvContext;
        mmLayerUtilityIconv_HostName(pIconvContext, this->EditboxBasename, &this->hTextBaseNameHost);

        mmExplorerCommand_RenameItem(this->pExplorerCommand, this->pItem, mmString_CStr(&this->hTextBaseNameHost));
        mmExplorerCommand_Refresh(this->pExplorerCommand, mmString_CStr(&this->hDirectory));

        static const char* message = "Rename File";
        this->LoggerView(MM_LOG_INFO, 0, message, message);
        return true;
    }
    bool mmLayerExplorerDetailsPng::OnWidgetImageButton10EventMouseClick(const CEGUI::EventArgs& args)
    {
        mmExplorerCommand_Copy(this->pExplorerCommand, this->pItemSelect);

        static const char* message = "Copy File";
        this->LoggerView(MM_LOG_INFO, 0, message, message);
        return true;
    }
    bool mmLayerExplorerDetailsPng::OnWidgetImageButton11EventMouseClick(const CEGUI::EventArgs& args)
    {
        mmExplorerCommand_Cut(this->pExplorerCommand, this->pItemSelect);

        static const char* message = "Cut File";
        this->LoggerView(MM_LOG_INFO, 0, message, message);
        return true;
    }
    bool mmLayerExplorerDetailsPng::OnWidgetImageButton12EventMouseClick(const CEGUI::EventArgs& args)
    {
        mmLayerImageryView* pLayerImagery = NULL;

        pLayerImagery = (mmLayerImageryView*)mmLayerDirector_SceneExclusiveForeground(this->pDirector, "mm/mmLayerImageryView", "mmLayerImageryView");
        pLayerImagery->SetItem(this->pItem);
        pLayerImagery->UpdateLayerValue();

        mmExplorerCommand_OpenReg(this->pExplorerCommand, this->pItem);
        return true;
    }
    bool mmLayerExplorerDetailsPng::OnWidgetImageButton20EventMouseClick(const CEGUI::EventArgs& args)
    {
        this->RemoveItemLayerEnsure(&mmLayerExplorerDetailsBase::OnLayerEnsureEventChoiceRemove);
        return true;
    }
    bool mmLayerExplorerDetailsPng::OnEditboxBasenameEventTextAccepted(const CEGUI::EventArgs& args)
    {
        this->OnWidgetImageButton00EventMouseClick(args);
        return true;
    }
}
