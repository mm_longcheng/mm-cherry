/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerExplorerDetailsEmpty.h"

#include "random/mmXoshiro.h"

#include "CEGUI/WindowManager.h"
#include "CEGUI/widgets/Editbox.h"

#include "toolkit/mmIconv.h"

#include "explorer/mmExplorerCommand.h"
#include "explorer/mmExplorerItem.h"

#include "layer/utility/mmLayerUtilityIconv.h"

namespace mm
{
    const CEGUI::String mmLayerExplorerDetailsEmpty::EventNamespace = "mm";
    const CEGUI::String mmLayerExplorerDetailsEmpty::WidgetTypeName = "mm/mmLayerExplorerDetailsEmpty";

    mmLayerExplorerDetailsEmpty::mmLayerExplorerDetailsEmpty(const CEGUI::String& type, const CEGUI::String& name)
        : mmLayerExplorerDetailsBase(type, name)
        , LayerExplorerDetailsEmpty(NULL)
        , StaticImageBackground(NULL)

        , EditboxBasename(NULL)
        , EditboxBasepath(NULL)
        , StaticImageEmbellish(NULL)

        , WidgetImageButton00(NULL)
        , WidgetImageButton10(NULL)
        , WidgetImageButton20(NULL)

        , pEditboxBasename(NULL)
        , pEditboxBasepath(NULL)
    {
        mmString_Init(&this->hTextBaseNameHost);
        mmString_Init(&this->hTextBasePathHost);
    }
    mmLayerExplorerDetailsEmpty::~mmLayerExplorerDetailsEmpty(void)
    {
        mmString_Destroy(&this->hTextBaseNameHost);
        mmString_Destroy(&this->hTextBasePathHost);
    }
    void mmLayerExplorerDetailsEmpty::OnFinishLaunching(void)
    {
        struct mmSurfaceMaster* pSurfaceMaster = this->pSurfaceMaster;

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        this->LayerExplorerDetailsEmpty = pWindowManager->loadLayoutFromFile("explorer/LayerExplorerDetailsEmpty.layout");
        this->addChild(this->LayerExplorerDetailsEmpty);
        // Manual set whole area.
        CEGUI::URect hWholeArea(CEGUI::UDim(0, 0), CEGUI::UDim(0, 0), CEGUI::UDim(1, 0), CEGUI::UDim(1, 0));
        this->LayerExplorerDetailsEmpty->setArea(hWholeArea);

        this->StaticImageBackground = this->LayerExplorerDetailsEmpty->getChild("StaticImageBackground");

        this->EditboxBasename = this->StaticImageBackground->getChild("EditboxBasename");
        this->EditboxBasepath = this->StaticImageBackground->getChild("EditboxBasepath");
        this->StaticImageEmbellish = this->StaticImageBackground->getChild("StaticImageEmbellish");

        this->WidgetImageButton00 = this->StaticImageBackground->getChild("WidgetImageButton00");
        this->WidgetImageButton10 = this->StaticImageBackground->getChild("WidgetImageButton10");
        this->WidgetImageButton20 = this->StaticImageBackground->getChild("WidgetImageButton20");

        this->WidgetImageButton00->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsEmpty::OnWidgetImageButton00EventMouseClick, this));
        this->WidgetImageButton10->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsEmpty::OnWidgetImageButton10EventMouseClick, this));
        this->WidgetImageButton20->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsEmpty::OnWidgetImageButton20EventMouseClick, this));

        this->pEditboxBasename = (CEGUI::Editbox*)this->EditboxBasename;
        this->pEditboxBasepath = (CEGUI::Editbox*)this->EditboxBasepath;

        this->pEditboxBasename->subscribeEvent(CEGUI::Editbox::EventTextAccepted, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsEmpty::OnEditboxBasenameEventTextAccepted, this));
        this->pEditboxBasepath->subscribeEvent(CEGUI::Editbox::EventTextAccepted, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsEmpty::OnEditboxBasepathEventTextAccepted, this));
    }

    void mmLayerExplorerDetailsEmpty::OnBeforeTerminate(void)
    {
        struct mmSurfaceMaster* pSurfaceMaster = this->pSurfaceMaster;

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        this->removeChild(this->LayerExplorerDetailsEmpty);
        pWindowManager->destroyWindow(this->LayerExplorerDetailsEmpty);
        this->LayerExplorerDetailsEmpty = NULL;
    }
    void mmLayerExplorerDetailsEmpty::UpdateLayerValue(void)
    {
        this->RandomEmbellishImage(this->StaticImageEmbellish);
    }
    void mmLayerExplorerDetailsEmpty::UpdateUtf8View(void)
    {
        // do nothing here.
    }
    bool mmLayerExplorerDetailsEmpty::OnWidgetImageButton00EventMouseClick(const CEGUI::EventArgs& args)
    {
        struct mmIconvContext* pIconvContext = this->pIconvContext;
        mmLayerUtilityIconv_HostName(pIconvContext, this->EditboxBasename, &this->hTextBaseNameHost);

        std::string hRealUrl;
        hRealUrl = hRealUrl + mmString_CStr(&this->hDirectory) + "/" + mmString_CStr(&this->hTextBaseNameHost);
        mmExplorerCommand_Touch(this->pExplorerCommand, hRealUrl.c_str());
        mmExplorerCommand_Refresh(this->pExplorerCommand, mmString_CStr(&this->hDirectory));

        static const char* message = "Touch File";
        this->LoggerView(MM_LOG_INFO, 0, message, message);
        return true;
    }
    bool mmLayerExplorerDetailsEmpty::OnWidgetImageButton10EventMouseClick(const CEGUI::EventArgs& args)
    {
        struct mmIconvContext* pIconvContext = this->pIconvContext;
        mmLayerUtilityIconv_HostName(pIconvContext, this->EditboxBasepath, &this->hTextBasePathHost);

        std::string hRealUrl;
        hRealUrl = hRealUrl + mmString_CStr(&this->hDirectory) + "/" + mmString_CStr(&this->hTextBasePathHost);
        mmExplorerCommand_Mkdir(this->pExplorerCommand, hRealUrl.c_str(), 00776);
        mmExplorerCommand_Refresh(this->pExplorerCommand, mmString_CStr(&this->hDirectory));

        static const char* message = "Make Directory";
        this->LoggerView(MM_LOG_INFO, 0, message, message);
        return true;
    }
    bool mmLayerExplorerDetailsEmpty::OnWidgetImageButton20EventMouseClick(const CEGUI::EventArgs& args)
    {
        mmExplorerCommand_Paste(this->pExplorerCommand, mmString_CStr(&this->hDirectory));
        mmExplorerCommand_Refresh(this->pExplorerCommand, mmString_CStr(&this->hDirectory));

        static const char* message = "Paste File";
        this->LoggerView(MM_LOG_INFO, 0, message, message);
        return true;
    }
    bool mmLayerExplorerDetailsEmpty::OnEditboxBasenameEventTextAccepted(const CEGUI::EventArgs& args)
    {
        this->OnWidgetImageButton00EventMouseClick(args);
        return true;
    }
    bool mmLayerExplorerDetailsEmpty::OnEditboxBasepathEventTextAccepted(const CEGUI::EventArgs& args)
    {
        this->OnWidgetImageButton10EventMouseClick(args);
        return true;
    }
}
