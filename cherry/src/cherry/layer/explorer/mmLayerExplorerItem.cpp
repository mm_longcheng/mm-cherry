/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerExplorerItem.h"

#include "layer/widgets/mmWidgetTransform.h"

#include "CEGUI/WindowManager.h"
#include "CEGUI/widgets/Listbox.h"

#include "CEGUIOgreRenderer/Texture.h"
#include "CEGUIOgreRenderer/GeometryBuffer.h"

#include "CEGUI/TplWindowRendererProperty.h"

#include "explorer/mmExplorerItem.h"
#include "explorer/mmExplorerImage.h"

#include "layer/utility/mmLayerUtilityIconv.h"

namespace mm
{
    const CEGUI::String mmLayerExplorerItem::EventNamespace = "mm";
    const CEGUI::String mmLayerExplorerItem::WidgetTypeName = "mm/mmLayerExplorerItem";

    const CEGUI::String mmLayerExplorerItem::EventLayerItemMouseClick = "EventLayerItemMouseClick";
    const CEGUI::String mmLayerExplorerItem::EventLayerItemImageMouseClick = "EventLayerItemImageMouseClick";

    mmLayerExplorerItem::mmLayerExplorerItem(const CEGUI::String& type, const CEGUI::String& name)
        : mmLayerContext(type, name)
        , LayerExplorerItem(NULL)
        , StaticImageBackground(NULL)

        , StaticImageType(NULL)
        , StaticTextBasename(NULL)
        , StaticTextAuthority(NULL)
        , StaticTextMTime(NULL)
        , StaticTextSize(NULL)

        , hIndex(0)

        , pItem(NULL)
        , pIconvContext(NULL)
        , pExplorerImage(NULL)

        , hColourLustre(0.0f, 0.0f, 1.0f, 1.0f)
        , hColourSelect(0.0f, 1.0f, 0.0f, 1.0f)
        , hColourCommon(1.0f, 1.0f, 1.0f, 1.0f)
        , hIsSelect(false)
    {

    }
    mmLayerExplorerItem::~mmLayerExplorerItem(void)
    {

    }
    void mmLayerExplorerItem::OnFinishLaunching(void)
    {
        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        this->LayerExplorerItem = pWindowManager->loadLayoutFromFile("explorer/LayerExplorerItem.layout");
        this->addChild(this->LayerExplorerItem);

        // Design size.
        CEGUI::URect hArea = this->LayerExplorerItem->getArea();
        this->setArea(hArea);

        // Manual set whole area.
        CEGUI::URect hWholeArea(CEGUI::UDim(0, 0), CEGUI::UDim(0, 0), CEGUI::UDim(1, 0), CEGUI::UDim(1, 0));
        this->LayerExplorerItem->setArea(hWholeArea);

        this->StaticImageBackground = this->LayerExplorerItem->getChild("StaticImageBackground");

        this->StaticImageType = this->StaticImageBackground->getChild("StaticImageType");
        this->StaticTextBasename = this->StaticImageBackground->getChild("StaticTextBasename");
        this->StaticTextAuthority = this->StaticImageBackground->getChild("StaticTextAuthority");
        this->StaticTextMTime = this->StaticImageBackground->getChild("StaticTextMTime");
        this->StaticTextSize = this->StaticImageBackground->getChild("StaticTextSize");

        this->hItemEventMouseClickConn = this->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerExplorerItem::OnLayerItemEventMouseClick, this));
        this->hTypeEventMouseClickConn = this->StaticImageType->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerExplorerItem::OnLayerTypeEventMouseClick, this));
    }
    void mmLayerExplorerItem::OnBeforeTerminate(void)
    {
        this->hItemEventMouseClickConn->disconnect();
        this->hTypeEventMouseClickConn->disconnect();

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        this->removeChild(this->LayerExplorerItem);
        pWindowManager->destroyWindow(this->LayerExplorerItem);
        this->LayerExplorerItem = NULL;
    }
    void mmLayerExplorerItem::SetIndex(size_t hIndex)
    {
        this->hIndex = hIndex;
    }
    void mmLayerExplorerItem::SetItem(struct mmExplorerItem* pItem)
    {
        this->pItem = pItem;
    }
    void mmLayerExplorerItem::SetIconvContext(struct mmIconvContext* pIconvContext)
    {
        this->pIconvContext = pIconvContext;
    }
    void mmLayerExplorerItem::SetExplorerImage(struct mmExplorerImage* pExplorerImage)
    {
        this->pExplorerImage = pExplorerImage;
    }
    void mmLayerExplorerItem::SetIsSelect(bool hIsSelect)
    {
        CEGUI::Colour _colour_0;
        CEGUI::String _colour_0_string;

        CEGUI::Colour _colour_1;
        CEGUI::String _colour_1_string;

        this->hIsSelect = hIsSelect;

        _colour_0 = this->hIsSelect ? this->hColourSelect : this->hColourCommon;
        _colour_0_string = CEGUI::PropertyHelper<CEGUI::Colour>::toString(_colour_0);

        _colour_1 = this->hIsSelect ? this->hColourLustre : this->hColourCommon;
        _colour_1_string = CEGUI::PropertyHelper<CEGUI::Colour>::toString(_colour_1);

        this->StaticImageBackground->setProperty("FrameColours", _colour_0_string);

        this->StaticImageType->setProperty("FrameColours", _colour_1_string);

        this->StaticTextBasename->setProperty("FrameColours", _colour_0_string);
        this->StaticTextAuthority->setProperty("FrameColours", _colour_0_string);
        this->StaticTextMTime->setProperty("FrameColours", _colour_0_string);
        this->StaticTextSize->setProperty("FrameColours", _colour_0_string);

        this->StaticTextBasename->setProperty("TextColours", _colour_0_string);
        this->StaticTextAuthority->setProperty("TextColours", _colour_0_string);
        this->StaticTextMTime->setProperty("TextColours", _colour_0_string);
        this->StaticTextSize->setProperty("TextColours", _colour_0_string);
    }
    void mmLayerExplorerItem::UpdateLayerValue(void)
    {
        char mode_string[16] = { 0 };
        char hTimeString[64] = { 0 };
        char size_string[32] = { 0 };

        mmTm_t tm;
        struct mmTimeInfo tp;

        this->StaticImageType->setProperty("Image", mmString_CStr(&this->pItem->type_image));

        this->UpdateUtf8View();

        mmExplorerItem_ModeString(this->pItem, mode_string);
        this->StaticTextAuthority->setText(mode_string);

        tp.sec = this->pItem->s.st_mtime;
        tp.msec = 0;
        mmTime_Tm(&tp, &tm);
        // 2000/12/11 12:33
        mmSprintf(hTimeString, "%4d/%02d/%02d %02d:%02d",
            tm.mmTm_year, tm.mmTm_mon,
            tm.mmTm_mday, tm.mmTm_hour,
            tm.mmTm_min);

        this->StaticTextMTime->setText(hTimeString);

        mmExplorerItem_SizeString(this->pItem, size_string);
        this->StaticTextSize->setText(size_string);
    }
    void mmLayerExplorerItem::UpdateUtf8View(void)
    {
        mmLayerUtilityIconv_ViewName(this->pIconvContext, this->StaticTextBasename, &this->pItem->basename);
    }
    void mmLayerExplorerItem::OnFinishAttach(void)
    {
        // need do nothing here.
    }
    void mmLayerExplorerItem::OnBeforeDetach(void)
    {
        // need do nothing here.
    }
    bool mmLayerExplorerItem::OnLayerItemEventMouseClick(const CEGUI::EventArgs& args)
    {
        this->SetIsSelect(!this->hIsSelect);

        CEGUI::WindowEventArgs _args(this);
        this->fireEvent(EventLayerItemMouseClick, _args, EventNamespace);
        return true;
    }
    bool mmLayerExplorerItem::OnLayerTypeEventMouseClick(const CEGUI::EventArgs& args)
    {
        this->SetIsSelect(!this->hIsSelect);

        CEGUI::WindowEventArgs _args(this);
        this->fireEvent(EventLayerItemImageMouseClick, _args, EventNamespace);
        return true;
    }
}
