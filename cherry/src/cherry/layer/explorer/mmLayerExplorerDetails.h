/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmLayerExplorerDetails_h__
#define __mmLayerExplorerDetails_h__

#include "core/mmCore.h"

#include "random/mmXoshiro.h"

#include "container/mmRbtreeString.h"

#include "dish/mmEvent.h"

#include "layer/director/mmLayerContext.h"

#include "CEGUI/Window.h"

#include <string>
#include <map>

struct mmExplorerCommand;
struct mmExplorerFinder;
struct mmExplorerImage;
struct mmExplorerItem;

struct mmIconvContext;

namespace mm
{
    class mmLayerExplorerDetailsBase;

    class mmLayerExplorerDetails : public mmLayerContext
    {
    public:
        static const CEGUI::String EventNamespace;          //!< Namespace for global events
        static const CEGUI::String WidgetTypeName;          //!< Window factory name
    public:
        typedef std::map<std::string, std::string> item_layer_map_type;
        typedef std::map<std::string, mmLayerExplorerDetailsBase*> details_map_type;
    public:
        struct mmExplorerCommand* pExplorerCommand;
        struct mmExplorerFinder* pExplorerFinder;
        struct mmExplorerImage* pExplorerImage;
    public:
        // weak ref.
        struct mmIconvContext* pIconvContext;
        struct mmRbtsetVpt* pItemSelect;
    public:
        struct mmRbtreeStringString hItemLayerRbtree;
        // strong ref.
        struct mmRbtreeStringVpt hDetailsRbtree;
        // weak ref.
        mmLayerExplorerDetailsBase* pCurrentDetails;
    public:
        struct mmString hDirectory;
    public:
        struct mmXoshiro256starstar hRandmon;
    public:
        mmLayerExplorerDetails(const CEGUI::String& type, const CEGUI::String& name);
        virtual ~mmLayerExplorerDetails(void);
    public:
        // mmLayerContext
        virtual void OnFinishLaunching(void);
        virtual void OnBeforeTerminate(void);
    public:
        void SetDirectory(const char* pDirectory);
        void SetItemSelect(struct mmRbtsetVpt* pItemSelect);
        void SetIconvContext(struct mmIconvContext* pIconvContext);
        void SetExplorerCommand(struct mmExplorerCommand* pExplorerCommand);
        void SetExplorerFinder(struct mmExplorerFinder* pExplorerFinder);
        void SetExplorerImage(struct mmExplorerImage* pExplorerImage);
    public:
        void UpdateLayerValue(void);
        void UpdateUtf8View(void);
    private:
        void GetItemLayer(const char* pType, struct mmString* pLayer);
        void AddItemLayer(const char* pType, const char* pLayer);
        void RmvItemLayer(const char* pType);
        void ClearItemLayer(void);
    private:
        void RegisterDefaultItemLayer(void);
    private:
        mmLayerExplorerDetailsBase* AddItemDetails(const char* pLayer);
        mmLayerExplorerDetailsBase* GetItemDetails(const char* pLayer);
        mmLayerExplorerDetailsBase* GetItemDetailsInstance(const char* pLayer);
        void RmvItemDetails(const char* pLayer);
        void ClearItemDetails(void);
    };
}

#endif//__mmLayerExplorerDetails_h__