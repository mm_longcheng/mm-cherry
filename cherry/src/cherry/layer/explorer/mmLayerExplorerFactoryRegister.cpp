/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerExplorerFactoryRegister.h"
#include "layer/director/mmLayerDirector.h"
#include "CEGUI/WindowFactoryManager.h"

#include "mmLayerExplorerFinder.h"
#include "mmLayerExplorerDirectory.h"
#include "mmLayerExplorerItem.h"
#include "mmLayerExplorerEntry.h"
#include "mmLayerExplorerDetails.h"

#include "mmLayerExplorerDetailsDirectory.h"
#include "mmLayerExplorerDetailsEmpty.h"
#include "mmLayerExplorerDetailsMulti.h"
#include "mmLayerExplorerDetailsUnknown.h"
#include "mmLayerExplorerDetailsEmu.h"
#include "mmLayerExplorerDetailsNes.h"
#include "mmLayerExplorerDetailsPng.h"
#include "mmLayerExplorerDetailsTxt.h"
#include "mmLayerExplorerDetailsZip.h"

void mmLayerExplorerFactory_Register(struct mmLayerDirector* pDirector)
{
    CEGUI::WindowFactoryManager* pWindowFactoryManager = CEGUI::WindowFactoryManager::getSingletonPtr();

    pWindowFactoryManager->addWindowType<mm::mmLayerExplorerFinder>();
    pWindowFactoryManager->addWindowType<mm::mmLayerExplorerDirectory>();
    pWindowFactoryManager->addWindowType<mm::mmLayerExplorerItem>();
    pWindowFactoryManager->addWindowType<mm::mmLayerExplorerEntry>();
    pWindowFactoryManager->addWindowType<mm::mmLayerExplorerDetails>();

    pWindowFactoryManager->addWindowType<mm::mmLayerExplorerDetailsDirectory>();
    pWindowFactoryManager->addWindowType<mm::mmLayerExplorerDetailsEmpty>();
    pWindowFactoryManager->addWindowType<mm::mmLayerExplorerDetailsMulti>();
    pWindowFactoryManager->addWindowType<mm::mmLayerExplorerDetailsUnknown>();
    pWindowFactoryManager->addWindowType<mm::mmLayerExplorerDetailsEmu>();
    pWindowFactoryManager->addWindowType<mm::mmLayerExplorerDetailsNes>();
    pWindowFactoryManager->addWindowType<mm::mmLayerExplorerDetailsPng>();
    pWindowFactoryManager->addWindowType<mm::mmLayerExplorerDetailsTxt>();
    pWindowFactoryManager->addWindowType<mm::mmLayerExplorerDetailsZip>();
}
void mmLayerExplorerFactory_Unregister(struct mmLayerDirector* pDirector)
{
    CEGUI::WindowFactoryManager* pWindowFactoryManager = CEGUI::WindowFactoryManager::getSingletonPtr();

    pWindowFactoryManager->removeFactory(mm::mmLayerExplorerFinder::WidgetTypeName);
    pWindowFactoryManager->removeFactory(mm::mmLayerExplorerDirectory::WidgetTypeName);
    pWindowFactoryManager->removeFactory(mm::mmLayerExplorerItem::WidgetTypeName);
    pWindowFactoryManager->removeFactory(mm::mmLayerExplorerEntry::WidgetTypeName);
    pWindowFactoryManager->removeFactory(mm::mmLayerExplorerDetails::WidgetTypeName);

    pWindowFactoryManager->removeFactory(mm::mmLayerExplorerDetailsDirectory::WidgetTypeName);
    pWindowFactoryManager->removeFactory(mm::mmLayerExplorerDetailsEmpty::WidgetTypeName);
    pWindowFactoryManager->removeFactory(mm::mmLayerExplorerDetailsMulti::WidgetTypeName);
    pWindowFactoryManager->removeFactory(mm::mmLayerExplorerDetailsUnknown::WidgetTypeName);
    pWindowFactoryManager->removeFactory(mm::mmLayerExplorerDetailsEmu::WidgetTypeName);
    pWindowFactoryManager->removeFactory(mm::mmLayerExplorerDetailsNes::WidgetTypeName);
    pWindowFactoryManager->removeFactory(mm::mmLayerExplorerDetailsPng::WidgetTypeName);
    pWindowFactoryManager->removeFactory(mm::mmLayerExplorerDetailsTxt::WidgetTypeName);
    pWindowFactoryManager->removeFactory(mm::mmLayerExplorerDetailsZip::WidgetTypeName);
}
