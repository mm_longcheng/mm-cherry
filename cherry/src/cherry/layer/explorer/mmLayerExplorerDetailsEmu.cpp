/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerExplorerDetailsEmu.h"

#include "OgreTextureManager.h"

#include "CEGUI/WindowManager.h"
#include "CEGUI/ImageManager.h"
#include "CEGUI/Texture.h"
#include "CEGUI/widgets/Editbox.h"

#include "CEGUIOgreRenderer/Texture.h"

#include "OgreTextureManager.h"

#include "dish/mmFilePath.h"

#include "explorer/mmExplorerCommand.h"
#include "explorer/mmExplorerFinder.h"

#include "layer/director/mmLayerDirector.h"

#include "layer/utility/mmLayerUtilityIconv.h"

#include "layer/emulator/mmLayerEmulatorMachine.h"

#include "module/mmModuleLogger.h"

namespace mm
{
    const CEGUI::String mmLayerExplorerDetailsEmu::EventNamespace = "mm";
    const CEGUI::String mmLayerExplorerDetailsEmu::WidgetTypeName = "mm/mmLayerExplorerDetailsEmu";

    const std::string mmLayerExplorerDetailsEmu::CEGUI_RTT_IMAGERY = "LayerExplorerDetailsEmu_CEGUI_RTT_Imagery_";
    const std::string mmLayerExplorerDetailsEmu::CEGUI_RTT_TEXTURE = "LayerExplorerDetailsEmu_CEGUI_RTT_Texture_";

    mmLayerExplorerDetailsEmu::mmLayerExplorerDetailsEmu(const CEGUI::String& type, const CEGUI::String& name)
        : mmLayerExplorerDetailsBase(type, name)
        , LayerExplorerDetailsEmu(NULL)
        , StaticImageBackground(NULL)

        , EditboxBasename(NULL)
        , StaticImageIcon(NULL)
        , StaticImagePreview(NULL)

        , WidgetImageButton00(NULL)
        , WidgetImageButton10(NULL)
        , WidgetImageButton11(NULL)
        , WidgetImageButton12(NULL)
        , WidgetImageButton20(NULL)
        , WidgetImageButton21(NULL)
        , WidgetImageButton22(NULL)
        , WidgetImageButton23(NULL)

        , DefaultWindowInformation(NULL)
        , StaticTextSourceName(NULL)
        , StaticTextSourceVersion(NULL)
        , StaticTextAuthor(NULL)
        , StaticTextPlayerNumber(NULL)
        , StaticTextIntroduction(NULL)

        , LabelIndex(NULL)

        , pEditboxBasename(NULL)

        , hTimerInterval(0.0)
        , hTimerSwitch(3.0)
        , hSnapshotIndex(0)

        , hOgreTexture()
        , pImage(NULL)
        , pCEGUIOgreTexture(NULL)
        , hImageryIcon("")
        , hImageryName("")
        , hTextureName("")
    {
        mmString_Init(&this->hTextBaseNameHost);
        mmEmulatorAssets_Init(&this->hEmulatorAssets);
        mmExplorerItemEmu_Init(&this->hItemEmu);
        mmIconvContext_Init(&this->pIconvContextInformation);

        mmIconvContext_Cachelist(&this->pIconvContextInformation);
        mmIconvContext_UpdateFormatLocale(&this->pIconvContextInformation, "utf-8");
    }
    mmLayerExplorerDetailsEmu::~mmLayerExplorerDetailsEmu(void)
    {
        mmString_Destroy(&this->hTextBaseNameHost);
        mmEmulatorAssets_Destroy(&this->hEmulatorAssets);
        mmExplorerItemEmu_Destroy(&this->hItemEmu);
        mmIconvContext_Destroy(&this->pIconvContextInformation);
    }
    void mmLayerExplorerDetailsEmu::OnFinishLaunching(void)
    {
        struct mmSurfaceMaster* pSurfaceMaster = this->pSurfaceMaster;
        mm::mmEventSet* pEventSet = &pSurfaceMaster->hEventSet;

        pEventSet->SubscribeEvent(mmSurfaceMaster_EventWindowSizeChanged, &mmLayerExplorerDetailsEmu::OnEventWindowSizeChanged, this);

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        this->LayerExplorerDetailsEmu = pWindowManager->loadLayoutFromFile("explorer/LayerExplorerDetailsEmu.layout");
        this->addChild(this->LayerExplorerDetailsEmu);
        // Manual set whole area.
        CEGUI::URect hWholeArea(CEGUI::UDim(0, 0), CEGUI::UDim(0, 0), CEGUI::UDim(1, 0), CEGUI::UDim(1, 0));
        this->LayerExplorerDetailsEmu->setArea(hWholeArea);

        this->StaticImageBackground = this->LayerExplorerDetailsEmu->getChild("StaticImageBackground");

        this->EditboxBasename = this->StaticImageBackground->getChild("EditboxBasename");
        this->StaticImageIcon = this->StaticImageBackground->getChild("StaticImageIcon");
        this->StaticImagePreview = this->StaticImageBackground->getChild("StaticImagePreview");

        this->WidgetImageButton00 = this->StaticImageBackground->getChild("WidgetImageButton00");
        this->WidgetImageButton10 = this->StaticImageBackground->getChild("WidgetImageButton10");
        this->WidgetImageButton11 = this->StaticImageBackground->getChild("WidgetImageButton11");
        this->WidgetImageButton12 = this->StaticImageBackground->getChild("WidgetImageButton12");
        this->WidgetImageButton20 = this->StaticImageBackground->getChild("WidgetImageButton20");
        this->WidgetImageButton21 = this->StaticImageBackground->getChild("WidgetImageButton21");
        this->WidgetImageButton22 = this->StaticImageBackground->getChild("WidgetImageButton22");
        this->WidgetImageButton23 = this->StaticImageBackground->getChild("WidgetImageButton23");

        this->DefaultWindowInformation = this->StaticImageBackground->getChild("DefaultWindowInformation");
        this->StaticTextSourceName = this->DefaultWindowInformation->getChild("StaticTextSourceName");
        this->StaticTextSourceVersion = this->DefaultWindowInformation->getChild("StaticTextSourceVersion");
        this->StaticTextAuthor = this->DefaultWindowInformation->getChild("StaticTextAuthor");
        this->StaticTextPlayerNumber = this->DefaultWindowInformation->getChild("StaticTextPlayerNumber");
        this->StaticTextIntroduction = this->DefaultWindowInformation->getChild("StaticTextIntroduction");

        this->LabelIndex = this->StaticImageBackground->getChild("LabelIndex");

        this->WidgetImageButton00->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsEmu::OnWidgetImageButton00EventMouseClick, this));
        this->WidgetImageButton10->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsEmu::OnWidgetImageButton10EventMouseClick, this));
        this->WidgetImageButton11->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsEmu::OnWidgetImageButton11EventMouseClick, this));
        this->WidgetImageButton12->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsEmu::OnWidgetImageButton12EventMouseClick, this));
        this->WidgetImageButton20->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsEmu::OnWidgetImageButton20EventMouseClick, this));
        this->WidgetImageButton21->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsEmu::OnWidgetImageButton21EventMouseClick, this));
        this->WidgetImageButton22->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsEmu::OnWidgetImageButton22EventMouseClick, this));
        this->WidgetImageButton23->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsEmu::OnWidgetImageButton23EventMouseClick, this));

        this->pEditboxBasename = (CEGUI::Editbox*)this->EditboxBasename;

        this->pEditboxBasename->subscribeEvent(CEGUI::Editbox::EventTextAccepted, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsEmu::OnEditboxBasenameEventTextAccepted, this));

        this->StaticImageIcon->subscribeEvent(CEGUI::Window::EventUpdated, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsEmu::OnStaticImageIconEventUpdated, this));
        this->StaticImageIcon->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsEmu::OnStaticImageIconEventMouseClick, this));

        this->CreateEmuResourcesGroup();

        mmExplorerItemEmu_SetFileContext(&this->hItemEmu, &this->pExplorerFinder->file_context);

        this->hImageryIcon = this->StaticImageIcon->getProperty("Image");
        this->hImageryName = CEGUI_RTT_IMAGERY + this->getName();
        this->hTextureName = CEGUI_RTT_TEXTURE + this->getName();
        //
        struct mmCEGUISystem* pCEGUISystem = &this->pContextMaster->hCEGUISystem;
        CEGUI::CEGUIOgreRenderer* pCEGUIOgreRenderer = pCEGUISystem->pRenderer;
        //
        this->pCEGUIOgreTexture = (CEGUI::CEGUIOgreTexture*)&pCEGUIOgreRenderer->createTexture(this->hTextureName);
        //
        CEGUI::ImageManager* pImageManager = CEGUI::ImageManager::getSingletonPtr();
        this->pImage = static_cast<CEGUI::BasicImage*>(&pImageManager->create("BasicImage", this->hImageryName));
        //
        struct mmAdaptiveSize* pAdaptiveSize = &pSurfaceMaster->hAdaptiveSize;
        Ogre::uint hPixelW = (Ogre::uint)(pAdaptiveSize->hActuallySafeRect[2]);
        Ogre::uint hPixelH = (Ogre::uint)(pAdaptiveSize->hActuallySafeRect[3]);

        this->pImage->setNativeResolution(CEGUI::Sizef((float)hPixelW, (float)hPixelH));
        this->pImage->setAutoScaled(CEGUI::ASM_Min);

        mmLayerEmulatorMachine* pLayerEmulatorMachine = NULL;
        pLayerEmulatorMachine = (mmLayerEmulatorMachine*)mmLayerDirector_SceneExclusiveBackground(this->pDirector, "mm/mmLayerEmulatorMachine", "mmLayerEmulatorMachine");
        this->hLayerEmulatorWindow.SetEmulatorMachine(&pLayerEmulatorMachine->hEmulatorMachine);
        this->hLayerEmulatorWindow.SetLayerContext(this);
        this->hLayerEmulatorWindow.SetWindowScreen(this->StaticImagePreview);
        this->hLayerEmulatorWindow.SetFingerIsVisible(false);
        this->hLayerEmulatorWindow.OnFinishLaunching();
        //
        this->hEventEmulatorPlayConn = pLayerEmulatorMachine->subscribeEvent(mmLayerEmulatorMachine::EventEmulatorPlay, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsEmu::OnLayerEmulatorMachineEventEmulatorPlay, this));
        this->hEventEmulatorStopConn = pLayerEmulatorMachine->subscribeEvent(mmLayerEmulatorMachine::EventEmulatorStop, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsEmu::OnLayerEmulatorMachineEventEmulatorStop, this));

        this->hScrollbar.SetWindow(this->StaticTextIntroduction);
        this->hScrollbar.OnFinishLaunching();
    }

    void mmLayerExplorerDetailsEmu::OnBeforeTerminate(void)
    {
        struct mmSurfaceMaster* pSurfaceMaster = this->pSurfaceMaster;
        mm::mmEventSet* pEventSet = &pSurfaceMaster->hEventSet;
        
        this->hScrollbar.OnBeforeTerminate();

        this->RecycleSnapshotTexture();

        this->hEventEmulatorPlayConn->disconnect();
        this->hEventEmulatorStopConn->disconnect();
        //
        this->hLayerEmulatorWindow.OnBeforeTerminate();

        CEGUI::ImageManager* ImageManager = CEGUI::ImageManager::getSingletonPtr();
        ImageManager->destroy(*this->pImage);
        this->pImage = NULL;
        //
        struct mmCEGUISystem* pCEGUISystem = &this->pContextMaster->hCEGUISystem;
        CEGUI::CEGUIOgreRenderer* pCEGUIOgreRenderer = pCEGUISystem->pRenderer;
        //
        pCEGUIOgreRenderer->destroyTexture(*this->pCEGUIOgreTexture);
        this->pCEGUIOgreTexture = NULL;
        //
        this->DeleteEmuResourcesGroup();
        //
        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        this->removeChild(this->LayerExplorerDetailsEmu);
        pWindowManager->destroyWindow(this->LayerExplorerDetailsEmu);
        this->LayerExplorerDetailsEmu = NULL;

        pEventSet->UnsubscribeEvent(mmSurfaceMaster_EventWindowSizeChanged, &mmLayerExplorerDetailsEmu::OnEventWindowSizeChanged, this);
    }
    void mmLayerExplorerDetailsEmu::UpdateLayerValue(void)
    {
        mmExplorerItemEmu_SetPath(&this->hItemEmu, mmString_CStr(&this->pItem->pathname), mmString_CStr(&this->pItem->basename));
        
        this->DeleteEmuResourcesGroup();
        mmExplorerItemEmu_UpdateValue(&this->hItemEmu);
        this->CreateEmuResourcesGroup();

        this->UpdateInformation();

        this->UpdateUtf8View();

        this->hTimerInterval = 0.0;
        this->hSnapshotIndex = 0;
        this->RefreshSnapshotTexture();

        this->EmulatorPreviewPause();

        this->WidgetImageButton21->setVisible(true);
        this->WidgetImageButton22->setVisible(false);
        this->WidgetImageButton23->setVisible(false);

        this->StaticImagePreview->setVisible(false);
        this->DefaultWindowInformation->setVisible(true);

        this->UpdateIconvContextInformation();
        this->UpdateUtf8InformationView();
    }
    void mmLayerExplorerDetailsEmu::UpdateUtf8View(void)
    {
        struct mmIconvContext* pIconvContext = this->pIconvContext;

        mmLayerUtilityIconv_ViewName(pIconvContext, this->EditboxBasename, &this->pItem->basename);
    }
    void mmLayerExplorerDetailsEmu::OnEnterBackground(void)
    {
        this->EmulatorPreviewPause();
    }
    void mmLayerExplorerDetailsEmu::OnEnterForeground(void)
    {
        // do nothing here.
    }
    void mmLayerExplorerDetailsEmu::notifyScreenAreaChanged(bool recursive/*  = true */)
    {
        mmLayerContext::notifyScreenAreaChanged(recursive);
        this->hLayerEmulatorWindow.UpdateTransform();
    }
    void mmLayerExplorerDetailsEmu::UpdateInformation(void)
    {
        struct mmExplorerItemEmuInformation* pInformation = &this->hItemEmu.information;
        struct mmPackageAssets* pPackageAssets = &this->pContextMaster->hPackageAssets;

        const char* pExternalFilesDirectory = mmString_CStr(&pPackageAssets->hExternalFilesDirectory);

        mmEmulatorAssets_SetPath(&this->hEmulatorAssets, mmString_CStr(&this->pItem->pathname), mmString_CStr(&this->pItem->basename));
        mmEmulatorAssets_SetAssetsType(&this->hEmulatorAssets, MM_EMU_ASSETS_SOURCE);
        mmEmulatorAssets_SetRootPath(&this->hEmulatorAssets, mmString_CStr(&this->pItem->fullname));
        mmEmulatorAssets_SetAssetsName(&this->hEmulatorAssets, mmString_CStr(&pInformation->assets_name));
        mmEmulatorAssets_SetSourceName(&this->hEmulatorAssets, mmString_CStr(&pInformation->source_name));
        mmEmulatorAssets_SetWritable(&this->hEmulatorAssets, pExternalFilesDirectory, "emulator");
        mmEmulatorAssets_SetFileContext(&this->hEmulatorAssets, &this->pExplorerFinder->file_context);
    }
    void mmLayerExplorerDetailsEmu::UpdateIconvContextInformation(void)
    {
        struct mmExplorerItemEmuInformation* pInformation = &this->hItemEmu.information;

        struct mmIconvContext* pIconvContext = &this->pIconvContextInformation;

        const char* _coder_n_c_str = mmString_CStr(&pInformation->encoding);
        const char* _coder_o_c_str = mmString_CStr(&pIconvContext->hFormatHost);

        if (0 != mmStrcmp(_coder_n_c_str, _coder_o_c_str))
        {
            if (MM_TRUE == mmIconvContext_IsValidFormat(pIconvContext, _coder_n_c_str))
            {
                mmIconvContext_UpdateFormat(pIconvContext, _coder_n_c_str, "utf-8");
            }
            else
            {
                struct mmContextMaster* pContextMaster = this->pContextMaster;
                mm::mmModule* pModule = &pContextMaster->hModule;

                mm::mmModuleLogger* pModuleLogger = pModule->GetData<mm::mmModuleLogger>();

                char message[128] = { 0 };
                mmSprintf(message, "Iconv coder: %s is invalid.", _coder_n_c_str);
                pModuleLogger->LogView(MM_LOG_INFO, 0, message, message);
            }
        }
    }
    void mmLayerExplorerDetailsEmu::UpdateUtf8InformationView(void)
    {
        struct mmExplorerItemEmuInformation* pInformation = &this->hItemEmu.information;

        struct mmIconvContext* pIconvContext = &this->pIconvContextInformation;

        mmLayerUtilityIconv_ViewName(pIconvContext, this->StaticTextSourceName, &pInformation->source_name);
        mmLayerUtilityIconv_ViewName(pIconvContext, this->StaticTextSourceVersion, &pInformation->source_version);
        mmLayerUtilityIconv_ViewName(pIconvContext, this->StaticTextAuthor, &pInformation->author);
        mmLayerUtilityIconv_ViewName(pIconvContext, this->StaticTextPlayerNumber, &pInformation->player_number);
        mmLayerUtilityIconv_ViewName(pIconvContext, this->StaticTextIntroduction, &pInformation->introduction_string);
    }
    void mmLayerExplorerDetailsEmu::UpdateImageAttributes(void)
    {
        struct mmSurfaceMaster* pSurfaceMaster = this->pSurfaceMaster;

        struct mmAdaptiveSize* pAdaptiveSize = &pSurfaceMaster->hAdaptiveSize;
        Ogre::uint hPixelW = (Ogre::uint)(pAdaptiveSize->hActuallySafeRect[2]);
        Ogre::uint hPixelH = (Ogre::uint)(pAdaptiveSize->hActuallySafeRect[3]);

        const CEGUI::Rectf& _rectf = this->StaticImageIcon->getOuterRectClipper();
        float _win_w = _rectf.getWidth();
        float _win_h = _rectf.getHeight();
        _win_w = _win_w < 1 ? 1 : _win_w;
        _win_h = _win_h < 1 ? 1 : _win_h;

        const CEGUI::Sizef& _tex_size = this->pCEGUIOgreTexture->getSize();

        float _tex_real_w = (float)_tex_size.d_width;
        float _tex_real_h = (float)_tex_size.d_height;

        float _native_resolution_w = _tex_real_w * (float)hPixelW / _win_w;
        float _native_resolution_h = _tex_real_h * (float)hPixelH / _win_h;

        CEGUI::Rectf _imageArea(0, 0, _tex_real_w, _tex_real_h);
        CEGUI::Sizef _imageNativeResolution(_native_resolution_w, _native_resolution_h);
        this->pImage->setArea(_imageArea);
        this->pImage->setNativeResolution(_imageNativeResolution);
        this->pImage->setAutoScaled(CEGUI::ASM_Min);
    }
    void mmLayerExplorerDetailsEmu::ProduceSnapshotTexture(void)
    {
        Ogre::TextureManager* pTextureManager = Ogre::TextureManager::getSingletonPtr();

        struct mmFileAssetsInfo* pInfo = NULL;
        struct mmVectorValue* pVectorSnapshot = &this->hItemEmu.vector_snapshot;

        pInfo = (struct mmFileAssetsInfo*)mmVectorValue_GetIndexReference(pVectorSnapshot, this->hSnapshotIndex);

        assert(!this->hOgreTexture && "you must destroy ogre texture before load the new one.");

        this->hOgreTexture = pTextureManager->load(mmString_CStr(&pInfo->filename), mmString_CStr(&this->hItemEmu.fullname));

        Ogre::uint32 _w = this->hOgreTexture->getWidth();
        Ogre::uint32 _h = this->hOgreTexture->getHeight();

        CEGUI::Rectf _texture_area(0, 0, (float)_w, (float)_h);

        this->pCEGUIOgreTexture->setOgreTexture(this->hOgreTexture, _texture_area);

        this->pImage->setTexture(this->pCEGUIOgreTexture);

        this->UpdateImageAttributes();
    }
    void mmLayerExplorerDetailsEmu::RecycleSnapshotTexture(void)
    {
        if (this->hOgreTexture)
        {
            Ogre::TextureManager* pTextureManager = Ogre::TextureManager::getSingletonPtr();

            pTextureManager->remove(this->hOgreTexture);
            this->hOgreTexture.reset();

            CEGUI::Rectf _texture_area(0, 0, 0, 0);
            this->pCEGUIOgreTexture->setOgreTexture(this->hOgreTexture, _texture_area);
        }
    }
    void mmLayerExplorerDetailsEmu::RefreshSnapshotTexture(void)
    {
        struct mmVectorValue* pVectorValue = &this->hItemEmu.vector_snapshot;

        if (0 < pVectorValue->size)
        {
            char _index_string[64] = { 0 };

            this->StaticImageIcon->setProperty("Image", this->hImageryName);

            mmSprintf(_index_string, "%" PRIuPTR "/%" PRIuPTR "", this->hSnapshotIndex + 1, pVectorValue->size);
            this->LabelIndex->setText(_index_string);

            this->RecycleSnapshotTexture();
            this->ProduceSnapshotTexture();

            this->hSnapshotIndex++;
            this->hSnapshotIndex = this->hSnapshotIndex % pVectorValue->size;
        }
        else
        {
            this->StaticImageIcon->setProperty("Image", this->hImageryIcon);

            this->LabelIndex->setText("");
        }
    }
    void mmLayerExplorerDetailsEmu::CreateEmuResourcesGroup(void)
    {
        if(!mmString_Empty(&this->hItemEmu.fullname))
        {
            // ogre resource link.
            Ogre::ResourceGroupManager* pResourceGroupManager = Ogre::ResourceGroupManager::getSingletonPtr();
            Ogre::String _fullname(mmString_CStr(&this->hItemEmu.fullname));

            pResourceGroupManager->createResourceGroup(_fullname);
            pResourceGroupManager->addResourceLocation(".", "mmFileSystem", _fullname);
            pResourceGroupManager->initialiseResourceGroup(_fullname);

            typedef Ogre::ResourceGroupManager::LocationList LocationList;
            const LocationList& _LocationList = pResourceGroupManager->getResourceLocationList(_fullname);

            for (LocationList::const_iterator it = _LocationList.begin();
            it != _LocationList.end(); it++)
            {
                Ogre::ResourceGroupManager::ResourceLocation* e = (*it);
                mm::mmOgreFileSystemArchive* archive = (mm::mmOgreFileSystemArchive*)e->archive;
                archive->SetFileContext(this->hItemEmu.file_context);
            }
        }
    }
    void mmLayerExplorerDetailsEmu::DeleteEmuResourcesGroup(void)
    {
        if(!mmString_Empty(&this->hItemEmu.fullname))
        {
            // ogre resource unlink.
            Ogre::ResourceGroupManager* pResourceGroupManager = Ogre::ResourceGroupManager::getSingletonPtr();
            Ogre::String _fullname(mmString_CStr(&this->hItemEmu.fullname));

            if (pResourceGroupManager->resourceGroupExists(_fullname))
            {
                pResourceGroupManager->removeResourceLocation(".", _fullname);
                pResourceGroupManager->destroyResourceGroup(_fullname);
            }
        }
    }
    void mmLayerExplorerDetailsEmu::EmulatorPreviewPlay(void)
    {
        mmLayerEmulatorMachine* pLayerEmulatorMachine = NULL;

        struct mmEmulatorMachine* pEmulatorMachine = NULL;

        pLayerEmulatorMachine = (mmLayerEmulatorMachine*)mmLayerDirector_SceneExclusiveBackground(this->pDirector, "mm/mmLayerEmulatorMachine", "mmLayerEmulatorMachine");

        pEmulatorMachine = &pLayerEmulatorMachine->hEmulatorMachine;

        if (pLayerEmulatorMachine->hCurrentEmulatorAssets != mmString_CStr(&this->pItem->fullname) ||
            MM_SUCCESS != mmEmulatorMachine_GetRomLoadCode(pEmulatorMachine))
        {
            mmEmulatorMachine_SetAssets(pEmulatorMachine, &this->hEmulatorAssets);

            mmEmulatorMachine_LoadRom(pEmulatorMachine);
            mmEmulatorMachine_Play(pEmulatorMachine);

            pLayerEmulatorMachine->hCurrentEmulatorAssets = mmString_CStr(&this->pItem->fullname);

            this->hLayerEmulatorWindow.SetFingerIsVisible(true);
        }
        else
        {
            if (MM_EMULATOR_PAUSED == mmEmulatorMachine_GetState(pEmulatorMachine) &&
                MM_SUCCESS == mmEmulatorMachine_GetRomLoadCode(pEmulatorMachine))
            {
                this->hLayerEmulatorWindow.SetFingerIsVisible(true);

                mmEmulatorMachine_Resume(pEmulatorMachine);
            }
        }

        this->WidgetImageButton21->setVisible(false);
        this->WidgetImageButton22->setVisible(true);
        this->WidgetImageButton23->setVisible(true);

        this->StaticImagePreview->setVisible(true);
        this->DefaultWindowInformation->setVisible(false);
    }
    void mmLayerExplorerDetailsEmu::EmulatorPreviewPause(void)
    {
        mmLayerEmulatorMachine* pLayerEmulatorMachine = NULL;

        struct mmEmulatorMachine* pEmulatorMachine = NULL;

        pLayerEmulatorMachine = (mmLayerEmulatorMachine*)mmLayerDirector_SceneExclusiveBackground(this->pDirector, "mm/mmLayerEmulatorMachine", "mmLayerEmulatorMachine");

        pEmulatorMachine = &pLayerEmulatorMachine->hEmulatorMachine;

        if (MM_EMULATOR_PLAYING == mmEmulatorMachine_GetState(pEmulatorMachine) &&
            MM_SUCCESS == mmEmulatorMachine_GetRomLoadCode(pEmulatorMachine))
        {
            this->hLayerEmulatorWindow.SetFingerIsVisible(false);

            mmEmulatorMachine_Pause(pEmulatorMachine);
        }

        this->WidgetImageButton21->setVisible(true);
        this->WidgetImageButton22->setVisible(false);
    }
    void mmLayerExplorerDetailsEmu::EmulatorPreviewStop(void)
    {
        this->EmulatorPreviewPause();

        this->WidgetImageButton23->setVisible(false);

        this->StaticImagePreview->setVisible(false);
        this->DefaultWindowInformation->setVisible(true);
    }
    bool mmLayerExplorerDetailsEmu::OnEventWindowSizeChanged(const mmEventArgs& args)
    {
        this->UpdateImageAttributes();
        return true;
    }
    bool mmLayerExplorerDetailsEmu::OnWidgetImageButton00EventMouseClick(const CEGUI::EventArgs& args)
    {
        struct mmIconvContext* pIconvContext = this->pIconvContext;
        mmLayerUtilityIconv_HostName(pIconvContext, this->EditboxBasename, &this->hTextBaseNameHost);

        mmExplorerCommand_RenameItem(this->pExplorerCommand, this->pItem, mmString_CStr(&this->hTextBaseNameHost));
        mmExplorerCommand_Refresh(this->pExplorerCommand, mmString_CStr(&this->hDirectory));

        static const char* message = "Rename File";
        this->LoggerView(MM_LOG_INFO, 0, message, message);
        return true;
    }
    bool mmLayerExplorerDetailsEmu::OnWidgetImageButton10EventMouseClick(const CEGUI::EventArgs& args)
    {
        mmExplorerCommand_Copy(this->pExplorerCommand, this->pItemSelect);

        static const char* message = "Copy File";
        this->LoggerView(MM_LOG_INFO, 0, message, message);
        return true;
    }
    bool mmLayerExplorerDetailsEmu::OnWidgetImageButton11EventMouseClick(const CEGUI::EventArgs& args)
    {
        mmExplorerCommand_Cut(this->pExplorerCommand, this->pItemSelect);
 
        static const char* message = "Cut File";
        this->LoggerView(MM_LOG_INFO, 0, message, message);
        return true;
    }
    bool mmLayerExplorerDetailsEmu::OnWidgetImageButton12EventMouseClick(const CEGUI::EventArgs& args)
    {
        mmLayerEmulatorMachine* pLayerEmulatorMachine = NULL;
        struct mmEmulatorMachine* pEmulatorMachine = NULL;

        pLayerEmulatorMachine = (mmLayerEmulatorMachine*)mmLayerDirector_SceneExclusiveForeground(this->pDirector, "mm/mmLayerEmulatorMachine", "mmLayerEmulatorMachine");

        pEmulatorMachine = &pLayerEmulatorMachine->hEmulatorMachine;

        if (pLayerEmulatorMachine->hCurrentEmulatorAssets != mmString_CStr(&this->pItem->fullname) ||
            MM_SUCCESS != mmEmulatorMachine_GetRomLoadCode(pEmulatorMachine))
        {
            mmEmulatorMachine_SetAssets(pEmulatorMachine, &this->hEmulatorAssets);

            mmEmulatorMachine_LoadRom(pEmulatorMachine);
            mmEmulatorMachine_Play(pEmulatorMachine);

            pLayerEmulatorMachine->hCurrentEmulatorAssets = mmString_CStr(&this->pItem->fullname);

            this->WidgetImageButton21->setVisible(false);
            this->WidgetImageButton22->setVisible(true);
            this->WidgetImageButton23->setVisible(true);

            this->StaticImagePreview->setVisible(true);
            this->DefaultWindowInformation->setVisible(false);

            this->hLayerEmulatorWindow.SetFingerIsVisible(true);
        }
        // notice: 
        // when we open a emulator, we can only play or Resume at the rom not load.
        // because if the rom already load, the behaver is only switch the layer,
        // and can not play or Resume at all.

        pLayerEmulatorMachine->UpdateLayerValue();

        mmExplorerCommand_OpenReg(this->pExplorerCommand, this->pItem);

        return true;
    }
    bool mmLayerExplorerDetailsEmu::OnWidgetImageButton20EventMouseClick(const CEGUI::EventArgs& args)
    {
        this->RemoveItemLayerEnsure(&mmLayerExplorerDetailsBase::OnLayerEnsureEventChoiceRemove);
        return true;
    }
    bool mmLayerExplorerDetailsEmu::OnWidgetImageButton21EventMouseClick(const CEGUI::EventArgs& args)
    {
        // play
        this->EmulatorPreviewPlay();

        mmExplorerCommand_Preview(this->pExplorerCommand, this->pItem);
        return true;
    }
    bool mmLayerExplorerDetailsEmu::OnWidgetImageButton22EventMouseClick(const CEGUI::EventArgs& args)
    {
        // pause
        this->EmulatorPreviewPause();
        return true;
    }
    bool mmLayerExplorerDetailsEmu::OnWidgetImageButton23EventMouseClick(const CEGUI::EventArgs& args)
    {
        // stop
        this->EmulatorPreviewStop();
        return true;
    }
    bool mmLayerExplorerDetailsEmu::OnEditboxBasenameEventTextAccepted(const CEGUI::EventArgs& args)
    {
        this->OnWidgetImageButton00EventMouseClick(args);
        return true;
    }
    bool mmLayerExplorerDetailsEmu::OnStaticImageIconEventUpdated(const CEGUI::EventArgs& args)
    {
        const CEGUI::UpdateEventArgs& evt = (const CEGUI::UpdateEventArgs&)(args);

        this->hTimerInterval += evt.d_timeSinceLastFrame;

        if (this->hTimerInterval >= this->hTimerSwitch)
        {
            this->RefreshSnapshotTexture();
            this->hTimerInterval = 0.0;
        }

        return true;
    }
    bool mmLayerExplorerDetailsEmu::OnStaticImageIconEventMouseClick(const CEGUI::EventArgs& args)
    {
        this->RefreshSnapshotTexture();
        this->hTimerInterval = 0.0;
        return true;
    }
    bool mmLayerExplorerDetailsEmu::OnLayerEmulatorMachineEventEmulatorPlay(const CEGUI::EventArgs& args)
    {
        this->WidgetImageButton21->setVisible(false);
        this->WidgetImageButton22->setVisible(true);

        this->StaticImagePreview->setVisible(true);
        this->DefaultWindowInformation->setVisible(false);
        return true;
    }
    bool mmLayerExplorerDetailsEmu::OnLayerEmulatorMachineEventEmulatorStop(const CEGUI::EventArgs& args)
    {
        this->WidgetImageButton21->setVisible(true);
        this->WidgetImageButton22->setVisible(false);

        this->StaticImagePreview->setVisible(false);
        this->DefaultWindowInformation->setVisible(true);
        return true;
    }
}
