/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmLayerExplorerDirectory_h__
#define __mmLayerExplorerDirectory_h__

#include "core/mmCore.h"

#include "container/mmRbtsetVpt.h"

#include "dish/mmEvent.h"

#include "layer/director/mmLayerContext.h"

#include "emulator/mmEmulatorMachine.h"

#include "CEGUI/Window.h"

#include "layer/utility/mmLayerUtilityItemPool.h"

struct mmExplorerCommand;
struct mmExplorerFinder;
struct mmExplorerImage;

struct mmIconvContext;

#define mmLayerExplorerDirectory_RANKING_N1_NUMBER 7
#define mmLayerExplorerDirectory_RANKING_N2_NUMBER 14

namespace mm
{
    class mmWidgetScrollable;
    class mmLayerExplorerItem;

    class mmLayerExplorerDirectory : public mmLayerContext
    {
    public:
        static const CEGUI::String EventNamespace;          //!< Namespace for global events
        static const CEGUI::String WidgetTypeName;          //!< Window factory name
    public:
        static const CEGUI::String EventLayerItemMouseClick;
    public:
        // "($Home)"
        static const struct mmString HomeAliasName;
    public:
        CEGUI::Window* LayerExplorerDirectory;
        CEGUI::Window* StaticImageBackground;

        CEGUI::Window* WidgetImageButtonUp;
        CEGUI::Window* WidgetImageButtonRefresh;
        CEGUI::Window* WidgetScrollableYList;
        CEGUI::Window* WidgetComboboxUrl;

        mmWidgetScrollable* pWidgetScrollableYList;
        // weak ref.
        CEGUI::Combobox* pWidgetComboboxUrl;
        //
        CEGUI::Editbox* pWidgetComboboxUrlEditbox;
    public:
        struct mmExplorerFinder* pExplorerFinder;
        struct mmExplorerImage* pExplorerImage;
    public:
        struct mmIconvContext* pIconvContext;
    public:
        struct mmVectorValue hItemUrlVector;
    public:
        struct mmString hExplorerDirectory;
        struct mmString hExplorerDirectoryAliasName;
        struct mmString hTextUrlHost;
        struct mmString hTextUrlHostAliasName;
    public:
        struct mmLayerUtilityItemPool hItemPool;
    public:
        struct mmRbtsetVpt hLayerItemSelect;
        struct mmRbtsetVpt hItemSelect;
    private:
        CEGUI::Event::Connection hEventCursorMovedConn;
        CEGUI::Event::Connection hEventCursorBeganConn;
        CEGUI::Event::Connection hEventCursorEndedConn;
    public:
        mmLayerExplorerDirectory(const CEGUI::String& type, const CEGUI::String& name);
        virtual ~mmLayerExplorerDirectory(void);
    public:
        // mmLayerContext
        virtual void OnFinishLaunching(void);
        virtual void OnBeforeTerminate(void);
    public:
        void SetExplorerFinder(struct mmExplorerFinder* pExplorerFinder);
        void SetExplorerImage(struct mmExplorerImage* pExplorerImage);
        void SetIconvContext(struct mmIconvContext* pIconvContext);
    public:
        void SetExplorerDirectory(const char* pExplorerDirectory);
        const char* GetExplorerDirectory(void) const;
    public:
        void RefreshExplorerDirectory(void);
        void RefreshExplorerItemVectorView(void);
        void RefreshLayerItemEvent(void);
        void RefreshComboboxUrlLayer(void);
    public:
        void CalculationCurrentHostUrl(void);
        void CalculationHistoryExplorerDirectory(void);
    public:
        void CreateItemEvent(mmLayerExplorerItem* pLayerItem);
        void DeleteItemEvent(mmLayerExplorerItem* pLayerItem);
        void UpdateItemEvent(mmLayerExplorerItem* pLayerItem);
        void UpdateItemEventCurrent(void);
    public:
        void UpdateUtf8View(void);
        void UpdateUrlUtf8View(void);
        void UpdateUrlVectorUtf8View(void);
    public:
        void UpdateUrlComboboxView(void);
    public:
        void UnselectItemVector(void);
    private:
        void CreateComboboxUrlItem(void);
        void DeleteComboboxUrlItem(void);
        void UpdateComboboxUrlItem(void);
    private:
        static void EncodeAliasName(struct mmString* hAliasName, struct mmString* hPathName, struct mmString* hRealName, struct mmString* hViewName);
        static void DecodeAliasName(struct mmString* hAliasName, struct mmString* hPathName, struct mmString* hRealName, struct mmString* hViewName);
    private:
        bool OnLayerEventTouchsMoved(const CEGUI::EventArgs& args);
        bool OnLayerEventTouchsBegan(const CEGUI::EventArgs& args);
        bool OnLayerEventTouchsEnded(const CEGUI::EventArgs& args);
        bool OnLayerEventTouchsBreak(const CEGUI::EventArgs& args);
    private:
        bool OnHandleEventCursorMoved(const CEGUI::EventArgs& args);
        bool OnHandleEventCursorBegan(const CEGUI::EventArgs& args);
        bool OnHandleEventCursorEnded(const CEGUI::EventArgs& args);
    private:
        bool OnLayerItemEventLayerItemMouseClick(const CEGUI::EventArgs& args);
        bool OnLayerItemEventLayerItemImageMouseClick(const CEGUI::EventArgs& args);
    private:
        bool OnWidgetScrollableYListEventMouseClick(const CEGUI::EventArgs& args);
    private:
        bool OnButtonUpEventMouseClick(const CEGUI::EventArgs& args);
        bool OnButtonRefreshEventMouseClick(const CEGUI::EventArgs& args);
    private:
        bool OnWidgetComboboxUrlEventTextChanged(const CEGUI::EventArgs& args);
        bool OnWidgetComboboxUrlEventTextAccepted(const CEGUI::EventArgs& args);
        bool OnWidgetComboboxUrlEventListSelectionAccepted(const CEGUI::EventArgs& args);
    };
}

#endif//__mmLayerExplorerDirectory_h__
