/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerExplorerFinder.h"
#include "mmLayerExplorerDirectory.h"
#include "mmLayerExplorerDetails.h"
#include "mmLayerExplorerItem.h"

#include "CEGUI/WindowManager.h"
#include "CEGUI/widgets/Editbox.h"

#include "nwsi/mmEventObjectArgs.h"

#include "explorer/mmExplorerItem.h"
#include "explorer/mmExplorerImageEmu.h"

#include "module/mmModuleTookit.h"
#include "module/mmModuleExplorer.h"

#include "layer/director/mmLayerDirector.h"

#include "layer/common/mmLayerCommonEnsure.h"
#include "layer/common/mmLayerCommonEnsureEvent.h"
#include "layer/common/mmLayerCommonIconvEncode.h"

#include "database/mmDatabaseExplorer.h"

static void __static_LayerExplorerFinder_ExplorerCommandCallbackUrlOpenDir(void* obj, struct mmExplorerItem* _item)
{
    struct mmExplorerCommand* impl = (struct mmExplorerCommand*)(obj);
    mm::mmLayerExplorerFinder* p = (mm::mmLayerExplorerFinder*)(impl->callback.obj);
    p->OnEventItemOpenDir(_item);
}
static void __static_LayerExplorerFinder_ExplorerCommandCallbackUrlOpenReg(void* obj, struct mmExplorerItem* _item)
{
    struct mmExplorerCommand* impl = (struct mmExplorerCommand*)(obj);
    mm::mmLayerExplorerFinder* p = (mm::mmLayerExplorerFinder*)(impl->callback.obj);
    p->OnEventItemOpenReg(_item);
}
static void __static_LayerExplorerFinder_ExplorerCommandCallbackUrlPreview(void* obj, struct mmExplorerItem* _item)
{
    struct mmExplorerCommand* impl = (struct mmExplorerCommand*)(obj);
    mm::mmLayerExplorerFinder* p = (mm::mmLayerExplorerFinder*)(impl->callback.obj);
    p->OnEventItemPreview(_item);
}
static void __static_LayerExplorerFinder_ExplorerCommandCallbackStrRefresh(void* obj, const char* directory)
{
    struct mmExplorerCommand* impl = (struct mmExplorerCommand*)(obj);
    mm::mmLayerExplorerFinder* p = (mm::mmLayerExplorerFinder*)(impl->callback.obj);
    p->OnEventDirectoryRefresh(directory);
}
namespace mm
{
    const CEGUI::String mmLayerExplorerFinder::EventNamespace = "mm";
    const CEGUI::String mmLayerExplorerFinder::WidgetTypeName = "mm/mmLayerExplorerFinder";

    mmLayerExplorerFinder::mmLayerExplorerFinder(const CEGUI::String& type, const CEGUI::String& name)
        : mmLayerContext(type, name)
        , LayerExplorerFinder(NULL)
        , StaticImageBackground(NULL)

        , StaticTextFps(NULL)
        , WidgetImageButtonBack(NULL)
        , WidgetImageButtonHome(NULL)
        , LayerExplorerDirectoryDirectory(NULL)
        , LayerCommonIconvEncodeWindow(NULL)
        , LayerExplorerDetailsDetails(NULL)

        , pCommonIconvEncode(NULL)
        , pExplorerDirectory(NULL)
        , pExplorerDetails(NULL)
    {
        struct mmExplorerCommandCallback hCallback;

        mmExplorerCommand_Init(&this->pExplorerCommand);
        mmExplorerFinder_Init(&this->pExplorerFinder);
        mmExplorerImage_Init(&this->pExplorerImage);
        mmIconvContext_Init(&this->pIconvContext);

        hCallback.OpenDir = &__static_LayerExplorerFinder_ExplorerCommandCallbackUrlOpenDir;
        hCallback.OpenReg = &__static_LayerExplorerFinder_ExplorerCommandCallbackUrlOpenReg;
        hCallback.Preview = &__static_LayerExplorerFinder_ExplorerCommandCallbackUrlPreview;
        hCallback.Refresh = &__static_LayerExplorerFinder_ExplorerCommandCallbackStrRefresh;
        hCallback.obj = this;
        mmExplorerCommand_SetCallback(&this->pExplorerCommand, &hCallback);
    }
    mmLayerExplorerFinder::~mmLayerExplorerFinder(void)
    {
        mmExplorerCommand_Destroy(&this->pExplorerCommand);
        mmExplorerFinder_Destroy(&this->pExplorerFinder);
        mmExplorerImage_Destroy(&this->pExplorerImage);
        mmIconvContext_Destroy(&this->pIconvContext);
    }
    void mmLayerExplorerFinder::SetExplorerDirectory(const char* pExplorerDirectory)
    {
        assert(NULL != this->pExplorerDirectory && "NULL != this->pExplorerDirectory is invalid.");
        this->pExplorerDirectory->SetExplorerDirectory(pExplorerDirectory);
    }
    void mmLayerExplorerFinder::OnFinishLaunching(void)
    {
        struct mmContextMaster* pContextMaster = this->pContextMaster;
        struct mmSurfaceMaster* pSurfaceMaster = this->pSurfaceMaster;

        struct mmFrameScheduler* pFrameScheduler = &pSurfaceMaster->hFrameScheduler;

        mm::mmModule* pModule = &pContextMaster->hModule;

        mm::mmModuleExplorer* pModuleExplorer = pModule->GetData<mm::mmModuleExplorer>();

        pModuleExplorer->hEventSet.SubscribeEvent(mm::mmModuleExplorer::EventFormatUpdate, &mmLayerExplorerFinder::OnIconvContextEventFormatUpdate, this);

        mmIconvContext_Cachelist(&this->pIconvContext);
        mmIconvContext_UpdateFormat(&this->pIconvContext, mmString_CStr(&pModuleExplorer->hFormatHost), mmString_CStr(&pModuleExplorer->hFormatView));

        mmExplorerImage_RegisterDefaultImageMapper(&this->pExplorerImage);
        mmExplorerImage_AddSSFunc(&this->pExplorerImage, "-.zip", &mmExplorerImage_FuncEmulator);

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        this->LayerExplorerFinder = pWindowManager->loadLayoutFromFile("explorer/LayerExplorerFinder.layout");
        this->addChild(this->LayerExplorerFinder);

        this->StaticImageBackground = this->LayerExplorerFinder->getChild("StaticImageBackground");

        this->StaticTextFps = this->StaticImageBackground->getChild("StaticTextFps");
        this->WidgetImageButtonBack = this->StaticImageBackground->getChild("WidgetImageButtonBack");
        this->WidgetImageButtonHome = this->StaticImageBackground->getChild("WidgetImageButtonHome");
        this->LayerExplorerDirectoryDirectory = this->StaticImageBackground->getChild("LayerExplorerDirectoryDirectory");
        this->LayerCommonIconvEncodeWindow = this->StaticImageBackground->getChild("LayerCommonIconvEncodeWindow");
        this->LayerExplorerDetailsDetails = this->StaticImageBackground->getChild("LayerExplorerDetailsDetails");

        this->pCommonIconvEncode = (mmLayerCommonIconvEncode*)this->LayerCommonIconvEncodeWindow;
        this->pExplorerDirectory = (mmLayerExplorerDirectory*)this->LayerExplorerDirectoryDirectory;
        this->pExplorerDetails = (mmLayerExplorerDetails*)this->LayerExplorerDetailsDetails;

        // spacing common CEGUI::Window object, we need manual attach context event.
        this->AttachContextEvent(this->pCommonIconvEncode);
        this->AttachContextEvent(this->pExplorerDirectory);
        this->AttachContextEvent(this->pExplorerDetails);

        this->hUtilityFps.SetFrameTimer(&pFrameScheduler->frame_timer);
        this->hUtilityFps.SetWindow(this->StaticTextFps);
        this->hUtilityFps.OnFinishLaunching();

        this->pCommonIconvEncode->SetDirector(this->pDirector);
        this->pCommonIconvEncode->SetContextMaster(this->pContextMaster);
        this->pCommonIconvEncode->SetSurfaceMaster(this->pSurfaceMaster);
        this->pCommonIconvEncode->SetIconvContext(&this->pIconvContext);
        this->pCommonIconvEncode->OnFinishLaunching();

        this->pExplorerDirectory->SetDirector(this->pDirector);
        this->pExplorerDirectory->SetContextMaster(this->pContextMaster);
        this->pExplorerDirectory->SetSurfaceMaster(this->pSurfaceMaster);
        this->pExplorerDirectory->SetExplorerFinder(&this->pExplorerFinder);
        this->pExplorerDirectory->SetExplorerImage(&this->pExplorerImage);
        this->pExplorerDirectory->SetIconvContext(&this->pIconvContext);
        this->pExplorerDirectory->OnFinishLaunching();
        //
        this->pExplorerDetails->SetDirector(this->pDirector);
        this->pExplorerDetails->SetContextMaster(this->pContextMaster);
        this->pExplorerDetails->SetSurfaceMaster(this->pSurfaceMaster);
        this->pExplorerDetails->SetIconvContext(&this->pIconvContext);
        this->pExplorerDetails->SetExplorerCommand(&this->pExplorerCommand);
        this->pExplorerDetails->SetExplorerFinder(&this->pExplorerFinder);
        this->pExplorerDetails->SetExplorerImage(&this->pExplorerImage);
        this->pExplorerDetails->SetItemSelect(&this->pExplorerDirectory->hItemSelect);
        this->pExplorerDetails->OnFinishLaunching();

        this->pExplorerDetails->UpdateLayerValue();

        this->hEventLayerItemMouseClickConn = this->pExplorerDirectory->subscribeEvent(mmLayerExplorerDirectory::EventLayerItemMouseClick, CEGUI::Event::Subscriber(&mmLayerExplorerFinder::OnLayerItemEventLayerItemMouseClick, this));
        this->hEventIconvCoderChangedConn = this->LayerCommonIconvEncodeWindow->subscribeEvent(mmLayerCommonIconvEncode::EventIconvCoderChanged, CEGUI::Event::Subscriber(&mmLayerExplorerFinder::OnLayerCommonIconvEncodeWindowEventIconvCoderChanged, this));
        //
        this->hFpsEventMouseClickConn = this->StaticTextFps->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerExplorerFinder::OnStaticTextFpsEventMouseClick, this));
        this->hBackEventMouseClickConn = this->WidgetImageButtonBack->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerExplorerFinder::OnWidgetImageButtonBackEventMouseClick, this));
        this->hHomeEventMouseClickConn = this->WidgetImageButtonBack->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerExplorerFinder::OnWidgetImageButtonHomeEventMouseClick, this));
    }

    void mmLayerExplorerFinder::OnBeforeTerminate(void)
    {
        struct mmContextMaster* pContextMaster = this->pContextMaster;
        struct mmSurfaceMaster* pSurfaceMaster = this->pSurfaceMaster;
        mm::mmModule* pModule = &pContextMaster->hModule;

        mm::mmModuleExplorer* pModuleExplorer = pModule->GetData<mm::mmModuleExplorer>();

        this->hEventLayerItemMouseClickConn->disconnect();
        this->hEventIconvCoderChangedConn->disconnect();
        //
        this->hFpsEventMouseClickConn->disconnect();
        this->hBackEventMouseClickConn->disconnect();
        this->hHomeEventMouseClickConn->disconnect();

        // spacing common CEGUI::Window object, we need manual attach context event.
        this->DetachContextEvent(this->pCommonIconvEncode);
        this->DetachContextEvent(this->pExplorerDirectory);
        this->DetachContextEvent(this->pExplorerDetails);

        this->pCommonIconvEncode->OnBeforeTerminate();
        this->pExplorerDirectory->OnBeforeTerminate();
        this->pExplorerDetails->OnBeforeTerminate();

        this->pCommonIconvEncode = NULL;
        this->pExplorerDirectory = NULL;
        this->pExplorerDetails = NULL;

        this->hUtilityFps.OnBeforeTerminate();

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        this->removeChild(this->LayerExplorerFinder);
        pWindowManager->destroyWindow(this->LayerExplorerFinder);
        this->LayerExplorerFinder = NULL;

        pModuleExplorer->hEventSet.UnsubscribeEvent(mm::mmModuleExplorer::EventFormatUpdate, &mmLayerExplorerFinder::OnIconvContextEventFormatUpdate, this);
    }
    void mmLayerExplorerFinder::OnEventItemOpenDir(struct mmExplorerItem* _item)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogI(gLogger, "%s %d OpenDir: %s", __FUNCTION__, __LINE__, mmString_CStr(&_item->basename));
        this->pExplorerDirectory->SetExplorerDirectory(mmString_CStr(&_item->fullname));
    }
    void mmLayerExplorerFinder::OnEventItemOpenReg(struct mmExplorerItem* _item)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogI(gLogger, "%s %d OpenReg: %s", __FUNCTION__, __LINE__, mmString_CStr(&_item->basename));
    }
    void mmLayerExplorerFinder::OnEventItemPreview(struct mmExplorerItem* _item)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogI(gLogger, "%s %d Preview: %s", __FUNCTION__, __LINE__, mmString_CStr(&_item->basename));
    }
    void mmLayerExplorerFinder::OnEventDirectoryRefresh(const char* directory)
    {
        this->pExplorerDirectory->RefreshExplorerDirectory();
        this->pExplorerDirectory->RefreshLayerItemEvent();
    }
    bool mmLayerExplorerFinder::OnIconvContextEventFormatUpdate(const mmEventArgs& args)
    {
        const mmEventObjectArgs& evt = (const mmEventObjectArgs&)(args);
        mmModuleExplorer* pModuleExplorer = (mmModuleExplorer*)evt.pObject;
        mmIconvContext_UpdateFormat(&this->pIconvContext, mmString_CStr(&pModuleExplorer->hFormatHost), mmString_CStr(&pModuleExplorer->hFormatView));
        
        this->pCommonIconvEncode->UpdateLayerValue();
        this->pExplorerDirectory->UpdateUtf8View();
        this->pExplorerDetails->UpdateUtf8View();
        return true;
    }
    bool mmLayerExplorerFinder::OnLayerItemEventLayerItemMouseClick(const CEGUI::EventArgs& args)
    {
        const CEGUI::MouseEventArgs& evt = (const CEGUI::MouseEventArgs&)(args);

        this->pExplorerDetails->SetDirectory(mmString_CStr(&this->pExplorerDirectory->hExplorerDirectory));
        this->pExplorerDetails->UpdateLayerValue();

        return true;
    }
    bool mmLayerExplorerFinder::OnLayerCommonIconvEncodeWindowEventIconvCoderChanged(const CEGUI::EventArgs& args)
    {
        struct mmContextMaster* pContextMaster = this->pContextMaster;
        mm::mmModule* pModule = &pContextMaster->hModule;

        mm::mmModuleExplorer* pModuleExplorer = pModule->GetData<mm::mmModuleExplorer>();

        struct mmIconvContext* pIconvContext = &this->pIconvContext;

        const char* _coder_h_c_str = mmString_CStr(&pIconvContext->hFormatHost);
        const char* _coder_v_c_str = mmString_CStr(&pIconvContext->hFormatView);

        pModuleExplorer->FormatUpdate(_coder_h_c_str, _coder_v_c_str);
        return true;
    }
    bool mmLayerExplorerFinder::OnStaticTextFpsEventMouseClick(const CEGUI::EventArgs& args)
    {
        mmLayerDirector_SceneExclusiveForeground(this->pDirector, "mm/mmLayerCommonTextureManager", "mmLayerCommonTextureManager");
        return true;
    }
    bool mmLayerExplorerFinder::OnWidgetImageButtonBackEventMouseClick(const CEGUI::EventArgs& args)
    {
        mmLayerContext* w = mmLayerDirector_LayerAdd(this->pDirector, "mm/mmLayerCommonEnsure", "mmLayerCommonEnsure");
        mmLayerCommonEnsure* pLayerEnsure = (mmLayerCommonEnsure*)w;
        // 0x00000002 exit program.
        pLayerEnsure->SetDescriptionErrorcode(0x00000002);
        pLayerEnsure->StartTimerTerminate();

        assert(false == pLayerEnsure->hEventChoiceConn.isValid() || false == pLayerEnsure->hEventChoiceConn->connected() && "we must detach_handle after event handle.");

        pLayerEnsure->hEventChoiceConn = pLayerEnsure->subscribeEvent(
            mmLayerCommonEnsure::EventChoice, CEGUI::Event::Subscriber(&mmLayerExplorerFinder::OnLayerEnsureEventChoiceExit, this));

        return true;
    }
    bool  mmLayerExplorerFinder::OnWidgetImageButtonHomeEventMouseClick(const CEGUI::EventArgs& args)
    {
        return true;
    }
    bool mmLayerExplorerFinder::OnLayerEnsureEventChoiceExit(const CEGUI::EventArgs& args)
    {
        const mmChoiceEventArgs& evt = (const mmChoiceEventArgs&)(args);

        if (mmChoiceEventArgs::ENSURE == evt.hCode)
        {
            // shutdown the program.
            mmContextMaster_OnShutdown(&this->pContextMaster->hSuper);
        }
        return true;
    }
}
