/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerExplorerEntry.h"

#include "layer/widgets/mmWidgetTransform.h"

#include "CEGUI/WindowManager.h"
#include "CEGUI/widgets/Listbox.h"

#include "CEGUIOgreRenderer/Texture.h"
#include "CEGUIOgreRenderer/GeometryBuffer.h"

#include "CEGUI/TplWindowRendererProperty.h"

#include "explorer/mmExplorerItem.h"
#include "explorer/mmExplorerImage.h"

#include "layer/utility/mmLayerUtilityIconv.h"

namespace mm
{
    const CEGUI::String mmLayerExplorerEntry::EventNamespace = "mm";
    const CEGUI::String mmLayerExplorerEntry::WidgetTypeName = "mm/mmLayerExplorerEntry";

    const CEGUI::String mmLayerExplorerEntry::EventLayerEntryMouseClick = "EventLayerEntryMouseClick";
    const CEGUI::String mmLayerExplorerEntry::EventLayerEntryImageMouseClick = "EventLayerEntryImageMouseClick";

    mmLayerExplorerEntry::mmLayerExplorerEntry(const CEGUI::String& type, const CEGUI::String& name)
        : mmLayerContext(type, name)
        , LayerExplorerEntry(NULL)
        , StaticImageBackground(NULL)

        , StaticImageType(NULL)
        , StaticTextBasename(NULL)
        , StaticTextAuthority(NULL)
        , StaticTextSize(NULL)

        , hIndex(0)
        , pItem(NULL)
        , pIconvContext(NULL)
        , pExplorerImage(NULL)

        , hColourLustre(0.0f, 0.0f, 1.0f, 1.0f)
        , hColourSelect(0.0f, 1.0f, 0.0f, 1.0f)
        , hColourCommon(1.0f, 1.0f, 1.0f, 1.0f)
        , hIsSelect(false)
    {

    }
    mmLayerExplorerEntry::~mmLayerExplorerEntry(void)
    {

    }
    void mmLayerExplorerEntry::OnFinishLaunching(void)
    {
        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        this->LayerExplorerEntry = pWindowManager->loadLayoutFromFile("explorer/LayerExplorerEntry.layout");
        this->addChild(this->LayerExplorerEntry);

        // Design size.
        CEGUI::URect hArea = this->LayerExplorerEntry->getArea();
        this->setArea(hArea);

        // Manual set whole area.
        CEGUI::URect hWholeArea(CEGUI::UDim(0, 0), CEGUI::UDim(0, 0), CEGUI::UDim(1, 0), CEGUI::UDim(1, 0));
        this->LayerExplorerEntry->setArea(hWholeArea);

        this->StaticImageBackground = this->LayerExplorerEntry->getChild("StaticImageBackground");

        this->StaticImageType = this->StaticImageBackground->getChild("StaticImageType");
        this->StaticTextBasename = this->StaticImageBackground->getChild("StaticTextBasename");
        this->StaticTextAuthority = this->StaticImageBackground->getChild("StaticTextAuthority");
        this->StaticTextSize = this->StaticImageBackground->getChild("StaticTextSize");

        this->hItemEventMouseClickConn = this->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerExplorerEntry::OnLayerItemEventMouseClick, this));
        this->hTypeEventMouseClickConn = this->StaticImageType->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerExplorerEntry::OnLayerTypeEventMouseClick, this));
    }
    void mmLayerExplorerEntry::OnBeforeTerminate(void)
    {
        this->hItemEventMouseClickConn->disconnect();
        this->hTypeEventMouseClickConn->disconnect();

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        this->removeChild(this->LayerExplorerEntry);
        pWindowManager->destroyWindow(this->LayerExplorerEntry);
        this->LayerExplorerEntry = NULL;
    }
    void mmLayerExplorerEntry::SetIndex(size_t hIndex)
    {
        this->hIndex = hIndex;
    }
    void mmLayerExplorerEntry::SetItem(struct mmExplorerItem* pItem)
    {
        this->pItem = pItem;
    }
    void mmLayerExplorerEntry::SetIconvContext(struct mmIconvContext* pIconvContext)
    {
        this->pIconvContext = pIconvContext;
    }
    void mmLayerExplorerEntry::SetExplorerImage(struct mmExplorerImage* pExplorerImage)
    {
        this->pExplorerImage = pExplorerImage;
    }
    void mmLayerExplorerEntry::SetIsSelect(bool hIsSelect)
    {
        CEGUI::Colour _colour_0;
        CEGUI::String _colour_0_string;

        CEGUI::Colour _colour_1;
        CEGUI::String _colour_1_string;

        this->hIsSelect = hIsSelect;

        _colour_0 = this->hIsSelect ? this->hColourSelect : this->hColourCommon;
        _colour_0_string = CEGUI::PropertyHelper<CEGUI::Colour>::toString(_colour_0);

        _colour_1 = this->hIsSelect ? this->hColourLustre : this->hColourCommon;
        _colour_1_string = CEGUI::PropertyHelper<CEGUI::Colour>::toString(_colour_1);

        this->StaticImageBackground->setProperty("FrameColours", _colour_0_string);

        this->StaticImageType->setProperty("FrameColours", _colour_1_string);

        this->StaticTextBasename->setProperty("FrameColours", _colour_0_string);
        this->StaticTextAuthority->setProperty("FrameColours", _colour_0_string);
        this->StaticTextSize->setProperty("FrameColours", _colour_0_string);

        this->StaticTextBasename->setProperty("TextColours", _colour_0_string);
        this->StaticTextAuthority->setProperty("TextColours", _colour_0_string);
        this->StaticTextSize->setProperty("TextColours", _colour_0_string);
    }
    void mmLayerExplorerEntry::UpdateLayerValue(void)
    {
        char mode_string[16] = { 0 };
        char hTimeString[64] = { 0 };
        char size_string[32] = { 0 };

        this->StaticImageType->setProperty("Image", mmString_CStr(&this->pItem->type_image));

        this->UpdateUtf8View();

        mmExplorerItem_ModeString(this->pItem, mode_string);
        this->StaticTextAuthority->setText(mode_string);

        mmExplorerItem_SizeString(this->pItem, size_string);
        this->StaticTextSize->setText(size_string);
    }
    void mmLayerExplorerEntry::UpdateUtf8View(void)
    {
        mmLayerUtilityIconv_ViewName(this->pIconvContext, this->StaticTextBasename, &this->pItem->basename);
    }
    void mmLayerExplorerEntry::OnFinishAttach(void)
    {
        // need do nothing here.
    }
    void mmLayerExplorerEntry::OnBeforeDetach(void)
    {
        // need do nothing here.
    }
    bool mmLayerExplorerEntry::OnLayerItemEventMouseClick(const CEGUI::EventArgs& args)
    {
        this->SetIsSelect(!this->hIsSelect);

        CEGUI::WindowEventArgs hArgs(this);
        this->fireEvent(EventLayerEntryMouseClick, hArgs, EventNamespace);
        return true;
    }
    bool mmLayerExplorerEntry::OnLayerTypeEventMouseClick(const CEGUI::EventArgs& args)
    {
        this->SetIsSelect(!this->hIsSelect);

        CEGUI::WindowEventArgs hArgs(this);
        this->fireEvent(EventLayerEntryImageMouseClick, hArgs, EventNamespace);
        return true;
    }
}
