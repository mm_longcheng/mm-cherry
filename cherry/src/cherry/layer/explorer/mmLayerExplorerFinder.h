/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmLayerExplorerFinder_h__
#define __mmLayerExplorerFinder_h__

#include "core/mmCore.h"

#include "dish/mmEvent.h"

#include "layer/director/mmLayerContext.h"

#include "CEGUI/Window.h"

#include "explorer/mmExplorerCommand.h"
#include "explorer/mmExplorerFinder.h"
#include "explorer/mmExplorerImage.h"

#include "toolkit/mmIconv.h"

#include "layer/utility/mmLayerUtilityFps.h"

namespace mm
{
    class mmLayerCommonIconvEncode;

    class mmLayerExplorerDirectory;
    class mmLayerExplorerDetails;

    class mmLayerExplorerFinder : public mmLayerContext
    {
    public:
        static const CEGUI::String EventNamespace;          //!< Namespace for global events
        static const CEGUI::String WidgetTypeName;          //!< Window factory name
    public:
        CEGUI::Window* LayerExplorerFinder;
        CEGUI::Window* StaticImageBackground;

        CEGUI::Window* StaticTextFps;
        CEGUI::Window* WidgetImageButtonBack;
        CEGUI::Window* WidgetImageButtonHome;
        CEGUI::Window* LayerExplorerDirectoryDirectory;
        CEGUI::Window* LayerCommonIconvEncodeWindow;
        CEGUI::Window* LayerExplorerDetailsDetails;

        mmLayerCommonIconvEncode* pCommonIconvEncode;
        mmLayerExplorerDirectory* pExplorerDirectory;
        mmLayerExplorerDetails* pExplorerDetails;
    public:
        struct mmExplorerCommand pExplorerCommand;
        struct mmExplorerFinder pExplorerFinder;
        struct mmExplorerImage pExplorerImage;
    public:
        struct mmIconvContext pIconvContext;
    public:
        mmLayerUtilityFps hUtilityFps;
    public:
        CEGUI::Event::Connection hEventLayerItemMouseClickConn;
        CEGUI::Event::Connection hEventIconvCoderChangedConn;

        CEGUI::Event::Connection hFpsEventMouseClickConn;
        CEGUI::Event::Connection hBackEventMouseClickConn;
        CEGUI::Event::Connection hHomeEventMouseClickConn;
    public:
        // mm_event_handler pIconvContext_event_format_update_conn;
    public:
        mmLayerExplorerFinder(const CEGUI::String& type, const CEGUI::String& name);
        virtual ~mmLayerExplorerFinder(void);
    public:
        void SetExplorerDirectory(const char* pExplorerDirectory);
    public:
        // mmLayerContext
        virtual void OnFinishLaunching(void);
        virtual void OnBeforeTerminate(void);
    public:
        void OnEventItemOpenDir(struct mmExplorerItem* pItem);
        void OnEventItemOpenReg(struct mmExplorerItem* pItem);
        void OnEventItemPreview(struct mmExplorerItem* pItem);
        void OnEventDirectoryRefresh(const char* pDirectory);
    private:
        bool OnIconvContextEventFormatUpdate(const mmEventArgs& args);
    private:
        bool OnLayerItemEventLayerItemMouseClick(const CEGUI::EventArgs& args);
    private:
        bool OnLayerCommonIconvEncodeWindowEventIconvCoderChanged(const CEGUI::EventArgs& args);
    private:
        bool OnStaticTextFpsEventMouseClick(const CEGUI::EventArgs& args);
        bool OnWidgetImageButtonBackEventMouseClick(const CEGUI::EventArgs& args);
        bool OnWidgetImageButtonHomeEventMouseClick(const CEGUI::EventArgs& args);
    private:
        bool OnLayerEnsureEventChoiceExit(const CEGUI::EventArgs& args);
    };
}

#endif//__mmLayerExplorerFinder_h__