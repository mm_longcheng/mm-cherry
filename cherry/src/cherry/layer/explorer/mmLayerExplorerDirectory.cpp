/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerExplorerDirectory.h"

#include "core/mmString.h"

#include "dish/mmFilePath.h"

#include "layer/widgets/mmWidgetScrollable.h"
#include "layer/widgets/mmWidgetTransform.h"

#include "CEGUI/WindowManager.h"
#include "CEGUI/widgets/Editbox.h"
#include "CEGUI/widgets/Combobox.h"

#include "CEGUIOgreRenderer/Texture.h"
#include "CEGUIOgreRenderer/GeometryBuffer.h"

#include "CEGUI/TplWindowRendererProperty.h"

#include "mmLayerExplorerItem.h"

#include "explorer/mmExplorerCommand.h"
#include "explorer/mmExplorerFinder.h"
#include "explorer/mmExplorerImage.h"
#include "explorer/mmExplorerItem.h"
#include "explorer/mmExplorerImageEmu.h"

#include "layer/utility/mmLayerUtilityIconv.h"
#include "layer/utility/mmLayerUtilityListUrlItem.h"

#include "module/mmModuleLogger.h"
#include "module/mmModuleDatabase.h"

namespace mm
{
    static CEGUI::Window* __static_ChildrenCreatorExplorerDirectory_Produce(mmWidgetScrollable* obj, size_t index)
    {
        const mmWidgetScrollableChildrenCreator* pChildrenCreator = obj->GetChildrenCreator();
        mmLayerExplorerDirectory* pLayerExplorerDirectory = (mmLayerExplorerDirectory*)(pChildrenCreator->obj);
        struct mmLayerUtilityItemPool* pItemPool = &pLayerExplorerDirectory->hItemPool;;
        mmLayerExplorerItem* pLayerItem = (mmLayerExplorerItem*)mmLayerUtilityItemPool_Produce(pItemPool);
        pLayerItem->SetIndex(index);
        pLayerExplorerDirectory->CreateItemEvent(pLayerItem);
        return pLayerItem;
    }
    static void __static_ChildrenCreatorExplorerDirectory_Recycle(mmWidgetScrollable* obj, CEGUI::Window* w)
    {
        const mmWidgetScrollableChildrenCreator* pChildrenCreator = obj->GetChildrenCreator();
        mmLayerExplorerDirectory* pLayerExplorerDirectory = (mmLayerExplorerDirectory*)(pChildrenCreator->obj);
        struct mmLayerUtilityItemPool* pItemPool = &pLayerExplorerDirectory->hItemPool;;
        mmLayerExplorerItem* pLayerItem = (mmLayerExplorerItem*)(w);
        pLayerExplorerDirectory->DeleteItemEvent(pLayerItem);
        mmLayerUtilityItemPool_Recycle(pItemPool, pLayerItem);
    }

    static void __static_ChildrenCreatorExplorerDirectory_FinishAttach(mmWidgetScrollable* obj, CEGUI::Window* w)
    {
        mmLayerExplorerItem* pLayerItem = (mmLayerExplorerItem*)(w);
        pLayerItem->OnFinishAttach();
    }

    static void __static_ChildrenCreatorExplorerDirectory_BeforeDetach(mmWidgetScrollable* obj, CEGUI::Window* w)
    {
        mmLayerExplorerItem* pLayerItem = (mmLayerExplorerItem*)(w);
        pLayerItem->OnBeforeDetach();
    }

    const CEGUI::String mmLayerExplorerDirectory::EventNamespace = "mm";
    const CEGUI::String mmLayerExplorerDirectory::WidgetTypeName = "mm/mmLayerExplorerDirectory";

    const CEGUI::String mmLayerExplorerDirectory::EventLayerItemMouseClick = "EventLayerItemMouseClick";

    const struct mmString mmLayerExplorerDirectory::HomeAliasName = mmString_Make("($Home)");

    mmLayerExplorerDirectory::mmLayerExplorerDirectory(const CEGUI::String& type, const CEGUI::String& name)
        : mmLayerContext(type, name)
        , LayerExplorerDirectory(NULL)
        , StaticImageBackground(NULL)

        , WidgetImageButtonUp(NULL)
        , WidgetImageButtonRefresh(NULL)
        , WidgetScrollableYList(NULL)
        , WidgetComboboxUrl(NULL)

        , pWidgetScrollableYList(NULL)
        , pWidgetComboboxUrl(NULL)
        , pWidgetComboboxUrlEditbox(NULL)

        , pExplorerFinder(NULL)
        , pExplorerImage(NULL)

        , pIconvContext(NULL)
    {
        struct mmVectorValueEventAllocator hEventAllocator;

        mmVectorValue_Init(&this->hItemUrlVector);
        mmString_Init(&this->hExplorerDirectory);
        mmString_Init(&this->hExplorerDirectoryAliasName);
        mmString_Init(&this->hTextUrlHost);
        mmString_Init(&this->hTextUrlHostAliasName);
        mmLayerUtilityItemPool_Init(&this->hItemPool);
        mmRbtsetVpt_Init(&this->hLayerItemSelect);
        mmRbtsetVpt_Init(&this->hItemSelect);
        //
        mmLayerUtilityItemPool_SetType(&this->hItemPool, "mm/mmLayerExplorerItem");
        //
        hEventAllocator.Produce = &mmVectorValue_ListUrlItemEventProduce;
        hEventAllocator.Recycle = &mmVectorValue_ListUrlItemEventRecycle;
        hEventAllocator.obj = this;
        mmVectorValue_SetElemSize(&this->hItemUrlVector, sizeof(mmLayerUtilityListUrlItem));
        mmVectorValue_SetEventAllocator(&this->hItemUrlVector, &hEventAllocator);
    }
    mmLayerExplorerDirectory::~mmLayerExplorerDirectory(void)
    {
        assert(0 == this->hItemUrlVector.size && "clear item url map before destroy.");

        mmVectorValue_Destroy(&this->hItemUrlVector);
        mmString_Destroy(&this->hExplorerDirectory);
        mmString_Destroy(&this->hExplorerDirectoryAliasName);
        mmString_Destroy(&this->hTextUrlHost);
        mmString_Destroy(&this->hTextUrlHostAliasName);
        mmLayerUtilityItemPool_Destroy(&this->hItemPool);
        mmRbtsetVpt_Destroy(&this->hLayerItemSelect);
        mmRbtsetVpt_Destroy(&this->hItemSelect);
    }
    void mmLayerExplorerDirectory::OnFinishLaunching(void)
    {
        struct mmSurfaceMaster* pSurfaceMaster = this->pSurfaceMaster;

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        this->LayerExplorerDirectory = pWindowManager->loadLayoutFromFile("explorer/LayerExplorerDirectory.layout");
        this->addChild(this->LayerExplorerDirectory);
        // Manual set whole area.
        CEGUI::URect hWholeArea(CEGUI::UDim(0, 0), CEGUI::UDim(0, 0), CEGUI::UDim(1, 0), CEGUI::UDim(1, 0));
        this->LayerExplorerDirectory->setArea(hWholeArea);

        this->StaticImageBackground = this->LayerExplorerDirectory->getChild("StaticImageBackground");

        this->WidgetImageButtonUp = this->StaticImageBackground->getChild("WidgetImageButtonUp");
        this->WidgetImageButtonRefresh = this->StaticImageBackground->getChild("WidgetImageButtonRefresh");
        this->WidgetScrollableYList = this->StaticImageBackground->getChild("WidgetScrollableYList");
        this->WidgetComboboxUrl = this->StaticImageBackground->getChild("WidgetComboboxUrl");

        this->pWidgetScrollableYList = (mmWidgetScrollable*)this->WidgetScrollableYList;
        this->pWidgetComboboxUrl = (CEGUI::Combobox*)this->WidgetComboboxUrl;
        this->pWidgetComboboxUrlEditbox = this->pWidgetComboboxUrl->getEditbox();;

        this->WidgetImageButtonUp->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerExplorerDirectory::OnButtonUpEventMouseClick, this));
        this->WidgetImageButtonRefresh->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerExplorerDirectory::OnButtonRefreshEventMouseClick, this));
        //
        this->pWidgetComboboxUrlEditbox->subscribeEvent(CEGUI::Window::EventTextChanged, CEGUI::Event::Subscriber(&mmLayerExplorerDirectory::OnWidgetComboboxUrlEventTextChanged, this));
        this->pWidgetComboboxUrl->subscribeEvent(CEGUI::Combobox::EventTextAccepted, CEGUI::Event::Subscriber(&mmLayerExplorerDirectory::OnWidgetComboboxUrlEventTextAccepted, this));
        this->pWidgetComboboxUrl->subscribeEvent(CEGUI::Combobox::EventListSelectionAccepted, CEGUI::Event::Subscriber(&mmLayerExplorerDirectory::OnWidgetComboboxUrlEventListSelectionAccepted, this));

        this->subscribeEvent(mmLayerContext::EventTouchsMoved, CEGUI::Event::Subscriber(&mmLayerExplorerDirectory::OnLayerEventTouchsMoved, this));
        this->subscribeEvent(mmLayerContext::EventTouchsBegan, CEGUI::Event::Subscriber(&mmLayerExplorerDirectory::OnLayerEventTouchsBegan, this));
        this->subscribeEvent(mmLayerContext::EventTouchsEnded, CEGUI::Event::Subscriber(&mmLayerExplorerDirectory::OnLayerEventTouchsEnded, this));
        this->subscribeEvent(mmLayerContext::EventTouchsBreak, CEGUI::Event::Subscriber(&mmLayerExplorerDirectory::OnLayerEventTouchsBreak, this));

        this->pWidgetScrollableYList->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerExplorerDirectory::OnWidgetScrollableYListEventMouseClick, this));

        mmLayerUtilityItemPool_OnFinishLaunching(&this->hItemPool);

        struct mmWidgetScrollableChildrenCreator hChildrenCreator;
        hChildrenCreator.obj = this;
        hChildrenCreator.Produce = &__static_ChildrenCreatorExplorerDirectory_Produce;
        hChildrenCreator.Recycle = &__static_ChildrenCreatorExplorerDirectory_Recycle;
        hChildrenCreator.FinishAttach = &__static_ChildrenCreatorExplorerDirectory_FinishAttach;
        hChildrenCreator.BeforeDetach = &__static_ChildrenCreatorExplorerDirectory_BeforeDetach;
        //
        this->pWidgetScrollableYList->SetChildrenCreator(&hChildrenCreator);
        this->pWidgetScrollableYList->OnFinishLaunching();

        this->CalculationHistoryExplorerDirectory();

        this->hEventCursorMovedConn = this->subscribeEvent(mmLayerContext::EventCursorMoved, CEGUI::Event::Subscriber(&mmLayerExplorerDirectory::OnHandleEventCursorMoved, this));
        this->hEventCursorBeganConn = this->subscribeEvent(mmLayerContext::EventCursorBegan, CEGUI::Event::Subscriber(&mmLayerExplorerDirectory::OnHandleEventCursorBegan, this));
        this->hEventCursorEndedConn = this->subscribeEvent(mmLayerContext::EventCursorEnded, CEGUI::Event::Subscriber(&mmLayerExplorerDirectory::OnHandleEventCursorEnded, this));
    }

    void mmLayerExplorerDirectory::OnBeforeTerminate(void)
    {
        struct mmContextMaster* pContextMaster = this->pContextMaster;
        struct mmSurfaceMaster* pSurfaceMaster = this->pSurfaceMaster;

        mm::mmModule* pModule = &pContextMaster->hModule;

        mm::mmModuleDatabase* pModuleDatabase = pModule->GetData<mm::mmModuleDatabase>();
        struct mmDatabaseExplorer* pDatabaseExplorer = &pModuleDatabase->hDatabaseExplorer;

        float _scroll_position_new = this->pWidgetScrollableYList->GetScrollPosition();
        // we need leave before terminate.
        mmDatabaseExplorer_UrlLeave(pDatabaseExplorer, mmString_CStr(&this->hExplorerDirectory));
        // safe the url position.
        mmDatabaseExplorer_SetUrlPosition(pDatabaseExplorer, mmString_CStr(&this->hExplorerDirectory), _scroll_position_new);

        // delete the combobox item.
        this->DeleteComboboxUrlItem();

        this->hEventCursorMovedConn->disconnect();
        this->hEventCursorBeganConn->disconnect();
        this->hEventCursorEndedConn->disconnect();

        this->pWidgetScrollableYList->OnBeforeTerminate();
        this->pWidgetScrollableYList = NULL;

        mmLayerUtilityItemPool_OnBeforeTerminate(&this->hItemPool);

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        this->removeChild(this->LayerExplorerDirectory);
        pWindowManager->destroyWindow(this->LayerExplorerDirectory);
        this->LayerExplorerDirectory = NULL;
    }
    void mmLayerExplorerDirectory::SetExplorerFinder(struct mmExplorerFinder* pExplorerFinder)
    {
        this->pExplorerFinder = pExplorerFinder;
    }
    void mmLayerExplorerDirectory::SetExplorerImage(struct mmExplorerImage* pExplorerImage)
    {
        this->pExplorerImage = pExplorerImage;
    }
    void mmLayerExplorerDirectory::SetIconvContext(struct mmIconvContext* pIconvContext)
    {
        this->pIconvContext = pIconvContext;
    }
    void mmLayerExplorerDirectory::SetExplorerDirectory(const char* pExplorerDirectory)
    {
        if (0 == mmAccess(pExplorerDirectory, MM_ACCESS_R_OK))
        {
            struct mmContextMaster* pContextMaster = this->pContextMaster;
            mm::mmModule* pModule = &pContextMaster->hModule;

            mm::mmModuleDatabase* pModuleDatabase = pModule->GetData<mm::mmModuleDatabase>();
            struct mmDatabaseExplorer* pDatabaseExplorer = &pModuleDatabase->hDatabaseExplorer;

            float hScrollPositionNew = this->pWidgetScrollableYList->GetScrollPosition();
            float hScrollPositionOld = 0.0f;

            mmDatabaseExplorer_UrlLeave(pDatabaseExplorer, mmString_CStr(&this->hExplorerDirectory));
            mmDatabaseExplorer_UrlEnter(pDatabaseExplorer, pExplorerDirectory);
            // safe the url position.
            mmDatabaseExplorer_SetUrlPosition(pDatabaseExplorer, mmString_CStr(&this->hExplorerDirectory), hScrollPositionNew);

            // assign the new explorer_directory value.
            mmString_Assigns(&this->hExplorerDirectory, pExplorerDirectory);
            // load the url position.
            hScrollPositionOld = mmDatabaseExplorer_GetUrlPosition(pDatabaseExplorer, mmString_CStr(&this->hExplorerDirectory));

            this->UnselectItemVector();
            // refresh the explorer view.
            this->RefreshExplorerDirectory();
            // fire the refresh event.
            this->RefreshLayerItemEvent();

            // apply the _scroll_position_old.
            this->pWidgetScrollableYList->SetScrollPosition(hScrollPositionOld);
            // refresh the item vector view.
            this->RefreshExplorerItemVectorView();

            // refresh combobox url item.
            this->RefreshComboboxUrlLayer();
        }
        else
        {
            struct mmContextMaster* pContextMaster = this->pContextMaster;
            mm::mmModule* pModule = &pContextMaster->hModule;

            mm::mmModuleLogger* pModuleLogger = pModule->GetData<mm::mmModuleLogger>();

            mm::mmModuleDatabase* pModuleDatabase = pModule->GetData<mm::mmModuleDatabase>();
            struct mmDatabaseExplorer* pDatabaseExplorer = &pModuleDatabase->hDatabaseExplorer;

            // the directory is invalid, we need delete from the database.
            mmDatabaseExplorer_UrlDelete(pDatabaseExplorer, pExplorerDirectory);

            static const char* message = "Directory does not exist.";
            pModuleLogger->LogView(MM_LOG_INFO, 0, message, message);
        }
    }
    const char* mmLayerExplorerDirectory::GetExplorerDirectory(void) const
    {
        return mmString_CStr((struct mmString*)&this->hExplorerDirectory);
    }
    void mmLayerExplorerDirectory::RefreshExplorerDirectory(void)
    {
        mmExplorerFinder_SetDirectory(this->pExplorerFinder, mmString_CStr(&this->hExplorerDirectory));
        mmExplorerFinder_Refresh(this->pExplorerFinder);

        this->UpdateUrlUtf8View();

        this->pWidgetScrollableYList->SetIndexSize(this->pExplorerFinder->vector_item.size);
        this->pWidgetScrollableYList->NotifyAttributesChanged();
        this->pWidgetScrollableYList->NotifyTrimChildrenNode();

        this->UpdateItemEventCurrent();
    }
    void mmLayerExplorerDirectory::RefreshExplorerItemVectorView(void)
    {
        this->pWidgetScrollableYList->NotifyAttributesChanged();
        this->pWidgetScrollableYList->NotifyTrimChildrenNode();
    }
    void mmLayerExplorerDirectory::RefreshLayerItemEvent(void)
    {
        CEGUI::WindowEventArgs hArgs(this);
        this->fireEvent(EventLayerItemMouseClick, hArgs, EventNamespace);
    }
    void mmLayerExplorerDirectory::RefreshComboboxUrlLayer(void)
    {
        this->UpdateComboboxUrlItem();

        this->UpdateUrlComboboxView();
    }
    void mmLayerExplorerDirectory::CalculationCurrentHostUrl(void)
    {
        struct mmContextMaster* pContextMaster = this->pContextMaster;
        struct mmPackageAssets* pPackageAssets = &pContextMaster->hPackageAssets;
        struct mmString* pHomeDirectory = &pPackageAssets->hHomeDirectory;
        struct mmString* pHomeAliasName = (struct mmString*)&HomeAliasName;
        
        mmLayerUtilityIconv_HostName(this->pIconvContext, this->pWidgetComboboxUrlEditbox, &this->hTextUrlHostAliasName);
        
        mmLayerExplorerDirectory::DecodeAliasName(pHomeAliasName, pHomeDirectory, &this->hTextUrlHost, &this->hTextUrlHostAliasName);
    }
    void mmLayerExplorerDirectory::CalculationHistoryExplorerDirectory(void)
    {
        struct mmContextMaster* pContextMaster = this->pContextMaster;
        struct mmPackageAssets* pPackageAssets = &pContextMaster->hPackageAssets;

        mm::mmModule* pModule = &pContextMaster->hModule;

        mm::mmModuleDatabase* pModuleDatabase = pModule->GetData<mm::mmModuleDatabase>();
        struct mmDatabaseExplorer* pDatabaseExplorer = &pModuleDatabase->hDatabaseExplorer;
        //
        struct mmString hLastUrl;

        mmString_Init(&hLastUrl);
        //
        mmDatabaseExplorer_UrlLast(pDatabaseExplorer, &hLastUrl);
        //        
        const char* pExplorerDirectory = mmString_CStr(&pPackageAssets->hExternalFilesDirectory);
        //
        if (mmString_Empty(&hLastUrl))
        {
            // to the default url.
            this->SetExplorerDirectory(pExplorerDirectory);
        }
        else
        {
            if (0 == mmAccess(mmString_CStr(&hLastUrl), MM_ACCESS_R_OK))
            {
                // to the last url.
                this->SetExplorerDirectory(mmString_CStr(&hLastUrl));
            }
            else
            {
                // to the default url.
                this->SetExplorerDirectory(pExplorerDirectory);
            }
        }

        mmString_Destroy(&hLastUrl);
    }
    void mmLayerExplorerDirectory::CreateItemEvent(mmLayerExplorerItem* pLayerItem)
    {
        struct mmExplorerFinder* pExplorerFinder = this->pExplorerFinder;
        struct mmVectorValue* pVectorItem = &pExplorerFinder->vector_item;
        struct mmExplorerItem* pItem = (struct mmExplorerItem*)mmVectorValue_GetIndexReference(pVectorItem, pLayerItem->hIndex);

        pLayerItem->SetIconvContext(this->pIconvContext);
        pLayerItem->SetExplorerImage(this->pExplorerImage);

        pLayerItem->hEventLayerItemMouseClickConn = pLayerItem->subscribeEvent(
            mmLayerExplorerItem::EventLayerItemMouseClick,
            CEGUI::Event::Subscriber(&mmLayerExplorerDirectory::OnLayerItemEventLayerItemMouseClick, this));

        pLayerItem->hEventLayerItemImageMouseClickConn = pLayerItem->subscribeEvent(
            mmLayerExplorerItem::EventLayerItemImageMouseClick,
            CEGUI::Event::Subscriber(&mmLayerExplorerDirectory::OnLayerItemEventLayerItemImageMouseClick, this));

        this->UpdateItemEvent(pLayerItem);
    }
    void mmLayerExplorerDirectory::DeleteItemEvent(mmLayerExplorerItem* pLayerItem)
    {
        mmRbtsetVpt_Rmv(&this->hLayerItemSelect, pLayerItem);
        mmRbtsetVpt_Rmv(&this->hItemSelect, pLayerItem->pItem);
        pLayerItem->SetIsSelect(false);
        pLayerItem->SetItem(NULL);
        pLayerItem->hEventLayerItemMouseClickConn->disconnect();
        pLayerItem->hEventLayerItemImageMouseClickConn->disconnect();
    }
    void mmLayerExplorerDirectory::UpdateItemEvent(mmLayerExplorerItem* pLayerItem)
    {
        struct mmExplorerFinder* pExplorerFinder = this->pExplorerFinder;
        struct mmVectorValue* pVectorItem = &pExplorerFinder->vector_item;
        struct mmExplorerItem* pItem = (struct mmExplorerItem*)mmVectorValue_GetIndexReference(pVectorItem, pLayerItem->hIndex);

        mmExplorerImage_ItemType(this->pExplorerImage, pItem);

        pLayerItem->SetItem(pItem);
        pLayerItem->UpdateLayerValue();
    }
    void mmLayerExplorerDirectory::UpdateItemEventCurrent(void)
    {
        // item view.
        struct mmRbtreeU64Vpt* pWindowRbtree = this->pWidgetScrollableYList->GetWindowRbtree();

        CEGUI::Window* child = NULL;
        struct mmRbNode* n = NULL;
        struct mmRbtreeU64VptIterator* it = NULL;
        mmLayerExplorerItem* pLayerItem = NULL;
        // max
        n = mmRb_First(&pWindowRbtree->rbt);
        while (NULL != n)
        {
            it = (struct mmRbtreeU64VptIterator*)mmRb_Entry(n, struct mmRbtreeU64VptIterator, n);
            // next
            n = mmRb_Next(n);

            child = (CEGUI::Window*)it->v;

            pLayerItem = (mmLayerExplorerItem*)child;

            this->UpdateItemEvent(pLayerItem);
        }
    }
    void mmLayerExplorerDirectory::UpdateUtf8View(void)
    {
        // url view.
        this->UpdateUrlUtf8View();

        // url item.
        this->UpdateUrlComboboxView();

        // item view.
        this->UpdateUrlVectorUtf8View();
    }
    void mmLayerExplorerDirectory::UpdateUrlUtf8View(void)
    {
        struct mmContextMaster* pContextMaster = this->pContextMaster;
        struct mmPackageAssets* pPackageAssets = &pContextMaster->hPackageAssets;
        struct mmString* pHomeDirectory = &pPackageAssets->hHomeDirectory;
        struct mmString* pHomeAliasName = (struct mmString*)&HomeAliasName;
        
        mmLayerExplorerDirectory::EncodeAliasName(pHomeAliasName, pHomeDirectory, &this->hExplorerDirectory, &this->hExplorerDirectoryAliasName);
        
        mmLayerUtilityIconv_ViewName(this->pIconvContext, this->pWidgetComboboxUrlEditbox, &this->hExplorerDirectoryAliasName);
    }
    void mmLayerExplorerDirectory::UpdateUrlVectorUtf8View(void)
    {
        struct mmRbtreeU64Vpt* pWindowRbtree = this->pWidgetScrollableYList->GetWindowRbtree();

        CEGUI::Window* child = NULL;
        struct mmRbNode* n = NULL;
        struct mmRbtreeU64VptIterator* it = NULL;
        mmLayerExplorerItem* pLayerItem = NULL;
        // max
        n = mmRb_First(&pWindowRbtree->rbt);
        while (NULL != n)
        {
            it = (struct mmRbtreeU64VptIterator*)mmRb_Entry(n, struct mmRbtreeU64VptIterator, n);
            // next
            n = mmRb_Next(n);

            child = (CEGUI::Window*)it->v;

            pLayerItem = (mmLayerExplorerItem*)child;

            pLayerItem->UpdateUtf8View();
        }
    }
    void mmLayerExplorerDirectory::UpdateUrlComboboxView(void)
    {
        struct mmIconvContext* pIconvContext = this->pIconvContext;

        mmLayerUtilityListUrlItem* pItem = NULL;

        size_t index = 0;

        while (index < this->hItemUrlVector.size)
        {
            pItem = (mmLayerUtilityListUrlItem*)mmVectorValue_GetIndexReference(&this->hItemUrlVector, index);
            pItem->UpdateUtf8View();
            index++;
        }
    }
    void mmLayerExplorerDirectory::UnselectItemVector(void)
    {
        mmLayerExplorerItem* e = NULL;
        struct mmRbNode* n = NULL;
        struct mmRbtsetVptIterator* it = NULL;
        // max
        n = mmRb_First(&this->hLayerItemSelect.rbt);
        while (NULL != n)
        {
            it = (struct mmRbtsetVptIterator*)mmRb_Entry(n, struct mmRbtsetVptIterator, n);
            // next
            n = mmRb_Next(n);

            e = (mmLayerExplorerItem*)it->k;

            mmRbtsetVpt_Erase(&this->hLayerItemSelect, it);
            mmRbtsetVpt_Rmv(&this->hItemSelect, e->pItem);

            e->SetIsSelect(false);
        }
    }
    void mmLayerExplorerDirectory::CreateComboboxUrlItem(void)
    {
        struct mmContextMaster* pContextMaster = this->pContextMaster;
        struct mmPackageAssets* pPackageAssets = &pContextMaster->hPackageAssets;
        struct mmString* pHomeDirectory = &pPackageAssets->hHomeDirectory;
        struct mmString* pHomeAliasName = (struct mmString*)&HomeAliasName;
        
        struct mmString hUrlAliasName;
        
        mm::mmModule* pModule = &pContextMaster->hModule;

        mm::mmModuleDatabase* pModuleDatabase = pModule->GetData<mm::mmModuleDatabase>();
        struct mmDatabaseExplorer* pDatabaseExplorer = &pModuleDatabase->hDatabaseExplorer;

        struct mmVectorValue* pVectorItem = &pDatabaseExplorer->vector_item;
        
        mmString_Init(&hUrlAliasName);

        // the first n1 elem is time_last_leave
        mmDatabaseExplorer_UrlRankingLeaveSelect(pDatabaseExplorer, mmLayerExplorerDirectory_RANKING_N1_NUMBER);
        // the second n2 - n1 elem is time_stay
        mmDatabaseExplorer_UrlRankingStaySelect(pDatabaseExplorer, mmLayerExplorerDirectory_RANKING_N2_NUMBER);
        //
        {
            size_t index = 0;
            size_t size = 0;
            mmLayerUtilityListUrlItem* pItem = NULL;
            mmLayerUtilityTextItem* pTextItem;
            struct mmDatabaseExplorerHistoryItem* pHistoryItem = NULL;

            size = pVectorItem->size;

            mmVectorValue_AlignedMemory(&this->hItemUrlVector, pVectorItem->size);

            while (index < size)
            {
                pHistoryItem = (mmDatabaseExplorerHistoryItem*)mmVectorValue_GetIndexReference(pVectorItem, index);
                
                mmLayerExplorerDirectory::EncodeAliasName(pHomeAliasName, pHomeDirectory, &pHistoryItem->url, &hUrlAliasName);
                //
                pItem = (mmLayerUtilityListUrlItem*)mmVectorValue_GetIndexReference(&this->hItemUrlVector, index);
                pItem->SetItemId(index);
                pItem->SetUrl("", mmString_CStr(&hUrlAliasName));
                pItem->SetIconvContext(this->pIconvContext);
                pItem->SetHideSuffixName(MM_FALSE);
                pTextItem = &pItem->hTextItem;
                pTextItem->setSelectionBrushImage("TaharezLook/TextSelectionBrush");
                pTextItem->SetScaleY(1.4f);
                pItem->OnFinishLaunching();
                this->pWidgetComboboxUrl->addItem(pTextItem);

                index++;
            }
        }
        //
        mmDatabaseExplorer_UrlVectorClear(pDatabaseExplorer);
        
        mmString_Destroy(&hUrlAliasName);
    }
    void mmLayerExplorerDirectory::DeleteComboboxUrlItem(void)
    {
        size_t size = this->hItemUrlVector.size;

        size_t index = 0;

        mmLayerUtilityListUrlItem* pItem = NULL;
        CEGUI::ListboxTextItem* pTextItem;

        while (index < size)
        {
            pItem = (mmLayerUtilityListUrlItem*)mmVectorValue_GetIndexReference(&this->hItemUrlVector, index);
            pTextItem = &pItem->hTextItem;
            this->pWidgetComboboxUrl->removeItem(pTextItem);
            pItem->OnBeforeTerminate();
            index++;
        }

        mmVectorValue_Clear(&this->hItemUrlVector);
    }
    void mmLayerExplorerDirectory::UpdateComboboxUrlItem(void)
    {
        this->DeleteComboboxUrlItem();
        this->CreateComboboxUrlItem();
    }

    void mmLayerExplorerDirectory::EncodeAliasName(struct mmString* hAliasName, struct mmString* hPathName, struct mmString* hRealName, struct mmString* hViewName)
    {
        struct mmString hAliasName0;
        struct mmString hAliasName1;
        size_t o = 0;
        size_t l = 0;
        
        mmString_Init(&hAliasName0);
        mmString_Init(&hAliasName1);
        
        o = 0;
        l = mmString_Size(hPathName);
        mmString_Substr(hRealName, &hAliasName0, o, l);
        
        o = mmString_Size(hRealName) > l ? l : mmString_Size(hRealName);
        l = mmString_Size(hRealName) - o;
        mmString_Substr(hRealName, &hAliasName1, o, l);
        
        if(0 == mmString_Compare(hPathName, &hAliasName0))
        {
            mmString_Assign(hViewName, (struct mmString*)&HomeAliasName);
            mmString_Append(hViewName, &hAliasName1);
        }
        else
        {
            mmString_Assign(hViewName, hRealName);
        }
        
        mmString_Destroy(&hAliasName0);
        mmString_Destroy(&hAliasName1);
    }
    void mmLayerExplorerDirectory::DecodeAliasName(struct mmString* hAliasName, struct mmString* hPathName, struct mmString* hRealName, struct mmString* hViewName)
    {
        struct mmString hAliasName0;
        struct mmString hAliasName1;
        size_t o = 0;
        size_t l = 0;
        
        mmString_Init(&hAliasName0);
        mmString_Init(&hAliasName1);
        
        o = 0;
        l = mmString_Size(hAliasName);
        mmString_Substr(hViewName, &hAliasName0, o, l);
        
        o = mmString_Size(hViewName) > l ? l : mmString_Size(hViewName);
        l = mmString_Size(hViewName) - o;
        mmString_Substr(hViewName, &hAliasName1, o, l);
        
        if(0 == mmString_Compare((struct mmString*)&HomeAliasName, &hAliasName0))
        {
            mmString_Assign(hRealName, hPathName);
            mmString_Append(hRealName, &hAliasName1);
        }
        else
        {
            mmString_Assign(hRealName, hViewName);
        }
        
        mmString_Destroy(&hAliasName0);
        mmString_Destroy(&hAliasName1);
    }
    bool mmLayerExplorerDirectory::OnLayerEventTouchsMoved(const CEGUI::EventArgs& args)
    {
        const mmTouchsEventArgs& evt = (const mmTouchsEventArgs&)(args);
        return false;
    }
    bool mmLayerExplorerDirectory::OnLayerEventTouchsBegan(const CEGUI::EventArgs& args)
    {
        const mmTouchsEventArgs& evt = (const mmTouchsEventArgs&)(args);
        return false;
    }
    bool mmLayerExplorerDirectory::OnLayerEventTouchsEnded(const CEGUI::EventArgs& args)
    {
        const mmTouchsEventArgs& evt = (const mmTouchsEventArgs&)(args);
        return false;
    }
    bool mmLayerExplorerDirectory::OnLayerEventTouchsBreak(const CEGUI::EventArgs& args)
    {
        const mmTouchsEventArgs& evt = (const mmTouchsEventArgs&)(args);
        return false;
    }
    bool mmLayerExplorerDirectory::OnHandleEventCursorMoved(const CEGUI::EventArgs& args)
    {
        const mmCursorEventArgs& evt = (const mmCursorEventArgs&)(args);
        this->pWidgetScrollableYList->OnEventCursorMoved(evt.content);
        return false;
    }
    bool mmLayerExplorerDirectory::OnHandleEventCursorBegan(const CEGUI::EventArgs& args)
    {
        const mmCursorEventArgs& evt = (const mmCursorEventArgs&)(args);
        this->pWidgetScrollableYList->OnEventCursorBegan(evt.content);
        return false;
    }
    bool mmLayerExplorerDirectory::OnHandleEventCursorEnded(const CEGUI::EventArgs& args)
    {
        const mmCursorEventArgs& evt = (const mmCursorEventArgs&)(args);
        this->pWidgetScrollableYList->OnEventCursorEnded(evt.content);
        return false;
    }
    bool mmLayerExplorerDirectory::OnLayerItemEventLayerItemMouseClick(const CEGUI::EventArgs& args)
    {
        const CEGUI::MouseEventArgs& evt = (const CEGUI::MouseEventArgs&)(args);

        mmLayerExplorerItem* pLayerItem = (mmLayerExplorerItem*)(evt.window);

        this->UnselectItemVector();

        if (pLayerItem->hIsSelect)
        {
            mmRbtsetVpt_Add(&this->hLayerItemSelect, pLayerItem);
            mmRbtsetVpt_Add(&this->hItemSelect, pLayerItem->pItem);
        }
        else
        {
            mmRbtsetVpt_Rmv(&this->hLayerItemSelect, pLayerItem);
            mmRbtsetVpt_Rmv(&this->hItemSelect, pLayerItem->pItem);
        }

        this->RefreshLayerItemEvent();

        return true;
    }
    bool mmLayerExplorerDirectory::OnLayerItemEventLayerItemImageMouseClick(const CEGUI::EventArgs& args)
    {
        const CEGUI::MouseEventArgs& evt = (const CEGUI::MouseEventArgs&)(args);

        mmLayerExplorerItem* pLayerItem = (mmLayerExplorerItem*)(evt.window);

        if (pLayerItem->hIsSelect)
        {
            mmRbtsetVpt_Add(&this->hLayerItemSelect, pLayerItem);
            mmRbtsetVpt_Add(&this->hItemSelect, pLayerItem->pItem);
        }
        else
        {
            mmRbtsetVpt_Rmv(&this->hLayerItemSelect, pLayerItem);
            mmRbtsetVpt_Rmv(&this->hItemSelect, pLayerItem->pItem);
        }

        this->RefreshLayerItemEvent();

        return true;
    }
    bool mmLayerExplorerDirectory::OnWidgetScrollableYListEventMouseClick(const CEGUI::EventArgs& args)
    {
        this->UnselectItemVector();
        this->RefreshLayerItemEvent();
        return true;
    }
    bool mmLayerExplorerDirectory::OnButtonUpEventMouseClick(const CEGUI::EventArgs& args)
    {
        struct mmString hBasename;
        struct mmString hPathname;

        mmString_Init(&hBasename);
        mmString_Init(&hPathname);

        this->CalculationCurrentHostUrl();
        mmDirectoryParentPath(&this->hTextUrlHost, &hBasename, &hPathname);
        this->SetExplorerDirectory(mmString_CStr(&hPathname));

        mmString_Destroy(&hBasename);
        mmString_Destroy(&hPathname);

        return true;
    }
    bool mmLayerExplorerDirectory::OnButtonRefreshEventMouseClick(const CEGUI::EventArgs& args)
    {
        this->CalculationCurrentHostUrl();
        this->SetExplorerDirectory(mmString_CStr(&this->hTextUrlHost));
        return true;
    }
    bool mmLayerExplorerDirectory::OnWidgetComboboxUrlEventTextChanged(const CEGUI::EventArgs& args)
    {
        // need do nothing.
        return true;
    }
    bool mmLayerExplorerDirectory::OnWidgetComboboxUrlEventTextAccepted(const CEGUI::EventArgs& args)
    {
        this->OnButtonRefreshEventMouseClick(args);
        return true;
    }
    bool mmLayerExplorerDirectory::OnWidgetComboboxUrlEventListSelectionAccepted(const CEGUI::EventArgs& args)
    {
        const CEGUI::WindowEventArgs& evt = (const CEGUI::WindowEventArgs&)(args);
        CEGUI::Combobox* pCombobox = (CEGUI::Combobox*)(evt.window);

        do
        {
            size_t index = 0;
            mmLayerUtilityListUrlItem* pItem = NULL;

            CEGUI::ListboxItem* pSelectItem = pCombobox->getSelectedItem();
            if (NULL == pSelectItem)
            {
                break;
            }
            index = (size_t)pSelectItem->getID();
            pItem = (mmLayerUtilityListUrlItem*)mmVectorValue_GetIndexReference(&this->hItemUrlVector, index);
            if (NULL == pItem)
            {
                break;
            }

            struct mmContextMaster* pContextMaster = this->pContextMaster;
            struct mmPackageAssets* pPackageAssets = &pContextMaster->hPackageAssets;
            struct mmString* pHomeDirectory = &pPackageAssets->hHomeDirectory;
            struct mmString* pHomeAliasName = (struct mmString*)&HomeAliasName;

            mmLayerExplorerDirectory::DecodeAliasName(pHomeAliasName, pHomeDirectory, &this->hTextUrlHost, &pItem->hUrlItem.basename);

            this->SetExplorerDirectory(mmString_CStr(&this->hTextUrlHost));
        } while (0);

        return true;
    }
}
