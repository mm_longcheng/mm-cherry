/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerExplorerDetailsMulti.h"
#include "mmLayerExplorerEntry.h"

#include "CEGUI/WindowManager.h"

#include "explorer/mmExplorerCommand.h"
#include "explorer/mmExplorerImage.h"

#include "layer/widgets/mmWidgetScrollable.h"

namespace mm
{
    static CEGUI::Window* __static_ChildrenCreatorExplorerDetailsMulti_Produce(mmWidgetScrollable* obj, size_t index)
    {
        const mmWidgetScrollableChildrenCreator* pChildrenCreator = obj->GetChildrenCreator();
        mmLayerExplorerDetailsMulti* pLayerExplorerDetailsMulti = (mmLayerExplorerDetailsMulti*)(pChildrenCreator->obj);
        struct mmLayerUtilityItemPool* pEntryPool = &pLayerExplorerDetailsMulti->hEntryPool;
        mmLayerExplorerEntry* pLayerEntry = (mmLayerExplorerEntry*)mmLayerUtilityItemPool_Produce(pEntryPool);
        pLayerEntry->SetIndex(index);
        pLayerExplorerDetailsMulti->CreateEntryEvent(pLayerEntry);
        return pLayerEntry;
    }
    static void __static_ChildrenCreatorExplorerDetailsMulti_Recycle(mmWidgetScrollable* obj, CEGUI::Window* w)
    {
        const mmWidgetScrollableChildrenCreator* pChildrenCreator = obj->GetChildrenCreator();
        mmLayerExplorerDetailsMulti* pLayerExplorerDetailsMulti = (mmLayerExplorerDetailsMulti*)(pChildrenCreator->obj);
        struct mmLayerUtilityItemPool* pEntryPool = &pLayerExplorerDetailsMulti->hEntryPool;
        mmLayerExplorerEntry* pLayerEntry = (mmLayerExplorerEntry*)(w);
        pLayerExplorerDetailsMulti->DeleteEntryEvent(pLayerEntry);
        mmLayerUtilityItemPool_Recycle(pEntryPool, pLayerEntry);
    }

    static void __static_ChildrenCreatorExplorerDetailsMulti_FinishAttach(mmWidgetScrollable* obj, CEGUI::Window* w)
    {
        mmLayerExplorerEntry* pLayerEntry = (mmLayerExplorerEntry*)(w);
        pLayerEntry->OnFinishAttach();
    }

    static void __static_ChildrenCreatorExplorerDetailsMulti_BeforeDetach(mmWidgetScrollable* obj, CEGUI::Window* w)
    {
        mmLayerExplorerEntry* pLayerEntry = (mmLayerExplorerEntry*)(w);
        pLayerEntry->OnBeforeDetach();
    }

    const CEGUI::String mmLayerExplorerDetailsMulti::EventNamespace = "mm";
    const CEGUI::String mmLayerExplorerDetailsMulti::WidgetTypeName = "mm/mmLayerExplorerDetailsMulti";

    const CEGUI::String mmLayerExplorerDetailsMulti::EventLayerEntryMouseClick = "EventLayerEntryMouseClick";

    mmLayerExplorerDetailsMulti::mmLayerExplorerDetailsMulti(const CEGUI::String& type, const CEGUI::String& name)
        : mmLayerExplorerDetailsBase(type, name)
        , LayerExplorerDetailsMulti(NULL)
        , StaticImageBackground(NULL)

        , StaticImageIcon(NULL)

        , WidgetImageButton10(NULL)
        , WidgetImageButton11(NULL)
        , WidgetImageButton20(NULL)

        , WidgetScrollableYList(NULL)

        , pWidgetScrollableYList(NULL)
    {
        mmVectorVpt_Init(&this->hVectorVptItem);
        mmLayerUtilityItemPool_Init(&this->hEntryPool);
        mmRbtsetVpt_Init(&this->hLayerEntrySelect);
        mmRbtsetVpt_Init(&this->hEntrySelect);
        //
        mmLayerUtilityItemPool_SetType(&this->hEntryPool, "mm/mmLayerExplorerEntry");
    }
    mmLayerExplorerDetailsMulti::~mmLayerExplorerDetailsMulti(void)
    {
        mmVectorVpt_Destroy(&this->hVectorVptItem);
        mmLayerUtilityItemPool_Destroy(&this->hEntryPool);
        mmRbtsetVpt_Destroy(&this->hLayerEntrySelect);
        mmRbtsetVpt_Destroy(&this->hEntrySelect);
    }
    void mmLayerExplorerDetailsMulti::OnFinishLaunching(void)
    {
        struct mmSurfaceMaster* pSurfaceMaster = this->pSurfaceMaster;

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        this->LayerExplorerDetailsMulti = pWindowManager->loadLayoutFromFile("explorer/LayerExplorerDetailsMulti.layout");
        this->addChild(this->LayerExplorerDetailsMulti);
        // Manual set whole area.
        CEGUI::URect hWholeArea(CEGUI::UDim(0, 0), CEGUI::UDim(0, 0), CEGUI::UDim(1, 0), CEGUI::UDim(1, 0));
        this->LayerExplorerDetailsMulti->setArea(hWholeArea);

        this->StaticImageBackground = this->LayerExplorerDetailsMulti->getChild("StaticImageBackground");

        this->StaticImageIcon = this->StaticImageBackground->getChild("StaticImageIcon");

        this->WidgetImageButton10 = this->StaticImageBackground->getChild("WidgetImageButton10");
        this->WidgetImageButton11 = this->StaticImageBackground->getChild("WidgetImageButton11");
        this->WidgetImageButton20 = this->StaticImageBackground->getChild("WidgetImageButton20");

        this->WidgetScrollableYList = this->StaticImageBackground->getChild("WidgetScrollableYList");

        this->WidgetImageButton10->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsMulti::OnWidgetImageButton10EventMouseClick, this));
        this->WidgetImageButton11->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsMulti::OnWidgetImageButton11EventMouseClick, this));
        this->WidgetImageButton20->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsMulti::OnWidgetImageButton20EventMouseClick, this));
        //
        this->pWidgetScrollableYList = (mmWidgetScrollable*)this->WidgetScrollableYList;

        mmLayerUtilityItemPool_OnFinishLaunching(&this->hEntryPool);

        struct mmWidgetScrollableChildrenCreator hChildrenCreator;
        hChildrenCreator.obj = this;
        hChildrenCreator.Produce = &__static_ChildrenCreatorExplorerDetailsMulti_Produce;
        hChildrenCreator.Recycle = &__static_ChildrenCreatorExplorerDetailsMulti_Recycle;
        hChildrenCreator.FinishAttach = &__static_ChildrenCreatorExplorerDetailsMulti_FinishAttach;
        hChildrenCreator.BeforeDetach = &__static_ChildrenCreatorExplorerDetailsMulti_BeforeDetach;
        //
        this->pWidgetScrollableYList->SetChildrenCreator(&hChildrenCreator);
        this->pWidgetScrollableYList->OnFinishLaunching();

        this->hEventCursorMovedConn = this->subscribeEvent(mmLayerContext::EventCursorMoved, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsMulti::OnHandleEventCursorMoved, this));
        this->hEventCursorBeganConn = this->subscribeEvent(mmLayerContext::EventCursorBegan, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsMulti::OnHandleEventCursorBegan, this));
        this->hEventCursorEndedConn = this->subscribeEvent(mmLayerContext::EventCursorEnded, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsMulti::OnHandleEventCursorEnded, this));

        this->pWidgetScrollableYList->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsMulti::OnWidgetScrollableYListEventMouseClick, this));
    }

    void mmLayerExplorerDetailsMulti::OnBeforeTerminate(void)
    {
        struct mmSurfaceMaster* pSurfaceMaster = this->pSurfaceMaster;

        this->hEventCursorMovedConn->disconnect();
        this->hEventCursorBeganConn->disconnect();
        this->hEventCursorEndedConn->disconnect();

        this->pWidgetScrollableYList->OnBeforeTerminate();
        this->pWidgetScrollableYList = NULL;

        mmLayerUtilityItemPool_OnBeforeTerminate(&this->hEntryPool);

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        this->removeChild(this->LayerExplorerDetailsMulti);
        pWindowManager->destroyWindow(this->LayerExplorerDetailsMulti);
        this->LayerExplorerDetailsMulti = NULL;
    }
    void mmLayerExplorerDetailsMulti::UpdateLayerValue(void)
    {
        this->UpdateMultiVectorItem();

        this->RefreshMultiVector();

        this->UpdateUtf8View();
    }
    void mmLayerExplorerDetailsMulti::UpdateUtf8View(void)
    {
        // item view.
        struct mmRbtreeU64Vpt* pWindowRbtree = this->pWidgetScrollableYList->GetWindowRbtree();

        CEGUI::Window* child = NULL;
        struct mmRbNode* n = NULL;
        struct mmRbtreeU64VptIterator* it = NULL;
        mmLayerExplorerEntry* pLayerItem = NULL;
        // max
        n = mmRb_First(&pWindowRbtree->rbt);
        while (NULL != n)
        {
            it = (struct mmRbtreeU64VptIterator*)mmRb_Entry(n, struct mmRbtreeU64VptIterator, n);
            // next
            n = mmRb_Next(n);

            child = (CEGUI::Window*)it->v;

            pLayerItem = (mmLayerExplorerEntry*)child;

            pLayerItem->UpdateUtf8View();
        }
    }
    void mmLayerExplorerDetailsMulti::UpdateMultiVectorItem(void)
    {
        struct mmRbNode* n = NULL;
        struct mmRbtsetVptIterator* it = NULL;

        size_t index = 0;

        mmVectorVpt_AlignedMemory(&this->hVectorVptItem, this->pItemSelect->size);

        // notice:
        // beause we use vector pool for struct mmExplorerItem alloc.
        // so the pointer order from small to large, is directory to regular.

        // min
        n = mmRb_Last(&this->pItemSelect->rbt);
        while (NULL != n)
        {
            it = (struct mmRbtsetVptIterator*)mmRb_Entry(n, struct mmRbtsetVptIterator, n);
            // prev
            n = mmRb_Prev(n);

            mmVectorVpt_SetIndex(&this->hVectorVptItem, index, it->k);

            index++;
        }
    }
    void mmLayerExplorerDetailsMulti::RefreshMultiVector(void)
    {
        this->pWidgetScrollableYList->SetIndexSize(this->hVectorVptItem.size);
        this->pWidgetScrollableYList->NotifyAttributesChanged();
        this->pWidgetScrollableYList->NotifyTrimChildrenNode();

        this->UpdateEntryEventCurrent();
    }
    void mmLayerExplorerDetailsMulti::RefreshLayerEntryEvent(void)
    {
        CEGUI::WindowEventArgs hArgs(this);
        this->fireEvent(EventLayerEntryMouseClick, hArgs, EventNamespace);
    }
    void mmLayerExplorerDetailsMulti::CreateEntryEvent(mmLayerExplorerEntry* pLayerEntry)
    {
        struct mmExplorerFinder* pExplorerFinder = this->pExplorerFinder;
        struct mmExplorerItem* pItem = (struct mmExplorerItem*)mmVectorVpt_GetIndexReference(&this->hVectorVptItem, pLayerEntry->hIndex);

        pLayerEntry->SetIconvContext(this->pIconvContext);
        pLayerEntry->SetExplorerImage(this->pExplorerImage);

        pLayerEntry->hEventLayerEntryMouseClickConn = pLayerEntry->subscribeEvent(
            mmLayerExplorerEntry::EventLayerEntryMouseClick,
            CEGUI::Event::Subscriber(&mmLayerExplorerDetailsMulti::OnLayerEntryEventLayerEntryMouseClick, this));

        pLayerEntry->hEventLayerEntryImageMouseClickConn = pLayerEntry->subscribeEvent(
            mmLayerExplorerEntry::EventLayerEntryImageMouseClick,
            CEGUI::Event::Subscriber(&mmLayerExplorerDetailsMulti::OnLayerEntryEventLayerEntryImageMouseClick, this));

        this->UpdateEntryEvent(pLayerEntry);
    }
    void mmLayerExplorerDetailsMulti::DeleteEntryEvent(mmLayerExplorerEntry* pLayerEntry)
    {
        mmRbtsetVpt_Rmv(&this->hLayerEntrySelect, pLayerEntry);
        mmRbtsetVpt_Rmv(&this->hEntrySelect, pLayerEntry->pItem);
        pLayerEntry->SetIsSelect(false);
        pLayerEntry->SetItem(NULL);
        pLayerEntry->hEventLayerEntryMouseClickConn->disconnect();
        pLayerEntry->hEventLayerEntryImageMouseClickConn->disconnect();
    }
    void mmLayerExplorerDetailsMulti::UpdateEntryEvent(mmLayerExplorerEntry* pLayerEntry)
    {
        struct mmExplorerFinder* pExplorerFinder = this->pExplorerFinder;
        struct mmExplorerItem* pItem = (struct mmExplorerItem*)mmVectorVpt_GetIndexReference(&this->hVectorVptItem, pLayerEntry->hIndex);

        mmExplorerImage_ItemType(this->pExplorerImage, pItem);

        pLayerEntry->SetItem(pItem);
        pLayerEntry->UpdateLayerValue();
    }
    void mmLayerExplorerDetailsMulti::UpdateEntryEventCurrent(void)
    {
        // item view.
        struct mmRbtreeU64Vpt* pWindowRbtree = this->pWidgetScrollableYList->GetWindowRbtree();


        CEGUI::Window* child = NULL;
        struct mmRbNode* n = NULL;
        struct mmRbtreeU64VptIterator* it = NULL;
        mmLayerExplorerEntry* pLayerEntry = NULL;
        // max
        n = mmRb_First(&pWindowRbtree->rbt);
        while (NULL != n)
        {
            it = (struct mmRbtreeU64VptIterator*)mmRb_Entry(n, struct mmRbtreeU64VptIterator, n);
            // next
            n = mmRb_Next(n);

            child = (CEGUI::Window*)it->v;

            pLayerEntry = (mmLayerExplorerEntry*)child;

            this->UpdateEntryEvent(pLayerEntry);
        }
    }
    void mmLayerExplorerDetailsMulti::UnselectEntryVector(void)
    {
        mmLayerExplorerEntry* e = NULL;
        struct mmRbNode* n = NULL;
        struct mmRbtsetVptIterator* it = NULL;
        // max
        n = mmRb_First(&this->hLayerEntrySelect.rbt);
        while (NULL != n)
        {
            it = (struct mmRbtsetVptIterator*)mmRb_Entry(n, struct mmRbtsetVptIterator, n);
            // next
            n = mmRb_Next(n);

            e = (mmLayerExplorerEntry*)it->k;

            mmRbtsetVpt_Erase(&this->hLayerEntrySelect, it);
            mmRbtsetVpt_Rmv(&this->hEntrySelect, e->pItem);

            e->SetIsSelect(false);
        }
    }
    bool mmLayerExplorerDetailsMulti::OnWidgetImageButton10EventMouseClick(const CEGUI::EventArgs& args)
    {
        mmExplorerCommand_Copy(this->pExplorerCommand, this->pItemSelect);

        static const char* message = "Copy File";
        this->LoggerView(MM_LOG_INFO, 0, message, message);
        return true;
    }
    bool mmLayerExplorerDetailsMulti::OnWidgetImageButton11EventMouseClick(const CEGUI::EventArgs& args)
    {
        mmExplorerCommand_Cut(this->pExplorerCommand, this->pItemSelect);

        static const char* message = "Cut File";
        this->LoggerView(MM_LOG_INFO, 0, message, message);
        return true;
    }
    bool mmLayerExplorerDetailsMulti::OnWidgetImageButton20EventMouseClick(const CEGUI::EventArgs& args)
    {
        this->RemoveItemLayerEnsure(&mmLayerExplorerDetailsBase::OnLayerEnsureEventChoiceRemoveVector);
        return true;
    }
    bool mmLayerExplorerDetailsMulti::OnHandleEventCursorMoved(const CEGUI::EventArgs& args)
    {
        const mmCursorEventArgs& evt = (const mmCursorEventArgs&)(args);
        this->pWidgetScrollableYList->OnEventCursorMoved(evt.content);
        return false;
    }
    bool mmLayerExplorerDetailsMulti::OnHandleEventCursorBegan(const CEGUI::EventArgs& args)
    {
        const mmCursorEventArgs& evt = (const mmCursorEventArgs&)(args);
        this->pWidgetScrollableYList->OnEventCursorBegan(evt.content);
        return false;
    }
    bool mmLayerExplorerDetailsMulti::OnHandleEventCursorEnded(const CEGUI::EventArgs& args)
    {
        const mmCursorEventArgs& evt = (const mmCursorEventArgs&)(args);
        this->pWidgetScrollableYList->OnEventCursorEnded(evt.content);
        return false;
    }
    bool mmLayerExplorerDetailsMulti::OnLayerEntryEventLayerEntryMouseClick(const CEGUI::EventArgs& args)
    {
        const CEGUI::MouseEventArgs& evt = (const CEGUI::MouseEventArgs&)(args);

        mmLayerExplorerEntry* pLayerEntry = (mmLayerExplorerEntry*)(evt.window);

        this->UnselectEntryVector();

        if (pLayerEntry->hIsSelect)
        {
            mmRbtsetVpt_Add(&this->hLayerEntrySelect, pLayerEntry);
            mmRbtsetVpt_Add(&this->hEntrySelect, pLayerEntry->pItem);
        }
        else
        {
            mmRbtsetVpt_Rmv(&this->hLayerEntrySelect, pLayerEntry);
            mmRbtsetVpt_Rmv(&this->hEntrySelect, pLayerEntry->pItem);
        }

        this->RefreshLayerEntryEvent();

        return true;
    }
    bool mmLayerExplorerDetailsMulti::OnLayerEntryEventLayerEntryImageMouseClick(const CEGUI::EventArgs& args)
    {
        const CEGUI::MouseEventArgs& evt = (const CEGUI::MouseEventArgs&)(args);

        mmLayerExplorerEntry* pLayerEntry = (mmLayerExplorerEntry*)(evt.window);

        if (pLayerEntry->hIsSelect)
        {
            mmRbtsetVpt_Add(&this->hLayerEntrySelect, pLayerEntry);
            mmRbtsetVpt_Add(&this->hEntrySelect, pLayerEntry->pItem);
        }
        else
        {
            mmRbtsetVpt_Rmv(&this->hLayerEntrySelect, pLayerEntry);
            mmRbtsetVpt_Rmv(&this->hEntrySelect, pLayerEntry->pItem);
        }

        this->RefreshLayerEntryEvent();

        return true;
    }
    bool mmLayerExplorerDetailsMulti::OnWidgetScrollableYListEventMouseClick(const CEGUI::EventArgs& args)
    {
        this->UnselectEntryVector();
        this->RefreshLayerEntryEvent();
        return true;
    }
}
