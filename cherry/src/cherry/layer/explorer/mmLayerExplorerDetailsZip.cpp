/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerExplorerDetailsZip.h"
#include "mmLayerExplorerEntry.h"

#include "CEGUI/WindowManager.h"
#include "CEGUI/widgets/Editbox.h"

#include "explorer/mmExplorerCommand.h"
#include "explorer/mmExplorerImage.h"

#include "layer/utility/mmLayerUtilityIconv.h"

#include "layer/widgets/mmWidgetScrollable.h"

namespace mm
{
    static CEGUI::Window* __static_ChildrenCreatorExplorerDetailsZip_Produce(mmWidgetScrollable* obj, size_t index)
    {
        const mmWidgetScrollableChildrenCreator* pChildrenCreator = obj->GetChildrenCreator();
        mmLayerExplorerDetailsZip* pLayerExplorerDetailsZip = (mmLayerExplorerDetailsZip*)(pChildrenCreator->obj);
        struct mmLayerUtilityItemPool* pEntryPool = &pLayerExplorerDetailsZip->hEntryPool;
        mmLayerExplorerEntry* pLayerEntry = (mmLayerExplorerEntry*)mmLayerUtilityItemPool_Produce(pEntryPool);
        pLayerEntry->SetIndex(index);
        pLayerExplorerDetailsZip->CreateEntryEvent(pLayerEntry);
        return pLayerEntry;
    }
    static void __static_ChildrenCreatorExplorerDetailsZip_Recycle(mmWidgetScrollable* obj, CEGUI::Window* w)
    {
        const mmWidgetScrollableChildrenCreator* pChildrenCreator = obj->GetChildrenCreator();
        mmLayerExplorerDetailsZip* pLayerExplorerDetailsZip = (mmLayerExplorerDetailsZip*)(pChildrenCreator->obj);
        struct mmLayerUtilityItemPool* pEntryPool = &pLayerExplorerDetailsZip->hEntryPool;
        mmLayerExplorerEntry* pLayerEntry = (mmLayerExplorerEntry*)(w);
        pLayerExplorerDetailsZip->DeleteEntryEvent(pLayerEntry);
        mmLayerUtilityItemPool_Recycle(pEntryPool, pLayerEntry);
    }

    static void __static_ChildrenCreatorExplorerDetailsZip_FinishAttach(mmWidgetScrollable* obj, CEGUI::Window* w)
    {
        mmLayerExplorerEntry* pLayerEntry = (mmLayerExplorerEntry*)(w);
        pLayerEntry->OnFinishAttach();
    }

    static void __static_ChildrenCreatorExplorerDetailsZip_BeforeDetach(mmWidgetScrollable* obj, CEGUI::Window* w)
    {
        mmLayerExplorerEntry* pLayerEntry = (mmLayerExplorerEntry*)(w);
        pLayerEntry->OnBeforeDetach();
    }

    const CEGUI::String mmLayerExplorerDetailsZip::EventNamespace = "mm";
    const CEGUI::String mmLayerExplorerDetailsZip::WidgetTypeName = "mm/mmLayerExplorerDetailsZip";

    const CEGUI::String mmLayerExplorerDetailsZip::EventLayerEntryMouseClick = "EventLayerEntryMouseClick";

    mmLayerExplorerDetailsZip::mmLayerExplorerDetailsZip(const CEGUI::String& type, const CEGUI::String& name)
        : mmLayerExplorerDetailsBase(type, name)
        , LayerExplorerDetailsZip(NULL)
        , StaticImageBackground(NULL)

        , EditboxBasename(NULL)
        , StaticImageIcon(NULL)
        , StaticImageEmbellish(NULL)

        , WidgetImageButton00(NULL)
        , WidgetImageButton10(NULL)
        , WidgetImageButton11(NULL)
        , WidgetImageButton20(NULL)
        , WidgetImageButton21(NULL)

        , WidgetScrollableYList(NULL)

        , pEditboxBasename(NULL)

        , pWidgetScrollableYList(NULL)
    {
        mmString_Init(&this->hTextBaseNameHost);
        mmExplorerItemZip_Init(&this->hItemZip);
        mmLayerUtilityItemPool_Init(&this->hEntryPool);
        mmRbtsetVpt_Init(&this->hLayerEntrySelect);
        mmRbtsetVpt_Init(&this->hEntrySelect);
        //
        mmLayerUtilityItemPool_SetType(&this->hEntryPool, "mm/mmLayerExplorerEntry");
    }
    mmLayerExplorerDetailsZip::~mmLayerExplorerDetailsZip(void)
    {
        mmString_Destroy(&this->hTextBaseNameHost);
        mmExplorerItemZip_Destroy(&this->hItemZip);
        mmLayerUtilityItemPool_Destroy(&this->hEntryPool);
        mmRbtsetVpt_Destroy(&this->hLayerEntrySelect);
        mmRbtsetVpt_Destroy(&this->hEntrySelect);
    }
    void mmLayerExplorerDetailsZip::OnFinishLaunching(void)
    {
        struct mmSurfaceMaster* pSurfaceMaster = this->pSurfaceMaster;

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        this->LayerExplorerDetailsZip = pWindowManager->loadLayoutFromFile("explorer/LayerExplorerDetailsZip.layout");
        this->addChild(this->LayerExplorerDetailsZip);
        // Manual set whole area.
        CEGUI::URect hWholeArea(CEGUI::UDim(0, 0), CEGUI::UDim(0, 0), CEGUI::UDim(1, 0), CEGUI::UDim(1, 0));
        this->LayerExplorerDetailsZip->setArea(hWholeArea);

        this->StaticImageBackground = this->LayerExplorerDetailsZip->getChild("StaticImageBackground");

        this->EditboxBasename = this->StaticImageBackground->getChild("EditboxBasename");
        this->StaticImageIcon = this->StaticImageBackground->getChild("StaticImageIcon");
        this->StaticImageEmbellish = this->StaticImageBackground->getChild("StaticImageEmbellish");

        this->WidgetImageButton00 = this->StaticImageBackground->getChild("WidgetImageButton00");
        this->WidgetImageButton10 = this->StaticImageBackground->getChild("WidgetImageButton10");
        this->WidgetImageButton11 = this->StaticImageBackground->getChild("WidgetImageButton11");
        this->WidgetImageButton20 = this->StaticImageBackground->getChild("WidgetImageButton20");
        this->WidgetImageButton21 = this->StaticImageBackground->getChild("WidgetImageButton21");

        this->WidgetScrollableYList = this->StaticImageBackground->getChild("WidgetScrollableYList");

        this->WidgetImageButton00->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsZip::OnWidgetImageButton00EventMouseClick, this));
        this->WidgetImageButton10->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsZip::OnWidgetImageButton10EventMouseClick, this));
        this->WidgetImageButton11->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsZip::OnWidgetImageButton11EventMouseClick, this));
        this->WidgetImageButton20->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsZip::OnWidgetImageButton20EventMouseClick, this));
        this->WidgetImageButton21->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsZip::OnWidgetImageButton21EventMouseClick, this));

        this->pEditboxBasename = (CEGUI::Editbox*)this->EditboxBasename;
        this->pWidgetScrollableYList = (mmWidgetScrollable*)this->WidgetScrollableYList;

        this->pEditboxBasename->subscribeEvent(CEGUI::Editbox::EventTextAccepted, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsZip::OnEditboxBasenameEventTextAccepted, this));

        mmLayerUtilityItemPool_OnFinishLaunching(&this->hEntryPool);

        struct mmWidgetScrollableChildrenCreator hChildrenCreator;
        hChildrenCreator.obj = this;
        hChildrenCreator.Produce = &__static_ChildrenCreatorExplorerDetailsZip_Produce;
        hChildrenCreator.Recycle = &__static_ChildrenCreatorExplorerDetailsZip_Recycle;
        hChildrenCreator.FinishAttach = &__static_ChildrenCreatorExplorerDetailsZip_FinishAttach;
        hChildrenCreator.BeforeDetach = &__static_ChildrenCreatorExplorerDetailsZip_BeforeDetach;
        //
        this->pWidgetScrollableYList->SetChildrenCreator(&hChildrenCreator);
        this->pWidgetScrollableYList->OnFinishLaunching();

        this->hEventCursorMovedConn = this->subscribeEvent(mmLayerContext::EventCursorMoved, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsZip::OnHandleEventCursorMoved, this));
        this->hEventCursorBeganConn = this->subscribeEvent(mmLayerContext::EventCursorBegan, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsZip::OnHandleEventCursorBegan, this));
        this->hEventCursorEndedConn = this->subscribeEvent(mmLayerContext::EventCursorEnded, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsZip::OnHandleEventCursorEnded, this));

        this->pWidgetScrollableYList->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsZip::OnWidgetScrollableYListEventMouseClick, this));
    }

    void mmLayerExplorerDetailsZip::OnBeforeTerminate(void)
    {
        struct mmSurfaceMaster* pSurfaceMaster = this->pSurfaceMaster;

        this->hEventCursorMovedConn->disconnect();
        this->hEventCursorBeganConn->disconnect();
        this->hEventCursorEndedConn->disconnect();

        this->pWidgetScrollableYList->OnBeforeTerminate();
        this->pWidgetScrollableYList = NULL;

        mmLayerUtilityItemPool_OnBeforeTerminate(&this->hEntryPool);

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        this->removeChild(this->LayerExplorerDetailsZip);
        pWindowManager->destroyWindow(this->LayerExplorerDetailsZip);
        this->LayerExplorerDetailsZip = NULL;
    }
    void mmLayerExplorerDetailsZip::UpdateLayerValue(void)
    {
        this->UpdateUtf8View();

        this->StaticImageEmbellish->setVisible(true);
        this->WidgetScrollableYList->setVisible(false);
    }
    void mmLayerExplorerDetailsZip::UpdateUtf8View(void)
    {
        // url view.
        this->UpdateUrlUtf8View();

        // item view.
        struct mmRbtreeU64Vpt* pWindowRbtree = this->pWidgetScrollableYList->GetWindowRbtree();

        CEGUI::Window* child = NULL;
        struct mmRbNode* n = NULL;
        struct mmRbtreeU64VptIterator* it = NULL;
        mmLayerExplorerEntry* _layer_item = NULL;
        // max
        n = mmRb_First(&pWindowRbtree->rbt);
        while (NULL != n)
        {
            it = (struct mmRbtreeU64VptIterator*)mmRb_Entry(n, struct mmRbtreeU64VptIterator, n);
            // next
            n = mmRb_Next(n);

            child = (CEGUI::Window*)it->v;

            _layer_item = (mmLayerExplorerEntry*)child;

            _layer_item->UpdateUtf8View();
        }
    }
    void mmLayerExplorerDetailsZip::UpdateUrlUtf8View(void)
    {
        struct mmIconvContext* pIconvContext = this->pIconvContext;

        mmLayerUtilityIconv_ViewName(pIconvContext, this->EditboxBasename, &this->pItem->basename);
    }
    void mmLayerExplorerDetailsZip::RefreshZipVector(void)
    {
        struct mmVectorValue* pVectorItem = &this->hItemZip.vector_item;

        mmExplorerItemZip_SetPath(&this->hItemZip, mmString_CStr(&this->pItem->pathname), mmString_CStr(&this->pItem->basename));
        mmExplorerItemZip_Refresh(&this->hItemZip);

        this->pWidgetScrollableYList->SetIndexSize(pVectorItem->size);
        this->pWidgetScrollableYList->NotifyAttributesChanged();
        this->pWidgetScrollableYList->NotifyTrimChildrenNode();

        this->UpdateEntryEventCurrent();
    }
    void mmLayerExplorerDetailsZip::RefreshLayerEntryEvent(void)
    {
        CEGUI::WindowEventArgs hArgs(this);
        this->fireEvent(EventLayerEntryMouseClick, hArgs, EventNamespace);
    }
    void mmLayerExplorerDetailsZip::CreateEntryEvent(mmLayerExplorerEntry* pLayerEntry)
    {
        struct mmExplorerFinder* pExplorerFinder = this->pExplorerFinder;
        struct mmVectorValue* pVectorItem = &this->hItemZip.vector_item;
        struct mmExplorerItem* pItem = (struct mmExplorerItem*)mmVectorValue_GetIndexReference(pVectorItem, pLayerEntry->hIndex);

        pLayerEntry->SetIconvContext(this->pIconvContext);
        pLayerEntry->SetExplorerImage(this->pExplorerImage);

        pLayerEntry->hEventLayerEntryMouseClickConn = pLayerEntry->subscribeEvent(
            mmLayerExplorerEntry::EventLayerEntryMouseClick,
            CEGUI::Event::Subscriber(&mmLayerExplorerDetailsZip::OnLayerEntryEventLayerEntryMouseClick, this));

        pLayerEntry->hEventLayerEntryImageMouseClickConn = pLayerEntry->subscribeEvent(
            mmLayerExplorerEntry::EventLayerEntryImageMouseClick,
            CEGUI::Event::Subscriber(&mmLayerExplorerDetailsZip::OnLayerEntryEventLayerEntryImageMouseClick, this));

        this->UpdateEntryEvent(pLayerEntry);
    }
    void mmLayerExplorerDetailsZip::DeleteEntryEvent(mmLayerExplorerEntry* pLayerEntry)
    {
        mmRbtsetVpt_Rmv(&this->hLayerEntrySelect, pLayerEntry);
        mmRbtsetVpt_Rmv(&this->hEntrySelect, pLayerEntry->pItem);
        pLayerEntry->SetIsSelect(false);
        pLayerEntry->SetItem(NULL);
        pLayerEntry->hEventLayerEntryMouseClickConn->disconnect();
        pLayerEntry->hEventLayerEntryImageMouseClickConn->disconnect();
    }
    void mmLayerExplorerDetailsZip::UpdateEntryEvent(mmLayerExplorerEntry* pLayerEntry)
    {
        struct mmExplorerFinder* pExplorerFinder = this->pExplorerFinder;
        struct mmVectorValue* pVectorItem = &this->hItemZip.vector_item;
        struct mmExplorerItem* pItem = (struct mmExplorerItem*)mmVectorValue_GetIndexReference(pVectorItem, pLayerEntry->hIndex);

        mmExplorerImage_ItemType(this->pExplorerImage, pItem);

        pLayerEntry->SetItem(pItem);
        pLayerEntry->UpdateLayerValue();
    }
    void mmLayerExplorerDetailsZip::UpdateEntryEventCurrent(void)
    {
        // item view.
        struct mmRbtreeU64Vpt* pWindowRbtree = this->pWidgetScrollableYList->GetWindowRbtree();

        CEGUI::Window* child = NULL;
        struct mmRbNode* n = NULL;
        struct mmRbtreeU64VptIterator* it = NULL;
        mmLayerExplorerEntry* pLayerEntry = NULL;
        // max
        n = mmRb_First(&pWindowRbtree->rbt);
        while (NULL != n)
        {
            it = (struct mmRbtreeU64VptIterator*)mmRb_Entry(n, struct mmRbtreeU64VptIterator, n);
            // next
            n = mmRb_Next(n);

            child = (CEGUI::Window*)it->v;

            pLayerEntry = (mmLayerExplorerEntry*)child;

            this->UpdateEntryEvent(pLayerEntry);
        }
    }
    void mmLayerExplorerDetailsZip::UnselectEntryVector(void)
    {
        mmLayerExplorerEntry* e = NULL;
        struct mmRbNode* n = NULL;
        struct mmRbtsetVptIterator* it = NULL;
        // max
        n = mmRb_First(&this->hLayerEntrySelect.rbt);
        while (NULL != n)
        {
            it = (struct mmRbtsetVptIterator*)mmRb_Entry(n, struct mmRbtsetVptIterator, n);
            // next
            n = mmRb_Next(n);

            e = (mmLayerExplorerEntry*)it->k;

            mmRbtsetVpt_Erase(&this->hLayerEntrySelect, it);
            mmRbtsetVpt_Rmv(&this->hEntrySelect, e->pItem);

            e->SetIsSelect(false);
        }
    }
    bool mmLayerExplorerDetailsZip::OnWidgetImageButton00EventMouseClick(const CEGUI::EventArgs& args)
    {
        struct mmIconvContext* pIconvContext = this->pIconvContext;
        mmLayerUtilityIconv_HostName(pIconvContext, this->EditboxBasename, &this->hTextBaseNameHost);

        mmExplorerCommand_RenameItem(this->pExplorerCommand, this->pItem, mmString_CStr(&this->hTextBaseNameHost));
        mmExplorerCommand_Refresh(this->pExplorerCommand, mmString_CStr(&this->hDirectory));

        static const char* message = "Rename File";
        this->LoggerView(MM_LOG_INFO, 0, message, message);
        return true;
    }
    bool mmLayerExplorerDetailsZip::OnWidgetImageButton10EventMouseClick(const CEGUI::EventArgs& args)
    {
        mmExplorerCommand_Copy(this->pExplorerCommand, this->pItemSelect);

        static const char* message = "Copy File";
        this->LoggerView(MM_LOG_INFO, 0, message, message);
        return true;
    }
    bool mmLayerExplorerDetailsZip::OnWidgetImageButton11EventMouseClick(const CEGUI::EventArgs& args)
    {
        mmExplorerCommand_Cut(this->pExplorerCommand, this->pItemSelect);

        static const char* message = "Cut File";
        this->LoggerView(MM_LOG_INFO, 0, message, message);
        return true;
    }
    bool mmLayerExplorerDetailsZip::OnWidgetImageButton20EventMouseClick(const CEGUI::EventArgs& args)
    {
        this->RemoveItemLayerEnsure(&mmLayerExplorerDetailsBase::OnLayerEnsureEventChoiceRemove);
        return true;
    }
    bool mmLayerExplorerDetailsZip::OnWidgetImageButton21EventMouseClick(const CEGUI::EventArgs& args)
    {
        this->RefreshZipVector();

        this->StaticImageEmbellish->setVisible(false);
        this->WidgetScrollableYList->setVisible(true);

        mmExplorerCommand_Preview(this->pExplorerCommand, this->pItem);
        return true;
    }
    bool mmLayerExplorerDetailsZip::OnEditboxBasenameEventTextAccepted(const CEGUI::EventArgs& args)
    {
        this->OnWidgetImageButton00EventMouseClick(args);
        return true;
    }
    bool mmLayerExplorerDetailsZip::OnHandleEventCursorMoved(const CEGUI::EventArgs& args)
    {
        const mmCursorEventArgs& evt = (const mmCursorEventArgs&)(args);
        this->pWidgetScrollableYList->OnEventCursorMoved(evt.content);
        return false;
    }
    bool mmLayerExplorerDetailsZip::OnHandleEventCursorBegan(const CEGUI::EventArgs& args)
    {
        const mmCursorEventArgs& evt = (const mmCursorEventArgs&)(args);
        this->pWidgetScrollableYList->OnEventCursorBegan(evt.content);
        return false;
    }
    bool mmLayerExplorerDetailsZip::OnHandleEventCursorEnded(const CEGUI::EventArgs& args)
    {
        const mmCursorEventArgs& evt = (const mmCursorEventArgs&)(args);
        this->pWidgetScrollableYList->OnEventCursorEnded(evt.content);
        return false;
    }
    bool mmLayerExplorerDetailsZip::OnLayerEntryEventLayerEntryMouseClick(const CEGUI::EventArgs& args)
    {
        const CEGUI::MouseEventArgs& evt = (const CEGUI::MouseEventArgs&)(args);

        mmLayerExplorerEntry* pLayerEntry = (mmLayerExplorerEntry*)(evt.window);

        this->UnselectEntryVector();

        if (pLayerEntry->hIsSelect)
        {
            mmRbtsetVpt_Add(&this->hLayerEntrySelect, pLayerEntry);
            mmRbtsetVpt_Add(&this->hEntrySelect, pLayerEntry->pItem);
        }
        else
        {
            mmRbtsetVpt_Rmv(&this->hLayerEntrySelect, pLayerEntry);
            mmRbtsetVpt_Rmv(&this->hEntrySelect, pLayerEntry->pItem);
        }

        this->RefreshLayerEntryEvent();

        return true;
    }
    bool mmLayerExplorerDetailsZip::OnLayerEntryEventLayerEntryImageMouseClick(const CEGUI::EventArgs& args)
    {
        const CEGUI::MouseEventArgs& evt = (const CEGUI::MouseEventArgs&)(args);

        mmLayerExplorerEntry* pLayerEntry = (mmLayerExplorerEntry*)(evt.window);

        if (pLayerEntry->hIsSelect)
        {
            mmRbtsetVpt_Add(&this->hLayerEntrySelect, pLayerEntry);
            mmRbtsetVpt_Add(&this->hEntrySelect, pLayerEntry->pItem);
        }
        else
        {
            mmRbtsetVpt_Rmv(&this->hLayerEntrySelect, pLayerEntry);
            mmRbtsetVpt_Rmv(&this->hEntrySelect, pLayerEntry->pItem);
        }

        this->RefreshLayerEntryEvent();

        return true;
    }
    bool mmLayerExplorerDetailsZip::OnWidgetScrollableYListEventMouseClick(const CEGUI::EventArgs& args)
    {
        this->UnselectEntryVector();
        this->RefreshLayerEntryEvent();
        return true;
    }
}
