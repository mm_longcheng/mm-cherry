/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerExplorerDetailsNes.h"

#include "dish/mmFilePath.h"

#include "CEGUI/WindowManager.h"
#include "CEGUI/widgets/Editbox.h"

#include "explorer/mmExplorerCommand.h"
#include "explorer/mmExplorerFinder.h"

#include "layer/director/mmLayerDirector.h"

#include "layer/utility/mmLayerUtilityIconv.h"

#include "layer/emulator/mmLayerEmulatorMachine.h"

namespace mm
{
    const CEGUI::String mmLayerExplorerDetailsNes::EventNamespace = "mm";
    const CEGUI::String mmLayerExplorerDetailsNes::WidgetTypeName = "mm/mmLayerExplorerDetailsNes";

    mmLayerExplorerDetailsNes::mmLayerExplorerDetailsNes(const CEGUI::String& type, const CEGUI::String& name)
        : mmLayerExplorerDetailsBase(type, name)
        , LayerExplorerDetailsNes(NULL)
        , StaticImageBackground(NULL)

        , EditboxBasename(NULL)
        , StaticImageIcon(NULL)
        , StaticImagePreview(NULL)
        , StaticImageEmbellish(NULL)

        , WidgetImageButton00(NULL)
        , WidgetImageButton10(NULL)
        , WidgetImageButton11(NULL)
        , WidgetImageButton12(NULL)
        , WidgetImageButton20(NULL)
        , WidgetImageButton21(NULL)
        , WidgetImageButton22(NULL)
        , WidgetImageButton23(NULL)

        , pEditboxBasename(NULL)
    {
        mmString_Init(&this->hTextBaseNameHost);
        mmEmulatorAssets_Init(&this->hEmulatorAssets);
    }
    mmLayerExplorerDetailsNes::~mmLayerExplorerDetailsNes(void)
    {
        mmString_Destroy(&this->hTextBaseNameHost);
        mmEmulatorAssets_Destroy(&this->hEmulatorAssets);
    }
    void mmLayerExplorerDetailsNes::OnFinishLaunching(void)
    {
        struct mmSurfaceMaster* pSurfaceMaster = this->pSurfaceMaster;

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        this->LayerExplorerDetailsNes = pWindowManager->loadLayoutFromFile("explorer/LayerExplorerDetailsNes.layout");
        this->addChild(this->LayerExplorerDetailsNes);
        // Manual set whole area.
        CEGUI::URect hWholeArea(CEGUI::UDim(0, 0), CEGUI::UDim(0, 0), CEGUI::UDim(1, 0), CEGUI::UDim(1, 0));
        this->LayerExplorerDetailsNes->setArea(hWholeArea);

        this->StaticImageBackground = this->LayerExplorerDetailsNes->getChild("StaticImageBackground");

        this->EditboxBasename = this->StaticImageBackground->getChild("EditboxBasename");
        this->StaticImageIcon = this->StaticImageBackground->getChild("StaticImageIcon");
        this->StaticImagePreview = this->StaticImageBackground->getChild("StaticImagePreview");
        this->StaticImageEmbellish = this->StaticImageBackground->getChild("StaticImageEmbellish");

        this->WidgetImageButton00 = this->StaticImageBackground->getChild("WidgetImageButton00");
        this->WidgetImageButton10 = this->StaticImageBackground->getChild("WidgetImageButton10");
        this->WidgetImageButton11 = this->StaticImageBackground->getChild("WidgetImageButton11");
        this->WidgetImageButton12 = this->StaticImageBackground->getChild("WidgetImageButton12");
        this->WidgetImageButton20 = this->StaticImageBackground->getChild("WidgetImageButton20");
        this->WidgetImageButton21 = this->StaticImageBackground->getChild("WidgetImageButton21");
        this->WidgetImageButton22 = this->StaticImageBackground->getChild("WidgetImageButton22");
        this->WidgetImageButton23 = this->StaticImageBackground->getChild("WidgetImageButton23");

        this->WidgetImageButton00->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsNes::OnWidgetImageButton00EventMouseClick, this));
        this->WidgetImageButton10->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsNes::OnWidgetImageButton10EventMouseClick, this));
        this->WidgetImageButton11->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsNes::OnWidgetImageButton11EventMouseClick, this));
        this->WidgetImageButton12->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsNes::OnWidgetImageButton12EventMouseClick, this));
        this->WidgetImageButton20->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsNes::OnWidgetImageButton20EventMouseClick, this));
        this->WidgetImageButton21->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsNes::OnWidgetImageButton21EventMouseClick, this));
        this->WidgetImageButton22->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsNes::OnWidgetImageButton22EventMouseClick, this));
        this->WidgetImageButton23->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsNes::OnWidgetImageButton23EventMouseClick, this));

        this->pEditboxBasename = (CEGUI::Editbox*)this->EditboxBasename;

        this->pEditboxBasename->subscribeEvent(CEGUI::Editbox::EventTextAccepted, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsNes::OnEditboxBasenameEventTextAccepted, this));

        mmLayerEmulatorMachine* pLayerEmulatorMachine = NULL;
        pLayerEmulatorMachine = (mmLayerEmulatorMachine*)mmLayerDirector_SceneExclusiveBackground(this->pDirector, "mm/mmLayerEmulatorMachine", "mmLayerEmulatorMachine");
        this->hLayerEmulatorWindow.SetEmulatorMachine(&pLayerEmulatorMachine->hEmulatorMachine);
        this->hLayerEmulatorWindow.SetLayerContext(this);
        this->hLayerEmulatorWindow.SetWindowScreen(this->StaticImagePreview);
        this->hLayerEmulatorWindow.SetFingerIsVisible(false);
        this->hLayerEmulatorWindow.OnFinishLaunching();
        //
        this->hEventEmulatorPlayConn = pLayerEmulatorMachine->subscribeEvent(mmLayerEmulatorMachine::EventEmulatorPlay, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsNes::OnLayerEmulatorMachineEventEmulatorPlay, this));
        this->hEventEmulatorStopConn = pLayerEmulatorMachine->subscribeEvent(mmLayerEmulatorMachine::EventEmulatorStop, CEGUI::Event::Subscriber(&mmLayerExplorerDetailsNes::OnLayerEmulatorMachineEventEmulatorStop, this));
    }

    void mmLayerExplorerDetailsNes::OnBeforeTerminate(void)
    {
        struct mmSurfaceMaster* pSurfaceMaster = this->pSurfaceMaster;

        this->hEventEmulatorPlayConn->disconnect();
        this->hEventEmulatorStopConn->disconnect();
        //
        this->hLayerEmulatorWindow.OnBeforeTerminate();

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
        this->removeChild(this->LayerExplorerDetailsNes);
        pWindowManager->destroyWindow(this->LayerExplorerDetailsNes);
        this->LayerExplorerDetailsNes = NULL;
    }
    void mmLayerExplorerDetailsNes::UpdateLayerValue(void)
    {
        this->UpdateInformation();

        this->UpdateUtf8View();

        this->RandomEmbellishImage(this->StaticImageEmbellish);

        this->EmulatorPreviewPause();

        this->WidgetImageButton21->setVisible(true);
        this->WidgetImageButton22->setVisible(false);
        this->WidgetImageButton23->setVisible(false);

        this->StaticImagePreview->setVisible(false);
        this->StaticImageEmbellish->setVisible(true);
    }
    void mmLayerExplorerDetailsNes::UpdateUtf8View(void)
    {
        struct mmIconvContext* pIconvContext = this->pIconvContext;

        mmLayerUtilityIconv_ViewName(pIconvContext, this->EditboxBasename, &this->pItem->basename);
    }
    void mmLayerExplorerDetailsNes::OnEnterBackground(void)
    {
        this->EmulatorPreviewPause();
    }
    void mmLayerExplorerDetailsNes::OnEnterForeground(void)
    {
        // do nothing here.
    }
    void mmLayerExplorerDetailsNes::notifyScreenAreaChanged(bool recursive/*  = true */)
    {
        mmLayerContext::notifyScreenAreaChanged(recursive);
        this->hLayerEmulatorWindow.UpdateTransform();
    }
    void mmLayerExplorerDetailsNes::UpdateInformation(void)
    {
        struct mmPackageAssets* pPackageAssets = &this->pContextMaster->hPackageAssets;

        const char* pExternalFilesDirectory = mmString_CStr(&pPackageAssets->hExternalFilesDirectory);

        struct mmString qualified_name;
        struct mmString basename;
        struct mmString suffixname;

        mmString_Init(&qualified_name);
        mmString_Init(&basename);
        mmString_Init(&suffixname);

        mmString_Assigns(&qualified_name, mmString_CStr(&this->pItem->basename));

        mmPathSplitSuffixName(&qualified_name, &basename, &suffixname);

        mmEmulatorAssets_SetPath(&this->hEmulatorAssets, mmString_CStr(&this->pItem->pathname), mmString_CStr(&this->pItem->basename));
        mmEmulatorAssets_SetAssetsType(&this->hEmulatorAssets, MM_EMU_ASSETS_FOLDER);
        mmEmulatorAssets_SetRootPath(&this->hEmulatorAssets, mmString_CStr(&this->pItem->pathname));
        mmEmulatorAssets_SetAssetsName(&this->hEmulatorAssets, mmString_CStr(&this->pItem->basename));
        mmEmulatorAssets_SetSourceName(&this->hEmulatorAssets, mmString_CStr(&basename));
        mmEmulatorAssets_SetWritable(&this->hEmulatorAssets, pExternalFilesDirectory, "emulator");
        mmEmulatorAssets_SetFileContext(&this->hEmulatorAssets, &this->pExplorerFinder->file_context);

        mmString_Destroy(&qualified_name);
        mmString_Destroy(&basename);
        mmString_Destroy(&suffixname);
    }
    void mmLayerExplorerDetailsNes::EmulatorPreviewPlay(void)
    {
        mmLayerEmulatorMachine* pLayerEmulatorMachine = NULL;

        struct mmEmulatorMachine* pEmulatorMachine = NULL;

        pLayerEmulatorMachine = (mmLayerEmulatorMachine*)mmLayerDirector_SceneExclusiveBackground(this->pDirector, "mm/mmLayerEmulatorMachine", "mmLayerEmulatorMachine");

        pEmulatorMachine = &pLayerEmulatorMachine->hEmulatorMachine;

        if (pLayerEmulatorMachine->hCurrentEmulatorAssets != mmString_CStr(&this->pItem->fullname) ||
            MM_SUCCESS != mmEmulatorMachine_GetRomLoadCode(pEmulatorMachine))
        {
            mmEmulatorMachine_SetAssets(pEmulatorMachine, &this->hEmulatorAssets);

            mmEmulatorMachine_LoadRom(pEmulatorMachine);
            mmEmulatorMachine_Play(pEmulatorMachine);

            pLayerEmulatorMachine->hCurrentEmulatorAssets = mmString_CStr(&this->pItem->fullname);

            this->hLayerEmulatorWindow.SetFingerIsVisible(true);
        }
        else
        {
            if (MM_EMULATOR_PAUSED == mmEmulatorMachine_GetState(pEmulatorMachine) &&
                MM_SUCCESS == mmEmulatorMachine_GetRomLoadCode(pEmulatorMachine))
            {
                this->hLayerEmulatorWindow.SetFingerIsVisible(true);

                mmEmulatorMachine_Resume(pEmulatorMachine);
            }
        }

        this->WidgetImageButton21->setVisible(false);
        this->WidgetImageButton22->setVisible(true);
        this->WidgetImageButton23->setVisible(true);

        this->StaticImagePreview->setVisible(true);
        this->StaticImageEmbellish->setVisible(false);
    }
    void mmLayerExplorerDetailsNes::EmulatorPreviewPause(void)
    {
        mmLayerEmulatorMachine* pLayerEmulatorMachine = NULL;

        struct mmEmulatorMachine* pEmulatorMachine = NULL;

        pLayerEmulatorMachine = (mmLayerEmulatorMachine*)mmLayerDirector_SceneExclusiveBackground(this->pDirector, "mm/mmLayerEmulatorMachine", "mmLayerEmulatorMachine");

        pEmulatorMachine = &pLayerEmulatorMachine->hEmulatorMachine;

        if (MM_EMULATOR_PLAYING == mmEmulatorMachine_GetState(pEmulatorMachine) &&
            MM_SUCCESS == mmEmulatorMachine_GetRomLoadCode(pEmulatorMachine))
        {
            this->hLayerEmulatorWindow.SetFingerIsVisible(false);

            mmEmulatorMachine_Pause(pEmulatorMachine);
        }

        this->WidgetImageButton21->setVisible(true);
        this->WidgetImageButton22->setVisible(false);
    }
    void mmLayerExplorerDetailsNes::EmulatorPreviewStop(void)
    {
        this->EmulatorPreviewPause();

        this->WidgetImageButton23->setVisible(false);

        this->StaticImagePreview->setVisible(false);
        this->StaticImageEmbellish->setVisible(true);
    }
    bool mmLayerExplorerDetailsNes::OnWidgetImageButton00EventMouseClick(const CEGUI::EventArgs& args)
    {
        struct mmIconvContext* pIconvContext = this->pIconvContext;
        mmLayerUtilityIconv_HostName(pIconvContext, this->EditboxBasename, &this->hTextBaseNameHost);

        mmExplorerCommand_RenameItem(this->pExplorerCommand, this->pItem, mmString_CStr(&this->hTextBaseNameHost));
        mmExplorerCommand_Refresh(this->pExplorerCommand, mmString_CStr(&this->hDirectory));

        static const char* message = "Rename File";
        this->LoggerView(MM_LOG_INFO, 0, message, message);
        return true;
    }
    bool mmLayerExplorerDetailsNes::OnWidgetImageButton10EventMouseClick(const CEGUI::EventArgs& args)
    {
        mmExplorerCommand_Copy(this->pExplorerCommand, this->pItemSelect);

        static const char* message = "Copy File";
        this->LoggerView(MM_LOG_INFO, 0, message, message);
        return true;
    }
    bool mmLayerExplorerDetailsNes::OnWidgetImageButton11EventMouseClick(const CEGUI::EventArgs& args)
    {
        mmExplorerCommand_Cut(this->pExplorerCommand, this->pItemSelect);

        static const char* message = "Cut File";
        this->LoggerView(MM_LOG_INFO, 0, message, message);
        return true;
    }
    bool mmLayerExplorerDetailsNes::OnWidgetImageButton12EventMouseClick(const CEGUI::EventArgs& args)
    {
        mmLayerEmulatorMachine* pLayerEmulatorMachine = NULL;
        struct mmEmulatorMachine* pEmulatorMachine = NULL;

        pLayerEmulatorMachine = (mmLayerEmulatorMachine*)mmLayerDirector_SceneExclusiveForeground(this->pDirector, "mm/mmLayerEmulatorMachine", "mmLayerEmulatorMachine");

        pEmulatorMachine = &pLayerEmulatorMachine->hEmulatorMachine;

        if (pLayerEmulatorMachine->hCurrentEmulatorAssets != mmString_CStr(&this->pItem->fullname) ||
            MM_SUCCESS != mmEmulatorMachine_GetRomLoadCode(pEmulatorMachine))
        {
            mmEmulatorMachine_SetAssets(pEmulatorMachine, &this->hEmulatorAssets);

            mmEmulatorMachine_LoadRom(pEmulatorMachine);
            mmEmulatorMachine_Play(pEmulatorMachine);

            pLayerEmulatorMachine->hCurrentEmulatorAssets = mmString_CStr(&this->pItem->fullname);

            this->WidgetImageButton21->setVisible(false);
            this->WidgetImageButton22->setVisible(true);
            this->WidgetImageButton23->setVisible(true);

            this->StaticImagePreview->setVisible(true);
            this->StaticImageEmbellish->setVisible(false);

            this->hLayerEmulatorWindow.SetFingerIsVisible(true);
        }
        // notice: 
        // when we open a emulator, we can only play or Resume at the rom not load.
        // because if the rom already load, the behaver is only switch the layer,
        // and can not play or Resume at all.

        pLayerEmulatorMachine->UpdateLayerValue();

        mmExplorerCommand_OpenReg(this->pExplorerCommand, this->pItem);

        return true;
    }
    bool mmLayerExplorerDetailsNes::OnWidgetImageButton20EventMouseClick(const CEGUI::EventArgs& args)
    {
        this->RemoveItemLayerEnsure(&mmLayerExplorerDetailsBase::OnLayerEnsureEventChoiceRemove);
        return true;
    }
    bool mmLayerExplorerDetailsNes::OnWidgetImageButton21EventMouseClick(const CEGUI::EventArgs& args)
    {
        // play
        this->EmulatorPreviewPlay();

        mmExplorerCommand_Preview(this->pExplorerCommand, this->pItem);

        return true;
    }
    bool mmLayerExplorerDetailsNes::OnWidgetImageButton22EventMouseClick(const CEGUI::EventArgs& args)
    {
        // pause
        this->EmulatorPreviewPause();
        return true;
    }
    bool mmLayerExplorerDetailsNes::OnWidgetImageButton23EventMouseClick(const CEGUI::EventArgs& args)
    {
        // stop
        this->EmulatorPreviewStop();
        return true;
    }
    bool mmLayerExplorerDetailsNes::OnEditboxBasenameEventTextAccepted(const CEGUI::EventArgs& args)
    {
        this->OnWidgetImageButton00EventMouseClick(args);
        return true;
    }
    bool mmLayerExplorerDetailsNes::OnLayerEmulatorMachineEventEmulatorPlay(const CEGUI::EventArgs& args)
    {
        this->WidgetImageButton21->setVisible(false);
        this->WidgetImageButton22->setVisible(true);

        this->StaticImagePreview->setVisible(true);
        this->StaticImageEmbellish->setVisible(false);
        return true;
    }
    bool mmLayerExplorerDetailsNes::OnLayerEmulatorMachineEventEmulatorStop(const CEGUI::EventArgs& args)
    {
        this->WidgetImageButton21->setVisible(true);
        this->WidgetImageButton22->setVisible(false);

        this->StaticImagePreview->setVisible(false);
        this->StaticImageEmbellish->setVisible(true);
        return true;
    }
}
