/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLayerExplorerDetails.h"

#include "CEGUI/WindowManager.h"

#include "explorer/mmExplorerItem.h"

#include "mmLayerExplorerItem.h"
#include "mmLayerExplorerDetailsBase.h"

namespace mm
{
    const CEGUI::String mmLayerExplorerDetails::EventNamespace = "mm";
    const CEGUI::String mmLayerExplorerDetails::WidgetTypeName = "mm/mmLayerExplorerDetails";

    mmLayerExplorerDetails::mmLayerExplorerDetails(const CEGUI::String& type, const CEGUI::String& name)
        : mmLayerContext(type, name)

        , pExplorerCommand(NULL)
        , pExplorerFinder(NULL)
        , pExplorerImage(NULL)

        , pIconvContext(NULL)
        , pItemSelect(NULL)

        , pCurrentDetails(NULL)
    {
        mmRbtreeStringString_Init(&this->hItemLayerRbtree);
        mmRbtreeStringVpt_Init(&this->hDetailsRbtree);

        mmString_Init(&this->hDirectory);
        mmXoshiro256starstar_Init(&this->hRandmon);

        mmUInt64_t seed = (mmUInt64_t)time(NULL);
        mmXoshiro256starstar_Srand(&this->hRandmon, seed);
    }
    mmLayerExplorerDetails::~mmLayerExplorerDetails(void)
    {
        assert(0 == this->hItemLayerRbtree.size && "0 == this->hItemLayerRbtree.size is invalid.");
        assert(0 == this->hDetailsRbtree.size && "0 == this->hDetailsRbtree.size is invalid.");

        mmRbtreeStringString_Destroy(&this->hItemLayerRbtree);
        mmRbtreeStringVpt_Destroy(&this->hDetailsRbtree);

        mmString_Destroy(&this->hDirectory);
        mmXoshiro256starstar_Destroy(&this->hRandmon);
    }
    void mmLayerExplorerDetails::OnFinishLaunching(void)
    {
        this->RegisterDefaultItemLayer();
    }

    void mmLayerExplorerDetails::OnBeforeTerminate(void)
    {
        this->ClearItemDetails();
        this->ClearItemLayer();
    }
    void mmLayerExplorerDetails::SetDirectory(const char* pDirectory)
    {
        mmString_Assigns(&this->hDirectory, pDirectory);
    }
    void mmLayerExplorerDetails::SetItemSelect(struct mmRbtsetVpt* pItemSelect)
    {
        this->pItemSelect = pItemSelect;
    }
    void mmLayerExplorerDetails::SetIconvContext(struct mmIconvContext* pIconvContext)
    {
        this->pIconvContext = pIconvContext;
    }
    void mmLayerExplorerDetails::SetExplorerCommand(struct mmExplorerCommand* pExplorerCommand)
    {
        this->pExplorerCommand = pExplorerCommand;
    }
    void mmLayerExplorerDetails::SetExplorerFinder(struct mmExplorerFinder* pExplorerFinder)
    {
        this->pExplorerFinder = pExplorerFinder;
    }
    void mmLayerExplorerDetails::SetExplorerImage(struct mmExplorerImage* pExplorerImage)
    {
        this->pExplorerImage = pExplorerImage;
    }
    void mmLayerExplorerDetails::UpdateLayerValue(void)
    {
        if (NULL != this->pItemSelect)
        {
            const char* pLayer = "";

            if (NULL != this->pCurrentDetails)
            {
                this->pCurrentDetails->OnEnterBackground();
                this->pCurrentDetails->setVisible(false);
                this->pCurrentDetails = NULL;
            }

            if (0 == this->pItemSelect->size)
            {
                // empty
                pLayer = "mm/mmLayerExplorerDetailsEmpty";

                this->pCurrentDetails = this->GetItemDetailsInstance(pLayer);

                if (NULL != this->pCurrentDetails)
                {
                    this->pCurrentDetails->SetDirectory(mmString_CStr(&this->hDirectory));
                    this->pCurrentDetails->SetItemSelect(this->pItemSelect);
                    this->pCurrentDetails->UpdateLayerValue();
                    this->pCurrentDetails->setVisible(true);
                    this->pCurrentDetails->OnEnterForeground();
                }
            }
            else if (1 < this->pItemSelect->size)
            {
                // multi
                pLayer = "mm/mmLayerExplorerDetailsMulti";

                this->pCurrentDetails = this->GetItemDetailsInstance(pLayer);

                if (NULL != this->pCurrentDetails)
                {
                    this->pCurrentDetails->SetDirectory(mmString_CStr(&this->hDirectory));
                    this->pCurrentDetails->SetItemSelect(this->pItemSelect);
                    this->pCurrentDetails->UpdateLayerValue();
                    this->pCurrentDetails->setVisible(true);
                    this->pCurrentDetails->OnEnterForeground();
                }
            }
            else
            {
                struct mmString hLayer;
                // single
                struct mmExplorerItem* _item = NULL;
                struct mmRbNode* n = NULL;
                struct mmRbtsetVptIterator* it = NULL;
                // max
                n = mmRb_First(&this->pItemSelect->rbt);
                it = (struct mmRbtsetVptIterator*)mmRb_Entry(n, struct mmRbtsetVptIterator, n);
                _item = (struct mmExplorerItem*)it->k;

                mmString_Init(&hLayer);

                this->GetItemLayer(mmString_CStr(&_item->type_real), &hLayer);

                this->pCurrentDetails = this->GetItemDetailsInstance(mmString_CStr(&hLayer));

                if (NULL != this->pCurrentDetails)
                {
                    this->pCurrentDetails->SetDirectory(mmString_CStr(&this->hDirectory));
                    this->pCurrentDetails->SetItem(_item);
                    this->pCurrentDetails->SetItemSelect(this->pItemSelect);
                    this->pCurrentDetails->UpdateLayerValue();
                    this->pCurrentDetails->setVisible(true);
                    this->pCurrentDetails->OnEnterForeground();
                }

                mmString_Destroy(&hLayer);
            }
        }
    }
    void mmLayerExplorerDetails::UpdateUtf8View(void)
    {
        mmLayerExplorerDetailsBase* e = NULL;

        struct mmRbNode* n = NULL;
        struct mmRbtreeStringVptIterator* it = NULL;
        //
        n = mmRb_First(&this->hDetailsRbtree.rbt);
        while (NULL != n)
        {
            it = (struct mmRbtreeStringVptIterator*)mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
            n = mmRb_Next(n);
            // 
            e = (mmLayerExplorerDetailsBase*)it->v;

            e->UpdateUtf8View();
        }
    }
    void mmLayerExplorerDetails::GetItemLayer(const char* pType, struct mmString* pLayer)
    {
        struct mmString hWeakType;
        mmString_MakeWeak(&hWeakType, pType);

        struct mmRbtreeStringStringIterator* it = NULL;
        it = mmRbtreeStringString_GetIterator(&this->hItemLayerRbtree, &hWeakType);
        if (NULL != it)
        {
            mmString_Assign(pLayer, &it->v);
        }
        else
        {
            mmString_Assigns(pLayer, "mm/mmLayerExplorerDetailsUnknown");
        }
    }
    void mmLayerExplorerDetails::AddItemLayer(const char* pType, const char* pLayer)
    {
        struct mmString hWeakType;
        mmString_MakeWeak(&hWeakType, pType);

        struct mmString hWeakLayer;
        mmString_MakeWeak(&hWeakLayer, pLayer);

        mmRbtreeStringString_Set(&this->hItemLayerRbtree, &hWeakType, &hWeakLayer);
    }
    void mmLayerExplorerDetails::RmvItemLayer(const char* pType)
    {
        struct mmString hWeakType;
        mmString_MakeWeak(&hWeakType, pType);

        mmRbtreeStringString_Rmv(&this->hItemLayerRbtree, &hWeakType);
    }
    void mmLayerExplorerDetails::ClearItemLayer(void)
    {
        mmRbtreeStringString_Clear(&this->hItemLayerRbtree);
    }
    void mmLayerExplorerDetails::RegisterDefaultItemLayer(void)
    {
        this->AddItemLayer("d.---", "mm/mmLayerExplorerDetailsDirectory");
        this->AddItemLayer("u.---", "mm/mmLayerExplorerDetailsUnknown");
        this->AddItemLayer("-.emu", "mm/mmLayerExplorerDetailsEmu");

        this->AddItemLayer("-.nes", "mm/mmLayerExplorerDetailsNes");
        this->AddItemLayer("-.NES", "mm/mmLayerExplorerDetailsNes");
        this->AddItemLayer("-.Nes", "mm/mmLayerExplorerDetailsNes");

        this->AddItemLayer("-.nsf", "mm/mmLayerExplorerDetailsNes");
        this->AddItemLayer("-.NSF", "mm/mmLayerExplorerDetailsNes");
        this->AddItemLayer("-.Nsf", "mm/mmLayerExplorerDetailsNes");

        this->AddItemLayer("-.sdf", "mm/mmLayerExplorerDetailsNes");
        this->AddItemLayer("-.SDF", "mm/mmLayerExplorerDetailsNes");
        this->AddItemLayer("-.Sdf", "mm/mmLayerExplorerDetailsNes");

        this->AddItemLayer("-.png", "mm/mmLayerExplorerDetailsPng");
        this->AddItemLayer("-.txt", "mm/mmLayerExplorerDetailsTxt");
        this->AddItemLayer("-.zip", "mm/mmLayerExplorerDetailsZip");
    }
    mmLayerExplorerDetailsBase* mmLayerExplorerDetails::AddItemDetails(const char* pLayer)
    {
        struct mmString hWeakLayer;
        mmString_MakeWeak(&hWeakLayer, pLayer);

        mmLayerExplorerDetailsBase* e = NULL;

        struct mmRbtreeStringVptIterator* it = NULL;
        it = mmRbtreeStringVpt_GetIterator(&this->hDetailsRbtree, &hWeakLayer);
        if (NULL == it)
        {
            CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
            e = (mmLayerExplorerDetailsBase*)pWindowManager->createWindow(pLayer);
            this->addChild(e);

            e->SetDirector(this->pDirector);
            e->SetContextMaster(this->pContextMaster);
            e->SetSurfaceMaster(this->pSurfaceMaster);
            e->SetIconvContext(this->pIconvContext);
            e->SetExplorerCommand(this->pExplorerCommand);
            e->SetExplorerFinder(this->pExplorerFinder);
            e->SetExplorerImage(this->pExplorerImage);
            e->SetXoshiroRandmon(&this->hRandmon);
            e->OnFinishLaunching();

            mmRbtreeStringVpt_Set(&this->hDetailsRbtree, &hWeakLayer, e);
        }
        else
        {
            e = (mmLayerExplorerDetailsBase*)it->v;
        }
        return e;
    }
    mmLayerExplorerDetailsBase* mmLayerExplorerDetails::GetItemDetails(const char* pLayer)
    {
        struct mmString hWeakLayer;
        mmString_MakeWeak(&hWeakLayer, pLayer);

        mmLayerExplorerDetailsBase* e = NULL;

        struct mmRbtreeStringVptIterator* it = NULL;
        it = mmRbtreeStringVpt_GetIterator(&this->hDetailsRbtree, &hWeakLayer);
        if (NULL != it)
        {
            e = (mmLayerExplorerDetailsBase*)it->v;
        }
        return e;
    }
    mmLayerExplorerDetailsBase* mmLayerExplorerDetails::GetItemDetailsInstance(const char* pLayer)
    {
        mmLayerExplorerDetailsBase* e = NULL;
        e = this->GetItemDetails(pLayer);
        if (NULL == e)
        {
            e = this->AddItemDetails(pLayer);
        }
        return e;
    }
    void mmLayerExplorerDetails::RmvItemDetails(const char* pLayer)
    {
        struct mmString hWeakLayer;
        mmString_MakeWeak(&hWeakLayer, pLayer);

        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();

        mmLayerExplorerDetailsBase* e = NULL;

        struct mmRbtreeStringVptIterator* it = NULL;
        it = mmRbtreeStringVpt_GetIterator(&this->hDetailsRbtree, &hWeakLayer);
        if (NULL != it)
        {
            e = (mmLayerExplorerDetailsBase*)it->v;

            mmRbtreeStringVpt_Erase(&this->hDetailsRbtree, it);
            e->OnBeforeTerminate();
            this->removeChild(e);
            pWindowManager->destroyWindow(e);
        }
    }
    void mmLayerExplorerDetails::ClearItemDetails(void)
    {
        CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();

        mmLayerExplorerDetailsBase* e = NULL;

        struct mmRbNode* n = NULL;
        struct mmRbtreeStringVptIterator* it = NULL;
        //
        n = mmRb_First(&this->hDetailsRbtree.rbt);
        while (NULL != n)
        {
            it = (struct mmRbtreeStringVptIterator*)mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
            n = mmRb_Next(n);
            // 
            e = (mmLayerExplorerDetailsBase*)it->v;

            mmRbtreeStringVpt_Erase(&this->hDetailsRbtree, it);
            this->removeChild(e);
            e->OnBeforeTerminate();
            pWindowManager->destroyWindow(e);
        }
    }
}
