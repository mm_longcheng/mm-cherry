/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmActivityMasterAndroid.h"

#include "core/mmAlloc.h"

#include "mmActivityMaster.h"

MM_EXPORT_NWSI jclass mmActivityMaster_ObjectClass = NULL;
MM_EXPORT_NWSI jfieldID mmActivityMaster_Field_hNativePtr = NULL;

MM_EXPORT_NWSI void mmActivityMaster_AttachVirtualMachine(JNIEnv* env)
{
    jclass object_class = env->FindClass("org/mm/nwsi/mmActivityMaster");
    mmActivityMaster_ObjectClass = (jclass)env->NewGlobalRef(object_class);

    mmActivityMaster_Field_hNativePtr = env->GetFieldID(object_class, "hNativePtr", "J");

    env->DeleteLocalRef(object_class);
}
MM_EXPORT_NWSI void mmActivityMaster_DetachVirtualMachine(JNIEnv* env)
{
    env->DeleteGlobalRef(mmActivityMaster_ObjectClass);
    mmActivityMaster_ObjectClass = NULL;

    mmActivityMaster_Field_hNativePtr = NULL;
}

// java -> c
#include "core/mmPrefix.h"

MM_EXPORT_NWSI struct mmActivityMaster* mmActivityMaster_GetNativePtr(JNIEnv* env, jobject obj)
{
    jlong j_hNativePtr = env->GetLongField(obj, mmActivityMaster_Field_hNativePtr);
    return (struct mmActivityMaster*)j_hNativePtr;
}

MM_EXPORT_NWSI void mmActivityMaster_SetNativePtr(JNIEnv* env, jobject obj, struct mmActivityMaster* p)
{
    jlong j_hNativePtr = (jlong)p;
    env->SetLongField(obj, mmActivityMaster_Field_hNativePtr, j_hNativePtr);
}

JNIEXPORT void JNICALL Java_org_mm_nwsi_mmActivityMaster_NativeAlloc(JNIEnv* env, jobject obj)
{
    struct mmActivityMaster* p = (struct mmActivityMaster*)mmMalloc(sizeof(struct mmActivityMaster));
    // c++ struct can not internal structure life cycle manual, do it external.
    new (p) struct mmActivityMaster();
    mmActivityMaster_SetNativePtr(env, obj, p);
}
JNIEXPORT void JNICALL Java_org_mm_nwsi_mmActivityMaster_NativeDealloc(JNIEnv* env, jobject obj)
{
    struct mmActivityMaster* p = mmActivityMaster_GetNativePtr(env, obj);
    mmActivityMaster_SetNativePtr(env, obj, NULL);
    // c++ struct can not internal structure life cycle manual, do it external.
    p->~mmActivityMaster();
    mmFree(p);
}

JNIEXPORT void JNICALL Java_org_mm_nwsi_mmActivityMaster_NativeInit(JNIEnv* env, jobject obj)
{
    struct mmActivityMaster* p = mmActivityMaster_GetNativePtr(env, obj);
    mmActivityMaster_Init(p);
}

JNIEXPORT void JNICALL Java_org_mm_nwsi_mmActivityMaster_NativeDestroy(JNIEnv* env, jobject obj)
{
    struct mmActivityMaster* p = mmActivityMaster_GetNativePtr(env, obj);
    mmActivityMaster_Destroy(p);
}

#include "core/mmSuffix.h"

