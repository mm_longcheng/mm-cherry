#include <jni.h>

#include "dish/mmJavaVm.h"

#include "dish/mmDishNative.h"
#include "nwsi/mmNwsiNative.h"

#include "mmActivityMasterAndroid.h"

JNIEXPORT jint JNI_OnLoad(JavaVM* vm, void* reserved)
{
    struct mmJavaVm* pJavaVm = mmJavaVm_Instance();
    JNIEnv* env = mmJavaVm_CurrentThreadJniEnv(pJavaVm);

    mmDishNative_AttachVirtualMachine(vm);
    mmNwsiNative_AttachVirtualMachine(vm);

    mmActivityMaster_AttachVirtualMachine(env);
    return JNI_VERSION_1_4;
}
JNIEXPORT void JNICALL JNI_OnUnload(JavaVM* vm, void* reserved)
{
    struct mmJavaVm* pJavaVm = mmJavaVm_Instance();
    JNIEnv* env = mmJavaVm_CurrentThreadJniEnv(pJavaVm);

    mmActivityMaster_DetachVirtualMachine(env);

    mmNwsiNative_DetachVirtualMachine(vm);
    mmDishNative_DetachVirtualMachine(vm);
}
