package org.mm.eggs;

import org.mm.core.mmLogger;
import org.mm.nwsi.mmActivityMaster;
import org.mm.nwsi.mmUIApplication;

import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

public class mmUIActivityMaster extends AppCompatActivity
{
    private static final String TAG = mmUIActivityMaster.class.getSimpleName();
    
    private mmUIApplication pUIApplication = null;
    private mmActivityMaster pActivityMaster = null;
    
    @Override
    public void onCreate(final Bundle savedInstanceState)
    {    	
        super.onCreate(savedInstanceState);
        
        Log.i(TAG, TAG + " onCreate event begin.");

        LoadNativeLibrary();

        this.pActivityMaster = new mmActivityMaster();
        this.pUIApplication = new mmUIApplication(this);
        
        this.pActivityMaster.Init();
        this.pUIApplication.Init();
        
        this.pUIApplication.SetActivity(this);
        this.pUIApplication.SetActivityMaster(this.pActivityMaster);
        this.pUIApplication.SetModuleName("mm.nwsi");
        this.pUIApplication.SetRenderSystemName("OpenGL ES 2.x Rendering Subsystem");
        this.pUIApplication.SetLoggerFileName("nwsi");
        this.pUIApplication.SetLoggerLevel(mmLogger.MM_LOG_INFO);
        this.pUIApplication.SetUIFullScreenLayout(true);
        this.pUIApplication.OnFinishLaunching();
        this.pUIApplication.OnStart();

        this.setContentView(this.pUIApplication);
        
        Log.i(TAG, TAG + " onCreate event end.");
        Log.i(TAG, TAG + " init succeed.");
    }
    
    @Override
    protected void onStart() 
    {
        super.onStart();
        Log.i(TAG, TAG + " onStart event.");
    }
    
    @Override
    protected void onResume() 
    {
        super.onResume();
        Log.i(TAG, TAG + " onResume event.");
        this.pUIApplication.OnEnterForeground();
    }

    @Override
    protected void onPause() 
    {
        this.pUIApplication.OnEnterBackground();
        Log.i(TAG, TAG + " onPause event.");
        super.onPause();
    }
    
    @Override
    protected void onStop() 
    {
        super.onStop();
        Log.i(TAG, TAG + " onStop event.");
    }
    
    @Override
    protected void onDestroy() 
    {
        Log.i(TAG, TAG + " onDestroy event.");
        
        this.pUIApplication.OnShutdown();
        this.pUIApplication.OnJoin();
        this.pUIApplication.OnBeforeTerminate();
        
        this.pUIApplication.Destroy();
        this.pActivityMaster.Destroy();
        
        this.pUIApplication = null;
        this.pActivityMaster = null;
        
        super.onDestroy();
        Log.i(TAG, TAG + " destroy succeed.");
    }

    static public void LoadNativeLibrary()
    {
        // stl library
        // ----------------------------------------------------------
        mmUIApplication.LoadLibrary("gnustl_shared");
        // ----------------------------------------------------------

        // Tools library
        // ----------------------------------------------------------
        mmUIApplication.LoadLibrary("z_shared");
        mmUIApplication.LoadLibrary("minizip_shared");
        mmUIApplication.LoadLibrary("zzip_shared");
        mmUIApplication.LoadLibrary("tinyxml_shared");

        mmUIApplication.LoadLibrary("mm_core_shared");
        mmUIApplication.LoadLibrary("mm_dish_shared");

        mmUIApplication.LoadLibrary("freetype_shared");
        mmUIApplication.LoadLibrary("FreeImage_shared");

        mmUIApplication.LoadLibrary("pcre_shared");
        mmUIApplication.LoadLibrary("iconv_shared");

        mmUIApplication.LoadLibrary("OpenAL_shared");

        mmUIApplication.LoadLibrary("OgreMain_shared");
        mmUIApplication.LoadLibrary("RenderSystem_GLES2_shared");
        mmUIApplication.LoadLibrary("Components_RTShaderSystem_shared");

        mmUIApplication.LoadLibrary("CEGUIMain_shared");
        mmUIApplication.LoadLibrary("CEGUIOgreRenderer_shared");

        mmUIApplication.LoadLibrary("CEGUICoreWindowRendererSet_shared");

        mmUIApplication.LoadLibrary("mm_nwsi_shared");
        // ----------------------------------------------------------

        // Expansion library
        // ----------------------------------------------------------
        mmUIApplication.LoadLibrary("sqlite_shared");
        mmUIApplication.LoadLibrary("mm_net_shared");
        // ----------------------------------------------------------
        // Main library.
        // ----------------------------------------------------------
        mmUIApplication.LoadLibrary("mm_eggs_shared");
        // ----------------------------------------------------------
    }
}
