// stdafx.h : Include files for standard system include files,
// or frequently used but rarely changed
// Project-specific include files
//

#ifndef __stdafx_h__
#define __stdafx_h__

#include "../resource/targetver.h"

// Exclude rarely used data from Windows headers
#define WIN32_LEAN_AND_MEAN             
// Windows head file: 
#include <windows.h>

// C runtime head files
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>


// TODO: Reference other header files required by the program here

#endif//__stdafx_h__
