/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmUIWindowApplication.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"

#include "nwsi/mmUIDisplayMetrics.h"

#include "resource.h"

static CHAR __mmUIWindowApplication_szWindowClass[MM_MAX_LOADSTRING] = { 0 };

static LRESULT CALLBACK __static_mmUIWindowApplication_WndProc(HWND, UINT, WPARAM, LPARAM);
static INT_PTR CALLBACK __static_mmUIWindowApplication_About(HWND, UINT, WPARAM, LPARAM);

static void __static_mmUIWindowApplication_UpdateDisplayDensity(struct mmUIWindowApplication* p);

static void __static_mmUIWindowApplication_CreateAcceleratorTable(struct mmUIWindowApplication* p);
static void __static_mmUIWindowApplication_DestroyAcceleratorTable(struct mmUIWindowApplication* p);

static void __static_mmUIWindowApplication_CreateSurface(struct mmUIWindowApplication* p);
static void __static_mmUIWindowApplication_DestroySurface(struct mmUIWindowApplication* p);

static void __static_mmUIWindowApplication_ShowWindow(struct mmUIWindowApplication* p);

static void __static_mmUIWindowApplication_ThreadGetMessage(struct mmUIWindowApplication* p);

static void __static_mmUIWindowApplication_ThreadPeekMessage(struct mmUIWindowApplication* p);
static void __static_mmUIWindowApplication_ThreadEnter(struct mmUIWindowApplication* p);
static void __static_mmUIWindowApplication_ThreadLeave(struct mmUIWindowApplication* p);

static void __static_mmUIWindowApplication_OnDisplayLinkUpdate(struct mmUIWindowApplication* p);

static void __static_mmUIWindowApplication_OnPollWait(struct mmUIWindowApplication* p);
static void __static_mmUIWindowApplication_OnTerminal(struct mmUIWindowApplication* p);

static void __static_mmUIWindowApplication_OnUpdateSizeCache(struct mmUIWindowApplication* p, int x, int y, int w, int h);
static void __static_mmUIWindowApplication_OnSizeChange(struct mmUIWindowApplication* p, int w, int h);

static void __static_mmUIWindowApplication_AllocConsole(struct mmUIWindowApplication* p);
static void __static_mmUIWindowApplication_FreeConsole(struct mmUIWindowApplication* p);
static void __static_mmUIWindowApplication_SwitchConsoleStatus(struct mmUIWindowApplication* p, BOOL status);

static void __static_mmUIWindowApplication_LaunchOptions(struct mmUIWindowApplication* p);

void mmUIWindowApplication_Init(struct mmUIWindowApplication* p)
{
    mmActivityMaster_Init(&p->pActivityMaster);
    mmNativeApplication_Init(&p->hApplication);
    mmUIViewSurfaceMaster_Init(&p->hUIViewSurfaceMaster);

    mmApplicationOptions_Init(&p->hApplicationOptions);
    mmString_Init(&p->hLaunchFile);

    p->hDisplayDensity = 1.0;
    p->hFrameScale = 1.0;

    mmMemset(p->hCanvasRect, 0, sizeof(double) * 4);

    p->hWndConsole = NULL;
    p->bConsoleStatus = FALSE;

    p->state = MM_TS_CLOSED;

    mmMemset(p->szTitle, 0, sizeof(CHAR) * MM_MAX_LOADSTRING);
    p->hWnd = NULL;
    p->hInstance = NULL;
    p->hAcceleratorTable = NULL;
    p->nCmdShow = 0;

    p->bMenu = TRUE;
    p->dwExStyle = 0;
    p->dwStyle = WS_VISIBLE | WS_CLIPCHILDREN | WS_OVERLAPPEDWINDOW;

    p->hWindowX = 0;
    p->hWindowY = 0;
    p->hWindowW = 0;
    p->hWindowH = 0;

    p->hClientX = mmUIWindowApplication_X_DEFAULT;
    p->hClientY = mmUIWindowApplication_Y_DEFAULT;
    p->hClientW = mmUIWindowApplication_W_DEFAULT;
    p->hClientH = mmUIWindowApplication_H_DEFAULT;

    p->hTerminateCode = -1;

#ifdef _DEBUG
    p->bConsoleStatus = TRUE;
#else
    p->bConsoleStatus = FALSE;
#endif//_DEBUG

    mmString_Assigns(&p->hLaunchFile, "launch.config");
}
void mmUIWindowApplication_Destroy(struct mmUIWindowApplication* p)
{
    // Terminate code value will maintain. 
    // p->hTerminateCode = 0;

    p->hClientX = mmUIWindowApplication_X_DEFAULT;
    p->hClientY = mmUIWindowApplication_Y_DEFAULT;
    p->hClientW = mmUIWindowApplication_W_DEFAULT;
    p->hClientH = mmUIWindowApplication_H_DEFAULT;

    p->hWindowX = 0;
    p->hWindowY = 0;
    p->hWindowW = 0;
    p->hWindowH = 0;

    p->dwExStyle = 0;
    p->dwStyle = WS_VISIBLE | WS_CLIPCHILDREN | WS_OVERLAPPEDWINDOW;
    p->bMenu = TRUE;

    p->nCmdShow = 0;
    p->hAcceleratorTable = NULL;
    p->hInstance = NULL;
    p->hWnd = NULL;
    mmMemset(p->szTitle, 0, sizeof(CHAR) * MM_MAX_LOADSTRING);

    p->state = MM_TS_CLOSED;

    p->bConsoleStatus = FALSE;
    p->hWndConsole = NULL;

    mmMemset(p->hCanvasRect, 0, sizeof(double) * 4);

    p->hFrameScale = 1.0;
    p->hDisplayDensity = 1.0;

    mmUIViewSurfaceMaster_Destroy(&p->hUIViewSurfaceMaster);
    mmNativeApplication_Destroy(&p->hApplication);
    mmActivityMaster_Destroy(&p->pActivityMaster);
}

void mmUIWindowApplication_SetLaunchFile(struct mmUIWindowApplication* p, const char* pLaunchFile)
{
    mmString_Assigns(&p->hLaunchFile, pLaunchFile);
}

void mmUIWindowApplication_SetInstance(struct mmUIWindowApplication* p, HINSTANCE hInstance)
{
    p->hInstance = hInstance;
}
void mmUIWindowApplication_SetCommandShow(struct mmUIWindowApplication* p, int nCmdShow)
{
    p->nCmdShow = nCmdShow;
}
void mmUIWindowApplication_SetClientRect(struct mmUIWindowApplication* p, int x, int y, int w, int h)
{
    p->hClientX = x;
    p->hClientY = y;
    p->hClientW = w;
    p->hClientH = h;
}
void mmUIWindowApplication_SetCanvasRect(struct mmUIWindowApplication* p, int x, int y, int w, int h)
{
    p->hCanvasRect[0] = (double)x;
    p->hCanvasRect[1] = (double)y;
    p->hCanvasRect[2] = (double)w;
    p->hCanvasRect[3] = (double)h;
}
void mmUIWindowApplication_SetFrameScale(struct mmUIWindowApplication* p, double hFrameScale)
{
    p->hFrameScale = hFrameScale;
}
void mmUIWindowApplication_SetDisplayFrequency(struct mmUIWindowApplication* p, double hDisplayFrequency)
{
    mmNativeApplication_SetDisplayFrequency(&p->hApplication, hDisplayFrequency);
}

void mmUIWindowApplication_OnFinishLaunching(struct mmUIWindowApplication* p)
{
    __static_mmUIWindowApplication_LaunchOptions(p);

    __static_mmUIWindowApplication_UpdateDisplayDensity(p);

    mmNativeApplication_SetNativeApplicationHandler(&p->hApplication, p->hInstance);
    mmNativeApplication_SetActivityMaster(&p->hApplication, &p->pActivityMaster.hSuper);
    mmNativeApplication_OnFinishLaunching(&p->hApplication);

    LoadStringA(p->hInstance, IDS_APP_TITLE, p->szTitle, MM_MAX_LOADSTRING);

    __static_mmUIWindowApplication_AllocConsole(p);

    __static_mmUIWindowApplication_CreateAcceleratorTable(p);
    __static_mmUIWindowApplication_CreateSurface(p);
    __static_mmUIWindowApplication_ShowWindow(p);

    mmUIViewSurfaceMaster_SetSurfaceMaster(&p->hUIViewSurfaceMaster, &p->hApplication.hSurfaceMaster);
    mmUIViewSurfaceMaster_SetInstance(&p->hUIViewSurfaceMaster, p->hInstance);
    mmUIViewSurfaceMaster_SetCommandShow(&p->hUIViewSurfaceMaster, p->nCmdShow);
    mmUIViewSurfaceMaster_SetClientRect(&p->hUIViewSurfaceMaster, p->hClientX, p->hClientY, p->hClientW, p->hClientH);
    mmUIViewSurfaceMaster_SetWndParent(&p->hUIViewSurfaceMaster, p->hWnd);
    mmUIViewSurfaceMaster_SetAcceleratorTable(&p->hUIViewSurfaceMaster, p->hAcceleratorTable);
    mmUIViewSurfaceMaster_SetDisplayDensity(&p->hUIViewSurfaceMaster, p->hDisplayDensity * p->hFrameScale);
    mmUIViewSurfaceMaster_OnFinishLaunching(&p->hUIViewSurfaceMaster);
    mmUIViewSurfaceMaster_OnUpdateSizeChange(&p->hUIViewSurfaceMaster, p->hClientW, p->hClientH);

    __static_mmUIWindowApplication_ThreadEnter(p);
}
void mmUIWindowApplication_OnBeforeTerminate(struct mmUIWindowApplication* p)
{
    __static_mmUIWindowApplication_ThreadLeave(p);

    mmUIViewSurfaceMaster_OnBeforeTerminate(&p->hUIViewSurfaceMaster);

    __static_mmUIWindowApplication_DestroySurface(p);
    __static_mmUIWindowApplication_DestroyAcceleratorTable(p);

    __static_mmUIWindowApplication_AllocConsole(p);

    mmNativeApplication_OnBeforeTerminate(&p->hApplication);
}
void mmUIWindowApplication_OnEnterForeground(struct mmUIWindowApplication* p)
{
    mmNativeApplication_OnEnterForeground(&p->hApplication);
}
void mmUIWindowApplication_OnEnterBackground(struct mmUIWindowApplication* p)
{
    mmNativeApplication_OnEnterBackground(&p->hApplication);
}
void mmUIWindowApplication_OnStart(struct mmUIWindowApplication* p)
{
    p->state = MM_TS_FINISH == p->state ? MM_TS_CLOSED : MM_TS_MOTION;
    mmNativeApplication_OnStart(&p->hApplication);
    mmUIViewSurfaceMaster_OnStart(&p->hUIViewSurfaceMaster);
}
void mmUIWindowApplication_OnInterrupt(struct mmUIWindowApplication* p)
{
    p->state = MM_TS_CLOSED;
    mmUIViewSurfaceMaster_OnInterrupt(&p->hUIViewSurfaceMaster);
    mmNativeApplication_OnInterrupt(&p->hApplication);
}
void mmUIWindowApplication_OnShutdown(struct mmUIWindowApplication* p)
{
    p->state = MM_TS_FINISH;
    mmUIViewSurfaceMaster_OnShutdown(&p->hUIViewSurfaceMaster);
    mmNativeApplication_OnShutdown(&p->hApplication);
}
void mmUIWindowApplication_OnJoin(struct mmUIWindowApplication* p)
{
    __static_mmUIWindowApplication_OnPollWait(p);
    //
    mmUIViewSurfaceMaster_OnJoin(&p->hUIViewSurfaceMaster);
    mmNativeApplication_OnJoin(&p->hApplication);
}

ATOM mmUIWindowApplication_RegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXA wcex;

    mmUIViewSurfaceMaster_RegisterClass(hInstance);

    LoadStringA(hInstance, IDC_MMUIWINDOWAPPLICATION, __mmUIWindowApplication_szWindowClass, MM_MAX_LOADSTRING);

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc = __static_mmUIWindowApplication_WndProc;
    wcex.cbClsExtra = 0;
    wcex.cbWndExtra = 0;
    wcex.hInstance = hInstance;
    wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON_48x48));
    wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
    wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
    wcex.lpszMenuName = MAKEINTRESOURCEA(IDC_MMUIWINDOWAPPLICATION);
    wcex.lpszClassName = __mmUIWindowApplication_szWindowClass;
    wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_ICON_32x32));

    return RegisterClassExA(&wcex);
}
BOOL mmUIWindowApplication_UnregisterClass(HINSTANCE hInstance)
{
    mmUIViewSurfaceMaster_UnregisterClass(hInstance);
    return UnregisterClassA(__mmUIWindowApplication_szWindowClass, hInstance);
}

static void __static_mmUIWindowApplication_UpdateDisplayDensity(struct mmUIWindowApplication* p)
{
    double hDisplayDensity = p->hDisplayDensity * p->hFrameScale;

    p->hDisplayDensity = mmUIDisplayMetrics_GetDisplayDensity(NULL);

    p->hClientX = (int)(p->hCanvasRect[0] * hDisplayDensity);
    p->hClientY = (int)(p->hCanvasRect[1] * hDisplayDensity);
    p->hClientW = (int)(p->hCanvasRect[2] * hDisplayDensity);
    p->hClientH = (int)(p->hCanvasRect[3] * hDisplayDensity);
}

static void __static_mmUIWindowApplication_CreateAcceleratorTable(struct mmUIWindowApplication* p)
{
    p->hAcceleratorTable = LoadAccelerators(p->hInstance, MAKEINTRESOURCE(IDC_MMUIWINDOWAPPLICATION));
}
static void __static_mmUIWindowApplication_DestroyAcceleratorTable(struct mmUIWindowApplication* p)
{
    DestroyAcceleratorTable(p->hAcceleratorTable);
    p->hAcceleratorTable = NULL;
}

static void __static_mmUIWindowApplication_CreateSurface(struct mmUIWindowApplication* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();

    RECT rc;

    SetRect(&rc, p->hClientX, p->hClientY, p->hClientX + p->hClientW, p->hClientY + p->hClientH);
    AdjustWindowRectEx(&rc, p->dwStyle, TRUE, p->dwExStyle);

    p->hWindowX = rc.left;
    p->hWindowY = rc.top;
    p->hWindowW = rc.right - rc.left;
    p->hWindowH = rc.bottom - rc.top;

    p->hWindowX = p->hWindowX < 0 ? 0 : p->hWindowX;
    p->hWindowY = p->hWindowY < 0 ? 0 : p->hWindowY;

    LoadMenuA(p->hInstance, "");

    p->hWnd = CreateWindowExA(
        p->dwExStyle,
        __mmUIWindowApplication_szWindowClass, p->szTitle,
        p->dwStyle,
        p->hWindowX, p->hWindowY,
        p->hWindowW, p->hWindowH,
        NULL,
        NULL,
        p->hInstance,
        NULL);

    if (p->hWnd)
    {
        mmLogger_LogI(gLogger, "%s %d CreateWindowEx Window((%d, %d), (%d, %d)) Client((%d, %d), (%d, %d)) success.",
            __FUNCTION__, __LINE__,
            p->hWindowX, p->hWindowY, p->hWindowW, p->hWindowH,
            p->hClientX, p->hClientY, p->hClientW, p->hClientH);

        SetWindowLongPtr(p->hWnd, GWLP_USERDATA, (LONG_PTR)p);
    }
    else
    {
        mmLogger_LogF(gLogger, "%s %d CreateWindowEx Window((%d, %d), (%d, %d)) Client((%d, %d), (%d, %d)) failure.",
            __FUNCTION__, __LINE__,
            p->hWindowX, p->hWindowY, p->hWindowW, p->hWindowH,
            p->hClientX, p->hClientY, p->hClientW, p->hClientH);
    }
}
static void __static_mmUIWindowApplication_DestroySurface(struct mmUIWindowApplication* p)
{
    if (NULL != p->hWnd)
    {
        DestroyWindow(p->hWnd);
        p->hWnd = NULL;
    }
}
static void __static_mmUIWindowApplication_ShowWindow(struct mmUIWindowApplication* p)
{
    ShowWindow(p->hWnd, p->nCmdShow);
    UpdateWindow(p->hWnd);
}
static void __static_mmUIWindowApplication_ThreadPeekMessage(struct mmUIWindowApplication* p)
{
    MSG msg;
    // Peek message use PM_REMOVE. 
    // Note: Main message loop HWND must NULL.
    while (PeekMessage(&msg, NULL, (UINT)0U, (UINT)0U, PM_REMOVE))
    {
        if (!TranslateAccelerator(msg.hwnd, p->hAcceleratorTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }
}
static void __static_mmUIWindowApplication_ThreadGetMessage(struct mmUIWindowApplication* p)
{
    MSG msg;
    // Main message loop. 
    // Note: Main message loop HWND must NULL.
    while (GetMessage(&msg, NULL, (UINT)0U, (UINT)0U))
    {
        if (!TranslateAccelerator(msg.hwnd, p->hAcceleratorTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }
}
static void __static_mmUIWindowApplication_ThreadEnter(struct mmUIWindowApplication* p)
{

}
static void __static_mmUIWindowApplication_ThreadLeave(struct mmUIWindowApplication* p)
{

}

static void __static_mmUIWindowApplication_OnDisplayLinkUpdate(struct mmUIWindowApplication* p) 
{
    // OnUpdate
    mmUIViewSurfaceMaster_OnUpdate(&p->hUIViewSurfaceMaster);

    // PeekMessage.
    __static_mmUIWindowApplication_ThreadPeekMessage(p);

    // OnTimewait
    mmUIViewSurfaceMaster_OnTimewait(&p->hUIViewSurfaceMaster);
}

static void __static_mmUIWindowApplication_OnPollWait(struct mmUIWindowApplication* p)
{
    // looper
    while (MM_TS_MOTION == p->state)
    {
        __static_mmUIWindowApplication_OnDisplayLinkUpdate(p);
    }
}

static void __static_mmUIWindowApplication_OnTerminal(struct mmUIWindowApplication* p)
{
    mmUIWindowApplication_OnShutdown(p);
    __static_mmUIWindowApplication_DestroySurface(p);
    p->hTerminateCode = 0;
}

static void __static_mmUIWindowApplication_OnUpdateSizeCache(struct mmUIWindowApplication* p, int x, int y, int w, int h)
{
    RECT rc;

    mmUIWindowApplication_SetClientRect(p, x, y, w, h);

    SetRect(&rc, p->hClientX, p->hClientY, p->hClientW, p->hClientH);

    AdjustWindowRectEx(&rc, p->dwStyle, p->bMenu, p->dwExStyle);

    p->hWindowX = rc.left;
    p->hWindowY = rc.top;
    p->hWindowW = rc.right - rc.left;
    p->hWindowH = rc.bottom - rc.top;

    p->hWindowX = p->hWindowX < 0 ? 0 : p->hWindowX;
    p->hWindowY = p->hWindowY < 0 ? 0 : p->hWindowY;
}

static void __static_mmUIWindowApplication_OnSizeChange(struct mmUIWindowApplication* p, int w, int h)
{
    __static_mmUIWindowApplication_OnUpdateSizeCache(p, p->hClientX, p->hClientY, w, h);
    mmUIViewSurfaceMaster_OnUpdateSize(&p->hUIViewSurfaceMaster, p->hClientW, p->hClientH);
}

static void __static_mmUIWindowApplication_AllocConsole(struct mmUIWindowApplication* p)
{
    AllocConsole();

    p->hWndConsole = GetConsoleWindow();
    freopen( "CONIN$", "r",  stdin);
    freopen("CONOUT$", "w", stdout);
    freopen("CONOUT$", "w", stderr);

    __static_mmUIWindowApplication_SwitchConsoleStatus(p, p->bConsoleStatus);
}
static void __static_mmUIWindowApplication_FreeConsole(struct mmUIWindowApplication* p)
{
    FreeConsole();
    p->hWndConsole = NULL;
}
static void __static_mmUIWindowApplication_SwitchConsoleStatus(struct mmUIWindowApplication* p, BOOL status)
{
    p->bConsoleStatus = status;
    if (FALSE == p->bConsoleStatus)
    {
        ShowWindow(p->hWndConsole, SW_HIDE);
    }
    else
    {
        ShowWindow(p->hWndConsole, SW_SHOW);
    }
}
static void __static_mmUIWindowApplication_LaunchOptions(struct mmUIWindowApplication* p)
{
    struct mmNativeApplication* pApplication = &p->hApplication;
    struct mmContextMaster* pContextMaster = &pApplication->hContextMaster;
    struct mmFileContext* pFileContext = &pContextMaster->hFileContext;
    struct mmPackageAssets* pPackageAssets = &pContextMaster->hPackageAssets;
    struct mmApplicationOptions* pOptions = &p->hApplicationOptions;

    const char* pAssetsRootFolderPath = mmString_CStr(&pOptions->AssetsRootFolderPath);
    const char* pAssetsRootFolderBase = mmString_CStr(&pOptions->AssetsRootFolderBase);
    const char* pAssetsRootSourcePath = mmString_CStr(&pOptions->AssetsRootSourcePath);
    const char* pAssetsRootSourceBase = mmString_CStr(&pOptions->AssetsRootSourceBase);

    int x = 0;
    int y = 0;
    int w = 0;
    int h = 0;

    mmApplicationOptions_Analysis(pOptions, mmString_CStr(&p->hLaunchFile));

    x = pOptions->CanvasRect[0];
    y = pOptions->CanvasRect[1];
    w = pOptions->CanvasRect[2];
    h = pOptions->CanvasRect[3];

    mmNativeApplication_SetRenderSystemName(&p->hApplication, mmString_CStr(&pOptions->RenderSystemName));
    mmNativeApplication_SetLoggerFileName(&p->hApplication, mmString_CStr(&pOptions->LoggerFileName));
    mmNativeApplication_SetLoggerLevel(&p->hApplication, pOptions->LoggerLevel);
    mmNativeApplication_SetDisplayFrequency(&p->hApplication, pOptions->DisplayFrequency);

    mmPackageAssets_SetPluginFolder(pPackageAssets, mmString_CStr(&pOptions->PluginFolder));
    mmPackageAssets_SetDynlibFolder(pPackageAssets, mmString_CStr(&pOptions->DynlibFolder));
    mmPackageAssets_SetShaderCacheFolder(pPackageAssets, mmString_CStr(&pOptions->ShaderCacheFolder));
    mmPackageAssets_SetLoggerPathName(pPackageAssets, mmString_CStr(&pOptions->LoggerPathName));
    mmPackageAssets_SetAssetsRootFolder(pPackageAssets, pAssetsRootFolderPath, pAssetsRootFolderBase);
    mmPackageAssets_SetAssetsRootSource(pPackageAssets, pAssetsRootSourcePath, pAssetsRootSourceBase);

    mmUIWindowApplication_SetCanvasRect(p, x, y, w, h);
    mmUIWindowApplication_SetFrameScale(p, pOptions->FrameScale);
}
static LRESULT CALLBACK __static_mmUIWindowApplication_WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    struct mmUIWindowApplication* p = (struct mmUIWindowApplication*)GetWindowLongPtr(hWnd, GWLP_USERDATA);

    if (NULL == p)
    {
        return DefWindowProc(hWnd, message, wParam, lParam);
    }

    switch (message)
    {
    case WM_COMMAND:
    {
        int wmId = LOWORD(wParam);
        // Analysis menu selection: 
        switch (wmId)
        {
        case IDM_ABOUT:
            DialogBox(p->hInstance, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, __static_mmUIWindowApplication_About);
            break;
        case IDM_EXIT:
            __static_mmUIWindowApplication_OnTerminal(p);
            break;
        case IDM_FILE_TERMINAL:
            __static_mmUIWindowApplication_SwitchConsoleStatus(p, !p->bConsoleStatus);
            break;
        default:
            return DefWindowProc(hWnd, message, wParam, lParam);
        }
    }
    break;

    case WM_PAINT:
    {
        PAINTSTRUCT ps;
        HDC hdc = BeginPaint(hWnd, &ps);
        // TODO: Add any drawing code using hdc here...
        EndPaint(hWnd, &ps);
    }
    break;

    case WM_DESTROY:
    {
        PostQuitMessage(0);
    }
    break;

    case WM_CLOSE:
    {
        __static_mmUIWindowApplication_OnTerminal(p);
    }
    break;

    case WM_SIZE:
    {
        if (FALSE == IsIconic(hWnd))
        {
            int w = LOWORD(lParam); // width of client area 
            int h = HIWORD(lParam); // Hight of client area 
            __static_mmUIWindowApplication_OnSizeChange(p, w, h);
        }

        // ActiveStatus
        {
            switch (wParam)
            {
            case SIZE_RESTORED:
                mmUIWindowApplication_OnEnterForeground(p);
                mmUIViewSurfaceMaster_OnEnterForeground(&p->hUIViewSurfaceMaster);
                break;
            case SIZE_MINIMIZED:
                mmUIViewSurfaceMaster_OnEnterBackground(&p->hUIViewSurfaceMaster);
                mmUIWindowApplication_OnEnterBackground(p);
                break;
            default:
                break;
            }
        }
    }
    break;

    case WM_ACTIVATE:
    {
        if (LOWORD(wParam) != WA_INACTIVE)
        {
            mmUIWindowApplication_OnEnterForeground(p);
        }
        else
        {
            mmUIWindowApplication_OnEnterBackground(p);
        }
        SendMessage(p->hUIViewSurfaceMaster.hWnd, message, wParam, lParam);
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    break;

    case WM_GETMINMAXINFO:
    {
        // Prevent the window from going smaller than some minimu size
        ((MINMAXINFO*)lParam)->ptMinTrackSize.x = 160;
        ((MINMAXINFO*)lParam)->ptMinTrackSize.y = 100;
    }
    break;

    case WM_KEYDOWN:
    case WM_KEYUP:
    case WM_IME_CHAR:
    case WM_IME_COMPOSITION:
    case WM_IME_STARTCOMPOSITION:
    case WM_IME_ENDCOMPOSITION:
    case WM_MOUSEWHEEL:
    {
        SendMessage(p->hUIViewSurfaceMaster.hWnd, message, wParam, lParam);
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    break;

    default:
    {
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    break;
    }
    return 0;
}

// The message handler for the "About" box.
INT_PTR CALLBACK __static_mmUIWindowApplication_About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}