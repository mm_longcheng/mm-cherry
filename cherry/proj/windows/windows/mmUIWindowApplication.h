/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmUIWindowApplication_h__
#define __mmUIWindowApplication_h__

#include "core/mmCore.h"

#include "nwsi/mmUIViewDefineWindows.h"
#include "nwsi/mmNativeApplication.h"
#include "nwsi/mmUIViewSurfaceMaster.h"

#include "mmApplicationOptions.h"

#include "mmActivityMaster.h"

#include "core/mmPrefix.h"

#define mmUIWindowApplication_X_DEFAULT 0
#define mmUIWindowApplication_Y_DEFAULT 0
// 1080 * 0.35 = 378
#define mmUIWindowApplication_W_DEFAULT 378
// 1920 * 0.35 = 672
#define mmUIWindowApplication_H_DEFAULT 672

struct mmUIWindowApplication
{
    struct mmActivityMaster pActivityMaster;
    struct mmNativeApplication hApplication;
    struct mmUIViewSurfaceMaster hUIViewSurfaceMaster;
    struct mmApplicationOptions hApplicationOptions;
    struct mmString hLaunchFile;

    double hDisplayDensity;
    double hFrameScale;

    // {x, y, w, h}
    double hCanvasRect[4];

    HWND hWndConsole;
    BOOL bConsoleStatus;

    // mmThreadState_t, default is MM_TS_CLOSED(0)
    mmSInt8_t state;

    CHAR szTitle[MM_MAX_LOADSTRING];

    HWND hWnd;
    HINSTANCE hInstance;
    HACCEL hAcceleratorTable;
    int nCmdShow;

    BOOL bMenu;
    DWORD dwExStyle;
    DWORD dwStyle;

    int hWindowX;
    int hWindowY;
    int hWindowW;
    int hWindowH;

    int hClientX;
    int hClientY;
    int hClientW;
    int hClientH;

    int hTerminateCode;
};

void mmUIWindowApplication_Init(struct mmUIWindowApplication* p);
void mmUIWindowApplication_Destroy(struct mmUIWindowApplication* p);

void mmUIWindowApplication_SetLaunchFile(struct mmUIWindowApplication* p, const char* pLaunchFile);

void mmUIWindowApplication_SetInstance(struct mmUIWindowApplication* p, HINSTANCE hInstance);
void mmUIWindowApplication_SetCommandShow(struct mmUIWindowApplication* p, int nCmdShow);
void mmUIWindowApplication_SetClientRect(struct mmUIWindowApplication* p, int x, int y, int w, int h);
void mmUIWindowApplication_SetCanvasRect(struct mmUIWindowApplication* p, int x, int y, int w, int h);
void mmUIWindowApplication_SetFrameScale(struct mmUIWindowApplication* p, double hFrameScale);
void mmUIWindowApplication_SetDisplayFrequency(struct mmUIWindowApplication* p, double hDisplayFrequency);

void mmUIWindowApplication_OnFinishLaunching(struct mmUIWindowApplication* p);
void mmUIWindowApplication_OnBeforeTerminate(struct mmUIWindowApplication* p);

void mmUIWindowApplication_OnEnterForeground(struct mmUIWindowApplication* p);
void mmUIWindowApplication_OnEnterBackground(struct mmUIWindowApplication* p);

void mmUIWindowApplication_OnStart(struct mmUIWindowApplication* p);
void mmUIWindowApplication_OnInterrupt(struct mmUIWindowApplication* p);
void mmUIWindowApplication_OnShutdown(struct mmUIWindowApplication* p);
void mmUIWindowApplication_OnJoin(struct mmUIWindowApplication* p);

ATOM mmUIWindowApplication_RegisterClass(HINSTANCE hInstance);
BOOL mmUIWindowApplication_UnregisterClass(HINSTANCE hInstance);

#include "core/mmSuffix.h"

#endif//__mmUIWindowApplication_h__
