:: copylibs.bat 32 x86 Debug   "_d"
:: copylibs.bat 32 x86 Release ""
:: copylibs.bat 64 x64 Debug   "_d"
:: copylibs.bat 64 x64 Release ""

@echo off

@set OPTION=/s /e /h /d /y

call :cp-libs %3 %4 %1 %2
call datafiles-mslink.bat %3

GOTO:EOF

:: cp-libs Debug _d 32 x86
:cp-libs
call :cp-lib  mm-lib  %~1 pthread          libpthread_shared%~2.dll
call :cp-lib  mm-lib  %~1 zziplib          libzzip_shared%~2.dll
call :cp-lib  mm-lib  %~1 zlib             libz_shared%~2.dll
call :cp-lib  mm-lib  %~1 zlib             libminizip_shared%~2.dll
call :cp-lib  mm-lib  %~1 libiconv         libiconv_shared%~2.dll
call :cp-lib  mm-lib  %~1 lua              liblua_shared%~2.dll
call :cp-lib  mm-lib  %~1 freetype         libfreetype_shared%~2.dll
call :cp-lib  mm-lib  %~1 FreeImage        libFreeImage_shared%~2.dll
call :cp-lib  mm-lib  %~1 pcre             libpcre_shared%~2.dll
call :cp-lib  mm-lib  %~1 sqlite           libsqlite_shared%~2.dll
call :cp-lib  mm-lib  %~1 Ogre             libOgreMain_shared%~2.dll
call :cp-lib  mm-lib  %~1 Ogre             libOgreRTShaderSystem_shared%~2.dll
call :cp-lib  mm-lib  %~1 Ogre             libOgreTerrain_shared%~2.dll
call :cp-lib  mm-lib  %~1 Ogre             libOgrePaging_shared%~2.dll
call :cp-lib  mm-lib  %~1 Ogre             libPlugin*.dll
call :cp-lib  mm-lib  %~1 Ogre             libRenderSystem*.dll
call :cp-lib  mm-lib  %~1 cegui            libCEGUIBase_shared%~2.dll
call :cp-lib  mm-lib  %~1 cegui            libCEGUICommonDialogs_shared%~2.dll
call :cp-lib  mm-lib  %~1 cegui            libCEGUICoreWindowRendererSet_shared%~2.dll
call :cp-lib  mm-lib  %~1 cegui            libCEGUIFreeImageImageCodec_shared%~2.dll
call :cp-lib  mm-lib  %~1 cegui            libCEGUITinyXMLParser_shared%~2.dll
call :cp-lib  mm-lib  %~1 openssl          libcrypto_shared%~2.dll
call :cp-lib  mm-lib  %~1 OpenAL           libOpenAL_shared%~2.dll
call :cp-lib  mm-lib  %~1 OgreAL           libOgreAL_shared%~2.dll
call :cp-lib  mm-lib  %~1 ogre-oggsound    libogre-oggsound_shared%~2.dll
call :cp-lib  mm-lib  %~1 libogg           libogg_shared%~2.dll
call :cp-lib  mm-lib  %~1 libvorbis        libvorbis_shared%~2.dll
call :cp-lib  mm-lib  %~1 libvorbis        libvorbisfile_shared%~2.dll
call :cp-lib  mm-lib  %~1 ParticleUniverse libPlugin_ParticleUniverse_shared%~2.dll
call :cp-lib  mm-lib  %~1 fdlibm           libfdlibm_shared%~2.dll

call :cp-core mm-core %~1 mm               libmm_core_shared%~2.dll
call :cp-core mm-core %~1 mm               libmm_net_shared%~2.dll
call :cp-core mm-core %~1 dish             libmm_dish_shared%~2.dll
call :cp-core mm-core %~1 dish             libmm_fix32_shared%~2.dll
call :cp-core mm-core %~1 dish             libmm_raptorq_shared%~2.dll
call :cp-core mm-core %~1 dish             libmm_track_shared%~2.dll
call :cp-core mm-core %~1 dish             libmm_rq_shared%~2.dll
call :cp-core mm-core %~1 dish             libmm_unicode_shared%~2.dll
call :cp-core mm-core %~1 nwsi             libmm_nwsi_shared%~2.dll
call :cp-core mm-core %~1 nwsi             libCEGUIOgreRenderer_shared%~2.dll
call :cp-core mm-core %~1 data             libmm_data_protobuf_shared%~2.dll
call :cp-core mm-core %~1 data             libmm_data_lua_shared%~2.dll
call :cp-core mm-core %~1 data             libmm_data_openssl_shared%~2.dll
call :cp-core mm-nes  %~1 emu              libmm_emu_shared%~2.dll
call :cp-core mm-nes  %~1 emu              libmm_emulator_shared%~2.dll

@xcopy %POWERVR_SDK_HOME%\\Builds\\Windows\\x86_%~3\\Lib\*.dll bin\\%~1\* %OPTION%
@xcopy %MM_HOME%\\mm-lib\\sdk\\Cg\\bin\\windows\\%~4\cg.dll    bin\\%~1\* %OPTION%
echo f | @xcopy launch_binary.config                           bin\\%~1\launch.config /h /d /y
GOTO:EOF

:: cp-lib mm-lib Debug pthread libpthread_shared.dll
:cp-lib
@xcopy %MM_HOME%\\%~1\\build\\%~3\\proj_windows\\bin\\%~2\%~4 bin\\%~2\* %OPTION%
GOTO:EOF

:: cp-core mm-core Debug mm libmm_core_shared.dll
:cp-core
@xcopy %MM_HOME%\\%~1\\%~3\\proj\\windows\\bin\\%~2\%~4 bin\\%~2\* %OPTION%
GOTO:EOF
