LOCAL_PATH := $(call my-dir)

MM_MAKE_HOME ?= $(MM_HOME)/mm-make

include $(LOCAL_PATH)/lib_prebuild.mk

include $(LOCAL_PATH)/libmm_eggs_shared.mk

$(call import-module,android/cpufeatures) 
$(call import-module,android/native_app_glue)