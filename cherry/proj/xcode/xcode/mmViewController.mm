//
//  mmViewController.m
//  mm_nwsi
//
//  Created by mm_longcheng on 2019/10/15.
//  Copyright © 2019 mm_longcheng@icloud.com. All rights reserved.
//

#import "mmViewController.h"

#import "nwsi/mmObjcARC.h"

#import "nwsi/mmNativeApplication.h"
#import "nwsi/mmUIViewSurfaceMaster.h"


@interface mmViewController ()
{
    struct mmNativeApplication* pNativeApplication;
}

@property (weak, nonatomic) UIWindow* windowMaster;

@end

@implementation mmViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)dealloc
{
    mmObjc_Dealloc(super);
}

- (void)SetWindowMaster:(UIWindow*)window
{
    self.windowMaster = window;
}
- (void)SetNativeApplication:(struct mmNativeApplication*)pNativeApplication
{
    self->pNativeApplication = pNativeApplication;
}

- (void)OnFinishLaunching
{
    struct mmSurfaceMaster* pSurfaceMaster = &self->pNativeApplication->hSurfaceMaster;
    
    [self.viewSurfaceMaster SetSurfaceMaster: pSurfaceMaster];
    [self.viewSurfaceMaster SetWindowMaster: self.windowMaster];
    [self.viewSurfaceMaster OnFinishLaunching];
}
- (void)OnBeforeTerminate
{
    [self.viewSurfaceMaster OnBeforeTerminate];
}

- (void)OnEnterBackground
{
    [self.viewSurfaceMaster OnEnterBackground];
}
- (void)OnEnterForeground
{
    [self.viewSurfaceMaster OnEnterForeground];
}

@end
