//
//  mmViewController.h
//  mm_nwsi
//
//  Created by mm_longcheng on 2019/10/15.
//  Copyright © 2019 mm_longcheng@icloud.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@class mmUIViewSurfaceMaster;

@interface mmViewController : UIViewController

@property (weak, nonatomic) IBOutlet mmUIViewSurfaceMaster *viewSurfaceMaster;

- (void)SetWindowMaster:(UIWindow*)window;
- (void)SetNativeApplication:(struct mmNativeApplication*)pNativeApplication;

- (void)OnFinishLaunching;
- (void)OnBeforeTerminate;

- (void)OnEnterBackground;
- (void)OnEnterForeground;

@end

