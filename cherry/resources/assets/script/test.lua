local mmLuaContext = require("mm/mmLuaContext")
local mmLogger = require("mm/mmLogger")

local ModelError = require("model/ModelError")
local ModelLanguage = require("model/ModelLanguage")

ModelError.Test_Func()
ModelLanguage.Test_Func()
