local mmLogger = require("mm/mmLogger")
local math = require('math')
local pairs = pairs
local print = print
local tostring = tostring
local string = string
----------------------------------------
local modname="ModelTalk"
local M={}
_G[modname]=M
package.loaded[modname]=M
if setfenv then
	setfenv(1, M) -- for 5.1
else
	_ENV = M -- for 5.2
end
----------------------------------------------
--member
MessageColour =
{
	[0] = {r=1.0,g=0.8,b=0.0,a=1.0,},
	[1] = {r=1.0,g=1.0,b=1.0,a=1.0,},
}
----------------------------------------------
--function
function TalkMessageColour(flag)
	local u = MessageColour[flag]
	if u then
		return u.r,u.g,u.b,u.a
	else
		mmLogger.LogW(modname..".TalkMessageColour flag:"..flag.." is invalid.")
	end
	return 1,1,1,1
end
----------------------------------------------
--test
----------------------------------------------
return M
