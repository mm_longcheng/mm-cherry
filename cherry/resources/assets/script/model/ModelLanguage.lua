local mmLogger = require("mm/mmLogger")
local math = require('math')
local pairs = pairs
local print = print
local tostring = tostring
local string = string
----------------------------------------
local DataLanguage = require('data/DataLanguage')
----------------------------------------
local modname="ModelLanguage"
local M={}
_G[modname]=M
package.loaded[modname]=M
if setfenv then
	setfenv(1, M) -- for 5.1
else
	_ENV = M -- for 5.2
end
----------------------------------------------
--member
----------------------------------------------
--function
function LanguageDescByCode(language, code)
	local table_name = language
	local t = DataLanguage[table_name]
	if t then
		return t[code].desc
	else
		mmLogger.LogW(modname..".LanguageDescByCode language:"..language.." is invalid.")
	end
	return ""
end
----------------------------------------------
--test
function Test_0()
	local desc1 = LanguageDescByCode("zh_CN", 0x00000001)
	print("desc1: "..desc1)
	local desc2 = LanguageDescByCode("en_US", 0x00000001)
	print("desc2: "..desc2)
end
function Test_Func()
	Test_0()
end
----------------------------------------------
return M
