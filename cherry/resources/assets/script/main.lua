local p = {}
----------------------------------------------
package.path = "script/?.lua;" .. package.path
----------------------------------------------
local mmLuaContext = require("mm/mmLuaContext")
local mmLogger = require("mm/mmLogger")
----------------------------------------------
function LoaderFunction(modulename)
	--print (modulename, package.path)
    local errmsg = ""
    local modulepath = string.gsub(modulename, "%.", "/")
    for path in string.gmatch(package.path, "([^;]+)") do
        local filename = string.gsub(path, "%?", modulepath)
        filename = string.gsub(filename, "\\", "/")
        local result = mmLuaContext.RequireModule(filename)
        if result then
            return result
        end
        errmsg = errmsg.."\n\tno file '"..filename.."' (checked with custom loader)"
    end
  return errmsg
end
function Initialize()
	if nil ~= package.loaders then
		-- version <= 5.1
		table.insert(package.loaders, 1, LoaderFunction)
	else
		-- version >= 5.2
		table.insert(package.searchers, 1, LoaderFunction)
	end
	--
	require("model/ModelError")
	require("model/ModelLanguage")
	--
	mmLogger.LogI("lua main Initialize success.")
end
----------------------------------------------
Initialize()
----------------------------------------------
return p
