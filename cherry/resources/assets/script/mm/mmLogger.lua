local p = {}
----------------------------------------------
function LoggerPrint( message ) print(message) end
----------------------------------------------
function p.LogU( message ) LoggerPrint( message ) end
function p.LogF( message ) LoggerPrint( message ) end
function p.LogC( message ) LoggerPrint( message ) end
function p.LogE( message ) LoggerPrint( message ) end
function p.LogA( message ) LoggerPrint( message ) end
function p.LogW( message ) LoggerPrint( message ) end
function p.LogN( message ) LoggerPrint( message ) end
function p.LogI( message ) LoggerPrint( message ) end
function p.LogT( message ) LoggerPrint( message ) end
function p.LogD( message ) LoggerPrint( message ) end
function p.LogV( message ) LoggerPrint( message ) end
----------------------------------------------
return p
