local type = type
----------------------------------------
local modname="DataLanguage"
local M={}
_G[modname]=M
package.loaded[modname]=M
if setfenv then
	setfenv(1, M) -- for 5.1
else
	_ENV = M -- for 5.2
end
----------------------------------------
if not (type(language)=="table") then
	language = {}
end

language =
{
	[1]= {desc="English (United States)",language="en_US",},
	[2]= {desc="中文 (中国)",language="zh_CN",},
}
if not (type(code)=="table") then
	code = {}
end

code =
{
	[1]= {id=1,desc="删除资源二次确认",hex="0x00000001",},
	[2]= {id=2,desc="退出程序二次确认",hex="0x00000002",},
}
if not (type(zh_CN)=="table") then
	zh_CN = {}
end

zh_CN =
{
	[1]= {id=1,desc="删除操作不可逆.输入确认码来避免误操作.中部按钮可以跳过确认码。",},
	[2]= {id=2,desc="确定要退出程序吗?",},
}
if not (type(en_US)=="table") then
	en_US = {}
end

en_US =
{
	[1]= {id=1,desc="The delete operation is irreversible. Enter the confirmation code to avoid misuse. The middle button can skip the confirmation code.",},
	[2]= {id=2,desc="Are you sure you want to quit the program?",},
}
----------------------------------------------
return M
