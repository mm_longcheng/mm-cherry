#version 150 core

uniform sampler2D texture0;

in vec2 outUV0;
in vec4 outColor;

out vec4 fragColour;

void main(void)
{
   fragColour = texture(texture0, outUV0) * outColor;
}