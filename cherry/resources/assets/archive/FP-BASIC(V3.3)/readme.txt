﻿＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝  
          FC浮点游戏BASIC ( Family Computer Game & Float Point BASIC )
＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝  


 [1] 兼容任天堂原版 FBASIC(V3.0), 内存由4K扩展到8K,支持V3.0的扩张语法和命令(如 AUTO)
 
 [2] 移植 Apple II 的浮点运算模块,可以用浮点函数: SIN, COS, TAN, ATN, LOG, SQR, INT, ^ 等
 
 [3] 小霸王键盘(Subor Keyboard)兼容

 [4] 支持磁带记录BASIC文件和BG背景作图文件

 [5] 支持联机打印(模拟器虽然不能模拟,但实际卡带可以)


    请用惊风(Temryu)的可支持小霸王键盘的模拟器:VirtuaNESex(071111)或以上版本,文件调入后先要
将模拟器设置成小霸王键盘模式:
    
    在模拟器菜单"编辑"->"特殊控制器"设置成"小霸王键盘"，每次打开NES文件时要设置一次。
 
    我自己还改写了个"英雄救美女"的FP-BSIAC游戏,其虚拟磁带文件是：fp_hero.vtp ，大家可以用模拟器
将它调入内存中运行，有关FP-BASIC的磁带存取命令说明如下：

   存BASIC源程序到磁带命令： SAVE “文件名"   或 SAVE         (SAVE 可简写成 SA.)
   从磁带取BASIC源程序命令： LOAD “文件名"   或 LOAD         (LOAD 可简写成 LO.)

   模拟器的磁带存取功能在模拟器菜单"编辑"->"特殊设备"->"磁带"。在用SAVE命令前应先开模拟器的"录制";
在用LOAD命令时应后开模拟器的"播放".   

   进入FP-BASIC后,用LOAD (或LO.)将磁带文件fp_hero.vtp读入(按"E"键可以中断读文件),你可以用LIST(或L.)
来列表程序,按"ESC"键来暂停列表(再按任意键恢复列表),用RUN(或R.)来运行程序。当然你也可以修改它,再用
SAVE(或SA.)来保存你修改后的程序,要热启动(Hot Reaset)FP-BASIC,请打入 SYSTEM(或S.)命令。

 
 文件： FP-BASIC(V3.3).nes  : 浮点游戏BASIC语言的FC ROM
        FP_HERO.VTP         : 浮点型的“英雄救美女”BASIC游戏


    具体介绍请看: http://hi.baidu.com/maxzhou88/blog/item/3f2a29dec5700153cdbf1a46.html
                           
 

                        原创人：周哥 maxzhou88 (zhougeng@tom.com)
			        http://hi.baidu.com/maxzhou88
                                http://maxzhou88.ys168.com/
                                Hopework Soft Dep.
					               2010-02-27
                        欢迎转载请注明出处和原作者并保持文件包的完整性