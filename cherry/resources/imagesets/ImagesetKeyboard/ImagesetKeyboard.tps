<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.6.1</string>
        <key>fileName</key>
        <string>ImagesetKeyboard.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>CEGUI imageset</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>imageset</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>ImagesetKeyboard.imageset</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <false/>
        <key>trimSpriteNames</key>
        <true/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>0</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">None</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">texture/0.png</key>
            <key type="filename">texture/1.png</key>
            <key type="filename">texture/2.png</key>
            <key type="filename">texture/3.png</key>
            <key type="filename">texture/4.png</key>
            <key type="filename">texture/5.png</key>
            <key type="filename">texture/6.png</key>
            <key type="filename">texture/7.png</key>
            <key type="filename">texture/8.png</key>
            <key type="filename">texture/9.png</key>
            <key type="filename">texture/A.png</key>
            <key type="filename">texture/ALT.png</key>
            <key type="filename">texture/APOSTROPHE.png</key>
            <key type="filename">texture/ARROWD.png</key>
            <key type="filename">texture/ARROWL.png</key>
            <key type="filename">texture/ARROWR.png</key>
            <key type="filename">texture/ARROWU.png</key>
            <key type="filename">texture/B.png</key>
            <key type="filename">texture/BACKGROUND.png</key>
            <key type="filename">texture/BRACKETL.png</key>
            <key type="filename">texture/BRACKETR.png</key>
            <key type="filename">texture/C.png</key>
            <key type="filename">texture/COMMA.png</key>
            <key type="filename">texture/D.png</key>
            <key type="filename">texture/DELETE.png</key>
            <key type="filename">texture/E.png</key>
            <key type="filename">texture/END.png</key>
            <key type="filename">texture/EQUALS.png</key>
            <key type="filename">texture/ESCAPE.png</key>
            <key type="filename">texture/F.png</key>
            <key type="filename">texture/F1.png</key>
            <key type="filename">texture/F10.png</key>
            <key type="filename">texture/F11.png</key>
            <key type="filename">texture/F12.png</key>
            <key type="filename">texture/F2.png</key>
            <key type="filename">texture/F3.png</key>
            <key type="filename">texture/F4.png</key>
            <key type="filename">texture/F5.png</key>
            <key type="filename">texture/F6.png</key>
            <key type="filename">texture/F7.png</key>
            <key type="filename">texture/F8.png</key>
            <key type="filename">texture/F9.png</key>
            <key type="filename">texture/Fn.png</key>
            <key type="filename">texture/G.png</key>
            <key type="filename">texture/GRAVE.png</key>
            <key type="filename">texture/H.png</key>
            <key type="filename">texture/HOME.png</key>
            <key type="filename">texture/I.png</key>
            <key type="filename">texture/INSERT.png</key>
            <key type="filename">texture/J.png</key>
            <key type="filename">texture/K.png</key>
            <key type="filename">texture/L.png</key>
            <key type="filename">texture/LIGHT_CAPS.png</key>
            <key type="filename">texture/LIGHT_NUM.png</key>
            <key type="filename">texture/LIGHT_SCROLL.png</key>
            <key type="filename">texture/M.png</key>
            <key type="filename">texture/MINUS.png</key>
            <key type="filename">texture/N.png</key>
            <key type="filename">texture/O.png</key>
            <key type="filename">texture/P.png</key>
            <key type="filename">texture/PAGED.png</key>
            <key type="filename">texture/PAGEU.png</key>
            <key type="filename">texture/PB.png</key>
            <key type="filename">texture/PERIOD.png</key>
            <key type="filename">texture/PS.png</key>
            <key type="filename">texture/Q.png</key>
            <key type="filename">texture/R.png</key>
            <key type="filename">texture/S.png</key>
            <key type="filename">texture/SEMICOLON.png</key>
            <key type="filename">texture/SL.png</key>
            <key type="filename">texture/SLASHR.png</key>
            <key type="filename">texture/S_1.png</key>
            <key type="filename">texture/S_2.png</key>
            <key type="filename">texture/S_3.png</key>
            <key type="filename">texture/S_4.png</key>
            <key type="filename">texture/S_5.png</key>
            <key type="filename">texture/S_6.png</key>
            <key type="filename">texture/S_7.png</key>
            <key type="filename">texture/S_8.png</key>
            <key type="filename">texture/S_9.png</key>
            <key type="filename">texture/S_DECIMAL.png</key>
            <key type="filename">texture/S_DIV.png</key>
            <key type="filename">texture/S_MUL.png</key>
            <key type="filename">texture/S_NUM.png</key>
            <key type="filename">texture/S_SUB.png</key>
            <key type="filename">texture/T.png</key>
            <key type="filename">texture/U.png</key>
            <key type="filename">texture/V.png</key>
            <key type="filename">texture/W.png</key>
            <key type="filename">texture/X.png</key>
            <key type="filename">texture/Y.png</key>
            <key type="filename">texture/Z.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>32,32,64,64</rect>
                <key>scale9Paddings</key>
                <rect>32,32,64,64</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">texture/APPMENU.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>40,32,80,64</rect>
                <key>scale9Paddings</key>
                <rect>40,32,80,64</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">texture/BACKSPACE.png</key>
            <key type="filename">texture/COMMAND.png</key>
            <key type="filename">texture/ENTER.png</key>
            <key type="filename">texture/SLASHL.png</key>
            <key type="filename">texture/TAB.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>43,32,86,64</rect>
                <key>scale9Paddings</key>
                <rect>43,32,86,64</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">texture/CAPSLOCK.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>49,32,99,64</rect>
                <key>scale9Paddings</key>
                <rect>49,32,99,64</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">texture/CTRL.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>39,32,79,64</rect>
                <key>scale9Paddings</key>
                <rect>39,32,79,64</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">texture/SHIFT.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>66,32,131,64</rect>
                <key>scale9Paddings</key>
                <rect>66,32,131,64</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">texture/SPACE.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>195,32,389,64</rect>
                <key>scale9Paddings</key>
                <rect>195,32,389,64</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">texture/S_0.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>64,32,128,64</rect>
                <key>scale9Paddings</key>
                <rect>64,32,128,64</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">texture/S_ADD.png</key>
            <key type="filename">texture/S_ENTER.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>32,64,64,128</rect>
                <key>scale9Paddings</key>
                <rect>32,64,64,128</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>texture</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
