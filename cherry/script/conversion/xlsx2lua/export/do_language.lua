local script_common_root="../../.."
local script_use_root=".."
local import_file_path=script_use_root.."/data/xlsx"
local export_file_path=script_use_root.."/data/lua"
-----------------------------------------------------------
local old_path = package.path
package.path =
";"..script_common_root.."/generator/?.lua"..
";"..script_common_root.."/generator/?.?"..
";"..script_common_root.."/xlsx_export/?.lua"..
";"..script_common_root.."/xlsx_export/?.?"..
package.path

require 'os'
local xlsx_export = require 'xlsx_export'
local xlsx_util = require 'xlsx_util'
local math = require 'math'
package.path = old_path
-----------------------------------------------------------
xlsx_export.cscript_path = script_common_root.."/xlsx_export/"
-----------------------------------------------------------------
xlsx_export.export_csv(import_file_path.."/language.xlsx", 0)
-----------------------------------------------------------------
local of_file = io.open(export_file_path.."/DataLanguage.lua", "w")
if not of_file then
	print("Failed to open output file: DataLanguage.lua")
else
----------------------------------------
	of_file:write([[
local type = type
----------------------------------------
local modname="DataLanguage"
local M={}
_G[modname]=M
package.loaded[modname]=M
if setfenv then
	setfenv(1, M) -- for 5.1
else
	_ENV = M -- for 5.2
end
----------------------------------------
]])
----------------------------------------
	of_file:write([[
if not (type(language)=="table") then
	language = {}
end

language =]])
-- id	task_sub_type	task_desc	note
local data_language_s = {}
function handle_data_language(o)
	local language 			= o[1][1]
	-- if id and id>0 then
	if language and language ~= 0 then --
		local cell = {}
		------------------------------
		-- 语言缩写	中文描述
		cell.language 			= language			--语言缩写
		cell.desc 				= o[1][2]           --中文描述
		------------------------------
		data_language_s[#data_language_s + 1] = cell
	end
end

xlsx_export.handle_file("tmp/language.csv", 3, 1, handle_data_language)
xlsx_export.output_table(data_language_s, of_file)
----------------------------------------
	of_file:write([[
if not (type(code)=="table") then
	code = {}
end

code =]])
-- id	task_sub_type	task_desc	note
local data_code_s = {}
function handle_data_code(o)
	local id 			= tonumber(o[1][1])
	if id and id > 0 then
		local cell = {}
		------------------------------
		-- 编号	中文描述
		cell.id 				= id			    --编号
		cell.hex 				= o[1][1]			--编号16进制
		cell.desc 				= o[1][2]			--中文描述
		------------------------------
		data_code_s[cell.id] = cell
	end
end

xlsx_export.handle_file("tmp/code.csv", 3, 1, handle_data_code)
xlsx_export.output_table(data_code_s, of_file)
----------------------------------------
	of_file:write([[
if not (type(zh_CN)=="table") then
	zh_CN = {}
end

zh_CN =]])
-- id	task_sub_type	task_desc	note
local data_zh_CN_s = {}
function handle_data_zh_CN(o)
	local id 			= tonumber(o[1][1])
	if id and id > 0 then
		local cell = {}
		------------------------------
		-- 编号	描述
		cell.id 				= id			--编号
		cell.desc 				= o[1][2]       --描述
		------------------------------
		data_zh_CN_s[cell.id] = cell
	end
end

xlsx_export.handle_file("tmp/zh_CN.csv", 3, 1, handle_data_zh_CN)
xlsx_export.output_table(data_zh_CN_s, of_file)
----------------------------------------
	of_file:write([[
if not (type(en_US)=="table") then
	en_US = {}
end

en_US =]])
-- id	task_sub_type	task_desc	note
local data_en_US_s = {}
function handle_data_en_US(o)
	local id 			= tonumber(o[1][1])
	if id and id > 0 then
		local cell = {}
		------------------------------
		-- 编号	描述
		cell.id 				= id			--编号
		cell.desc 				= o[1][2]       --描述
		------------------------------
		data_en_US_s[cell.id] = cell
	end
end

xlsx_export.handle_file("tmp/en_US.csv", 3, 1, handle_data_en_US)
xlsx_export.output_table(data_en_US_s, of_file)
-----------------------------------------------------------------
	of_file:write([[
----------------------------------------------
return M
]])
end
-----------------------------------------------------------------
xlsx_export.clear_csv()
of_file:close()
local source_file = "../data/lua/DataLanguage.lua"
local target_file = script_common_root.."/../resources/assets/script/data/DataLanguage.lua"
xlsx_util.copy_file(source_file, target_file)
