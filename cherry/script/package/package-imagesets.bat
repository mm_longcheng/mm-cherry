@echo off 

cd %~dp0

set Packer=TexturePacker
set ImagePath=..\..\resources\imagesets
set AssetsPath=..\..\resources\assets\cegui\imagesets
set Option1=
set Option2=/s /h /d /y

for /d %%i in (%ImagePath%\*) do (
    call :PackerTPS %%i
)
:::::::::::::::::::::::::::::::::::::::::::::::::
pause
::
GOTO :EOF
:::::::::::::::::::::::::::::::::::::::::::::::::
:: PackerTPS function.
:PackerTPS
SETLOCAL
:: REM.
for /r %%i in (%~1\*.tps) do (
    %Packer% %%i %Option1%
    xcopy %~1\%%~ni.png      %AssetsPath%\* %Option2%
    xcopy %~1\%%~ni.imageset %AssetsPath%\* %Option2%
)
ENDLOCAL
GOTO :EOF
:::::::::::::::::::::::::::::::::::::::::::::::::
pause