#!/bin/bash

function PackerTPS()
{
	# echo $1
	for i in `ls $1 | grep .tps`
	do
		${Packer} $1/${i} ${Option1}
		${Copy} ${Option2} $1/${i%.*}.png      ${AssetsPath}
		${Copy} ${Option2} $1/${i%.*}.imageset ${AssetsPath}
	done
}

function PackerDirectory()
{
	for i in `ls $1`
	do
		PackerTPS $1/${i}
	done
}

Packer=TexturePacker
Copy=cp
ImagePath=../../resources/imagesets
AssetsPath=../../resources/assets/cegui/imagesets
Option1=
Option2=

PackerDirectory ${ImagePath}
