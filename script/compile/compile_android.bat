@echo off
:::::::::::::::::::::::::::::::::::::::::::::::::
set PLATFORM=android
set ACTION=%*
:::::::::::::::::::::::::::::::::::::::::::::::::
cd ../..
:::::::::::::::::::::::::::::::::::::::::::::::::
call :build_project "cherry"
:::::::::::::::::::::::::::::::::::::::::::::::::
cd script
:::::::::::::::::::::::::::::::::::::::::::::::::
pause
::
GOTO :EOF
:::::::::::::::::::::::::::::::::::::::::::::::::
:: build_project function.
:build_project
SETLOCAL
REM.
cd %~1
cd proj/%PLATFORM%
echo y | compile.bat %ACTION%
cd ../..
cd ..
ENDLOCAL
GOTO :EOF
:::::::::::::::::::::::::::::::::::::::::::::::::
pause